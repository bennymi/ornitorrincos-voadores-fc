/*
 * PlaySide.h
 *
 *  Created on: Aug 29, 2015
 *      Author: mmaximo
 */

#ifndef SOURCE_REPRESENTATIONS_PLAYSIDE_H_
#define SOURCE_REPRESENTATIONS_PLAYSIDE_H_

#include <iostream>

namespace representations {

    class PlaySide {
    public:
        enum PLAY_SIDE {
            LEFT, RIGHT, UNKNOWN,
        };

        static std::string toString(representations::PlaySide::PLAY_SIDE playSide) {
            std::string str;
            if (playSide == representations::PlaySide::LEFT) {
                str = "Left";
                return str;
            } else if (playSide == representations::PlaySide::RIGHT) {
                str = "Right";
                return str;
            } else {
                str = "Unknown";
                return str;
            }
        }

        static representations::PlaySide::PLAY_SIDE getOppositePlaySide(representations::PlaySide::PLAY_SIDE playSide) {
            if (playSide == representations::PlaySide::LEFT) {
                return representations::PlaySide::RIGHT;
            } else if (playSide == representations::PlaySide::RIGHT) {
                return representations::PlaySide::LEFT;
            } else {
                return representations::PlaySide::UNKNOWN;
            }
        }
    };
} /* namespace representations */

#endif /* SOURCE_REPRESENTATIONS_PLAYSIDE_H_ */
