//
// Created by luckeciano on 01/05/16.
//

#ifndef PROJECT_DRAWVISIBLEOBJECTS_H
#define PROJECT_DRAWVISIBLEOBJECTS_H

#include "base/BaseAgentTest.h"


class DrawVisibleObjects {
public:
    DrawVisibleObjects();

    virtual ~DrawVisibleObjects();

    void draw(Pose2D robotPose, perception::Perception &perception, modeling::AgentModel &agentModel,
              utils::roboviz::Roboviz &roboviz);

    void drawLineBallToAgent(modeling::Modeling &modeling, utils::roboviz::Roboviz &roboviz);

    void drawVisionDirection(modeling::Modeling &modeling, utils::roboviz::Roboviz &roboviz);

    void drawLineToObstacles(modeling::Modeling &modeling, utils::roboviz::Roboviz &roboviz);

    void drawCircleToMarking(modeling::Modeling &modeling, utils::roboviz::Roboviz &roboviz,
                             representations::HearData &hearData);

};


#endif //PROJECT_DRAWVISIBLEOBJECTS_H
