//
// Created by luckeciano on 04/06/16.
//

#include "base/BaseAgentTest.h"
#include "decision_making/positioning/BasicDefensivePositioning.h"

class BasicDefensivePositioningAgent : public BaseAgentTest {
private:
    decision_making::positioning::BasicDefensePositioning basicDefensePositioning;


public:
    virtual void testLoop(int nSteps) { BaseAgentTest::testLoop(nSteps); }

    virtual void setup() {
        wiz.setAgentPositionAndDirection(1, representations::PlaySide::PLAY_SIDE::LEFT, Vector3<double>(0.0, 0.0, 0.3),
                                         0.0);
    }

    virtual void runStep() {

        Pose2D position = basicDefensePositioning.getAgentPosition(modeling);

        if (!decisionMaking.decideFall(modeling)) {

            behaviorFactory.getFocusBall().behave(modeling);
            decisionMaking.lookRequest = behaviorFactory.getFocusBall().getLookRequest();

            behaviorFactory.getNavigatePosition().setTarget(position);
            behaviorFactory.getNavigatePosition().behave(modeling);
            decisionMaking.movementRequest = behaviorFactory.getNavigatePosition().getMovementRequest();
        }

        std::string buff("animated");
        std::string name("animated");
        roboviz.drawCircle(modeling.getWorldModel().getBall().getPosition().translation.x,
                           modeling.getWorldModel().getBall().getPosition().translation.y, 0.2,
                           5, 1, 100, 0, &name);
        roboviz.drawLine(modeling.getWorldModel().getSelf().getPosition().translation.x,
                         modeling.getWorldModel().getSelf().getPosition().translation.y,
                         0, modeling.getWorldModel().getSelf().getPosition().translation.x,
                         modeling.getWorldModel().getSelf().getPosition().translation.y, 1,
                         5, 233, 12, 0, &name);
        roboviz.drawLine(modeling.getWorldModel().getSelf().getPosition().translation.x,
                         modeling.getWorldModel().getSelf().getPosition().translation.y, 0.0, position.translation.x,
                         position.translation.y, 0.0,
                         1.0, 1.0, 0.0, 0.0, &name);

        roboviz.drawLine(modeling.getWorldModel().getOpponents()[1]->getPosition().translation.x,
                         modeling.getWorldModel().getOpponents()[1]->getPosition().translation.y,
                         0, modeling.getWorldModel().getOpponents()[1]->getPosition().translation.x,
                         modeling.getWorldModel().getOpponents()[1]->getPosition().translation.y, 1, 1, 1, 0, 0, &name);
        roboviz.drawLine(modeling.getWorldModel().getOpponents()[2]->getPosition().translation.x,
                         modeling.getWorldModel().getOpponents()[2]->getPosition().translation.y,
                         0, modeling.getWorldModel().getOpponents()[2]->getPosition().translation.x,
                         modeling.getWorldModel().getOpponents()[2]->getPosition().translation.y, 1, 1, 1, 0, 0, &name);
        roboviz.swapBuffers(&buff);

        roboviz.drawLine(position.translation.x, position.translation.y, 0.0, position.translation.x,
                         position.translation.y, 2.0,
                         1.0, 1.0, 0.0, 0.0, &name);
        roboviz.drawLine(modeling.getWorldModel().getOpponentClosestFromBall().getPosition().translation.x,
                         modeling.getWorldModel().getOpponentClosestFromBall().getPosition().translation.y, 0.0,
                         modeling.getWorldModel().getOpponentClosestFromBall().getPosition().translation.x,
                         modeling.getWorldModel().getOpponentClosestFromBall().getPosition().translation.y, 2.5,
                         4.5, 1.0, 50, 0.0, &name);
        roboviz.swapBuffers(&buff);

        std::cout << "Closest Op X: "
                  << modeling.getWorldModel().getOpponentClosestFromBall().getPosition().translation.x << endl;
        std::cout << "Closest Op Y: "
                  << modeling.getWorldModel().getOpponentClosestFromBall().getPosition().translation.y << endl;


    }
};

int main() {

    /**
    * Start the external agent
    */
    std::vector<std::thread> threads;
    for (int i = 2; i < 4; i++) {
        std::stringstream stream;
        stream << "./PassiveAgent_AgentTest " << i << " " << "Enemy &";
        system(stream.str().c_str());
        stream.str("");
        stream << "sleep 5";
        system(stream.str().c_str());
    }

    /**
     * Start active agent
     */
    BasicDefensivePositioningAgent t;
    t.testLoop(50000);
}

