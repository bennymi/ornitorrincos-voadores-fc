//
// Created by francisco on 6/16/16.
//

#ifndef ITANDROIDS_SOCCER3D_CPP_LONGDISTANCEKICK_H
#define ITANDROIDS_SOCCER3D_CPP_LONGDISTANCEKICK_H

#include "KeyframeKick.h"

namespace decision_making {
    namespace behavior {
        namespace kick_behavior {
            class LongDistanceKick : public KeyframeKick {
            public:
                LongDistanceKick();

            private:

            };

        }
    }
}


#endif //ITANDROIDS_SOCCER3D_CPP_LONGDISTANCEKICK_H
