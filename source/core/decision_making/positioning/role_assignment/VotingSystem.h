//
// Created by luckeciano on 9/11/16.
//

#ifndef ITANDROIDS_SOCCER3D_CPP_VOTINGSYSTEM_H
#define ITANDROIDS_SOCCER3D_CPP_VOTINGSYSTEM_H

#include <vector>
#include <core/modeling/Modeling.h>


namespace decision_making {
    namespace positioning {
        namespace role_assignment {


            class VotingSystem {

            public:
                void receiveRoleAssignVectors(modeling::PositioningModel &positioningModel);

                std::vector<int> generateSyncRoleVector(modeling::PositioningModel &positioningModel);


            private:
                /*
                 * Vectors of role assign, corresponding to each player
                 */
                std::vector<int> roleAssignmentVectors[modeling::WorldModel::NUM_PLAYERS_PER_TEAM];

            };
        }
    }
}


#endif //ITANDROIDS_SOCCER3D_CPP_VOTINGSYSTEM_H
