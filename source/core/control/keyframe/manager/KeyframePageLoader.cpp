//
// Created by francisco on 17/12/16.
//

#include "KeyframePageLoader.h"
#include "control/keyframe/page/LinearKeyframePage.h"
#include "control/keyframe/page/SplineKeyframePage.h"
#include "control/keyframe/page/KeyframeStep.h"

using control::keyframe::KeyframeStep;

namespace control {
namespace keyframe {
namespace manager {


template<>
std::shared_ptr<KeyframePage> KeyframePageLoader<pt::ptree>::initializeAndLoadSimplePage(pt::ptree &pageInfo){
    std::shared_ptr<KeyframePage> generatedPage;
    std::string name;
    double speedRate;
    std::string interpolator;

    for (pt::ptree::value_type & child : pageInfo){
        if(child.first == "name"){
            name = child.second.get_value<std::string>();
        } else if(child.first == "speedRate"){
            speedRate = child.second.get_value<double>();
        } else if(child.first == "interpolator"){
            interpolator = child.second.data();
            if(interpolator == "linear"){
                generatedPage = std::make_shared<LinearKeyframePage>();
            } else if(interpolator == "spline"){
                generatedPage = std::make_shared<SplineKeyframePage>();
            } else {
                generatedPage = std::make_shared<LinearKeyframePage>();
            }
        } else if(child.first == "steps"){
            for (pt::ptree::value_type & stepData : child.second){

                auto  step = generateStep(stepData.second);
                generatedPage->addStep(step);
            }
            //Check page type to be used
        }

    }
    generatedPage->setName(name);
    generatedPage->setSpeedRate(speedRate);
    generatedPage->init();
//    get steps
    return generatedPage;

}

template<>
std::shared_ptr<ComplexKeyframePage> KeyframePageLoader<pt::ptree>::initializeComplexPage(pt::ptree &pageInfo){
//Generate a complex page
    auto  generatedPage = std::make_shared<ComplexKeyframePage>();
    for (pt::ptree::value_type & child : pageInfo) {
        if (child.first == "name") {
            generatedPage->setName(child.second.get_value<std::string>());
        } else if(child.first == "speedRate"){
            generatedPage->setSpeedRate(child.second.get_value<double>());
        }
    }
    return generatedPage;
}

template<>
void KeyframePageLoader<pt::ptree>::loadComplexPage(pt::ptree &pageInfo, std::shared_ptr<ComplexKeyframePage> complexPage,
                                                    std::map<std::string, std::shared_ptr<KeyframePage>> pages){
    for(pt::ptree::value_type & child : pageInfo){

        if(child.first == "pages"){
            for (pt::ptree::value_type & pageChild : child.second){
                std::string name;
                bool isInverted = false;
                double speedRate = 1;
                for(pt::ptree::value_type & pageData : pageChild.second){
                    if(pageData.first == "name"){
                        name = pageData.second.data();
                    } else if(pageData.first == "inverted"){
                        isInverted = pageData.second.get_value<bool>();
                    } else if(pageData.first == "speedRate"){
                        speedRate = pageData.second.get_value<double>();
                    }
                }
                complexPage->addPage(pages[name], isInverted, speedRate);
            }
        }
    }
    complexPage->init();
}

template<>
std::shared_ptr<KeyframeStep> KeyframePageLoader<pt::ptree>::generateStep(pt::ptree & stepInfo){

        auto step = std::make_shared<KeyframeStep>();
        auto & joints =  step->getJoints();
        for (pt::ptree::value_type &stepData : stepInfo){
            if(stepData.first == "neckYaw"){
                joints.neckYaw = stepData.second.get_value<double>();
            } else if(stepData.first == "neckPitch"){
                joints.neckPitch = stepData.second.get_value<double>();
            } else if(stepData.first == "leftShoulderPitch"){
                joints.leftShoulderPitch = stepData.second.get_value<double>();
            } else if(stepData.first == "leftShoulderYaw"){
                joints.leftShoulderYaw = stepData.second.get_value<double>();
            } else if(stepData.first == "leftArmRoll"){
                joints.leftArmRoll = stepData.second.get_value<double>();
            } else if(stepData.first == "leftArmYaw"){
                joints.leftArmYaw = stepData.second.get_value<double>();
            } else if(stepData.first == "leftHipYawpitch"){
                joints.leftHipYawPitch = stepData.second.get_value<double>();
            } else if(stepData.first == "leftHipRoll"){
                joints.leftHipRoll = stepData.second.get_value<double>();
            } else if(stepData.first == "leftHipPitch"){
                joints.leftHipPitch = stepData.second.get_value<double>();
            } else if(stepData.first == "leftKneePitch"){
                joints.leftKneePitch = stepData.second.get_value<double>();
            } else if(stepData.first == "leftFootPitch"){
                joints.leftFootPitch = stepData.second.get_value<double>();
            } else if(stepData.first == "leftFootRoll"){
                joints.leftFootRoll = stepData.second.get_value<double>();
            } else if(stepData.first == "rightShoulderPitch"){
                joints.rightShoulderPitch = stepData.second.get_value<double>();
            } else if(stepData.first == "rightShoulderYaw"){
                joints.rightShoulderYaw = stepData.second.get_value<double>();
            } else if(stepData.first == "rightArmRoll"){
                joints.rightArmRoll = stepData.second.get_value<double>();
            } else if(stepData.first == "rightArmYaw"){
                joints.rightArmYaw = stepData.second.get_value<double>();
            } else if(stepData.first == "rightHipYawpitch"){
                joints.rightHipYawPitch = stepData.second.get_value<double>();
            } else if(stepData.first == "rightHipRoll"){
                joints.rightHipRoll = stepData.second.get_value<double>();
            } else if(stepData.first == "rightHipPitch"){
                joints.rightHipPitch = stepData.second.get_value<double>();
            } else if(stepData.first == "rightKneePitch"){
                joints.rightKneePitch = stepData.second.get_value<double>();
            } else if(stepData.first == "rightFootPitch"){
                joints.rightFootPitch = stepData.second.get_value<double>();
            } else if(stepData.first == "rightFootRoll"){
                joints.rightFootRoll = stepData.second.get_value<double>();
            } else if(stepData.first == "delta"){
                step->getDuration() = stepData.second.get_value<double>();
            }

        }
    return step;
}



}
}
}
