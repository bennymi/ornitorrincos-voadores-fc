//
// Created by francisco on 19/12/16.
//

#include "StateMachineNodeCreator.h"
#include "WalkNodeCreator.h"
#include "KickNodeCreator.h"
#include "KeyframeNodeCreator.h"

namespace utils {
namespace configure_tree {
namespace control {
    template<>
    std::shared_ptr<Parameter<pt::ptree,std::string>> StateMachineNodeCreator::generateNode(){
        auto stateMachineNode = std::make_shared<BranchParameter<>>();
        auto walkNode = WalkNodeCreator().generateNode<pt::ptree, std::string>();
        auto kickNode = KickNodeCreator().generateNode<pt::ptree, std::string>();
        auto keyframeNode = KeyframeNodeCreator().generateNode<pt::ptree, std::string>();
        stateMachineNode->addChild("walk", walkNode);
        stateMachineNode->addChild("kick", kickNode);
        stateMachineNode->addChild("keyframe", keyframeNode);
        return stateMachineNode;
    };
}
}
}
