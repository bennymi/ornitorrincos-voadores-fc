//
// Created by francisco on 6/20/16.
//

#ifndef ITANDROIDS_SOCCER3D_CPP_BASEFULLAGENTTEST_H
#define ITANDROIDS_SOCCER3D_CPP_BASEFULLAGENTTEST_H

#include "BaseRoleAgentTest.h"

class BaseFullAgentTest : public BaseRoleAgentTest {
public:
    BaseFullAgentTest(std::string host,
                      int agentPort,
                      int monitorPort,
                      int agentNumber,
                      int robotType,
                      std::string teamName);

protected:
    decision_making::role::Role *role;

    virtual void setup();

    virtual void runStep();
};


#endif //ITANDROIDS_SOCCER3D_CPP_BASEFULLAGENTTEST_H
