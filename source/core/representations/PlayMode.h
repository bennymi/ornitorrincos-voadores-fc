/*
 * PlayMode.h
 *
 *  Created on: Aug 29, 2015
 *      Author: mmaximo
 */

#ifndef SOURCE_REPRESENTATIONS_PLAYMODE_H_
#define SOURCE_REPRESENTATIONS_PLAYMODE_H_

#include <iostream>
#include "representations/PlaySide.h"
#include "representations/RawPlayMode.h"

namespace representations {

    class PlayMode {
    public:
        /**
         * PlayMode after taking into account the side our team is playing
         * @todo add missing play modes
         */
        enum PLAY_MODE {
            /** The game has not started yet */
                    BEFORE_KICK_OFF,

            /** The ball is in the center, and our team is allowed to kick it first */
                    OWN_KICK_OFF,

            /** The ball is in the center, and the opponent team is allowed to kick it */
                    OPPONENT_KICK_OFF,

            /** The game is procesing normally, no special rules are in place */
                    PLAY_ON,

            /**
             * The ball left the playfield, our team is allowed to kick it back into the
             * field
             */
                    OWN_KICK_IN,

            /**
             * The ball left the playfield, the opponent team is allowed to kick it back
             * into the field
             */
                    OPPONENT_KICK_IN,

            /**
             * The ball left the playfield, our team is allowed to kick it back into the
             * field from a corner
             */
                    OWN_CORNER_KICK,

            /**
             * The ball left the playfield, the opponent team is allowed to kick it back
             * into the field from a corner
             */
                    OPPONENT_CORNER_KICK,

            /**
             * The ball left the playfield while it was near the own goal, and our team
             * is allowed to kick it first
             */
                    OWN_GOAL_KICK,

            /**
             * The ball left the playfield while it was near the opponent goal, and the
             * opponent team is allowed to kick it first
             */
                    OPPONENT_GOAL_KICK,

            /**
             * The opponent team violated the offside rule and our team is allowed to
             * kick the ball first
             */
                    OWN_OFFSIDE,

            /**
             * Our team violated the offside rule and the opponent team is allowed to
             * kick the ball first
             */
                    OPPONENT_OFFSIDE,

            /** The game is over */
                    GAME_OVER,

            /** A goal was counted for our team */
                    OWN_GOAL,

            /** A goal was counted for the opponent team */
                    OPPONENT_GOAL,

            /**
             * Our team got a free kick (probably because the referee decided to do so)
             * and may kick the ball first
             */
                    OWN_FREE_KICK,

            /**
             * The opponent right team got a free kick (probably because the referee
             * decided to do so) and may kick the ball first
             */
                    OPPONENT_FREE_KICK,

            /** No gameState or an unknown one (should not be used!) */
                    NONE,
        };

        static PlayMode::PLAY_MODE determinePlayMode(
                RawPlayMode::RAW_PLAY_MODE rawPlayMode, PlaySide::PLAY_SIDE playSide) {
            switch (rawPlayMode) {
                case RawPlayMode::BEFORE_KICK_OFF:
                    return BEFORE_KICK_OFF;

                case RawPlayMode::PLAY_ON:
                    return PLAY_ON;

                case RawPlayMode::GAME_OVER:
                    return GAME_OVER;

                case RawPlayMode::KICK_OFF_LEFT:
                    return checkSide(playSide, OWN_KICK_OFF, OPPONENT_KICK_OFF);

                case RawPlayMode::KICK_OFF_RIGHT:
                    return checkSide(playSide, OPPONENT_KICK_OFF, OWN_KICK_OFF);

                case RawPlayMode::KICK_IN_LEFT:
                    return checkSide(playSide, OWN_KICK_IN, OPPONENT_KICK_IN);

                case RawPlayMode::KICK_IN_RIGHT:
                    return checkSide(playSide, OPPONENT_KICK_IN, OWN_KICK_IN);

                case RawPlayMode::CORNER_KICK_LEFT:
                    return checkSide(playSide, OWN_CORNER_KICK, OPPONENT_CORNER_KICK);

                case RawPlayMode::CORNER_KICK_RIGHT:
                    return checkSide(playSide, OPPONENT_CORNER_KICK, OWN_CORNER_KICK);

                case RawPlayMode::FREE_KICK_LEFT:
                    return checkSide(playSide, OWN_FREE_KICK, OPPONENT_FREE_KICK);

                case RawPlayMode::FREE_KICK_RIGHT:
                    return checkSide(playSide, OPPONENT_FREE_KICK, OWN_FREE_KICK);

                case RawPlayMode::GOAL_KICK_LEFT:
                    return checkSide(playSide, OWN_GOAL_KICK, OPPONENT_GOAL_KICK);

                case RawPlayMode::GOAL_KICK_RIGHT:
                    return checkSide(playSide, OPPONENT_GOAL_KICK, OWN_GOAL_KICK);

                case RawPlayMode::OFFSIDE_LEFT:
                    return checkSide(playSide, OWN_OFFSIDE, OPPONENT_OFFSIDE);

                case RawPlayMode::OFFSIDE_RIGHT:
                    return checkSide(playSide, OPPONENT_OFFSIDE, OWN_OFFSIDE);

                case RawPlayMode::GOAL_LEFT:
                    return checkSide(playSide, OWN_GOAL, OPPONENT_GOAL);

                case RawPlayMode::GOAL_RIGHT:
                    return checkSide(playSide, OPPONENT_GOAL, OWN_GOAL);

                default:
                    return NONE;
            }
        }

    private:
        static PlayMode::PLAY_MODE checkSide(PlaySide::PLAY_SIDE playSide,
                                             PlayMode::PLAY_MODE leftState, PlayMode::PLAY_MODE rightState) {
            if (playSide == PlaySide::LEFT) {
                return leftState;
            } else {
                return rightState;
            }
        }

    };
} /* namespace representations */

#endif /* SOURCE_REPRESENTATIONS_PLAYMODE_H_ */
