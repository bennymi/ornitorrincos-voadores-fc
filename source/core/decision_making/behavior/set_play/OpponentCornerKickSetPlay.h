//
// Created by dicksiano on 8/14/16.
//

#ifndef ITANDROIDS_SOCCER3D_CPP_OPPONENTCORNERKICKSETPLAY_H
#define ITANDROIDS_SOCCER3D_CPP_OPPONENTCORNERKICKSETPLAY_H

#include "../SetPlay.h"

namespace decision_making {
    namespace behavior {

        class OpponentCornerKickSetPlay : public SetPlay {
        public:

            /**
            * Default constructor.
            */
            OpponentCornerKickSetPlay();

            /**
            * Assignment constructor.
            *
            * @param pointer to BehaviorFactory.
            */
            OpponentCornerKickSetPlay(BehaviorFactory *behaviorFactory);

            /**
            * Destructor.
            */
            virtual ~OpponentCornerKickSetPlay();

            /**
            * This function controls the actions.
            *
            * @param modeling.
            */
            void behave(modeling::Modeling &modeling);

        private:

        };

    } /* namespace behavior */
} /* namespace decision_making */




#endif //ITANDROIDS_SOCCER3D_CPP_OPPONENTCORNERKICKSETPLAY_H
