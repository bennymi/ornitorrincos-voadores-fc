//
// Created by francisco on 6/12/16.
//

#ifndef ITANDROIDS_SOCCER3D_CPP_BASEROLEAGENTTEST_H
#define ITANDROIDS_SOCCER3D_CPP_BASEROLEAGENTTEST_H

#include "BaseAgentTest.h"
#include "decision_making/RoleStrategyStub.h"
#include "decision_making/DecisionMaking.h"
#include "decision_making/RoleStub.h"


class BaseRoleAgentTest : public BaseAgentTest {
public:
    BaseRoleAgentTest(std::string host = "127.0.0.1", int agentPort = 3100, int monitorPort = 3200, int agentNumber = 1,
                      int robotType = 0,
                      std::string teamName = representations::ITAndroidsConstants::TEAM_DEFAULT_NAME);

    /**
     * This method is responsible for running the loop.
     */
    virtual void testLoop(int numberOfSteps) override;

    /**
     * This abstract method is to be implemented with one step
     * of the agent update.
     */
    virtual void runStep() = 0;

    /**
     * This abstract method is to be implemented with the initialization
     * logic for the test.
     */
    virtual void setup() = 0;

protected:
    std::unique_ptr<decision_making::DecisionMakingImpl> decisionMaking;
    std::shared_ptr<decision_making::RoleStrategy> roleStrategy;
    decision_making::role::RoleStub *role;
};


#endif //ITANDROIDS_SOCCER3D_CPP_BASEROLEAGENTTEST_H
