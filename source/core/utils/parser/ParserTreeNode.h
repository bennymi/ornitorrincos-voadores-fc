/*
 * ParserTreeNode.h
 *
 *  Created on: Aug 8, 2015
 *      Author: mmaximo
 */

#ifndef SOURCE_UTILS_PARSER_PARSERTREENODE_H_
#define SOURCE_UTILS_PARSER_PARSERTREENODE_H_

#include <string>
#include <vector>

namespace utils {
    namespace parser {

        struct ParserTreeNode {
            ParserTreeNode();

            virtual ~ParserTreeNode();

            std::string content;
            std::vector<ParserTreeNode *> children;
            int count;

            std::string toString();

            std::string toMonitorString();
        };

    } /* namespace parser */
} /* namespace utils */

#endif /* SOURCE_UTILS_PARSER_PARSERTREENODE_H_ */
