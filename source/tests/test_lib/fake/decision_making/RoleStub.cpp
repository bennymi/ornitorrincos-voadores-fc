//
// Created by muzio on 30/05/16.
//

#include "RoleStub.h"


namespace decision_making {
    namespace role {

        RoleStub::RoleStub() {

        }

        RoleStub::~RoleStub() {

        }

        void RoleStub::execute(modeling::Modeling &modeling) {

        }

    } /* namespace role */
} /* namespace decision_making */