//
// Created by dicksiano on 8/21/16.
//

#include "base/BaseAgentTest.h"

class OwnCornerKick_AgentTest : public BaseAgentTest {

public:

    virtual void testLoop(int nSteps) { BaseAgentTest::testLoop(nSteps); }


    virtual void setup() {
        wiz.setPlayMode(representations::RawPlayMode::CORNER_KICK_LEFT);
        representations::FlagType flag;
        wiz.setBallPosition(modeling.getWorldModel().getFieldDescription().getFlagKnownPosition(flag.F2L));
        wiz.setAgentPositionAndDirection(1, representations::PlaySide::LEFT, Vector3<double>(
                modeling.getWorldModel().getFieldDescription().getFlagKnownPosition(flag.F2L).x - 0.3,
                modeling.getWorldModel().getFieldDescription().getFlagKnownPosition(flag.F2L).y - 0.3, 0.3), 0.0);
    }

    virtual void runStep() {
        wiz.setPlayMode(representations::RawPlayMode::CORNER_KICK_LEFT);

        utils::roboviz::Roboviz roboviz1;
        std::string name1("animated");
        std::string buff1("animated");
        roboviz1.swapBuffers(&buff1);


        roboviz1.drawLine(-3.0, 0.0, 0, -3.0, 0.0, 1, 1, 20, 20, 20, &name1);
        roboviz1.drawLine(3.0, 0.0, 0, 3.0, 0.0, 1, 1, 20, 20, 20, &name1);

        Pose2D selfPosition = modeling.getWorldModel().getSelf().getPosition();
        roboviz1.drawLine(selfPosition.translation.x, selfPosition.translation.y, 0,
                          selfPosition.translation.x, selfPosition.translation.y, 1,
                          1, 20, 20, 20, &name1);

        if (!decisionMaking.decideFall(modeling)) {
            behaviorFactory.getSetPlay().behave(modeling); // Behavior!
            decisionMaking.movementRequest = behaviorFactory.getSetPlay().getMovementRequest();
            decisionMaking.beamRequest = behaviorFactory.getSetPlay().getBeamRequest();

            behaviorFactory.getActiveVision().behave(modeling); // Vision!
            decisionMaking.lookRequest = behaviorFactory.getActiveVision().getLookRequest();
        }
    }
};

int main() {

    /**
     * Start the external agent
     */
    std::vector<std::thread> threads;
    for (int i = 2; i < 3; i++) {
        std::stringstream stream;
        stream << "./agent 127.0.0.1 3100 ITAndroids " << i + 7 << " &";
        system(stream.str().c_str());
        stream.str("");
        stream << "sleep 5";
        system(stream.str().c_str());
    }

    OwnCornerKick_AgentTest agent; // Agent test
    agent.testLoop(50000); // Run the test!
    return 0;

}