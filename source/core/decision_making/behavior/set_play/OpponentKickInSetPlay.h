//
// Created by dicksiano on 8/14/16.
//

#ifndef ITANDROIDS_SOCCER3D_CPP_OPPONENTKICKINSETPLAY_H
#define ITANDROIDS_SOCCER3D_CPP_OPPONENTKICKINSETPLAY_H

#include "../SetPlay.h"

namespace decision_making {
    namespace behavior {

        class OpponentKickInSetPlay : public SetPlay {
        public:

            /**
            * Default constructor.
            */
            OpponentKickInSetPlay();

            /**
            * Assignment constructor.
            *
            * @param pointer to BehaviorFactory.
            */
            OpponentKickInSetPlay(BehaviorFactory *behaviorFactory);

            /**
            * Destructor.
            */
            virtual ~OpponentKickInSetPlay();

            /**
            * This function controls the actions.
            *
            * @param modeling.
            */
            void behave(modeling::Modeling &modeling);

        private:

        };

    } /* namespace behavior */
} /* namespace decision_making */



#endif //ITANDROIDS_SOCCER3D_CPP_OPPONENTKICKINSETPLAY_H
