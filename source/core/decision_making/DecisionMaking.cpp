/*
 * DecisionMaking.cpp
 *
 *  Created on: Aug 7, 2015
 *      Author: mmaximo
 */

#include "decision_making/DecisionMaking.h"


namespace decision_making {

    DecisionMaking::DecisionMaking() {

    }

    DecisionMaking::~DecisionMaking() {
    }

    DecisionMakingImpl::DecisionMakingImpl() :
            roleStrategy(std::make_shared<RoleStrategyImpl>()) {
    }

    DecisionMakingImpl::DecisionMakingImpl(std::shared_ptr<RoleStrategy> roleStrategy) {
        this->roleStrategy = roleStrategy;
    }

    DecisionMakingImpl::~DecisionMakingImpl() {

    }

    void DecisionMakingImpl::decide(modeling::Modeling &modeling) {
        // Check if the robot has fallen
        if (decideFall(modeling))
            return;

        // Select and assign roles to each teammate
        if (modeling.getWorldModel().getTeammates().size() == 10) {
            markingSystem.selectRolesAndAssign(modeling);
        }

        auto &role = *roleStrategy->getRole(modeling);

        // Execute behaviors according to the role
        role.execute(modeling);

        // Update the requests
        movementRequest = role.getMovementRequest();
        lookRequest = role.getLookRequest();
        sayRequest = role.getSayMessage();
        beamRequest = role.getBeamRequest();
    }

    bool DecisionMakingImpl::decideFall(modeling::Modeling &modeling) {
        /*
         * Check if the agent is fallen according to accelerations threshold.
         * If the agent is fallen, the function activates the keyframe that wakes
         * the agent.
         */
        if (modeling.getAgentModel().hasFallen()) {
            lookRequest = std::make_shared<control::LookRequest>(0, 0);
            if (modeling.getAgentModel().hasFallenFront()) {
                movementRequest = std::make_shared<control::KeyframeRequest>(
                        control::keyframe::KeyframeType::STAND_UP_FROM_FRONT, false);
            } else if (modeling.getAgentModel().hasFallenBack()) {
                movementRequest = std::make_shared<control::KeyframeRequest>
                        (control::keyframe::KeyframeType::STAND_UP_FROM_BACK, false);
            } else if (modeling.getAgentModel().hasFallenSide()) {
                movementRequest = std::make_shared<control::KeyframeRequest>
                        (control::keyframe::KeyframeType::MOVE_ARM_TO_FALL_BACK);
            }
            return true;
        }
        return false;
    }

    std::shared_ptr<std::string> DecisionMakingImpl::getSayMessage() {
        return sayRequest;
    }

    std::shared_ptr<control::LookRequest> DecisionMakingImpl::getLookRequest() {
        return lookRequest;
    }

    std::shared_ptr<control::MovementRequest> DecisionMakingImpl::getMovementRequest() {
        return movementRequest;
    }

    std::shared_ptr<itandroids_lib::math::Pose2D> DecisionMakingImpl::getBeamRequest() {
        return beamRequest;
    }


} /* namespace decision_making */
