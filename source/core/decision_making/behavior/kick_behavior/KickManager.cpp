//
// Created by francisco on 6/16/16.
//

#include "KickManager.h"
#include "ShortDistance5mKick.h"
#include "LongDistance20mKick.h"

namespace decision_making {
    namespace behavior {
        namespace kick_behavior {


            KickManager::KickManager() {
                kicks.push_back(std::make_shared<LongDistanceKick>());
                kicks.push_back(std::make_shared<ShortDistanceKick>());
                kicks.push_back(std::make_shared<ShortDistance5mKick>());
                kicks.push_back(std::make_shared<LongDistance20mKick>());
            }

            std::shared_ptr<Kick> KickManager::getKick(
                    double distance, double direction) {
                selectedKick = selectKick(distance, direction);
                selectedKick->computeParameters();
                return selectedKick;
            }

            std::shared_ptr<Kick> KickManager::selectKick(double distance, double direction) {
//                if(distance > 13 ){
//                    return kicks[3];
//                } else
                if (distance > 5) {
                    return kicks[0];
                } else {
//                if(distance > 5){
                    return kicks[1];
                }
//                else {
//                    return kicks[2];
//                }

            }


        }
    }
}


