/*
 * FocusBall.cpp
 *
 *  Created on: Nov 1, 2015
 *      Author: mmaximo
 */

#include "FocusBall.h"

#include "BehaviorFactory.h"

namespace decision_making {
    namespace behavior {

        const double FocusBall::SCAN_TIME = 2.0;
        const double FocusBall::FOCUSING_TIME = 2.0;
        const double FocusBall::PAN_AMPLITUDE = 60.0 * M_PI / 180.0;
        const double FocusBall::TILT_AMPLITUDE = 30.0 * M_PI / 180.0;

        const double FocusBall::LOOK_AROUND_DELAY = 8;
        const double FocusBall::TIME_TO_LOOK_AROUND = 2;

        FocusBall::FocusBall() : Behavior(nullptr), state(SCANNING), lastStateSwitchTime(0.0), timeToTurnHead(),
                                 first(true), lookRequest(nullptr) {
        }

        FocusBall::FocusBall(BehaviorFactory *factory) : Behavior(factory), state(SCANNING), lastStateSwitchTime(0.0),
                                                         timeToTurnHead(0.0), first(true), lookRequest(nullptr) {

        }

        FocusBall::~FocusBall() {
        }

        void FocusBall::behave(modeling::Modeling &modeling) {
            if (lookRequest) {
                lookRequest = nullptr;
            }

            if (first) {
                lastStateSwitchTime = modeling.getAgentModel().getAgentPerception().getGlobalTime();
                timeToTurnHead = modeling.getAgentModel().getAgentPerception().getGlobalTime();
                first = false;
            }

            double globalTime = modeling.getAgentModel().getAgentPerception().getGlobalTime();

            if (isBetterScan(modeling)) {
                if (state == FOCUSING_BALL)
                    lastStateSwitchTime = globalTime;

                state = SCANNING;
            } else {
                if (state == SCANNING)
                    lastStateSwitchTime = globalTime;

                state = FOCUSING_BALL;
            }
            if (state == SCANNING) {

                double timeDelta = modeling.getAgentModel().getAgentPerception().getGlobalTime() - timeToTurnHead;
                double time = globalTime - lastStateSwitchTime;

                turnHead(modeling, time);

                if (timeDelta > LOOK_AROUND_DELAY + TIME_TO_LOOK_AROUND)
                    timeToTurnHead = globalTime;

                if (time > SCAN_TIME)
                    lastStateSwitchTime = globalTime;

            } else {

                focusOnTheBall(modeling);
            }

        }

        std::shared_ptr<control::LookRequest> FocusBall::getLookRequest() {
            return lookRequest;
        }

        void FocusBall::turnHead(modeling::Modeling &modeling, double time) {
            double pan = PAN_AMPLITUDE * sin(2.0 * M_PI * time / SCAN_TIME + M_PI / 2.0);
            double tilt = TILT_AMPLITUDE * sin(4.0 * M_PI * time / SCAN_TIME);
            lookRequest = std::make_shared<control::LookRequest>(pan, tilt);
        }

        void FocusBall::focusOnTheBall(modeling::Modeling &modeling) {
            Pose2D ball = modeling.getWorldModel().getBall().getPosition();
            Pose2D robot = modeling.getWorldModel().getSelf().getPosition();
            Vector2<double> robotToBall = Vector2<double>(ball.translation.x - robot.translation.x,
                                                          ball.translation.y - robot.translation.y);
            double ballAngle = atan2(ball.translation.y - robot.translation.y,
                                     ball.translation.x - robot.translation.x);
            double height = -fabs(modeling.getAgentModel().getFromCameraToRootTransform().translation.z);
            double pan = ballAngle - modeling.getWorldModel().getSelf().getPosition().rotation;
            double tilt = atan2(height, robotToBall.abs());

            lookRequest = std::make_shared<control::LookRequest>(pan, tilt);
        }

        bool FocusBall::isBetterScan(modeling::Modeling &modeling) {

            int numberOfVisibleObjects = modeling.getWorldModel().getVisibleObjectsCounter();
            Pose2D ballPosition = modeling.getWorldModel().getBall().getPosition();
            Pose2D robotPosition = modeling.getWorldModel().getSelf().getPosition();
            Vector2<double> robotToBall = Vector2<double>(ballPosition.translation.x - robotPosition.translation.x,
                                                          ballPosition.translation.y - robotPosition.translation.y);

            double globalTime = modeling.getAgentModel().getAgentPerception().getGlobalTime();
            double timeDelta = modeling.getAgentModel().getAgentPerception().getGlobalTime() - timeToTurnHead;

            bool isTimeToTurnHead = (timeDelta - (LOOK_AROUND_DELAY + TIME_TO_LOOK_AROUND)) <= TIME_TO_LOOK_AROUND
                                    || timeDelta > (LOOK_AROUND_DELAY + TIME_TO_LOOK_AROUND);
            //std::cout << "is time to turn " << isTimeToTurnHead << std::endl;
            //std::cout << "time delta " << timeDelta << std::endl;
            //std::cout << "get age " << modeling.getWorldModel().getBall().getAge(globalTime) << std::endl;
            return (modeling.getWorldModel().getBall().getAge(globalTime) > 1
                    || (robotToBall.abs() > 1.5 && isTimeToTurnHead));
        }
    } /* namespace behavior */
} /* namespace decision_making */
