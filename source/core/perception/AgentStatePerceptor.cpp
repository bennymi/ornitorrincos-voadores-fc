/*
 * AgentStatePerceptor.cpp
 *
 *  Created on: Aug 8, 2015
 *      Author: mmaximo
 */

#include "perception/AgentStatePerceptor.h"

#include "utils/parser/KeyValuePair.h"

namespace perception {

    /*
     * This constructor sets to zero temperature and battery.
     */
    AgentStatePerceptor::AgentStatePerceptor() {
        temperature = 0;
        battery = 0;
    }

    /*
     * This constructor gets information of temperature and battery
     * from parser.
     */
    AgentStatePerceptor::AgentStatePerceptor(utils::parser::ParserTreeNode &node) :
            Perceptor(node) {
        using namespace utils::parser;
        temperature = atof(
                KeyValuePair(node.children[0]->content, ' ').value.c_str());
        battery = atof(KeyValuePair(node.children[1]->content, ' ').value.c_str());
    }

    AgentStatePerceptor::~AgentStatePerceptor() {
    }

    /*
     * Getter methods: Return data of agent state (temperature and battery)
     */
    double AgentStatePerceptor::getBattery() {
        return battery;
    }

    double AgentStatePerceptor::getTemperature() {
        return temperature;
    }

} /* namespace perception */
