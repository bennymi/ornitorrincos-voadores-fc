/*
 * BallTracker.h
 *
 *  Created on: Aug 29, 2015
 *      Author: mmaximo
 */

#ifndef SOURCE_MODELING_STATE_ESTIMATION_BALLTRACKER_H_
#define SOURCE_MODELING_STATE_ESTIMATION_BALLTRACKER_H_

#include "math/Vector3.h"
#include "math/Pose2D.h"
#include "perception/VisibleBall.h"
#include "signal_processing/LinearKalmanFilter.h"

namespace modeling {
    namespace state_estimation {

        using namespace itandroids_lib::math;
        using namespace itandroids_lib::signal_processing;
        using namespace representations;

        class BallTracker {
        public:
            BallTracker();

            virtual ~BallTracker();

            Pose2D getPosition();

            Pose2D getVelocity();

            void update(double globalTime, const Pose2D &odometry, perception::VisibleBall *ballPosition,
                        Pose2D &agentEstimate);

            void update(double globalTime, const Pose2D &odometry);

            void resetEstimate();

            bool hasLostBall();

        private:
            LinearDiscreteStochasticModel stochasticModel;
            const int N_OF_CICLES;
            LinearKalmanFilter KF;
            int counterNotViewBall;
            bool lostBall;

        };


    } /* namespace state_estimation */
} /* namespace modeling */

#endif /* SOURCE_MODELING_STATE_ESTIMATION_BALLTRACKER_H_ */
