/*
 * DelaunayFormationFactory.cpp
 *
 *  Created on: Oct 28, 2015
 *      Author: itandroids
 */

#include "DelaunayFormationFactory.h"

namespace decision_making {
    namespace positioning {
        namespace delaunay_triangulation {

            DelaunayFormationFactory::DelaunayFormationFactory(std::string folderPath) {
                triangulations = std::vector<std::shared_ptr<DelaunayInterpolator>>(
                        DelaunayFormationType::NUM_FORMATIONS);
                boost::filesystem::path dirPath(folderPath);
                boost::filesystem::directory_iterator end_iter;

                // Iterate through all .conf files.
                // Each .conf file represents a specific formation.
                for (boost::filesystem::directory_iterator iter(dirPath); iter != end_iter;
                     ++iter) {
                    if (iter->path().extension().string() == ".conf") {
                        positions.clear();
                        DelaunayFileParser parser(iter->path().string());
                        std::unique_ptr<itandroids_lib::geometry::DelaunayTriangulation> triangulation =
                                std::make_unique<itandroids_lib::geometry::DelaunayTriangulation>();

                        positions.push_back(parser.getPositions());
                        triangulation->addVertices(positions.back());
                        triangulation->compute();
                        std::shared_ptr<DelaunayInterpolator> interpolator = std::make_shared<DelaunayInterpolator>(
                                std::move(triangulation), parser.getPredefinedPositions());
                        triangulations[DelaunayFormationType::stringToType(
                                iter->path().stem().string())] = interpolator;


                    }
                }
            }

            DelaunayFormationFactory::~DelaunayFormationFactory() {
            }

            std::vector<std::shared_ptr<DelaunayInterpolator>> DelaunayFormationFactory::getTriangulations() {
                return triangulations;
            }

        } /* namespace delaunay_triangulation */
    } /* namespace positioning */
} /* namespace decision_making */
