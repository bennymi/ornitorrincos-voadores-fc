/*
 * VisibleGoalPost.cpp
 *
 *  Created on: Sep 1, 2015
 *      Author: itandroids
 */

#include "VisibleGoalPost.h"

namespace perception {

    /*
     * This constructor to case of no data
     */
    VisibleGoalPost::VisibleGoalPost() {
        goalPostType = representations::GoalPostType::G1L;
    }

    /**
     * This constructor takes a parser tree node and manipulates it to get information
     * of goal post identification.
     */
    VisibleGoalPost::VisibleGoalPost(utils::parser::ParserTreeNode &node) :
            VisibleObject(node) {
        std::string compareString = node.content;
        boost::trim(compareString);
        goalPostType = representations::GoalPostType::identifyType(compareString);
//	if (compareString.compare("G1L")== 0){
//		goalPostType = perception::GoalPostType::LEFT_FIRST_POST;
//	} else if(compareString.compare("G1R")== 0) {
//		goalPostType = perception::GoalPostType::RIGHT_FIRST_POST;
//	} else if(compareString.compare("G2L")== 0) {
//		goalPostType = perception::GoalPostType::LEFT_SECOND_POST;
//	} else if(compareString.compare("G2R")== 0) {
//		goalPostType = perception::GoalPostType::RIGHT_SECOND_POST;
//	}
    }

    VisibleGoalPost::~VisibleGoalPost() {
    }

    /*
     * Getter method: returns the goal post type.
     */
    representations::GoalPostType::GOAL_POST_TYPE &VisibleGoalPost::getGoalPostType() {
        return goalPostType;
    }

} /* namespace perception */
