#!/bin/bash

# instalando dependencias externas
pushd $HOME
sudo apt-get update
sudo apt-get install libaudio-dev
sudo apt-get install libfontconfig1-dev
sudo apt-get install libxrender-dev
sudo apt-get install libice-dev
sudo apt-get install libxi-dev
sudo apt-get install libxt-de
sudo apt-get install build-essential libtool
