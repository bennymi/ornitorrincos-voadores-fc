/*
 * VisibleBall.cpp
 *
 *  Created on: Sep 1, 2015
 *      Author: itandroids
 */

#include "VisibleBall.h"

namespace perception {

    VisibleBall::VisibleBall() {

    }

    VisibleBall::VisibleBall(utils::parser::ParserTreeNode &node) :
            VisibleObject(node) {
    }

    VisibleBall::~VisibleBall() {
    }

} /* namespace perception */
