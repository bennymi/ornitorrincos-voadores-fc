/*
 * MovementRequestType.h
 *
 *  Created on: Oct 25, 2015
 *      Author: itandroids
 */

#ifndef SOURCE_CONTROL_WALKING_MOVEMENTREQUESTTYPE_H_
#define SOURCE_CONTROL_WALKING_MOVEMENTREQUESTTYPE_H_


class MovementRequestType {
public:
    enum MOVEMENT_REQUEST_TYPE {
        WALK_REQUEST,
        KICK_REQUEST,
        KEYFRAME_REQUEST,
        UNKNOWN,
    };
};


#endif /* SOURCE_CONTROL_WALKING_MOVEMENTREQUESTTYPE_H_ */
