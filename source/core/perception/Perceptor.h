/*
 * Perceptor.h
 *
 *  Created on: Aug 8, 2015
 *      Author: mmaximo
 */

#ifndef SOURCE_PERCEPTION_PERCEPTOR_H_
#define SOURCE_PERCEPTION_PERCEPTOR_H_

#include <iostream>
#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>
#include "utils/parser/ParserTreeNode.h"
#include "math/Vector3.h"
#include "representations/HearData.h"
#include "representations/ForceResistanceData.h"

namespace perception {

    using namespace itandroids_lib::math;

    enum BODY_PARTS {
        TORSO,
        NECK,
        HEAD,
        SHOULDER,
        UPPERARM,
        ELBOW,
        LOWERARM,
        HIP1,
        HIP2,
        THIGH,
        SHANK,
        ANKLE,
        FOOT,
    };

    enum HINGE_JOINTS {
        NECK_YAW,
        NECK_PITCH,
        LEFT_SHOULDER_PITCH,
        LEFT_SHOULDER_YAW,
        LEFT_ARM_ROLL,
        LEFT_ARM_YAW,
        LEFT_HIP_YAWPITCH,
        LEFT_HIP_ROLL,
        LEFT_HIP_PITCH,
        LEFT_KNEE_PITCH,
        LEFT_FOOT_PITCH,
        LEFT_FOOT_ROLL,
        RIGHT_SHOULDER_PITCH,
        RIGHT_SHOULDER_YAW,
        RIGHT_ARM_ROLL,
        RIGHT_ARM_YAW,
        RIGHT_HIP_YAWPITCH,
        RIGHT_HIP_ROLL,
        RIGHT_HIP_PITCH,
        RIGHT_KNEE_PITCH,
        RIGHT_FOOT_PITCH,
        RIGHT_FOOT_ROLL,
        UNKNOWN_JOINT,
    };

    enum PERCEPTOR_TYPE {
        ACCELEROMETER,
        AGENT_STATE,
        FORCE_RESISTANCE,
        GAME_STATE,
        GLOBAL_TIME,
        GYRO_RATE,
        HEAR,
        VISION,
        HINGE_JOINT,
        TOUCH,
        UNIVERSAL_JOINT,
        UNKNOWN,
    };

    class Perceptor {
    public:
        Perceptor();

        /**
         * Constructor for Perceptor.
         *
         * @param node part of the parser tree that represents
         *             an perceptor.
         */
        Perceptor(utils::parser::ParserTreeNode &);

        /**
         * Destructor
         */
        virtual ~Perceptor();

        /**
         * Get the perceptor's name
         *
         * @return name
         */
        std::string getName();

        /**
         * Get the body part
         *
         * @param string with the body part's name
         * @return bodyPart
         */
        perception::BODY_PARTS getBodyPart(std::string);

        /**
         * Get the hinge joint
         *
         * @param string with the hinge joint's name
         * @return hingeJoint
         */
        perception::HINGE_JOINTS getHingeJoint(std::string);

        /**
         * Get the perceptor type
         *
         * @param string with the perceptor type's name
         * @return perceptorType
         */
        perception::PERCEPTOR_TYPE getPerceptorType();

    protected:
        perception::PERCEPTOR_TYPE perceptorType;
        std::string name;
    };

} /* namespace perception */

#endif /* SOURCE_PERCEPTION_PERCEPTOR_H_ */
