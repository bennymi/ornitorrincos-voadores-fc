#ifndef SOURCE_PERCEPTION_TOUCHPERCEPTOR_H_
#define SOURCE_PERCEPTION_TOUCHPERCEPTOR_H_

#include "perception/Perceptor.h"
#include "utils/parser/KeyValuePair.h"
#include "utils/parser/ParserTreeNode.h"
#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>

namespace perception {

    class TouchPerceptor : public Perceptor {
    public:

        /**
         * Assignment constructor
         *
         */
        TouchPerceptor(utils::parser::ParserTreeNode &node);

        /**
         * destructor
         *
         */
        virtual ~TouchPerceptor();

        /**
         * Get nameTouchPerceptor
         *
         * @return nameTouchPerceptor
         */
        std::string getName();

        /**
         * Get stateTouchPerceptor value
         *
         * @return value
         */
        bool isTouching();

    private:
        /**
         * Perceptor's name.
         *
         * The perceptor always reports its own unique name. This allows the use of more than one TouchPerceptor per agent.
         */
        std::string name;
        /**
         * state is a boolean that is 1 if a collision is detected. If no collision is detected, state is 0.
         *
         */
        bool state;

    };

} /* namespace perception */

#endif /* SOURCE_PERCEPTION_TOUCHPERCEPTOR_H_ */
