/*
 * HearData.cpp
 *
 *  Created on: Aug 8, 2015
 *      Author: mmaximo
 */

#include <boost/algorithm/string.hpp>
#include <core/modeling/WorldModel.h>

namespace representations {

    HearData::HearData() {
        team = "unknown";
        time = 0;
        cycles = 0;
        heardServerTime = 0;
        fromSelf = true;
        fFallen = false;
        direction = 0;
        message = "";
        ballX = 0;
        ballY = 0;
        ballLastSeenServerTime = 0;
        agentX = 0;
        agentY = 0;
    }

    HearData::HearData(std::string team, double heardServerTime, bool fromSelf, double direction,
                       std::string message) {
        this->roleAssignment.clear();
        this->bits.clear();
        this->team = team;
        this->heardServerTime = heardServerTime;
        this->fromSelf = fromSelf;
        this->direction = direction;
        this->message = message;
        this->opponentsPos.clear();
        //time = 0;
    }

    void HearData::decode(std::string ourTeamName) {


        /*
         * This clause is necessary to avoid decode others team's messages.
         */
        if (team == ourTeamName) {
            /* the message */
            stringToBits();

            uNum = bitsToInt(bits, 0, 3);

            if (uNum == 1) //goalie decoding
                bitsToDataGoalie();
            else bitsToData();

        }
    }

    HearData::~HearData() {
    }

    double HearData::getHeardServerTime() {
        return heardServerTime;
    }

    std::string HearData::getTeam() {
        return team;
    }

    double HearData::getTime() {
        return heardServerTime;
    }

    bool HearData::isFromSelf() {
        return fromSelf;
    }

    double HearData::getDirection() {
        return direction;
    }

    std::string HearData::getMessage() {
        return message;
    }

    std::vector<int> HearData::getRoleAssigment() {
        return roleAssignment;
    }

    int HearData::getUniformNumber() {
        return uNum;
    }

    double HearData::getHeardBallXPosition() {
        return ballX;
    }

    double HearData::getHeardBallYPosition() {
        return ballY;
    }

    bool HearData::isFallen() {
        return fFallen;
    }

    double HearData::getHeardAgentXPosition() {
        return agentX;
    }

    double HearData::getHeardAgentYPosition() {
        return agentY;
    }

    double HearData::getCycle() {
        return cycles;
    }

    bool HearData::hasStarted() {
        if (heardServerTime < 300) { //first half
            if (heardServerTime > 3.0)
                return true;
            else return false;
        } else { //second half
            if (heardServerTime > 303.0)
                return true;
            else return false;
        }

    }

    void HearData::stringToBits() {
        const std::string commAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
        bits.resize(message.length() * 6);

        for (unsigned long i = 0; i < message.length(); i++) {

            const char c = message.at(i);
            unsigned long n = commAlphabet.find(c);
            // If n equals string::npos, the vector needs to be cleared for the next message
            if (n == std::string::npos) {
                bits.clear();
            }

            // Converts into bits the ASCII number for each char
            for (int j = 5; j >= 0; j--) {
                bits[i * 6 + j] = (int) (n % 2);
                n /= 2;
            }
        }
    }

    void HearData::bitsToDataGoalie() {
        if (bits.size() < (4 + 18 + 1 + 22 + 22 + 22 + 22)) {
            time = 0;
            //ballLastSeenTime = 0;
            ballX = 0;
            ballY = 0;
            agentX = 0;
            agentY = 0;
            fFallen = false;
            roleAssignment.clear();
            opponentsPos.clear();
        }


        int ctr = 0;

        // It converts the message into data for each element
        // Then it uses ctr to represent the inicial position for each data in bits
        uNum = bitsToInt(bits, ctr, ctr + 3);
        ctr += 4;

        //agent X
        int axInteger = signedBitsToInt(bits, ctr, ctr + 4);
        ctr += 5;
        int axDecimal = bitsToInt(bits, ctr, ctr + 3);
        ctr += 4;
        if (axInteger < 0)
            axDecimal = -axDecimal;
        agentX = axInteger + 0.1 * axDecimal;

        //agent Y
        int ayInteger = signedBitsToInt(bits, ctr, ctr + 4);
        ctr += 5;
        int ayDecimal = bitsToInt(bits, ctr, ctr + 3);
        ctr += 4;
        if (ayInteger < 0)
            ayDecimal = -ayDecimal;
        agentY = ayInteger + 0.1 * ayDecimal;

        //agent is fallen?
        int isFallen = bitsToInt(bits, ctr, ctr);
        if (isFallen)
            fFallen = true;
        else fFallen = false;

        ctr += 1;

        //opponent positions
        for (int i = 0; i < 4; i++) {
            int oppUnifNUmber = bitsToInt(bits, ctr, ctr + 3);
            ctr += 4;

            int oppXInteger = signedBitsToInt(bits, ctr, ctr + 4);
            ctr += 5;
            int oppXDecimal = bitsToInt(bits, ctr, ctr + 3);
            ctr += 4;
            if (oppXInteger < 0)
                oppXDecimal = -oppXDecimal;
            double oppX = oppXInteger + 0.1 * oppXDecimal;

            int oppYInteger = signedBitsToInt(bits, ctr, ctr + 4);
            ctr += 5;
            int oppYDecimal = bitsToInt(bits, ctr, ctr + 3);
            ctr += 4;
            if (oppYInteger < 0)
                oppYDecimal = -oppYDecimal;
            double oppY = oppYInteger + 0.1 * oppYDecimal;
            OtherPlayer opponent;
            opponent.setId(oppUnifNUmber);
            opponent.setPosition(Pose2D(oppX, oppY));
            opponent.setRelevant(true);
            opponent.setIsFallen(false);
            opponent.setLastHeardTime(getTime());
            opponentsPos.push_back(opponent);
        }


    }

    void HearData::bitsToData() {


        if (bits.size() < (4 + 9 + 9 + 9 + 9 + 1 + 55)) {
            time = 0;
            //ballLastSeenTime = 0;
            ballX = 0;
            ballY = 0;
            agentX = 0;
            agentY = 0;
            fFallen = false;
            roleAssignment.clear();
        }


        int ctr = 0;

        // It converts the message into data for each element
        // Then it uses ctr to represent the inicial position for each data in bits
        uNum = bitsToInt(bits, ctr, ctr + 3);
        ctr += 4;

        int axInteger = signedBitsToInt(bits, ctr, ctr + 4);
        ctr += 5;
        int axDecimal = bitsToInt(bits, ctr, ctr + 3);
        ctr += 4;
        if (axInteger < 0)
            axDecimal = -axDecimal;
        agentX = axInteger + 0.1 * axDecimal;

        int ayInteger = signedBitsToInt(bits, ctr, ctr + 4);
        ctr += 5;
        int ayDecimal = bitsToInt(bits, ctr, ctr + 3);
        ctr += 4;
        if (ayInteger < 0)
            ayDecimal = -ayDecimal;
        agentY = ayInteger + 0.1 * ayDecimal;

        int isFallen = bitsToInt(bits, ctr, ctr);
        if (isFallen == 1)
            fFallen = true;
        else fFallen = false;

        ctr += 1;


        int bxInteger = signedBitsToInt(bits, ctr, ctr + 4);
        ctr += 5;
        int bxDecimal = bitsToInt(bits, ctr, ctr + 3);
        ctr += 4;
        if (bxInteger < 0)
            bxDecimal = -bxDecimal;
        ballX = bxInteger + 0.1 * bxDecimal;

        int byInteger = signedBitsToInt(bits, ctr, ctr + 4);
        ctr += 5;
        int byDecimal = bitsToInt(bits, ctr, ctr + 3);
        ctr += 4;
        if (byInteger < 0)
            byDecimal = -byDecimal;
        ballY = byInteger + 0.1 * byDecimal;


        for (int i = 0; i < 11; i++) {
            roleAssignment.push_back(bitsToInt(bits, ctr, ctr + 4));
            ctr += 5;
        }

    }

    int HearData::bitsToInt(std::vector<int> &bits, const int &start, const int &end) {
        if (start < 0 || end >= (int) bits.size()) {
            return 0;//Error.
        }

        // Mathematical conversion from base 2 to base 10
        int n = 0;
        for (int i = start; i <= end; i++) {
            n *= 2;
            n += bits[i];
        }

        return n;
    }

    int HearData::signedBitsToInt(std::vector<int> &bits, const int &start, const int &end) {
        if (start < 0 || end >= (int) bits.size()) {
            return 0;//Error.
        }

        // Mathematical conversion from base 2 to base 10
        int n = 0;

        for (int i = start + 1; i <= end; i++) {
            n *= 2;
            n += bits[i];
        }

        if (bits[start] == 1)
            n = -n;

        return n;
    }

    std::vector<OtherPlayer> HearData::getOpponentPositions() {
        return opponentsPos;
    };

} /* namespace representations */
