//
// Created by francisco on 07/05/16.
//

#ifndef ITANDROIDS_SOCCER3D_CPP_ACTIVEVISIONFILEPARSER_H
#define ITANDROIDS_SOCCER3D_CPP_ACTIVEVISIONFILEPARSER_H

#include <iostream>
#include <vector>
#include "boost/multi_array.hpp"
#include <cmath>
#include <math/Pose2D.h>
#include "math/MathUtils.h"

namespace decision_making {
    namespace behavior {
        namespace active_vision {
            class ActiveVisionFileParser {
            public:
                ActiveVisionFileParser();

                ~ActiveVisionFileParser();

                void parse(std::string filepath);

                double getCostValue(const itandroids_lib::math::Pose2D &robotPose, double pan, double tilt);

                const std::vector<std::vector<std::vector<std::vector<double>>>> &getData();

                static const double INITIAL_X, INITIAL_Y, INITIAL_PAN, INITIAL_TILT;
                static const double FINAL_X, FINAL_Y, FINAL_PAN, FINAL_TILT;
                static const double DX, DY, DPAN, DTILT;
            private:
                std::shared_ptr<std::vector<std::vector<std::vector<std::vector<double>>>>> data;
                int dimX, dimY, dimTheta, dimPsi;

            };
        }
    }
}


#endif //ITANDROIDS_SOCCER3D_CPP_ACTIVEVISIONFILEPARSER_H
