//
// Created by francisco on 6/16/16.
//

#ifndef ITANDROIDS_SOCCER3D_CPP_KICK_H
#define ITANDROIDS_SOCCER3D_CPP_KICK_H

#include "decision_making/behavior/Behavior.h"
#include "KickParameters.h"

using ::decision_making::behavior::Behavior;
namespace decision_making {
    namespace behavior {
        namespace kick_behavior {
            class Kick : public Behavior {
            public:
                virtual ~Kick();

                virtual KickParameters &getKickParameters();

                virtual void setKickParameters(const KickParameters &parameters);

                virtual void computeParameters();

                virtual double getAccuracy();

                virtual void behave(modeling::Modeling &modeling) = 0;

            private:
                KickParameters parameters;
            };
        }
    }
}


#endif //ITANDROIDS_SOCCER3D_CPP_KICK_H
