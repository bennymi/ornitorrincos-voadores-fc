//
// Created by francisco on 5/21/16.
//

//
// Created by francisco on 05/02/16.
//

#include "base/BaseAgentTest.h"

class PotentialFieldNavigation_AgentTest : public BaseAgentTest {
private:

public:
    virtual void testLoop(int nSteps) { BaseAgentTest::testLoop(nSteps); }

    virtual void setup() {
        wiz.setAgentPositionAndDirection(1, representations::PlaySide::LEFT, Vector3<double>(-1, 0, 0.35), 0);
    }

    virtual void runStep() {
        std::string buff("animated");
        std::string name("animated");
        behaviorFactory.getPotentialFieldNavigationBehavior().setTarget(
                modeling.getWorldModel().getBall().getPosition().translation);
        behaviorFactory.getPotentialFieldNavigationBehavior().behave(modeling);
        const auto &direction = behaviorFactory.getPotentialFieldNavigationBehavior().getFieldDirection();

        decisionMaking.movementRequest = behaviorFactory.getPotentialFieldNavigationBehavior().getMovementRequest();

//        itandroids_lib::math::Pose2D pose (0,direction.x/(direction.abs()*3), direction.y/(direction.abs()*3));
//        decisionMaking.movementRequest = std::make_shared<control::WalkRequest>(pose);
        roboviz.drawLine(modeling.getWorldModel().getSelf().getPosition().translation.x,
                         modeling.getWorldModel().getSelf().getPosition().translation.y,
                         0,
                         modeling.getWorldModel().getSelf().getPosition().translation.x + direction.x / direction.abs(),
                         modeling.getWorldModel().getSelf().getPosition().translation.y + direction.y / direction.abs(),
                         0, 1,
                         255, 255, 255, &name);

        for (auto teammate : modeling.getWorldModel().getTeammates()) {
            roboviz.drawLine(teammate->getPosition().translation.x,
                             teammate->getPosition().translation.y,
                             0, teammate->getPosition().translation.x,
                             teammate->getPosition().translation.y, 0.5, 3.0,
                             0.0, 0.0, 1.0, &name);
        }

        for (auto opponent : modeling.getWorldModel().getOpponents()) {
            roboviz.drawLine(opponent->getPosition().translation.x,
                             opponent->getPosition().translation.y,
                             0, opponent->getPosition().translation.x,
                             opponent->getPosition().translation.y, 0.5, 3.0,
                             1.0, 0.0, 0.0, &name);
        }

        const double X_STEP = 0.2;
        const double Y_STEP = 0.2;
        for (double x = -modeling::FieldDescription::FIELD_LENGTH / 2;
             x <= modeling::FieldDescription::FIELD_LENGTH / 2; x += X_STEP) {
            for (double y = -modeling::FieldDescription::FIELD_WIDTH / 2;
                 y <= modeling::FieldDescription::FIELD_WIDTH / 2; y += Y_STEP) {
                Vector2<double> selfPosition(x, y);
                if (modeling.getWorldModel().getSelf().getPosition().translation.distance(selfPosition) < 3.0) {
                    auto direction = behaviorFactory.getPotentialFieldNavigationBehavior().calculateFieldDirection(
                            selfPosition, modeling);
                    direction.normalize(0.2);
                    roboviz.drawLine(x, y, 0.1, x + direction.x,
                                     y + direction.y, 0.1, 1.0, 1.0, 1.0, 0.0, &name);
                }
//                roboviz.drawCicle(x+direction.x,y+direction.y,0.01,0.01,1,0,0,&n1);
            }
        }
        roboviz.swapBuffers(&buff);
        //head movement
    }
};

int main() {
    PotentialFieldNavigation_AgentTest t;
    t.testLoop(50000);
}
