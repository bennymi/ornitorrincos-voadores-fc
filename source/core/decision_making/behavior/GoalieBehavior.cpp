//
// Created by dicksiano on 24/05/16.
//

#include "GoalieBehavior.h"
#include "BehaviorFactory.h"

namespace decision_making {
    namespace behavior {

        using namespace itandroids_lib;

        GoalieBehavior::GoalieBehavior() : Behavior(nullptr) {
        }

        GoalieBehavior::GoalieBehavior(BehaviorFactory *behaviorFactory) : Behavior(behaviorFactory) {
        }

        GoalieBehavior::~GoalieBehavior() {
        }

        void GoalieBehavior::behave(modeling::Modeling &modeling) {
            movementRequest = nullptr;

            changeStates(modeling); // Set the current state according to the game situation
            perform(modeling); // Perform an action according to the state

        } /* function behave */

        /**
         * This function calculate the goalie's position according to the ball's position.
         * The goalie must go to the position where the ball would to break into the goal.
         */
        Pose2D GoalieBehavior::calculatePosition(modeling::Modeling &modeling) {

            itandroids_lib::math::Vector3<double> ownGoalPosition =
                    modeling.getWorldModel().getOwnGoalPosition();

            itandroids_lib::math::Vector2<double> goalWorldPosition(ownGoalPosition.x,
                                                                    ownGoalPosition.y);

            double distance = modeling::FieldDescription::GOAL_AREA_WIDTH / 2;

            itandroids_lib::math::Pose2D &ballPosition =
                    modeling.getWorldModel().getBall().getPosition();

            itandroids_lib::math::LinearInterpolator<> xInterpolator(goalWorldPosition.x,
                                                                     ballPosition.translation.x);

            itandroids_lib::math::LinearInterpolator<> yInterpolator(goalWorldPosition.y,
                                                                     ballPosition.translation.y);

            itandroids_lib::math::Vector2<double> result(
                    xInterpolator.interpolate(
                            distance
                            / (goalWorldPosition - ballPosition.translation).abs()),

                    yInterpolator.interpolate(
                            distance
                            / (goalWorldPosition - ballPosition.translation).abs()));

            double orientation = result.angle();
            double fieldHalfLenght = modeling::FieldDescription::FIELD_WIDTH / 2;

            itandroids_lib::math::Pose2D strategicPosition(ownGoalPosition.x + X_OFFSET_FROM_GOAL, result.y);

            return strategicPosition;
        } /* function calculate position */

        void GoalieBehavior::calculateParameters(modeling::Modeling &modeling) {
            Pose2D &selfPosition =
                    modeling.getWorldModel().getSelf().getPosition(); // Getting self position.
            Pose2D &ballPosition =
                    modeling.getWorldModel().getBall().getPosition(); // Getting ball position.
            Vector3<double> ownGoalPosition =
                    modeling.getWorldModel().getOwnGoalPosition(); // Getting own goal position.
            Pose2D closestOpponentFromBall =
                    modeling.getWorldModel().getOpponentClosestFromBall().getPosition(); // Getting closest opponent from ball position.

            ballDistanceFromGoal = (ballPosition - ownGoalPosition).abs();
            ballDistanceFromSelf = (ballPosition - selfPosition).abs();
            selfDistanceFromGoal = (selfPosition - ownGoalPosition).abs();
            ballDistanceFromClosestOpponent = (closestOpponentFromBall - ballPosition).abs();
            ballVelocity = modeling.getWorldModel().getBall().getVelocity().abs();
            closestOpponentFromBallDistance = (closestOpponentFromBall - ownGoalPosition).abs();
        } /* function calculate parameters */

        bool GoalieBehavior::shouldGoToBall(modeling::Modeling &modeling) {
            Pose2D &selfPosition =
                    modeling.getWorldModel().getSelf().getPosition();


            //If the ball is behind us, doesn't make sense to follow it (Counter goal)
            if (selfPosition.translation.x > modeling.getWorldModel().getBall().getPosition().translation.x)
                return false;

            //Zone 1 is danger zone, the ball must be out of here immediately
            if (ballDistanceFromGoal < DISTANCE_ZONE_1 && ballVelocity < SAFE_BALL_VELOCITY)
                return true;

                /*Intermediate zone:
                 * The agent will ignore the ball by itself in this zone, but if there's
                 * a opponent with ball, the goalie "charges" into it*/
            else if (ballDistanceFromGoal < DISTANCE_ZONE_2) {
                if (ballDistanceFromClosestOpponent < 0.7)
                    return true;

                else
                    return false;
            } else
                return false;

            /*  if((ballDistanceFromGoal <= 0 || ballDistanceFromGoal > 100.00) || (ballDistanceFromClosestOpponent > 100.00 || ballDistanceFromClosestOpponent <= 0))
                  return false;



              return ((ballDistanceFromGoal < 2.2 && ballVelocity < SAFE_VELOCITY_TO_KICK) ||
                          (ballDistanceFromGoal < 2.6 && ballVelocity < SAFE_VELOCITY_TO_KICK &&
                           ballDistanceFromClosestOpponent < 1.2)); */
        } /* function should go to ball */


        /**
         * This function evaluate if the goalie should dive according to the game situation.
         */
        bool GoalieBehavior::evaluateDive(modeling::Modeling &modeling) {
            Pose2D selfPosition = modeling.getWorldModel().getSelf().getPosition();
            Pose2D ballCurrentPosition = modeling.getWorldModel().getBall().getPosition(); // ball's current position
            Pose2D ballFuturePosition = ballCurrentPosition +
                                        modeling.getWorldModel().getBall().getVelocity() *
                                        0.02; // ball's future position
            /* This  line represents the ball's trajectory, considering the current position and the ball's
             position after 0.02 seconds (a cycle) */
            itandroids_lib::geometry::Line2D ballTrajectoryLine(Vector2<double>(ballCurrentPosition.translation.x,
                                                                                ballCurrentPosition.translation.y),
                                                                Vector2<double>(ballFuturePosition.translation.x,
                                                                                ballFuturePosition.translation.y));

            /* In that case, the ball is behind us. Doesn't make sense to dive */
            if (selfPosition.translation.x > ballCurrentPosition.translation.x)
                return false;

            /* In that case, the ball is too slow. Doesn't make sense to dive */
            if (ballVelocity < SAFE_BALL_VELOCITY)
                return false;

            /* In that case, the ball trajectory won't end up inside the goal
               First of all, is important to check if the ball's velocity is relevant. */
            if (ballVelocity > SAFE_BALL_VELOCITY &&
                abs(ballTrajectoryLine.getYGivenX(-0.5 * modeling::FieldDescription::FIELD_LENGTH))
                > 0.9 * modeling::FieldDescription::GOAL_AREA_WIDTH)
                return false;

            /* So, the goalie is behind the ball. Now, is important to check if the ball will end up inside
               the goal within a safe time */
            double estimatedTimeInterval = ballDistanceFromGoal /
                                           ballVelocity; // ball velocity > SAFE_BALL_VELOCITY, prevents from division by zero.

            return ((estimatedTimeInterval < SAFE_TIME) &&   // Check if the ball is close to came up to the goal.
                    (abs(ballTrajectoryLine.getYGivenX(-0.5 * modeling::FieldDescription::FIELD_LENGTH))
                     <= 0.5 *
                        modeling::FieldDescription::GOAL_AREA_WIDTH)); // Check if the ball will end up inside the goal.

        } /* function evaluate dive */

        bool GoalieBehavior::evaluateShortKick(modeling::Modeling &modeling) {

            if (closestOpponentFromBallDistance < SAFE_BALL_DISTANCE_TO_KICK)
                return false;

            return ((ballDistanceFromSelf < SAFE_BALL_DISTANCE_TO_KICK) &&
                    (ballVelocity < SAFE_VELOCITY_TO_KICK));

        } /* function evaluate short kick */

        bool GoalieBehavior::evaluateLongKick(modeling::Modeling &modeling) {

            if (closestOpponentFromBallDistance < SAFE_BALL_DISTANCE_TO_KICK)
                return false;

            if (modeling.getWorldModel().getPlayMode() == representations::PlayMode::OWN_GOAL_KICK)
                return true;

            return false;

        } /* function evaluate long kick */

        /**
         * This function changes the goalie's state according to the game situation.
         */
        void GoalieBehavior::changeStates(modeling::Modeling &modeling) {

            calculateParameters(modeling); // Calculate current parameters;
            switch (state) {
                case GoalieBehavior::GOALIE_STATES::DIVE:

                    if (evaluateDive(modeling))
                        state = DIVE;

                    else if (evaluateShortKick(modeling)) {
                        state = SHORT_DISTANCE_KICK;
                    } else if (evaluateLongKick(modeling)) {
                        state = LONG_DISTANCE_KICK;
                    } else
                        state = POSITIONING_TO_DEFEND;

                    break;

                case GoalieBehavior::GOALIE_STATES::SHORT_DISTANCE_KICK:

                    if (evaluateDive(modeling))
                        state = DIVE;

                    else if (evaluateShortKick(modeling)) {
                        state = SHORT_DISTANCE_KICK;
                    } else if (evaluateLongKick(modeling)) {
                        state = LONG_DISTANCE_KICK;
                    } else
                        state = POSITIONING_TO_DEFEND;

                    break;

                case GoalieBehavior::GOALIE_STATES::LONG_DISTANCE_KICK:

                    if (evaluateDive(modeling))
                        state = DIVE;

                    else if (evaluateShortKick(modeling)) {
                        state = SHORT_DISTANCE_KICK;
                    } else if (evaluateLongKick(modeling)) {
                        state = LONG_DISTANCE_KICK;
                    } else
                        state = POSITIONING_TO_DEFEND;

                    break;

                case GoalieBehavior::GOALIE_STATES::GOING_TO_BALL_POSITION:

                    if (shouldGoToBall(modeling))
                        state = GOING_TO_BALL_POSITION;

                    else if (evaluateDive(modeling))
                        state = DIVE;

                    else if (evaluateShortKick(modeling))
                        state = SHORT_DISTANCE_KICK;

                    else if (evaluateLongKick(modeling))
                        state = LONG_DISTANCE_KICK;

                    else if (shouldGoToBall(modeling))
                        state = GOING_TO_BALL_POSITION;

                    else if (evaluateDive(modeling))
                        state = DIVE;

                    else
                        state = POSITIONING_TO_DEFEND;

                    break;

                case GoalieBehavior::GOALIE_STATES::POSITIONING_TO_DEFEND:
                    if (evaluateDive(modeling))
                        state = DIVE;

                    else if (shouldGoToBall(modeling))
                        state = GOING_TO_BALL_POSITION;

                    else
                        state = POSITIONING_TO_DEFEND;

                    break;

                default:

                    state = GoalieBehavior::GOALIE_STATES::POSITIONING_TO_DEFEND;
            }

        }/* function change states */

        /**
         * This function executes the actions according to the current state.
         */
        void GoalieBehavior::perform(modeling::Modeling &modeling) {
            itandroids_lib::math::Pose2D strategicPosition;

            switch (state) {
                case GoalieBehavior::GOALIE_STATES::DIVE:
                    behaviorFactory->getKeep().behave(modeling);
                    movementRequest = behaviorFactory->getKeep().getMovementRequest();
                    break;

                case GoalieBehavior::GOALIE_STATES::SHORT_DISTANCE_KICK:
                    behaviorFactory->getKickBehavior().setTarget(Vector2<double>(-10.0, 0.0));
                    behaviorFactory->getKickBehavior().behave(modeling);
                    movementRequest = behaviorFactory->getKickBehavior().getMovementRequest();
                    break;

                case GoalieBehavior::GOALIE_STATES::LONG_DISTANCE_KICK:
                    behaviorFactory->getKickBehavior().setTarget(Vector2<double>(0.0, 0.0));
                    behaviorFactory->getKickBehavior().behave(modeling);
                    movementRequest = behaviorFactory->getKickBehavior().getMovementRequest();
                    break;

                case GoalieBehavior::GOALIE_STATES::GOING_TO_BALL_POSITION:
                    strategicPosition = modeling.getWorldModel().getBall().getPosition();
                    behaviorFactory->getNavigatePosition().setTarget(strategicPosition);
                    behaviorFactory->getNavigatePosition().behave(modeling);
                    movementRequest = behaviorFactory->getNavigatePosition().getMovementRequest();
                    break;

                case GoalieBehavior::GOALIE_STATES::POSITIONING_TO_DEFEND:
                    strategicPosition = calculatePosition(modeling);
                    behaviorFactory->getNavigatePosition().setTarget(strategicPosition);
                    behaviorFactory->getNavigatePosition().behave(modeling);
                    movementRequest = behaviorFactory->getNavigatePosition().getMovementRequest();
                    break;
            }

        }/* function perform */
    } /* namespace behavior */
} /* namespace decision_making */