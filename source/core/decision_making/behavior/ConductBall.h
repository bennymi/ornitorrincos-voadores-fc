/*
 * ConductBall.h
 *
 *  Created on: Oct 31, 2015
 *      Author: itandroids
 */

#ifndef SOURCE_DECISION_MAKING_BEHAVIOR_CONDUCTBALL_H_
#define SOURCE_DECISION_MAKING_BEHAVIOR_CONDUCTBALL_H_

#include "Behavior.h"

#include "modeling/Modeling.h"
#include "representations/OtherPlayer.h"

using namespace representations;
namespace decision_making {
    namespace behavior {
        class BehaviorFactory;

        class ConductBall : public Behavior {
        public:

            /**
             * Default constructor.
             */
            ConductBall();

            /**
             * Assignment constructor.
             *
             * @param behaviorFactory pointer to BehaviorFactory object.
             */
            ConductBall(BehaviorFactory *behaviorFactory);

            /**
             * Destructor.
             */
            virtual ~ConductBall();

            /**
             * Function to conduct the ball.
             *
             * @param modeling.
             */
            virtual void behave(modeling::Modeling &modeling);

        private:

        };

    } /* namespace behavior */
} /* namespace decision_making */

#endif /* SOURCE_DECISION_MAKING_BEHAVIOR_CONDUCTBALL_H_ */
