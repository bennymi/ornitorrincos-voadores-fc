//
// Created by francisco on 6/22/16.
//

#include "ShortDistance5mKick.h"

namespace decision_making {
    namespace behavior {
        namespace kick_behavior {
            ShortDistance5mKick::ShortDistance5mKick() : KeyframeKick("kick5m") {
                setKickParameters(
                        KickParameters(Vector2<double>(-0.17, -0.04), 0));
            }
        }
    }
}