//
// Created by francisco on 18/12/16.
//

#ifndef ITANDROIDS_SOCCER3D_CPP_KEYFRAMENODECREATOR_H
#define ITANDROIDS_SOCCER3D_CPP_KEYFRAMENODECREATOR_H

#include "utils/configure_tree/NodeCreator.h"
#include <boost/property_tree/ptree.hpp>
#include <boost/filesystem.hpp>
#include "representations/ITAndroidsConstants.h"
#include "boost/filesystem/operations.hpp" // includes boost/filesystem/path.hpp
#include "utils/patterns/LeafParameter.h"
#include "utils/patterns/BranchParameter.h"

using namespace boost::filesystem;

using ::itandroids_lib::utils::patterns::BranchParameter;
using ::itandroids_lib::utils::patterns::LeafParameter;
namespace pt = boost::property_tree;

namespace utils{
namespace configure_tree{
namespace control{

    class KeyframeNodeCreator : NodeCreator<KeyframeNodeCreator>{
    public:
        KeyframeNodeCreator(std::string folderPath = representations::ITAndroidsConstants::KEYFRAME_FOLDER);
        template<class Content, class Key>
        std::shared_ptr<Parameter<Content,Key>> generateNode(){
            auto branchNode = std::make_shared<BranchParameter<Content,Key>>();
            path dirPath(folderPath);
            boost::filesystem::recursive_directory_iterator end_iter;
            for (boost::filesystem::recursive_directory_iterator iter(dirPath); iter != end_iter;
                 ++iter) {
                //Opening files
                if (iter->path().extension().string() == ".json") {
                    auto leafNode = std::make_shared<LeafParameter<Content,Key>>();
                    loadParameterContent(leafNode, iter->path().string());
                    branchNode->addChild(iter->path().string(), leafNode);
                }
            }
            return branchNode;
        }

    private:
        template <class Content, class Key>
        void loadParameterContent(std::shared_ptr<LeafParameter<Content,Key>> param, std::string filePath){}
        std::string folderPath;

    };
    template<>
    void KeyframeNodeCreator::loadParameterContent<pt::ptree, std::string>(std::shared_ptr<LeafParameter<pt::ptree,std::string>> param, std::string filePath);
}
}
}



#endif //ITANDROIDS_SOCCER3D_CPP_KEYFRAMENODECREATOR_H
