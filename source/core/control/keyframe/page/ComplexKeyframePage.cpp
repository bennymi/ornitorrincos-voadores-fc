//
// Created by francisco on 6/1/16.
//

#include <utils/LogSystem.h>
#include "ComplexKeyframePage.h"

namespace control {
    namespace keyframe {


        ComplexKeyframePage::ComplexKeyframePage() {
            currentPageNumber = 0;
        }

        void ComplexKeyframePage::addPage(std::shared_ptr<KeyframePage> page, bool isInverted, double speedRate) {
            pages.push_back(page);
            invertionValues.push_back(isInverted);
            speedRates.push_back(speedRate);
            //auto & logger = itandroids_lib::utils::LogSystem::getInstance().getLogger("pages");

            //logger.log("page") << getName() << " " << page->getName();
        }

        std::vector<double> ComplexKeyframePage::interpolate() {
            return std::vector<double>();
        }

        void ComplexKeyframePage::createInterpolators() {

        }

        representations::NaoJoints &ComplexKeyframePage::getDesiredJoints(representations::NaoJoints &perceivedJoints,
                                                                          double globalTime, bool isInverted) {

            if (hasFinished() && hasStarted) {
                hasStarted = false;
            } else if (!hasStarted) {
                reset();
                currentPageNumber = 0;
                hasStarted = true;
                desiredJoints = representations::NaoJoints();
            } else {
                if (pages[currentPageNumber]->hasFinished()) {
                    currentPageNumber++;
                    if (currentPageNumber < pages.size()) {
                        pages[currentPageNumber]->reset();
                    }
                }
                if (currentPageNumber < pages.size()) {
                    desiredJoints = pages[currentPageNumber]->getDesiredJoints(perceivedJoints, globalTime*speedRate*speedRates[currentPageNumber], isInverted ^
                                                                                                            invertionValues[currentPageNumber]);
                }

            }
            return desiredJoints;
        }

        void ComplexKeyframePage::init() {
            movementTime = 0;
            pageTimes.clear();
            for (auto &k : pages) {
                movementTime += k->getMovementTime() / k->getSpeedRate();
                pageTimes.push_back(movementTime);
            }

        }

        void ComplexKeyframePage::reset() {
            currentTime = 0;
            currentPageNumber = 0;
            for (auto &k : pages) {
                k->reset();
            }
            hasStarted = false;
        }

        double ComplexKeyframePage::getMovementTime() {
            return movementTime;
        }

        bool ComplexKeyframePage::hasFinished() {
            return (currentPageNumber >= pages.size());
        }


    }
}


