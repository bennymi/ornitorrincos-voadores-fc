//
// Created by dicksiano on 21/06/16.
//

#include "DeadBall_AgentTest.h"

DeadBall_AgentTest::DeadBall_AgentTest(std::string host, int agentPort, int monitorPort, int agentNumber)
        : BaseAgentTest(host, agentPort, monitorPort, agentNumber) {
}

int main(int argc, char *argv[]) {
    std::string host = "127.0.0.1";
    int agentPort = 3100;
    int monitorPort = 3200;
    int agentNumber = 1;
    if (argc > 1) {
        host = argv[1];
    }
    if (argc > 2) {
        agentPort = std::stoi(argv[2]);
    }
    if (argc > 3) {
        monitorPort = std::stoi(argv[3]);
    }
    if (argc > 4) {
        agentNumber = std::stoi(argv[4]);
    }
    DeadBall_AgentTest agent(host, agentPort, monitorPort, agentNumber);
    agent.testLoop(9000);
    return 0;
}