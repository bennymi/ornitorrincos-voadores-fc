/*
 * FlagType.h
 *
 *  Created on: Sep 6, 2015
 *      Author: mmaximo
 */

#ifndef SOURCE_REPRESENTATIONS_FLAGTYPE_H_
#define SOURCE_REPRESENTATIONS_FLAGTYPE_H_

#include <string>

namespace representations {

    class FlagType {
    public:
        enum FLAG_TYPE {
            F1L, F2L, F1R, F2R, INVALID
        };

        static const int NUM_FLAG_TYPES = 4;

        static FLAG_TYPE identifyType(std::string flagName);

        static std::string getFlagName(FLAG_TYPE type);
    };

} /* namespace representations */

#endif /* SOURCE_REPRESENTATIONS_FLAGTYPE_H_ */
