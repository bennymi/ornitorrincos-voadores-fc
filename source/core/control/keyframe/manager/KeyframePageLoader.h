//
// Created by francisco on 17/12/16.
//

#ifndef ITANDROIDS_SOCCER3D_CPP_KEYFRAMEPAGEGENERATOR_H
#define ITANDROIDS_SOCCER3D_CPP_KEYFRAMEPAGEGENERATOR_H

#include <boost/property_tree/ptree.hpp>
#include <memory>
#include "control/keyframe/page/KeyframePage.h"
#include "control/keyframe/page/ComplexKeyframePage.h"
#include "control/keyframe/page/KeyframeStep.h"

namespace pt = boost::property_tree;
using ::control::keyframe::KeyframePage;
using ::control::keyframe::KeyframeStep;
namespace control {
namespace keyframe {
namespace manager {
    template <typename T>
    class KeyframePageLoader {
    public:
        /**
         * This function is used for generating a KeyframePage, in order to be used in the KeyframePlayer
         * @param pageInfo
         */
        std::shared_ptr<KeyframePage> initializeAndLoadSimplePage(T & pageInfo);
        /**
         * This functions initialize a complex page, without generating it's dependencies
         * @param pageInfo the content related to the page
         * @return
         */
        std::shared_ptr<ComplexKeyframePage> initializeComplexPage(T & pageInfo);
        /**
         * This functions loads a Complex page, adding it's dependencies;
         */
        void loadComplexPage(T & pageInfo, std::shared_ptr<ComplexKeyframePage> complexPage, std::map<std::string, std::shared_ptr<KeyframePage>> pages);

    private:
        std::shared_ptr<KeyframeStep> generateStep(T & stepInfo);
    };

    /**
     * Function for generating a simple page using a property_tree
     * @param pageInfo
     */
    template<>
    std::shared_ptr<KeyframePage> KeyframePageLoader<pt::ptree>::initializeAndLoadSimplePage(pt::ptree &pageInfo);
    /**
     * Initialize complex pages, so that they can be dependent on one another
     * @param pageInfo
     * @return a pointer to a complexPage
     */
    template<>
    std::shared_ptr<ComplexKeyframePage> KeyframePageLoader<pt::ptree>::initializeComplexPage(pt::ptree &pageInfo);

    /**
     *
     * @param pageInfo property tree related to the Keyframe
     * @param complexPage page related to the property tree
     * @param pages All the pages of the KeyframePageManager
     */
    template<>
    void KeyframePageLoader<pt::ptree>::loadComplexPage(pt::ptree &pageInfo, std::shared_ptr<ComplexKeyframePage> complexPage,
                                                        std::map<std::string, std::shared_ptr<KeyframePage>> pages);


    /**
     * Function for generating a step of a keyframe movement from a property tree
     * @param stepInfo
     * @return a ponter to the generatedStep
     */
    template<>
    std::shared_ptr<KeyframeStep> KeyframePageLoader<pt::ptree>::generateStep(pt::ptree & stepInfo);


}
}
}

#endif //ITANDROIDS_SOCCER3D_CPP_KEYFRAMEPAGEGENERATOR_H
