//
// Created by dicksiano on 27/05/16.
//

#ifndef ITANDROIDS_SOCCER3D_CPP_DEFENSIVEROLE_H
#define ITANDROIDS_SOCCER3D_CPP_DEFENSIVEROLE_H

#include "modeling/Modeling.h"
#include "Role.h"

namespace decision_making {
    namespace role {

/**
 * Class that represents a defensive role.
 */
        class DefensiveRole : public Role {
        public:

            /**
             * Default constructor for the Defensive Role
             */
            DefensiveRole();

            /**
             * Default destructor for the Defensive Role
             */
            virtual ~DefensiveRole();

        private:
            void behaveMovement(modeling::Modeling &modeling);
        };

    } /* namespace role */
} /* namespace decision_making */



#endif //ITANDROIDS_SOCCER3D_CPP_DEFENSIVEROLE_H
