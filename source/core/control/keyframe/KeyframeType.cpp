/*
 * KeyframeMotionPageType.cpp
 *
 *  Created on: Oct 13, 2015
 *      Author: itandroids
 */

#include "KeyframeType.h"

namespace control {
    namespace keyframe {

        /**
         * General Keyframes
         */


        const std::string KeyframeType::TEST = std::string("test");
        const std::string KeyframeType::INIT = std::string("getReady");

        /**
         * Stand Up Keyframes
         */

        const std::string KeyframeType::STAND_UP_FROM_BACK = std::string("standUpFromBack");
        const std::string KeyframeType::STAND_UP_FROM_FRONT = std::string("standUpFromFront");

        const std::string KeyframeType::MOVE_ARM_TO_FALL_BACK = std::string("moveArmToFallBack");

        /**
         * Kicks in the form of keyframe
         */

        const std::string KeyframeType::KICK_RIGHT_LEG = std::string("kickRightLeg");
        const std::string KeyframeType::KICK_LEFT_LEG = std::string("kickLeftLeg");

        /**
         * Goalie keyframes
         *
         */
        const std::string KeyframeType::CATCH_RIGHT = std::string("catchRight");
        const std::string KeyframeType::CATCH_LEFT = std::string("catchLeft");

        /**
         * Test Keyframes
         */
        const std::string KeyframeType::FALL_BACK = std::string("fallBack");
        const std::string KeyframeType::FALL_FRONT = std::string("fallFront");
        const std::string KeyframeType::FALL_RIGHT_SIDE = std::string("fallRightSide");

    } /* namespace keyframe */
} /* namespace control */
