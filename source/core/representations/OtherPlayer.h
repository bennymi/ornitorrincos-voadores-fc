/*
 * OtherPlayer.h
 *
 *  Created on: Sep 8, 2015
 *      Author: mmaximo
 */

#ifndef SOURCE_REPRESENTATIONS_OTHERPLAYER_H_
#define SOURCE_REPRESENTATIONS_OTHERPLAYER_H_

#include "math/Pose2D.h"

namespace representations {
    using namespace itandroids_lib::math;

    class OtherPlayer {
    public:
        OtherPlayer();

        OtherPlayer(Pose2D position, Pose2D velocity);

        virtual ~OtherPlayer();

        bool isRelevant() const;

        void setRelevant(bool relevant);

        Pose2D getPosition() const;

        Pose2D getLocalPosition() const;

        Pose2D getVelocity() const;

        Pose2D getLocalVelocity() const;

        int getId() const;

        void setPosition(Pose2D position);

        void setLocalPosition(Pose2D position);

        void setVelocity(Pose2D velocity);

        void setLocalVelocity(Pose2D velocity);

        void setId(int uniformNumber);

        void setLastSeenTime(double globalTime);

        double getLastSeenTime();

        double getLastHeardTime();

        double setLastHeardTime(double lastHeardTime);

        void setIsFallen(bool isFallen);

        double getAge(double currentTime);

        bool isFallen();

        bool isGoalie();

    private:
        bool hasRelevance;
        bool hasFallen;
        Pose2D position;
        Pose2D localPosition;
        Pose2D velocity;
        Pose2D localVelocity;
        int uniformNumber;
        double lastSeenTime;
        double lastHeardTime;
    };

} /* namespace representations */

#endif /* SOURCE_REPRESENTATIONS_OTHERPLAYER_H_ */
