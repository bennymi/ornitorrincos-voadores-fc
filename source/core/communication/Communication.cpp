/*
 * Communication.cpp
 *
 *  Created on: Aug 7, 2015
 *      Author: mmaximo
 */

#include "communication/Communication.h"

namespace communication {

    Communication::Communication() : serverSocket(nullptr) {
    }

    Communication::Communication(ServerSocket *serverSocket) :
            serverSocket(serverSocket) {
    }

    Communication::~Communication() {
        if (serverSocket)
            delete serverSocket;
    }

    void Communication::establishConnection() {
        serverSocket->establishConnection(); // Establish the connection

    }

    void Communication::closeConnection() {
        serverSocket->closeConnection(); // Close the connection
    }

    void Communication::receiveMessage() {
        currentMessage = serverSocket->receiveMessage(); // Receive a message from server socket
    }

    void Communication::sendMessage(std::string message) {
        if (connectionClosed()) return;
        serverSocket->sendMessage(message); // Sends a message to server socket
    }

    std::string &Communication::getCurrentMessage() {
        return currentMessage;
    }

} /* namespace communication */
