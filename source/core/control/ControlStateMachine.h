//
// Created by francisco on 26/11/16.
//

#ifndef ITANDROIDS_SOCCER3D_CPP_CONTROLSTATEMACHINE_H
#define ITANDROIDS_SOCCER3D_CPP_CONTROLSTATEMACHINE_H

#include <boost/statechart/event.hpp>
#include <boost/statechart/state_machine.hpp>
#include <boost/statechart/simple_state.hpp>
#include <boost/statechart/state.hpp>
#include <boost/statechart/transition.hpp>
#include <boost/statechart/custom_reaction.hpp>
#include <ctime>
#include <iostream>
#include <boost/mpl/list.hpp>

namespace mpl = boost::mpl;

#include "control/kick/KickZMP.h"
#include "control/walking/OmnidirectionalWalkZMP.h"
#include "MovementRequest.h"
#include "WalkRequest.h"
#include "control/keyframe/KeyframePlayer.h"
#include "utils/patterns/Configurable.h"

using itandroids_lib::utils::patterns::Configurable;
using itandroids_lib::utils::patterns::Parameter;
namespace perception {
    class Perception;
}

namespace representations {
    class ITAndroidsConstants;

    struct NaoJoints;

    class RobotType;
}


namespace control {

    // forward declare classes

    class MovementSwitchSmoother;


    class JointsController;


    class KeyframeRequest;

    class KickRequest;

    class WalkRequest;


    namespace sc = boost::statechart;


    class IControlSystem {
    public:
//        virtual ~IControlSystem() = 0;
        virtual void control(perception::Perception &perception) = 0;

        virtual representations::NaoJoints getJointsPositions();

        // @todo put a restart method over here
    };

    class IdleState;


    class ControlStateMachine
            : public sc::state_machine<ControlStateMachine, IdleState>,
              public IControlSystem,
              public Configurable<ControlStateMachine> {
    public:
        /**
         * Constructor that receives a robot type, for keyframe selection and different walk parameters
         */
        ControlStateMachine(RobotType::ROBOT_TYPE robotType);

        void control(perception::Perception &perception);

        void smooth(NaoJoints &currentPositions, NaoJoints &finalPositions);

        bool smoothing;
        MovementSwitchSmoother smoother;
        walking::OmnidirectionalWalkZMPParams walkParams;
        perception::Perception *perception;
        kick::KickZMPParams kickParams;
        walking::OmnidirectionalWalkZMP *walk;
        kick::KickZMP *kickZmp;
        control::keyframe::KeyframePlayer keyframePlayer;
        double previousGlobalTime;
        WalkMode previousWalkMode;
        representations::NaoJoints jointsPositions;
        representations::NaoJoints previousJointsPositions;
        std::shared_ptr<MovementRequest> movementRequest;

        void setMovementRequest(std::shared_ptr<MovementRequest> movementRequest);

        /**
         * Template function for configurable data
         * It sets the parameters for walk, kick, and keyframe data
         */
        template<class Content, class Key>
        void setParameter(Parameter<Content, Key> &parameter) {
            if (parameter.hasChild()) {
                for (auto &child: parameter.getChildren()) {
                    if (child.first == "walk")
                        walk->setParameter(*child.second);
                    if (child.first == "kick")
                        kickZmp->setParameter(*child.second);
                    if (child.first == "keyframe")
                        keyframePlayer.setParameter(*child.second);
                }
            }
        }
    };


    class SetMovementEvent : public sc::event<SetMovementEvent> {
    public:
        SetMovementEvent(std::shared_ptr<control::MovementRequest> movementRequest) : movementRequest(movementRequest) {
        }

        const std::shared_ptr<control::MovementRequest> getMovementRequest() const {
            return movementRequest;
        }

    private:


        std::shared_ptr<control::MovementRequest> movementRequest;

    };


    class ControlMovementEvent : public sc::event<ControlMovementEvent> {
    };

    class IdleState : public sc::state<IdleState, ControlStateMachine>, public IControlSystem {

    public:
        IdleState(my_context ctx);

        typedef mpl::list<sc::custom_reaction<SetMovementEvent>, sc::custom_reaction<ControlMovementEvent>> reactions;

        sc::result react(const SetMovementEvent &evt);

        sc::result react(const ControlMovementEvent &evt);

        void control(perception::Perception &perception);

    };



    // define control states




    class CurrentMovementEndedEvent : public sc::event<CurrentMovementEndedEvent> {

    };


    class IControlMovementSystem : public IControlSystem {
    public:
        IControlMovementSystem();

    protected:
        virtual bool hasFinished() = 0;

        bool resetMovement;
        bool isFirstTime;

    };

    class KickMovementState : public sc::state<KickMovementState, ControlStateMachine>, public IControlMovementSystem {
    public:
        KickMovementState(my_context ctx);

        typedef mpl::list<sc::transition<CurrentMovementEndedEvent, IdleState>,
                sc::custom_reaction<ControlMovementEvent>> reactions;

        void control(perception::Perception &perception);

        sc::result react(const ControlMovementEvent &evt);

    private:
        virtual bool hasFinished();

        std::shared_ptr<KickRequest> kickRequest;
    };

    class WalkMovementState : public sc::state<WalkMovementState, ControlStateMachine>, public IControlMovementSystem {
    public:
        WalkMovementState(my_context ctx);

        typedef mpl::list<sc::custom_reaction<SetMovementEvent>,
                sc::transition<CurrentMovementEndedEvent, IdleState>,
                sc::custom_reaction<ControlMovementEvent>> reactions;

        void control(perception::Perception &perception);

        sc::result react(const SetMovementEvent &);

        sc::result react(const ControlMovementEvent &evt);

    private:
        virtual bool hasFinished();

        std::shared_ptr<WalkRequest> walkRequest;

    };


    class KeyframeMovementState
            : public sc::state<KeyframeMovementState, ControlStateMachine>, public IControlMovementSystem {
    public:
        KeyframeMovementState(my_context ctx);

        typedef mpl::list<sc::transition<CurrentMovementEndedEvent, IdleState>, sc::custom_reaction<ControlMovementEvent>> reactions;

        void control(perception::Perception &perception);

        sc::result react(const ControlMovementEvent &evt);

    private:
        virtual bool hasFinished();

        std::shared_ptr<KeyframeRequest> keyframeRequest;
    };


}

#endif //ITANDROIDS_SOCCER3D_CPP_CONTROLSTATEMACHINE_H


