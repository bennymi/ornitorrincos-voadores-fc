/*
 * WalkRequest.h
 *
 *  Created on: Oct 24, 2015
 *      Author: itandroids
 */

#ifndef SOURCE_CONTROL_WALKREQUEST_H_
#define SOURCE_CONTROL_WALKREQUEST_H_

#include "MovementRequest.h"
#include "math/Pose2D.h"
#include "math/Vector3.h"
#include "representations/NaoJoints.h"
#include "MovementRequestType.h"
#include "control/walking/OmnidirectionalWalkZMP.h"

using ::control::walking::OmnidirectionalWalkZMPParams;

namespace control {

    /// @todo Unify the WalkMode Options
    enum WalkMode {
        NORMAL, PRECISE, STOP
    };

    class WalkRequest : public MovementRequest {
    public:
        WalkRequest(itandroids_lib::math::Pose2D velocity);

        WalkRequest(itandroids_lib::math::Pose2D velocity, WalkMode walkMode);

        virtual ~WalkRequest();

        WalkMode getWalkMode();

        itandroids_lib::math::Pose2D &getVelocity();

    private:
        bool isWithParameters;
        itandroids_lib::math::Pose2D velocity;
        WalkMode walkMode;

    };

} /* namespace control */

#endif /* SOURCE_CONTROL_WALKREQUEST_H_ */
