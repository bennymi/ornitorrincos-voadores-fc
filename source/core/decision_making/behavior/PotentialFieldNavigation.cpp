//
// Created by francisco on 5/19/16.
//

#include "PotentialFieldNavigation.h"
#include "control/WalkRequest.h"

namespace decision_making {
    namespace behavior {

        const double PotentialFieldNavigation::TEAMMATE_INFLUENCE_RADIUS = 0.5;
        const double PotentialFieldNavigation::OPPONENT_INFLUENCE_RADIUS = 1.0;

        PotentialFieldNavigation::PotentialFieldNavigation(BehaviorFactory *factory) : Behavior(factory),
                                                                                       soccerPotentialField(
                                                                                               SoccerPotentialField(
                                                                                                       PotentialFieldParameters())) {
        }


        PotentialFieldNavigation::PotentialFieldNavigation(BehaviorFactory *factory,
                                                           PotentialFieldParameters parameters) :
                Behavior(factory), soccerPotentialField(parameters) {
        }

        void PotentialFieldNavigation::behave(modeling::Modeling &modeling) {
            fieldDirection = calculateFieldDirection(modeling.getWorldModel().getSelf().getPosition().translation,
                                                     modeling);

            double referenceAngle = fieldDirection.angle();
            double angularSpeed = navigationHelper.controlAngle(referenceAngle,
                                                                modeling.getWorldModel().getSelf().getPosition().rotation);
            Pose2D velocity(angularSpeed, fieldDirection.globalToRelative(Vector2<double>(),
                                                                          modeling.getWorldModel().getSelf().getPosition().rotation));
            velocity = navigationHelper.saturateWalkVelocity(velocity);

            movementRequest = std::make_shared<control::WalkRequest>(velocity);
        }

        Vector2<double>
        PotentialFieldNavigation::calculateFieldDirection(Vector2<double> selfPosition, modeling::Modeling &modeling) {
            soccerPotentialField.clear();

            Vector2<double> targetPosition = target;//modeling.getWorldModel().getBall().getPosition().translation;
            Vector3<double> goalPosition3D = modeling.getWorldModel().getTheirGoalPosition();
            Vector2<double> goalPosition = Vector2<double>(goalPosition3D.x, goalPosition3D.y);

            Vector2<double> selfToTarget = targetPosition - selfPosition;

            soccerPotentialField.addTarget(targetPosition);

            for (auto &opponent : modeling.getWorldModel().getOpponents()) {
                Vector2<double> selfToOpponent = opponent->getPosition().translation - selfPosition;
                double crossProduct = selfToTarget.cross(selfToOpponent);
                bool counterClockwise = (crossProduct > 0.0);

                if (selfToOpponent.abs() <= OPPONENT_INFLUENCE_RADIUS)
                    soccerPotentialField.addOpponent(opponent->getPosition().translation, counterClockwise);
            }

            for (auto &teammate : modeling.getWorldModel().getTeammates()) {
                Vector2<double> selfToTeammate = teammate->getPosition().translation - selfPosition;
                double crossProduct = selfToTarget.cross(selfToTeammate);
                bool counterClockwise = (crossProduct > 0.0);

                if (selfToTeammate.abs() <= TEAMMATE_INFLUENCE_RADIUS)
                    soccerPotentialField.addTeammate(teammate->getPosition().translation, counterClockwise);
            }

            fieldDirection = soccerPotentialField.calculateFieldDirection(selfPosition);

            return fieldDirection;
        }


        void PotentialFieldNavigation::setTarget(Vector2<double> target) {
            this->target = target;
        }

//        void PotentialFieldNavigation::addBallDipole(Vector2<double> ballPosition,
//                                                               Vector2<double> goalPosition) {
//            Vector2<double> diffVector = goalPosition - ballPosition;
//            diffVector.normalize();
//            Vector2<double> ballForwardPosition = ballPosition + (diffVector/parameters.getBallToGoalNormalizedRatio());
//            auto ballAttractiveObject = PotentialFieldObject(ballPosition, parameters.getBallAttraction(), PotentialFieldFunctions::SQUARE_INVERSE_FUNCTION);
//            objects.push_back(ballAttractiveObject);
//            auto ballRepulsiveObject = PotentialFieldObject(ballForwardPosition, parameters.getBallRepulsion(), PotentialFieldFunctions::SQUARE_INVERSE_FUNCTION);
//            objects.push_back(ballRepulsiveObject);
//        }

        const Vector2<double> &PotentialFieldNavigation::getFieldDirection() {
            return fieldDirection;
        }


    }
}