/*
 * BehaviorFactory.h
 *
 *  Created on: Oct 31, 2015
 *      Author: itandroids
 */

#ifndef SOURCE_DECISION_MAKING_BEHAVIOR_BEHAVIORFACTORY_H_
#define SOURCE_DECISION_MAKING_BEHAVIOR_BEHAVIORFACTORY_H_

#include "BasicBehavior.h"
#include "NavigateToPosition.h"
#include "FollowLine.h"
#include "ConductBall.h"
#include "FocusBall.h"
#include "Say.h"
#include "NavigateToStaticPosition.h"
#include "PotentialFieldNavigation.h"
#include "GoalieBehavior.h"
#include "ActiveVision.h"
#include "Keep.h"
#include "CircleBall.h"
#include "KickBehavior.h"
#include "NavigateToBall.h"
#include "DribbleBall.h"
#include "BeamBehavior.h"

#include "SetPlay.h"
#include "set_play/BeforeKickOffSetPlay.h"
#include "set_play/GameOverSetPlay.h"

#include "set_play/OpponentCornerKickSetPlay.h"
#include "set_play/OpponentFreeKickSetPlay.h"
#include "set_play/OpponentGoalKickSetPlay.h"
#include "set_play/OpponentGoalSetPlay.h"
#include "set_play/OpponentKickInSetPlay.h"
#include "set_play/OpponentKickOffSetPlay.h"
#include "set_play/OpponentOffsideSetPlay.h"

#include "set_play/OwnCornerKickSetPlay.h"
#include "set_play/OwnFreeKickSetPlay.h"
#include "set_play/OwnGoalKickSetPlay.h"
#include "set_play/OwnGoalSetPlay.h"
#include "set_play/OwnKickInSetPlay.h"
#include "set_play/OwnKickOffSetPlay.h"
#include "set_play/OwnOffsideSetPlay.h"


namespace decision_making {
    namespace behavior {

        class BehaviorFactory {
        public:
            /**
             * Constructor for BehaviorFactory.
             */
            BehaviorFactory();

            /**
             * Destructor.
             */
            virtual ~BehaviorFactory();

            /**
             * Method that returns the BasicBehavior object
             */
            BasicBehavior &getBasicBehavior();

            /**
             * Method that returns the NavigatePosition object
             */
            NavigateToPosition &getNavigatePosition();

            /**
             * Method that returns the FollowLine object
             */
            FollowLine &getFollowLine();

            /**
             * Method that returns the ConductBall object
             */
            ConductBall &getConductBall(); // Podemos alterar

            /**
             * Method that returns the NavigateToStaticPosition object
             */
            NavigateToStaticPosition &getNavigateToStaticPosition();

            /**
             * Method that returns the FocusBall object
             */
            FocusBall &getFocusBall();

            /**
             * Method that returns the Say object
             * @return say Object
             */
            Say &getSay();

            /**
             * Method that returns the potentialFieldNavigator object
             * @return potential field Object
             */
            PotentialFieldNavigation &getPotentialFieldNavigationBehavior();

            /**
             * Method that returns the GoalieBehavior object
             * @return goalie behavior Object
             */
            GoalieBehavior &getGoalieBehavior();

            /**
             * Method that returns the active vision behavior
             * @return active vision Object
             */
            ActiveVision &getActiveVision();

            /**
             * Method that returns the keep behavior
             * @return keep object
             */
            Keep &getKeep();

            /**
             * Method that returns the circle Ball Behavior
             * @return circleBall object
             */
            CircleBall &getCircleBall();

            /**
             * Method that returns the kickBehavior
             * @return kickBehavior object
             */
            KickBehavior &getKickBehavior();


            /**
             * Method that returns the NavigateToBall behavior.
             * @return navigateToBall object
             */
            NavigateToBall &getNavigateToBall();

            /**
             * Method that returns the DribbleBall behavior.
             * @return dribbleBall object
             */
            DribbleBall &getDribbleBall();

            /**
             * Method that returns the DribbleBall behavior.
             * @return dribbleBall object
             */
            BeamBehavior &getBeamBehavior();

            /**
             * Method that returns the Set Play .
             * @return setPlay object
             */
            SetPlay &getSetPlay();

            BeforeKickOffSetPlay &getBeforeKickOffSetPlay();

            GameOverSetPlay &getGameOverSetPlay();

            OpponentCornerKickSetPlay &getOpponentCornerKickSetPlay();

            OpponentFreeKickSetPlay &getOpponentFreeKickSetPlay();

            OpponentGoalKickSetPlay &getOpponentGoalKickSetPlay();

            OpponentGoalSetPlay &getOpponentGoalSetPlay();

            OpponentKickInSetPlay &getOpponentKickInSetPlay();

            OpponentKickOffSetPlay &getOpponentKickOffSetPlay();

            OpponentOffsideSetPlay &getOpponentOffsideSetPlay();

            OwnCornerKickSetPlay &getOwnCornerKickSetPlay();

            OwnFreeKickSetPlay &getOwnFreeKickSetPlay();

            OwnGoalKickSetPlay &getOwnGoalKickSetPlay();

            OwnGoalSetPlay &getOwnGoalSetPlay();

            OwnKickInSetPlay &getOwnKickInSetPlay();

            OwnKickOffSetPlay &getOwnKickOffSetPlay();

            OwnOffsideSetPlay &getOwnOffsideSetPlay();


        private:
            BasicBehavior basicBehavior;
            GoalieBehavior goalieBehavior;
            NavigateToPosition navigateToPosition;
            NavigateToStaticPosition navigateToStaticPosition;
            PotentialFieldNavigation potentialFieldNavigator;
            ConductBall conductBall;
            FollowLine followLine;
            FocusBall focusBall;
            Say say;
            ActiveVision activeVision;
            Keep keep;
            CircleBall circleBall;
            KickBehavior kickBehavior;
            NavigateToBall navigateToBall;
            DribbleBall dribbleBall;
            BeamBehavior beamBehavior;

            SetPlay setPlay;
            BeforeKickOffSetPlay beforeKickOffSetPlay;
            GameOverSetPlay gameOverSetPlay;
            OpponentCornerKickSetPlay opponentCornerKickSetPlay;
            OpponentFreeKickSetPlay opponentFreeKickSetPlay;
            OpponentGoalKickSetPlay opponentGoalKickSetPlay;
            OpponentGoalSetPlay opponentGoalSetPlay;
            OpponentKickInSetPlay opponentKickInSetPlay;
            OpponentKickOffSetPlay opponentKickOffSetPlay;
            OpponentOffsideSetPlay opponentOffsideSetPlay;
            OwnCornerKickSetPlay ownCornerKickSetPlay;
            OwnFreeKickSetPlay ownFreeKickSetPlay;
            OwnGoalKickSetPlay ownGoalKickSetPlay;
            OwnGoalSetPlay ownGoalSetPlay;
            OwnKickInSetPlay ownKickInSetPlay;
            OwnKickOffSetPlay ownKickOffSetPlay;
            OwnOffsideSetPlay ownOffsideSetPlay;
        };

    } /* namespace behavior */
} /* namespace decision_making */

#endif /* SOURCE_DECISION_MAKING_BEHAVIOR_BEHAVIORFACTORY_H_ */
