//
// Created by francisco on 09/12/16.
//

#ifndef ITANDROIDS_SOCCER3D_CPP_PARAMETERTREECREATOR_H
#define ITANDROIDS_SOCCER3D_CPP_PARAMETERTREECREATOR_H

#include "utils/patterns/LeafParameter.h"
#include "utils/patterns/BranchParameter.h"
#include "utils/patterns/Parameter.h"
#include <boost/property_tree/json_parser.hpp>
#include <boost/property_tree/ptree.hpp>
#include "utils/configure_tree/control/ControlNodeCreator.h"
#include "utils/configure_tree/representations/RepresentationsNodeCreator.h"
using utils::configure_tree::control::ControlNodeCreator;
using utils::configure_tree::representations::RepresentationsNodeCreator;
using ::itandroids_lib::utils::patterns::Parameter;
using ::itandroids_lib::utils::patterns::BranchParameter;

namespace utils {
    namespace configure_tree {
        class ParameterTreeCreator {
        public:

            template<class Content = pt::ptree, class Key = std::string>
            static std::shared_ptr<Parameter<Content,Key>> getRoot();

        private:
            template<class Content, class Key>
            static void connectNodesToRoot(std::shared_ptr<BranchParameter<Content,Key>> root, std::shared_ptr<Parameter<Content,Key>> control, std::shared_ptr<Parameter<Content,Key>> representations);
            ParameterTreeCreator();
        };

        template<class Content , class Key >
        std::shared_ptr<Parameter<Content,Key>> ParameterTreeCreator::getRoot(){
            auto root = std::make_shared<BranchParameter<Content,Key>>();
            ControlNodeCreator controlNodeCreator;
            auto controlNode = controlNodeCreator.generateNode<Content,Key>();
            RepresentationsNodeCreator representationsNodeCreator;
            auto representationsNode = representationsNodeCreator.generateNode<Content,Key>();
            connectNodesToRoot(root, controlNode, representationsNode);
            return root;
        }

        template<>
        void ParameterTreeCreator::connectNodesToRoot<pt::ptree, std::string>(std::shared_ptr<BranchParameter<pt::ptree, std::string>> root,
                                                      std::shared_ptr<Parameter<pt::ptree, std::string>> control,
                                                      std::shared_ptr<Parameter<pt::ptree, std::string>> representations);

    }

}


#endif //ITANDROIDS_SOCCER3D_CPP_PARAMETERTREECREATOR_H
