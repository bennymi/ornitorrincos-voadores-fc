//
// Created by francisco on 5/21/16.
//

#include "PotentialFieldParameters.h"


namespace decision_making {
    namespace behavior {
        namespace potential_field_navigation {
            PotentialFieldParameters::PotentialFieldParameters() : //BALL_ATTRACTION(DEFAULT_BALL_ATTRACTION),
            //BALL_REPULSION(DEFAULT_BALL_REPULSION),
            //BALL_TO_GOAL_NORMALIZED_RATIO(DEFAULT_BALL_TO_GOAL_NORMALIZED_RATIO),
                    OPPONENT_REPULSION(DEFAULT_OPPONENT_REPULSION),
                    TEAMMATE_REPULSION(DEFAULT_TEAMMATE_REPULSION),
                    FIELD_BORDER_REPULSION(DEFAULT_FIELD_BORDER_REPULSION),
                    TARGET_ATTRACTION(DEFAULT_TARGET_ATTRACTION),
                    //THEIR_GOAL_ATTRACTION(DEFAULT_THEIR_GOAL_ATTRACTION),
                    DIPOLE_DISPLACEMENT(DEFAULT_DIPOLE_DISPLACEMENT),
                    RANDOM_RATIO(DEFAULT_RANDOM_RATIO) {

            }

            PotentialFieldParameters::PotentialFieldParameters(//double ballAttraction,
                    //double ballRepulsion,
                    //double ballToGoalNormalizedRatio,
                    double opponentRepulsion,
                    double teammateRepulsion,
                    double fieldBorderRepulsion,
                    double targetAttraction,
                    //double theirGoalAttraction,
                    double dipoleDisplacement,
                    double randomRatio) :
            //BALL_ATTRACTION(ballAttraction),
            //BALL_REPULSION(ballRepulsion),
            //BALL_TO_GOAL_NORMALIZED_RATIO(ballToGoalNormalizedRatio),
                    OPPONENT_REPULSION(opponentRepulsion),
                    TEAMMATE_REPULSION(teammateRepulsion),
                    FIELD_BORDER_REPULSION(fieldBorderRepulsion),
                    TARGET_ATTRACTION(targetAttraction),
                    //THEIR_GOAL_ATTRACTION(theirGoalAttraction),
                    DIPOLE_DISPLACEMENT(dipoleDisplacement),
                    RANDOM_RATIO(randomRatio) {

            }

//            const double &PotentialFieldParameters::getBallAttraction() {
//                return BALL_ATTRACTION;
//            }

//            const double &PotentialFieldParameters::getBallRepulsion() {
//                return BALL_REPULSION;
//            }
//
//            const double &PotentialFieldParameters::getBallToGoalNormalizedRatio() {
//                return BALL_TO_GOAL_NORMALIZED_RATIO;
//            }

            const double &PotentialFieldParameters::getOpponentRepulsion() {
                return OPPONENT_REPULSION;
            }

            const double &PotentialFieldParameters::getTeammateRepulsion() {
                return TEAMMATE_REPULSION;
            }

            const double &PotentialFieldParameters::getFieldBorderRepulsion() {
                return FIELD_BORDER_REPULSION;
            }

            const double &PotentialFieldParameters::getTargetAttraction() {
                return TARGET_ATTRACTION;
            }

//            const double &PotentialFieldParameters::getTheirGoalAttraction() {
//                return THEIR_GOAL_ATTRACTION;
//            }

            const double &PotentialFieldParameters::getDipoleDisplacement() {
                return DIPOLE_DISPLACEMENT;
            }

            const double &PotentialFieldParameters::getRandomRatio() {
                return RANDOM_RATIO;
            }


        }
    }
}


