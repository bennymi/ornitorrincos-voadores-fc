/*
 * AgentStatePerceptor.h
 *
 *  Created on: Aug 8, 2015
 *      Author: mmaximo
 */

#ifndef SOURCE_PERCEPTION_AGENTSTATEPERCEPTOR_H_
#define SOURCE_PERCEPTION_AGENTSTATEPERCEPTOR_H_

#include "utils/parser/ParserTreeNode.h"
#include "perception/Perceptor.h"

namespace perception {

    class AgentStatePerceptor : public Perceptor {
    public:
        /**
         * Default Constructor for Agent State perceptor
         */
        AgentStatePerceptor();

        /**
         * Constructor for Agent State Perceptor
         *  @param node part of the parser tree that represents
         *             an AgentState Perceptor.
         */
        AgentStatePerceptor(utils::parser::ParserTreeNode &node);

        /**
         * Destructor
         */
        virtual ~AgentStatePerceptor();

        /**
         * Gets the Temperature measured by agent state
         * @return measured temperature
         */
        double getTemperature();

        /**
         * Gets the Battery level
         * @return measured battery level
         */
        double getBattery();

    private:
        double temperature;
        double battery;
    };

} /* namespace perception */

#endif /* SOURCE_PERCEPTION_AGENTSTATEPERCEPTOR_H_ */
