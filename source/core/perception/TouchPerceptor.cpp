#include "TouchPerceptor.h"

namespace perception {

    /**
     * This constructor takes a parser tree node and manipulates it to get information
     * of name and state.
     */
    TouchPerceptor::TouchPerceptor(utils::parser::ParserTreeNode &node) :
            Perceptor(node) {
        std::vector<std::string> stringVector;
        boost::split(stringVector, node.content, boost::is_any_of(" "));
        name = stringVector[2];
        state = boost::lexical_cast<bool>(stringVector[4]);

    }

    TouchPerceptor::~TouchPerceptor() {
    }

    /*
     * Getter method: returns the name of agent
     */
    std::string TouchPerceptor::getName() {
        return name;
    }

    /*
     * This method returns true if the state is touching.
     */
    bool TouchPerceptor::isTouching() {
        return state;
    }

} /* namespace perception */
