/*
 * Self.cpp
 *
 *  Created on: Sep 8, 2015
 *      Author: mmaximo
 */

#include "Self.h"

namespace representations {

    Self::Self() {
    }

    Self::~Self() {
    }

    void Self::setPosition(itandroids_lib::math::Pose2D pose) {
        position = pose;
    }

    itandroids_lib::math::Pose2D &Self::getPosition() {
        return position;
    }

} /* namespace representations */
