/*
 * Wizard.h
 *
 *  Created on: Aug 8, 2015
 *      Author: mmaximo
 */

#ifndef SOURCE_UTILS_WIZARD_WIZARD_H_
#define SOURCE_UTILS_WIZARD_WIZARD_H_

#include "math/Vector3.h"
#include "math/Matrix.h"
#include "math/Pose3D.h"
#include "representations/PlaySide.h"
#include "representations/RawPlayMode.h"
#include "communication/Communication.h"
#include "communication/ServerSocket.h"
#include <boost/lexical_cast.hpp>
#include <boost/algorithm/string.hpp>
#include "utils/parser/Parser.h"
#include "utils/parser/KeyValuePair.h"
#include "utils/parser/ParserTreeNode.h"
#include <mutex>
#include <thread>

namespace utils {
    namespace wizard {

        using namespace itandroids_lib::math;

        class Wizard {
        public:
            Wizard(communication::Communication &communication);

            virtual ~Wizard();

            /**
             * A function to change the agent position and direction in the field
             *
             * @oaram unum Agent uniform Number
             * @param playSide the agent playside in the field
             * @param newPosition a vector indicating the new position in (x,y,z)
             * @param newDirection indicating the agent position to be sed
             *
             */
            void setAgentPositionAndDirection(int unum,
                                              representations::PlaySide::PLAY_SIDE playSide,
                                              Vector3<double> newPosition,
                                              double newDirection);

            void setBatteryLevel(int unum,
                                 representations::PlaySide::PLAY_SIDE teamSide, double batteryLevel);

            void setTemperature(int unum, representations::PlaySide::PLAY_SIDE teamSide,
                                double temperature);

            void setGameTime(double time);

            void setBallPosition(Vector3<double> newPosition);

            void setBallPositionAndVelocity(Vector3<double> newPosition, Vector3<double> newVelocity);

            void setBallVelocity(Vector3<double> newVelocity);

            void setPlayMode(representations::RawPlayMode::RAW_PLAY_MODE playMode);

            void dropBall();

            void kickOff(representations::PlaySide::PLAY_SIDE teamSide);

            void selectAgent(int unum, representations::PlaySide::PLAY_SIDE teamSide);

            void killAgent(int unum, representations::PlaySide::PLAY_SIDE teamSide);

            void killSelectedAgent();

            void repositionAgent(int unum, representations::PlaySide::PLAY_SIDE teamSide);

            void repositionSelectedAgent();

            void killSimulator();

            void setScore(int left, int right);

            itandroids_lib::math::Matrix<4, 4, double> readTransformationMatrix(std::string vectorString);

            void run();

            double getLastUpdateTime();

            itandroids_lib::math::Matrix<4, 4, double> getAgentTransform();

            itandroids_lib::math::Matrix<4, 4, double> getBallTransform();


            Vector3<double> getAgentTranslation();

            Vector3<double> getBallTranslation();

            RotationMatrix getAgentRotation();

            Pose3D getAgentPose3D();

        private:
            void communicationTask();

            void update(std::string content);

            void iterateAndCompare(utils::parser::ParserTreeNode *node);

            void stop();

            void setSendMessage(std::string command);

            void sendMessage();

            std::string getVectorString(Vector3<double> vect);

            itandroids_lib::math::Matrix<4, 4, double> readTransformationMatrix(utils::parser::ParserTreeNode *node);

            std::thread communicationThread;
            double lastUpdateTime;
            int transformCount;
            std::vector<std::string> messages;

            std::mutex mtx;
            std::mutex updateMtx;
            communication::Communication &comm;
            itandroids_lib::math::Matrix<4, 4, double> agentHeadTransform;
            itandroids_lib::math::Matrix<4, 4, double> ballTransform;
            itandroids_lib::math::Matrix<4, 4, double> firstAgentTransform;
            itandroids_lib::math::Matrix<4, 4, double> torsoTransform;
            itandroids_lib::math::Matrix<4, 4, double> headTransform;
            itandroids_lib::math::Matrix<4, 4, double> firstTorsoTransform;
            itandroids_lib::math::Matrix<4, 4, double> firstHeadTransform;

        };

    } /* namespace wizard */
} /* namespace utils */

#endif /* SOURCE_UTILS_WIZARD_WIZARD_H_ */
