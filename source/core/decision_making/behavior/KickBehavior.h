//
// Created by francisco on 6/5/16.
//

#ifndef ITANDROIDS_SOCCER3D_CPP_KICKBEHAVIOR_H
#define ITANDROIDS_SOCCER3D_CPP_KICKBEHAVIOR_H

#include "decision_making/behavior/Behavior.h"
#include "math/Vector2.h"
#include "modeling/Modeling.h"
#include "decision_making/behavior/kick_behavior/KickParameters.h"
#include "decision_making/behavior/kick_behavior/KickManager.h"
#include "decision_making/behavior/kick_behavior/Kick.h"


namespace decision_making {
    namespace behavior {
        using ::decision_making::behavior::kick_behavior::KickParameters;
        using ::decision_making::behavior::kick_behavior::Kick;

        class BehaviorFactory;

        class KickBehavior : public Behavior {
            enum STATES {
                ARRIVING_TO_BALL,
                ROTATING_AROUND_BALL,
                GETTING_CORRECT_POSITION,
                KICKING,
            };

        public:
            KickBehavior();

            KickBehavior(BehaviorFactory *factory);

            void setKick(modeling::Modeling &modeling);

            void doKick(modeling::Modeling &modeling);

            void behave(modeling::Modeling &modeling);

            void setTarget(Vector2<double> target);

            double getKickCostValue(modeling::Modeling &modeling);

        protected:
            kick_behavior::KickManager kickManager;
        private:
            void findState(modeling::Modeling &modeling);

            KickParameters params;
            Vector2<double> target;
            STATES state;
            static const double BEST_POSITION_DIFF;
            static const double CIRCLE_BALL_RATIO;
            static const double LONG_DISTANCE_OFFSET;
            Pose2D expectedPosition;
            double expectedSelfToBallAngle;
            Pose2D relativeSelfToBallPosition;
            std::shared_ptr<Kick> settedKick;

        };
    }
}


#endif //ITANDROIDS_SOCCER3D_CPP_KICKBEHAVIOR_H
