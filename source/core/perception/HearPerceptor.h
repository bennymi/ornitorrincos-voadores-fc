/*
 * HearPerceptor.h
 *
 *  Created on: Aug 8, 2015
 *      Author: mmaximo
 */

#ifndef SOURCE_PERCEPTION_HEARPERCEPTOR_H_
#define SOURCE_PERCEPTION_HEARPERCEPTOR_H_

#include "utils/parser/KeyValuePair.h"
#include "utils/parser/ParserTreeNode.h"
#include "perception/Perceptor.h"
#include <boost/lexical_cast.hpp>
#include <boost/algorithm/string.hpp>
#include "math/MathUtils.h"

namespace perception {

    using namespace itandroids_lib::math;

    class HearPerceptor : public Perceptor {
    public:
        HearPerceptor();

        /**
         * Constructor for HearPerceptor.
         *
         * @param node part of the parser tree that represents
         *             an hear perceptor.
         */
        HearPerceptor(utils::parser::ParserTreeNode &node);

        /**
         * Destructor.
         */
        virtual ~HearPerceptor();

        /**
         * Gets the team
         * @return team.
         */
        std::string getTeam();

        /**
         * Gets the time
         * @return time.
         */
        double getTime();

        /**
         * Gets the boolean fromSelf
         * @return fromSelf.
         */
        bool isFromSelf();

        /**
         * Gets a string that represents a message
         * @return message.
         */
        std::string getMessage();

        /**
         * Gets the message's direction
         * @return direction.
         */
        double getDirection();

        /**
         * Gets the hearData
         * @return hearData.
         */
        representations::HearData getHearData();

    protected:

    private:
        std::string team;
        double time;

        /**
        * It is 1 if the message is from itself. Else, it is 0
        */
        bool fromSelf;
        double direction;
        std::string message;
        representations::HearData hearData;
    };

} /* namespace perception */

#endif /* SOURCE_PERCEPTION_HEARPERCEPTOR_H_ */
