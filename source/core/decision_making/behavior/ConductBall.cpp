/*
 * ConductBall.cpp
 *
 *  Created on: Oct 31, 2015
 *      Author: itandroids
 */

#include "ConductBall.h"
#include "BehaviorFactory.h"

namespace decision_making {
    namespace behavior {

        ConductBall::ConductBall() {

        }

        ConductBall::ConductBall(BehaviorFactory *behaviorFactory) :
                Behavior(behaviorFactory) {

        }

        ConductBall::~ConductBall() {
        }

        void ConductBall::behave(modeling::Modeling &modeling) {

            Pose2D &selfPose = modeling.getWorldModel().getSelf().getPosition();
            Pose2D &ballPose = modeling.getWorldModel().getBall().getPosition();

            double direction = (ballPose - selfPose).getAngle();

            behaviorFactory->getFollowLine().setLine(direction, ballPose.translation);
            behaviorFactory->getFollowLine().behave(modeling);
            movementRequest = behaviorFactory->getFollowLine().getMovementRequest();
        }

    } /* namespace behavior */
} /* namespace decision_making */
