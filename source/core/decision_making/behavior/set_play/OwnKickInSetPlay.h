//
// Created by dicksiano on 8/14/16.
//

#ifndef ITANDROIDS_SOCCER3D_CPP_OWNKICKINSETPLAY_H
#define ITANDROIDS_SOCCER3D_CPP_OWNKICKINSETPLAY_H

#include "../SetPlay.h"


namespace decision_making {
    namespace behavior {

        class OwnKickInSetPlay : public SetPlay {
        public:

            /**
            * Default constructor.
            */
            OwnKickInSetPlay();

            /**
            * Assignment constructor.
            *
            * @param pointer to BehaviorFactory.
            */
            OwnKickInSetPlay(BehaviorFactory *behaviorFactory);

            /**
            * Destructor.
            */
            virtual ~OwnKickInSetPlay();

            /**
            * This function controls the actions.
            *
            * @param modeling.
            */
            void behave(modeling::Modeling &modeling);

        private:

        };

    } /* namespace behavior */
} /* namespace decision_making */


#endif //ITANDROIDS_SOCCER3D_CPP_OWNKICKINSETPLAY_H
