//
// Created by francisco on 6/16/16.
//

#include "Kick.h"
#include "KickParameters.h"

namespace decision_making {
    namespace behavior {
        namespace kick_behavior {

            KickParameters &Kick::getKickParameters() {
                return parameters;
            }

            void Kick::setKickParameters(const KickParameters &parameters) {
                this->parameters = parameters;
            }

            double Kick::getAccuracy() {
                return 0;
            }

            void Kick::computeParameters() {

            }

            Kick::~Kick() {

            }


        }
    }
}


