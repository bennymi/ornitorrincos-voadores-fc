//
// Created by francisco on 6/19/16.
//

#ifndef ITANDROIDS_SOCCER3D_CPP_KICKMANAGERSTUB_H
#define ITANDROIDS_SOCCER3D_CPP_KICKMANAGERSTUB_H

#include "decision_making/behavior/kick_behavior/KickManager.h"

namespace decision_making {
    namespace behavior {
        namespace kick_behavior {
            class KickManagerStub : public KickManager {
            public:
                void setKick(std::shared_ptr<Kick> kick);

            private:
                virtual std::shared_ptr<Kick> selectKick(double distance, double direction) override;
            };
        }
    }
}


#endif //ITANDROIDS_SOCCER3D_CPP_KICKMANAGERSTUB_H
