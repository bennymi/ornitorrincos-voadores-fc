//
// Created by francisco on 5/19/16.
//

#include "decision_making/behavior/PotentialFieldNavigation.h"
#include "utils/LogSystem.h"
#include "representations/NavigationParams.h"
#include "decision_making/behavior/potential_field_navigation/PotentialFieldParameters.h"
#include "utils/wizard/Wizard.h"
#include "utils/roboviz/Roboviz.h"
#include "communication/Communication.h"
#include "communication/ServerSocket.h"

using ::decision_making::behavior::PotentialFieldNavigation;
using ::decision_making::behavior::BehaviorFactory;
using ::itandroids_lib::math::Vector2;
using ::itandroids_lib::utils::LogSystem;
using ::representations::NavigationParams;
using ::decision_making::behavior::potential_field_navigation::PotentialFieldParameters;
using ::utils::wizard::Wizard;
using ::utils::roboviz::Roboviz;

class PotentialFieldNavigationBehavior_AgentTest : public PotentialFieldNavigation {
public:
    PotentialFieldNavigationBehavior_AgentTest(BehaviorFactory *factory);

    PotentialFieldNavigationBehavior_AgentTest(BehaviorFactory *factory, PotentialFieldParameters parameters);

    void addBallDipoleTest(Vector2<double> ballPosition, Vector2<double> goalPosition);

    void addBallMonopoleTest(Vector2<double> ballPosition);

    void addOpponentObjectTest(Vector2<double> opponentPosition);

    void addGoalTest(Vector2<double> goalPosition);

    void addRandomTest();

    Vector2<double> getDirection(const Vector2<double> &selfPosition);

    Wizard wiz;
private:

    communication::Communication wizCommunication;


};

PotentialFieldNavigationBehavior_AgentTest::PotentialFieldNavigationBehavior_AgentTest(BehaviorFactory *factory)
        : PotentialFieldNavigation(factory), wizCommunication(new communication::ServerSocketImpl("127.0.0.1", 3200)),
          wiz(wizCommunication) {}

void PotentialFieldNavigationBehavior_AgentTest::addBallDipoleTest(Vector2<double> ballPosition,
                                                                   Vector2<double> goalPosition) {
//    PotentialFieldNavigation::addBallDipole(ballPosition, goalPosition);
    wizCommunication.establishConnection();
}

void PotentialFieldNavigationBehavior_AgentTest::addOpponentObjectTest(Vector2<double> opponentPosition) {
//    PotentialFieldNavigation::addOpponent(opponentPosition);
}

Vector2<double> PotentialFieldNavigationBehavior_AgentTest::getDirection(const Vector2<double> &selfPosition) {
//    return PotentialFieldNavigation::getFieldDirection(selfPosition);
}

void PotentialFieldNavigationBehavior_AgentTest::addGoalTest(Vector2<double> goalPosition) {
//    return PotentialFieldNavigation::addGoal(goalPosition);
}

PotentialFieldNavigationBehavior_AgentTest::PotentialFieldNavigationBehavior_AgentTest(BehaviorFactory *factory,
                                                                                       PotentialFieldParameters parameters)
        : PotentialFieldNavigation(factory, parameters),
          wizCommunication(new communication::ServerSocketImpl("127.0.0.1", 3200)), wiz(wizCommunication) {
    wizCommunication.establishConnection();
    wiz.run();
}

void PotentialFieldNavigationBehavior_AgentTest::addBallMonopoleTest(Vector2<double> ballPosition) {
//    PotentialFieldNavigation::addBallMonopole(ballPosition);
}

void PotentialFieldNavigationBehavior_AgentTest::addRandomTest() {
//    PotentialFieldNavigation::addRandomField();
}


int main() {
//    //Adding the objects
//    //add the ball
//    Vector2<double> ballPosition(8,0);
//    Vector2<double> goalPosition(15,0);
//    Roboviz roboviz;
////    PotentialFieldParameters parameters(50, -40,PotentialFieldParameters::DEFAULT_BALL_TO_GOAL_NORMALIZED_RATIO, PotentialFieldParameters::DEFAULT_OPPONENT_REPULSION, 10, 5);
//    PotentialFieldNavigationBehavior_AgentTest potentialField(nullptr, parameters);
////    potentialField.wiz.run();
//    potentialField.addBallDipoleTest(ballPosition, goalPosition);
//    potentialField.addRandomTest();
////    potentialField.addBallMonopoleTest(ballPosition);
//    //add the opponents
//    std::random_device rd;
//    std::mt19937 gen(rd());
//    std::uniform_real_distribution<> disx(-15, 15);
//    std::uniform_real_distribution<> disy(-10,10);
//    Vector2<double> opponentPosition;
//    for (int i = 1; i<= 11; i++){
//        opponentPosition = Vector2<double> (disx(gen), disy(gen));
//        potentialField.addOpponentObjectTest(opponentPosition);
//    }
//
//    auto & logger = LogSystem::getInstance().getLogger("BallPotentialField");
//    std::string n1("animated.visibleObjects");
//    std::string buffer("animated");
//    const double X_STEP = 0.1;
//    const double Y_STEP = 0.1;
//    while(true){
//
//        for (double x = -modeling::FieldDescription::FIELD_LENGTH/2; x <= modeling::FieldDescription::FIELD_LENGTH/2; x+=X_STEP){
//            for(double y = -modeling::FieldDescription::FIELD_WIDTH/2; y <= modeling::FieldDescription::FIELD_WIDTH/2; y+=Y_STEP){
//                Vector2<double> selfPosition(x,y);
//                auto direction = potentialField.getDirection(selfPosition);
//                direction.normalize(0.2);
//                roboviz.drawLine(x, y, 0.1, x + direction.x,
//                                 y+direction.y, 0.1, 1.0, 1.0, 1.0, 0.0, &n1);
////                roboviz.drawCicle(x+direction.x,y+direction.y,0.01,0.01,1,0,0,&n1);
//            }
//        }
//        roboviz.swapBuffers(&buffer);
//    }
//
//    return 0;
}