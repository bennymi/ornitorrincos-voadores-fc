//
// Created by dicksiano on 08/05/16.
//

#ifndef ITANDROIDS_SOCCER3D_CPP_SAY_H
#define ITANDROIDS_SOCCER3D_CPP_SAY_H

#include "Behavior.h"


using namespace itandroids_lib::math;

namespace decision_making {
    namespace behavior {

        using namespace itandroids_lib::math;

        class BehaviorFactory;

        class Say : public Behavior {
        public:
            /*
             * Default constructor.
             */
            Say();

            /*
             * Constructor.
             *
             * @param pointer to behavior factory.
             */
            Say(BehaviorFactory *behaviorFactory);

            /*
             * Constructor used for manual role assigment vector
             */
            Say(int cycles, std::vector<int> message, double ballX, double ballY, double agentX, double agentY,
                bool isFallen);


            /*
             * Deafault destructor.
             */
            virtual ~Say();

            /*
             * Method that decides if the agent should say a message.
             *
             * @param modeling
             */
            void behave(modeling::Modeling &modeling);

            /*
             * Method that returns the pointer to message.
             *
             * @return message
             */
            std::shared_ptr<std::string> getSayMessage();

            /*
             * Method that returns the content of message in a string
             */
            std::string getStringMessage();


        private:
            std::shared_ptr<std::string> sayMessage;
            int cycles;
            int numberAgent;
            double lastSayTime;
            double agentX;
            double agentY;
            double currentServerTime;
            double ballLastSeenServerTime;
            double ballX;
            double ballY;
            bool agentFallen;
            std::vector<int> roleAssignment;
            std::string stringMessage;

            void dataToBitsGoalie(std::vector<int> &bits, modeling::Modeling &modeling);

            void dataToBits(std::vector<int> &bits);

            void bitsToString(const std::vector<int> &bits, std::string &message);

            // std::vector<int> intToBits(const unsigned long long int &n, const int &numBits);
            std::vector<int> intToSignedBits(const long long &n, const int &numBits);

            std::vector<int> intToBits(const int &n, const int &numBits);
        };

    } /* namespace behavior */
} /* namespace decision_making */


#endif //ITANDROIDS_SOCCER3D_CPP_SAY_H
