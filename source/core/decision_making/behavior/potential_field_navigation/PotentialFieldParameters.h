//
// Created by francisco on 5/21/16.
//

#ifndef ITANDROIDS_SOCCER3D_CPP_POTENTIALFIELDPARAMETERS_H
#define ITANDROIDS_SOCCER3D_CPP_POTENTIALFIELDPARAMETERS_H

namespace decision_making {
    namespace behavior {
        namespace potential_field_navigation {

            class PotentialFieldParameters {
            public:
                PotentialFieldParameters();

                PotentialFieldParameters(//double ballAttraction,
                        //double ballRepulsion,
                        //double ballToGoalNormalizedRatio,
                        double opponentRepulsion,
                        double teammateRepulsion,
                        double fieldBorderRepulsion,
                        double targetAttraction,
                        //double theirGoalAttraction,
                        double dipoleDisplacement,
                        double randomRatio);

                //static constexpr double DEFAULT_BALL_ATTRACTION = -20.0;//100;
                //static constexpr double DEFAULT_BALL_REPULSION = -90;
                //static constexpr double DEFAULT_BALL_TO_GOAL_NORMALIZED_RATIO = 5;
                static constexpr double DEFAULT_OPPONENT_REPULSION = 10.0;
                static constexpr double DEFAULT_TEAMMATE_REPULSION = 5.0;
                static constexpr double DEFAULT_FIELD_BORDER_REPULSION = 5.0;
                static constexpr double DEFAULT_TARGET_ATTRACTION = -10.0;
                //static constexpr double DEFAULT_THEIR_GOAL_ATTRACTION = -20.0;
                static constexpr double DEFAULT_DIPOLE_DISPLACEMENT = 0.5;
                static constexpr double DEFAULT_RANDOM_RATIO = 5.0;

                //const double & getBallAttraction();
                //const double & getBallRepulsion();
                //const double & getBallToGoalNormalizedRatio();
                const double &getOpponentRepulsion();

                const double &getTeammateRepulsion();

                const double &getFieldBorderRepulsion();

                const double &getTargetAttraction();

                //const double & getTheirGoalAttraction();
                const double &getDipoleDisplacement();

                const double &getRandomRatio();

            private:
                //const double BALL_ATTRACTION;
                //const double BALL_REPULSION;
                //const double BALL_TO_GOAL_NORMALIZED_RATIO;
                const double OPPONENT_REPULSION;
                const double TEAMMATE_REPULSION;
                const double FIELD_BORDER_REPULSION;
                const double TARGET_ATTRACTION;
                //const double THEIR_GOAL_ATTRACTION;
                const double DIPOLE_DISPLACEMENT;
                const double RANDOM_RATIO;
            };

        }
    }
}


#endif //ITANDROIDS_SOCCER3D_CPP_POTENTIALFIELDPARAMETERS_H
