/*
 * GyroRatePerceptor.cpp
 *
 *  Created on: Aug 8, 2015
 *      Author: mmaximo
 */

#include "perception/GyroRatePerceptor.h"

namespace perception {

    /**
     * This constructor takes a parser tree node and manipulates it to get information
     * of name and angular velocity in x, y and z coordinates.
     */
    GyroRatePerceptor::GyroRatePerceptor(utils::parser::ParserTreeNode &node) :
            Perceptor(node) {
        //First child gives the name
        std::vector<utils::parser::ParserTreeNode *> &children = node.children;
        name = utils::parser::KeyValuePair(children[0]->content, ' ').value;
        //Second child gives the angular velocity in x, y and z
        std::string angVelString = utils::parser::KeyValuePair(
                node.children[1]->content, ' ').value;
        std::vector<std::string> stringVector;
        boost::split(stringVector, angVelString, boost::is_any_of(" "));
        angularVelocity.x = MathUtils::degreesToRadians(
                boost::lexical_cast<double>(stringVector[0]));
        angularVelocity.y = MathUtils::degreesToRadians(
                boost::lexical_cast<double>(stringVector[1]));
        angularVelocity.z = MathUtils::degreesToRadians(
                boost::lexical_cast<double>(stringVector[2]));

    }

    GyroRatePerceptor::~GyroRatePerceptor() {
    }

    /*
     * Getter methods: returns name and angular velocity of gyro rate
     */

    std::string GyroRatePerceptor::getName() {
        return name;
    }

    Vector3<double> GyroRatePerceptor::getAngularVelocity() {
        return angularVelocity;
    }

} /* namespace perception */
