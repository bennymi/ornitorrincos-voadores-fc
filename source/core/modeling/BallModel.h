/*
 * BallModel.h
 *
 *  Created on: Aug 29, 2015
 *      Author: mmaximo
 */

#ifndef SOURCE_MODELING_BALLMODEL_H_
#define SOURCE_MODELING_BALLMODEL_H_

#include "math/Vector3.h"
#include "math/Pose2D.h"
#include "perception/VisibleBall.h"
#include "representations/Ball.h"
#include "modeling/state_estimation/BallTracker.h"

namespace modeling {

    using namespace itandroids_lib::math;
    using namespace representations;

    /**
    * Class that represents the ball model.
    */
    class BallModel {
    public:
        /**
         * Default constructor of BallModel.
         */
        BallModel();

        /**
         * Default destructor of BallModel.
         */
        virtual ~BallModel();

        /**
         * Updates the ball model.
         *
         * @param globalTime current global time.
         * @param visibleBall data from ball.
         * @param odometry information.
         * @param agentEstimate agent coordinates estimation in current cycle.
         */

        void update(double globalTime,
                    perception::VisibleBall *visibleBall,
                    Pose2D &odometry, Pose2D &agentEstimate);

        /**
         * Updates the ball model.
         *
         * @param globalTime current global time.
         * @param hear data from ball.
         * @param odometry information.
         * @param agentEstimate agent coordinates estimation in current cycle.
         */

        void update(double globalTime,
                    representations::HearData hearData, std::string ourTeamName,
                    Pose2D &odometry, Pose2D &agentEstimate);

        /**
         * Gets the ball.
         *
         * @return the ball.
         */

        Ball &getObject();

        /**
         * Gets the last global time update.
         *
         * @return the lastGlobalTime.
         */


    private:
        state_estimation::BallTracker tracker;
        double lastGlobalTime;
        double dt;
        Ball ball;
    };

} /* namespace modeling */

#endif /* SOURCE_MODELING_BALLMODEL_H_ */
