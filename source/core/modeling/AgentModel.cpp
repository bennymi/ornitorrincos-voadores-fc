/*
 * AgentModel.cpp
 *
 *  Created on: Aug 8, 2015
 *      Author: mmaximo
 */

#include "modeling/AgentModel.h"

namespace modeling {

    AgentModel::AgentModel() : rollAngle(0.0), pitchAngle(0.0),
                               agentPerception(nullptr), agentNumber(-1) {
        this->FORCE_THRESHOLD = 2.0;
        this->filteredAcceleration = Vector3<double>(0.0, 0.0, 0.0);
        this->ACCELERATION_FILTER_INNOVATION = 0.1;
    }

    AgentModel::AgentModel(int agentNumber) : rollAngle(0.0), pitchAngle(0.0),
                                              agentPerception(nullptr), agentNumber(agentNumber) {
        this->FORCE_THRESHOLD = 2.0;
        this->filteredAcceleration = Vector3<double>(0.0, 0.0, 0.0);
        this->ACCELERATION_FILTER_INNOVATION = 0.1;
    }

    AgentModel::~AgentModel() {
    }

    void AgentModel::update(perception::AgentPerception &agentPerception) {
        this->agentPerception = &agentPerception;
        NaoJoints joints = agentPerception.getNaoJoints(); // Get agent joints
        bodyModel.update(joints); // Update body model with the joints

        Pose3D torsoToSupportFoot;
        // Check which foot touchs the ground
        if (leftFootTouchsGround()) {
            torsoToSupportFoot = bodyModel.getLeftLegEndEffectorTransform();
        } else if (rightFootTouchsGround()) {
            torsoToSupportFoot = bodyModel.getRightLegEndEffectorTransform();
        } else {
            double zLeft = bodyModel.getLeftLegEndEffectorTransform().translation.z;
            double zRight = bodyModel.getRightLegEndEffectorTransform().translation.z;
            if (fabs(zLeft) > fabs(zRight))
                torsoToSupportFoot = bodyModel.getLeftLegEndEffectorTransform();
            else
                torsoToSupportFoot = bodyModel.getRightLegEndEffectorTransform();
        }

        Pose3D torsoFromRoot = torsoToSupportFoot.invert();
        torsoFromRoot.translation.x = 0.0;
        torsoFromRoot.translation.y = 0.0;
        double angle = torsoFromRoot.rotation.getZAngle();
        if (!std::isnan(angle))
            torsoFromRoot.rotation.rotateZ(-angle);

        // Transform the coordinate system from head to root
        fromCameraToRoot = torsoFromRoot * bodyModel.getTransformAtCenter(HEAD);

        // Simple transformation (used while the above one was not working correctly...)
//        fromCameraToRoot = bodyModel.getTransformAtCenter(HEAD);
//        fromCameraToRoot.translation.z -= torsoToSupportFoot.translation.z;

        Vector3<double> &acceleration = agentPerception.getTorsoAcceleration();
        rollAngle = atan2(acceleration.y, acceleration.z);

        pitchAngle = atan2(-acceleration.x,
                           sqrt(
                                   acceleration.y * acceleration.y
                                   + acceleration.z * acceleration.z));

        filteredAcceleration = acceleration * ACCELERATION_FILTER_INNOVATION +
                               filteredAcceleration * (1.0 - ACCELERATION_FILTER_INNOVATION);

        centerOfMass = bodyModel.computeCenterOfMass();
//        std::cout << "Center of mass: " << centerOfMass.x << " " << centerOfMass.y << " " << centerOfMass.z << std::endl;
    }

    const RotationMatrix &AgentModel::getTorsoRotation() {
        return torsoRotation;
    }

    const Vector3<double> &AgentModel::getTorsoAngularVelocity() {
        return agentPerception->getTorsoAngularVelocities();
    }

    const Vector3<double> &AgentModel::getTorsoAcceleration() {
        return agentPerception->getTorsoAcceleration();
    }

    const bool AgentModel::hasFallen() {
        return (hasFallenBack() || hasFallenFront() || hasFallenSide());
    }

    const bool AgentModel::hasFallenFront() {
        return (filteredAcceleration.y < -6.5); // A large acceleration means that the agent fell
    }

    const bool AgentModel::hasFallenBack() {
        return (filteredAcceleration.y > 6.5); // A large acceleration means that the agent fell
    }

    const bool AgentModel::hasFallenSide() {
        return (filteredAcceleration.x < -6.5) ||
               (filteredAcceleration.x > 6.5); // A large acceleration means that the agent fell
    }

    const Vector3<double> &AgentModel::getCenterOfMass() {
        return centerOfMass;
    }

    const Vector3<double> &AgentModel::getZeroMomentPoint() {
        return zeroMomentPoint;
    }

    bool AgentModel::leftFootTouchsGround() {
        return (agentPerception->getLeftFootForceResistanceData().getForce().z
                > FORCE_THRESHOLD);
    }

    bool AgentModel::rightFootTouchsGround() {
        return (agentPerception->getRightFootForceResistanceData().getForce().z
                > FORCE_THRESHOLD);
    }

    Pose3D &AgentModel::getFromCameraToRootTransform() {
        return fromCameraToRoot;
    }

    perception::AgentPerception &AgentModel::getAgentPerception() {
        return *agentPerception;
    }

    const int &AgentModel::getAgentNumber() {
        return agentNumber;
    }
} /* namespace modeling */
