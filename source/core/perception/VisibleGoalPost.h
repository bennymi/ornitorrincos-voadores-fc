/*
 * VisibleGoalPost.h
 *
 *  Created on: Sep 1, 2015
 *      Author: itandroids
 */

#ifndef SOURCE_PERCEPTION_VISIBLEGOALPOST_H_
#define SOURCE_PERCEPTION_VISIBLEGOALPOST_H_

#include "VisibleObject.h"
//#include "perception/GoalPostType.h"
#include "representations/GoalPostType.h"

namespace perception {

    class VisibleGoalPost : public VisibleObject {
    public:
        VisibleGoalPost();

        VisibleGoalPost(utils::parser::ParserTreeNode &);

        virtual ~VisibleGoalPost();

        representations::GoalPostType::GOAL_POST_TYPE &getGoalPostType();

    private:
        representations::GoalPostType::GOAL_POST_TYPE goalPostType;
    };

} /* namespace perception */

#endif /* SOURCE_PERCEPTION_VISIBLEGOALPOST_H_ */
