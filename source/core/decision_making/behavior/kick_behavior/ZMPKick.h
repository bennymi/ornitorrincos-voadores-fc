//
// Created by francisco on 6/20/16.
//

#ifndef ITANDROIDS_SOCCER3D_CPP_ZMPKICK_H
#define ITANDROIDS_SOCCER3D_CPP_ZMPKICK_H

#include "Kick.h"
#include "control/KickRequest.h"

namespace decision_making {
    namespace behavior {
        namespace kick_behavior {

            class ZMPKick : public Kick {
            public:
                ZMPKick();

                virtual void behave(modeling::Modeling &modeling) override;

            private:

            };
        }
    }
}


#endif //ITANDROIDS_SOCCER3D_CPP_ZMPKICK_H
