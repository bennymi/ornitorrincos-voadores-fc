//
// Created by francisco on 6/16/16.
//

#ifndef ITANDROIDS_SOCCER3D_CPP_KEYFRAMEKICK_H
#define ITANDROIDS_SOCCER3D_CPP_KEYFRAMEKICK_H

#include "Kick.h"

namespace decision_making {
    namespace behavior {
        namespace kick_behavior {

            class KeyframeKick : public Kick {
            public:
                KeyframeKick(std::string keyframeName);

                virtual ~KeyframeKick();

                virtual void behave(modeling::Modeling &modeling);

            private:
                std::string keyframeName;
            };
        }
    }
}


#endif //ITANDROIDS_SOCCER3D_CPP_KEYFRAMEKICK_H
