/*
 * DecisionMakingStub.h
 *
 *  Created on: Oct 1, 2015
 *      Author: mmaximo
 */

#ifndef TESTS_SOURCE_DECISION_MAKING_DECISIONMAKINGSTUB_H_
#define TESTS_SOURCE_DECISION_MAKING_DECISIONMAKINGSTUB_H_

#include "math/Vector3.h"
#include "decision_making/DecisionMaking.h"

namespace decision_making {

    using namespace itandroids_lib::math;

    class DecisionMakingStub : public DecisionMaking {
    public:
        std::shared_ptr<std::string> sayMessage;
        std::shared_ptr<Pose2D> beamRequest;
        std::shared_ptr<control::LookRequest> lookRequest;
        std::shared_ptr<control::MovementRequest> movementRequest;

        DecisionMakingStub();

        ~DecisionMakingStub();

        std::shared_ptr<std::string> getSayMessage();

        std::shared_ptr<control::LookRequest> getLookRequest();

        std::shared_ptr<control::MovementRequest> getMovementRequest();

        std::shared_ptr<Pose2D> getBeamRequest();

        bool decideFall(modeling::Modeling &modeling);
    };

} /* namespace decision_making */

#endif /* TESTS_SOURCE_DECISION_MAKING_DECISIONMAKINGSTUB_H_ */
