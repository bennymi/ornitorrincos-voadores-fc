/*
 * RawPlayMode.h
 *
 *  Created on: Aug 29, 2015
 *      Author: mmaximo
 */

#ifndef SOURCE_REPRESENTATIONS_RAWPLAYMODE_H_
#define SOURCE_REPRESENTATIONS_RAWPLAYMODE_H_

#include <iostream>

namespace representations {

/**
 * Raw play mode before taking into account the side
 * our team is playing.
 */
    class RawPlayMode {
    public:
        enum RAW_PLAY_MODE {
            BEFORE_KICK_OFF,
            KICK_OFF_LEFT,
            KICK_OFF_RIGHT,
            PLAY_ON,
            KICK_IN_LEFT,
            KICK_IN_RIGHT,
            CORNER_KICK_LEFT,
            CORNER_KICK_RIGHT,
            GOAL_KICK_LEFT,
            GOAL_KICK_RIGHT,
            OFFSIDE_LEFT,
            OFFSIDE_RIGHT,
            GAME_OVER,
            GOAL_LEFT,
            GOAL_RIGHT,
            FREE_KICK_LEFT,
            FREE_KICK_RIGHT,
            NONE
        };

        static std::string toString(representations::RawPlayMode::RAW_PLAY_MODE playMode) {
            std::string str;
            if (playMode == representations::RawPlayMode::BEFORE_KICK_OFF) {
                str = "BeforeKickOff";
            } else if (playMode == representations::RawPlayMode::KICK_OFF_LEFT) {
                str = "KickOff_Left";
            } else if (playMode == representations::RawPlayMode::KICK_OFF_RIGHT) {
                str = "KickOff_Right";
            } else if (playMode == representations::RawPlayMode::PLAY_ON) {
                str = "PlayOn";
            } else if (playMode == representations::RawPlayMode::KICK_IN_LEFT) {
                str = "KickIn_Left";
            } else if (playMode == representations::RawPlayMode::KICK_IN_RIGHT) {
                str = "KickIn_RIGHT";
            } else if (playMode == representations::RawPlayMode::CORNER_KICK_LEFT) {
                str = "corner_kick_left";
            } else if (playMode == representations::RawPlayMode::CORNER_KICK_RIGHT) {
                str = "corner_kick_right";
            } else if (playMode == representations::RawPlayMode::GOAL_KICK_LEFT) {
                str = "goal_kick_left";
            } else if (playMode == representations::RawPlayMode::GOAL_KICK_RIGHT) {
                str = "goal_kick_right";
            } else if (playMode == representations::RawPlayMode::OFFSIDE_LEFT) {
                str = "offside_left";
            } else if (playMode == representations::RawPlayMode::OFFSIDE_RIGHT) {
                str = "offside_right";
            } else if (playMode == representations::RawPlayMode::GAME_OVER) {
                str = "GameOver";
            } else if (playMode == representations::RawPlayMode::GOAL_LEFT) {
                str = "Goal_Left";
            } else if (playMode == representations::RawPlayMode::GOAL_RIGHT) {
                str = "Goal_Right";
            } else if (playMode == representations::RawPlayMode::FREE_KICK_LEFT) {
                str = "free_kick_left";
            } else if (playMode == representations::RawPlayMode::FREE_KICK_RIGHT) {
                str = "free_kick_right";
            } else {
                str = "NONE";
            }

            return str;
        }
    };
} /* namespace representations */

#endif /* SOURCE_REPRESENTATIONS_RAWPLAYMODE_H_ */
