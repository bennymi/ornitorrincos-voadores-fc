/*
 * NaoJoints.cpp
 *
 *  Created on: Aug 7, 2015
 *      Author: mmaximo
 */

#include <math.h>
#include "representations/NaoJoints.h"

namespace representations {

    NaoJoints::NaoJoints() {
        clear();
    }

    NaoJoints::NaoJoints(std::vector<perception::HingeJointPerceptor> perceptors) {
        clear();
        std::vector<perception::HingeJointPerceptor>::iterator iter;
        for (iter = perceptors.begin(); iter != perceptors.end(); iter++) {
            perception::HINGE_JOINTS joint = (*iter).getHingeJoint(
                    (*iter).getName());
            if (joint == perception::LEFT_ARM_ROLL)
                this->leftArmRoll = (*iter).getAngle();
            if (joint == perception::LEFT_ARM_YAW)
                this->leftArmYaw = (*iter).getAngle();
            if (joint == perception::LEFT_FOOT_PITCH)
                this->leftFootPitch = (*iter).getAngle();
            if (joint == perception::LEFT_FOOT_ROLL)
                this->leftFootRoll = (*iter).getAngle();
            if (joint == perception::LEFT_HIP_PITCH)
                this->leftHipPitch = (*iter).getAngle();
            if (joint == perception::LEFT_HIP_ROLL)
                this->leftHipRoll = (*iter).getAngle();
            if (joint == perception::LEFT_HIP_YAWPITCH)
                this->leftHipYawPitch = (*iter).getAngle();
            if (joint == perception::LEFT_KNEE_PITCH)
                this->leftKneePitch = (*iter).getAngle();
            if (joint == perception::LEFT_SHOULDER_PITCH)
                this->leftShoulderPitch = (*iter).getAngle();
            if (joint == perception::LEFT_SHOULDER_YAW)
                this->leftShoulderYaw = (*iter).getAngle();
            if (joint == perception::NECK_PITCH)
                this->neckPitch = (*iter).getAngle();
            if (joint == perception::NECK_YAW)
                this->neckYaw = (*iter).getAngle();
            if (joint == perception::RIGHT_ARM_ROLL)
                this->rightArmRoll = (*iter).getAngle();
            if (joint == perception::RIGHT_ARM_YAW)
                this->rightArmYaw = (*iter).getAngle();
            if (joint == perception::RIGHT_FOOT_PITCH)
                this->rightFootPitch = (*iter).getAngle();
            if (joint == perception::RIGHT_FOOT_ROLL)
                this->rightFootRoll = (*iter).getAngle();
            if (joint == perception::RIGHT_HIP_PITCH)
                this->rightHipPitch = (*iter).getAngle();
            if (joint == perception::RIGHT_HIP_ROLL)
                this->rightHipRoll = (*iter).getAngle();
            if (joint == perception::RIGHT_HIP_YAWPITCH)
                this->rightHipYawPitch = (*iter).getAngle();
            if (joint == perception::RIGHT_KNEE_PITCH)
                this->rightKneePitch = (*iter).getAngle();
            if (joint == perception::RIGHT_SHOULDER_PITCH)
                this->rightShoulderPitch = (*iter).getAngle();
            if (joint == perception::RIGHT_SHOULDER_YAW)
                this->rightShoulderYaw = (*iter).getAngle();
        }
    }

    NaoJoints::NaoJoints(std::vector<double> joints) {
        clear();
        for (int i = 0; i < joints.size(); i++) {
            setValue(static_cast<HINGE_JOINTS>(i), joints[i]);
        }
    }

    NaoJoints::~NaoJoints() {
    }

    void NaoJoints::assign(
            std::vector<perception::HingeJointPerceptor> &perceptors) {
        clear();
        std::vector<perception::HingeJointPerceptor>::iterator iter;
        for (iter = perceptors.begin(); iter != perceptors.end(); iter++) {
            perception::HINGE_JOINTS joint = (*iter).getHingeJoint(
                    (*iter).getName());
            if (joint == perception::LEFT_ARM_ROLL)
                this->leftArmRoll = (*iter).getAngle();
            if (joint == perception::LEFT_ARM_YAW)
                this->leftArmYaw = (*iter).getAngle();
            if (joint == perception::LEFT_FOOT_PITCH)
                this->leftFootPitch = (*iter).getAngle();
            if (joint == perception::LEFT_FOOT_ROLL)
                this->leftFootRoll = (*iter).getAngle();
            if (joint == perception::LEFT_HIP_PITCH)
                this->leftHipPitch = (*iter).getAngle();
            if (joint == perception::LEFT_HIP_ROLL)
                this->leftHipRoll = (*iter).getAngle();
            if (joint == perception::LEFT_HIP_YAWPITCH)
                this->leftHipYawPitch = (*iter).getAngle();
            if (joint == perception::LEFT_KNEE_PITCH)
                this->leftKneePitch = (*iter).getAngle();
            if (joint == perception::LEFT_SHOULDER_PITCH)
                this->leftShoulderPitch = (*iter).getAngle();
            if (joint == perception::LEFT_SHOULDER_YAW)
                this->leftShoulderYaw = (*iter).getAngle();
            if (joint == perception::NECK_PITCH)
                this->neckPitch = (*iter).getAngle();
            if (joint == perception::NECK_YAW)
                this->neckYaw = (*iter).getAngle();
            if (joint == perception::RIGHT_ARM_ROLL)
                this->rightArmRoll = (*iter).getAngle();
            if (joint == perception::RIGHT_ARM_YAW)
                this->rightArmYaw = (*iter).getAngle();
            if (joint == perception::RIGHT_FOOT_PITCH)
                this->rightFootPitch = (*iter).getAngle();
            if (joint == perception::RIGHT_FOOT_ROLL)
                this->rightFootRoll = (*iter).getAngle();
            if (joint == perception::RIGHT_HIP_PITCH)
                this->rightHipPitch = (*iter).getAngle();
            if (joint == perception::RIGHT_HIP_ROLL)
                this->rightHipRoll = (*iter).getAngle();
            if (joint == perception::RIGHT_HIP_YAWPITCH)
                this->rightHipYawPitch = (*iter).getAngle();
            if (joint == perception::RIGHT_KNEE_PITCH)
                this->rightKneePitch = (*iter).getAngle();
            if (joint == perception::RIGHT_SHOULDER_PITCH)
                this->rightShoulderPitch = (*iter).getAngle();
            if (joint == perception::RIGHT_SHOULDER_YAW)
                this->rightShoulderYaw = (*iter).getAngle();
        }
    }

    void NaoJoints::clear() {
        this->neckPitch = 0.0;
        this->neckYaw = 0.0;

        this->leftShoulderPitch = 0.0;
        this->leftShoulderYaw = 0.0;
        this->leftArmRoll = 0.0;
        this->leftArmYaw = 0.0;

        this->rightShoulderPitch = 0.0;
        this->rightShoulderYaw = 0.0;
        this->rightArmRoll = 0.0;
        this->rightArmYaw = 0.0;

        this->leftHipYawPitch = 0.0;
        this->leftHipRoll = 0.0;
        this->leftHipPitch = 0.0;
        this->leftKneePitch = 0.0;
        this->leftFootPitch = 0.0;
        this->leftFootRoll = 0.0;

        this->rightHipYawPitch = 0.0;
        this->rightHipRoll = 0.0;
        this->rightHipPitch = 0.0;
        this->rightKneePitch = 0.0;
        this->rightFootPitch = 0.0;
        this->rightFootRoll = 0.0;
    }

    NaoJoints NaoJoints::operator+(const NaoJoints &other) {
        NaoJoints result;

        result.neckPitch = this->neckPitch + other.neckPitch;
        result.neckYaw = this->neckYaw + other.neckYaw;

        result.leftShoulderPitch = this->leftShoulderPitch
                                   + other.leftShoulderPitch;
        result.leftShoulderYaw = this->leftShoulderYaw + other.leftShoulderYaw;
        result.leftArmRoll = this->leftArmRoll + other.leftArmRoll;
        result.leftArmYaw = this->leftArmYaw + other.leftArmYaw;

        result.rightShoulderPitch = this->rightShoulderPitch
                                    + other.rightShoulderPitch;
        result.rightShoulderYaw = this->rightShoulderYaw + other.rightShoulderYaw;
        result.rightArmRoll = this->rightArmRoll + other.rightArmRoll;
        result.rightArmYaw = this->rightArmYaw + other.rightArmYaw;

        result.leftHipYawPitch = this->leftHipYawPitch + other.leftHipYawPitch;
        result.leftHipRoll = this->leftHipRoll + other.leftHipRoll;
        result.leftHipPitch = this->leftHipPitch + other.leftHipPitch;
        result.leftKneePitch = this->leftKneePitch + other.leftKneePitch;
        result.leftFootPitch = this->leftFootPitch + other.leftFootPitch;
        result.leftFootRoll = this->leftFootRoll + other.leftFootRoll;

        result.rightHipYawPitch = this->rightHipYawPitch + other.rightHipYawPitch;
        result.rightHipRoll = this->rightHipRoll + other.rightHipRoll;
        result.rightHipPitch = this->rightHipPitch + other.rightHipPitch;
        result.rightKneePitch = this->rightKneePitch + other.rightKneePitch;
        result.rightFootPitch = this->rightFootPitch + other.rightFootPitch;
        result.rightFootRoll = this->rightFootRoll + other.rightFootRoll;

        return result;
    }

    NaoJoints NaoJoints::operator-(const NaoJoints &other) {
        NaoJoints result;

        result.neckPitch = this->neckPitch - other.neckPitch;
        result.neckYaw = this->neckYaw - other.neckYaw;

        result.leftShoulderPitch = this->leftShoulderPitch
                                   - other.leftShoulderPitch;
        result.leftShoulderYaw = this->leftShoulderYaw - other.leftShoulderYaw;
        result.leftArmRoll = this->leftArmRoll - other.leftArmRoll;
        result.leftArmYaw = this->leftArmYaw - other.leftArmYaw;

        result.rightShoulderPitch = this->rightShoulderPitch
                                    - other.rightShoulderPitch;
        result.rightShoulderYaw = this->rightShoulderYaw - other.rightShoulderYaw;
        result.rightArmRoll = this->rightArmRoll - other.rightArmRoll;
        result.rightArmYaw = this->rightArmYaw - other.rightArmYaw;

        result.leftHipYawPitch = this->leftHipYawPitch - other.leftHipYawPitch;
        result.leftHipRoll = this->leftHipRoll - other.leftHipRoll;
        result.leftHipPitch = this->leftHipPitch - other.leftHipPitch;
        result.leftKneePitch = this->leftKneePitch - other.leftKneePitch;
        result.leftFootPitch = this->leftFootPitch - other.leftFootPitch;
        result.leftFootRoll = this->leftFootRoll - other.leftFootRoll;

        result.rightHipYawPitch = this->rightHipYawPitch - other.rightHipYawPitch;
        result.rightHipRoll = this->rightHipRoll - other.rightHipRoll;
        result.rightHipPitch = this->rightHipPitch - other.rightHipPitch;
        result.rightKneePitch = this->rightKneePitch - other.rightKneePitch;
        result.rightFootPitch = this->rightFootPitch - other.rightFootPitch;
        result.rightFootRoll = this->rightFootRoll - other.rightFootRoll;

        return result;
    }


    NaoJoints NaoJoints::operator*(const double scalar) {
        NaoJoints result;

        result.neckPitch = this->neckPitch * scalar;
        result.neckYaw = this->neckYaw * scalar;

        result.leftShoulderPitch = this->leftShoulderPitch
                                   * scalar;
        result.leftShoulderYaw = this->leftShoulderYaw * scalar;
        result.leftArmRoll = this->leftArmRoll * scalar;
        result.leftArmYaw = this->leftArmYaw * scalar;

        result.rightShoulderPitch = this->rightShoulderPitch
                                    * scalar;
        result.rightShoulderYaw = this->rightShoulderYaw * scalar;
        result.rightArmRoll = this->rightArmRoll * scalar;
        result.rightArmYaw = this->rightArmYaw * scalar;

        result.leftHipYawPitch = this->leftHipYawPitch * scalar;
        result.leftHipRoll = this->leftHipRoll * scalar;
        result.leftHipPitch = this->leftHipPitch * scalar;
        result.leftKneePitch = this->leftKneePitch * scalar;
        result.leftFootPitch = this->leftFootPitch * scalar;
        result.leftFootRoll = this->leftFootRoll * scalar;

        result.rightHipYawPitch = this->rightHipYawPitch * scalar;
        result.rightHipRoll = this->rightHipRoll * scalar;
        result.rightHipPitch = this->rightHipPitch * scalar;
        result.rightKneePitch = this->rightKneePitch * scalar;
        result.rightFootPitch = this->rightFootPitch * scalar;
        result.rightFootRoll = this->rightFootRoll * scalar;

        return result;
    }

    NaoJoints NaoJoints::operator*(const NaoJoints &other) {
        NaoJoints result;

        result.neckPitch = this->neckPitch * other.neckPitch;
        result.neckYaw = this->neckYaw * other.neckYaw;

        result.leftShoulderPitch = this->leftShoulderPitch
                                   * other.leftShoulderPitch;
        result.leftShoulderYaw = this->leftShoulderYaw * other.leftShoulderYaw;
        result.leftArmRoll = this->leftArmRoll * other.leftArmRoll;
        result.leftArmYaw = this->leftArmYaw * other.leftArmYaw;

        result.rightShoulderPitch = this->rightShoulderPitch
                                    * other.rightShoulderPitch;
        result.rightShoulderYaw = this->rightShoulderYaw * other.rightShoulderYaw;
        result.rightArmRoll = this->rightArmRoll * other.rightArmRoll;
        result.rightArmYaw = this->rightArmYaw * other.rightArmYaw;

        result.leftHipYawPitch = this->leftHipYawPitch * other.leftHipYawPitch;
        result.leftHipRoll = this->leftHipRoll * other.leftHipRoll;
        result.leftHipPitch = this->leftHipPitch * other.leftHipPitch;
        result.leftKneePitch = this->leftKneePitch * other.leftKneePitch;
        result.leftFootPitch = this->leftFootPitch * other.leftFootPitch;
        result.leftFootRoll = this->leftFootRoll * other.leftFootRoll;

        result.rightHipYawPitch = this->rightHipYawPitch * other.rightHipYawPitch;
        result.rightHipRoll = this->rightHipRoll * other.rightHipRoll;
        result.rightHipPitch = this->rightHipPitch * other.rightHipPitch;
        result.rightKneePitch = this->rightKneePitch * other.rightKneePitch;
        result.rightFootPitch = this->rightFootPitch * other.rightFootPitch;
        result.rightFootRoll = this->rightFootRoll * other.rightFootRoll;

        return result;
    }

    NaoJoints &NaoJoints::operator*=(const NaoJoints &other) {
        neckPitch *= other.neckPitch;
        neckYaw *= other.neckYaw;

        leftShoulderPitch *= other.leftShoulderPitch;
        leftShoulderYaw *= other.leftShoulderYaw;
        leftArmRoll *= other.leftArmRoll;
        leftArmYaw *= other.leftArmYaw;

        rightShoulderPitch *= other.rightShoulderPitch;
        rightShoulderYaw *= other.rightShoulderYaw;
        rightArmRoll *= other.rightArmRoll;
        rightArmYaw *= other.rightArmYaw;

        leftHipYawPitch *= other.leftHipYawPitch;
        leftHipRoll *= other.leftHipRoll;
        leftHipPitch *= other.leftHipPitch;
        leftKneePitch *= other.leftKneePitch;
        leftFootPitch *= other.leftFootPitch;
        leftFootRoll *= other.leftFootRoll;

        rightHipYawPitch *= other.rightHipYawPitch;
        rightHipRoll *= other.rightHipRoll;
        rightHipPitch *= other.rightHipPitch;
        rightKneePitch *= other.rightKneePitch;
        rightFootPitch *= other.rightFootPitch;
        rightFootRoll *= other.rightFootRoll;

        return *this;
    }

    void NaoJoints::scale(double scalar) {
        this->neckPitch *= scalar;
        this->neckYaw *= scalar;

        this->leftShoulderPitch *= scalar;
        this->leftShoulderYaw *= scalar;
        this->leftArmRoll *= scalar;
        this->leftArmYaw *= scalar;

        this->rightShoulderPitch *= scalar;
        this->rightShoulderYaw *= scalar;
        this->rightArmRoll *= scalar;
        this->rightArmYaw *= scalar;

        this->leftHipYawPitch *= scalar;
        this->leftHipRoll *= scalar;
        this->leftHipPitch *= scalar;
        this->leftKneePitch *= scalar;
        this->leftFootPitch *= scalar;
        this->leftFootRoll *= scalar;

        this->rightHipYawPitch *= scalar;
        this->rightHipRoll *= scalar;
        this->rightHipPitch *= scalar;
        this->rightKneePitch *= scalar;
        this->rightFootPitch *= scalar;
        this->rightFootRoll *= scalar;
    }

    double NaoJoints::lInfiniteNorm() {
        double norm = 0.0f;

        norm = std::max(fabs(leftShoulderPitch), norm);
        norm = std::max(fabs(leftShoulderYaw), norm);
        norm = std::max(fabs(leftArmRoll), norm);
        norm = std::max(fabs(leftArmYaw), norm);

        norm = std::max(fabs(rightShoulderPitch), norm);
        norm = std::max(fabs(rightShoulderYaw), norm);
        norm = std::max(fabs(rightArmRoll), norm);
        norm = std::max(fabs(rightArmYaw), norm);

        norm = std::max(fabs(leftHipYawPitch), norm);
        norm = std::max(fabs(leftHipRoll), norm);
        norm = std::max(fabs(leftHipPitch), norm);
        norm = std::max(fabs(leftKneePitch), norm);
        norm = std::max(fabs(leftFootPitch), norm);
        norm = std::max(fabs(leftFootRoll), norm);

        norm = std::max(fabs(rightHipYawPitch), norm);
        norm = std::max(fabs(rightHipRoll), norm);
        norm = std::max(fabs(rightHipPitch), norm);
        norm = std::max(fabs(rightKneePitch), norm);
        norm = std::max(fabs(rightFootPitch), norm);
        norm = std::max(fabs(rightFootRoll), norm);

        return norm;
    }

    double NaoJoints::getValue(HINGE_JOINTS joint) {
        switch (joint) {
            case NECK_YAW:
                return neckYaw;
            case NECK_PITCH:
                return neckPitch;
            case LEFT_SHOULDER_PITCH:
                return leftShoulderPitch;
            case LEFT_SHOULDER_YAW:
                return leftShoulderYaw;
            case LEFT_ARM_ROLL:
                return leftArmRoll;
            case LEFT_ARM_YAW:
                return leftArmYaw;
            case LEFT_HIP_YAWPITCH:
                return leftHipYawPitch;
            case LEFT_HIP_ROLL:
                return leftHipRoll;
            case LEFT_HIP_PITCH:
                return leftHipPitch;
            case LEFT_KNEE_PITCH:
                return leftKneePitch;
            case LEFT_FOOT_PITCH:
                return leftFootPitch;
            case LEFT_FOOT_ROLL:
                return leftFootRoll;
            case RIGHT_SHOULDER_PITCH:
                return rightShoulderPitch;
            case RIGHT_SHOULDER_YAW:
                return rightShoulderYaw;
            case RIGHT_ARM_ROLL:
                return rightArmRoll;
            case RIGHT_ARM_YAW:
                return rightArmYaw;
            case RIGHT_HIP_YAWPITCH:
                return rightHipYawPitch;
            case RIGHT_HIP_ROLL:
                return rightHipRoll;
            case RIGHT_HIP_PITCH:
                return rightHipPitch;
            case RIGHT_KNEE_PITCH:
                return rightKneePitch;
            case RIGHT_FOOT_PITCH:
                return rightFootPitch;
            case RIGHT_FOOT_ROLL:
                return rightFootRoll;
        }
        return 0.0;
    }

    void NaoJoints::setValue(HINGE_JOINTS joint, double value) {
        switch (joint) {
            case NECK_YAW:
                neckYaw = value;
                break;
            case NECK_PITCH:
                neckPitch = value;
                break;
            case LEFT_SHOULDER_PITCH:
                leftShoulderPitch = value;
                break;
            case LEFT_SHOULDER_YAW:
                leftShoulderYaw = value;
                break;
            case LEFT_ARM_ROLL:
                leftArmRoll = value;
                break;
            case LEFT_ARM_YAW:
                leftArmYaw = value;
                break;
            case LEFT_HIP_YAWPITCH:
                leftHipYawPitch = value;
                break;
            case LEFT_HIP_ROLL:
                leftHipRoll = value;
                break;
            case LEFT_HIP_PITCH:
                leftHipPitch = value;
                break;
            case LEFT_KNEE_PITCH:
                leftKneePitch = value;
                break;
            case LEFT_FOOT_PITCH:
                leftFootPitch = value;
                break;
            case LEFT_FOOT_ROLL:
                leftFootRoll = value;
                break;
            case RIGHT_SHOULDER_PITCH:
                rightShoulderPitch = value;
                break;
            case RIGHT_SHOULDER_YAW:
                rightShoulderYaw = value;
                break;
            case RIGHT_ARM_ROLL:
                rightArmRoll = value;
                break;
            case RIGHT_ARM_YAW:
                rightArmYaw = value;
                break;
            case RIGHT_HIP_YAWPITCH:
                rightHipYawPitch = value;
                break;
            case RIGHT_HIP_ROLL:
                rightHipRoll = value;
                break;
            case RIGHT_HIP_PITCH:
                rightHipPitch = value;
                break;
            case RIGHT_KNEE_PITCH:
                rightKneePitch = value;
                break;
            case RIGHT_FOOT_PITCH:
                rightFootPitch = value;
                break;
            case RIGHT_FOOT_ROLL:
                rightFootRoll = value;
                break;
        }
    }

    std::map<NaoJoints::HINGE_JOINTS, double> NaoJoints::getAsMap() {
        std::map<HINGE_JOINTS, double> map;
        map[NECK_YAW] = neckYaw;
        map[NECK_PITCH] = neckPitch;
        map[LEFT_SHOULDER_PITCH] = leftShoulderPitch;
        map[LEFT_SHOULDER_YAW] = leftShoulderYaw;
        map[LEFT_ARM_ROLL] = leftArmRoll;
        map[LEFT_ARM_YAW] = leftArmYaw;
        map[LEFT_HIP_YAWPITCH] = leftHipYawPitch;
        map[LEFT_HIP_ROLL] = leftHipRoll;
        map[LEFT_HIP_PITCH] = leftHipPitch;
        map[LEFT_KNEE_PITCH] = leftKneePitch;
        map[LEFT_FOOT_PITCH] = leftFootPitch;
        map[LEFT_FOOT_ROLL] = leftFootRoll;
        map[RIGHT_SHOULDER_PITCH] = rightShoulderPitch;
        map[RIGHT_SHOULDER_YAW] = rightShoulderYaw;
        map[RIGHT_ARM_ROLL] = rightArmRoll;
        map[RIGHT_ARM_YAW] = rightArmYaw;
        map[RIGHT_HIP_YAWPITCH] = rightHipYawPitch;
        map[RIGHT_HIP_ROLL] = rightHipRoll;
        map[RIGHT_HIP_PITCH] = rightHipPitch;
        map[RIGHT_KNEE_PITCH] = rightKneePitch;
        map[RIGHT_FOOT_PITCH] = rightFootPitch;
        map[RIGHT_FOOT_ROLL] = rightFootRoll;
        return map;
    }


    NaoJoints NaoJoints::getNaoDirectionsFixing() {
        NaoJoints joints;


        joints.leftShoulderPitch = -1.0f;
        joints.leftShoulderYaw = 1.0f;
        joints.leftArmRoll = 1.0f;
        joints.leftArmYaw = 1.0f;


        joints.leftHipYawPitch = -1.0f;
        joints.leftHipRoll = 1.0f;
        joints.leftHipPitch = -1.0f;
        joints.leftKneePitch = -1.0f;
        joints.leftFootPitch = -1.0f;
        joints.leftFootRoll = 1.0f;

        joints.rightShoulderPitch = -1.0f;
        joints.rightShoulderYaw = 1.0f;
        joints.rightArmRoll = 1.0f;
        joints.rightArmYaw = 1.0f;

        joints.rightHipYawPitch = 1.0f;
        joints.rightHipRoll = 1.0f;
        joints.rightHipPitch = -1.0f;
        joints.rightKneePitch = -1.0f;
        joints.rightFootPitch = -1.0f;
        joints.rightFootRoll = 1.0f;

        return joints;
    }

    std::string NaoJoints::toString() {
        std::string str = "neckYaw:" + boost::lexical_cast<std::string>(neckYaw)
                          + "\n" + "neckPitch:" + boost::lexical_cast<std::string>(neckPitch)
                          + "\n" + "leftShoulderPitch:"
                          + boost::lexical_cast<std::string>(leftShoulderPitch) + "\n"
                          + "leftShoulderYaw:"
                          + boost::lexical_cast<std::string>(leftShoulderYaw) + "\n"
                          + "leftArmRoll:" + boost::lexical_cast<std::string>(leftArmRoll)
                          + "\n" + "leftArmYaw:"
                          + boost::lexical_cast<std::string>(leftArmYaw) + "\n"
                          + "leftHipYawPitch:"
                          + boost::lexical_cast<std::string>(leftHipYawPitch) + "\n"
                          + "leftHipRoll:" + boost::lexical_cast<std::string>(leftHipRoll)
                          + "\n" + "leftHipPitch:"
                          + boost::lexical_cast<std::string>(leftHipPitch) + "\n"
                          + "leftKneePitch:" + boost::lexical_cast<std::string>(leftKneePitch)
                          + "\n" + "leftFootPitch:"
                          + boost::lexical_cast<std::string>(leftFootPitch) + "\n"
                          + "leftFootRoll:" + boost::lexical_cast<std::string>(leftFootRoll)
                          + "\n" + "rightShoulderPitch:"
                          + boost::lexical_cast<std::string>(rightShoulderPitch) + "\n"
                          + "rightShoulderYaw: "
                          + boost::lexical_cast<std::string>(rightShoulderYaw) + "\n"
                          + "rightArmRoll:" + boost::lexical_cast<std::string>(rightArmRoll)
                          + "\n" + "rightArmYaw:"
                          + boost::lexical_cast<std::string>(rightArmYaw) + "\n"
                          + "rightHipYawPitch:"
                          + boost::lexical_cast<std::string>(rightHipYawPitch) + "\n"
                          + "rightHipRoll:" + boost::lexical_cast<std::string>(rightHipRoll)
                          + "\n" + "rightHipPitch:"
                          + boost::lexical_cast<std::string>(rightHipPitch) + "\n"
                          + "rightKneePitch:"
                          + boost::lexical_cast<std::string>(rightKneePitch) + "\n"
                          + "rightFootPitch:"
                          + boost::lexical_cast<std::string>(rightFootPitch) + "\n"
                          + "rightFootRoll:" + boost::lexical_cast<std::string>(rightFootRoll)
                          + "\n";
        return str;
    }

    std::string NaoJoints::toSimpleString() {
        std::stringstream str;
        auto map = getAsMap();
        for (auto &k : map) {
            str << k.second << " ";
        }
        return str.str();
    }

    NaoJoints NaoJoints::getSimetricPositions(NaoJoints joints) {
        NaoJoints retJoints;

        retJoints.rightArmRoll = joints.leftArmRoll;
        retJoints.rightArmYaw = joints.leftArmYaw;
        retJoints.rightFootPitch = joints.leftFootPitch;
        retJoints.rightFootRoll = -joints.leftFootRoll;
        retJoints.rightHipPitch = joints.leftHipPitch;
        retJoints.rightHipRoll = -joints.leftHipRoll;
        retJoints.rightHipYawPitch = -joints.leftHipYawPitch;
        retJoints.rightKneePitch = joints.leftKneePitch;
        retJoints.rightShoulderPitch = joints.leftShoulderPitch;
        retJoints.rightShoulderYaw = joints.leftShoulderYaw;
        retJoints.leftArmRoll = joints.rightArmRoll;
        retJoints.leftArmYaw = joints.rightArmYaw;
        retJoints.leftFootPitch = joints.rightFootPitch;
        retJoints.leftFootRoll = -joints.rightFootRoll;
        retJoints.leftHipPitch = joints.rightHipPitch;
        retJoints.leftHipRoll = -joints.rightHipRoll;
        retJoints.leftHipYawPitch = -joints.rightHipYawPitch;
        retJoints.leftKneePitch = joints.rightKneePitch;
        retJoints.leftShoulderPitch = joints.rightShoulderPitch;
        retJoints.leftShoulderYaw = -joints.rightShoulderYaw;

        return retJoints;
    }

} /* namespace representations */

