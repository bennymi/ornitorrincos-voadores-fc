//
// Created by dicksiano on 27/05/16.
//

#ifndef ITANDROIDS_SOCCER3D_CPP_GAMESITUATION_H
#define ITANDROIDS_SOCCER3D_CPP_GAMESITUATION_H


namespace representations {

    class GameSituation {
    public:
        enum GAME_SITUATION {
            ATTACKING, DEFENDING,
        };

    };
} /* namespace representations */

#endif //ITANDROIDS_SOCCER3D_CPP_GAMESITUATION_H
