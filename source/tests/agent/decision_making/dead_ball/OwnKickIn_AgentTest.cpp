//
// Created by dicksiano on 8/21/16.
//

#include "base/BaseAgentTest.h"

class OwnKickIn_AgentTest : public BaseAgentTest {

public:

    virtual void testLoop(int nSteps) { BaseAgentTest::testLoop(nSteps); }


    virtual void setup() {
        wiz.setPlayMode(representations::RawPlayMode::KICK_IN_LEFT);
        //wiz.setBallPosition(Vector3<double >(-3.0, +10.0, 0.0));
    }

    virtual void runStep() {
        wiz.setPlayMode(representations::RawPlayMode::KICK_IN_LEFT);
        //wiz.setBallPosition(Vector3<double >(-3.0, +10.0, 0.0));

        utils::roboviz::Roboviz roboviz1;
        std::string name1("animated");
        std::string buff1("animated");
        roboviz1.swapBuffers(&buff1);

        Pose2D selfPosition = modeling.getWorldModel().getSelf().getPosition();
        roboviz1.drawLine(selfPosition.translation.x, selfPosition.translation.y, 0,
                          selfPosition.translation.x, selfPosition.translation.y, 1,
                          1, 20, 20, 20, &name1);

        Pose2D ballPosition = modeling.getWorldModel().getBall().getPosition();
        Pose2D theirGoalPosition = modeling.getWorldModel().getTheirGoalPosition();

        Pose2D vecPosition = (theirGoalPosition - ballPosition);
        vecPosition = vecPosition * (2.0 / vecPosition.abs());

        Pose2D strategicPosition = ballPosition + vecPosition;
        roboviz1.drawLine(strategicPosition.translation.x, strategicPosition.translation.y, 0,
                          strategicPosition.translation.x, strategicPosition.translation.y, 1,
                          1, 20, 200, 100, &name1);

        if (!decisionMaking.decideFall(modeling)) {
            behaviorFactory.getSetPlay().behave(modeling); // Behavior!
            decisionMaking.movementRequest = behaviorFactory.getSetPlay().getMovementRequest();

            behaviorFactory.getFocusBall().behave(modeling); // Vision!
            decisionMaking.lookRequest = behaviorFactory.getFocusBall().getLookRequest();
        }
    }
};

int main() {

    /**
     * Start the external agent
     */
    std::vector<std::thread> threads;
    for (int i = 2; i < 2; i++) {
        std::stringstream stream;
        stream << "./DeadBall_AgentTest 127.0.0.1 3100 3200 " << i << " &";
        system(stream.str().c_str());
        stream.str("");
        stream << "sleep 5";
        system(stream.str().c_str());
        std::cout << "Dead Ball agent n " << i << std::endl;
    }

    OwnKickIn_AgentTest agent; // Agent test
    agent.testLoop(50000); // Run the test!
    return 0;

}