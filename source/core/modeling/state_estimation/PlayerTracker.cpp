/*
 * PlayerTracker.cpp
 *
 *  Created on: Aug 29, 2015
 *      Author: mmaximo
 */

#include "PlayerTracker.h"

namespace modeling {
    namespace state_estimation_player {

        PlayerTracker::PlayerTracker() {

            Eigen::MatrixXd A(4, 4);
            Eigen::MatrixXd B(4, 2);
            Eigen::MatrixXd C(2, 4);
            Eigen::MatrixXd Q(4, 4);
            Eigen::MatrixXd W(2, 2);

            double dt = 0.02;
            A << 1, 0, dt, 0,
                    0, 1, 0, dt,
                    0, 0, 1, 0,
                    0, 0, 0, 1;
            B << (dt * dt / 2), 0,
                    0, (dt * dt / 2),
                    dt, 0,
                    0, dt;
//            B<<-1,0,
//                    0,-1,
//                    0,0,
//                    0,0;
            C << 1, 0, 0, 0,
                    0, 1, 0, 0;
            Q << 4e-8, 0, 4e-06, 0,
                    0, 4e-8, 0, 4e-6,
                    4e-6, 0, 0.0004, 0,
                    0, 4e-6, 0, 0.0004;
            Q = Q * 1e3;
            W << 0.4, 0,
                    0, 0.4;
            stochasticModel1 = LinearDiscreteStochasticModel(A, B, Q, C, W);

            Eigen::VectorXd initialState(4);
            initialState << 0, 0, 0, 0;
            Eigen::MatrixXd initialCovariance(4, 4);
            initialCovariance << 10, 0, 0, 0,
                    0, 10, 0, 0,
                    0, 0, 10, 0,
                    0, 0, 0, 10;
            lostPlayer = true;
            counterNotViewPlayer = 0;
            KF1 = LinearKalmanFilter(stochasticModel1, initialState, initialCovariance);
        }

        Pose2D PlayerTracker::getPosition() {
            return Pose2D(KF1.getStateEstimative()[0], KF1.getStateEstimative()[1]);
        }

        Pose2D PlayerTracker::getVelocity() {
            return Pose2D(KF1.getStateEstimative()[2], KF1.getStateEstimative()[3]);
        }

        PlayerTracker::~PlayerTracker() {
        }

        void PlayerTracker::update(double globalTime,
                                   perception::VisiblePlayer *visiblePlayer,
                                   Pose2D &odometry) {
            counterNotViewPlayer = 0;
            Eigen::VectorXd u(2);
            Eigen::VectorXd z(2);
            u << 0, 0;
            z << visiblePlayer->getPosition().x, visiblePlayer->getPosition().y;
            //std::cout << "Tá vendo by bafo véi" << std::endl;
            //std::cout << visiblePlayer->getPosition().x << "  " << visiblePlayer->getPosition().y << std::endl;
            if (lostPlayer) {
                KF1.resetFromObservation(z);
                lostPlayer = false;
            }
            Eigen::MatrixXd rot(4, 4);
            double rotation = odometry.rotation;
            rot << cos(rotation), sin(rotation), 0, 0,
                    sin(-1 * rotation), cos(rotation), 0, 0,
                    0, 0, cos(rotation), sin(rotation),
                    0, 0, sin(-1 * rotation), cos(rotation);
            Eigen::VectorXd disp(4);
            disp << (-1) * odometry.translation.x, (-1) * odometry.translation.y, 0, 0;
            KF1.referentialChange(rot, disp);
            KF1.prediction(u);
            KF1.filtering(z);
            //std::cout << KF1.getStateEstimative()[2] << " " << KF1.getStateEstimative()[3] << std::endl;
        }

        void PlayerTracker::update(double globalTime, const Pose2D &odometry) {
            if (counterNotViewPlayer >= 100) {
                resetEstimate();
                lostPlayer = true;
            }
            if (!lostPlayer) {
                counterNotViewPlayer += 1;
                Eigen::VectorXd u(2);
                u << 0, 0;
                Eigen::MatrixXd rot(4, 4);
                double rotation = odometry.rotation;
                rot << cos(rotation), sin(rotation), 0, 0,
                        sin(-1 * rotation), cos(rotation), 0, 0,
                        0, 0, cos(rotation), sin(rotation),
                        0, 0, sin(-1 * rotation), cos(rotation);
                Eigen::VectorXd disp(4);
                disp << (-1) * odometry.translation.x, (-1) * odometry.translation.y, 0, 0;
                KF1.referentialChange(rot, disp);
                KF1.prediction(u);
            }
        }

        bool PlayerTracker::hasLostPlayer() {
            return lostPlayer;
        }

        void PlayerTracker::resetEstimate() {
            Eigen::VectorXd resetState(4);
            resetState << 0, 0, 0, 0;
            Eigen::MatrixXd resetCovariance(4, 4);
            resetCovariance << 10, 0, 0, 0,
                    0, 10, 0, 0,
                    0, 0, 10, 0,
                    0, 0, 0, 10;
            KF1.resetFromState(resetState, resetCovariance);
        }
    } /* namespace state_estimation */
} /* namespace modeling */
