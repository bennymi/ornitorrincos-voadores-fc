/*
 * DelaunayFormationType.h
 *
 *  Created on: Oct 28, 2015
 *      Author: itandroids
 */

#ifndef SOURCE_DECISION_MAKING_POSITIONING_DELAUNAY_TRIANGULATION_DELAUNAYFORMATIONTYPE_H_
#define SOURCE_DECISION_MAKING_POSITIONING_DELAUNAY_TRIANGULATION_DELAUNAYFORMATIONTYPE_H_

#include <string>

namespace decision_making {
    namespace positioning {
        namespace delaunay_triangulation {

            class DelaunayFormationType {
            public:
                enum DELAUNAY_FORMATION_TYPE {
                    DEFAULT_FORMATION, NUM_FORMATIONS,
                };

                static std::string toString(DELAUNAY_FORMATION_TYPE type) {
                    if (type == DEFAULT_FORMATION) {
                        return std::string("default-formation");
                    } else {
                        return std::string("unknown-formation");
                    }
                }

                static DelaunayFormationType::DELAUNAY_FORMATION_TYPE stringToType(
                        std::string name) {
                    if (name == "default-formation") {
                        return DEFAULT_FORMATION;
                    }
                }
            };
        }
    }
}

#endif /* SOURCE_DECISION_MAKING_POSITIONING_DELAUNAY_TRIANGULATION_DELAUNAYFORMATIONTYPE_H_ */
