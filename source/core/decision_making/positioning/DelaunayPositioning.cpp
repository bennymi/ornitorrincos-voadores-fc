//
// Created by dicksiano on 27/05/16.
//

#include "DelaunayPositioning.h"

namespace decision_making {
    namespace positioning {

        DelaunayPositioning::DelaunayPositioning(std::string folderpath)
                : formations(folderpath) {
        }

        DelaunayPositioning::~DelaunayPositioning() {
        }

        itandroids_lib::math::Pose2D DelaunayPositioning::getAgentPosition(modeling::Modeling &modeling) {

            int agentNum = modeling.getAgentModel().getAgentNumber(); // Check the agent number.
            representations::PlayMode::PLAY_MODE playMode = modeling.getWorldModel().getPlayMode(); // Check the current Play Mode.

            std::shared_ptr<delaunay_triangulation::DelaunayInterpolator> dI;
            dI = formations.getTriangulations()[delaunay_triangulation::DelaunayFormationType::DEFAULT_FORMATION];

            itandroids_lib::math::Pose2D ballPose = modeling.getWorldModel().getBall().getPosition();

            std::vector<itandroids_lib::math::Vector2<double>> v = dI->interpolate(ballPose.translation);
            if (v.size() < modeling::WorldModel::NUM_PLAYERS_PER_TEAM) {
                return modeling.getWorldModel().getSelf().getPosition();
            } else {
                return v[agentNum - 1];
            }
        }

        std::vector<itandroids_lib::math::Vector2<double> > DelaunayPositioning::getTeamPosition(
                modeling::Modeling &modeling) {
            /*role vector:
            * 0 - goalie;
            * 1 - backRight, 2 - backLeft, 3 - mid
            * 4 - wingLeft, 5 - wingRight, 6 - supporter
            * 7 - fowardLeft, 8 - fowardRight, 9 - onBall, 10 - fowardCenter
            */
            auto dI = formations.getTriangulations()[0];

            itandroids_lib::math::Pose2D ballPose = modeling.getWorldModel().getBall().getPosition();

            std::vector<itandroids_lib::math::Vector2<double>> v = dI->interpolate(ballPose.translation);

            return v;
        }

    } /* namespace positioning */
} /* namespace decision_making */