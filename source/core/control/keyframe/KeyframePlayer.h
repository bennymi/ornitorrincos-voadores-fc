/*
 * KeyframeMotionPlayer.h
 *
 *  Created on: Oct 6, 2015
 *      Author: itandroids
 */

#ifndef SOURCE_CONTROL_KEYFRAME_KEYFRAMEMOTIONPLAYER_H_
#define SOURCE_CONTROL_KEYFRAME_KEYFRAMEMOTIONPLAYER_H_

#include <vector>
#include <core/representations/ITAndroidsConstants.h>
#include "utils/patterns/Configurable.h"
#include "core/control/keyframe/page/KeyframePage.h"
#include "representations/NaoJoints.h"
#include "boost/algorithm/string.hpp"
#include <boost/property_tree/ptree.hpp>
#include "utils/patterns/Parameter.h"
#include "KeyframeType.h"
#include "core/control/keyframe/manager/KeyframePageManager.h"

using itandroids_lib::utils::patterns::Parameter;
namespace pt = boost::property_tree;
namespace control {
    namespace keyframe {

        using namespace representations;

        class KeyframePlayer : public itandroids_lib::utils::patterns::Configurable<KeyframePlayer>{
        public:
            /**
             * Prefferred constructor for creating a keyframe player with a configuration tree
             * @param keyframeNode
             */
            template<class Content,class Key>
            KeyframePlayer(Parameter<Content,Key> & keyframeNode) {
                    setParameter(keyframeNode);
            }

            /**
             * Constructor that receives the filepath of the keyframe files
             * Default path: ../configuration/core/keyframe
             * This is a deprecated constructor
             *
             */
            KeyframePlayer(std::string filepath = representations::ITAndroidsConstants::KEYFRAME_FOLDER);

            virtual ~KeyframePlayer();
            /**
             * Deprecated update function, must use setParameter function now
             */
            void update();
            /**
             * Deprecated update function, must use setParameter function now
             * @param folderPath
             */
            void update(std::string folderPath);

            void setPage(std::string name);

            KeyframePage &getCurrentPage();

            representations::NaoJoints &
            getDesiredJoints(representations::NaoJoints &perceivedJoints, double elapsedTime, bool isInverted);
            /**
             * Check if given page has ended
             * @param keyframeType
             * @return
             */
            bool hasPageEnded(std::string keyframeType);

            template<class Content, class Key>
            void setParameter(Parameter<Content, Key> & keyframeNode){
                manager.setParameter(keyframeNode);
            };


        private:
            std::shared_ptr<KeyframePage> currentPage;
            std::string filepath;
            int currentPageId;
            int numPages;
            control::keyframe::manager::KeyframePageManager manager;
        };


    } /* namespace keyframe */
} /* namespace control */

#endif /* SOURCE_CONTROL_KEYFRAME_KEYFRAMEMOTIONPLAYER_H_ */
