/*
 * LookRequest.cpp
 *
 *  Created on: Oct 1, 2015
 *      Author: mmaximo
 */

#include "control/LookRequest.h"

namespace control {

    LookRequest::LookRequest(double pan, double tilt) : pan(pan), tilt(tilt) {
    }

    LookRequest::~LookRequest() {
    }

    double LookRequest::getPan() {
        return pan;
    }

    double LookRequest::getTilt() {
        return tilt;
    }

} /* namespace control */
