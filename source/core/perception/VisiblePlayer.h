/*
 * VisiblePlayer.h
 *
 *  Created on: Sep 1, 2015
 *      Author: itandroids
 */

#ifndef SOURCE_PERCEPTION_VISIBLEPLAYER_H_
#define SOURCE_PERCEPTION_VISIBLEPLAYER_H_

#include "VisibleObject.h"
#include "math/Vector3.h"
#include "utils/parser/ParserTreeNode.h"
#include "utils/parser/KeyValuePair.h"
#include "boost/lexical_cast.hpp"

namespace perception {

    typedef std::map<std::string, Vector3 < double> >
    stringVectorMap;

    using namespace itandroids_lib::math;

    class VisiblePlayer : public VisibleObject {
    public:
        VisiblePlayer();

        VisiblePlayer(utils::parser::ParserTreeNode &node);

        virtual ~VisiblePlayer();

        int getId();

        std::string getTeamName();

        std::map<std::string, Vector3<double> > getAllBodyParts();

        void setBodyPartPosition(std::string partName, Vector3<double> position); // REALMENTE RECEBE UM VECTOR3?
        Vector3<double> getBodyPartPosition(std::string partName);

        virtual void transformPosition(Pose3D &transform);

    private:
        int id;
        std::string teamName;
        stringVectorMap bodyPartMap;

    };

} /* namespace perception */

#endif /* SOURCE_PERCEPTION_VISIBLEPLAYER_H_ */
