//
// Created by dicksiano on 13/03/16.
//

#ifndef ITANDROIDS_SOCCER3D_CPP_BEHAVIORSTATE_H
#define ITANDROIDS_SOCCER3D_CPP_BEHAVIORSTATE_H


namespace decision_making {

    class BehaviorState {
    public:
        /**
         *  Defines the agent state during the game
         *
         */
        enum BEHAVIOR_STATE {
            /** The agent tries to reach the ball */
                    MOVING_BEHIND_BALL,

            /** The agent conducts the ball towards the opponent's goal */
                    CONDUCTING_BALL_FORWARD,

            /** The agent is between the ball and the opponent team goal */
                    HAS_PASSED_BALL,

            /** The agent is close to the ball, but he must face the goal before conduct the ball */
                    TURNING_TO_THE_GOAL,

            /** The agent is close to the goal and he should prepare himself to kick the ball */
                    PREPARING_TO_KICK,

            /** The agent is close to the goal and he should kick the ball */
                    KICKING_THE_BALL,

            /** Default state */
                    NONE,
        };

    };
} /* namespace decision_making */


#endif //ITANDROIDS_SOCCER3D_CPP_BEHAVIORSTATE_H
