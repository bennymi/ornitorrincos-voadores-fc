/*
 * KeyframeMotionConstants.h
 *
 *  Created on: Sep 29, 2014
 *      Author: manga
 */

#ifndef KEYFRAMEMOTIONCONSTANTS_H_
#define KEYFRAMEMOTIONCONSTANTS_H_

#include "representations/NaoJoints.h"

namespace control {

    namespace keyframe {
        /**
         * Parser constants
         */
        const int TIME_LIST_SIZE = 5;
        /**
         * Keyframe types
         */


    } /* namespace keyframe */

} /* namespace control */

#endif /* KEYFRAMEMOTIONCONSTANTS_H_ */
