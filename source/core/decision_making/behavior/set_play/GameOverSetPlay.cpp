//
// Created by dicksiano on 8/14/16.
//

#include "GameOverSetPlay.h"
#include "../BehaviorFactory.h"

namespace decision_making {
    namespace behavior {

        GameOverSetPlay::GameOverSetPlay() : SetPlay(nullptr) {
        }

        GameOverSetPlay::GameOverSetPlay(BehaviorFactory *behaviorFactory) : SetPlay(behaviorFactory) {
        }

        GameOverSetPlay::~GameOverSetPlay() {
        }

        void GameOverSetPlay::behave(modeling::Modeling &modeling) {
            goToDelaunayPositioning(modeling);
        } /* function behave */

    } /* namespace behavior */
} /* namespace decision_making */