/*
 * BoundingBox.h
 *
 *  Created on: Oct 4, 2015
 *      Author: mmaximo
 */

#ifndef SOURCE_MODELING_BOUNDINGBOX_H_
#define SOURCE_MODELING_BOUNDINGBOX_H_

#include "math/RotationMatrix.h"

namespace modeling {

    using namespace itandroids_lib::math;

/**
 * Class that represents a bounding box.
 * Box with the smallest measure within which all the points lie.
 */
    class BoundingBox {
    public:
        /**
         * Constructor of bounding box in cartesian coordinates.
         *
         * @param length the length of bounding box.
         * @param width the width of bounding box.
         * @param height the height of bounding box.
         */
        BoundingBox(double length, double width, double height);

        /**
         * Constructor of bounding box in cylindrical coordinates.
         * @param height the height of bounding box.
         * @param the radius of cylindrical bounding box.
         */

        BoundingBox(double height, double radius);

        /**
         * Constructor of bounding box in spherical coordinates.
         * @param radius the radius of bounding box.
         */

        BoundingBox(double radius);

        /**
         * Default constructor of bounding box.
         */

        virtual ~BoundingBox();

        /**
         * Gets the length of bounding box.
         *
         * @return the length of bounding box.
         */
        double getLength();

        /**
         * Gets the width of bounding box.
         *
         * @return the width of bounding box.
         */

        double getWidth();

        /**
         * Gets the height of bounding box.
         *
         * @return of height of bounding box.
         */

        double getHeight();

        /**
         * Transforms the bounding box accordingly to the rotation matrix.
         *
         * @param rotationMatrix the matrix of rotation.
         */

        void transform(const RotationMatrix &rotationMatrix);

    private:
        double length;
        double width;
        double height;
    };

} /* namespace modeling */

#endif /* SOURCE_MODELING_BOUNDINGBOX_H_ */
