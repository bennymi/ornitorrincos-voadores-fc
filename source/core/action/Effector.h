/*
 * Effector.h
 *
 *  Created on: Aug 8, 2015
 *      Author: mmaximo
 */

#ifndef SOURCE_ACTION_EFFECTOR_H_
#define SOURCE_ACTION_EFFECTOR_H_

#include <iostream>

namespace action {

/**
 * Abstract class that represents a generic effector.
 */
    class Effector {
    public:
        /**
         * Default constructor.
         */
        Effector();

        virtual ~Effector();

        /**
         * Gets the effector's representation as
         * a server message.
         */
        virtual std::string toString() = 0;
    };

} /* namespace action */

#endif /* SOURCE_ACTION_EFFECTOR_H_ */
