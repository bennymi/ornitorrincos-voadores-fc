//
// Created by luckeciano on 8/9/16.
//

#ifndef ITANDROIDS_SOCCER3D_CPP_BIT64OPERATOR_H
#define ITANDROIDS_SOCCER3D_CPP_BIT64OPERATOR_H

#include <boost/cstdint.hpp>


#define PLAYERS 15
#define MAXN PLAYERS
#define DATA_64WORDS_SIZE (MAXN*MAXN-1)/64+2

typedef uint64_t uint64;


namespace decision_making {
    namespace positioning {
        namespace role_assignment {
            class Bit64Operator {
                //Functions for operations on 64 bit values
            public:
                static bool isZero(const uint64 (&value)[DATA_64WORDS_SIZE]);

                static bool isEqual(const uint64 (&valueA)[DATA_64WORDS_SIZE],
                                    const uint64 (&valueB)[DATA_64WORDS_SIZE]);

                static bool isLessThan(const uint64 (&valueA)[DATA_64WORDS_SIZE],
                                       const uint64 (&valueB)[DATA_64WORDS_SIZE]);

                static void setMax(uint64 (&value)[DATA_64WORDS_SIZE]);

                static void setZero(uint64 (&value)[DATA_64WORDS_SIZE]);

                static void setBit(uint64 (&value)[DATA_64WORDS_SIZE], long bit);

                static void assignValue(uint64 (&valueA)[DATA_64WORDS_SIZE],
                                        const uint64 (&valueB)[DATA_64WORDS_SIZE]);

                static void subtractValue(uint64 (&valueA)[DATA_64WORDS_SIZE],
                                          const uint64 (&valueB)[DATA_64WORDS_SIZE]);

                static void addValue(uint64 (&valueA)[DATA_64WORDS_SIZE],
                                     const uint64 (&valueB)[DATA_64WORDS_SIZE]);


            };
        }
    }
}


#endif //ITANDROIDS_SOCCER3D_CPP_BIT64OPERATOR_H
