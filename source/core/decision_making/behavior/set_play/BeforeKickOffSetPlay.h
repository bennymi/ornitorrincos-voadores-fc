//
// Created by dicksiano on 8/14/16.
//

#ifndef ITANDROIDS_SOCCER3D_CPP_BEFOREKICKOFFSETPLAY_H
#define ITANDROIDS_SOCCER3D_CPP_BEFOREKICKOFFSETPLAY_H

#include "../Behavior.h"

namespace decision_making {
    namespace behavior {

        class BeforeKickOffSetPlay : public Behavior {
        public:

            /**
            * Default constructor.
            */
            BeforeKickOffSetPlay();

            /**
            * Assignment constructor.
            *
            * @param pointer to BehaviorFactory.
            */
            BeforeKickOffSetPlay(BehaviorFactory *behaviorFactory);

            /**
            * Destructor.
            */
            virtual ~BeforeKickOffSetPlay();

            /**
            * This function controls the actions.
            *
            * @param modeling.
            */
            void behave(modeling::Modeling &modeling);

            std::shared_ptr<itandroids_lib::math::Pose2D> getBeamRequest() { return beamRequest; }

        private:
            /*
            * All derived set plays can modify beamRequest.
            */
            std::shared_ptr<itandroids_lib::math::Pose2D> beamRequest;
        };

    } /* namespace behavior */
} /* namespace decision_making */



#endif //ITANDROIDS_SOCCER3D_CPP_BEFOREKICKOFFSETPLAY_H
