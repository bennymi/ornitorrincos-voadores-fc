/*
 * KeyframeMotionPage.cpp
 *
 *  Created on: Oct 6, 2015
 *      Author: itandroids
 */
#include "KeyframePage.h"

#include "math/MathUtils.h"

#include <stdio.h>
#include <utils/LogSystem.h>

namespace control {

    namespace keyframe {

        using namespace itandroids_lib::math;

        const double KeyframePage::MAXIMUM_DIFF = itandroids_lib::math::MathUtils::degreesToRadians(10);

        KeyframePage::KeyframePage() :
                id(0), currentTime(0.0f), currentStep(0), speedRate(0), initialTime(0), movementTime(0) {
            isSmoothing = false;
            hasStarted = false;
        }

        KeyframePage::KeyframePage(std::string name) :
                name(name), id(0), currentTime(0.0f), currentStep(0), speedRate(0), initialTime(0), movementTime(0) {
            isSmoothing = false;
        }

        KeyframePage::~KeyframePage() {
        }

        void KeyframePage::addStep(std::shared_ptr<KeyframeStep> step) {
            movementTime += step->getDuration();
            steps.push_back(step);
        }

        void KeyframePage::setName(std::string name) {
            this->name = name;
        }


        void KeyframePage::setInitialTime(double time) {
            initialTime = time;
        }

        bool KeyframePage::hasFinished() {
            return (currentTime * speedRate >= movementTime);
        }

        double KeyframePage::getCurrentTime() {
            return currentTime;
        }

        std::string KeyframePage::getName() {
            return name;
        }


        void KeyframePage::setSpeedRate(double speedRate) {
            this->speedRate = speedRate;
        }

        double KeyframePage::getSpeedRate() {
            return speedRate;
        }

        void KeyframePage::reset() {
            currentTime = 0.0f;
            currentStep = 0;
            hasStarted = false;
            isSmoothing = false;
            desiredJoints = steps.front()->getJoints();
        }

        representations::NaoJoints &
        KeyframePage::getDesiredJoints(representations::NaoJoints &perceivedJoints, double globalTime,
                                       bool isInverted) {
            NaoJoints currentPosition =
                    perceivedJoints * NaoJoints::getNaoDirectionsFixing();
            if (currentTime * speedRate >= movementTime && hasStarted && currentStep >= steps.size()) {
                hasStarted = false;
                isSmoothing = false;
            } else if (!hasStarted) {
                // do a linear interpolation from initial joints to expected joints
                if (isSmoothing == false) {
                    initialTime = globalTime;
                    isSmoothing = true;
                }

                NaoJoints targetPosition = isInverted ? representations::NaoJoints::getSimetricPositions(
                        steps[0]->getJoints()) : steps[0]->getJoints();
                itandroids_lib::math::LinearInterpolator<NaoJoints> interpolator(currentPosition, targetPosition);
                if (steps[0]->getDuration() != 0) {
                    desiredJoints = interpolator.interpolate(
                            (globalTime - initialTime) * speedRate / steps[0]->getDuration());
                } else {
                    desiredJoints = interpolator.interpolate(1);
                }

                if ((globalTime - initialTime) * speedRate >= steps[0]->getDuration()) {
                    hasStarted = true;
                    initialTime = globalTime;
                    currentTime = (globalTime - initialTime);
                    currentStep = 0;
                    desiredJoints = steps[0]->getJoints();
                    if (isInverted) {
                        desiredJoints = representations::NaoJoints::getSimetricPositions(desiredJoints);
                    }
                }
                previousDesiredPositions = desiredJoints;
                previousJoints = currentPosition;
            } else {
                currentTime = (globalTime - initialTime);
                while (currentTime >= stepTimes[currentStep + 1]) {
                    currentStep++;
                }
                if (currentStep < steps.size() && currentTime * speedRate < movementTime) {
                    auto interpolateJoints = interpolate();
                    desiredJoints = representations::NaoJoints(interpolateJoints);
                    if (isInverted) {
                        desiredJoints = representations::NaoJoints::getSimetricPositions(desiredJoints);
                    }
                } else {
                    desiredJoints = (*(steps.end() - 1))->getJoints();
                    if (isInverted) {
                        desiredJoints = representations::NaoJoints::getSimetricPositions(desiredJoints);
                    }
                }

            }

            return desiredJoints;
        }


        void KeyframePage::init() {
            if (steps.size() > 0) {
                movementTime = -steps[0]->getDuration();
                for (auto &step : steps) {
                    movementTime += step->getDuration();
                    stepTimes.push_back(movementTime);
                }
            }
            createInterpolators();

        }

        double KeyframePage::getMovementTime() {
            return movementTime;
        }


    } /* namespace keyframe */

} /* namespace control */

