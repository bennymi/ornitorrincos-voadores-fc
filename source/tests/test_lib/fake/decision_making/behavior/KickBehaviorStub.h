//
// Created by francisco on 6/20/16.
//

#ifndef ITANDROIDS_SOCCER3D_CPP_KICKBEHAVIORSTUB_H
#define ITANDROIDS_SOCCER3D_CPP_KICKBEHAVIORSTUB_H

#include "decision_making/behavior/KickBehavior.h"
#include "decision_making/behavior/kick_behavior/KickManager.h"

namespace decision_making {
    namespace behavior {

        class KickBehaviorStub : public KickBehavior {
        public:
            using KickBehavior::KickBehavior;

            kick_behavior::KickManager &getManager();

        private:
        };
    }
}


#endif //ITANDROIDS_SOCCER3D_CPP_KICKBEHAVIORSTUB_H
