Convenções de Código - ITAndroids 3D:

1. Diretrizes das Convenções:
- Fácil de usar, pois temos eletracas e bixos no time.
- Evitar que as pessoas errem.
- Melhor ainda: fazer acertar sem pensar.
- Usar padrão do Eclipse onde der.

2. Estilo de Código:
- Língua: código e comentários em inglês; documentação fora do código pode ser em português.
- Documentação Doxygen: usar padrão Javadoc.
- Formatação: usar padrão do Eclipse C++.
- Nomes: usar padrão do Eclipse C++.
    -- Ver em Preferences->C/C++->Code Style->Name Style
- Convenções de nome:
    -- Padrão: camelCase
    -- Classes: MyClass
    -- Variáveis: myVariable
    -- Enums/constantes: MY_ENUM, MY_CONSTANT
    -- Namespaces: my_namespace
    -- Arquivos/diretórios: MyClass.h, MyClass.cpp, MyClass_UnitTest.cpp,
			    MyClass_ComponentTest.cpp, MyClass_AgentTest.cpp,
                            MyClass.o, MyClass_UnitTest.o, my_dir,
                            my_file.xxx, MyClassBinary, my_binary

3. Estrutura de Pastas:
itandroids-soccer3d-cpp/
    binaries: executáveis
    build: .o
    documentation: doxygen, relatórios etc.
    etcetera: coisas genéricas
    externals: dependências externas (e.g. bibliotecas)
    source: .h e .cpp
    tests: testes do código que estiver em "source"
    tools: ferramentas

4. Estrutura de Source:
source/
    agent: código dos agentes; main's ficam aqui
    communication
    perception
    modeling
    decision_making
    control
    representations: estruturas de dados comuns a mais de um componente (se não tiver método, pode ser só .h)
    utils
- Um componente pode ser formado por mais componentes (recursivamente):
source/
    modeling/
        world_model
        localization
source/
    control/
        walking
        kick

5. Estrutura de Tests:
tests/
    unit: testes de unidade
    component: verificar se um componente funciona corretamente (teste não precisa ser automatizado)
    integration: verificar integração entre componentes
    agent: agentes de teste (e.g. agente controlado pelo teclado)

6. Estrutura de Binaries:
binaries/
    (...)

7. Comandos de CMake:
cmake
make

8. Mais convenções:
- Namespaces devem seguir estrutura de pastas:
    -- modeling::world_model
    -- tests::component::modeling::world_model
- Os includes devem seguir o mesmo esquema de namespaces:
	#include "modeling/world_model/WorldModel.h
- Quanto precisar usar números em ponto flutuante, prefira usar double a usar float.
- Por padrão, as unidades de todas as variáveis devem estar no SI (radianos, metros, quilogramas etc.).
- Cada componente deve ter uma classe representativa (de preferência com mesmo nome do componente).
- Prefira fazer as coisas na forma de C++ do que de C (e.g. prefira std::cout ao invés de printf).
- Para nomes de getters e setters, use getNomeVariavel() e setNomeVariavel().
- Use #ifndef, #define, #endif na definição de cada arquivo (crie os arquivos usando o Eclipse para que já fiquem no padrão correto).
- Coloque as regiões de acesso nas classes na seguinte ordem: public, protected, private (quem vai ler a classe geralmente está atrás das funções públicas).
- Coloque as variáveis antes das funções em cada uma das regiões.
- Siga a mesma ordem das funções no .h e no .cpp.
- Uso de struct para definir uma estrutura de dados muito simples é permitido.
- Deixar variáveis computadas no timestep atual na memória do componente e deixar que os outros módulos acessem (feio, mas facilitar debug).

9. Uso de dependências:
- Usar sempre GoogleTest para testes de unidade.
- Usar Boost como biblioteca de C++.
- Usar sempre Eigen para Álgebra Linear (vetores, matrizes etc.).
- Não incluir dependências que precisem de instalação para executar, pois torna complicado o uso nas máquinas da competição.
- Usar o mínimo de dependências possível.
- Evitar dependências que tornem o código não portável (e.g. tornar o código preso a algum sistema operacional).
