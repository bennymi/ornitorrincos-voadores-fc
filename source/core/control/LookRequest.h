/*
 * LookRequest.h
 *
 *  Created on: Oct 1, 2015
 *      Author: mmaximo
 */

#ifndef SOURCE_CONTROL_LOOKREQUEST_H_
#define SOURCE_CONTROL_LOOKREQUEST_H_

namespace control {

    class LookRequest {
    public:
        LookRequest(double pan, double tilt);

        virtual ~LookRequest();

        double getPan();

        double getTilt();

    private:
        double pan;
        double tilt;
    };

} /* namespace control */

#endif /* SOURCE_CONTROL_LOOKREQUEST_H_ */
