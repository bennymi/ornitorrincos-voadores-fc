/*
 * Wizard.cpp
 *
 *  Created on: Aug 8, 2015
 *      Author: mmaximo
 */

#include "Wizard.h"
#include <math/MathUtils.h>

namespace utils {
    namespace wizard {

        Wizard::Wizard(communication::Communication &communication) :
                comm(communication) {
            lastUpdateTime = 0;
            transformCount = 0;
        }

        Wizard::~Wizard() {
            if (communicationThread.joinable()) {
                communicationThread.join();
            }
        }

        void Wizard::update(std::string content) {
            std::lock_guard<std::mutex> l(updateMtx);
            transformCount = 0;
            int torsoIndex;
            int headIndex;
            if (content.empty())
                return;
            utils::parser::Parser parser;
            utils::parser::ParserTreeNode *root = parser.parseMessage(content);
            std::string auxString;
            utils::parser::KeyValuePair pair = utils::parser::KeyValuePair(
                    root->children[0]->children[0]->content, ' ');
            auxString = pair.key;
            boost::trim(auxString);
            if (auxString == "time") {
                lastUpdateTime = boost::lexical_cast<double>(pair.value);
            }
            std::vector<utils::parser::ParserTreeNode *>::iterator iter;
            int nodeNumber = 0;
            for (iter = root->children[2]->children.begin();
                 iter != root->children[2]->children.end(); iter++) {
                std::string compareStr;
                compareStr = (*iter)->content;
                std::vector<std::string> strVector;
                boost::split(strVector, compareStr, boost::is_any_of(" "));
                if (strVector[0] == "nd") {
                    nodeNumber++;
                }

                if (nodeNumber == 36) {
                    std::vector<utils::parser::ParserTreeNode *>::iterator ballIter;
                    for (ballIter = (*iter)->children.begin();
                         ballIter != (*iter)->children.end(); ballIter++) {
                        compareStr = (*ballIter)->content;
                        boost::split(strVector, compareStr, boost::is_any_of(" "));
                        if (strVector[0] == "SLT") {
                            utils::parser::ParserTreeNode *auxPtr = *ballIter;
                            ballTransform = readTransformationMatrix(auxPtr);
                        }
                    }
                } else if (nodeNumber == 37) {
                    std::vector<utils::parser::ParserTreeNode *>::iterator agentIter;
                    int agentNodeNumber = 0;
                    for (agentIter = (*iter)->children.begin();
                         agentIter != (*iter)->children.end(); agentIter++) {

                        compareStr = (*agentIter)->content;
                        boost::split(strVector, compareStr, boost::is_any_of(" "));
                        if (strVector[0] == "SLT") {
                            utils::parser::ParserTreeNode *auxPtr = *agentIter;
                            firstAgentTransform = readTransformationMatrix(auxPtr);

                        } else if (strVector[0] == "nd") {
                            agentNodeNumber++;
                        }

                        if (agentNodeNumber == 1) { //Torso node
                            std::vector<utils::parser::ParserTreeNode *>::iterator torsoIter;
                            int torsoNodeNumber = 0;
                            for (torsoIter = (*agentIter)->children.begin();
                                 torsoIter != (*agentIter)->children.end();
                                 torsoIter++) {
                                compareStr = (*torsoIter)->content;
                                boost::split(strVector, compareStr,
                                             boost::is_any_of(" "));
                                if (strVector[0] == "SLT") {
                                    utils::parser::ParserTreeNode *auxPtr = *torsoIter;
                                    firstTorsoTransform = readTransformationMatrix(
                                            auxPtr);

                                } else if (strVector[0] == "nd") {
                                    torsoNodeNumber++;
                                }

                                if (torsoNodeNumber == 1) {
                                    std::vector<utils::parser::ParserTreeNode *>::iterator torsoChildIter;

                                    for (torsoChildIter = (*torsoIter)->children.begin();
                                         torsoChildIter
                                         != (*torsoIter)->children.end();
                                         torsoChildIter++) {
                                        compareStr = (*torsoChildIter)->content;
                                        boost::trim(compareStr);
                                        boost::split(strVector, compareStr,
                                                     boost::is_any_of(" "));
                                        if (strVector[0] == "SLT") {
                                            utils::parser::ParserTreeNode *auxPtr =
                                                    *torsoChildIter;

                                            torsoTransform = readTransformationMatrix(
                                                    auxPtr);

                                        }
                                    }
                                }
                            }
                        } else if (agentNodeNumber == 3) { //Head node
                            std::vector<utils::parser::ParserTreeNode *>::iterator headIter;
                            int headNodeNumber = 0;
                            for (headIter = (*agentIter)->children.begin();
                                 headIter != (*agentIter)->children.end();
                                 headIter++) {
                                compareStr = (*headIter)->content;
                                boost::split(strVector, compareStr,
                                             boost::is_any_of(" "));
                                if (strVector[0] == "SLT") {
                                    utils::parser::ParserTreeNode *auxPtr = *headIter;
                                    firstHeadTransform = readTransformationMatrix(
                                            auxPtr);
                                } else if (strVector[0] == "nd") {
                                    headNodeNumber++;
                                }

                                if (headNodeNumber == 1) {
                                    std::vector<utils::parser::ParserTreeNode *>::iterator headChildIter;
                                    for (headChildIter = (*headIter)->children.begin();
                                         headChildIter != (*headIter)->children.end();
                                         headChildIter++) {
                                        compareStr = (*headIter)->content;
                                        boost::split(strVector, compareStr,
                                                     boost::is_any_of(" "));
                                        if (strVector[0] == "SLT") {
                                            utils::parser::ParserTreeNode *auxPtr =
                                                    *headChildIter;
                                            agentHeadTransform =
                                                    readTransformationMatrix(auxPtr);
                                        }
                                    }
                                }
                            }
                        }

                    }
                }
            }
            delete root;
        }

        double Wizard::getLastUpdateTime() {
            std::lock_guard<std::mutex> l(updateMtx);
            return lastUpdateTime;
        }

        itandroids_lib::math::Matrix<4, 4, double> Wizard::getAgentTransform() {
            std::lock_guard<std::mutex> l(updateMtx);
            itandroids_lib::math::Matrix<4, 4, double> agentTransform =
                    torsoTransform * firstTorsoTransform * firstAgentTransform;
            return agentTransform;
        }

        Vector3<double> Wizard::getAgentTranslation() {
            std::lock_guard<std::mutex> l(updateMtx);
            Vector3<double> translation;
            itandroids_lib::math::Matrix<4, 4, double> agentTransform;
            agentTransform = torsoTransform * firstTorsoTransform * firstAgentTransform;
            translation.x = agentTransform[0][3];
            translation.y = agentTransform[1][3];
            translation.z = agentTransform[2][3];
            return translation;
        }

        RotationMatrix Wizard::getAgentRotation() {
            std::lock_guard<std::mutex> l(updateMtx);
            itandroids_lib::math::Matrix<4, 4, double> agentTransform =
                    torsoTransform * firstTorsoTransform * firstAgentTransform;
            return RotationMatrix(
                    Vector3<double>(agentTransform[0][0], agentTransform[1][0],
                                    agentTransform[2][0]),
                    Vector3<double>(agentTransform[0][1], agentTransform[1][1],
                                    agentTransform[2][1]),
                    Vector3<double>(agentTransform[0][2], agentTransform[1][2],
                                    agentTransform[2][2]));
        }

        Pose3D Wizard::getAgentPose3D() {
            return Pose3D(getAgentRotation(), getAgentTranslation());
        }

        itandroids_lib::math::Matrix<4, 4, double> Wizard::getBallTransform() {
            std::lock_guard<std::mutex> l(updateMtx);
            return ballTransform;
        }

        void Wizard::stop() {
            comm.closeConnection();
        }

        void Wizard::setSendMessage(std::string command) {
            std::lock_guard<std::mutex> l(mtx);
            messages.push_back(command);
        }

        void Wizard::setAgentPositionAndDirection(int uniformNumber,
                                                  representations::PlaySide::PLAY_SIDE playSide,
                                                  Vector3<double> newPosition, double newDirection) {
            double sendDirection = itandroids_lib::math::MathUtils::normalizeAngle(newDirection);
            sendDirection -= M_PI_2;
            sendDirection = itandroids_lib::math::MathUtils::radiansToDegrees(sendDirection);
            std::string command = "(agent (unum "
                                  + boost::lexical_cast<std::string>(uniformNumber) + ") (team "
                                  + representations::PlaySide::toString(playSide) + ") (move "
                                  + getVectorString(newPosition) + " "
                                  + boost::lexical_cast<std::string>(sendDirection) + "))";
            setSendMessage(command);
        }

        void Wizard::setBatteryLevel(int unum,
                                     representations::PlaySide::PLAY_SIDE teamSide, double batteryLevel) {
            std::string command;
            command = "(agent (unum " + boost::lexical_cast<std::string>(unum)
                      + ") (team " + representations::PlaySide::toString(teamSide)
                      + ") (battery " + boost::lexical_cast<std::string>(batteryLevel)
                      + "))";
            setSendMessage(command);
        }

        void Wizard::setTemperature(int unum,
                                    representations::PlaySide::PLAY_SIDE teamSide, double temperature) {
            std::string command;
            command = "(agent (unum " + boost::lexical_cast<std::string>(unum)
                      + ") (team " + representations::PlaySide::toString(teamSide)
                      + ") (temperature " + boost::lexical_cast<std::string>(temperature)
                      + "))";
            setSendMessage(command);
        }

        void Wizard::setGameTime(double gameTime) {
            std::string command = "(time " + boost::lexical_cast<std::string>(gameTime)
                                  + " )";
            setSendMessage(command);
        }

        void Wizard::setBallPosition(Vector3<double> newPosition) {
            std::string command;
            command = "(ball (pos " + getVectorString(newPosition) + "))";
            setSendMessage(command);
        }

        void Wizard::setBallPositionAndVelocity(Vector3<double> newPosition,
                                                Vector3<double> newVelocity) {
            std::string command;
            command = "(ball (pos " + getVectorString(newPosition) + ") (vel "
                      + getVectorString(newVelocity) + "))";
            setSendMessage(command);
        }

        void Wizard::setBallVelocity(Vector3<double> newVelocity) {
            std::string command;
            command = "(ball (vel " + getVectorString(newVelocity) + "))";
            setSendMessage(command);
        }

        void Wizard::setPlayMode(representations::RawPlayMode::RAW_PLAY_MODE playMode) {
            std::string command;
            command = "(playMode " + representations::RawPlayMode::toString(playMode)
                      + ")";
            setSendMessage(command);
        }

        void Wizard::dropBall() {
            std::string command;
            command = "(dropball)";
            setSendMessage(command);
        }

        void Wizard::kickOff(representations::PlaySide::PLAY_SIDE teamSide) {
            std::string command;
            command = "(kickOff " + representations::PlaySide::toString(teamSide) + ")";
            setSendMessage(command);
        }

        void Wizard::selectAgent(int unum,
                                 representations::PlaySide::PLAY_SIDE teamSide) {
            std::string command;
            command = "(select (unum " + boost::lexical_cast<std::string>(unum) + ")"
                      + " (team " + representations::PlaySide::toString(teamSide) + "))";
            setSendMessage(command);
        }

        void Wizard::killAgent(int unum,
                               representations::PlaySide::PLAY_SIDE teamSide) {
            std::string command;
            command = "(agent (unum " + boost::lexical_cast<std::string>(unum) + ")"
                      + " (team " + representations::PlaySide::toString(teamSide) + "))";
            setSendMessage(command);
        }

        void Wizard::killSelectedAgent() {
            std::string command;
            command = "(kill)";
            setSendMessage(command);
        }

        void Wizard::repositionAgent(int unum,
                                     representations::PlaySide::PLAY_SIDE teamSide) {
            std::string command;
            command = "(repos (unum + " + boost::lexical_cast<std::string>(unum) + ")"
                      + " (team " + representations::PlaySide::toString(teamSide) + "))";
            setSendMessage(command);
        }

        void Wizard::repositionSelectedAgent() {
            std::string command;
            command = "(repos)";
            setSendMessage(command);
        }

        void Wizard::killSimulator() {
            std::string command;
            command = "(killsim)";
            setSendMessage(command);
        }

        std::string Wizard::getVectorString(Vector3<double> vector) {
            std::string str;

            str = boost::lexical_cast<std::string>(vector.x) + " "
                  + boost::lexical_cast<std::string>(vector.y) + " "
                  + boost::lexical_cast<std::string>(vector.z);
            return str;
        }

        itandroids_lib::math::Matrix<4, 4, double> Wizard::readTransformationMatrix(
                utils::parser::ParserTreeNode *node) {
            std::string content;
            content = utils::parser::KeyValuePair(node->content, ' ').value;
            std::vector<std::string> stringVector;
            boost::split(stringVector, content, boost::is_any_of(" "));
            itandroids_lib::math::Matrix<4, 4, double> matrix;
            for (int vectorIndex = 0, matrixColumn = 0; matrixColumn < 4;
                 matrixColumn++) {
                for (int matrixLine = 0; matrixLine < 4; matrixLine++, vectorIndex++) {
                    matrix[matrixLine][matrixColumn] = boost::lexical_cast<double>(
                            stringVector[vectorIndex]);
                }
            }
            return matrix;
        }

        itandroids_lib::math::Matrix<4, 4, double> Wizard::readTransformationMatrix(
                std::string vectorString) {
            std::string content;
            content = vectorString;
            std::vector<std::string> stringVector;
            boost::split(stringVector, content, boost::is_any_of(" "));
            itandroids_lib::math::Matrix<4, 4, double> matrix;
            for (int vectorIndex = 0, matrixColumn = 0; matrixColumn < 4;
                 matrixColumn++) {
                for (int matrixLine = 0; matrixLine < 4; matrixLine++, vectorIndex++) {
                    matrix[matrixLine][matrixColumn] = boost::lexical_cast<double>(
                            stringVector[vectorIndex]);
                }
            }
            return matrix;
        }

        void Wizard::run() {
            communicationThread = std::thread(&Wizard::communicationTask, this);
        }

        void Wizard::communicationTask() {
            while (true) {
                comm.receiveMessage();
                update(comm.getCurrentMessage());
                sendMessage();
            }
        }


        void Wizard::sendMessage() {
            std::lock_guard<std::mutex> l(mtx);
            if (messages.empty()) {
                std::string emptyMessage = "(syn)";
                comm.sendMessage(emptyMessage);
            } else {
                for (auto &k : messages) {
                    k += "(syn)";
                    comm.sendMessage(k);
                }
                messages.clear();
            }

        }

        Vector3<double> Wizard::getBallTranslation() {
            std::lock_guard<std::mutex> l(mtx);
            Vector3<double> translation;
            translation.x = ballTransform[0][3];
            translation.y = ballTransform[1][3];
            translation.z = ballTransform[2][3];
            return translation;
        }

        void Wizard::setScore(int left, int right) {
            std::stringstream command;
            command << "(score (left " << left << ") (right " << right << "))";
            setSendMessage(command.str());
        }


    }
/* namespace wizard */
} /* namespace utils */
