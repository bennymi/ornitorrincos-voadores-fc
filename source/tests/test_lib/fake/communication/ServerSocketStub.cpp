/*
 * ServerSocket_Stub.cpp
 *
 *  Created on: Oct 1, 2015
 *      Author: mmaximo
 */

#include "ServerSocketStub.h"

namespace communication {

    ServerSocketStub::ServerSocketStub() :
            connectionEstablished(false) {
    }

    ServerSocketStub::~ServerSocketStub() {
    }


    bool ServerSocketStub::establishConnection() {
        connectionEstablished = true;
        return connectionEstablished;
    }

    void ServerSocketStub::closeConnection() {
        connectionEstablished = false;
    }

    std::string ServerSocketStub::receiveMessage() {
        std::string message = receiveQueue.front();
        receiveQueue.pop();
        return message;
    }

    void ServerSocketStub::sendMessage(const std::string &message) {
        sendQueue.push(message);
    }

    void ServerSocketStub::addMessageToReceive(const std::string &message) {
        receiveQueue.push(message);
    }

    std::queue<std::string> ServerSocketStub::getReceiveQueue() {
        return receiveQueue;
    }

    std::queue<std::string> ServerSocketStub::getSendQueue() {
        return sendQueue;
    }

    bool ServerSocketStub::isConnectionEstablished() {
        return connectionEstablished;
    }

    bool ServerSocketStub::connectionClosed() {
        return false;
    }


} /* namespace communication */
