//
// Created by francisco on 28/11/16.
//

#ifndef ITANDROIDS_SOCCER3D_CPP_ROBOTTYPE_H
#define ITANDROIDS_SOCCER3D_CPP_ROBOTTYPE_H

namespace representations {

    class RobotType {
    public:
        enum ROBOT_TYPE {
            STANDART_NAO = 0,
            SECONDARY_NAO = 2,
            STANDART_NAO_WITH_TOE = 4,

        };
    };
}


#endif //ITANDROIDS_SOCCER3D_CPP_ROBOTTYPE_H
