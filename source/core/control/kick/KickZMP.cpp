/*
 * Kick.cpp
 *
 *  Created on: Oct 11, 2015
 *      Author: mmaximo
 */

#include <math.h>

#include "control/kick/KickZMP.h"

namespace control {
    namespace kick {

        KickZMPParams::KickZMPParams() :
                moveZmpDuration(0.0f), kickDuration(0.0f), takeFootDuration(0.0f), kickAmplitude(
                0.0f), zFoot(0.0f), accelerationMoveRatio(0.0f), ySeparation(
                0.0f), zCom(0.0f), armsCompensationFactor(0.0f) {
        }

        KickZMP::KickZMP(KickZMPParams kickParams, bool leftLegKicks,
                         RobotPhysicalParameters physicalParameters,
                         InverseKinematics *inverseKinematics) :
                GRAVITY(9.80665f), kickParams(kickParams), leftLegKicks(leftLegKicks), physicalParams(
                physicalParameters), inverseKinematics(inverseKinematics), inStateTime(
                0.0f), totalTime(0.0f) {
            reset();
        }

        KickZMPParams KickZMPParams::getDefaultKickZMPParams() {
            KickZMPParams params;
            params.moveZmpDuration = 0.2;
            params.kickDuration = 0.2;
            params.takeFootDuration = 0.5; // straight: 0.3f, side: 0.5f (slips a bit)
            params.zCom = 0.3;
            params.zFoot = 0.05;
            params.accelerationMoveRatio = 0.2;
            params.ySeparation = 0.06;
            params.armsCompensationFactor = 0.0;
            params.kickAmplitude = 0.1;
            return params;
        }

        KickZMP::~KickZMP() {
            delete inverseKinematics;
        }

        void KickZMP::reset() {
            state = MOVING_ZMP_TO_SS;

            inStateTime = 0.0f;
            totalTime = 0.0f;

            double f = kickParams.accelerationMoveRatio / 2.0f;

            a1 = 1 / (2 * f * (1 - f));
            a2 = 1 / (1 - f);
            b2 = -f / (2 * (1 - f));
            a3 = -1 / (2 * f * (1 - f));
            b3 = 1 / (f * (1 - f));
            c3 = -1 / (2 * f * (1 - f)) + 1;

            totalDuration = kickParams.moveZmpDuration + kickParams.takeFootDuration
                            + kickParams.kickDuration + kickParams.takeFootDuration
                            + kickParams.moveZmpDuration;
            tZMP = sqrt(kickParams.zCom / GRAVITY);
        }

        void KickZMP::update(double elapsedTime, NaoJoints &jointsTargets) {
            totalTime += elapsedTime;
            inStateTime += elapsedTime;

            double t = totalTime / totalDuration;

            if (t > 1.0f)
                t = 1.0f;

            solveZMP(0.0f, kickParams.ySeparation, kickParams.ySeparation,
                     kickParams.ySeparation, kickParams.ySeparation, aP, aN);
            double yCom = getCoM(t, aP, aN, 0.0f, kickParams.ySeparation,
                                 kickParams.ySeparation);

            Vector3<double> torso(0.0f, yCom, 0.0f);

            kickFoot.y = 2.0f * kickParams.ySeparation;
            kickFoot.z = 0.0f;

            changeState();

            switch (state) {
                case MOVING_ZMP_TO_SS:
                    moveZmpToSS();
                    break;
                case TAKING_FOOT_OFF_THE_GROUND:
                    takeFootOffTheGround();
                    break;
                case KICKING:
                    kick();
                    break;
                case PUTTING_FOOT_ON_THE_GROUND:
                    putFootOnTheGround();
                    break;
                case MOVING_ZMP_TO_DS:
                    moveZmpToDS();
                    break;
            }

            if (leftLegKicks) {
                leftFoot = poseRelative(kickFoot, torso);
                rightFoot = -torso;

                zLeft = zKickFoot;
                zRight = kickParams.zCom;
            } else {
                torso.y = -torso.y;
                kickFoot.y = -kickFoot.y;

                leftFoot = -torso;
                rightFoot = poseRelative(kickFoot, torso);

                zLeft = kickParams.zCom;
                zRight = zKickFoot;
            }

            double xLeftArm;
            double xRightArm;
            if (leftLegKicks) {
                xLeftArm = -leftFoot.x * kickParams.armsCompensationFactor;
                xRightArm = leftFoot.x * kickParams.armsCompensationFactor;
            } else {
                xLeftArm = rightFoot.x * kickParams.armsCompensationFactor;
                xRightArm = -rightFoot.x * kickParams.armsCompensationFactor;
            }

            jointsTargets.leftShoulderPitch = M_PI / 2.0f
                                              - asin(xLeftArm / physicalParams.armLength);
            jointsTargets.rightShoulderPitch = M_PI / 2.0f
                                               - asin(xRightArm / physicalParams.armLength);

            inverseKinematics->computeJointsTargets(leftFoot.x, leftFoot.y, zLeft,
                                                    leftFoot.z, rightFoot.x, rightFoot.y, zRight, rightFoot.z,
                                                    jointsTargets);
        }

        void KickZMP::changeState() {
            switch (state) {
                case MOVING_ZMP_TO_SS:
                    if (inStateTime > kickParams.moveZmpDuration) {
                        state = TAKING_FOOT_OFF_THE_GROUND;
                        inStateTime -= kickParams.moveZmpDuration;
                    }
                    break;
                case TAKING_FOOT_OFF_THE_GROUND:
                    if (inStateTime > kickParams.takeFootDuration) {
                        state = KICKING;
                        inStateTime -= kickParams.takeFootDuration;
                    }
                    break;
                case KICKING:
                    if (inStateTime > kickParams.kickDuration) {
                        state = PUTTING_FOOT_ON_THE_GROUND;
                        inStateTime -= kickParams.kickDuration;
                    }
                    break;
                case PUTTING_FOOT_ON_THE_GROUND:
                    if (inStateTime > kickParams.takeFootDuration) {
                        state = MOVING_ZMP_TO_DS;
                        inStateTime -= kickParams.takeFootDuration;
                    }
                    break;
            }
        }

        void KickZMP::moveZmpToSS() {
            kickFoot.x = 0.0f;
            zKickFoot = kickParams.zCom;
        }

        void KickZMP::takeFootOffTheGround() {
            double t = inStateTime / kickParams.takeFootDuration;

            double takeFrac = computeTakeFrac(t);

            double zBegin = kickParams.zCom;
            double zEnd = kickParams.zCom - kickParams.zFoot;
            zKickFoot = MathUtils::linearInterpol(zBegin, zEnd, takeFrac);

            double xBegin = 0.0f;
            double xEnd = -kickParams.kickAmplitude;
            kickFoot.x = MathUtils::linearInterpol(xBegin, xEnd, takeFrac);
        }

        void KickZMP::kick() {
            double t = inStateTime / kickParams.kickDuration;

            zKickFoot = kickParams.zCom - kickParams.zFoot;

            double kickFrac = computeKickFrac(t);

            double xBegin = -kickParams.kickAmplitude;
            double xEnd = kickParams.kickAmplitude;
            kickFoot.x = MathUtils::linearInterpol(xBegin, xEnd, kickFrac);
        }

        void KickZMP::putFootOnTheGround() {
            double t = inStateTime / kickParams.takeFootDuration;

            double takeFrac = computeTakeFrac(t);

            double zBegin = kickParams.zCom - kickParams.zFoot;
            double zEnd = kickParams.zCom;
            zKickFoot = MathUtils::linearInterpol(zBegin, zEnd, takeFrac);

            double xBegin = kickParams.kickAmplitude;
            double xEnd = 0.0f;
            kickFoot.x = MathUtils::linearInterpol(xBegin, xEnd, takeFrac);
        }

        void KickZMP::moveZmpToDS() {
            kickFoot.x = 0.0f;
            zKickFoot = kickParams.zCom;
        }

        double KickZMP::computeTakeFrac(double t) {
            double t1 = kickParams.accelerationMoveRatio / 2.0f;
            double t2 = 1.0f - t1;

            if (t < t1)
                return 0.0f;
            if (t < t2) {
                return 0.5f * (1.0f - cos(M_PI * (t - t1) / (t2 - t1)));
            }
            return 1.0f;
        }

        double KickZMP::computeKickFrac(double t) {
            double f = kickParams.accelerationMoveRatio / 2.0f;

            if (t < f) {
                return (a1 * t * t);
            } else if (t < (1.0f - f)) {
                return (a2 * t + b2);
            } else {
                return (a3 * t * t + b3 * t + c3);
            }
        }

        void KickZMP::solveZMP(double zs, double z1, double z2, double x1, double x2,
                               double &aP, double &aN) {
            double T1 = kickParams.moveZmpDuration;
            double T2 = totalDuration - kickParams.moveZmpDuration;

            double m1 = (zs - z1) / T1;
            double m2 = -(zs - z2) / (totalDuration - T2);

            double c1 = x1 - z1 + tZMP * m1 * sinh(-T1 / tZMP);
            double c2 = x2 - z2 + tZMP * m2 * sinh((totalDuration - T2) / tZMP);
            double expTStep = exp(totalDuration / tZMP);
            aP = (c2 - c1 / expTStep) / (expTStep - 1 / expTStep);
            aN = (c1 * expTStep - c2) / (expTStep - 1 / expTStep);
        }

// TODO: Organize this code
        double KickZMP::getCoM(double t, double aP, double aN, double zs, double z1,
                               double z2) {
            double t1 = kickParams.moveZmpDuration / totalDuration;
            double t2 = 1.0f - t1;

            double expT = exp(totalDuration * t / tZMP);
            double com = aP * expT + aN / expT;

            double m1 = (zs - z1) / (t1 * totalDuration);
            double m2 = -(zs - z2) / (totalDuration - (t2 * totalDuration));

            if (t < t1)
                com += m1 * totalDuration * (t - t1)
                       - tZMP * m1 * sinh(totalDuration * (t - t1) / tZMP);
            else if (t > t2)
                com += m2 * totalDuration * (t - t2)
                       - tZMP * m2 * sinh(totalDuration * (t - t2) / tZMP);

            return com;
        }

        Vector3<double> KickZMP::poseGlobal(Vector3<double> poseRelative,
                                            Vector3<double> pose) {
            double ca = cos(pose.z);
            double sa = sin(pose.z);
            double x = pose.x + ca * poseRelative.x - sa * poseRelative.y;
            double y = pose.y + sa * poseRelative.x + ca * poseRelative.y;
            double z = pose.z + poseRelative.z;

            return Vector3<double>(x, y, MathUtils::normalizeAngle(z));
        }

        Vector3<double> KickZMP::poseRelative(Vector3<double> poseGlobal,
                                              Vector3<double> pose) {
            double ca = cos(pose.z);
            double sa = sin(pose.z);
            double px = poseGlobal.x - pose.x;
            double py = poseGlobal.y - pose.y;
            double pa = poseGlobal.z - pose.z;
            return Vector3<double>(ca * px + sa * py, -sa * px + ca * py,
                                   MathUtils::normalizeAngle(pa));
        }

        bool KickZMP::hasKicked() {
            return (totalTime > totalDuration);
        }

    } /* namespace kick */
} /* namespace control */
