//
// Created by francisco on 6/5/16.
//

#include "KickParameters.h"


namespace decision_making {
    namespace behavior {
        namespace kick_behavior {

            KickParameters::KickParameters(Vector2<double> relativeSelfPosition, double selfOrientation)
                    : relativeSelfPosition(relativeSelfPosition), selfOrientation(selfOrientation) {

            }

            const Vector2<double> &KickParameters::getRelativeSelfPosition() {
                return relativeSelfPosition;
            }

            const double &KickParameters::getSelfOrientation() {
                return selfOrientation;
            }

            KickParameters::KickParameters() {

            }


        }
    }
}