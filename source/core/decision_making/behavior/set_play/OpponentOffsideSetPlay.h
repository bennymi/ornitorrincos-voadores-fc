//
// Created by dicksiano on 8/14/16.
//

#ifndef ITANDROIDS_SOCCER3D_CPP_OPPONENTOFFSIDESETPLAY_H
#define ITANDROIDS_SOCCER3D_CPP_OPPONENTOFFSIDESETPLAY_H

#include "../SetPlay.h"

namespace decision_making {
    namespace behavior {

        class OpponentOffsideSetPlay : public SetPlay {
        public:

            /**
            * Default constructor.
            */
            OpponentOffsideSetPlay();

            /**
            * Assignment constructor.
            *
            * @param pointer to BehaviorFactory.
            */
            OpponentOffsideSetPlay(BehaviorFactory *behaviorFactory);

            /**
            * Destructor.
            */
            virtual ~OpponentOffsideSetPlay();

            /**
            * This function controls the actions.
            *
            * @param modeling.
            */
            void behave(modeling::Modeling &modeling);

        private:

        };

    } /* namespace behavior */
} /* namespace decision_making */




#endif //ITANDROIDS_SOCCER3D_CPP_OPPONENTOFFSIDESETPLAY_H
