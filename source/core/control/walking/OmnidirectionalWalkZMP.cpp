/*
 * OminidirecionalWalkZMP.cpp
 *
 *  Created on: Oct 10, 2015
 *      Author: mmaximo
 */

#include <limits>
#include <math.h>

#include "math/MathUtils.h"
#include "control/walking/OmnidirectionalWalkZMP.h"

namespace control {
    namespace walking {

        using namespace utils;

        const double OmnidirectionalWalkZMP::DEFAULT_LEFT_SHOULDER_YAW_OFFSET = 20.0
                                                                                * M_PI / 180.0;
        const double OmnidirectionalWalkZMP::DEFAULT_LEFT_ARM_ROLL_OFFSET = -90.0 * M_PI
                                                                            / 180.0;
        OmnidirectionalWalkZMPParams OmnidirectionalWalkZMPParams::normalParams;
        OmnidirectionalWalkZMPParams OmnidirectionalWalkZMPParams::preciseParams;

        OmnidirectionalWalkZMPParams::OmnidirectionalWalkZMPParams() :
                period(0.0), zCom(0.0), zStep(0.0), doubleSupportRatio(0.0), ySeparation(
                0.0), armsCompensationFactor(0.0) {
        }

        OmnidirectionalWalkZMPParams::OmnidirectionalWalkZMPParams(double period,
                                                                   double zCom, double zStep, double doubleSuportRatio,
                                                                   double ySeparation,
                                                                   double armsCompensationFactor) :
                period(period), zCom(zCom), zStep(zStep), doubleSupportRatio(
                doubleSuportRatio), ySeparation(ySeparation), armsCompensationFactor(
                armsCompensationFactor) {

        }

        OmnidirectionalWalkZMPParams
        OmnidirectionalWalkZMPParams::getOmnidirectionalWalkZMPParams(OmnidirectionalWalkZMParamsType type) {
            if (type == OmnidirectionalWalkZMParamsType::NORMAL) {
                return normalParams;
            } else if (type == OmnidirectionalWalkZMParamsType::PRECISE) {
                return preciseParams;
            }

        }

        OmnidirectionalWalkZMP::OmnidirectionalWalkZMP(
                OmnidirectionalWalkZMPParams walkParams,
                RobotPhysicalParameters physicalParams,
                InverseKinematics *inverseKinematics) :
                GRAVITY(9.80665), physicalParams(
                physicalParams), inverseKinematics(inverseKinematics), walkTime(
                0.0) {
            this->walkParams = std::make_unique<control::walking::OmnidirectionalWalkZMPParams>(walkParams);
            tZMP = sqrt(this->walkParams->zCom / GRAVITY);
            ALPHA = 0.9;
            restart();
        }

        OmnidirectionalWalkZMP::~OmnidirectionalWalkZMP() {
        }

        void OmnidirectionalWalkZMP::setDesiredVelocity(Pose2D commandedSpeed) {
            this->commandedSpeed = commandedSpeed;
        }

        void OmnidirectionalWalkZMP::restart() {
            walkTime = 0.0;
            leftFootIsSwing = false;
            torsoBegin.translation.x = 0.0;
            torsoBegin.translation.y = -walkParams->ySeparation;
            torsoBegin.rotation = 0.0;
            torsoEnd.translation.x = 0.0;
            torsoEnd.translation.y = -walkParams->ySeparation;
            torsoEnd.rotation = 0.0;
            swingFootBegin.translation.x = 0.0;
            swingFootBegin.translation.y = torsoBegin.translation.y - walkParams->ySeparation;
            swingFootBegin.rotation = 0.0;
            swingFootEnd.translation.x = 0.0;
            swingFootEnd.translation.y = torsoEnd.translation.y - walkParams->ySeparation;
            swingFootEnd.rotation = 0.0;
            odometry.translation.x = 0.0;
            odometry.translation.y = 0.0;
            odometry.rotation = 0.0;
            speed = Pose2D(0.0, 0.0, 0.0);
            previousTorso = torsoBegin;

            stoppingState = NONE;

        }

        void OmnidirectionalWalkZMP::update(double elapsedTime, NaoJoints &joints) {
            walkTime += elapsedTime;

            if (stoppingState == STOPPED)
                walkTime = 0.0;

            if (walkTime >= walkParams->period) {
                walkTime -= walkParams->period;
                computeNewStep();
            }

            double t = walkTime / walkParams->period;

            double aPx;
            double aNx;
            double aPy;
            double aNy;

            solveZMP(0.0, torsoBegin.translation.x, torsoEnd.translation.x, torsoBegin.translation.x,
                     torsoEnd.translation.x, aPx, aNx);
            solveZMP(0.0, torsoBegin.translation.y, torsoEnd.translation.y, torsoBegin.translation.y,
                     torsoEnd.translation.y, aPy, aNy);

            double comX = getCoM(t, aPx, aNx, 0.0, torsoBegin.translation.x, torsoEnd.translation.x);
            double comY = getCoM(t, aPy, aNy, 0.0, torsoBegin.translation.y, torsoEnd.translation.y);

            computeTorsoToAttainCoM(t, comX, comY, joints);

//            balance(agentModel, joints);
//            double xFrac = computeXFrac(t);
//            double yFrac = computeYFrac(t);
//            double zFrac = computeZFrac(t);
//            double thetaFrac = computeThetaFrac(t);
//
//            double thetaTorso = torsoBegin.rotation + (torsoEnd.rotation - torsoBegin.rotation) * thetaFrac;
//
//            torso.translation.x = comX;
//            torso.translation.y = comY;
//            torso.rotation = thetaTorso;
//
//            odometry = poseRelative(torso, previousTorso);
//
//            previousTorso = torso;
//
//            double xSwingGlobal = swingFootBegin.translation.x
//                                  + (swingFootEnd.translation.x - swingFootBegin.translation.x) * xFrac;
//            double ySwingGlobal = swingFootBegin.translation.y
//                                  + (swingFootEnd.translation.y - swingFootBegin.translation.y) * yFrac;
//            double thetaSwingGlobal = swingFootBegin.rotation
//                                      + (swingFootEnd.rotation - swingFootBegin.rotation) * thetaFrac;
//
//            Pose2D swingFoot(thetaSwingGlobal, xSwingGlobal, ySwingGlobal);
//
//            swingFoot = poseRelative(swingFoot, torso);
//            Pose2D supportFoot = poseRelative(Pose2D(0.0, 0.0, 0.0),
//                                                       torso);
//
//            double zSupport = walkParams->zCom;
//            double zSwing = walkParams->zCom - zFrac * walkParams->zStep;
//
//            Pose2D leftFoot;
//            Pose2D rightFoot;
//            double zLeft;
//            double zRight;
//            if (leftFootIsSwing) {
//                leftFoot = swingFoot;
//                rightFoot = supportFoot;
//                zLeft = zSwing;
//                zRight = zSupport;
//            } else {
//                leftFoot = supportFoot;
//                rightFoot = swingFoot;
//                zLeft = zSupport;
//                zRight = zSwing;
//            }
//
//            double xLeftArm = rightFoot.translation.x * walkParams->armsCompensationFactor;
//            double xRightArm = leftFoot.translation.x * walkParams->armsCompensationFactor;
//
//            joints.leftShoulderPitch = M_PI / 2.0
//                                       - asin(xLeftArm / physicalParams.armLength);
//            joints.rightShoulderPitch = M_PI / 2.0
//                                        - asin(xRightArm / physicalParams.armLength);
//            joints.leftShoulderYaw = DEFAULT_LEFT_SHOULDER_YAW_OFFSET;
//            joints.rightShoulderYaw = -DEFAULT_LEFT_SHOULDER_YAW_OFFSET;
//            joints.leftArmRoll = DEFAULT_LEFT_ARM_ROLL_OFFSET;
//            joints.rightArmRoll = -DEFAULT_LEFT_ARM_ROLL_OFFSET;
//
//            inverseKinematics->computeJointsTargets(leftFoot.translation.x, leftFoot.translation.y, zLeft,
//                                                    leftFoot.rotation, rightFoot.translation.x,
//                                                    rightFoot.translation.y, zRight, rightFoot.rotation, joints);
        }


        void OmnidirectionalWalkZMP::computeTorsoToAttainCoM(double t, double xCoM, double yCoM, NaoJoints &joints) {
            Vector3<double> torsoCorrection;

            double xFrac = computeXFrac(t);
            double yFrac = computeYFrac(t);
            double zFrac = computeZFrac(t);
            double thetaFrac = computeThetaFrac(t);

            double thetaTorso = torsoBegin.rotation + (torsoEnd.rotation - torsoBegin.rotation) * thetaFrac;


            double xSwingGlobal = swingFootBegin.translation.x
                                  + (swingFootEnd.translation.x - swingFootBegin.translation.x) * xFrac;
            double ySwingGlobal = swingFootBegin.translation.y
                                  + (swingFootEnd.translation.y - swingFootBegin.translation.y) * yFrac;
            double thetaSwingGlobal = swingFootBegin.rotation
                                      + (swingFootEnd.rotation - swingFootBegin.rotation) * thetaFrac;

            int numIterations = 1;
            for (int i = 0; i < numIterations; ++i) {
                torso.translation.x = xCoM + torsoCorrection.x;
                torso.translation.y = yCoM + torsoCorrection.y;
                torso.rotation = thetaTorso;

                Pose2D swingFoot(thetaSwingGlobal, xSwingGlobal, ySwingGlobal);
                swingFoot = poseRelative(swingFoot, torso);
                Pose2D supportFoot = poseRelative(Pose2D(0.0, 0.0, 0.0),
                                                  torso);

                double zSupport = walkParams->zCom + torsoCorrection.z;
                double zSwing = walkParams->zCom + torsoCorrection.z - zFrac * walkParams->zStep;

                Pose2D leftFoot;
                Pose2D rightFoot;
                double zLeft;
                double zRight;
                if (leftFootIsSwing) {
                    leftFoot = swingFoot;
                    rightFoot = supportFoot;
                    zLeft = zSwing;
                    zRight = zSupport;
                } else {
                    leftFoot = supportFoot;
                    rightFoot = swingFoot;
                    zLeft = zSupport;
                    zRight = zSwing;
                }

                double xLeftArm = rightFoot.translation.x * walkParams->armsCompensationFactor;
                double xRightArm = leftFoot.translation.x * walkParams->armsCompensationFactor;

                joints.leftShoulderPitch = M_PI / 2.0
                                           - asin(xLeftArm / physicalParams.armLength);
                joints.rightShoulderPitch = M_PI / 2.0
                                            - asin(xRightArm / physicalParams.armLength);
                joints.leftShoulderYaw = DEFAULT_LEFT_SHOULDER_YAW_OFFSET;
                joints.rightShoulderYaw = -DEFAULT_LEFT_SHOULDER_YAW_OFFSET;
                joints.leftArmRoll = DEFAULT_LEFT_ARM_ROLL_OFFSET;
                joints.rightArmRoll = -DEFAULT_LEFT_ARM_ROLL_OFFSET;

                inverseKinematics->computeJointsTargets(leftFoot.translation.x, leftFoot.translation.y, zLeft,
                                                        leftFoot.rotation, rightFoot.translation.x,
                                                        rightFoot.translation.y, zRight, rightFoot.rotation, joints);

                bodyModel.update(joints);
                Vector3<double> actualCoM = bodyModel.computeCenterOfMass();
//                actualCoM.y = -actualCoM.y;
                Pose2D actualCoMPose;
                actualCoMPose.translation = Vector2<double>(actualCoM.x, actualCoM.y);
                actualCoMPose = poseGlobal(actualCoMPose, torso);

                torsoCorrection.x = torsoCorrection.x + ALPHA * (xCoM - actualCoMPose.translation.x);
                torsoCorrection.y = torsoCorrection.y + ALPHA * (yCoM - actualCoMPose.translation.y);
                torsoCorrection.z = torsoCorrection.z +
                                    ALPHA * (walkParams->zCom - (walkParams->zCom + torsoCorrection.z + actualCoM.z));

//                if (i == numIterations - 1) {
//                    std::cout << "x: " << (xCoM - actualCoMPose.translation.x) / torsoCorrection.x << std::endl;
//
//                    std::cout << "y: " << (yCoM - actualCoMPose.translation.y) / torsoCorrection.y << std::endl;
//
//                    std::cout << "z: " <<
//                            (walkParams->zCom - (walkParams->zCom + torsoCorrection.z + actualCoM.z)) / torsoCorrection.z <<
//                        std::endl;
//                }
//                std::cout << "torso correction: " << torsoCorrection.x << " " << torsoCorrection.y << " " << torsoCorrection.z << std::endl;
            }

            odometry = poseRelative(torso, previousTorso);

            previousTorso = torso;
        }

        void OmnidirectionalWalkZMP::computeNewStep() {
            if (stoppingState == STOP_REQUESTED)
                stoppingState = ADJUSTING_1ST;
            else if (stoppingState == ADJUSTING_1ST)
                stoppingState = ADJUSTING_2ND;
            else if (stoppingState == ADJUSTING_2ND)
                stoppingState = STOPPED;

            double xIncrement = 0.2f;
            double yIncrement = 0.1;
            double zIncrement = MathUtils::degreesToRadians(60);
            if (speed.translation.x >= 0.8)
                xIncrement = 0.1f;

            speed.translation.x = speed.translation.x
                                  + 1.0
                                    * MathUtils::saturate(commandedSpeed.translation.x - speed.translation.x,
                                                          -xIncrement, xIncrement);
            speed.translation.y = speed.translation.y
                                  + 1.0 *
                                    MathUtils::saturate(commandedSpeed.translation.y - speed.translation.y, -yIncrement,
                                                        yIncrement);
            speed.rotation = speed.rotation
                             + 1.0
                               * MathUtils::saturate(commandedSpeed.rotation - speed.rotation,
                                                     -zIncrement, zIncrement);

            if (stoppingState == StoppingState::ADJUSTING_1ST
                || stoppingState == StoppingState::ADJUSTING_2ND) {
                speed.translation.x = 0.0f;
                speed.translation.y = 0.0f;
                speed.rotation = 0.0f;
            }

            switchSwingFoot();

            torsoBegin = poseRelative(torsoEnd, swingFootEnd);
            Pose2D supportFoot(0.0, 0.0, 0.0);
            swingFootBegin = poseRelative(supportFoot, swingFootEnd);
            previousTorso = poseRelative(previousTorso, swingFootEnd); // for odometry
            Pose2D torsoDisplacement = getDesiredTorsoDisplacement();
            torsoEnd = limitTorso(torsoBegin, torsoDisplacement);

            Pose2D torsoTwoSteps = torsoEnd
                                   + torsoDisplacement;

            bool oY = ((torsoDisplacement.translation.y >= 0 && leftFootIsSwing)
                       || (torsoDisplacement.translation.y < 0 && !leftFootIsSwing));
            bool oPsi = ((torsoDisplacement.rotation >= 0 && leftFootIsSwing)
                         || (torsoDisplacement.rotation < 0 && !leftFootIsSwing));

            Pose2D p;

            if (oPsi) {
                p = torsoTwoSteps;
            } else {
                p = torsoEnd;
            }

            swingFootEnd.rotation = 0.0;

            Pose2D torsoEndRelativeP = poseRelative(torsoEnd, p);
            Pose2D torsoTwoStepsRelativeP = poseRelative(torsoTwoSteps, p);

            double swingFootDisplacementY;
            if (leftFootIsSwing) {
                swingFootDisplacementY = walkParams->ySeparation;
            } else {
                swingFootDisplacementY = -walkParams->ySeparation;
            }

            if (oY) {
                swingFootEnd.translation.y = torsoTwoStepsRelativeP.translation.y + swingFootDisplacementY;
            } else {
                swingFootEnd.translation.y = torsoEndRelativeP.translation.y + swingFootDisplacementY;
            }

            double dx;
//            if (speed.rotation < std::numeric_limits<double>::epsilon()) {
            dx = walkParams->period * speed.translation.x;
//            } else {
//                double a = 2.0 * (speed.translation.x / speed.rotation)
//                           * sin(speed.rotation * walkParams->period / 2.0);
//                double b = 2.0 * (speed.translation.y / speed.rotation)
//                           * sin(speed.rotation * walkParams->period / 2.0);
//                double cossine = cos(speed.rotation * walkParams->period / 2.0);
//                double sine = sin(speed.rotation * walkParams->period / 2.0);
//
//                if (oPsi) {
//                    dx = a * cossine + b * sine;
//                } else {
//                    dx = a * cossine - b * sine;
//                }
//            }

            swingFootEnd.translation.x = torsoEndRelativeP.translation.x + dx / 2.0;

            swingFootEnd = poseGlobal(swingFootEnd, p);
        }

        Pose2D OmnidirectionalWalkZMP::limitTorso(Pose2D torsoBegin,
                                                  Pose2D torsoDisplacement) {
            Pose2D torsoEnd = torsoBegin + torsoDisplacement;

            if (leftFootIsSwing) {
                torsoEnd.translation.y = std::max(walkParams->ySeparation, torsoEnd.translation.y);
                torsoEnd.rotation = std::max(0.0, torsoEnd.rotation);
            } else {
                torsoEnd.translation.y = std::min(-walkParams->ySeparation,
                                                  torsoEnd.translation.y);//std::max(-walkParams->ySeparation, torsoEnd.y);
                torsoEnd.rotation = std::min(0.0, torsoEnd.rotation);//std::max(0.0, torsoEnd.z);
            }

            return torsoEnd;
        }

        void OmnidirectionalWalkZMP::switchSwingFoot() {
            leftFootIsSwing = !leftFootIsSwing;
        }

        Pose2D OmnidirectionalWalkZMP::getDesiredTorsoDisplacement() {
            Pose2D displacement;
//            if (speed.rotation < std::numeric_limits<double>::epsilon()) {
            displacement = speed * walkParams->period;
//            } else {
//                double a = 2.0 * (speed.translation.x / speed.rotation)
//                           * sin(speed.rotation * walkParams->period / 2.0);
//                double b = 2.0 * (speed.translation.y / speed.rotation)
//                           * sin(speed.rotation * walkParams->period / 2.0);
//                double cossine = cos(torsoBegin.rotation + speed.rotation * walkParams->period / 2.0);
//                double sine = sin(torsoBegin.rotation + speed.rotation * walkParams->period / 2.0);
//                displacement.translation.x = a * cossine - b * sine;
//                displacement.translation.y = a * sine + b * cossine;
//                displacement.rotation = walkParams->period * speed.rotation;
//            }
            return displacement;
        }

        void OmnidirectionalWalkZMP::solveZMP(double zs, double z1, double z2,
                                              double x1, double x2, double &aP, double &aN) {
            double T1 = walkParams->period * walkParams->doubleSupportRatio / 2.0;
            double T2 = walkParams->period - T1;

            double m1 = (zs - z1) / T1;
            double m2 = -(zs - z2) / (walkParams->period - T2);

            double c1 = x1 - z1 + tZMP * m1 * sinh(-T1 / tZMP);
            double c2 = x2 - z2 + tZMP * m2 * sinh((walkParams->period - T2) / tZMP);
            double expTStep = exp(walkParams->period / tZMP);
            aP = (c2 - c1 / expTStep) / (expTStep - 1 / expTStep);
            aN = (c1 * expTStep - c2) / (expTStep - 1 / expTStep);
        }

// TODO: Organize this code
        double OmnidirectionalWalkZMP::getCoM(double t, double aP, double aN, double zs,
                                              double z1, double z2) {
            double t1 = walkParams->doubleSupportRatio / 2.0;
            double t2 = 1.0 - t1;

            double expT = exp(walkParams->period * t / tZMP);
            double com = aP * expT + aN / expT;

            double m1 = (zs - z1) / (t1 * walkParams->period);
            double m2 = -(zs - z2) / (walkParams->period - (t2 * walkParams->period));

            if (t < t1)
                com += m1 * walkParams->period * (t - t1)
                       - tZMP * m1 * sinh(walkParams->period * (t - t1) / tZMP);
            else if (t > t2)
                com += m2 * walkParams->period * (t - t2)
                       - tZMP * m2 * sinh(walkParams->period * (t - t2) / tZMP);

            return com;
        }

        Pose2D OmnidirectionalWalkZMP::poseGlobal(Pose2D poseRelative,
                                                  Pose2D pose) {
            double ca = cos(pose.rotation);
            double sa = sin(pose.rotation);
            double x = pose.translation.x + ca * poseRelative.translation.x - sa * poseRelative.translation.y;
            double y = pose.translation.y + sa * poseRelative.translation.x + ca * poseRelative.translation.y;
            double z = pose.rotation + poseRelative.rotation;

            return Pose2D(MathUtils::normalizeAngle(z), x, y);
        }

        Pose2D OmnidirectionalWalkZMP::poseRelative(Pose2D poseGlobal,
                                                    Pose2D pose) {
            double ca = cos(pose.rotation);
            double sa = sin(pose.rotation);
            double px = poseGlobal.translation.x - pose.translation.x;
            double py = poseGlobal.translation.y - pose.translation.y;
            double pa = poseGlobal.rotation - pose.rotation;
            return Pose2D(MathUtils::normalizeAngle(pa), ca * px + sa * py, -sa * px + ca * py);
        }

        double OmnidirectionalWalkZMP::computeXFrac(double t) {
            double t1 = walkParams->doubleSupportRatio / 2.0;
            double t2 = 1.0 - t1;

            if (t < t1)
                return 0.0;
            if (t < t2) {
                return 0.5 * (1.0 - cos(M_PI * (t - t1) / (t2 - t1)));
            }
            return 1.0;
        }

        double OmnidirectionalWalkZMP::computeYFrac(double t) {
            double t1 = walkParams->doubleSupportRatio / 2.0;
            double t2 = 1.0 - t1;

            if (t < t1)
                return 0.0;
            if (t < t2) {
                return 0.5 * (1.0 - cos(M_PI * (t - t1) / (t2 - t1)));
            }
            return 1.0;
        }

        double OmnidirectionalWalkZMP::computeZFrac(double t) {
            double t1 = walkParams->doubleSupportRatio / 2.0;
            double t2 = 1.0 - t1;

            if (t > t1 && t < t2)
                return 0.5 * (1.0 - cos(2.0 * M_PI * (t - t1) / (t2 - t1)));
            return 0.0;
        }

        double OmnidirectionalWalkZMP::computeThetaFrac(double t) {
            double t1 = walkParams->doubleSupportRatio / 2.0;
            double t2 = 1.0 - t1;

            if (t < t1)
                return 0.0;
            if (t < t2) {
                return 0.5 * (1.0 - cos(M_PI * (t - t1) / (t2 - t1)));
            }
            return 1.0;
        }

        Pose2D OmnidirectionalWalkZMP::getOdometry() {
            return odometry;
        }

        void OmnidirectionalWalkZMP::requestStop() {
            if (stoppingState == NONE)
                stoppingState = STOP_REQUESTED;
        }

        bool OmnidirectionalWalkZMP::hasStopped() {
            return (stoppingState == STOPPED);
        }

        bool OmnidirectionalWalkZMP::isStopping() {
            return (stoppingState != NONE);
        }

        void OmnidirectionalWalkZMP::setWalkParams(const OmnidirectionalWalkZMPParams &params) {
            walkParams = std::make_unique<control::walking::OmnidirectionalWalkZMPParams>(params);
            tZMP = sqrt(this->walkParams->zCom / GRAVITY);
//            walkTime = 0;
//            restart();
        }

//        void OmnidirectionalWalkZMP::balance(AgentModel& agentModel, NaoJoints& joints) {
//            double hipPitchGain = -0.05;
//            double anklePitchGain = -0.03;
//            double hipRollGain = 0.05;
//            double ankleRollGain = 0.03;
//            joints.leftHipPitch += hipPitchGain * agentModel.getTorsoAngularVelocity().x;
//            joints.rightHipPitch += hipPitchGain * agentModel.getTorsoAngularVelocity().x;
//            joints.leftFootPitch += anklePitchGain * agentModel.getTorsoAngularVelocity().x;
//            joints.rightFootPitch += anklePitchGain * agentModel.getTorsoAngularVelocity().x;
//            joints.leftHipRoll += hipRollGain * agentModel.getTorsoAngularVelocity().y;
//            joints.rightHipRoll += hipRollGain * agentModel.getTorsoAngularVelocity().y;
//            joints.leftFootRoll += ankleRollGain * agentModel.getTorsoAngularVelocity().y;
//            joints.rightFootRoll += ankleRollGain * agentModel.getTorsoAngularVelocity().y;
//        }


    } /* namespace walking */
} /* namespace control */
