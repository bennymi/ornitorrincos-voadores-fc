//
// Created by muzio on 17/05/16.
//

#include "ObservationModel.h"
#include "geometry/GeometryUtils.h"

namespace modeling {
    namespace state_estimation {
        using itandroids_lib::geometry::Point2D;
        using itandroids_lib::geometry::GeometryUtils;

        ObservationModel::ObservationModel(double distanceVariance, double bearingVariance) :
                distanceVariance(distanceVariance), bearingVariance(bearingVariance) {

            //coordinates of actual lines
            actualLines.push_back(Line2D(Point2D(-15, 10), Point2D(15, 10)));
            actualLines.push_back(Line2D(Point2D(15, -10), Point2D(15, 10)));
            actualLines.push_back(Line2D(Point2D(-15, -10), Point2D(15, -10)));
            actualLines.push_back(Line2D(Point2D(-15, -10), Point2D(-15, 10)));
            actualLines.push_back(Line2D(Point2D(0, -10), Point2D(0, 10)));


            actualLines.push_back(Line2D(Point2D(13.2, 3), Point2D(13.2, -3)));
            actualLines.push_back(Line2D(Point2D(13.2, 3), Point2D(15, 3)));
            actualLines.push_back(Line2D(Point2D(13.2, -3), Point2D(15, -3)));
            actualLines.push_back(Line2D(Point2D(-13.2, 3), Point2D(-13.2, -3)));
            actualLines.push_back(Line2D(Point2D(-13.2, 3), Point2D(-15, 3)));
            actualLines.push_back(Line2D(Point2D(-13.2, -3), Point2D(-15, -3)));
        }

        double ObservationModel::calculateObservationLogProbabilityFromLandmarks(const Particle &particle,
                                                                                 const std::vector<LandmarkObservation> &landmarkObservations) {
            if (landmarkObservations.size() == 0) return -1e308;

            double logProbabilityFromLandmarks = 0.0;
            std::vector<LandmarkObservation>::const_iterator it;
            for (it = landmarkObservations.begin(); it != landmarkObservations.end(); ++it) {
                logProbabilityFromLandmarks += calculateObservationLogProbabilityFromOneLandmark(particle, *it);
            }

            return logProbabilityFromLandmarks;
        }

        double ObservationModel::calculateObservationLogProbabilityFromLines(const Particle &particle,
                                                                             const std::vector<Line2D> &observedLines) {
            double totalLogProb = 0.0;

            for (int i = 0; i < observedLines.size(); ++i) {
                double bestLogProb = -1e308;
                int bestMatching = -1;
                for (int j = 0; j < possibleActualLines.size(); ++j) {
                    double currentLogProb;
                    double sizeRatio = distancesBetweenActualAndObservedLines[j][i];
                    if (sizeRatio > 1.1) {
                        currentLogProb = -1e308;
                    } else {
                        Vector2<LandmarkObservation> LinesLandmarks = landmarksFromLinesUsingEstimation[j][i];

                        currentLogProb = calculateObservationLogProbabilityFromOneLandmark(particle, LinesLandmarks[0]);
                        currentLogProb += calculateObservationLogProbabilityFromOneLandmark(particle,
                                                                                            LinesLandmarks[1]);
                    }
                    if (currentLogProb > bestLogProb) {
                        bestLogProb = currentLogProb;
                        bestMatching = j;
                    }
                }

                totalLogProb += bestLogProb;
            }
            return totalLogProb;
        }

        double ObservationModel::calculateObservationLogProbabilityFromNegativeInfo(const Particle &particle,
                                                                                    const std::vector<LandmarkObservation> &observations) {
            double FIELD_OF_VIEW = 120 * M_PI / 180;
            std::vector<Line2D> lines;
            const Pose2D &particlePose = particle.getPose();

            Line2D delim1(Point2D(particlePose.translation.x,
                                  particlePose.translation.y),
                          particlePose.rotation + FIELD_OF_VIEW / 2.0);

            Line2D delim2(Point2D(particlePose.translation.x,
                                  particlePose.translation.y),
                          particlePose.rotation - FIELD_OF_VIEW / 2.0);

            int numBadLandmarks = 0;
            for (int i = 0; i < (int) observations.size(); i++) {
                const Point2D &p = observations[i].knownPosition;
                if (!GeometryUtils::pointIsBetweenLines(p, delim1, delim2)) {
                    numBadLandmarks += 1;
                }
            }

            return -numBadLandmarks * 20;
        }

        void ObservationModel::updateWithEstimate(const std::vector<Line2D> &observedLines, const Pose2D &estimate,
                                                  const representations::NaoJoints &jointsPosition) {
            calculatePossibleActualLines(estimate, jointsPosition);
            preComputeDistances(observedLines);
            preComputeLandmarksUsingCurrentEstimation(observedLines, estimate);
        }

        double ObservationModel::calculateObservationLogProbabilityFromOneLandmark(const Particle &particle,
                                                                                   const LandmarkObservation &observation) {
            const Pose2D &particlePose = particle.getPose();
            Vector2<double> vector = observation.knownPosition - particlePose.translation;
            double distanceDifference = vector.abs() - observation.distance;
            double bearingDifference = MathUtils::normalizeAngle(
                    (atan2(vector.y, vector.x) - particlePose.rotation)
                    - observation.bearing);
            double distanceProbability = -distanceDifference * distanceDifference / distanceVariance;
            double bearingProbability = -bearingDifference * bearingDifference / bearingVariance;

            return distanceProbability + bearingProbability;
        }

        Vector2<LandmarkObservation>
        ObservationModel::extractLandmarksFromLine(const Particle &currentParticle, const Line2D &actualLine,
                                                   const Line2D &observedLine) {
            const Vector2<double> &p1 = observedLine.getStart();
            const Vector2<double> &p2 = observedLine.getEnd();

            const Pose2D &currentParticlePose = currentParticle.getPose();
            Vector2<double> globalP1 = p1.relativeToGlobal(currentParticlePose.translation,
                                                           (float) currentParticlePose.rotation);
            Vector2<double> globalP2 = p2.relativeToGlobal(currentParticlePose.translation,
                                                           (float) currentParticlePose.rotation);
            Vector2<double> closestToP1 = actualLine.getPointOnLineClosestTo(globalP1);
            Vector2<double> closestToP2 = actualLine.getPointOnLineClosestTo(globalP2);

            extractedLandmark1.distance = p1.abs();
            extractedLandmark1.bearing = atan2(p1.y, p1.x);
            extractedLandmark1.knownPosition = closestToP1;
            extractedLandmark2.distance = p2.abs();
            extractedLandmark2.bearing = atan2(p2.y, p2.x);
            extractedLandmark2.knownPosition = closestToP2;

            linesLandmarks.x = extractedLandmark1;
            linesLandmarks.y = extractedLandmark2;
            return linesLandmarks;
        }

        void ObservationModel::preComputeDistances(const std::vector<Line2D> &observedLines) {

            for (int i = 0; i < possibleActualLines.size(); i++) {
                for (int j = 0; j < observedLines.size(); j++) {
                    distancesBetweenActualAndObservedLines[i][j] =
                            GeometryUtils::getDistanceTo(observedLines[j].getStart(),
                                                         observedLines[j].getEnd())
                            / GeometryUtils::getDistanceTo(possibleActualLines[i].getStart(),
                                                           possibleActualLines[i].getEnd());
                }
            }
        }

        void ObservationModel::preComputeLandmarksUsingCurrentEstimation(const std::vector<Line2D> &observedLines,
                                                                         const Pose2D &estimate) {
            Particle particleEstimate(estimate);
            for (int i = 0; i < possibleActualLines.size(); i++) {
                for (int j = 0; j < observedLines.size(); j++) {
                    landmarksFromLinesUsingEstimation[i][j] =
                            extractLandmarksFromLine(particleEstimate, possibleActualLines[i], observedLines[j]);
                }
            }
        }

        void ObservationModel::calculatePossibleActualLines(const Pose2D &estimatedPose,
                                                            const representations::NaoJoints &jointsPosition) {
            double FIELD_OF_VIEW = 120 * M_PI / 180;
            double THRESHOLD = 20 * M_PI / 180;
            double neckPan = jointsPosition.neckYaw;
            std::vector<Line2D> lines;

            Line2D delim1(Point2D(estimatedPose.translation.x,
                                  estimatedPose.translation.y),
                          (estimatedPose.rotation + neckPan) + (FIELD_OF_VIEW + THRESHOLD) / 2.0);

            Line2D delim2(Point2D(estimatedPose.translation.x,
                                  estimatedPose.translation.y),
                          (estimatedPose.rotation + neckPan) - (FIELD_OF_VIEW + THRESHOLD) / 2.0);

            for (int i = 0; i < actualLines.size(); i++) {
                Point2D p1 = actualLines[i].getStart();
                Point2D p2 = actualLines[i].getEnd();
                Point2D p3 = actualLines[i].getCenter();

                if (GeometryUtils::pointIsBetweenLines(p1, delim1, delim2) ||
                    GeometryUtils::pointIsBetweenLines(p2, delim1, delim2) ||
                    GeometryUtils::pointIsBetweenLines(p3, delim1, delim2))
                    lines.push_back(actualLines[i]);
            }

            possibleActualLines = lines;
        }

    }
}