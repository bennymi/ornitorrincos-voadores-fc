/*
 * SelfModel.h
 *
 *  Created on: Aug 29, 2015
 *      Author: mmaximo
 */

#ifndef SOURCE_MODELING_SELFMODEL_H_
#define SOURCE_MODELING_SELFMODEL_H_

#include "math/Vector3.h"
#include "modeling/state_estimation/Localizer.h"
#include "representations/Self.h"

namespace modeling {

    using namespace itandroids_lib::math;
    using namespace modeling::state_estimation;
    using namespace representations;

/**
 * Class that represents the self model.
 */
    class SelfModel {
    public:
        /**
         * Default constructor of SelfModel.
         *
         * @param fieldDescription the description of the components from the field.
         */
        SelfModel(FieldDescription &fieldDescription);

        /**
         * Default destructor of SelfModel.
         */

        virtual ~SelfModel();

        /**
         * Updates the self model properties.
         *
         * @param globalTime the current global time.
         * @param visibleFlags the position of visible flags.
         * @param visibleGoalPosts the position of visible goal posts.
         * @param visibleLines the position of visible lines.
         */

        void update(double globalTime,
                    const std::vector<perception::VisibleFlag *> &visibleFlags,
                    const std::vector<perception::VisibleGoalPost *> &visibleGoalPosts,
                    const std::vector<perception::VisibleLine *> &visibleLines,
                    Pose2D &odometry, representations::NaoJoints &jointsPosition);

        /**
         * Gets self object.
         *
         * @return self object.
         */

        Self &getObject();

        /**
         * Resets the position from localization.
         *
         * @param estimate the estimate.
         * @param sigmas the sigmas of localizer.
         */

        void resetLocalizer(Pose2D estimate, Pose2D sigmas);

    private:
        Localizer localizer;
        Self self;
    };

} /* namespace modeling */

#endif /* SOURCE_MODELING_SELFMODEL_H_ */
