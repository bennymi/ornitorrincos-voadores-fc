/*
 * Modeling.cpp
 *
 *  Created on: Aug 7, 2015
 *      Author: mmaximo
 */

#include "modeling/Modeling.h"
#include "control/Control.h"

namespace modeling {

    Modeling::Modeling(int agentNumber, std::string teamName) : agentModel(agentNumber), worldModel(teamName) {

    }

    Modeling::~Modeling() {
    }

    AgentModel &Modeling::getAgentModel() {
        return agentModel;
    }

    WorldModel &Modeling::getWorldModel() {
        return worldModel;
    }

    void Modeling::model(perception::Perception &perception, control::Control &control) {
        using namespace std;
        agentModel.update(perception.getAgentPerception());

        odometry = Pose2D(control.getWalkOdometry());
//        odometry.translation.x *= 0.7;
//        odometry.translation.y *= 0.9;
//        odometry.rotation *= 0.77;
        worldModel.update(perception.getAgentPerception(), agentModel, odometry);
    }

} /* namespace modeling */
