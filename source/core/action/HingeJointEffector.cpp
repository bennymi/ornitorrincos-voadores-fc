/*
 * HingeJointEffector.cpp
 *
 *  Created on: Aug 8, 2015
 *      Author: mmaximo
 */

#include "action/HingeJointEffector.h"

#include <sstream>

namespace action {

    HingeJointEffector::HingeJointEffector(const std::string &name, double axisSpeed) :
            name(name), axisSpeed(axisSpeed) {
    }

    HingeJointEffector::~HingeJointEffector() {
    }

    std::string HingeJointEffector::toString() {

        // Gets the joints's name and the axis speed as a string
        std::stringstream stream;
        stream << "(";
        stream << name;
        stream << " ";
        stream << axisSpeed;
        stream << ")";
        return stream.str();
    }

} /* namespace action */
