//
// Created by francisco on 6/5/16.
//

#ifndef ITANDROIDS_SOCCER3D_CPP_KICKPARAMETERS_H
#define ITANDROIDS_SOCCER3D_CPP_KICKPARAMETERS_H

#include "math/Vector2.h"

namespace decision_making {
    namespace behavior {
        namespace kick_behavior {
            using ::itandroids_lib::math::Vector2;

            class KickParameters {
            public:
                KickParameters();

                KickParameters(Vector2<double> relativeSelfPosition, double selfOrientation);

                const Vector2<double> &getRelativeSelfPosition();

                const double &getSelfOrientation();

            private:
                Vector2<double> relativeSelfPosition;
                double selfOrientation;
            };
        }
    }
}


#endif //ITANDROIDS_SOCCER3D_CPP_KICKPARAMETERS_H
