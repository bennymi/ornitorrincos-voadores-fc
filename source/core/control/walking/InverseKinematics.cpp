/*
 * InverseKinematics.cpp
 *
 *  Created on: Oct 10, 2015
 *      Author: mmaximo
 */

#include "InverseKinematics.h"

namespace control {
    namespace walking {

        InverseKinematics::InverseKinematics(RobotPhysicalParameters physicalParameters) :
                upperLegLength(physicalParameters.upperLegLength), lowerLegLength(
                physicalParameters.lowerLegLength), hipPitch0(
                physicalParameters.hipPitch0), kneePitch0(
                physicalParameters.kneePitch0), anklePitch0(
                physicalParameters.anklePitch0), physicalParameters(
                physicalParameters) {
        }

        InverseKinematics::~InverseKinematics() {
        }

        void InverseKinematics::computeJointsTargets(double xLeft, double yLeft,
                                                     double zLeft, double thetaLeft, double xRight, double yRight,
                                                     double zRight, double thetaRight, NaoJoints &jointsPositions) {
            xLeft -= physicalParameters.torsoToLeftHip.x;
            yLeft -= physicalParameters.torsoToLeftHip.y;
            xRight -= physicalParameters.torsoToLeftHip.x;
            yRight += physicalParameters.torsoToLeftHip.y;

            zLeft += physicalParameters.torsoToLeftHip.z;
            zRight += physicalParameters.torsoToLeftHip.z;

            double xLeftAux = xLeft;
            double yLeftAux = yLeft;
            xLeft = xLeftAux * cos(thetaLeft) + yLeftAux * sin(thetaLeft);
            yLeft = -xLeftAux * sin(thetaLeft) + yLeftAux * cos(thetaLeft);

            double xRightAux = xRight;
            double yRightAux = yRight;
            xRight = xRightAux * cos(thetaRight) + yRightAux * sin(thetaRight);
            yRight = -xRightAux * sin(thetaRight) + yRightAux * cos(thetaRight);

            xLeft -= physicalParameters.leftAnkleToLeftFoot.x;
            yLeft -= physicalParameters.leftAnkleToLeftFoot.y;
            xRight -= physicalParameters.leftAnkleToLeftFoot.x;
            yRight += physicalParameters.leftAnkleToLeftFoot.y;

            zLeft += physicalParameters.leftAnkleToLeftFoot.z;
            zRight += physicalParameters.leftAnkleToLeftFoot.z;

            double leftJointsTargets[6];
            double rightJointsTargets[6];
            computeJointsTargetsOneLeg(xLeft, yLeft, zLeft, thetaLeft,
                                       leftJointsTargets);
            computeJointsTargetsOneLeg(xRight, yRight, zRight, thetaRight,
                                       rightJointsTargets);

            setLeftLeg(leftJointsTargets, jointsPositions);
            setRightLeg(rightJointsTargets, jointsPositions);
        }

        void InverseKinematics::computeJointsTargetsOneLeg(double x, double y, double z,
                                                           double theta, double jointsTargets[]) {
            double yz = sqrt(y * y + z * z);
            double c = sqrt(x * x + y * y + z * z);
            double a = upperLegLength;
            double b = lowerLegLength;

            double gamma = acos((a * a + b * b - c * c) / (2.0f * a * b));
            double beta = acos((a * a + c * c - b * b) / (2.0f * a * c));
            double alpha = acos((b * b + c * c - a * a) / (2.0f * b * c));

            //printf("alpha: %f, beta: %f, gamma: %f\n", alpha * 180.0f / M_PI, beta * 180.0f / M_PI, gamma * 180.0f / M_PI);

            jointsTargets[0] = (-(atan2(x, yz) + beta) - hipPitch0);
            jointsTargets[1] = (M_PI - gamma - kneePitch0);
            jointsTargets[2] = (atan2(x, yz) - alpha - anklePitch0);
            jointsTargets[3] = atan2(y, z);
            jointsTargets[4] = -jointsTargets[3];
            jointsTargets[5] = theta;
        }

// double[] order: hipPitch, kneePitch,
// anklePitch, hipRoll, ankleRoll, hipYaw
        void InverseKinematics::setLeftLeg(double leftLeg[], NaoJoints &jointsPositions) {
            double z = leftLeg[5];
            double x = leftLeg[3];
            double y = leftLeg[0];
            double Rh[3][3];
            getHipRotationMatrix(z, x, y, Rh);

            double rp32 = (double) ((sqrt(2.0f) / 2.0f) * (-Rh[1][1] + Rh[2][1]));
            double rp12 = Rh[0][1];
            double rp22 = (double) ((sqrt(2.0f) / 2.0f) * (Rh[1][1] + Rh[2][1]));
            double rp31 = (double) ((sqrt(2.0f) / 2.0f) * (-Rh[1][0] + Rh[2][0]));
            double rp33 = (double) ((sqrt(2.0f) / 2.0f) * (-Rh[1][2] + Rh[2][2]));

            double hipRoll = (double) (asin(rp32) + M_PI / 4.0f);
            double hipYawPitch = (double) atan2(-rp12, rp22);
            double hipPitch = (double) atan2(-rp31, rp33);

            jointsPositions.leftHipPitch = hipPitch;
            jointsPositions.leftKneePitch = leftLeg[1];
            jointsPositions.leftFootPitch = leftLeg[2];
            jointsPositions.leftHipRoll = hipRoll;
            jointsPositions.leftFootRoll = leftLeg[4];
            jointsPositions.leftHipYawPitch = hipYawPitch;
        }

// double[] order: hipPitch, kneePitch,
// anklePitch, hipRoll, ankleRoll, hipYaw
        void InverseKinematics::setRightLeg(double rightLeg[], NaoJoints &jointsPositions) {
            double z = rightLeg[5];
            double x = rightLeg[3];
            double y = rightLeg[0];
            double Rh[3][3];
            getHipRotationMatrix(z, x, y, Rh);

            double rp32 = ((sqrt(2.0f) / 2.0f) * (Rh[1][1] + Rh[2][1]));
            double rp12 = Rh[0][1];
            double rp22 = ((sqrt(2.0f) / 2.0f) * (Rh[1][1] - Rh[2][1]));
            double rp31 = ((sqrt(2.0f) / 2.0f) * (Rh[1][0] + Rh[2][0]));
            double rp33 = ((sqrt(2.0f) / 2.0f) * (Rh[1][2] + Rh[2][2]));

            double hipRoll = (asin(rp32) - M_PI / 4.0f);
            double hipYawPitch = atan2(-rp12, rp22);
            double hipPitch = atan2(-rp31, rp33);

            jointsPositions.rightHipPitch = hipPitch;
            jointsPositions.rightKneePitch = rightLeg[1];
            jointsPositions.rightFootPitch = rightLeg[2];
            jointsPositions.rightHipRoll = hipRoll;
            jointsPositions.rightFootRoll = rightLeg[4];
            jointsPositions.rightHipYawPitch = hipYawPitch;
        }

        void InverseKinematics::getHipRotationMatrix(double z, double x, double y, double matrix[][3]) {
            double cx = cos(x);
            double sx = sin(x);
            double cy = cos(y);
            double sy = sin(y);
            double cz = cos(z);
            double sz = sin(z);

            matrix[0][0] = cy * cz - sx * sy * sz;
            matrix[0][1] = -cx * sz;
            matrix[0][2] = cz * sy + cy * sx * sz;
            matrix[1][0] = cz * sx * sy + cy * sz;
            matrix[1][1] = cx * cz;
            matrix[1][2] = -cy * cz * sx + sy * sz;
            matrix[2][0] = -cx * sy;
            matrix[2][1] = sx;
            matrix[2][2] = cx * cy;
        }

    }
/* namespace walking */
} /* namespace control */
