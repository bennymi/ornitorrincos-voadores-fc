/*
 * BoundingBox.cpp
 *
 *  Created on: Oct 4, 2015
 *      Author: mmaximo
 */

#include "BoundingBox.h"

namespace modeling {

    BoundingBox::BoundingBox(double length, double width, double height) :
            length(length), width(width), height(height) {
    }

    BoundingBox::BoundingBox(double height, double radius) :
            height(height), length(2.0 * radius), width(2.0 * radius) {
    }

    BoundingBox::BoundingBox(double radius) :
            length(2.0 * radius), width(2.0 * radius), height(2.0 * radius) {
    }

    BoundingBox::~BoundingBox() {
    }

    double BoundingBox::getLength() {
        return length;
    }

    double BoundingBox::getWidth() {
        return width;
    }

    double BoundingBox::getHeight() {
        return height;
    }

    void BoundingBox::transform(const RotationMatrix &rotationMatrix) {
        Vector3<double> box(length, width, height); // Construct a auxiliar box.
        box = rotationMatrix * box; // Transform box coordinates.
        length = fabs(box.x);
        width = fabs(box.y);
        height = fabs(box.z);
    }

} /* namespace modeling */
