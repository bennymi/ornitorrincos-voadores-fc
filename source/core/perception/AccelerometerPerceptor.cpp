/*
 * AccelerometerPerceptor.cpp
 *
 *  Created on: Aug 15, 2015
 *      Author: itandroids
 */

#include "AccelerometerPerceptor.h"

namespace perception {
    /**
     * AccelerometerPerceptor: This constructor takes a parser tree node and manipulates it to get information
     * of name and acceleration in x, y and z coordinates.
     */
    AccelerometerPerceptor::AccelerometerPerceptor(
            utils::parser::ParserTreeNode &node) :
            Perceptor(node) {
        //First children of the node is the name of perceptor
        std::string strName = utils::parser::KeyValuePair(node.children[0]->content,
                                                          ' ').value;
        name = strName;
        //Second children of node gives information about acceleration in x, y and z coordinates
        std::string accelerationString = utils::parser::KeyValuePair(
                node.children[1]->content, ' ').value;
        std::vector<std::string> stringVector;
        boost::split(stringVector, accelerationString, boost::is_any_of(" "));
        acceleration.x = boost::lexical_cast<double>(stringVector[0]);
        acceleration.y = boost::lexical_cast<double>(stringVector[1]);
        acceleration.z = boost::lexical_cast<double>(stringVector[2]);
    }

    AccelerometerPerceptor::~AccelerometerPerceptor() {
    }

    Vector3<double> AccelerometerPerceptor::getAcceleration() {
        //just return acceleration
        return acceleration;
    }

} /* namespace perception */
