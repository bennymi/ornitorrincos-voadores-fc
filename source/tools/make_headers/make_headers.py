def make_headers(rootDir, headerFilename):
    headerFile = open(headerFilename, 'r')
    headerLines = headerFile.readlines()
    print('%s\n' % headerLines)
    for dirName, subdirList, fileList in os.walk(rootDir):
        print('Found directory: %s' % dirName)
        for fname in fileList:
            fullname = os.path.join(dirName, fname)
            print('Filename: %s' % fullname)
            if os.path.isfile(fullname) and (fname.find('.h') != -1 or fname.find('.cpp') != -1):
                fr = open(fullname, 'r')
                lines = fr.readlines()
                for i in range(len(lines)):
                    line = lines[i]
                    # print('%s' % line)
                    if line.find('include') != -1 or line.find('ifndef') != -1:
                        lines = lines[i:]
                        break
                lines = headerLines + lines
                for line in lines:
                    sys.stdout.write('%s' % line)
                fw = open(fullname, 'w')
                for line in lines:
                    fw.write('%s' % line)


import os
import sys

argv = sys.argv
argc = len(argv)
# print('%s' % argv)
if argc < 3:
    print('Usage: <execute_command> <root_dir> <header_filename>')
rootDir = argv[1]
headerFilename = argv[2]
make_headers(rootDir, headerFilename)
