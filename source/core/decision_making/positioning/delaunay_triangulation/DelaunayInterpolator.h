/*
 * DelaunayInterpolator.h
 *
 *  Created on: Oct 28, 2015
 *      Author: itandroids
 */

#ifndef SOURCE_DECISION_MAKING_POSITIONING_DELAUNAY_TRIANGULATION_DELAUNAYINTERPOLATOR_H_
#define SOURCE_DECISION_MAKING_POSITIONING_DELAUNAY_TRIANGULATION_DELAUNAYINTERPOLATOR_H_

#include "geometry/DelaunayTriangulation.h"
#include "math/Vector2.h"
#include "modeling/WorldModel.h"
#include <vector>

using ::itandroids_lib::geometry::DelaunayTriangulation;

namespace decision_making {
    namespace positioning {
        namespace delaunay_triangulation {
            /**
             * Class that represent the Delaunay Interpolator.
             * It interpolate new positions according to predetermined positions and ball's position.
             */
            class DelaunayInterpolator {
            public:
                /**
                 * Constructor to Delaunay Interpolator
                 *
                 * @param pointer to triangulations
                 * @param a set of predefined positions
                 */
                DelaunayInterpolator(std::unique_ptr<DelaunayTriangulation> &&triangulation,
                                     std::vector<std::vector<itandroids_lib::geometry::Vector2D> > predefinedPositions);
                /**
                 * Default destructor for Delaunauy Interpolator
                 */
                virtual ~DelaunayInterpolator();

                /**
                 * Method that interpolate the positions
                 *
                 * @param Vector2 with ball's position
                 * @return the positions that are interpolated
                 */
                std::vector<itandroids_lib::math::Vector2<double> > interpolate(itandroids_lib::math::Vector2<double>);

            private:
                std::vector<std::vector<itandroids_lib::geometry::Vector2D> > predefinedPositions;
                std::unique_ptr<DelaunayTriangulation> triangulation;

            };

        } /* namespace delaunay_triangulation */
    } /* namespace positioning */
} /* namespace decision_making */

#endif /* SOURCE_DECISION_MAKING_POSITIONING_DELAUNAY_TRIANGULATION_DELAUNAYINTERPOLATOR_H_ */
