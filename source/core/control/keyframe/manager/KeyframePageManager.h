//
// Created by francisco on 18/12/16.
//

#ifndef ITANDROIDS_SOCCER3D_CPP_KEYFRAMEPAGEMANAGER_H
#define ITANDROIDS_SOCCER3D_CPP_KEYFRAMEPAGEMANAGER_H

#include "utils/patterns/Configurable.h"
#include <boost/property_tree/ptree.hpp>
#include "control/keyframe/page/KeyframePage.h"
#include "control/keyframe/page/ComplexKeyframePage.h"


namespace pt = boost::property_tree;
using itandroids_lib::utils::patterns::Parameter;
using itandroids_lib::utils::patterns::Configurable;
using ::control::keyframe::KeyframePage;
using ::control::keyframe::ComplexKeyframePage;

namespace control{
namespace keyframe{
namespace manager{
    /**
     * This class servers as a container for pages
     */
    class KeyframePageManager : Configurable<KeyframePageManager>{
    public:


    template<class Content, class Key>
    void setParameter(Parameter<Content,Key> & parameter);


    std::shared_ptr<KeyframePage> getPage(std::string pageName);
    private:
    template<class Content>
    bool isComplexPage(Content & content);

    std::map<std::string, std::shared_ptr<KeyframePage>> pages;

    };
    /**
     * Property tree specialization of the Parameter tree
     * @param parameter
     */
    template<>
    void KeyframePageManager::setParameter<pt::ptree, std::string>(Parameter<pt::ptree, std::string> &parameter);

    template<>
    bool KeyframePageManager::isComplexPage(pt::ptree & content);



}

}
}


#endif //ITANDROIDS_SOCCER3D_CPP_KEYFRAMEPAGEMANAGER_H
