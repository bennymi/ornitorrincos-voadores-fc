//
// Created by dicksiano on 8/14/16.
//

#ifndef ITANDROIDS_SOCCER3D_CPP_OWNOFFSIDESETPLAY_H
#define ITANDROIDS_SOCCER3D_CPP_OWNOFFSIDESETPLAY_H

#include "../SetPlay.h"

namespace decision_making {
    namespace behavior {

        class OwnOffsideSetPlay : public SetPlay {
        public:

            /**
            * Default constructor.
            */
            OwnOffsideSetPlay();

            /**
            * Assignment constructor.
            *
            * @param pointer to BehaviorFactory.
            */
            OwnOffsideSetPlay(BehaviorFactory *behaviorFactory);

            /**
            * Destructor.
            */
            virtual ~OwnOffsideSetPlay();

            /**
            * This function controls the actions.
            *
            * @param modeling.
            */
            void behave(modeling::Modeling &modeling);

        private:

        };

    } /* namespace behavior */
} /* namespace decision_making */


#endif //ITANDROIDS_SOCCER3D_CPP_OWNOFFSIDESETPLAY_H
