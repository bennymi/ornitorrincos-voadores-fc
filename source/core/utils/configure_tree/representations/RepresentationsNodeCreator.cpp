//
// Created by francisco on 19/12/16.
//

#include "RepresentationsNodeCreator.h"
#include "NavigationParamsNodeCreator.h"

namespace utils{
namespace configure_tree{
namespace representations{
template<>
std::shared_ptr<Parameter<pt::ptree,std::string>> RepresentationsNodeCreator::generateNode(){
    auto representationsNode = std::make_shared<BranchParameter<pt::ptree, std::string>>();
    representationsNode->addChild("navigationParams", NavigationParamsNodeCreator().generateNode<pt::ptree,std::string>());
    return representationsNode;
};
}
}
}