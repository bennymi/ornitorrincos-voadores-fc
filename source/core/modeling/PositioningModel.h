//
// Created by luckeciano on 9/27/16.
//

#ifndef ITANDROIDS_SOCCER3D_CPP_POSITIONINGMODEL_H
#define ITANDROIDS_SOCCER3D_CPP_POSITIONINGMODEL_H

#include <vector>
#include <core/representations/Ball.h>
#include <core/representations/OtherPlayer.h>
#include <core/representations/HearData.h>


namespace modeling {

    using namespace representations;

    class PositioningModel {
    public:
        /**
         * Default constructor of PositioningModel
         */
        PositioningModel();

        /**
         * Updates the positioning model.
         *
         * @param globalTime current global time.
         * @param hear data.
         */

        void update(double globalTime, representations::HearData hearData, std::string ourTeamName);

        /**
         * Get agent's role assignment vector.
         */

        std::vector<int> getMyRoleAssignmentVector();

        /**
         * Get sync role assignment vector.
         */

        std::vector<int> getSynchronizedRoleAssignmentVector();

        /**
         * Get teammates role assignment vector
         */
        std::vector<std::vector<int> > getTeammatesRoleAssignmentVector();

        /**
         * Sets my role assignment vector
         */
        void setMyRoleAssignmentVector(std::vector<int> newRoleVector);

    private:
        std::vector<int> myRoleAssignmentVector;
        std::vector<std::vector<int> > teammatesRoleAssignmentVector;
        std::vector<int> synchronizedRoleAssignmentVector;
        bool completePositioning;

    };


} /* namespace modeling */
#endif //ITANDROIDS_SOCCER3D_CPP_POSITIONINGMODEL_H
