//
// Created by muzio on 30/05/16.
//

#ifndef ITANDROIDS_SOCCER3D_CPP_GOALIEROLE_H
#define ITANDROIDS_SOCCER3D_CPP_GOALIEROLE_H

#include "Role.h"

namespace decision_making {
    namespace role {

        class GoalieRole : public Role {
        public:
            /**
             * Default constructor for the Goalie Role
             */
            GoalieRole();

            /**
             * Default destructor for the Goalie Role
             */
            virtual ~GoalieRole();

        private:
            void behaveMovement(modeling::Modeling &modeling);
        };

    } /* namespace role */
} /* namespace decision_making */


#endif //ITANDROIDS_SOCCER3D_CPP_GOALIEROLE_H
