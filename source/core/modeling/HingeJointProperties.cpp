/*
 * HingeJointProperties.cpp
 *
 *  Created on: Oct 4, 2015
 *      Author: mmaximo
 */

#include "HingeJointProperties.h"

namespace modeling {

    HingeJointProperties::HingeJointProperties() : name(""), type(NaoJoints::INVALID),
                                                   axis(Vector3<double>(0.0, 0.0, 0.0)), anchor(
                    Vector3<double>(0.0, 0.0, 0.0)), minAngle(0.0), maxAngle(0.0) {
    }

    HingeJointProperties::HingeJointProperties(std::string name,
                                               NaoJoints::HINGE_JOINTS hingeJointType, Vector3<double> anchor,
                                               Vector3<double> axis, double minAngle, double maxAngle) :
            name(name), type(hingeJointType), anchor(anchor), axis(axis), minAngle(
            minAngle), maxAngle(maxAngle) {
    }

} /* namespace modeling */
