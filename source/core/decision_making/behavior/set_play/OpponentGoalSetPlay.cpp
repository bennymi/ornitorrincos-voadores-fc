//
// Created by dicksiano on 8/14/16.
//

#include "OpponentGoalSetPlay.h"
#include "../BehaviorFactory.h"

namespace decision_making {
    namespace behavior {

        OpponentGoalSetPlay::OpponentGoalSetPlay() : SetPlay(nullptr) {
        }

        OpponentGoalSetPlay::OpponentGoalSetPlay(BehaviorFactory *behaviorFactory) : SetPlay(behaviorFactory) {
        }

        OpponentGoalSetPlay::~OpponentGoalSetPlay() {
        }

        void OpponentGoalSetPlay::behave(modeling::Modeling &modeling) {
            /* Delaunay */
            behaviorFactory->getBeamBehavior().behave(modeling);
            beamRequest = behaviorFactory->getBeamBehavior().getBeamRequest();
        } /* function behave */

    } /* namespace behavior */
} /* namespace decision_making */