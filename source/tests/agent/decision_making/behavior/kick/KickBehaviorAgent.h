//
// Created by francisco on 6/6/16.
//

#ifndef ITANDROIDS_SOCCER3D_CPP_KICKBEHAVIORAGENT_H
#define ITANDROIDS_SOCCER3D_CPP_KICKBEHAVIORAGENT_H

#include "base/BaseRoleAgentTest.h"
#include "utils/DrawVisibleObjects.h"

class KickBehaviorAgent : public BaseRoleAgentTest {
public:
    using BaseRoleAgentTest::BaseRoleAgentTest;

    virtual void setup() override;

    virtual void runStep() override;

private:
    double time;
    DrawVisibleObjects draw;
    std::string buffer;

};


void KickBehaviorAgent::setup() {
    time = 0;
    wiz.setAgentPositionAndDirection(agentNumber, representations::PlaySide::LEFT, Vector3<double>(-1, 0.3, 0.3), 0);
    wiz.setBallPosition(Vector3<double>(-0, 0, 0.3));
    buffer = "animated";
}

void KickBehaviorAgent::runStep() {
    time += 0.02;
    if (time > 3) {
        auto &logger = itandroids_lib::utils::LogSystem::getInstance().getLogger("positions");
        Vector2<double> target(modeling.getWorldModel().getTheirGoalPosition().x,
                               modeling.getWorldModel().getTheirGoalPosition().y);
        behaviorFactory.getKickBehavior().setTarget(target);
        behaviorFactory.getKickBehavior().behave(modeling);
        behaviorFactory.getActiveVision().behave(modeling);
        auto role = dynamic_cast<decision_making::role::RoleStub *>(roleStrategy->getRole(modeling));
//        role->lookRequest = behaviorFactory.getActiveVision().getLookRequest();
        role->lookRequest = behaviorFactory.getActiveVision().getLookRequest();
        role->movementRequest = behaviorFactory.getKickBehavior().getMovementRequest();
        decisionMaking->decide(modeling);
        draw.drawLineBallToAgent(modeling, roboviz);
        roboviz.swapBuffers(&buffer);
    }
}

#endif //ITANDROIDS_SOCCER3D_CPP_KICKBEHAVIORAGENT_H
