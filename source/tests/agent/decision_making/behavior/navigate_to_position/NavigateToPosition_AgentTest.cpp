//
// Created by francisco on 05/02/16.
//

#include "base/BaseAgentTest.h"

class NavigatoToPosition_AgentTest : public BaseAgentTest {
private:
    Pose2D poses[4];
    int i;
    double minPan;
    double maxPan;
    double panSpeed;
    double pan;

public:
    virtual void testLoop(int nSteps) { BaseAgentTest::testLoop(nSteps); }

    virtual void setup() {
        poses[0] = Pose2D(0.0, 5.0, 5.0);
        poses[1] = Pose2D(0.0, 5.0, -5.0);
        poses[2] = Pose2D(0.0, 0.0, 0.0);
        poses[3] = Pose2D(0.0, -5.0, 5.0);

        i = 0;
        minPan = -60.0;
        maxPan = 60.0;
        panSpeed = 40.0;
        pan = 0.0;
    }

    virtual void runStep() {
        std::string buff("animated");
        std::string name("animated");
        roboviz.drawLine(modeling.getWorldModel().getBall().getPosition().translation.x,
                         modeling.getWorldModel().getBall().getPosition().translation.y,
                         0, modeling.getWorldModel().getBall().getPosition().translation.x,
                         modeling.getWorldModel().getBall().getPosition().translation.y, 1, 1,
                         255, 255, 255, &name);
        roboviz.drawLine(modeling.getWorldModel().getSelf().getPosition().translation.x,
                         modeling.getWorldModel().getSelf().getPosition().translation.y,
                         0, modeling.getWorldModel().getSelf().getPosition().translation.x,
                         modeling.getWorldModel().getSelf().getPosition().translation.y, 1, 2,
                         255, 0, 0, &name);
        roboviz.swapBuffers(&buff);
        Pose2D target = poses[i];
        Pose2D position = modeling.getWorldModel().getSelf().getPosition();
        std::cout << "Target: " << target.translation.x << " " << target.translation.y << std::endl;
        std::cout << "Position: " << position.translation.x << " " <<
                  position.translation.y << std::endl;
        double distance = (target.translation - position.translation).abs();
        if (distance < 0.1) {
            ++i;
            i = i % 3;
            std::cout << "New target: " << i << std::endl;
        }
        behaviorFactory.getNavigatePosition().setTarget(target);
        behaviorFactory.getNavigatePosition().behave(modeling);
        decisionMaking.movementRequest = behaviorFactory.getNavigatePosition().getMovementRequest();
        decisionMaking.lookRequest = std::make_shared<control::LookRequest>(pan * M_PI / 180.0, 0.0);

        //head movement
        pan += panSpeed * 0.02;
        if (pan > maxPan) {
            pan = maxPan;
            panSpeed = -panSpeed;
        } else if (pan < minPan) {
            pan = minPan;
            panSpeed = -panSpeed;
        }
    }
};

int main() {
    NavigatoToPosition_AgentTest t;
    t.testLoop(50000);
}
