//
// Created by luckeciano on 8/10/16.
//

#ifndef ITANDROIDS_SOCCER3D_CPP_SCRAMBYMMDR_H
#define ITANDROIDS_SOCCER3D_CPP_SCRAMBYMMDR_H

#include "HungarianAlg.h"

typedef std::pair<double, double> Point;

// Edge looks like (dist, (left node, right node))
typedef std::pair<double, std::pair<int, int> > Edge;


namespace decision_making {
    namespace positioning {
        namespace role_assignment {

            class SCRAMByMMDR {

            public:
                //Priority Vectors to prioritized role assignment
                double priorityDistance[PLAYERS] = {0}; //distance to prioritized role assignment
                double priorityValue[PLAYERS] = {0}; //value of priorization

                inline double getDist(const Point &a, const Point &b) {
                    return hypot(a.first - b.first, a.second - b.second);
                }

                inline bool edgeVectorLessThan(const std::vector<Edge> &a, const std::vector<Edge> &b) {
                    const double ERROR_THRESH = .00005;
                    for (int i = 0; i < a.size(); i++) {
                        if (std::abs(a[i].first - b[i].first) < ERROR_THRESH) {
                            continue;
                        }
                        return a[i] < b[i];
                    }
                    return false;
                }
                //MMDR O(n⁵)

                class Test {
                private:
                    std::vector<Point> starts;
                    std::vector<Point> targets;

                public:
                    std::vector<Point> getStarts() {
                        return this->starts;
                    }

                    std::vector<Point> getTargets() {
                        return this->targets;
                    }

                    void addStart(Point p) {
                        starts.push_back(p);
                    }

                    void addTarget(Point q) {
                        targets.push_back(q);
                    }
                };

                void solvePriority(std::vector<int> finalTargets);

                std::vector<Edge> mmdrSolve();

                Test test;

            };
        }
    }
}


#endif //ITANDROIDS_SOCCER3D_CPP_SCRAMBYMMDR_H
