//
// Created by francisco on 5/29/16.
//

#ifndef ITANDROIDS_SOCCER3D_CPP_ACTIVEVISIONAGENT_H
#define ITANDROIDS_SOCCER3D_CPP_ACTIVEVISIONAGENT_H

#include <iostream>
#include "utils/Logger.h"
#include "utils/LogSystem.h"
#include "decision_making/behavior/ActiveVision.h"
#include "base/BaseAgentTest.h"
#include "utils/DrawVisibleObjects.h"
#include "ActiveVisionAgent.h"

using ::itandroids_lib::utils::LogSystem;
using ::itandroids_lib::utils::Logger;
using ::itandroids_lib::math::Vector3;
using ::itandroids_lib::math::Pose2D;
using ::itandroids_lib::math::Vector2;


class ActiveVisionAgent : public BaseAgentTest {
public:
    enum STATES {
        GOING_TO_FIRST_POINT,
        GOING_TO_SECOND_POINT,
        GOING_TO_THIRD_POINT,
        GOING_TO_FOURTH_POINT,
    };

    ActiveVisionAgent(std::string host = "127.0.0.1", int agentPort = 3100, int monitorPort = 3200, int agentNumber = 1,
                      int robotType = 4, std::string teamName = std::string("ITAndroids"));

    void runStep() override;

    void setup() override;

    void finish() override;

private:
    decision_making::behavior::ActiveVision activeVision;
    decision_making::behavior::BehaviorFactory factory;
    double distance;
    std::vector<Pose2D> poses;
    double dt;
    DrawVisibleObjects draw;
    STATES state;
    double ballMovementRadius;
    int counter;
};

ActiveVisionAgent::ActiveVisionAgent(std::string host, int agentPort, int monitorPort, int agentNumber,
                                     int robotType, std::string teamName) : activeVision(nullptr),
                                                                            state(ActiveVisionAgent::GOING_TO_FIRST_POINT),
                                                                            BaseAgentTest(host, agentPort, monitorPort,
                                                                                          agentNumber, robotType,
                                                                                          teamName) {
    poses.push_back(Pose2D(0, 10, -5));
    poses.push_back(Pose2D(M_PI_2, 10, 5));
    poses.push_back(Pose2D(-M_PI, -10, 5));
    poses.push_back(Pose2D(-M_PI_2, -10, -5));
    Logger &l = LogSystem::getInstance().getLogger("movement_data");
    distance = 0;
    dt = 0.02;
};

void ActiveVisionAgent::runStep() {
    counter++;
//    double ballX = ballMovementRadius * cos(counter * dt);
//    double ballY = ballMovementRadius * sin(counter * dt);
//    wiz.setBallPosition(Vector3<double>(ballX, ballY, 0));
//    ballMovementRadius += dt / 100;
    Logger &l = LogSystem::getInstance().getLogger("movement_data");
    activeVision.behave(modeling);
    decisionMaking.lookRequest = activeVision.getLookRequest();
    Vector2<double> real(wiz.getAgentTranslation().x, wiz.getAgentTranslation().y);
    distance += (real - modeling.getWorldModel().getSelf().getPosition().translation).abs();
    Pose2D &selfPosition = modeling.getWorldModel().getSelf().getPosition();
    Pose2D ballPosition = modeling.getWorldModel().getBall().getPosition();
    double ballRadius = 0.04;
    double robotVisionHeight = 0.545;
    Vector3<double> ballDirection(ballPosition.translation.x - selfPosition.translation.x, ballPosition.translation.y
                                                                                           - selfPosition.translation.y,
                                  ballRadius);
    l.log("percepted") << perception.getAgentPerception().getNaoJoints().neckYaw << " "
                       << perception.getAgentPerception().getNaoJoints().neckPitch;
    l.log("expected") << decisionMaking.lookRequest->getPan() << " " << decisionMaking.lookRequest->getTilt();

    /**
     * State machine for positioning on the field
     */

//    if(state == ActiveVisionAgent::GOING_TO_FIRST_POINT){
//        factory.getNavigatePosition().setTarget(poses[0]);
//        factory.getNavigatePosition().behave(modeling);
//        if((modeling.getWorldModel().getSelf().getPosition().translation - poses[0].translation).abs() < 0.1)
//            state = ActiveVisionAgent::GOING_TO_SECOND_POINT;
//    } else if (state == ActiveVisionAgent::GOING_TO_SECOND_POINT){
//        factory.getNavigatePosition().setTarget(poses[1]);
//        factory.getNavigatePosition().behave(modeling);
//        if((modeling.getWorldModel().getSelf().getPosition().translation - poses[1].translation).abs() < 0.1)
//            state = ActiveVisionAgent::GOING_TO_THIRD_POINT;
//    } else if (state == ActiveVisionAgent::GOING_TO_THIRD_POINT){
//        factory.getNavigatePosition().setTarget(poses[2]);
//        factory.getNavigatePosition().behave(modeling);
//        if((modeling.getWorldModel().getSelf().getPosition().translation - poses[2].translation).abs() < 0.1)
//            state = ActiveVisionAgent::GOING_TO_FOURTH_POINT;
//    } else {
//        factory.getNavigatePosition().setTarget(poses[3]);
//        factory.getNavigatePosition().behave(modeling);
//    }
//
//    decisionMaking.movementRequest = factory.getNavigatePosition().getMovementRequest();



    draw.draw(modeling.getWorldModel().getSelf().getPosition(), perception, modeling.getAgentModel(), roboviz);
    draw.drawVisionDirection(modeling, roboviz);
    std::string buffer("animated");
    draw.drawLineBallToAgent(modeling, roboviz);
    std::string name("animated.line");
    roboviz.drawLine(selfPosition.translation.x, selfPosition.translation.y, robotVisionHeight,
                     selfPosition.translation.x + ballDirection.x, selfPosition.translation.y + ballDirection.y,
                     ballDirection.z, 1, 35, 200, 87, &name);
    draw.drawLineToObstacles(modeling, roboviz);
    if (perception.getAgentPerception().getVisibleObjects().size() != 0) {

        roboviz.swapBuffers(&buffer);
    }


}

void ActiveVisionAgent::setup() {
    wiz.setAgentPositionAndDirection(1, representations::PlaySide::LEFT, Vector3<double>(-10, -5, 0.3), 0);
    wiz.setGameTime(0.02);
    wiz.setPlayMode(representations::RawPlayMode::PLAY_ON);
    counter = 0;
    ballMovementRadius = 0.5;
}

void ActiveVisionAgent::finish() {

}

#endif //ITANDROIDS_SOCCER3D_CPP_ACTIVEVISIONAGENT_H
