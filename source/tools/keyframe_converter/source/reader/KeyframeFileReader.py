# -*- coding: utf-8 -*-

from abc import ABC, abstractmethod


class KeyframeFileReader(ABC):

    def __init__(self, inputFile):
        self.inputFile = inputFile
        self.keyframeMovement = None
        self.interpolator = None

    @abstractmethod
    def readFile(self):
        pass

    def getKeyframeMovement(self):
        self.readFile()
        return self.keyframeMovement




