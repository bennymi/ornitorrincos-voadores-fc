//
// Created by francisco on 02/05/16.
//

#include "ActiveVision.h"

using modeling::FieldDescription;

namespace decision_making {
    namespace behavior {

        const double ActiveVision::TILT_MAX_LIMIT = 29.5 * M_PI / 180;
        const double ActiveVision::TILT_MIN_LIMIT = -38.5 * M_PI / 180;
        const double ActiveVision::PAN_LIMIT = 119.5 * M_PI / 180;
        const double ActiveVision::DISPERSION = 1.0;
        const double ActiveVision::SD = 1.0;
        const double ActiveVision::K0 = 1.0;
        const double ActiveVision::SD0 = 10.0;
        const std::string ActiveVision::ACTIVE_VISION_FILE_PATH = "../configuration/core/active_vision/default_active_vision.txt";

        ActiveVision::ActiveVision(decision_making::behavior::BehaviorFactory *factory) : LookBehavior(factory),
                                                                                          first(true) {
            parser.parse(ACTIVE_VISION_FILE_PATH);
            alpha = 2;
        }

        void ActiveVision::behave(modeling::Modeling &modeling) {
            if (lookRequest) {
                lookRequest = nullptr;
            }

            if (first) {
                currentHeadPan = 0.0;
                currentHeadTilt = 0.0;
                first = false;
            }
            currentHeadTilt = modeling.getAgentModel().getAgentPerception().getNaoJoints().neckPitch;
            currentHeadPan = modeling.getAgentModel().getAgentPerception().getNaoJoints().neckYaw;
            Pose2D selfPosition = modeling.getWorldModel().getSelf().getPosition();
            double omegaMax = 0.0, panMax = 0.0, tiltMax = 0.0;
            for (double tilt = active_vision::ActiveVisionFileParser::INITIAL_TILT; tilt <=
                                                                                    active_vision::ActiveVisionFileParser::FINAL_TILT; tilt += active_vision::ActiveVisionFileParser::DTILT) {
                for (double pan = active_vision::ActiveVisionFileParser::INITIAL_PAN; pan <=
                                                                                      active_vision::ActiveVisionFileParser::FINAL_PAN; pan += active_vision::ActiveVisionFileParser::DPAN) {
                    double headLimFun = headLimitFunction(pan, tilt);
                    double omegaLoc = 0;
                    if (headLimFun > 0) {
                        double headMovPen = headMovementPenalization(pan, tilt, currentHeadPan, currentHeadTilt);
                        omegaLoc = parser.getCostValue(selfPosition, pan, tilt) * headLimFun * headMovPen;
                        auto &visibleObjects = modeling.getAgentModel().getAgentPerception().getVisibleObjects();
                        for (auto &k : visibleObjects) {
                            if (k->getVisibleObjectType() == perception::VisibleObjectType::VISIBLE_PLAYER) {
                                double value = obstaclesPenalization(pan, tilt, modeling,
                                                                     (*dynamic_cast<perception::VisiblePlayer *>(k)));
                                omegaLoc *= value;
                            }
                        }
                        double omegaBall = ballFunction(pan, tilt, modeling);
                        double omegaTotal = omegaLoc + alpha * omegaBall;
                        if (omegaTotal > omegaMax) {
                            omegaMax = omegaTotal;
                            panMax = pan;
                            tiltMax = tilt;
                        }
                    }

                }
            }
            lookRequest = std::make_shared<control::LookRequest>(panMax, tiltMax);


        }

        double ActiveVision::headLimitFunction(double pan, double tilt) {
            if (pan > PAN_LIMIT || pan < -PAN_LIMIT || tilt > TILT_MAX_LIMIT || tilt < TILT_MIN_LIMIT) {
                return 0.0;
            }
            return 1.0;

        }

        double ActiveVision::headMovementPenalization(double pan, double tilt, double currentHeadPan,
                                                      double currentHeadTilt) {
            //Normalize constants
            double V = 1 / exp(DISPERSION);
            double G = 1;
            double misesFunction = V * exp(DISPERSION * cos(pan - currentHeadPan));
            double gaussianFunction = G * exp(-pow(tilt - currentHeadTilt, 2) / (2 * pow(SD, 2)));

            return misesFunction * gaussianFunction;

        }

        double ActiveVision::obstaclesPenalization(double pan, double tilt, modeling::Modeling &modeling,
                                                   perception::VisiblePlayer &obstacle) {
            Pose2D selfPosition = modeling.getWorldModel().getSelf().getPosition();
            Pose2D obstaclePosition = obstacle.getPosition();
            double robotHeight = 0.605;
            //Correction function k
            double k = obstacle.getDistance() / FieldDescription::FIELD_LENGTH;//Carteation


            //projecting obstacle in action space:

            //mean pan: angle between a vector with pose = 0 (vector [1,0] and the vector between self and obstacle)
            Vector2<double> poseVector(cos(selfPosition.rotation),
                                       sin(selfPosition.rotation)); // Vector (cosPsi,sinPsi)
            Vector2<double> obstacleDistance(obstaclePosition.translation.x - selfPosition.translation.x,
                                             obstaclePosition.translation.y - selfPosition.translation.y);
            obstacleDistance = obstacleDistance.normalize();
            double meanObstaclePan = acos(poseVector * obstacleDistance);
            if (meanObstaclePan < -PAN_LIMIT) {
                meanObstaclePan = -PAN_LIMIT;
            }
            if (meanObstaclePan > PAN_LIMIT) {
                meanObstaclePan = PAN_LIMIT;
            }

            //mean tilt: angle between vector [1,0] (x,z) and the vector between self and the center of obstacle
            // the numerator of atan is distance between the eyes and the center of the body
            double meanObstacleTilt = atan(
                    (0.5 * robotHeight - 0.50) / abs(selfPosition.translation.x - obstaclePosition.translation.x));
            if (meanObstacleTilt < TILT_MIN_LIMIT) {
                meanObstacleTilt = TILT_MIN_LIMIT;
            }
            if (meanObstacleTilt > TILT_MAX_LIMIT) {
                meanObstacleTilt = TILT_MAX_LIMIT;
            }




            //pPan approximation
            double pPan;
            double robotYDimension = 0.286, robotXDimension = 0.26;
            double beta = atan(0.5 * robotYDimension / (obstacle.getDistance() - robotXDimension));
            pPan = meanObstaclePan - beta;
            if (pPan > PAN_LIMIT) {
                pPan = PAN_LIMIT;
            }
            if (pPan < -PAN_LIMIT) {
                pPan = -PAN_LIMIT;
            }

            //qtilt approximation
            Vector2<double> centerOfObstacleXZ(obstaclePosition.translation.x, 0.5 * robotHeight);
            Vector2<double> robotHeightMidpoint(obstaclePosition.translation.x, robotHeight);
            Vector2<double> selfVisionPositionXZ(selfPosition.translation.x, robotHeight - 0.50);
            double qTilt = acos((centerOfObstacleXZ - selfVisionPositionXZ).normalize() * (robotHeightMidpoint -
                                                                                           selfVisionPositionXZ).normalize());
            if (qTilt > TILT_MAX_LIMIT) {
                qTilt = TILT_MAX_LIMIT;
            }

            if (qTilt < TILT_MIN_LIMIT) {
                qTilt = TILT_MIN_LIMIT;
            }

            //Calculating dispersion and variance
            double dispersionPan = 1 / (K0 * fabs(pPan - meanObstaclePan));
            double sigmaTilt = pow(SD0, 2) * pow(fabs(qTilt - meanObstacleTilt), 2);

            //Calculation Mises and Gaussian Functions
            double V = 1 / exp(dispersionPan);
            double G = 1.0;
            double misesFunction = V * exp(dispersionPan * cos(pan - meanObstaclePan));
            if (sigmaTilt < 1)
                sigmaTilt = 1;
            double gaussianFunction =
                    G * exp(-(tilt - meanObstacleTilt) * (tilt - meanObstacleTilt) / (2.0 * sigmaTilt * sigmaTilt));

            return k * (1 - misesFunction) * gaussianFunction;


        }

        double ActiveVision::ballFunction(double pan, double tilt, modeling::Modeling &modeling) {

            Pose2D &selfPosition = modeling.getWorldModel().getSelf().getPosition();
            Pose2D ballPosition = modeling.getWorldModel().getBall().getPosition();
            double panOrientation = pan + selfPosition.rotation;
            Vector3<double> visionDirection(panOrientation, tilt);
            double ballRadius = 0.04;
            double robotVisionHeight = 0.545;
            Vector3<double> ballDirection(ballPosition.translation.x - selfPosition.translation.x,
                                          ballPosition.translation.y
                                          - selfPosition.translation.y, ballRadius - robotVisionHeight);
            visionDirection = visionDirection.normalize();
            ballDirection = ballDirection.normalize();
            double visionAngle = acos(visionDirection * ballDirection);
            if (visionAngle < 45 * M_PI / 180.0 && visionAngle > -45 * M_PI / 180.0) {
                return exp(-std::abs(visionAngle));
            } else return 0.0;

        }

        double ActiveVision::getCurrentHeadPan() {
            return currentHeadPan;
        }

        double ActiveVision::getCurrentHeadTilt() {
            return currentHeadTilt;
        }

        void ActiveVision::setBallImportance(double alpha) {
            this->alpha = alpha;
        }


    }
}


