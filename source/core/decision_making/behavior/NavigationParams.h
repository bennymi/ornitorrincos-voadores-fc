//
// Created by mmaximo on 24/02/16.
//

#ifndef SOURCE_DECISION_MAKING_BEHAVIOR_NAVIGATIONPARAMS_H_
#define SOURCE_DECISION_MAKING_BEHAVIOR_NAVIGATIONPARAMS_H_

namespace decision_making {
    namespace behavior {

        /**
         * Class that storage the Navigation Parameters
         */
        class NavigationParams {
        public:
            static NavigationParams getDefaultNavigationParams();

            double getMaxVforward();

            double getMaxVbackward();

            double getMaxVy();

            double getMaxVpsi();

            double getCloseDistanceThreshold();

            double getLowVpsiFactor();

            double getLowVforwardFactor();

            double getLowVbackwardFactor();

            double getLowVyFactor();

            double getVelocityGain();

        private:
            double maxVforward;
            double maxVbackward;
            double maxVy;
            double maxVpsi;

            double closeDistanceThreshold;

            double lowVpsiFactor;
            double lowVforwardFactor;
            double lowVbackwardFactor;
            double lowVyFactor;
            double velocityGain;
        };

    } /* namespace behavior */
} /* namespace decision_making */

#endif //SOURCE_DECISION_MAKING_BEHAVIOR_NAVIGATIONPARAMS_H_