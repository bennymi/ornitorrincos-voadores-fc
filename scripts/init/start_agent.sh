#!/usr/bin/env bash

SERVER_IP=$1
SERVER_PORT=$2
TEAM_NAME=$3
AGENT_NUM=$4
cd binaries
./agent --server-ip $SERVER_IP --server-port $SERVER_PORT --team-name $TEAM_NAME --agent-number $AGENT_NUM &>/dev/null &
