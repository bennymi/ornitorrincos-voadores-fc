/*
 * SoccerPlayer.h
 *
 *  Created on: Oct 24, 2015
 *      Author: itandroids
 */

#ifndef SOURCE_AGENT_SOCCERPLAYER_H_
#define SOURCE_AGENT_SOCCERPLAYER_H_

#include "communication/Communication.h"
#include "perception/Perception.h"
#include "modeling/Modeling.h"
#include "decision_making/DecisionMaking.h"
#include "control/Control.h"
#include "action/Action.h"
#include "representations/ITAndroidsConstants.h"
#include "utils/configure_tree/ParameterTreeCreator.h"

namespace agent {

/**
* Class that represents a soccer player.
*/
    class SoccerPlayer {
    public:

        /**
         * Deprecated Constructor
         *
         * @param Server IP
         * @param Server Port
         * @param Team Name
         * @param Agent Number
         * @param Control ID
         */
        SoccerPlayer(int argc, char *argv[]);

        /**
         * Constructor that takes away the responsibility of identifying the Parameters
         */

        SoccerPlayer(std::string serverIP, int serverPort, std::string teamName, int agentNumber);

        /*
         * Destructor
         */
        virtual ~SoccerPlayer();

        /*
         * This method is responsible for the agent loop.
         *
         * First of all, the communication receive a message from the server. Then,
         * perception transform a server message into perceptors. That perceptors
         * are used by modeling to create a world model. According to the world's model,
         * decision making decides what the agent should do. Control is responsible for
         * turn a decision in movement. Then, action creates a message that will be
         * sent to the server by communication.
         */
        void play();

    protected:
        communication::Communication *communication;
        perception::Perception perception;
        modeling::Modeling *modeling;
        decision_making::DecisionMakingImpl decisionMaking;
        control::ControlImpl *control;
        action::ActionImpl action;

    };

} /* namespace agent */

#endif /* SOURCE_AGENT_SOCCERPLAYER_H_ */
