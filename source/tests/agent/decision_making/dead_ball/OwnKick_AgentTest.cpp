//
// Created by dicksiano on 21/06/16.
//

#include "base/BaseAgentTest.h"

class OwnKick_AgentTest : public BaseAgentTest {

public:

    virtual void testLoop(int nSteps) { BaseAgentTest::testLoop(nSteps); }


    virtual void setup() {
        wiz.setPlayMode(representations::RawPlayMode::FREE_KICK_LEFT);
    }

    virtual void runStep() {
        wiz.setPlayMode(representations::RawPlayMode::FREE_KICK_LEFT);

        utils::roboviz::Roboviz roboviz1;
        std::string name1("animated");
        std::string buff1("animated");
        roboviz1.swapBuffers(&buff1);

        Pose2D ballPosition = modeling.getWorldModel().getBall().getPosition();

        roboviz1.drawLine(ballPosition.translation.x, ballPosition.translation.y, 0, ballPosition.translation.x,
                          ballPosition.translation.y, 0, 1, 20, 20, 20, &name1);

        Pose2D selfPosition = modeling.getWorldModel().getSelf().getPosition();
        roboviz1.drawLine(selfPosition.translation.x, selfPosition.translation.y, 0,
                          selfPosition.translation.x, selfPosition.translation.y, 1,
                          1, 20, 20, 20, &name1);

        if (!decisionMaking.decideFall(modeling)) {
            behaviorFactory.getSetPlay().behave(modeling);
            decisionMaking.movementRequest = behaviorFactory.getSetPlay().getMovementRequest();
            decisionMaking.beamRequest = behaviorFactory.getSetPlay().getBeamRequest();

            behaviorFactory.getActiveVision().behave(modeling);
            decisionMaking.lookRequest = behaviorFactory.getActiveVision().getLookRequest();
        }
    }
};

int main() {

    OwnKick_AgentTest agent;
    agent.testLoop(50000);
    return 0;

}