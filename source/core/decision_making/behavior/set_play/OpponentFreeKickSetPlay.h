//
// Created by dicksiano on 8/14/16.
//

#ifndef ITANDROIDS_SOCCER3D_CPP_OPPONENTFREEKICKSETPLAY_H
#define ITANDROIDS_SOCCER3D_CPP_OPPONENTFREEKICKSETPLAY_H

#include "../SetPlay.h"

namespace decision_making {
    namespace behavior {

        class OpponentFreeKickSetPlay : public SetPlay {
        public:


            /**
            * Default constructor.
            */
            OpponentFreeKickSetPlay();

            /**
            * Assignment constructor.
            *
            * @param pointer to BehaviorFactory.
            */
            OpponentFreeKickSetPlay(BehaviorFactory *behaviorFactory);

            /**
            * Destructor.
            */
            virtual ~OpponentFreeKickSetPlay();

            /**
            * This function controls the actions.
            *
            * @param modeling.
            */
            void behave(modeling::Modeling &modeling);

        private:

        };

    } /* namespace behavior */
} /* namespace decision_making */


#endif //ITANDROIDS_SOCCER3D_CPP_OPPONENTFREEKICKSETPLAY_H
