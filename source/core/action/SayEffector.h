/*
 * SayEffector.h
 *
 *  Created on: Aug 8, 2015
 *      Author: mmaximo
 */

#ifndef SOURCE_ACTION_SAYEFFECTOR_H_
#define SOURCE_ACTION_SAYEFFECTOR_H_

#include "action/Effector.h"

namespace action {

/**
 * Class that represents a say effector.
 * SayEffector makes the robot say a message that may be heard by team mates and opponents
 * within a certain range via the HearPerceptor.
 */
    class SayEffector : public Effector {
    public:
        /**
         * Creates a SayEffector.
         *
         * @param message message to say.
         */
        SayEffector(const std::string &message);

        /**
         * Default destructor of SayEffector.
         */

        virtual ~SayEffector();

        /**
         * Gets the say effector as a server message.
         *
         * @return the server message.
         */

        std::string toString();

    private:
        std::string message;
    };

} /* namespace action */

#endif /* SOURCE_ACTION_SAYEFFECTOR_H_ */
