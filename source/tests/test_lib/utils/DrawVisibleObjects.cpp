//
// Created by luckeciano on 01/05/16.
//

#include <core/perception/Perception.h>
#include "DrawVisibleObjects.h"

DrawVisibleObjects::DrawVisibleObjects() {

}

DrawVisibleObjects::~DrawVisibleObjects() {

}

void DrawVisibleObjects::draw(Pose2D robotPose, perception::Perception &perception,
                              modeling::AgentModel &agentModel, utils::roboviz::Roboviz &roboviz) {
    std::string n1("animated.visibleObjects");
    std::vector<perception::VisibleObject *> &visibleObjects = perception.getAgentPerception().getVisibleObjects();
    std::vector<perception::VisibleObject *>::iterator it;
    for (it = visibleObjects.begin(); it != visibleObjects.end(); ++it) {

        if ((*it)->getVisibleObjectType() != perception::VisibleObjectType::VISIBLE_LINE)
            continue;

        double dx = (*it)->getPosition().x * cos(robotPose.rotation) - (*it)->getPosition().y * sin(robotPose.rotation);
        double dy = (*it)->getPosition().x * sin(robotPose.rotation) + (*it)->getPosition().y * cos(robotPose.rotation);
        Vector2<double> globalObjectPosition = robotPose.translation + Vector2<double>(dx, dy);
        if ((*it)->getVisibleObjectType() == perception::VisibleObjectType::VISIBLE_BALL)
            roboviz.drawCircle(globalObjectPosition.x, globalObjectPosition.y, 0.5, 1.0, 1.0, 0.0, 0.0, &n1);
        roboviz.drawLine(robotPose.translation.x, robotPose.translation.y, 0.0, globalObjectPosition.x,
                         globalObjectPosition.y, (*it)->getPosition().z, 1.0, 0.0, 1.0, 0.0, &n1);
        if ((*it)->getVisibleObjectType() == perception::VisibleObjectType::VISIBLE_LINE) {
            perception::VisibleLine *line = dynamic_cast<perception::VisibleLine *>(*it);
            double dx = line->getPosition2().x * cos(robotPose.rotation) -
                        (line)->getPosition2().y * sin(robotPose.rotation);
            double dy = (line)->getPosition2().x * sin(robotPose.rotation) +
                        (line)->getPosition2().y * cos(robotPose.rotation);
            Vector2<double> globalObjectPosition = robotPose.translation + Vector2<double>(dx, dy);
            roboviz.drawLine(robotPose.translation.x, robotPose.translation.y, 0.0, globalObjectPosition.x,
                             globalObjectPosition.y, (*it)->getPosition().z, 1.0, 0.0, 1.0, 0.0, &n1);
        }
//		Pose3D inverse = agentModel.getFromCameraToRootTransform().invert();
//		(*it)->transformPosition(inverse);
//		dx = (*it)->getPosition().x;
//		dy = (*it)->getPosition().y;
//		distance = sqrt(dx * dx + dy * dy);
//		globalObjectPosition = robotPose.translation + Vector2<double>(dx, dy);
//		roboviz.drawLine(robotPose.translation.x, robotPose.translation.y, 0.0, globalObjectPosition.x, globalObjectPosition.y, 0.0, 1.0, 1.0, 0.0, 0.0, &n1);
    }
}

void DrawVisibleObjects::drawLineBallToAgent(modeling::Modeling &modeling, utils::roboviz::Roboviz &roboviz) {
    Pose2D ballPosition = modeling.getWorldModel().getBall().getPosition();
    Pose2D selfPosition = modeling.getWorldModel().getSelf().getPosition();
    std::string n1("animated.visibleObjects");
    roboviz.drawLine(selfPosition.translation.x, selfPosition.translation.y, 0.0, ballPosition.translation.x,
                     ballPosition.translation.y, 0.0, 0.0, 1.0, 0, .0, &n1);
}

void DrawVisibleObjects::drawVisionDirection(modeling::Modeling &modeling, utils::roboviz::Roboviz &roboviz) {
    auto &perception = modeling.getAgentModel().getAgentPerception();
    auto &agentPosition = modeling.getWorldModel().getSelf().getPosition();
    auto joints = perception.getNaoJoints();
    Vector3<double> visionPosition(joints.neckPitch + agentPosition.rotation, joints.neckYaw);
    auto pos = modeling.getAgentModel().getFromCameraToRootTransform() * visionPosition;
    std::string n1("animated.visionLine");
    double headPosition = 1;
    roboviz.drawLine(agentPosition.translation.x, agentPosition.translation.y, headPosition,
                     agentPosition.translation.x + visionPosition.x, agentPosition.translation.y + visionPosition.y,
                     headPosition + visionPosition.z, 1, 100, 150, 30, &n1);
}

void DrawVisibleObjects::drawLineToObstacles(modeling::Modeling &modeling, utils::roboviz::Roboviz &roboviz) {
    auto &agentPosition = modeling.getWorldModel().getSelf().getPosition();

    std::string n1("animated.visionLine");
    auto &visibleObjects = modeling.getAgentModel().getAgentPerception().getVisibleObjects();
    for (auto &k : visibleObjects) {
        if (k->getVisibleObjectType() == perception::VisibleObjectType::VISIBLE_PLAYER) {
            Pose2D obstaclePosition = (*dynamic_cast<perception::VisiblePlayer *>(k)).getPosition();
            roboviz.drawLine(agentPosition.translation.x, agentPosition.translation.y, 0,
                             obstaclePosition.translation.x,
                             obstaclePosition.translation.y, 0, 0.0, 0.0, 0.0, 1.0, &n1);
        }
    }
}

void DrawVisibleObjects::drawCircleToMarking(modeling::Modeling &modeling, utils::roboviz::Roboviz &roboviz,
                                             representations::HearData &hearData) {
    std::vector<int> opponentPosition = modeling.getWorldModel().getSynchronizedRoleAssignmentVector();

    int ctr = 0;
    ctr += 16;

    double opponentX;
    double opponentY;

    //Manually obtains the opponentPosition
    int axInteger = 0;
    int ayInteger = 0;

    for (int i = ctr; i <= ctr + 4; i++) {
        axInteger *= 2;
        axInteger += opponentPosition[i];
    }

    if (opponentPosition[ctr] == 1)
        ctr = -ctr;

    ctr += 5;

    int axDecimal = 0;
    for (int i = ctr; i <= ctr + 3; i++) {
        axDecimal *= 2;
        axDecimal += opponentPosition[i];
    }
    ctr += 4;
    if (axInteger < 0)
        axDecimal = -axDecimal;
    opponentX = axInteger + 0.1 * axDecimal;


    for (int i = ctr; i <= ctr + 4; i++) {
        ayInteger *= 2;
        ayInteger += opponentPosition[i];
    }

    if (opponentPosition[ctr] == 1)
        ctr = -ctr;

    ctr += 5;

    int ayDecimal = 0;
    for (int i = ctr; i <= ctr + 3; i++) {
        ayDecimal *= 2;
        ayDecimal += opponentPosition[i];
        ctr += 4;
        if (ayInteger < 0)
            ayDecimal = -ayDecimal;
        opponentY = ayInteger + 0.1 * ayDecimal;
        std::string n1("animated.circleWhoToMark");
        //Draw a circle around the one to be marked
        roboviz.drawCircle(opponentX, opponentY, 12.00, 1.00, 255, 255, 0, &n1);

    }

}

