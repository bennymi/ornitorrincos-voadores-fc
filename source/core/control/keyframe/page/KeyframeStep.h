/*
 * KeyframeMotionStep.h
 *
 *  Created on: Oct 6, 2015
 *      Author: itandroids
 */

#ifndef SOURCE_CONTROL_KEYFRAME_KEYFRAMEMOTIONSTEP_H_
#define SOURCE_CONTROL_KEYFRAME_KEYFRAMEMOTIONSTEP_H_

#include "core/control/keyframe/KeyframeConstants.h"
#include "representations/NaoJoints.h"


namespace control {
    namespace keyframe {

        class KeyframeStep {
        public:
            KeyframeStep();

            KeyframeStep(std::vector<double> joints, double duration);

            virtual ~KeyframeStep();

            double &getDuration();

            representations::NaoJoints &getJoints();

        private:
            representations::NaoJoints naoJoints;
            double duration;
        };

    } /* namespace keyframe */

} /* namespace control */
#endif /* SOURCE_CONTROL_KEYFRAME_KEYFRAMEMOTIONSTEP_H_ */
