/*
 * VisionPerceptor.h
 *
 *  Created on: Aug 9, 2015
 *      Author: mmaximo
 */

#ifndef SOURCE_PERCEPTION_VISIONPERCEPTOR_H_
#define SOURCE_PERCEPTION_VISIONPERCEPTOR_H_

#include "utils/parser/KeyValuePair.h"
#include "utils/parser/ParserTreeNode.h"
#include "perception/Perceptor.h"
#include "perception/VisibleObject.h"
#include "perception/VisibleFlag.h"
#include "perception/VisibleBall.h"
#include "perception/VisiblePlayer.h"
#include "perception/VisibleLine.h"
#include "perception/VisibleGoalPost.h"
#include <list>

namespace perception {

    class VisionPerceptor : public Perceptor {
    public:
        VisionPerceptor();

        VisionPerceptor(utils::parser::ParserTreeNode &node);

        virtual ~VisionPerceptor();

        std::vector<perception::VisibleObject *> getVisibleObjects();

    private:
        std::vector<perception::VisibleObject *> visibleObjects;
    };

} /* namespace perception */

#endif /* SOURCE_PERCEPTION_VISIONPERCEPTOR_H_ */
