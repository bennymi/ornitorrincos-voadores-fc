//
// Created by dicksiano on 8/14/16.
//

#ifndef ITANDROIDS_SOCCER3D_CPP_OWNGOALSETPLAY_H
#define ITANDROIDS_SOCCER3D_CPP_OWNGOALSETPLAY_H

#include "../SetPlay.h"

namespace decision_making {
    namespace behavior {

        class OwnGoalSetPlay : public SetPlay {
        public:

            /**
            * Default constructor.
            */
            OwnGoalSetPlay();

            /**
            * Assignment constructor.
            *
            * @param pointer to BehaviorFactory.
            */
            OwnGoalSetPlay(BehaviorFactory *behaviorFactory);

            /**
            * Destructor.
            */
            virtual ~OwnGoalSetPlay();

            /**
            * This function controls the actions.
            *
            * @param modeling.
            */
            void behave(modeling::Modeling &modeling);

            std::shared_ptr<itandroids_lib::math::Pose2D> getBeamRequest() { return beamRequest; }

        private:
            /*
             * All derived set plays can modify beamRequest.
             */
            std::shared_ptr<itandroids_lib::math::Pose2D> beamRequest;
        };

    } /* namespace behavior */
} /* namespace decision_making */


#endif //ITANDROIDS_SOCCER3D_CPP_OWNGOALSETPLAY_H
