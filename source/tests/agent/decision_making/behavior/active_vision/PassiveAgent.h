//
// Created by francisco on 5/29/16.
//

#ifndef ITANDROIDS_SOCCER3D_CPP_PASSIVEAGENT_H
#define ITANDROIDS_SOCCER3D_CPP_PASSIVEAGENT_H

#include <iostream>
#include "utils/Logger.h"
#include "utils/LogSystem.h"
#include "decision_making/behavior/ActiveVision.h"
#include "base/BaseAgentTest.h"
#include "utils/DrawVisibleObjects.h"
#include "ActiveVisionAgent.h"

using ::itandroids_lib::utils::LogSystem;
using ::itandroids_lib::utils::Logger;
using ::itandroids_lib::math::Vector3;
using ::itandroids_lib::math::Pose2D;

class PassiveAgent : public BaseAgentTest {
public:
    PassiveAgent(int agentNumber, std::string teamName);

    void runStep() override;

    void setup() override;

private:
    decision_making::behavior::BehaviorFactory factory;

};

PassiveAgent::PassiveAgent(int agentNumber, std::string teamName) : BaseAgentTest("127.0.0.1", 3100, 3200, agentNumber,
                                                                                  0, teamName) {

}

void PassiveAgent::setup() {

}

void PassiveAgent::runStep() {
    //factory.getNavigateToStaticPosition().behave(modeling);
    //decisionMaking.movementRequest = factory.getNavigateToStaticPosition().getMovementRequest();
}


#endif //ITANDROIDS_SOCCER3D_CPP_PASSIVEAGENT_H
