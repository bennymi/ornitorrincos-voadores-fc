/*
 * GlobalTimePerceptor.h
 *
 *  Created on: Aug 30, 2015
 *      Author: itandroids
 */

#ifndef SOURCE_PERCEPTION_GLOBALTIMEPERCEPTOR_H_
#define SOURCE_PERCEPTION_GLOBALTIMEPERCEPTOR_H_

#include "Perceptor.h"
#include "utils/parser/ParserTreeNode.h"
#include "utils/parser/KeyValuePair.h"

namespace perception {

    class GlobalTimePerceptor : public Perceptor {
    public:

        GlobalTimePerceptor();

        /**
         * Constructor of Global Time Perceptor
         * @param node part of the parser tree that represents
         *             an Global Time perceptor.
         */
        GlobalTimePerceptor(utils::parser::ParserTreeNode &node);

        /**
         * Destructor
         */
        virtual ~GlobalTimePerceptor();

        /**
         * Gets measured global time
         * @return global time
         */
        double getGlobalTime();

    private:
        double globalTime;
    };

} /* namespace perception */

#endif /* SOURCE_PERCEPTION_GLOBALTIMEPERCEPTOR_H_ */
