//
// Created by francisco on 5/31/16.
//

#ifndef ITANDROIDS_SOCCER3D_CPP_DELAUNAYPOSITION_AGENTTEST_H
#define ITANDROIDS_SOCCER3D_CPP_DELAUNAYPOSITION_AGENTTEST_H

#include "base/BaseAgentTest.cpp"
#include "decision_making/positioning/DelaunayPositioning.h"

using ::decision_making::positioning::DelaunayPositioning;

class DelaunayPosition_AgentTest : public BaseAgentTest {

public:
    DelaunayPosition_AgentTest(std::string host = "127.0.0.1", int agentPort = 3100, int monitorPort = 3200,
                               int agentNumber = 1);

    void setup();

    void runStep();

private:
    int agentNumber;
    DelaunayPositioning positioning;
    std::string positionName;

};

#endif //ITANDROIDS_SOCCER3D_CPP_DELAUNAYPOSITION_AGENTTEST_H
