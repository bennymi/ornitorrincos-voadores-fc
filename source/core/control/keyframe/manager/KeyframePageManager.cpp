//
// Created by francisco on 18/12/16.
//

#include <core/control/keyframe/page/ComplexKeyframePage.h>
#include "KeyframePageManager.h"
#include "KeyframePageLoader.h"

namespace control{
namespace keyframe {
namespace manager {

template<>
void KeyframePageManager::setParameter<pt::ptree, std::string>(Parameter<pt::ptree, std::string> &parameter){
//    queue for complex pages to be processed
    KeyframePageLoader<pt::ptree> loader;
    std::vector<pt::ptree> complexPageNodes;
    std::vector<std::shared_ptr<ComplexKeyframePage>> complexPages;
    //load simple pages and initialize the complex pages
    for (auto & pageNode : parameter.getChildren()){
        auto & content = pageNode.second->getContent();
        if(isComplexPage(content)){
            complexPageNodes.push_back(content);
            auto complexPage = loader.initializeComplexPage(content);
            complexPages.push_back(complexPage);
            pages[complexPage->getName()] = complexPage;
        } else {
            auto simplePage = loader.initializeAndLoadSimplePage(content);
            pages[simplePage->getName()] = simplePage;
        }
    }
    // add dependencies in the complex pages
    for(int index = 0; index < complexPageNodes.size(); index++){
        loader.loadComplexPage(complexPageNodes[index], complexPages[index], pages);
    }


}

std::shared_ptr<KeyframePage> KeyframePageManager::getPage(std::string pageName) {
    return pages.at(pageName);
};

template<>
bool KeyframePageManager::isComplexPage(pt::ptree & content){
    return content.count("complex") != 0;
}


}
}
}