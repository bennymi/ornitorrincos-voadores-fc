/*
 * RobotPhysicalParameters.h
 *
 *  Created on: Oct 10, 2015
 *      Author: mmaximo
 */

#ifndef SOURCE_CONTROL_WALKING_ROBOTPHYSICALPARAMETERS_H_
#define SOURCE_CONTROL_WALKING_ROBOTPHYSICALPARAMETERS_H_

#include "math/Vector3.h"

namespace control {
    namespace walking {

        using namespace itandroids_lib::math;

        struct RobotPhysicalParameters {
            Vector3<double> torsoToLeftHip;
            Vector3<double> leftAnkleToLeftFoot;
            double upperLegLength;
            double lowerLegLength;
            double hipPitch0;
            double kneePitch0;
            double anklePitch0;

            double armLength;

            RobotPhysicalParameters();

            static RobotPhysicalParameters getNaoPhysicalParameters();
        };
    } /* namespace walking */
} /* namespace control */

#endif /* SOURCE_CONTROL_WALKING_ROBOTPHYSICALPARAMETERS_H_ */
