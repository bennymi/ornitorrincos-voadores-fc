//
// Created by francisco on 6/5/16.
//

#include <core/utils/roboviz/Roboviz.h>
#include "KickBehavior.h"
#include "decision_making/behavior/BehaviorFactory.h"

namespace decision_making {
    namespace behavior {
        const double KickBehavior::BEST_POSITION_DIFF = 0.01;//0.0004;
        const double KickBehavior::CIRCLE_BALL_RATIO = 1.0;
        const double KickBehavior::LONG_DISTANCE_OFFSET = 0.5;

        KickBehavior::KickBehavior() : Behavior(nullptr) {
            state = ROTATING_AROUND_BALL;
        }

        KickBehavior::KickBehavior(BehaviorFactory *factory) : Behavior(factory) {
            state = ROTATING_AROUND_BALL;
        }

        void KickBehavior::behave(modeling::Modeling &modeling) {

            /**
             * Assumes the kick has been setted
             */


            /**
             * Calculates the expected position for the kick
             */
//            if(!hasSettedKick){
            setKick(modeling);
//            } else{

            findState(modeling);
            if (state == ARRIVING_TO_BALL) {

                behaviorFactory->getNavigateToBall().behave(modeling);
                movementRequest = behaviorFactory->getNavigateToBall().getMovementRequest();
            } else if (state == ROTATING_AROUND_BALL) {
                behaviorFactory->getCircleBall().setDribbleDirection(expectedSelfToBallAngle);
                behaviorFactory->getCircleBall().behave(modeling);
                movementRequest = behaviorFactory->getCircleBall().getMovementRequest();

            } else if (state == GETTING_CORRECT_POSITION) {
                // is not in position
                /**
                 *  Rotates itself around the ball to get into position
                 */
                behaviorFactory->getNavigatePosition().setTarget(expectedPosition);
                behaviorFactory->getNavigatePosition().setPreciseMode(true);
                behaviorFactory->getNavigatePosition().behave(modeling);
                movementRequest = behaviorFactory->getNavigatePosition().getMovementRequest();

            } else if (state == KICKING) {
                doKick(modeling);
            }
//            }


        }

        void KickBehavior::setTarget(Vector2<double> target) {
            this->target = target;
        }

        double KickBehavior::getKickCostValue(modeling::Modeling &modeling) {
            return 0;
        }


        void KickBehavior::setKick(modeling::Modeling &modeling) {

            settedKick = kickManager.getKick(
                    (target - modeling.getWorldModel().getBall().getPosition().translation).abs());
            params = settedKick->getKickParameters();
            auto circleBall = decision_making::behavior::CircleBall(behaviorFactory, CIRCLE_BALL_RATIO *
                                                                                     params.getRelativeSelfPosition().abs());
            behaviorFactory->getCircleBall() = circleBall;
        }

        void KickBehavior::doKick(modeling::Modeling &modeling) {
            settedKick->behave(modeling);
            movementRequest = settedKick->getMovementRequest();
        }

        void KickBehavior::findState(modeling::Modeling &modeling) {

            utils::roboviz::Roboviz roboviz;
            std::string name = "animated.line";
            roboviz.drawLine(target.x, target.y, 0, target.x, target.y, 0.5, 1, 200, 200, 100, &name);
            auto ballPosition = modeling.getWorldModel().getBall().getPosition();
            Vector2<double> relativeTarget = target - modeling.getWorldModel().getBall().getPosition().translation;
            expectedSelfToBallAngle = params.getRelativeSelfPosition().angle() - M_PI + relativeTarget.angle();
            roboviz.drawLine(ballPosition.translation.x, ballPosition.translation.y, 0,
                             ballPosition.translation.x + std::cos(expectedSelfToBallAngle + M_PI),
                             ballPosition.translation.y + std::sin(expectedSelfToBallAngle + M_PI), 0, 1, 200, 00, 200,
                             &name);
            roboviz.drawLine(ballPosition.translation.x, ballPosition.translation.y, 0,
                             ballPosition.translation.x + relativeTarget.x,
                             ballPosition.translation.y + relativeTarget.y, 0, 1, 200, 200, 100, &name);
            auto relativeExpectedPosition = params.getRelativeSelfPosition();
            auto auxVector = Vector3<double>(relativeExpectedPosition.x, relativeExpectedPosition.y, 0);
            Vector2<double> expectedPositionVector =
                    relativeExpectedPosition + modeling.getWorldModel().getBall().getPosition().translation;
            roboviz.drawLine(expectedPositionVector.x, expectedPositionVector.y, 0, expectedPositionVector.x,
                             expectedPositionVector.y, 0.5, 1, 200, 100, 100, &name);
            RotationMatrix matrix;
            matrix.rotateZ(relativeTarget.angle());
            auxVector = matrix * auxVector;
            relativeExpectedPosition.x = auxVector.x;
            relativeExpectedPosition.y = auxVector.y;
            expectedPositionVector =
                    relativeExpectedPosition + modeling.getWorldModel().getBall().getPosition().translation;

            roboviz.drawLine(expectedPositionVector.x, expectedPositionVector.y, 0, expectedPositionVector.x,
                             expectedPositionVector.y, 0.5, 1, 200, 0, 100, &name);
            expectedPosition = Pose2D(params.getSelfOrientation() + relativeTarget.angle(), expectedPositionVector);
            relativeSelfToBallPosition = Pose2D(modeling.getWorldModel().getSelf().getPosition().rotation,
                                                modeling.getWorldModel().getSelf().getPosition().translation -
                                                modeling.getWorldModel().getBall().getPosition().translation);
            auto normalizedSelfToBallPosition =
                    relativeSelfToBallPosition.translation / relativeSelfToBallPosition.translation.abs();
            auto normalizedExpectedPosition = relativeExpectedPosition / relativeExpectedPosition.abs();

            if ((expectedPosition - modeling.getWorldModel().getSelf().getPosition()).abs() > LONG_DISTANCE_OFFSET) {
                state = ARRIVING_TO_BALL;
            } else if (state != GETTING_CORRECT_POSITION &&
                       std::acos(normalizedExpectedPosition * normalizedSelfToBallPosition) > 0.05) {
                state = ROTATING_AROUND_BALL;
            } else if (
                    (expectedPosition - modeling.getWorldModel().getSelf().getPosition()).abs() > BEST_POSITION_DIFF &&
                    std::abs(itandroids_lib::math::MathUtils::normalizeAngle(
                            expectedPosition.rotation - modeling.getWorldModel().getSelf().getPosition().rotation)) >
                    0.5 * M_PI / 180.0) {
                state = GETTING_CORRECT_POSITION;
            } else {
                state = KICKING;
            }
        }


    }
}


