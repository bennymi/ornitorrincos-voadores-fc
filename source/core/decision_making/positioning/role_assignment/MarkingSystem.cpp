//
// Created by luckeciano on 8/15/16.
//

#include "MarkingSystem.h"

using modeling::FieldDescription;

namespace decision_making {
    namespace positioning {
        namespace role_assignment {


            MarkingSystem::MarkingSystem() : delaunayPositioning(
                    representations::ITAndroidsConstants::FORMATION_FOLDER) {

            };

            int sortFunc(const void *a, const void *b) {
                if ((*(std::pair<int, Point> *) a).first
                    < (*(std::pair<int, Point> *) b).first)
                    return -1;
                if ((*(std::pair<int, Point> *) a).first
                    == (*(std::pair<int, Point> *) b).first)
                    return 0;
                else
                    return 1;
            }

            std::vector<representations::OtherPlayer *> MarkingSystem::decideWhoMarks(modeling::Modeling &modeling) {

                opponentsToMark.clear();
                /*
                 * Who to mark:
                 * 1 - Opponent is close enough to take a shot on goal
                 * 2 - Opponent is not the closest opponent to the ball
                 * 3 - Opponent is not too close to the ball
                 * 4 - Opponent is not too far behind the ball
                 */

                std::vector<representations::OtherPlayer *> opponents = modeling.getWorldModel().getOpponents();
                itandroids_lib::math::Pose2D ballPos = modeling.getWorldModel().getBall().getPosition();
                itandroids_lib::math::Pose2D goalPos(modeling.getWorldModel().getOwnGoalPosition());
                for (auto opponent: opponents) {
                    if (fabs(opponent->getPosition().translation.x) < modeling::FieldDescription::FIELD_LENGTH / 2.0 &&
                        fabs(opponent->getPosition().translation.y) < modeling::FieldDescription::FIELD_WIDTH / 2.0) {
                        if (opponent->getPosition().translation.x - ballPos.translation.x >
                            FieldDescription::FIELD_LENGTH / 6) {
                            //no mark, it's too far behind the ball
                        } else if ((opponent->getPosition() - ballPos.translation).abs() <
                                   FieldDescription::FIELD_WIDTH / 15) {
                            //no mark, it's too close to the ball
                        } else if (opponent == &modeling.getWorldModel().getOpponentClosestFromBall()) {
                            //no mark, it's the closest opponent to the ball
                        } else if ((opponent->getPosition().translation - goalPos.translation).abs() <
                                   7 * FieldDescription::FIELD_LENGTH / 15) {
                            //mark, is close enough to take a shot on goal
                            opponentsToMark.push_back(opponent);
                        }
                    }

                }

                return opponentsToMark;
            }

            void MarkingSystem::selectRolesAndAssign(modeling::Modeling &modeling) {
                std::vector<representations::OtherPlayer *> opponentsToMark = decideWhoMarks(modeling);
                std::vector<int> whoMarksUniformNumber;
                std::vector<representations::OtherPlayer *> teammates = modeling.getWorldModel().getTeammates();
                std::vector<std::pair<int, Point> > teamPositionsPairs; //player from our team and its position.

                for (int i = 0; i < teammates.size(); i++) {
                    teamPositionsPairs.push_back(
                            std::make_pair(teammates[i]->getId(), Point(teammates[i]->getPosition().translation.x,
                                                                        teammates[i]->getPosition().translation.y)));
                }

                teamPositionsPairs.push_back(std::make_pair(modeling.getAgentModel().getAgentNumber(),
                                                            Point(modeling.getWorldModel().getSelf().getPosition().translation.x,
                                                                  modeling.getWorldModel().getSelf().getPosition().translation.y)));

                qsort(&teamPositionsPairs[0], teamPositionsPairs.size(), sizeof(std::pair<int, Point>), sortFunc);


                std::vector<Point> teamPositions;
                for (int i = 0; i < teamPositionsPairs.size(); i++) {
                    teamPositions.push_back(teamPositionsPairs[i].second);
                }


                SCRAMByMMDR scramByMMDR;
                for (int i = 0; i < teamPositions.size(); i++) {
                    scramByMMDR.test.addStart(teamPositions[i]);
                }

                /*
                //Get Delaunay Roles
                std::vector<itandroids_lib::math::Vector2<double> > delaunayPos = delaunayPositioning.getTeamPosition(
                        modeling);
                for (auto delaunayPositions: delaunayPos) {
                    Point delPos = std::make_pair(delaunayPositions.x, delaunayPositions.y);
                    scramByMMDR.test.addTarget(delPos);
                }*/

                //Replace delaunay for marking positions;

                std::vector<itandroids_lib::math::Vector2<double> > delaunayPos = delaunayPositioning.getTeamPosition(
                        modeling);

                std::vector<itandroids_lib::math::Vector2<double> > finalTargets = delaunayPos;
                std::vector<int> roleAssignmentTargets({0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10});
                for (int opponent = opponentsToMark.size() - 1; opponent >= 0; opponent--) {
                    itandroids_lib::math::Vector2<double> lineFromOppToGoal = itandroids_lib::math::Vector2<double>
                            (modeling.getWorldModel().getOwnGoalPosition().x -
                             opponentsToMark[opponent]->getPosition().translation.x,
                             modeling.getWorldModel().getOwnGoalPosition().y -
                             opponentsToMark[opponent]->getPosition().translation.y);
                    lineFromOppToGoal = lineFromOppToGoal.normalize();
                    itandroids_lib::math::Vector2<double> markPosition(
                            opponentsToMark[opponent]->getPosition().translation.x + 1.5 * lineFromOppToGoal.x,
                            opponentsToMark[opponent]->getPosition().translation.y + 1.5 * lineFromOppToGoal.y);

                    double minimumDistance = (delaunayPos[1] - markPosition).abs();
                    int delaunayMinDist = 1;
                    for (int i = 1; i < delaunayPos.size(); i++) {
                        //take the delaunay pos of minimum distance from mark position
                        if ((delaunayPos[i] - markPosition).abs() < minimumDistance) {
                            //check if the position has already been replaced by a mark position
                            //and if the position is onBall or suppoter (major priority)
                            if (roleAssignmentTargets[i] < modeling::WorldModel::NUM_PLAYERS_PER_TEAM &&
                                roleAssignmentTargets[i] != 9 && roleAssignmentTargets[i] != 6) {
                                minimumDistance = (delaunayPos[i] - markPosition).abs();
                                delaunayMinDist = i;
                            }
                        }
                    }
                    //delaunayMinDist must be replaced by markPosition.
                    finalTargets[delaunayMinDist] = markPosition;
                    roleAssignmentTargets[delaunayMinDist] =
                            modeling::WorldModel::NUM_PLAYERS_PER_TEAM + opponentsToMark[opponent]->getId() - 1;
                }

                for (int i = 0; i < finalTargets.size(); i++) {
                    scramByMMDR.test.addTarget(Point(finalTargets[i].x, finalTargets[i].y));
                }

                scramByMMDR.solvePriority(roleAssignmentTargets);

                //Get opponentsToMark
                /* for (auto opponentToMark: opponentsToMark) {
                     whoMarksUniformNumber.push_back(opponentToMark->getId());
                     itandroids_lib::math::Vector2<double> lineFromOppToGoal = itandroids_lib::math::Vector2<double>
                             (modeling.getWorldModel().getOwnGoalPosition().x - opponentToMark->getPosition().translation.x,
                              modeling.getWorldModel().getOwnGoalPosition().y - opponentToMark->getPosition().translation.y);
                     lineFromOppToGoal = lineFromOppToGoal.normalize();
                     itandroids_lib::math::Pose2D markPosition(
                             opponentToMark->getPosition().translation.x + 1.5 * lineFromOppToGoal.x,
                             opponentToMark->getPosition().translation.y + 1.5 * lineFromOppToGoal.y);
                     Point oppToMarkPos = std::make_pair(markPosition.translation.x, markPosition.translation.y);
                     scramByMMDR.test.addTarget(oppToMarkPos);
                 }*/


                std::vector<Edge> roleAssignment = scramByMMDR.mmdrSolve();

                //Edge = pair<dist, pair <node start, node target>
                /*
                 * Order of nodes:
                 * Start: Uniform number. [0] = 1, [1] = 2, ... [10] = 11
                 * Target: Delaunay Roles. [0] = goalie, [1] = backRight, ... [10] = fowardCenter
                 *         Marking Opponents: [11] =  decideWhoMarks[0], [12] = decideWhoMarks[1]...
                 */

                int roleAssignIntegersVector[modeling::WorldModel::NUM_PLAYERS_PER_TEAM];

                /*
                 * This vector returns, ordered, the role of all players on team.
                 * As a concrete example one could have the following roles each represented by 5 bit integers
                 * assuming that you have no more than 32 roles (in our case, we have 22 roles):
                 * goalie = 0, backRight = 1, ... fowardCenter = 10, markOpp1 = 11, markOpp2 = 12, ...
                 * markOpp11 = 21.
                 * Then role assignment vector 0, 1, 10, 11, 12, 20, 21 ... (00000, 00001, 01010, 01011, 01100 ...) says
                 * that our player 1 goes to goalie's position, player 2 goes to backRight's position, player 3 goes to
                 * fowardCenter's position, player 4 marks opponent 1, player 5 marks opponent 2, player 6 marks opponent 10
                 * player 7 marks opponent 11...
                 */
                for (auto edge: roleAssignment) {
                    roleAssignIntegersVector[edge.second.first] = roleAssignmentTargets[edge.second.second];
                }

                for (int i = 0; i < modeling::WorldModel::NUM_PLAYERS_PER_TEAM; i++) {
                    if (roleAssignIntegersVector[i] == 0) {
                        roleAssignIntegersVector[i] = roleAssignIntegersVector[0];
                        roleAssignIntegersVector[0] = 0;


                    }
                }

                std::vector<int> finalRoleAssignVector(std::begin(roleAssignIntegersVector),
                                                       std::end(roleAssignIntegersVector));


                modeling.getWorldModel().updateMyRoleAssignmentVector(finalRoleAssignVector);

            }

            std::vector<representations::OtherPlayer *> MarkingSystem::getOpponentToMark() {
                return opponentsToMark;
            }


            /*priority vectors:
            * 0 - goalie;
            * 1 - backRight, 2 - backLeft, 3 - mid
            * 4 - wingLeft, 5 - wingRight, 6 - supporter
            * 7 - fowardLeft, 8 - fowardRight, 9 - onBall, 10 - fowardCenter
            */
        }
    }
}