//
// Created by dicksiano on 8/14/16.
//

#ifndef ITANDROIDS_SOCCER3D_CPP_GAMEOVERSETPLAY_H
#define ITANDROIDS_SOCCER3D_CPP_GAMEOVERSETPLAY_H

#include "../SetPlay.h"

namespace decision_making {
    namespace behavior {

        class GameOverSetPlay : public SetPlay {
        public:

            /**
            * Default constructor.
            */
            GameOverSetPlay();

            /**
            * Assignment constructor.
            *
            * @param pointer to BehaviorFactory.
            */
            GameOverSetPlay(BehaviorFactory *behaviorFactory);

            /**
            * Destructor.
            */
            virtual ~GameOverSetPlay();

            /**
            * This function controls the actions.
            *
            * @param modeling.
            */
            void behave(modeling::Modeling &modeling);

        private:

        };

    } /* namespace behavior */
} /* namespace decision_making */



#endif //ITANDROIDS_SOCCER3D_CPP_GAMEOVERSETPLAY_H
