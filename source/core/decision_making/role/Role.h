//
// Created by dicksiano on 27/05/16.
//

#ifndef ITANDROIDS_SOCCER3D_CPP_ROLE_H
#define ITANDROIDS_SOCCER3D_CPP_ROLE_H

#include "core/decision_making/behavior/BehaviorFactory.h"
#include "modeling/Modeling.h"
#include "control/LookRequest.h"
#include "control/MovementRequest.h"


namespace decision_making {
    namespace role {
/**
 * This class represents an abstract role.
 */
        class Role {
        public:
            /**
             * Default constructor for the Role
             */
            Role();

            /**
             * Default destructor for the Role
             */
            virtual ~Role();

            /**
             * Decides what actions a soccer player should execute
             */
            virtual void execute(modeling::Modeling &modeling);

            virtual void reset();

            /**
             * Function to get the message to send to the other agents
             */
            virtual std::shared_ptr<std::string> getSayMessage() { return sayMessage; }

            /**
             * method to get the movements for the head and neck joints
             */
            virtual std::shared_ptr<control::LookRequest> getLookRequest() { return lookRequest; }

            /**
             * method for getting the robots body joint positions
             */
            virtual std::shared_ptr<control::MovementRequest> getMovementRequest() { return movementRequest; }

            /**
             * method for returning
             */
            virtual std::shared_ptr<itandroids_lib::math::Pose2D> getBeamRequest() { return beamRequest; };


        protected:

            virtual void behaveMovement(modeling::Modeling &modeling) = 0;

            virtual void behaveSee(modeling::Modeling &modeling);

            virtual void behaveSay(modeling::Modeling &modeling);

            std::shared_ptr<std::string> sayMessage;
            std::shared_ptr<control::LookRequest> lookRequest;
            std::shared_ptr<control::MovementRequest> movementRequest;
            std::shared_ptr<itandroids_lib::math::Pose2D> beamRequest;
            decision_making::behavior::BehaviorFactory behaviorFactory;
        };

    } /* namespace role */
} /* namespace decision_making */


#endif //ITANDROIDS_SOCCER3D_CPP_ROLE_H
