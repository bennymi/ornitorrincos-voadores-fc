//
// Created by francisco on 6/16/16.
//

#include "KeyframeKick.h"

namespace decision_making {
    namespace behavior {
        namespace kick_behavior {
            KeyframeKick::KeyframeKick(std::string keyframeName) : keyframeName(keyframeName) {

            }

            KeyframeKick::~KeyframeKick() {

            }

            void KeyframeKick::behave(modeling::Modeling &modeling) {
                movementRequest = std::make_shared<control::KeyframeRequest>(keyframeName);
            }


        }
    }
}