/*
 * NaoJoints.h
 *
 *  Created on: Aug 7, 2015
 *      Author: mmaximo
 */

#ifndef SOURCE_REPRESENTATIONS_NAOJOINTS_H_
#define SOURCE_REPRESENTATIONS_NAOJOINTS_H_

#include <list>
#include <map>

#include "perception/Perceptor.h"
#include <boost/lexical_cast.hpp>
#include "perception/HingeJointPerceptor.h"

namespace representations {

    struct NaoJoints {
        enum HINGE_JOINTS {
            NECK_YAW,
            NECK_PITCH,
            LEFT_SHOULDER_PITCH,
            LEFT_SHOULDER_YAW,
            LEFT_ARM_ROLL,
            LEFT_ARM_YAW,
            LEFT_HIP_YAWPITCH,
            LEFT_HIP_ROLL,
            LEFT_HIP_PITCH,
            LEFT_KNEE_PITCH,
            LEFT_FOOT_PITCH,
            LEFT_FOOT_ROLL,
            RIGHT_SHOULDER_PITCH,
            RIGHT_SHOULDER_YAW,
            RIGHT_ARM_ROLL,
            RIGHT_ARM_YAW,
            RIGHT_HIP_YAWPITCH,
            RIGHT_HIP_ROLL,
            RIGHT_HIP_PITCH,
            RIGHT_KNEE_PITCH,
            RIGHT_FOOT_PITCH,
            RIGHT_FOOT_ROLL,
            NUM_JOINTS,
            INVALID,
        };

        double neckPitch;
        double neckYaw;

        double leftShoulderPitch;
        double leftShoulderYaw;
        double leftArmRoll;
        double leftArmYaw;

        double rightShoulderPitch;
        double rightShoulderYaw;
        double rightArmRoll;
        double rightArmYaw;

        double leftHipYawPitch;
        double leftHipRoll;
        double leftHipPitch;
        double leftKneePitch;
        double leftFootPitch;
        double leftFootRoll;

        double rightHipYawPitch;
        double rightHipRoll;
        double rightHipPitch;
        double rightKneePitch;
        double rightFootPitch;
        double rightFootRoll;

        NaoJoints();

        /**
         * receives a vector of hinge joints and set's them
         * @param list of HingeJointPerceptors
         */
        NaoJoints(std::vector<perception::HingeJointPerceptor>);

        /**
         * receives a vector of joins and set's them
         * @param joints
         */
        NaoJoints(std::vector<double> joints);

        ~NaoJoints();

        void clear();

        void assign(std::vector<perception::HingeJointPerceptor> &perceptors);

        NaoJoints operator+(const NaoJoints &other);

        NaoJoints operator-(const NaoJoints &other);

        NaoJoints operator*(const NaoJoints &other);

        NaoJoints operator*(const double scalar);

        NaoJoints &operator*=(const NaoJoints &other);

        void scale(double scalar);

        double lInfiniteNorm();

        double getValue(HINGE_JOINTS joint);

        void setValue(HINGE_JOINTS joint, double value);

        std::map<HINGE_JOINTS, double> getAsMap();

        std::string toString();

        std::string toSimpleString();

        static NaoJoints getNaoDirectionsFixing();

        /**
         * A method that returns the simetric joints positions with directions fixing
         * @param joints
         * @return the simetric joints with directions fixing
         */
        static NaoJoints getSimetricPositions(NaoJoints joints);
    };

} /* namespace representations */

#endif /* SOURCE_REPRESENTATIONS_NAOJOINTS_H_ */
