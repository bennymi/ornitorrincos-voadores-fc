//
// Created by dicksiano on 8/15/16.
//

#ifndef ITANDROIDS_SOCCER3D_CPP_GOALIE_AGENTTEST_H
#define ITANDROIDS_SOCCER3D_CPP_GOALIE_AGENTTEST_H

#include "decision_making/behavior/GoalieBehavior.h"
#include "base/BaseAgentTest.h"

class Goalie_AgentTest : public BaseAgentTest {

public:
    Goalie_AgentTest(std::string host = "127.0.0.1", int agentPort = 3100, int monitorPort = 3200, int agentNumber = 1);

    virtual void testLoop(int nSteps) { BaseAgentTest::testLoop(nSteps); }


    virtual void setup() {
    }

    virtual void runStep() {
        utils::roboviz::Roboviz roboviz1;
        std::string name1("animated");
        std::string buff1("animated");
        roboviz1.swapBuffers(&buff1);


        Pose2D selfPosition = modeling.getWorldModel().getSelf().getPosition();
        roboviz1.drawLine(selfPosition.translation.x, selfPosition.translation.y, 0,
                          selfPosition.translation.x, selfPosition.translation.y, 1,
                          1, 20, 20, 20, &name1);
        roboviz1.drawLine(selfPosition.translation.x, selfPosition.translation.y, 0,
                          selfPosition.translation.x, selfPosition.translation.y, 1,
                          1, 20, 200, 70, &name1);

        if (!decisionMaking.decideFall(modeling)) {
            behaviorFactory.getGoalieBehavior().behave(modeling);
            decisionMaking.movementRequest = behaviorFactory.getGoalieBehavior().getMovementRequest();

            behaviorFactory.getActiveVision().behave(modeling);
            decisionMaking.lookRequest = behaviorFactory.getActiveVision().getLookRequest();
        }
    }
};

#endif //ITANDROIDS_SOCCER3D_CPP_GOALIE_AGENTTEST_H
