/*
 * NavigateToPosition.cpp
 *
 *  Created on: Oct 31, 2015
 *      Author: itandroids
 */

#include "NavigateToPosition.h"
#include "BehaviorFactory.h"
#include "decision_making/behavior/NavigationParams.h"

namespace decision_making {
    namespace behavior {

        NavigateToPosition::NavigateToPosition() :
                insideCloseDistance(false), CLOSE_DISTANCE_THRESHOLD(1.0),
                LOW_VPSI_FACTOR(0.5), LOW_VFORWARD_FACTOR(0.5),
                LOW_VBACKWARD_FACTOR(0.5), LOW_VY_FACTOR(0.5),
                VELOCITY_GAIN(1.0), preciseMode(false) {
            configureUsingDefaultParams();
            configurePreciseModeParams();
        }

        NavigateToPosition::NavigateToPosition(BehaviorFactory *behaviorFactory) :
                insideCloseDistance(false),
                Behavior(behaviorFactory) {
            configureUsingDefaultParams();
            configurePreciseModeParams();
        }

        NavigateToPosition::~NavigateToPosition() {
        }

        void NavigateToPosition::configureUsingDefaultParams() {
            decision_making::behavior::NavigationParams navigationParams = decision_making::behavior::NavigationParams::getDefaultNavigationParams();
            maxVforward = navigationParams.getMaxVforward();
            maxVbackward = navigationParams.getMaxVbackward();
            maxVy = navigationParams.getMaxVy();
            maxVpsi = navigationParams.getMaxVpsi();

            CLOSE_DISTANCE_THRESHOLD = navigationParams.getCloseDistanceThreshold();

            LOW_VPSI_FACTOR = navigationParams.getLowVpsiFactor();
            LOW_VFORWARD_FACTOR = navigationParams.getLowVforwardFactor();
            LOW_VBACKWARD_FACTOR = navigationParams.getLowVbackwardFactor();
            LOW_VY_FACTOR = navigationParams.getLowVyFactor();

            VELOCITY_GAIN = navigationParams.getVelocityGain();
        }

        void NavigateToPosition::configurePreciseModeParams() {
            preciseWalkSaturation = 0.2;
        }

        void NavigateToPosition::setTarget(itandroids_lib::math::Pose2D target) {
            this->target.translation.x = target.translation.x;
            this->target.translation.y = target.translation.y;
            this->target.rotation = target.rotation;
        }

        void NavigateToPosition::behave(modeling::Modeling &modeling) {
            Pose2D walkVelocity;

            Pose2D player = modeling.getWorldModel().getSelf().getPosition();

            Vector2<double> otherGoalPosition(modeling.getWorldModel().getTheirGoalPosition().x,
                                              modeling.getWorldModel().getTheirGoalPosition().y);
            Vector2<double> agentToTarget = target.translation - player.translation;
            Vector2<double> targetToGoal = otherGoalPosition - target.translation;

            double distance = agentToTarget.abs();

            Pose2D targetRelativePlayer = target.globalToRelative(player);

            if (distance < CLOSE_DISTANCE_THRESHOLD
                || (insideCloseDistance && distance < 1.5 * CLOSE_DISTANCE_THRESHOLD)) {
                insideCloseDistance = true;

                walkVelocity.translation.x = targetRelativePlayer.translation.x;
                walkVelocity.translation.y = targetRelativePlayer.translation.y;
                walkVelocity.rotation = targetRelativePlayer.rotation;

                walkVelocity = walkVelocity * VELOCITY_GAIN;

//            walkVelocity = saturateWalkVelocity(walkVelocity);

                walkVelocity = navigationHelper.saturateWalkVelocity(walkVelocity);

//            Vector3<double> outputVelocity;
//            outputVelocity.x = walkVelocity.translation.x;
//            outputVelocity.y = walkVelocity.translation.y;
//            outputVelocity.z = walkVelocity.rotation;

                if (preciseMode) {
                    if (walkVelocity.translation.abs() > preciseWalkSaturation) {
                        walkVelocity.translation.normalize();
                        walkVelocity.translation = walkVelocity.translation * preciseWalkSaturation;
                    }
                    movementRequest = std::make_shared<control::WalkRequest>(walkVelocity, control::WalkMode::PRECISE);
                } else {
                    movementRequest = std::make_shared<control::WalkRequest>(walkVelocity);
                }
            } else {
                insideCloseDistance = false;

//                double faceAngle = atan2(targetRelativePlayer.translation.y,
//                                         targetRelativePlayer.translation.x);
//                walkVelocity.translation.x = targetRelativePlayer.translation.x;
//                walkVelocity.translation.y = targetRelativePlayer.translation.y;
//                walkVelocity.rotation = faceAngle;

                behaviorFactory->getPotentialFieldNavigationBehavior().setTarget(target.translation);
                behaviorFactory->getPotentialFieldNavigationBehavior().behave(modeling);

                movementRequest = behaviorFactory->getPotentialFieldNavigationBehavior().getMovementRequest();
            }

            // if preciseMode has been activated, deactivate it for the next iteration
            preciseMode = false;
        }


        Pose2D NavigateToPosition::saturateWalkVelocity(Pose2D walkVelocity) {
            Pose2D saturatedWalkVelocity;
            double factor = 10.0;
            if (fabs(walkVelocity.rotation) > maxVpsi * LOW_VPSI_FACTOR) {
                saturatedWalkVelocity.translation.x = MathUtils::saturate(walkVelocity.translation.x,
                                                                          -maxVbackward / factor, maxVforward / factor);
                saturatedWalkVelocity.translation.y = MathUtils::saturate(walkVelocity.translation.y, -maxVy / factor,
                                                                          maxVy / factor);
                saturatedWalkVelocity.rotation = MathUtils::saturate(walkVelocity.rotation, -maxVpsi, maxVpsi);
            } else if (walkVelocity.translation.x < -maxVbackward * LOW_VBACKWARD_FACTOR) {
                saturatedWalkVelocity.translation.x = MathUtils::saturate(walkVelocity.translation.x, -maxVbackward,
                                                                          maxVforward);
                saturatedWalkVelocity.translation.y = MathUtils::saturate(walkVelocity.translation.y, -maxVy / factor,
                                                                          maxVy / factor);
                saturatedWalkVelocity.rotation = MathUtils::saturate(walkVelocity.rotation, -maxVpsi / factor,
                                                                     maxVpsi / factor);
            } else if (walkVelocity.translation.x > maxVforward * LOW_VFORWARD_FACTOR) {
                saturatedWalkVelocity.translation.x = MathUtils::saturate(walkVelocity.translation.x, -maxVbackward,
                                                                          maxVforward);
                saturatedWalkVelocity.translation.y = MathUtils::saturate(walkVelocity.translation.y, -maxVy / factor,
                                                                          maxVy / factor);
                saturatedWalkVelocity.rotation = MathUtils::saturate(walkVelocity.rotation, -maxVpsi / (3.0 * factor),
                                                                     maxVpsi / (3.0 * factor));
            } else if (fabs(walkVelocity.translation.y) > maxVy * LOW_VY_FACTOR) {
                saturatedWalkVelocity.translation.x = MathUtils::saturate(walkVelocity.translation.x,
                                                                          -maxVbackward / factor, maxVforward / factor);
                saturatedWalkVelocity.translation.y = MathUtils::saturate(walkVelocity.translation.y, -maxVy, maxVy);
                saturatedWalkVelocity.rotation = MathUtils::saturate(walkVelocity.rotation, -maxVpsi / factor,
                                                                     maxVpsi / factor);
            } else {
                saturatedWalkVelocity.translation.x = walkVelocity.translation.x;
                saturatedWalkVelocity.translation.y = walkVelocity.translation.y;
                saturatedWalkVelocity.rotation = walkVelocity.rotation;
            }
            return saturatedWalkVelocity;
        }

        void NavigateToPosition::setPreciseMode(bool preciseMode) {
            this->preciseMode = true;
        }

    } /* namespace behavior */
} /* namespace decision_making */
