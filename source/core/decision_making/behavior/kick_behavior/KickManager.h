//
// Created by francisco on 6/16/16.
//

#ifndef ITANDROIDS_SOCCER3D_CPP_KICKMANAGER_H
#define ITANDROIDS_SOCCER3D_CPP_KICKMANAGER_H

#include "LongDistanceKick.h"
#include "ShortDistanceKick.h"
#include "ZMPKick.h"
#include "Kick.h"

namespace decision_making {
    namespace behavior {
        namespace kick_behavior {
            class KickManager {
            public:
                KickManager();

                std::shared_ptr<Kick> getKick(double distance, double direction = 0);

            protected:
                virtual std::shared_ptr<Kick> selectKick(double distance, double direction);

                double distance, direction;
                std::shared_ptr<Kick> selectedKick;
                std::vector<std::shared_ptr<Kick>> kicks;
            };
        }
    }
}


#endif //ITANDROIDS_SOCCER3D_CPP_KICKMANAGER_H
