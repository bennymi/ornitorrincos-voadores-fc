/*
 * HearData.h
 *
 *  Created on: Aug 8, 2015
 *      Author: mmaximo
 */

#ifndef SOURCE_REPRESENTATIONS_HEARDATA_H_
#define SOURCE_REPRESENTATIONS_HEARDATA_H_

#include <iostream>
#include <math/Vector2.h>
#include "OtherPlayer.h"

namespace representations {

    class HearData {

    public:
        /**
         * Default Constructor
         *
         */
        HearData();

        /**
         * Constructor HearData.
         *
         * @param team represents the team which belongs the message
         *
         * @param time represents the simulation time at which the given message was heard
         * 				in seconds
         *
         * @param fromSelf is true if the player is hearing their own words. If the message is
         * 				from another player fromSelf is false.
         *
         * @param direction represents relative horizontal direction in radian indicating
         * 				where the sound originated
         *
         * @param message represents a message shouted by other players or by himself
         *
         */
        HearData(std::string team, double time, bool fromSelf, double direction, std::string message);

        /**
         * Decodes the message and save fields heared (ball position, agent position, role assignment vector...)
         */
        void decode(std::string ourTeamName);

        /**
         * Destructor.
         */
        virtual ~HearData();

        /**
         * Gets the time heard from the server
         */

        double getHeardServerTime();

        /**
         * Gets the team.
         *
         * @return the team.
         */
        std::string getTeam();

        /**
         * Gets the time.
         *
         * @return the time.
         */
        double getTime();

        /**
         * Gets the state of the message: if player is listening his own words
         * or if the message is from another player.
         *
         * @return the boolean fromSelf.
         */
        bool isFromSelf();

        /**
         * Gets the message's direction.
         *
         * @return the direction.
         */
        double getDirection();

        /**
         * Gets the message.
         *
         * @return the message.
         */
        std::string getMessage();

        /**
         * Gets the role assigment vector
         *
         * @return the role assigment vector
         */
        std::vector<int> getRoleAssigment();

        /*
         * Gets uniform number from the agent who talks
         *
         * @return the uNum
         */
        int getUniformNumber();

        /**
         * Gets ball position heared from message
         */
        double getHeardBallXPosition();

        double getHeardBallYPosition();

        /*
         * Check if the agent is fallen
         */
        bool isFallen();

        /**
         * Gets agent position heared from message
         */
        double getHeardAgentXPosition();

        double getHeardAgentYPosition();

        /**
         * Gets the cycle number;
         */
        double getCycle();

        /**
         * Check if the game has started 
         */
        bool hasStarted();

        std::vector<OtherPlayer> getOpponentPositions();


    protected:

    private:
        std::string team;
        int cycles;
        int uNum;
        double time = 0;
        double heardServerTime;
        bool fromSelf;
        bool fFallen;
        double ballLastSeenServerTime;
        double ballX;
        double ballY;
        double agentX;
        double agentY;
        double direction;
        std::string message = "";
        std::vector<int> roleAssignment;
        std::vector<int> bits;
        std::vector<OtherPlayer> opponentsPos;

        void stringToBits();

        void bitsToDataGoalie();

        void bitsToData();

        int bitsToInt(std::vector<int> &bits, const int &start, const int &end);

        int signedBitsToInt(std::vector<int> &bits, const int &start, const int &end);
    };

} /* namespace representations */

#endif /* SOURCE_REPRESENTATIONS_HEARDATA_H_ */
