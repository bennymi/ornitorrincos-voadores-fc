//
// Created by mmaximo on 05/06/16.
//

#include "decision_making/behavior/NavigationHelper.h"

#include "math/MathUtils.h"

namespace decision_making {
    namespace behavior {

        using itandroids_lib::control::AngleController;

        NavigationHelperParams NavigationHelperParams::getDefaultParams() {
            using itandroids_lib::math::MathUtils;

            NavigationHelperParams params;
            params.maxForwardSpeed = 0.9;
            params.maxBackwardSpeed = 0.9;
            params.maxSideSpeed = 0.4;
            params.maxAngularSpeed = MathUtils::degreesToRadians(300.0);
            params.angleBandwith = 2.0 * M_PI * 0.5;
            return params;
        }

        NavigationHelper::NavigationHelper() : params(NavigationHelperParams::getDefaultParams()),
                                               angleController(
                                                       AngleController(params.angleBandwith, params.maxAngularSpeed)) {
        }

        NavigationHelper::NavigationHelper(NavigationHelperParams params) : params(params),
                                                                            angleController(AngleController(
                                                                                    params.angleBandwith,
                                                                                    params.maxAngularSpeed)) {
        }

        double NavigationHelper::controlAngle(double reference, double actual) {
            return angleController.control(reference, actual);
        }

        Pose2D NavigationHelper::saturateWalkVelocity(Pose2D &velocity) {
            using itandroids_lib::math::Vector2;
            using itandroids_lib::math::MathUtils;

            Pose2D saturatedWalkVelocity;

            double angularFactor = 1.0 - std::min(fabs(velocity.rotation) / params.maxAngularSpeed, 1.0);

            Vector2<double> linearDirection = velocity.translation;
            linearDirection.normalize();

            double normSaturation = 0.0;
            if (linearDirection.x >= 0.0)
                normSaturation += linearDirection.x * params.maxForwardSpeed;
            else
                normSaturation += (-linearDirection.x) * params.maxBackwardSpeed;
            normSaturation += fabs(linearDirection.y) * params.maxSideSpeed;
            normSaturation *= angularFactor;

            double scale = std::min(normSaturation, velocity.abs());

            saturatedWalkVelocity.translation = linearDirection * scale;
            saturatedWalkVelocity.rotation = MathUtils::saturate(velocity.rotation,
                                                                 -params.maxAngularSpeed, params.maxAngularSpeed);
            return saturatedWalkVelocity;
        }

    } /* namespace behavior */
} /* namespace decision_making */