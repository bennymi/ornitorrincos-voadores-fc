/*
 * AgentPerception.h
 *
 *  Created on: Aug 19, 2015
 *      Author: itandroids
 */

#ifndef SOURCE_PERCEPTION_AGENTPERCEPTION_H_
#define SOURCE_PERCEPTION_AGENTPERCEPTION_H_

#include "utils/parser/ParserTreeNode.h"
#include "perception/PerceptorFactory.h"
#include "representations/NaoJoints.h"
#include "representations/HearData.h"
#include "representations/PlayMode.h"
#include "representations/RawPlayMode.h"
#include "representations/PlaySide.h"
#include "perception/VisibleObject.h"
#include "representations/GameState.h"
#include "representations/ForceResistanceData.h"
#include "perception/GameStatePerceptor.h"
#include "perception/GlobalTimePerceptor.h"

#include "math/MathUtils.h"
#include "math/Vector3.h"
#include <list>

namespace perception {
    using namespace itandroids_lib::math;

    class AgentPerception {
    public:
        /**
         * Default Constructor
         */
        AgentPerception();

        virtual ~AgentPerception();

        /**
         * Get's the received game state
         * @return the GameState of the game
         */
        representations::GameState getGameState();

        int getUniformNumber();

        double getGlobalTime();

        representations::PlaySide::PLAY_SIDE getPlaySide();

        representations::RawPlayMode::RAW_PLAY_MODE getPlayMode();

        representations::NaoJoints &getNaoJoints();

        double getTemperature();

        double getBatteryPercentage();

        Vector3<double> &getTorsoAcceleration();

        Vector3<double> &getTorsoAngularVelocities();

        representations::ForceResistanceData &getLeftFootForceResistanceData();

        representations::ForceResistanceData &getRightFootForceResistanceData();

        representations::HearData getHearData();

        std::vector<perception::VisibleObject *> &getVisibleObjects();

        bool receivedVisionUpdate();

        /**
         * Updates the agent perception values
         *
         * @param list of Perceptor *
         */
        void update(std::vector<Perceptor *> &perceptors);

    protected:

    private:
        int uniformNumber;
        representations::GameState gameState;
        representations::PlaySide::PLAY_SIDE playSide;
        representations::RawPlayMode::RAW_PLAY_MODE playMode;
        bool visionUpdated;
        representations::NaoJoints naoJoints;
        double temperature;
        double globalTime;
        Vector3<double> torsoAngularVelocities;
        Vector3<double> torsoAcceleration;
        std::vector<perception::VisibleObject *> visibleObjects;
        representations::ForceResistanceData leftFootForceResistanceData;
        representations::ForceResistanceData rightFootForceResistanceData;
        representations::HearData hearData;
        double batteryPercentage;
    };

} /* namespace perception */

#endif /* SOURCE_PERCEPTION_AGENTPERCEPTION_H_ */
