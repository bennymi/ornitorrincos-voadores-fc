//
// Created by dicksiano on 8/9/16.
//

#ifndef ITANDROIDS_SOCCER3D_CPP_OWNKICKOFFOwnKickOffSetPlay_H
#define ITANDROIDS_SOCCER3D_CPP_OWNKICKOFFOwnKickOffSetPlay_H


#include "../SetPlay.h"

namespace decision_making {
    namespace behavior {

        class OwnKickOffSetPlay : public SetPlay {
        public:

            /**
            * Default constructor.
            */
            OwnKickOffSetPlay();

            /**
            * Assignment constructor.
            *
            * @param pointer to BehaviorFactory.
            */
            OwnKickOffSetPlay(BehaviorFactory *behaviorFactory);

            /**
            * Destructor.
            */
            virtual ~OwnKickOffSetPlay();

            /**
            * This function controls the actions.
            *
            * @param modeling.
            */
            void behave(modeling::Modeling &modeling);

        private:

        };

    } /* namespace behavior */
} /* namespace decision_making */


#endif //ITANDROIDS_SOCCER3D_CPP_OWNKICKOFFSETPLAY_H
