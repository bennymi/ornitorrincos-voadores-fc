/*
 * GoalPostType.h
 *
 *  Created on: Sep 6, 2015
 *      Author: mmaximo
 */

#ifndef SOURCE_REPRESENTATIONS_GOALPOSTTYPE_H_
#define SOURCE_REPRESENTATIONS_GOALPOSTTYPE_H_

#include <string>

namespace representations {

    class GoalPostType {
    public:
        enum GOAL_POST_TYPE {
            G1L, G2L, G1R, G2R, INVALID
        };

        static const int NUM_GOAL_POST_TYPES = 4;

        static GOAL_POST_TYPE identifyType(std::string goalPostName);
    };

} /* namespace representations */

#endif /* SOURCE_REPRESENTATIONS_GOALPOSTTYPE_H_ */
