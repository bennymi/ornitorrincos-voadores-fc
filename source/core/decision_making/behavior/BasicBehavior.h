/*
 * BasicBehavior.h
 *
 *  Created on: Feb 2, 2016
 *      Author: dicksiano
 */

#ifndef SOURCE_DECISION_MAKING_BEHAVIOR_BASICBEHAVIOR_H_
#define SOURCE_DECISION_MAKING_BEHAVIOR_BASICBEHAVIOR_H_

#include "Behavior.h"
#include "BehaviorState.h"
#include "modeling/Modeling.h"
#include "modeling/WorldModel.h"
#include "control/KickRequest.h"

namespace decision_making {
    namespace behavior {
        class BehaviorFactory;

        class BasicBehavior : public Behavior {
        public:


            /**
             * Default constructor.
             */
            BasicBehavior();

            /**
             * Assignment constructor.
             *
             * @param pointer to BehaviorFactory.
             */
            BasicBehavior(BehaviorFactory *behaviorFactory);

            /**
             * Destructor.
             */
            virtual ~BasicBehavior();

            /**
             * This function controls the agent's basic actions.
             *
             * @param modeling.
             */
            void behave(modeling::Modeling &modeling) override;

            /**
             * Atualizes the value of the behavior attributes.
             *
             * @param modeling
             */
            void changeBasicBehaviorState(modeling::Modeling &modeling);

            /**
             * Reacts according to the BehaviorState object value.
             */
            void perform(modeling::Modeling &modeling);

            /**
             * Method that returns the state of the behavior.
             */
            BehaviorState::BEHAVIOR_STATE getState();

        private:
            itandroids_lib::math::Pose2D behindBallPosition;
            itandroids_lib::math::Pose2D safePosition;
            itandroids_lib::math::Pose2D kickingPosition;

            double distanceFromTarget;
            double angleDisplacement = 0;
            static const double SAFE_ANGLE_DISPLACEMENT;
            static const double AGENT_RADIUS;
            double lastStateSwitchTime;
            BehaviorState::BEHAVIOR_STATE state = BehaviorState::NONE;

            Pose2D calculateBehindBallPosition(modeling::Modeling &modeling);

            Pose2D calculateSafePosition(modeling::Modeling &modeling);

            Pose2D calculateKickingPosition(modeling::Modeling &modeling);

            double calculateTargetAngle(modeling::Modeling &modeling);

        };

    } /* namespace behavior */
} /* namespace decision_making */

#endif /* SOURCE_DECISION_MAKING_BEHAVIOR_BASICBEHAVIOR_H_ */
