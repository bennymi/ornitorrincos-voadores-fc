//
// Created by muzio on 16/04/16.
//

#ifndef SOURCE_MODELING_STATE_ESTIMATION_PARTICLE_H_
#define SOURCE_MODELING_STATE_ESTIMATION_PARTICLE_H_

#include "math/Pose2D.h"

namespace modeling {
    namespace state_estimation {

        using namespace itandroids_lib::math;

        class Particle {
        public:
            Particle();

            Particle(Pose2D pose);

            virtual ~Particle();

            const Pose2D &getPose() const;

            double getWeight() const;

            void setPose(const Pose2D &pose);

            void setWeight(double weight);

        private:
            Pose2D pose;
            double weight;
        };

    } /* namespace state_estimation */
} /* namespace modeling */

#endif //SOURCE_MODELING_STATE_ESTIMATION_PARTICLE_H_
