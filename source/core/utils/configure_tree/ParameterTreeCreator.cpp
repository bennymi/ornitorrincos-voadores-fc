//
// Created by francisco on 09/12/16.
//

#include <boost/property_tree/ptree.hpp>
#include "ParameterTreeCreator.h"

namespace pt = boost::property_tree;

namespace utils {
    namespace configure_tree {

        ParameterTreeCreator::ParameterTreeCreator() {
        }

    template<>
    void ParameterTreeCreator::connectNodesToRoot<pt::ptree, std::string>(std::shared_ptr<BranchParameter<pt::ptree, std::string>> root,
                                                                          std::shared_ptr<Parameter<pt::ptree, std::string>> control,
                                                                          std::shared_ptr<Parameter<pt::ptree, std::string>> representations){
        root->addChild("control", control);
        root->addChild("representations", representations);
    };
    }
}
