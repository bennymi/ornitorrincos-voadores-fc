/*
 * Action.h
 *
 *  Created on: Aug 7, 2015
 *      Author: mmaximo
 */

#ifndef SOURCE_ACTION_ACTION_H_
#define SOURCE_ACTION_ACTION_H_

#include <iostream>

#include "decision_making/DecisionMaking.h"
#include "control/Control.h"
#include "action/EffectorFactory.h"

namespace action {

    using namespace decision_making;
    using namespace control;

    class Action {
    public:
        static const std::string NAO_SCENE_STRING;

        /**
         * Default constructor of Action.
         */

        Action();

        /**
         * Default destructor of Action.
         */

        virtual ~Action();

        virtual void create(int type) = 0;

        virtual void init(int playerNumber, std::string teamName) = 0;

        virtual void act(DecisionMaking &decisionMaking, Control &control) = 0;

        virtual const std::string &getServerMessage() = 0;

    private:
        std::string messageServer;
    };

    class ActionImpl : public Action {
    public:
        /**
         * Default constructor of ActionImpl.
         */
        ActionImpl();

        /**
         * Default destructor of ActionImpl.
         */

        virtual ~ActionImpl();

        /**
         * Adds a Create Effector in the effector factory.
         *
         * @param type the type of create effector.
         */

        void create(int type);

        /**
         * Adds a Init Effector in the effector factory.
         *
         * @param playerNumber the number of the player.
         * @param teamName the name of the team.
         */

        void init(int playerNumber, std::string teamName);

        /**
         * Adds to effector factory Say and Beam Effectors from decision making
         * and Joint Effector from control.
         *
         * @param decisionMaking the decision making.
         * @param control the control with joint commands.
         */

        void act(DecisionMaking &decisionMaking, Control &control);

        /**
         * Gets the server message.
         *
         * @return the server message.
         */

        const std::string &getServerMessage();

    private:
        EffectorFactory effectorFactory;
        std::string serverMessage;
    };

} /* namespace action */

#endif /* SOURCE_ACTION_ACTION_H_ */
