/*
 * MovementSwitchSmoother.cpp
 *
 *  Created on: Oct 27, 2015
 *      Author: mmaximo
 */

#include <math.h>
#include <utils/LogSystem.h>
#include "MovementSwitchSmoother.h"

namespace control {

    const double MovementSwitchSmoother::MAXIMUM_JOINT_DISPLACEMENT = 0.02 * 500.0 * M_PI / 180.0;

    MovementSwitchSmoother::MovementSwitchSmoother() {
    }

    MovementSwitchSmoother::~MovementSwitchSmoother() {
    }

    NaoJoints MovementSwitchSmoother::smooth(NaoJoints &currentPositions) {
        NaoJoints difference = finalPositions - currentPositions;
        double norm = difference.lInfiniteNorm();
        double fraction = MAXIMUM_JOINT_DISPLACEMENT / (norm);

        if (fraction > 1.0)
            fraction = 1.0;
        difference.scale(fraction);

        return currentPositions + difference;
    }

    void MovementSwitchSmoother::set(NaoJoints &finalPositions) {
        this->finalPositions = finalPositions;
    }

} /* namespace control */
