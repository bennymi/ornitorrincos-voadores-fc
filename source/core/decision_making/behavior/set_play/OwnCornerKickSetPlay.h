//
// Created by dicksiano on 8/14/16.
//

#ifndef ITANDROIDS_SOCCER3D_CPP_OWNCORNERKICKSETPLAY_H
#define ITANDROIDS_SOCCER3D_CPP_OWNCORNERKICKSETPLAY_H


#include "../SetPlay.h"

namespace decision_making {
    namespace behavior {

        class OwnCornerKickSetPlay : public SetPlay {
        public:

            /**
            * Default constructor.
            */
            OwnCornerKickSetPlay();

            /**
            * Assignment constructor.
            *
            * @param pointer to BehaviorFactory.
            */
            OwnCornerKickSetPlay(BehaviorFactory *behaviorFactory);

            /**
            * Destructor.
            */
            virtual ~OwnCornerKickSetPlay();

            /**
            * This function controls the actions.
            *
            * @param modeling.
            */
            void behave(modeling::Modeling &modeling);

        private:
        };

    } /* namespace behavior */
} /* namespace decision_making */


#endif //ITANDROIDS_SOCCER3D_CPP_OWNCORNERKICKSETPLAY_H
