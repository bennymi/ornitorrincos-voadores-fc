/*
 * MovementSwitchSmoother.h
 *
 *  Created on: Oct 27, 2015
 *      Author: mmaximo
 */

#ifndef SOURCE_CONTROL_MOVEMENTSWITCHSMOOTHER_H_
#define SOURCE_CONTROL_MOVEMENTSWITCHSMOOTHER_H_

#include "representations/NaoJoints.h"

namespace control {

    using namespace representations;

    class MovementSwitchSmoother {
    public:
        static const double MAXIMUM_JOINT_DISPLACEMENT;

        MovementSwitchSmoother();

        virtual ~MovementSwitchSmoother();

        NaoJoints smooth(NaoJoints &currentPositions);

        void set(NaoJoints &finalPositions);

    private:
        NaoJoints finalPositions;
        NaoJoints integrator;
    };

} /* namespace control */

#endif /* SOURCE_CONTROL_MOVEMENTSWITCHSMOOTHER_H_ */
