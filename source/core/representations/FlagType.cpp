/*
 * FlagType.cpp
 *
 *  Created on: Sep 6, 2015
 *      Author: mmaximo
 */

#include "FlagType.h"

namespace representations {

    FlagType::FLAG_TYPE FlagType::identifyType(std::string flagName) {
        if (flagName.compare("F1L") == 0)
            return FlagType::F1L;
        else if (flagName.compare("F2L") == 0)
            return FlagType::F2L;
        else if (flagName.compare("F1R") == 0)
            return FlagType::F1R;
        else if (flagName.compare("F2R") == 0)
            return FlagType::F2R;
        else
            return FlagType::INVALID;
    }

    std::string FlagType::getFlagName(FlagType::FLAG_TYPE flagType) {
        switch (flagType) {
            case FlagType::F1L:
                return "F1L";
            case FlagType::F2L:
                return "F2L";
            case FlagType::F1R:
                return "F1R";
            case FlagType::F2R:
                return "F2R";
            default:
                return "";
        }
    }

} /* namespace representations */
