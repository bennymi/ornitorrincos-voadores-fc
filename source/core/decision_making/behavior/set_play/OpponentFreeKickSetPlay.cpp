//
// Created by dicksiano on 8/14/16.
//

#include "OpponentFreeKickSetPlay.h"
#include "../BehaviorFactory.h"

namespace decision_making {
    namespace behavior {

        OpponentFreeKickSetPlay::OpponentFreeKickSetPlay() : SetPlay(nullptr) {
        }

        OpponentFreeKickSetPlay::OpponentFreeKickSetPlay(BehaviorFactory *behaviorFactory) : SetPlay(behaviorFactory) {
        }

        OpponentFreeKickSetPlay::~OpponentFreeKickSetPlay() {
        }

        void OpponentFreeKickSetPlay::behave(modeling::Modeling &modeling) {
            /// IMPLEMENTE
        } /* function behave */

    } /* namespace behavior */
} /* namespace decision_making */