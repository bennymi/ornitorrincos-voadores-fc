/*
 * NavigationParams.cpp
 *
 *  Created on: Oct 31, 2015
 *      Author: itandroids
 */

#include "NavigationParams.h"

namespace pt = boost::property_tree;

namespace representations {

    NavigationParams NavigationParams::normalParams;
    NavigationParams NavigationParams::preciseParams;

    NavigationParams::NavigationParams() {


    }

    NavigationParams::~NavigationParams() {

    }

    NavigationParams &NavigationParams::getNavigationParams(OmnidirectionalWalkZMParamsType type) {
        if (type == OmnidirectionalWalkZMParamsType::NORMAL) {
            return normalParams;
        } else if (type == OmnidirectionalWalkZMParamsType::PRECISE) {
            return preciseParams;
        } else {
            return normalParams;
        }

    }

    template<>
    void NavigationParams::setNavigationParameter(Parameter<pt::ptree, std::string> &parameter,
                                                  NavigationParams &navigationParameter) {
        auto &content = parameter.getContent();
        for (boost::property_tree::ptree::value_type &value: content) {
            if (value.first == "max_v_forward")
                navigationParameter.max_v_forward = std::stod(value.second.data());
            if (value.first == "max_v_backward")
                navigationParameter.max_v_backward = std::stod(value.second.data());
            if (value.first == "max_vy")
                navigationParameter.max_vy = std::stod(value.second.data());
            if (value.first == "max_vpsi")
                navigationParameter.max_vpsi = std::stod(value.second.data());
            if (value.first == "close_distance_threshold")
                navigationParameter.close_distance_threshold = std::stod(value.second.data());
            if (value.first == "low_vpsi_factor")
                navigationParameter.low_vpsi_factor = std::stod(value.second.data());
            if (value.first == "low_v_forward_factor")
                navigationParameter.low_v_forward_factor = std::stod(value.second.data());

        }
    }
} /* namespace representations */
