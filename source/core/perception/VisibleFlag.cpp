/*
 * VisibleFlag.cpp
 *
 *  Created on: Sep 1, 2015
 *      Author: itandroids
 */

#include "VisibleFlag.h"

namespace perception {

    /*
     * This constructor to case of no data
     */
    VisibleFlag::VisibleFlag() {
        flagType = representations::FlagType::F1L;
//	flagType = perception::FlagType::LEFT_FIRST_FLAG;
    }


    VisibleFlag::VisibleFlag(representations::FlagType::FLAG_TYPE flagType,
                             Vector3<double> position) :
            flagType(flagType), VisibleObject(
            representations::FlagType::getFlagName(flagType),
            perception::VisibleObjectType::VISIBLE_OBJECT_TYPE::VISIBLE_FLAG,
            position) {
    }

    /**
     * This constructor takes a parser tree node and manipulates it to get information
     * of flag identification
     */

    VisibleFlag::VisibleFlag(utils::parser::ParserTreeNode &node) :
            VisibleObject(node) {
        std::string compareString = node.content;
        flagType = representations::FlagType::identifyType(compareString);
//	if (compareString.compare("FL1") == 0)
//	{
//		flagType = perception::FlagType::LEFT_FIRST_FLAG;
//	} else if (compareString.compare("FL2") == 0) {
//		flagType = perception::FlagType::LEFT_SECOND_FLAG;
//	} else if (compareString.compare("FR1") == 0) {
//		flagType = perception::FlagType::RIGHT_FIRST_FLAG;
//	} else if (compareString.compare("FR2") == 0) {
//		flagType = perception::FlagType::RIGHT_SECOND_FLAG;
//	}
    }

    VisibleFlag::~VisibleFlag() {
    }

    /*
     * Getter method: returns flag type
     */
    representations::FlagType::FLAG_TYPE VisibleFlag::getFlagType() {
        return flagType;
    }

} /* namespace perception */
