/*
 * HingeJointEffector.h
 *
 *  Created on: Aug 8, 2015
 *      Author: mmaximo
 */

#ifndef SOURCE_ACTION_HINGEJOINTEFFECTOR_H_
#define SOURCE_ACTION_HINGEJOINTEFFECTOR_H_

#include "action/Effector.h"

namespace action {

/**
 * Class that represents a hinge joint effector.
 * HingeJointEffector specifies that a given force should be applied to a particular hinge joint.
 * Nao has 22 such hinges.
 */
    class HingeJointEffector : public Effector {
    public:
        /**
         * Creates a HingeJointEffector.
         *
         * @param name the joint's name.
         * @param axisSpeed speed commanded to the joint.
         */
        HingeJointEffector(const std::string &name, double axisSpeed);

        /**
         * Default destructor of HingerJoinEffector.
         */

        virtual ~HingeJointEffector();

        /**
         * Gets the hinge joint effector as a server message.
         *
         * @return the server message.
         */

        std::string toString();

    private:
        std::string name;
        double axisSpeed;
    };

} /* namespace action */

#endif /* SOURCE_ACTION_HINGEJOINTEFFECTOR_H_ */
