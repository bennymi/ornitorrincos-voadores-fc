//
// Created by francisco on 09/12/16.
//

#ifndef ITANDROIDS_SOCCER3D_CPP_REPRESENTATIONSLOADER_H
#define ITANDROIDS_SOCCER3D_CPP_REPRESENTATIONSLOADER_H

#include "utils/patterns/Parameter.h"
#include "utils/patterns/Configurable.h"
#include <boost/property_tree/ptree.hpp>
#include "utils/patterns/Parameter.h"

namespace pt = boost::property_tree;
using ::itandroids_lib::utils::patterns::Configurable;
using ::itandroids_lib::utils::patterns::Parameter;

namespace representations {

    class RepresentationsLoader : public Configurable<RepresentationsLoader> {
    public:

        template<class Content, class Key>
        void setParameter(Parameter<Content, Key> &parameter);

    private:


    };

    template<>
    void RepresentationsLoader::setParameter<pt::ptree, std::string>(Parameter<pt::ptree, std::string> &parameter);
}

#endif //ITANDROIDS_SOCCER3D_CPP_REPRESENTATIONSLOADER_H
