/*
 * Object.h
 *
 *  Created on: Aug 29, 2015
 *      Author: mmaximo
 */

#ifndef SOURCE_REPRESENTATIONS_OBJECT_H_
#define SOURCE_REPRESENTATIONS_OBJECT_H_

namespace representations {

    class Object {
    public:
        Object();

        virtual ~Object();
    };

} /* namespace representations */

#endif /* SOURCE_REPRESENTATIONS_OBJECT_H_ */
