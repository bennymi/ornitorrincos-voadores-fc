/*
 * FieldDescription.h
 *
 *  Created on: Sep 6, 2015
 *      Author: mmaximo
 */

#ifndef SOURCE_MODELING_FIELDDESCRIPTION_H_
#define SOURCE_MODELING_FIELDDESCRIPTION_H_

#include <string>
#include <map>

#include "representations/PlaySide.h"
#include "representations/FlagType.h"
#include "representations/GoalPostType.h"
#include "math/Vector3.h"

namespace modeling {

    using namespace representations;
    using namespace itandroids_lib::math;

/**
 * Class that represents the field description.
 */
    class FieldDescription {
    public:
        static const double FIELD_LENGTH;
        static const double FIELD_WIDTH;
        static const double SOCCER_2D_FIELD_LENGTH;
        static const double SOCCER_2D_FIELD_WIDTH;
        static const double GOAL_AREA_LENGTH;
        static const double GOAL_AREA_WIDTH;
        static const double PENALTY_AREA_LENGTH;

        static const double GOAL_POST_HEIGHT;
        static const double CENTER_CIRCLE_RADIUS;

        /**
         * Default constructor of FieldDescription.
         */

        FieldDescription();

        /**
         * Default destructor of FieldDescription.
         */

        virtual ~FieldDescription();

        /**
         * Updates playSide
         */
        void update(PlaySide::PLAY_SIDE playSide);

        /**
         * Gets the flag known position.
         *
         * @param flagType the type of the flag.
         *
         * @return The flag position.
         */
        Vector3<double> &getFlagKnownPosition(FlagType::FLAG_TYPE flagType);

        /**
         * Gets the goal post know position.
         *
         * @param goalPostType the type of the goal post.
         *
         * @return the goal position.
         */

        Vector3<double> &getGoalPostKnownPosition(GoalPostType::GOAL_POST_TYPE goalPostType);

    private:
        PlaySide::PLAY_SIDE currentPlaySide;

        Vector3<double> flagsPositionsLeft[FlagType::NUM_FLAG_TYPES];
        Vector3<double> goalPostsPositionsLeft[GoalPostType::NUM_GOAL_POST_TYPES];

        Vector3<double> flagsPositionsRight[FlagType::NUM_FLAG_TYPES];
        Vector3<double> goalPostsPositionsRight[GoalPostType::NUM_GOAL_POST_TYPES];
    };

} /* namespace modeling */

#endif /* SOURCE_MODELING_FIELDDESCRIPTION_H_ */
