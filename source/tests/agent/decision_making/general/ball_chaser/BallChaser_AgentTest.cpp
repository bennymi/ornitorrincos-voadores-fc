/*
 * BallChaser_AgentTest.cpp
 *
 *  Created on: Oct 28, 2015
 *      Author: mmaximo
 */

#include <tests/test_lib/utils/DrawVisibleObjects.h>
#include "base/BaseAgentTest.h"

using namespace itandroids_lib::math;

class BallChaser_AgentTest : public BaseAgentTest {
private:
    decision_making::behavior::FocusBall focusBall;
    Vector3<double> position;
    Pose2D pose;

    //Files
    std::ofstream positions;

public:
    virtual void testLoop(int nSteps) override { BaseAgentTest::testLoop(nSteps); }

    virtual void setup() override {

        position = Vector3<double>(0.0, 0.0, 0.0);
        positions.open("positions.txt");

    }

    virtual void runStep() override {
        focusBall.behave(modeling);
        decisionMaking.lookRequest = focusBall.getLookRequest();
        decisionMaking.movementRequest = std::make_shared<control::WalkRequest>(Vector3<double>(0.8, 0.0,
                                                                                                0.0));
        if ((BaseAgentTest::currentStep / 100) % 4 == 0) {
            position.x = -2.0;
            position.y = -2.0;
        }/* else if ((counter/100) % 4 == 1) {
			position.x = 2.0;
			position.y = -2.0;
		} else if ((counter/100) % 4 == 2) {
			position.x = -2.0;
			position.y = 2.0;
		} else {
			position.x = 2.0;
			position.y = 2.0;
		}*/
        //wizard.setAgentPositionAndDirection(1, representations::PlaySide::LEFT, Vector3<double>(position.x, position.y, 0.3), -90.0 + rotation * 180.0 / M_PI);

        //control.naoJoints.neckPitch = 0.5 * cos(2.0 * M_PI * 0.1 * accumulatedTime);
        //control.naoJoints.neckYaw = 0.5 * cos(2.0 * M_PI * 0.1 * accumulatedTime);

        pose = modeling.getWorldModel().getSelf().getPosition();
        Vector3<double> groundTruth = wiz.getAgentTranslation();
        double rotation = wiz.getAgentRotation().getZAngle() - 90.0 * M_PI / 180.0;
        if (std::isnan(rotation))
            rotation = 0.0;
        std::cout << "rotation: " << rotation << std::endl;
        std::string n1("animated.selfPosition");
        double radius = 0.5;
        roboviz.drawCircle(pose.translation.x, pose.translation.y, radius, 1.0,
                           0.0, 0.0, 1.0, &n1);
        roboviz.drawLine(pose.translation.x, pose.translation.y, 0.0,
                         pose.translation.x + radius * cos(pose.rotation),
                         pose.translation.y + radius * sin(pose.rotation), 0.0, 1.0, 0.0,
                         0.0, 1.0, &n1);
        DrawVisibleObjects drawVisibleObjects;
        drawVisibleObjects.draw(Pose2D(rotation, groundTruth.x, groundTruth.y), perception, modeling.getAgentModel(),
                                roboviz);
        std::string buffer("animated");
        if (perception.getAgentPerception().getVisibleObjects().size() != 0)
            roboviz.swapBuffers(&buffer);
        positions << groundTruth.x << " " << groundTruth.y << std::endl;
    }
};

int main() {
    BallChaser_AgentTest t;
    t.testLoop(1000);
    return 0;
}

