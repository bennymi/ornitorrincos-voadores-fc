/*
 * EulerAngles.h
 *
 *  Created on: Aug 29, 2015
 *      Author: mmaximo
 */

#ifndef SOURCE_REPRESENTATIONS_EULERANGLES_H_
#define SOURCE_REPRESENTATIONS_EULERANGLES_H_

namespace representations {

    struct EulerAngles {
        double roll;
        double pitch;
        double yaw;

        EulerAngles();
    };

} /* namespace representations */

#endif /* SOURCE_REPRESENTATIONS_EULERANGLES_H_ */
