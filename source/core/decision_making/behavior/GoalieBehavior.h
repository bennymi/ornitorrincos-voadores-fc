//
// Created by dicksiano on 24/05/16.
//

#ifndef ITANDROIDS_SOCCER3D_CPP_GOALIEBEHAVIOR_H
#define ITANDROIDS_SOCCER3D_CPP_GOALIEBEHAVIOR_H

#include "Behavior.h"
#include "modeling/FieldDescription.h"

namespace decision_making {
    namespace behavior {

        class BehaviorFactory;

/**
 * Class that represents the goalie behavior.
 */
        class GoalieBehavior : public Behavior {
        public:

            /**
             * Default constructor.
             */
            GoalieBehavior();

            /**
             * Assignment constructor.
             *
             * @param pointer to BehaviorFactory.
             */
            GoalieBehavior(BehaviorFactory *behaviorFactory);

            /**
             * Destructor.
             */
            virtual ~GoalieBehavior();

            /**
             * This function controls the goalie's actions.
             *
             * @param modeling.
             */
            void behave(modeling::Modeling &modeling);

        private:
            static constexpr double X_OFFSET_FROM_GOAL = 0.3;
            static constexpr double SAFE_BALL_DISTANCE_FROM_GOAL = 0.5;
            static constexpr double SAFE_BALL_DISTANCE_TO_KICK = 4.2;
            static constexpr double SAFE_BALL_VELOCITY = 0.4;
            static constexpr double SAFE_VELOCITY_TO_KICK = 0.35;
            static constexpr double SAFE_TIME = 0.4;
            static constexpr double DISTANCE_ZONE_1 = 2.25;
            static constexpr double DISTANCE_ZONE_2 = 3.15;

            enum GOALIE_STATES {
                DIVE,
                SHORT_DISTANCE_KICK,
                LONG_DISTANCE_KICK,
                GOING_TO_BALL_POSITION,
                POSITIONING_TO_DEFEND
            };

            GOALIE_STATES state;
            double ballDistanceFromGoal;
            double ballDistanceFromSelf;
            double ballVelocity;
            double selfDistanceFromGoal;
            double ballDistanceFromClosestOpponent;
            double closestOpponentFromBallDistance;

            bool shouldGoToBall(modeling::Modeling &modeling);

            /**
             * This function calculate the goalie's position according to the ball's position.
             *
             * @param modeling.
             */
            Pose2D calculatePosition(modeling::Modeling &modeling);

            /**
             * This function changes the goalie's state.
             *
             * @param modeling.
             */
            void changeStates(modeling::Modeling &modeling);

            /**
             * This function performs an action according to goalie's state.
             *
             * @param modeling.
             */
            void perform(modeling::Modeling &modeling);

            /**
             * This function calculates the parameters that will be used to change the goalie's state.
             *
             * @param modeling.
             */
            void calculateParameters(modeling::Modeling &modeling);

            /**
             * This function check if the dive movement should be used.
             *
             * @param modeling.
             */
            bool evaluateDive(modeling::Modeling &modeling);

            /**
             * This function check if the short kick movement should be used.
             *
             * @param modeling.
             */
            bool evaluateShortKick(modeling::Modeling &modeling);

            /**
             * This function check if the long kick movement should be used.
             *
             * @param modeling.
             */
            bool evaluateLongKick(modeling::Modeling &modeling);

        };

    } /* namespace behavior */
} /* namespace decision_making */


#endif //ITANDROIDS_SOCCER3D_CPP_GOALIEBEHAVIOR_H