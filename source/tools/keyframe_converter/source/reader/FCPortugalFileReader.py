# -*- coding: utf-8 -*-

from reader import KeyframeFileReader
from representations import KeyframeMovement
from representations import KeyframeStep
import xml.etree.ElementTree as Etree
import math


class FCPortugalFileReader(KeyframeFileReader.KeyframeFileReader):

    def __init__(self, inputFile):
        super().__init__(inputFile)
        self.interpolator = 'linear'

        self.NUM_JOINTS = 22
        self.lastJoints = [0] * self.NUM_JOINTS

    def readFile(self):
        tree = Etree.parse(self.inputFile)
        root = tree.getroot()
        steps = []
        name = ""
        name = root.attrib['name']
        for child in root:
            keyframeStep = self.getDataFromSlot(child)
            steps.append(keyframeStep)
        self.keyframeMovement = KeyframeMovement.KeyframeMovement(
            self.interpolator, steps, name)

    def convertJoints(self, joints):
        #It seems that the map is wrong, retry to write it
        strToCorrectedJoints = {"head1": 0, "head2": 1, "lleg1": 6, "lleg2": 7,
        "lleg3": 8, "lleg4": 9, "lleg5": 10, "lleg6": 11, "rleg1": 16,
        "rleg2": 17, "rleg3": 18, "rleg4": 19, "rleg5": 20, "rleg6": 21,
        "larm1": 2, "larm2": 3, "larm3": 4, "larm4": 5, "rarm1": 12,
         "rarm2": 13, "rarm3": 14, "rarm4": 15}
        uncorrectedJointsToStr = ["head1", "head2", "lleg1", "rleg1",
        "lleg2", "rleg2", "lleg3", "rleg3", "lleg4",
        "rleg4", "lleg5", "rleg5", "lleg6", "rleg6",
        "larm1", "rarm1", "larm2", "rarm2", "larm3",
        "rarm3", "larm4", "rarm4"]
        direction_values = [1, 1, -1, 1, 1, 1, -1, 1, -1, -1, -1,
             1, -1, 1, 1, 1, 1, 1, -1, -1, -1, 1]
        targetJoints = [0] * self.NUM_JOINTS
        for index, key in enumerate(uncorrectedJointsToStr):
            targetJoints[strToCorrectedJoints[key]] = \
            joints[index] * direction_values[strToCorrectedJoints[key]]

        return targetJoints

    def getTimeStepFromSlot(self, slot):
        time = float(slot.attrib['delta'])
        return time

    def getJointsFromSlot(self, slot):
        joints = self.lastJoints
        for move in slot:
            attributes = move.attrib
            joints[int(attributes['id'])] = \
             math.radians(float(attributes['angle']))
        self.lastJoints = joints
        convertedJoints = self.convertJoints(joints)
        return convertedJoints

    def getDataFromSlot(self, slot):
        step = KeyframeStep.KeyframeStep()
        step.setJointPositionsAsList(self.getJointsFromSlot(slot))
        step.time = self.getTimeStepFromSlot(slot)
        return step


