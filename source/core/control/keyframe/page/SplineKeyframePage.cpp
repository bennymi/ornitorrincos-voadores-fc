//
// Created by francisco on 5/22/16.
//

#include "SplineKeyframePage.h"

namespace control {
    namespace keyframe {

        std::vector<double> SplineKeyframePage::interpolate() {
            std::vector<double> interpolateJoints(representations::NaoJoints::NUM_JOINTS, 0);
            for (int i = 0; i < interpolators.size(); i++) {
                interpolateJoints[i] = interpolators[i].interpolate(currentTime * speedRate);
            }
            return interpolateJoints;
        }

        void SplineKeyframePage::createInterpolators() {
            interpolators.clear();
            std::vector<std::vector<Vector2<double>>> positions(representations::NaoJoints::NUM_JOINTS,
                                                                std::vector<Vector2<double>>());
            double stepsTime = 0;
            double firstStepTime = steps[0]->getDuration();
            for (auto &step : steps) {
                stepsTime += step->getDuration();
                auto stepJointMap = step->getJoints().getAsMap();
                for (auto &k : stepJointMap) {
                    Vector2<double> auxVector(stepsTime - firstStepTime, k.second);
                    positions[k.first].push_back(auxVector);
                }
            }
            for (auto &k : positions) {
                interpolators.push_back(SplineInterpolator(k));
            }

        }
    }
}




