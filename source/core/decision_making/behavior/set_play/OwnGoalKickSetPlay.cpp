//
// Created by dicksiano on 8/14/16.
//

#include "OwnGoalKickSetPlay.h"
#include "../BehaviorFactory.h"

namespace decision_making {
    namespace behavior {

        OwnGoalKickSetPlay::OwnGoalKickSetPlay() : SetPlay(nullptr) {
        }

        OwnGoalKickSetPlay::OwnGoalKickSetPlay(BehaviorFactory *behaviorFactory) : SetPlay(behaviorFactory) {
        }

        OwnGoalKickSetPlay::~OwnGoalKickSetPlay() {
        }

        void OwnGoalKickSetPlay::behave(modeling::Modeling &modeling) {
            /* Delaunay */
            goToDelaunayPositioning(modeling);
        } /* function behave */

    } /* namespace behavior */
} /* namespace decision_making */