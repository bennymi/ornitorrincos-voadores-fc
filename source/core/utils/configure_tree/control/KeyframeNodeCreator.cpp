//
// Created by francisco on 18/12/16.
//

#include "KeyframeNodeCreator.h"

#include <boost/property_tree/json_parser.hpp>

using namespace boost::filesystem;

using ::itandroids_lib::utils::patterns::BranchParameter;
using ::itandroids_lib::utils::patterns::LeafParameter;

namespace utils{
namespace configure_tree {
namespace control {
template<>
void KeyframeNodeCreator::loadParameterContent<pt::ptree, std::string>(std::shared_ptr<LeafParameter<pt::ptree,std::string>> param, std::string filePath){
    auto & content = param->getContent();
    boost::property_tree::read_json(filePath, content);
};

KeyframeNodeCreator::KeyframeNodeCreator(std::string folderPath) : folderPath(folderPath){

}
}
}
}

