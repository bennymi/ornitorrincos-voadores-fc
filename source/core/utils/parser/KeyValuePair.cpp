/*
 * KeyValuePair.cpp
 *
 *  Created on: Aug 9, 2015
 *      Author: mmaximo
 */

#include "KeyValuePair.h"

namespace utils {
    namespace parser {

        KeyValuePair::KeyValuePair(const std::string string, char delimiter) {
            for (int i = 0; i < string.length(); ++i) {
                if (string[i] == delimiter) {
                    key = string.substr(0, i);
                    value = string.substr(i + 1, string.length() - i - 1);
                    break;
                }
            }
        }

        KeyValuePair::~KeyValuePair() {
        }

    } /* namespace parser */
} /* namespace utils */
