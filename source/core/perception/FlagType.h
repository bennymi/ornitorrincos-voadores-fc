/*
 * FlagType.h
 *
 *  Created on: Sep 11, 2015
 *      Author: itandroids
 */

#ifndef SOURCE_PERCEPTION_FLAGTYPE_H_
#define SOURCE_PERCEPTION_FLAGTYPE_H_

namespace perception {
    class FlagType {
    public:
        enum FLAG_TYPE {
            LEFT_FIRST_FLAG,
            LEFT_SECOND_FLAG,
            RIGHT_FIRST_FLAG,
            RIGHT_SECOND_FLAG,
        };
    };

} /* namespace perception */

#endif /* SOURCE_PERCEPTION_FLAGTYPE_H_ */
