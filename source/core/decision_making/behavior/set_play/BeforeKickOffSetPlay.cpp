//
// Created by dicksiano on 8/14/16.
//

#include "BeforeKickOffSetPlay.h"
#include "../BehaviorFactory.h"

namespace decision_making {
    namespace behavior {

        BeforeKickOffSetPlay::BeforeKickOffSetPlay() : Behavior(nullptr) {
        }

        BeforeKickOffSetPlay::BeforeKickOffSetPlay(BehaviorFactory *behaviorFactory) : Behavior(behaviorFactory) {
        }

        BeforeKickOffSetPlay::~BeforeKickOffSetPlay() {
        }

        void BeforeKickOffSetPlay::behave(modeling::Modeling &modeling) {
            behaviorFactory->getBeamBehavior().behave(modeling);
            beamRequest = behaviorFactory->getBeamBehavior().getBeamRequest();
        } /* function behave */

    } /* namespace behavior */
} /* namespace decision_making */