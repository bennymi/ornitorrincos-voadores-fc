//
// Created by dicksiano on 8/14/16.
//

#ifndef ITANDROIDS_SOCCER3D_CPP_OWNGOALKICKSETPLAY_H
#define ITANDROIDS_SOCCER3D_CPP_OWNGOALKICKSETPLAY_H

#include "../SetPlay.h"

namespace decision_making {
    namespace behavior {

        class OwnGoalKickSetPlay : public SetPlay {
        public:
            /**
            * Default constructor.
            */
            OwnGoalKickSetPlay();

            /**
            * Assignment constructor.
            *
            * @param pointer to BehaviorFactory.
            */
            OwnGoalKickSetPlay(BehaviorFactory *behaviorFactory);

            /**
            * Destructor.
            */
            virtual ~OwnGoalKickSetPlay();

            /**
            * This function controls the actions.
            *
            * @param modeling.
            */
            void behave(modeling::Modeling &modeling);

        private:

        };

    } /* namespace behavior */
} /* namespace decision_making */


#endif //ITANDROIDS_SOCCER3D_CPP_OWNGOALKICKSETPLAY_H
