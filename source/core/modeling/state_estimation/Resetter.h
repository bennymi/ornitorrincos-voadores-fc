//
// Created by muzio on 17/05/16.
//

#ifndef SOURCE_MODELING_STATE_ESTIMATION_RESETTER_H_
#define SOURCE_MODELING_STATE_ESTIMATION_RESETTER_H_

#include "Particle.h"
#include "modeling/FieldDescription.h"
#include "geometry/Line2D.h"
#include "LocalizationRepresentations.h"
#include <vector>
#include <boost/random.hpp>

namespace modeling {
    namespace state_estimation {

        using itandroids_lib::geometry::Line2D;

        class Resetter {
        public:
            Resetter(const FieldDescription &fieldDescription, double distanceSigma, double bearingSigma);

            void
            sensorReset(Particle *particle, int numParticles, std::vector<LandmarkObservation> &landmarkObservations,
                        std::vector<Line2D> &lineObservations,
                        double resetProbability);

        private:
            const FieldDescription &fieldDescription;

            boost::variate_generator<boost::mt19937 &, boost::uniform_real<> > uniformDistribution;
            boost::variate_generator<boost::mt19937 &, boost::normal_distribution<> > distanceDistribution;
            boost::variate_generator<boost::mt19937 &, boost::normal_distribution<> > bearingDistribution;


            void computeParticleResetWithOneLandmark(Particle &particle, LandmarkObservation observation,
                                                     Line2D observedLine1, Line2D observedLine2);

            void computeParticleResetWithTwoLandmarks(Particle &particle, LandmarkObservation observation1,
                                                      LandmarkObservation observation2);

            //not being used yet
            void computeParticleResetWithThreeLandmarks(Particle &particle, LandmarkObservation observation1,
                                                        LandmarkObservation observation2,
                                                        LandmarkObservation observation3);

            void
            makeResettedParticle(Particle &particle, Vector2<double> translation, LandmarkObservation &observation1,
                                 LandmarkObservation &observation2);

            bool computeTwoCirclesIntersection(Vector2<double> center1, double radius1, Vector2<double> center2,
                                               double radius2,
                                               Vector2<double> &intersection1, Vector2<double> &intersection2);

            bool computeClosestCircleIntersection(Vector2<double> center1, double radius1, Vector2<double> center2,
                                                  double radius2,
                                                  Vector2<double> reference, Vector2<double> &closestIntersection);
        };

    } /* namespace state_estimation */
} /* namespace modeling */

#endif //SOURCE_MODELING_STATE_ESTIMATION_RESETTER_H_
