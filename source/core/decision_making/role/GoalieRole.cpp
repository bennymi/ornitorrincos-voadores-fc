//
// Created by muzio on 30/05/16.
//

#include "GoalieRole.h"

namespace decision_making {
    namespace role {

        GoalieRole::GoalieRole() {
        }


        GoalieRole::~GoalieRole() {
        }

        void GoalieRole::behaveMovement(modeling::Modeling &modeling) {
            if (modeling.getWorldModel().getPlayMode() == representations::PlayMode::BEFORE_KICK_OFF ||
                modeling.getWorldModel().getPlayMode() == representations::PlayMode::OWN_GOAL ||
                modeling.getWorldModel().getPlayMode() == representations::PlayMode::OPPONENT_GOAL) {

                behaviorFactory.getBeamBehavior().behave(modeling);
                beamRequest = behaviorFactory.getBeamBehavior().getBeamRequest();
                return;
            }

            behaviorFactory.getGoalieBehavior().behave(modeling);
            movementRequest = behaviorFactory.getGoalieBehavior().getMovementRequest();
        }


    } /* namespace role */
} /* namespace decision_making */