//
// Created by francisco on 5/27/16.
//

#include "StepKeyframePage.h"

namespace control {
    namespace keyframe {
        std::vector<double> StepKeyframePage::interpolate() {
            std::vector<double> v(representations::NaoJoints::NUM_JOINTS);
            auto mapAfter = steps[currentStep + 1]->getJoints().getAsMap();
            for (int i = 0; i < representations::NaoJoints::NUM_JOINTS; i++) {
                v[i] = mapAfter[static_cast<representations::NaoJoints::HINGE_JOINTS >(i)];
            }
            return v;
        }

        void StepKeyframePage::createInterpolators() {

        }
    }
}