//
// Created by francisco on 5/27/16.
//

#ifndef ITANDROIDS_SOCCER3D_CPP_STEPKEYFRAMEPAGE_H
#define ITANDROIDS_SOCCER3D_CPP_STEPKEYFRAMEPAGE_H

#include "KeyframePage.h"

namespace control {
    namespace keyframe {
        class StepKeyframePage : public KeyframePage {
        public:
            std::vector<double> interpolate() override;

        private:
            virtual void createInterpolators() override;
        };
    }
}


#endif //ITANDROIDS_SOCCER3D_CPP_STEPKEYFRAMEPAGE_H
