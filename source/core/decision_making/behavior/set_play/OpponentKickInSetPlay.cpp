//
// Created by dicksiano on 8/14/16.
//

#include "OpponentKickInSetPlay.h"
#include "../BehaviorFactory.h"

namespace decision_making {
    namespace behavior {

        OpponentKickInSetPlay::OpponentKickInSetPlay() : SetPlay(nullptr) {
        }

        OpponentKickInSetPlay::OpponentKickInSetPlay(BehaviorFactory *behaviorFactory) : SetPlay(behaviorFactory) {
        }

        OpponentKickInSetPlay::~OpponentKickInSetPlay() {
        }

        void OpponentKickInSetPlay::behave(modeling::Modeling &modeling) {
            /// IMPLEMENTE
        } /* function behave */

    } /* namespace behavior */
} /* namespace decision_making */