//
// Created by francisco on 16/05/16.
//

#ifndef ITANDROIDS_SOCCER3D_CPP_LOOKBEHAVIOR_H
#define ITANDROIDS_SOCCER3D_CPP_LOOKBEHAVIOR_H


#include <core/control/LookRequest.h>
#include "Behavior.h"

namespace decision_making {
    namespace behavior {
        class LookBehavior : public Behavior {
        public:
            LookBehavior(BehaviorFactory *factory);

            const std::shared_ptr<control::LookRequest> &getLookRequest();

        protected:
            std::shared_ptr<control::LookRequest> lookRequest;
        private:
        };
    }
}


#endif //ITANDROIDS_SOCCER3D_CPP_LOOKBEHAVIOR_H
