/*
 * HingeJointPerceptor.cpp
 *
 *  Created on: Aug 8, 2015
 *      Author: mmaximo
 */

#include "perception/HingeJointPerceptor.h"

namespace perception {

    /**
     * This constructor takes a parser tree node and manipulates it to get information
     * of name and angle.
     */
    HingeJointPerceptor::HingeJointPerceptor(utils::parser::ParserTreeNode &node) :
            Perceptor(node) {
        //The first child of parser tree node is the name
        name =
                utils::parser::KeyValuePair(node.children[0]->content, ' ').value.c_str();
        //The second child of parser tree node is the angle of hinge joint
        angle =
                MathUtils::degreesToRadians(
                        atof(
                                utils::parser::KeyValuePair(
                                        node.children[1]->content, ' ').value.c_str()));
    }

    HingeJointPerceptor::~HingeJointPerceptor() {

    }

    /*
     * Getter method: returns the angle
     */
    double HingeJointPerceptor::getAngle() {
        return angle;
    }

} /* namespace perception */
