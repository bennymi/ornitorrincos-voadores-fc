/*
 * BallTracker.cpp
 *
 *  Created on: Aug 29, 2015
 *      Author: mmaximo
 */

#include "BallTracker.h"

namespace modeling {
    namespace state_estimation {

        BallTracker::BallTracker() : N_OF_CICLES(100) {

            Eigen::MatrixXd A(4, 4);
            Eigen::MatrixXd B(4, 2);
            Eigen::MatrixXd C(2, 4);
            Eigen::MatrixXd Q(4, 4);
            Eigen::MatrixXd W(2, 2);

            double dt = 0.02;
            A << 1, 0, dt, 0,
                    0, 1, 0, dt,
                    0, 0, 1, 0,
                    0, 0, 0, 1;
            B << (dt * dt / 2), 0,
                    0, (dt * dt / 2),
                    dt, 0,
                    0, dt;
//            B<<-1,0,
//                    0,-1,
//                    0,0,
//                    0,0;
            C << 1, 0, 0, 0,
                    0, 1, 0, 0;
            Q << 4e-8, 0, 4e-06, 0,
                    0, 4e-8, 0, 4e-6,
                    4e-6, 0, 0.0004, 0,
                    0, 4e-6, 0, 0.0004;
            Q = Q * 1e3;
            W << 0.8, 0,
                    0, 0.8;
            stochasticModel = LinearDiscreteStochasticModel(A, B, Q, C, W);

            Eigen::VectorXd initialState(4);
            initialState << 0, 0, 0, 0;
            Eigen::MatrixXd initialCovariance(4, 4);
            initialCovariance << 10, 0, 0, 0,
                    0, 10, 0, 0,
                    0, 0, 10, 0,
                    0, 0, 0, 10;
            lostBall = true;
            KF = LinearKalmanFilter(stochasticModel, initialState, initialCovariance);
        }

        BallTracker::~BallTracker() {
        }

        Pose2D BallTracker::getPosition() {
            return Pose2D(KF.getStateEstimative()[0], KF.getStateEstimative()[1]);
        }

        Pose2D BallTracker::getVelocity() {
            return Pose2D(KF.getStateEstimative()[2], KF.getStateEstimative()[3]);
        }

        void BallTracker::update(double globalTime,
                                 const Pose2D &odometry,
                                 perception::VisibleBall *visibleBall, Pose2D &agentEstimate) {
            counterNotViewBall = 0;
            Eigen::VectorXd u(2);
            Eigen::VectorXd z(2);
            u << 0, 0;
            z << visibleBall->getPosition().x, visibleBall->getPosition().y;
            if (lostBall) {
                KF.resetFromObservation(z);
                lostBall = false;
            }
            Eigen::MatrixXd rot(4, 4);
            double rotation = odometry.rotation;
            rot << cos(rotation), sin(rotation), 0, 0,
                    sin(-1 * rotation), cos(rotation), 0, 0,
                    0, 0, cos(rotation), sin(rotation),
                    0, 0, sin(-1 * rotation), cos(rotation);
            Eigen::VectorXd disp(4);
            disp << (-1) * odometry.translation.x, (-1) * odometry.translation.y, 0, 0;
            KF.referentialChange(rot, disp);
            KF.prediction(u);
            KF.filtering(z);
        }

        void BallTracker::update(double globalTime,
                                 const Pose2D &odometry) {
            if ((counterNotViewBall >= 10 && (KF.getStateEstimative()[2] > 1.5 || KF.getStateEstimative()[3] > 1.5)) ||
                counterNotViewBall >= N_OF_CICLES) {
                resetEstimate();
                lostBall = true;
            }
            if (!lostBall) {
                counterNotViewBall += 1;
                Eigen::VectorXd u(2);
                u << 0, 0;
                Eigen::MatrixXd rot(4, 4);
                double rotation = odometry.rotation;
                rot << cos(rotation), sin(rotation), 0, 0,
                        sin(-1 * rotation), cos(rotation), 0, 0,
                        0, 0, cos(rotation), sin(rotation),
                        0, 0, sin(-1 * rotation), cos(rotation);
                Eigen::VectorXd disp(4);
                disp << (-1) * odometry.translation.x, (-1) * odometry.translation.y, 0, 0;
                KF.referentialChange(rot, disp);
                KF.prediction(u);
            }
        }

        bool BallTracker::hasLostBall() {
            return lostBall;
        }

        void BallTracker::resetEstimate() {
            Eigen::VectorXd resetState(4);
            resetState << 0, 0, 0, 0;
            Eigen::MatrixXd resetCovariance(4, 4);
            resetCovariance << 10, 0, 0, 0,
                    0, 10, 0, 0,
                    0, 0, 10, 0,
                    0, 0, 0, 10;
            KF.resetFromState(resetState, resetCovariance);
        }
    } /* namespace state_estimation */
} /* namespace modeling */
