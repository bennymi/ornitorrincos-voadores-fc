//
// Created by francisco on 16/12/16.
//

#ifndef ITANDROIDS_SOCCER3D_CPP_AGENTCOMMANDLINEPARSER_H
#define ITANDROIDS_SOCCER3D_CPP_AGENTCOMMANDLINEPARSER_H

#include "utils/command_line_parser/CommandLineParser.h"
#include "representations/ITAndroidsConstants.h"

namespace utils{
    namespace command_line_parser{
        class AgentCommandLineParser : public CommandLineParser{
        public:
            /**
             * Default Constructor, sets default values for initial values
             */
            AgentCommandLineParser();
            /**
             * This function sets the values needed by an agent
             * @param argc number of command line arguments
             * @param argv command line arguments
             */
            virtual void parse(int argc, char * argv[]) override;

            virtual std::string getServerIP();
            virtual int getServerPort();
            virtual std::string getTeamName();
            virtual int getAgentNumber();

        private:
            std::string serverIP;
            int serverPort;
            std::string teamName;
            int agentNumber;
        };
    }
}




#endif //ITANDROIDS_SOCCER3D_CPP_AGENTCOMMANDLINEPARSER_H
