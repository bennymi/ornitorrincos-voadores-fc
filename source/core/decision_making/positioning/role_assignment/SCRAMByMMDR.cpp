//
// Created by luckeciano on 8/10/16.
//

#include <core/modeling/WorldModel.h>

#include "SCRAMByMMDR.h"

namespace decision_making {
    namespace positioning {
        namespace role_assignment {
            void SCRAMByMMDR::solvePriority(std::vector<int> finalTargets) {
                /*priority vectors:
                * 0 - goalie;
                * 1 - backRight, 2 - backLeft, 3 - mid
                * 4 - wingLeft, 5 - wingRight, 6 - supporter
                * 7 - fowardLeft, 8 - fowardRight, 9 - onBall, 10 - fowardCenter
                */
                for (int i = 0; i < finalTargets.size(); i++) {
                    if (finalTargets[i] < modeling::WorldModel::NUM_PLAYERS_PER_TEAM) {
                        priorityDistance[i] = 3;
                        priorityValue[i] = 0;
                    } else { //priority for marking positions
                        priorityValue[i] = 100;
                        priorityDistance[i] = 3;
                    }

                }
                priorityValue[0] = 0;

            }

            std::vector<Edge> SCRAMByMMDR::mmdrSolve() {
                Test t = test;
                int n = t.getStarts().size();
                std::vector<Edge> edges;
                std::vector<Edge> answer;
                //Create edges from all pairwise distances
                for (int i = 0; i < n; i++) {
                    for (int j = 0; j < n; j++) {
                        std::pair<double, std::pair<int, int> >
                                costRoleAssign(getDist(t.getStarts()[i], t.getTargets()[j]), std::make_pair(i, j));
                        if (getDist(t.getStarts()[i], t.getTargets()[j]) > priorityDistance[j]) //cost for priorization
                            costRoleAssign.first += priorityValue[j];
                        edges.push_back(costRoleAssign);
                    }
                }
                std::sort(edges.begin(), edges.end());

                HungarianAlg hungarianAlg;

                long lastIndex = -1;
                for (int i = 0; i < edges.size(); i++) {
                    if (!i || edges[i].first != edges[lastIndex].first)
                        lastIndex = i;
                    hungarianAlg.newDist[edges[i].second.first][edges[i].second.second] = -lastIndex;
                }
                hungarianAlg.nLarge = n;
                hungarianAlg.hungarianLarge();
                for (int i = 0; i < n; i++) {
                    //printf ("Got: %d %d %lf\n", i, h[i], getDist(t.starts[i], t.targets[h[i]]));
                    answer.push_back(std::make_pair(getDist(t.getStarts()[i], t.getTargets()[hungarianAlg.xyLarge[i]]),
                                                    std::make_pair(i, hungarianAlg.xyLarge[i])));
                }

                //answer now contains the correct answer
                return answer;
            }
        }
    }
}