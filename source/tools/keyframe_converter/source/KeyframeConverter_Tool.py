# -*- coding: utf-8 -*-
#This is the main file for the Keyframe extractor tool

# It must run as a python3 script with command line arguments
# for configuration of the files

import argparse
from input_loader import InputLoader

parser = argparse.ArgumentParser(description=
"This is a tool for converting keyframes of multiple types")
parser.add_argument('-i', '--input',
help="Input file name", required=True)
parser.add_argument('-o', '--output',
help="Output file name")
parser.add_argument('-fr', '--file-reader', help="File reader type, can be \"FCPortugal\" or \"page\" ",
required=True)
parser.add_argument('-fw', '--file-writer', help="File writer type, can be \"page\" or \"json\"",
required=True)

inputLoader = InputLoader.InputLoader()

inputLoader.loadInput(parser.parse_args())

reader = inputLoader.getReader()

finalKeyframeMovement = reader.getKeyframeMovement()

fileWriter = inputLoader.getWriter()

fileWriter.writeToFile(inputLoader.getOutputFile(), finalKeyframeMovement)

