/*
 * VisionPerceptor.cpp
 *
 *  Created on: Aug 9, 2015
 *      Author: mmaximo
 */

#include "VisionPerceptor.h"

namespace perception {
    /*
     * Constructor to the case of no data
     */
    VisionPerceptor::VisionPerceptor() {
        name = "See";
        visibleObjects.clear();
    }

    /*
     * Destructor cleares the visible objects list
     */
    VisionPerceptor::~VisionPerceptor() {
        visibleObjects.clear();
    }

    /**
     * This constructor takes a parser tree node and manipulates it to get information
     * of vision and updates the list of visible objects.
     */
    VisionPerceptor::VisionPerceptor(utils::parser::ParserTreeNode &node) : Perceptor(node) {
        visibleObjects.clear();
        std::vector<utils::parser::ParserTreeNode *>::iterator vectorIterator;
        std::string iterString;
        //Iterates over the children of node and add visibles objects to the vector
        for (vectorIterator = node.children.begin();
             vectorIterator != node.children.end(); vectorIterator++) {
            iterString = (**vectorIterator).content;
            if (iterString.compare("F1L") == 0 || iterString.compare("F2L") == 0
                || iterString.compare("F1R") == 0
                || iterString.compare("F2R") == 0) {
                perception::VisibleFlag *visibleFlag = new VisibleFlag(
                        **vectorIterator);
                visibleObjects.push_back(visibleFlag);
            } else if (iterString.compare("P") == 0) {
                perception::VisiblePlayer *visiblePlayer = new VisiblePlayer(
                        **vectorIterator);
                visibleObjects.push_back(visiblePlayer);
            } else if (iterString.compare("L") == 0) {
                perception::VisibleLine *visibleLine = new VisibleLine(
                        **vectorIterator);
                visibleObjects.push_back(visibleLine);
            } else if (iterString.compare("G1L") == 0
                       || iterString.compare("G2L") == 0
                       || iterString.compare("G1R") == 0
                       || iterString.compare("G2R") == 0) {
                perception::VisibleGoalPost *visiblePost = new VisibleGoalPost(
                        **vectorIterator);
                visibleObjects.push_back(visiblePost);
            } else if (iterString.compare("B") == 0) {
                perception::VisibleBall *visibleBall = new VisibleBall(
                        **vectorIterator);
                //std::cout << visibleBall->getPosition().x << " " << visibleBall->getPosition().y << " " << visibleBall->getPosition().z << std::endl;
                visibleObjects.push_back(visibleBall);
            }
        }
    }

    /*
     * Getter method: returns the list of visible objetcs.
     */
    std::vector<perception::VisibleObject *> VisionPerceptor::getVisibleObjects() {
        return visibleObjects;
    }

} /* namespace perception */
