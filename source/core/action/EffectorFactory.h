/*
 * EffectorFactory.h
 *
 *  Created on: Sep 30, 2015
 *      Author: mmaximo
 */

#ifndef SOURCE_ACTION_EFFECTORFACTORY_H_
#define SOURCE_ACTION_EFFECTORFACTORY_H_

#include <iostream>
#include <list>
#include <map>

#include "action/Effector.h"
#include "representations/NaoJoints.h"
#include "math/Vector3.h"
#include "math/Pose2D.h"

namespace action {

    using namespace representations;
    using namespace itandroids_lib::math;

/**
 * Class that stores effectors.
 */
    class EffectorFactory {
    public:
        /**
         * Default constructor of EffectorFactory.
         */
        EffectorFactory();

        /**
         * Default destructor of EffectorFactory.
         */

        virtual ~EffectorFactory();

        /**
         * Clears the effector factory.
         */

        void clear();

        /**
         * Adds joint effectors in effector factory.
         *
         * @param naoJoints the joints.
         */

        void addJointsEffectors(NaoJoints &naoJoints);

        /**
         * Adds say effectors in effector factory.
         *
         * @param message the message of say effector.
         */

        void addSayEffector(const std::string &message);

        /**
         * Adds a create effector in effector factory.
         *
         * @param filename file that specifies the
         * 				   robot configuration.
         * @param type the robot type.
         */

        void addCreateEffector(const std::string &filename, int type);

        /**
         * Adds a init effector in effector factory.
         *
         * @param playerNumber the number of the player.
         * @param teamName the name of the team.
         */

        void addInitEffector(int playerNumber, const std::string &teamName);

        /**
         * Adds a beam effector in effector factory.
         *
         * @param pose coordinates of the player.
         */

        void addBeamEffector(const Pose2D &pose);

        /**
         * Gets the list of effectors as a server message.
         *
         * @return the server message.
         */

        std::string toString();

    private:
        std::list<Effector *> effectors;
        std::map<NaoJoints::HINGE_JOINTS, std::string> jointsEffectorsNamesMap;
    };

} /* namespace action */

#endif /* SOURCE_ACTION_EFFECTORFACTORY_H_ */
