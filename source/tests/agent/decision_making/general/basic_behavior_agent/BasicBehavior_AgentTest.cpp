//
// Created by francisco on 05/02/16.
//

#include "base/BaseAgentTest.h"


class BasicBehavior_AgentTest : public BaseAgentTest {
private:
    //Head movimentation
    double minPan;
    double maxPan;
    double panSpeed;
    double pan;

public:
    virtual void testLoop(int nSteps) { BaseAgentTest::testLoop(nSteps); }

    virtual void setup() {
        minPan = -60.0;
        maxPan = 60.0;
        panSpeed = 40.0;
        pan = 0.0;
    };

    virtual void runStep() {
        std::string name("animated");
        std::string buff("animated");
        roboviz.drawLine(modeling.getWorldModel().getSelf().getPosition().translation.x,
                         modeling.getWorldModel().getSelf().getPosition().translation.y, 0,
                         modeling.getWorldModel().getSelf().getPosition().translation.x,
                         modeling.getWorldModel().getSelf().getPosition().translation.y, 1, 1, 255, 255, 255, &name);
        roboviz.swapBuffers(&buff);
        behaviorFactory.getBasicBehavior().behave(modeling);
        decisionMaking.movementRequest = behaviorFactory.getBasicBehavior().getMovementRequest();

        decisionMaking.lookRequest = std::make_shared<control::LookRequest>(pan * M_PI / 180.0, 0.0);

        pan += panSpeed * 0.02;
        if (pan > maxPan) {
            pan = maxPan;
            panSpeed = -panSpeed;
        } else if (pan < minPan) {
            pan = minPan;
            panSpeed = -panSpeed;
        }
        std::string buffer("animated");
        if (perception.getAgentPerception().getVisibleObjects().size() != 0)
            roboviz.swapBuffers(&buffer);
    }

    virtual void finish() {
    }
};

int main() {
    BasicBehavior_AgentTest t;
    t.testLoop(50000);

}

