//

// Created by luckeciano on 8/9/16.

//


#include "HungarianAlg.h"

using namespace decision_making::positioning::role_assignment;


namespace decision_making {

    namespace positioning {

        namespace role_assignment {


            void HungarianAlg::initLabelsLarge() {

                memset(lxLarge, 0, sizeof(lxLarge));

                memset(lyLarge, 0, sizeof(lyLarge));

                for (int x = 0; x < nLarge; x++) {

                    for (int y = 0; y < nLarge; y++) {

                        uint64 cost[DATA_64WORDS_SIZE];

                        Bit64Operator::setBit(cost, newDist[x][y]);

                        if (Bit64Operator::isLessThan(lxLarge[x], cost)) {

                            Bit64Operator::assignValue(lxLarge[y], cost);

                        }

                        //lx_large[x] = std::max(lx_large[x], newdist[x][y]);

                    }

                }

            }


            void HungarianAlg::updateLabelsLarge() {

                int x, y;

                uint64 delta[DATA_64WORDS_SIZE];

                Bit64Operator::setMax(delta);  //init delta as infinity

                for (y = 0; y < nLarge; y++) { //calculate delta using slack

                    if (!TLarge[y]) {

                        if (Bit64Operator::isLessThan(slackLarge[y], delta)) {

                            Bit64Operator::assignValue(delta, slackLarge[y]);

                            //delta = std::min(delta, slackLarge[y]);

                        }

                    }

                }


                for (x = 0; x < nLarge; x++) { //updating X labels

                    if (SLarge[x]) {

                        Bit64Operator::subtractValue(lxLarge[x], delta);

                        //lxLarge[x] -= delta;

                    }

                }


                for (int y = 0; y < nLarge; y++) { //updating Y labels

                    if (TLarge[y]) {

                        Bit64Operator::addValue(lyLarge[y], delta);

                        //lyLarge[y] += delta;

                    }

                }


                for (int y = 0; y < nLarge; y++) { //updating slack array

                    if (!TLarge[y]) {

                        Bit64Operator::subtractValue(slackLarge[y], delta);

                        //slackLarge[y] -= delta;

                    }

                }

            }


            void HungarianAlg::addToTreeLarge(int x, int prevX) {

                //x - current vertex, prevx - vertex from X before x in the alternating path,

                //so we can add edges (prevx, xyLarge[x]), (xyLarge[x],x)

                SLarge[x] = true; //add x to S

                prevLarge[x] = prevX; //we need this when augmenting

                for (int y = 0; y < nLarge; y++) { //update slacks, because we add new vertex to S

                    uint64 temp[DATA_64WORDS_SIZE];

                    Bit64Operator::assignValue(temp, lxLarge[x]);

                    Bit64Operator::addValue(temp, lyLarge[y]);

                    uint64 cost[DATA_64WORDS_SIZE];

                    Bit64Operator::setBit(cost, newDist[x][y]);

                    Bit64Operator::subtractValue(temp, cost);

                    if (Bit64Operator::isLessThan(temp, slackLarge[y]))

                        //if (lxLarge[x] + lyLarge[y] - newDist[x][y] < slackLarge[y]

                    {

                        Bit64Operator::assignValue(slackLarge[y], temp);

                        //slackLarge[y] = lxLarge[x] + lyLarge[y] - newDist[x][y];

                        slackXLarge[y] = x;

                    }

                }

            }


            bool HungarianAlg::augmentLarge() { //"main" function of the algorithm

                if (maxMatchLarge == nLarge) return true; //check wether matching is already perfect

                int x, y, root; //just counters and root vertex

                int q[PLAYERS], writePos = 0, readPos = 0; //q - queue for bfs, wr, rd - write and read pos in queue


                memset(SLarge, false, sizeof(SLarge)); //init set S

                memset(TLarge, false, sizeof(TLarge)); //init set T

                memset(prevLarge, -1, sizeof(prevLarge)); //init set prev - for the alternating tree

                for (x = 0; x < nLarge; x++) { //finding root of the tree

                    if (xyLarge[x] == -1) {

                        q[writePos++] = root = x;

                        prevLarge[x] = -2;

                        SLarge[x] = true;

                        break;

                    }

                }


                for (y = 0; y < nLarge; y++) { //initializing slack array

                    //slackLarge[y] = lxLarge[root] + lyLarge[y] - newDist[root][y];

                    Bit64Operator::assignValue(slackLarge[y], lxLarge[root]);

                    Bit64Operator::addValue(slackLarge[y], lyLarge[y]);

                    uint64 cost[DATA_64WORDS_SIZE];

                    Bit64Operator::setBit(cost, newDist[root][y]);

                    Bit64Operator::subtractValue(slackLarge[y], cost);

                    slackXLarge[y] = root;

                }


                //second part of augment_large() function

                while (true) {                                             //main cycle

                    while (readPos < writePos) {                           //building tree with bfs cycle

                        x = q[readPos++];

                        for (y = 0; y < nLarge; y++) {                     //current vertex from X part

                            uint64 temp[

                                    DATA_64WORDS_SIZE];                    //iterate through all edges in equality graph

                            Bit64Operator::assignValue(temp, lxLarge[x]);

                            Bit64Operator::addValue(temp, lyLarge[y]);

                            uint64 cost[DATA_64WORDS_SIZE];

                            Bit64Operator::setBit(cost, newDist[x][y]);

                            if (Bit64Operator::isEqual(cost, temp) && !TLarge[y]) {

                                //  if (newDist[x][y] == lxLarge[x] + lyLarge[y] && !TLarge[y]

                                if (yxLarge[y] == -1) break;                        //an exposed vertex in Y found, so

                                //augmenting path exists!

                                //else just add y to T

                                TLarge[y] = true;                                    //add vertex yxLarge[y], which is matched

                                q[writePos++] = yxLarge[y];                       //with y, to the queue

                                addToTreeLarge(yxLarge[y],

                                               x);                            //add edges (x,y) and (y, yxLarge[y]) to the tree

                            }

                        }

                        if (y < nLarge) break; //augmenting path found!

                    }

                    if (y < nLarge) break; //augmenting path found!


                    updateLabelsLarge();

                    writePos = readPos = 0;

                    for (y = 0; y < nLarge; y++) {

                        //in this cycle we add edges that were added to the equality graph as a

                        //result of improving the labeling, we add edge (slackx[y], y) to the tree if

                        //and only if !T[y] &&  slack[y] == 0, also with this edge we add another one

                        //(y, yx_large[y]) or augment the matching, if y was exposed


                        if (!TLarge[y] && Bit64Operator::isZero(slackLarge[y])) {

                            //if (!TLarge[y] && slackLarge[y] == 0)

                            if (yxLarge[y] ==

                                -1) {                             //exposed vertex in Y found - augmenting path exists!

                                x = slackXLarge[y];

                                break;

                            } else {

                                TLarge[y] = true;                            //else just add y to T

                                if (!SLarge[yxLarge[y]]) {

                                    q[writePos++] = yxLarge[y];              //add vertex yxLarge[y], which is matched with y, to the queue

                                    addToTreeLarge(yxLarge[y],

                                                   slackXLarge[y]); //and add edges (x,y) and (y, yxLarge[y]) to the tree.

                                }

                            }

                        }


                    }

                    if (y < nLarge) break; //augmenting path found!


                }

                if (y < nLarge) {            //we found augmenting path!

                    maxMatchLarge++;          //increment matching

                    //in this cycle we inverse edges along augmenting path

                    for (int cx = x, cy = y, ty; cx != -2; cx = prevLarge[cx], cy = ty) {

                        ty = xyLarge[cx];

                        yxLarge[cy] = cx;

                        xyLarge[cx] = cy;

                    }

                    return false;

                    //augment_large()

                }

                return true;

                //end of augment_large() function

            }


            void HungarianAlg::hungarianLarge() {

                //int ret = 0              //weight of optimal matching


                maxMatchLarge = 0;          //number of vertices in current matching

                memset(xyLarge, -1, sizeof(xyLarge));

                memset(yxLarge, -1, sizeof(yxLarge));


                initLabelsLarge();       //step 0

                while (!augmentLarge()) { ; }  //steps 1-3

                //for (int x = 0; x < n; x++)       //forming answer there

                //  ret += newDist[x][xyLarge[x]];

                //return ret;

            }

        }

    }

}
