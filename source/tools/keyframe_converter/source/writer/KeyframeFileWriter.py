# -*- coding: utf-8 -*-


from abc import ABC, abstractmethod


class KeyframeFileWriter(ABC):

    def __init__(self):
        super(KeyframeFileWriter, self).__init__()

    @abstractmethod
    def writeToFile(self, outputFile, keyframeMovement):
        print("Do nothing")