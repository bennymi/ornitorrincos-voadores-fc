//
// Created by mmaximo on 6/29/16.
//

#include "decision_making/behavior/BeamBehavior.h"

namespace decision_making {
    namespace behavior {

        BeamBehavior::BeamBehavior(BehaviorFactory *factory) : Behavior(factory) {
        }

        void BeamBehavior::behave(modeling::Modeling &modeling) {
            int uniformNumber = modeling.getAgentModel().getAgentNumber();
            representations::PlayMode::PLAY_MODE playMode = modeling.getWorldModel().getPlayMode();
            itandroids_lib::math::Pose2D staticPosition;

            switch (uniformNumber) {
                case 1:
                    staticPosition.translation.x = -14.9;
                    staticPosition.translation.y = 0.0;
                    break;
                case 2:
                    staticPosition.translation.x = -14.0;
                    staticPosition.translation.y = -4.0;
                    break;
                case 3:
                    staticPosition.translation.x = -14.0;
                    staticPosition.translation.y = 4.0;
                    break;
                case 4:
                    staticPosition.translation.x = -11.0;
                    staticPosition.translation.y = -4.0;
                    break;
                case 5:
                    staticPosition.translation.x = -11.0;
                    staticPosition.translation.y = 4.0;
                    break;
                case 6:
                    staticPosition.translation.x = -9.0;
                    staticPosition.translation.y = -4.0;
                    break;
                case 7:
                    staticPosition.translation.x = -9.0;
                    staticPosition.translation.y = 4.0;
                    break;
                case 8:
                    staticPosition.translation.x = -7.0;
                    staticPosition.translation.y = -4.0;
                    break;
                case 9:
                    staticPosition.translation.x = -7.0;
                    staticPosition.translation.y = 4.0;
                    break;
                case 11:
                    staticPosition.translation.x = -4.0;
                    staticPosition.translation.y = -0.0;
                    break;
                case 10:
                    if (playMode == representations::PlayMode::PLAY_MODE::OWN_GOAL) {
                        staticPosition.translation.x = -2.5;//-0.3;
                        staticPosition.translation.y = 0.0;
                    } else {
                        staticPosition.translation.x = -0.5;//-0.3;
                        staticPosition.translation.y = 0.0;
                    }

                    break;
            }

            beamRequest = std::make_shared<itandroids_lib::math::Pose2D>(staticPosition);
        }


        std::shared_ptr<itandroids_lib::math::Pose2D> BeamBehavior::getBeamRequest() {
            return beamRequest;
        }

    } /* namespace behavior */
} /* namespace decision_making */