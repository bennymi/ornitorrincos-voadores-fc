//
// Created by dicksiano on 9/2/16.
//


#include "base/BaseAgentTest.h"

class PlayModeIdentification_AgentTest : public BaseAgentTest {

public:

    virtual void testLoop(int nSteps) { BaseAgentTest::testLoop(nSteps); }


    virtual void setup() {
        wiz.setPlayMode(representations::RawPlayMode::FREE_KICK_LEFT);
    }

    virtual void runStep() {
        utils::roboviz::Roboviz roboviz1;
        std::string name1("animated");
        std::string buff1("animated");
        roboviz1.swapBuffers(&buff1);

        Pose2D ballPosition = modeling.getWorldModel().getBall().getPosition();

        roboviz1.drawLine(ballPosition.translation.x, ballPosition.translation.y, 0, ballPosition.translation.x,
                          ballPosition.translation.y, 0, 1, 20, 20, 20, &name1);

        Pose2D selfPosition = modeling.getWorldModel().getSelf().getPosition();
        roboviz1.drawLine(selfPosition.translation.x, selfPosition.translation.y, 0,
                          selfPosition.translation.x, selfPosition.translation.y, 1,
                          1, 20, 20, 20, &name1);

        if (!decisionMaking.decideFall(modeling)) {
            auto playMode = modeling.getWorldModel().getPlayMode(); // Check the current Play Mode.
            int uniformNumber = modeling.getAgentModel().getAgentNumber(); // Gets the agent's number.

            switch (playMode) {
                case representations::PlayMode::PLAY_MODE::PLAY_ON: {
                    std::cout << "PLAY_ON" << std::endl;
                    break;
                }

                case representations::PlayMode::PLAY_MODE::OWN_KICK_OFF: {
                    std::cout << "OWN_KICK_OFF" << std::endl;
                    break;
                }

                case representations::PlayMode::PLAY_MODE::OWN_KICK_IN: {
                    std::cout << "OWN_KICK_IN" << std::endl;
                    break;
                }
                case representations::PlayMode::PLAY_MODE::OWN_CORNER_KICK: {
                    std::cout << "OWN_CORNER_KICK" << std::endl;
                    break;
                }
                case representations::PlayMode::PLAY_MODE::OWN_FREE_KICK: {
                    std::cout << "OWN_FREE_KICK" << std::endl;
                    break;
                }
                case representations::PlayMode::PLAY_MODE::OWN_OFFSIDE: {
                    std::cout << "OWN_OFFSIDE" << std::endl;
                    break;
                }

                case representations::PlayMode::PLAY_MODE::OWN_GOAL_KICK: {
                    std::cout << "OWN_GOAL_KICK" << std::endl;
                    break;
                }

                case representations::PlayMode::PLAY_MODE::OPPONENT_KICK_OFF: {
                    std::cout << "OPPONENT_KICK_OFF" << std::endl;
                    break;
                }

                case representations::PlayMode::PLAY_MODE::OPPONENT_KICK_IN: {
                    std::cout << "OPPONENT_KICK_IN" << std::endl;
                    break;
                }
                case representations::PlayMode::PLAY_MODE::OPPONENT_CORNER_KICK: {
                    std::cout << "OPPONENT_CORNER_KICK" << std::endl;
                    break;
                }
                case representations::PlayMode::PLAY_MODE::OPPONENT_GOAL_KICK: {
                    std::cout << "OPPONENT_GOAL_KICK" << std::endl;
                    break;
                }
                case representations::PlayMode::PLAY_MODE::OPPONENT_OFFSIDE: {
                    std::cout << "OPPONENT_OFFSIDE" << std::endl;
                    break;
                }
                case representations::PlayMode::PLAY_MODE::OPPONENT_FREE_KICK: {
                    std::cout << "OPPONENT_FREE_KICK" << std::endl;
                    break;
                }

                case representations::PlayMode::PLAY_MODE::BEFORE_KICK_OFF: {
                    std::cout << "BEFORE_KICK_OFF" << std::endl;
                    break;
                }

                case representations::PlayMode::PLAY_MODE::GAME_OVER: {
                    std::cout << "GAME_OVER" << std::endl;
                    break;
                }

                case representations::PlayMode::PLAY_MODE::OWN_GOAL: {
                    std::cout << "OWN_GOAL" << std::endl;
                    break;
                }
                case representations::PlayMode::PLAY_MODE::OPPONENT_GOAL: {
                    std::cout << "OPPONENT_GOA" << std::endl;
                    break;
                }

                case representations::PlayMode::PLAY_MODE::NONE: {
                    std::cout << "BUG! NONE Set Play" << std::endl;
                    break;
                }
            }

            behaviorFactory.getSetPlay().behave(modeling); // Behavior!
            decisionMaking.movementRequest = behaviorFactory.getSetPlay().getMovementRequest();
            decisionMaking.beamRequest = behaviorFactory.getSetPlay().getBeamRequest();

            behaviorFactory.getActiveVision().behave(modeling); // Vision!
            decisionMaking.lookRequest = behaviorFactory.getActiveVision().getLookRequest();
        }
    }
};

int main() {

    /**
     * St   art the external agent
     */
    std::vector<std::thread> threads;
    for (int i = 2; i < 2; i++) {
        std::stringstream stream;
        stream << "./DeadBall_AgentTest 127.0.0.1 3100 3200 " << i << " &";
        system(stream.str().c_str());
        stream.str("");
        stream << "sleep 5";
        system(stream.str().c_str());
    }

    PlayModeIdentification_AgentTest agent;
    agent.testLoop(50000);
    return 0;

}