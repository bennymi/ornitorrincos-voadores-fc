//
// Created by muzio on 17/05/16.
//

#ifndef SOURCE_MODELING_STATE_ESTIMATION_LOCALIZATIONREPRESENTATIONS_H_
#define SOURCE_MODELING_STATE_ESTIMATION_LOCALIZATIONREPRESENTATIONS_H_

#include "math/Vector2.h"
#include "representations/FlagType.h"
#include "representations/GoalPostType.h"

namespace modeling {
    namespace state_estimation {

        struct LandmarkObservation {
            double distance;
            double bearing;
            itandroids_lib::math::Vector2<double> knownPosition;
            representations::FlagType::FLAG_TYPE flagType;
            representations::GoalPostType::GOAL_POST_TYPE goalPostType;

            bool isFlag();

            bool onUpperBorder();

            bool onLeftBorder();

            static bool landmarksOnSameBorder(LandmarkObservation &l1, LandmarkObservation &l2);
        };

    }
}

#endif //SOURCE_MODELING_STATE_ESTIMATION_LOCALIZATIONREPRESENTATIONS_H_
