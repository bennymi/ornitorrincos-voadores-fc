//
// Created by francisco on 18/12/16.
//

#ifndef ITANDROIDS_SOCCER3D_CPP_NODECREATOR_H
#define ITANDROIDS_SOCCER3D_CPP_NODECREATOR_H

#include "utils/patterns/Parameter.h"
#include "utils/patterns/BranchParameter.h"
#include "utils/patterns/LeafParameter.h"
#include <boost/property_tree/json_parser.hpp>
#include <memory>
using itandroids_lib::utils::patterns::Parameter;
using itandroids_lib::utils::patterns::LeafParameter;
using itandroids_lib::utils::patterns::BranchParameter;
namespace pt = boost::property_tree;
namespace utils{
namespace configure_tree{

    template<class T>
    class NodeCreator {
    public:

        template<class Content, class Key>
        std::shared_ptr<Parameter<Content,Key>> generateNode(){
            return (static_cast<T*>(this))->generateNode();
        }

    private:

    };
}
}



#endif //ITANDROIDS_SOCCER3D_CPP_NODECREATOR_H
