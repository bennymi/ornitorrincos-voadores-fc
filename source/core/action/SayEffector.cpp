/*
 * SayEffector.cpp
 *
 *  Created on: Aug 8, 2015
 *      Author: mmaximo
 */

#include "action/SayEffector.h"

#include <sstream>

namespace action {

    SayEffector::SayEffector(const std::string &message) :
            message(message) {

    }

    SayEffector::~SayEffector() {
    }

    std::string SayEffector::toString() {

        // Gets the the agent's message as server message
        std::stringstream stream;
        stream << "(say ";
        stream << message;
        stream << ")";
        return stream.str();
    }

} /* namespace action */
