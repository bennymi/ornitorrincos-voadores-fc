//
// Created by francisco on 19/12/16.
//

#include "WalkNodeCreator.h"

namespace utils{
namespace configure_tree {
namespace control {
    template<>
    std::shared_ptr<Parameter<pt::ptree,std::string>> WalkNodeCreator::generateNode(){
        auto walkNode = std::make_shared<BranchParameter<pt::ptree, std::string>>();
        auto normalWalkNode = std::make_shared<LeafParameter<>>();
        auto preciseWalkNode = std::make_shared<LeafParameter<>>();
        boost::property_tree::read_json("../configuration/core/control/walk/normal-parameters.json",
                                        normalWalkNode->getContent());
        boost::property_tree::read_json("../configuration/core/control/walk/precise-parameters.json",
                                        preciseWalkNode->getContent());
        walkNode->addChild("normal", normalWalkNode);
        walkNode->addChild("precise", preciseWalkNode);

        return walkNode;
    };
}
}
}