/*
 * KeyframeMotionStep.cpp
 *
 *  Created on: Oct 6, 2015
 *      Author: itandroids
 */

#include "KeyframeStep.h"

namespace control {
    namespace keyframe {


        KeyframeStep::KeyframeStep() {
            duration = 0.0f;
        }

        KeyframeStep::KeyframeStep(std::vector<double> joints, double duration) :
                duration(duration), naoJoints(joints) {
        }

        KeyframeStep::~KeyframeStep() {
        }

        double &KeyframeStep::getDuration() {
            return duration;
        }

        representations::NaoJoints &KeyframeStep::getJoints() {
            return naoJoints;
        }
    } /* namespace keyframe */
} /* namespace control */
