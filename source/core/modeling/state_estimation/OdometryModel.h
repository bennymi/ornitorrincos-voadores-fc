//
// Created by muzio on 17/05/16.
//

#ifndef SOURCE_MODELING_STATE_ESTIMATION_ODOMETRYMODEL_H_
#define SOURCE_MODELING_STATE_ESTIMATION_ODOMETRYMODEL_H_

#include <boost/random/normal_distribution.hpp>
#include <boost/random.hpp>
#include "Particle.h"

namespace modeling {
    namespace state_estimation {

        class OdometryModel {
        public:
            OdometryModel(double odometryXSigma, double odometryYSigma, double odometryPsiSigma);

            ~OdometryModel();

            void propagate(Particle *particles, int numParticles, const Pose2D &odometry);

            Pose2D odometryModel(const Pose2D &odometry);

        private:
            boost::variate_generator<boost::mt19937 &, boost::normal_distribution<> > odometryXDistribution;
            boost::variate_generator<boost::mt19937 &, boost::normal_distribution<> > odometryYDistribution;
            boost::variate_generator<boost::mt19937 &, boost::normal_distribution<> > odometryPsiDistribution;

            double alpha;
            double beta;
            double gama;
        };

    }
}

#endif //SOURCE_MODELING_STATE_ESTIMATION_ODOMETRYMODEL_H_
