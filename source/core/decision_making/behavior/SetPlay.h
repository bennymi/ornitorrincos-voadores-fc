//
// Created by dicksiano on 8/14/16.
//

#ifndef ITANDROIDS_SOCCER3D_CPP_SETPLAY_H
#define ITANDROIDS_SOCCER3D_CPP_SETPLAY_H

#include "Behavior.h"
#include "decision_making/positioning/DelaunayPositioning.h"
#include <core/decision_making/positioning/BasicDefensivePositioning.h>

namespace decision_making {
    namespace behavior {
/**
 * Class that represents the Set Plays.
 */
        class SetPlay : public Behavior {
        public:
            /**
             * Default constructor.
             */
            SetPlay();

            /**
             * Assignment constructor.
             *
             * @param pointer to BehaviorFactory.
             */
            SetPlay(BehaviorFactory *behaviorFactory);

            /**
             * Destructor.
             */
            virtual ~SetPlay();

            /**
             * This method chooses an specific set play according to the Play Mode.
             *
             * @param modeling.
             */
            void behave(modeling::Modeling &modeling);

            /**
             * This method returns the beam request.
             *
             * @return beam request.
             */
            std::shared_ptr<itandroids_lib::math::Pose2D> getBeamRequest();

        protected:
            /*
             * These methods can be used by the derived Set Plays.
             */
            void goToDelaunayPositioning(modeling::Modeling &modeling);

            void goToBarrierPositioning(modeling::Modeling &modeling);

            void goToTheirKickOffPositioning(modeling::Modeling &modeling);

            Pose2D calculateReceiverPositioning(modeling::Modeling &modeling);

            void goToReceiverPositioning(modeling::Modeling &modeling);

        private:
            /*
             * All derived set plays can modify beamRequest.
             */
            std::shared_ptr<itandroids_lib::math::Pose2D> beamRequest;

            representations::PlayMode::PLAY_MODE playMode;
            representations::PlaySide::PLAY_SIDE ownPlaySide;
        };

    } /* namespace behavior */
} /* namespace decision_making */




#endif //ITANDROIDS_SOCCER3D_CPP_SETPLAY_H
