//
// Created by francisco on 16/05/16.
//

#include "LookBehavior.h"

namespace decision_making {
    namespace behavior {
        LookBehavior::LookBehavior(BehaviorFactory *factory) : Behavior(factory) {

        }

        const std::shared_ptr<control::LookRequest> &LookBehavior::getLookRequest() {
            return lookRequest;
        }


    }
}