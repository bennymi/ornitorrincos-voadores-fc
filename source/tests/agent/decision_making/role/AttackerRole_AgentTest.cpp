//
// Created by francisco on 6/2/16.
//

#include "AttackerRole_AgentTest.h"

void AttackerRole_AgentTest::setup() {
}

void AttackerRole_AgentTest::runStep() {
    wiz.setPlayMode(representations::RawPlayMode::KICK_OFF_LEFT);
    BaseFullAgentTest::runStep();
}

int main() {
    AttackerRole_AgentTest player("127.0.0.1", 3100, 3200, 8, 0, std::string("ITAndroids"));
    player.testLoop(10000);
    return 0;
}
