/*
 * GlobalTimePerceptor.cpp
 *
 *  Created on: Aug 30, 2015
 *      Author: itandroids
 */

#include "GlobalTimePerceptor.h"

namespace perception {

    /*
     * This constructor set global time (to zero) without parser tree node information.
     */
    GlobalTimePerceptor::GlobalTimePerceptor() {
        globalTime = 0;
    }

    /*
     * This constructor set global time with parser tree node information
     */

    GlobalTimePerceptor::GlobalTimePerceptor(utils::parser::ParserTreeNode &node) :
            Perceptor(node) {
        std::string value = node.children[0]->content;
        globalTime = boost::lexical_cast<double>(
                utils::parser::KeyValuePair(value, ' ').value);
    }

    GlobalTimePerceptor::~GlobalTimePerceptor() {
    }

    /*
     * Getter method: return global time perceptor variables (global time)
     */
    double GlobalTimePerceptor::getGlobalTime() {
        return globalTime;
    }

} /* namespace perception */
