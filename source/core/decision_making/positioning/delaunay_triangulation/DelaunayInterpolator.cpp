/*
 * DelaunayInterpolator.cpp
 *
 *  Created on: Oct 28, 2015
 *      Author: itandroids
 */

#include "DelaunayInterpolator.h"

namespace decision_making {
    namespace positioning {
        namespace delaunay_triangulation {

            DelaunayInterpolator::DelaunayInterpolator(
                    std::unique_ptr<DelaunayTriangulation> &&triangulation,
                    std::vector<std::vector<itandroids_lib::geometry::Vector2D> > predefinedPositions) :
                    triangulation(std::move(triangulation)), predefinedPositions(predefinedPositions) {

            }

            DelaunayInterpolator::~DelaunayInterpolator() {
            }

            std::vector<itandroids_lib::math::Vector2<double> > DelaunayInterpolator::interpolate(
                    itandroids_lib::math::Vector2<double> ballPos) {
                itandroids_lib::geometry::Vector2D ballPosition =
                        itandroids_lib::geometry::Vector2D(ballPos.x, ballPos.y);
                if (ballPosition.y > 10.0)
                    ballPosition.y = 10.0;
                if (ballPosition.y < -10.0)
                    ballPosition.y = -10.0;
                if (ballPosition.x > 15.0)
                    ballPosition.x = 15.0;
                if (ballPosition.x < -15.0)
                    ballPosition.x = -15;
                itandroids_lib::geometry::Triangle *triangleContains =
                        triangulation->findTriangleContains(ballPosition);
                if (triangleContains != NULL) {
                    double a = (triangleContains->getVertex(0)->pos().y - ballPosition.y)
                               / (triangleContains->getVertex(0)->pos().x - ballPosition.x);
                    double b = triangleContains->getVertex(0)->pos().y
                               - a * (triangleContains->getVertex(0)->pos().x);
                    double c = (triangleContains->getVertex(1)->pos().y
                                - triangleContains->getVertex(2)->pos().y)
                               / (triangleContains->getVertex(1)->pos().x
                                  - triangleContains->getVertex(2)->pos().x);
                    //std::cout << triangleContains->getVertex(1)->pos().x <<" "<< triangleContains->getVertex(2)->pos().x<< "\n";
                    // c dá inf quando os vertices 1 e 2 tem o mesmo x;
                    double d = triangleContains->getVertex(1)->pos().y
                               - c * (triangleContains->getVertex(1)->pos().x);

                    itandroids_lib::geometry::Vector2D posI;

                    posI.x = (d - b) / (a - c);
                    posI.y = (a * d - b * c) / (a - c);
                    //std::cout << "PosI" << " " << posI.x << " " << posI.y<< "\n";
                    //std::cout<< a << b << c << d << "\n";
                    //a e b dão 0, c e d inf
                    // se a reta 1 -> 2 for vertical e a reta P(bola) -> 0 não for horizontal.
                    if (triangleContains->getVertex(1)->pos().x
                        - triangleContains->getVertex(2)->pos().x == 0 && ((ballPosition.y
                                                                            -
                                                                            triangleContains->getVertex(0)->pos().y) !=
                                                                           0)) {
                        posI.x = triangleContains->getVertex(1)->pos().x;
                        posI.y = (triangleContains->getVertex(1)->pos().x - triangleContains->getVertex(0)->pos().x) *
                                 (ballPosition.y - triangleContains->getVertex(0)->pos().y) /
                                 (ballPosition.x - triangleContains->getVertex(0)->pos().x);
                    }
                    // se a reta 1 -> 2 for vertical e a reta P(bola) -> 0 for horizontal.
                    if (triangleContains->getVertex(1)->pos().x
                        - triangleContains->getVertex(2)->pos().x == 0 && ((ballPosition.y
                                                                            -
                                                                            triangleContains->getVertex(0)->pos().y) ==
                                                                           0)) {
                        posI.x = triangleContains->getVertex(1)->pos().x;
                        posI.y = triangleContains->getVertex(0)->pos().y;
                    }
                    // @todo -> reta P->0 vertical.
                    double m1 = posI.dist(triangleContains->getVertex(1)->pos());
                    double n1 = posI.dist(triangleContains->getVertex(2)->pos());
                    double m2 = ballPosition.dist(triangleContains->getVertex(0)->pos());
                    double n2 = ballPosition.dist(posI);

                    //std::cout<< m1 << n1 << m2 << n2 << "\n";
                    //m1, n1 e n2 estão dando -nan. m2 está dando 0.
                    std::vector<itandroids_lib::math::Vector2<double> > posIResults(
                            modeling::WorldModel::NUM_PLAYERS_PER_TEAM);
                    std::vector<itandroids_lib::math::Vector2<double> > results(
                            modeling::WorldModel::NUM_PLAYERS_PER_TEAM);
                    for (int i = 0; i < modeling::WorldModel::NUM_PLAYERS_PER_TEAM; i++) {
                        //predefinedPositions[triangleContains->getVertex(1)->getId()][i] está OK
                        //predefinedPositions[triangleContains->getVertex(2)->getId()][i] está OK
                        //m1 e n1 estão dando -nan
                        //std::cout << "prePosi2 "<<i<<" - " << predefinedPositions[triangleContains->getVertex(2)->getId()][i].x << " " << predefinedPositions[triangleContains->getVertex(2)->getId()][i].y << "\n";
                        //std::cout << m1 << " "<<n1<< "\n\n";
                        posIResults[i].x =
                                predefinedPositions[triangleContains->getVertex(1)->getId()][i].x
                                + (predefinedPositions[triangleContains->getVertex(
                                        2)->getId()][i].x
                                   - predefinedPositions[triangleContains->getVertex(
                                        1)->getId()][i].x)
                                  * ((m1) / (m1 + n1));
                        posIResults[i].y =
                                predefinedPositions[triangleContains->getVertex(1)->getId()][i].y
                                + (predefinedPositions[triangleContains->getVertex(
                                        2)->getId()][i].y
                                   - predefinedPositions[triangleContains->getVertex(
                                        1)->getId()][i].y)
                                  * ((m1) / (m1 + n1));
                    }
                    for (int i = 0; i < modeling::WorldModel::NUM_PLAYERS_PER_TEAM; i++) {
                        //posIResults tá dando -nan
                        //std::cout << "posIResults: "<< posIResults[i].x << " " << posIResults[i].y << "\n";

                        results[i].x =
                                predefinedPositions[triangleContains->getVertex(0)->getId()][i].x
                                + (posIResults[i].x
                                   - predefinedPositions[triangleContains->getVertex(
                                        0)->getId()][i].x)
                                  * ((m2) / (m2 + n2));

                        results[i].y =
                                predefinedPositions[triangleContains->getVertex(0)->getId()][i].y
                                + (posIResults[i].y
                                   - predefinedPositions[triangleContains->getVertex(
                                        0)->getId()][i].y)
                                  * ((m2) / (m2 + n2));
                        //std::cout << "result: "<< results[i].x << " " << results[i].y << "\n";

                        if (results[i].y > 10.0)
                            results[i].y = 10.0;
                        if (results[i].y < -10.0)
                            results[i].y = -10.0;
                        if (results[i].x > 15.0)
                            results[i].x = 15.0;
                        if (results[i].x < -15.0)
                            results[i].x = -15;

                    }
                    return results;
                }
                return std::vector<itandroids_lib::math::Vector2<double> >(modeling::WorldModel::NUM_PLAYERS_PER_TEAM);
            }

        } /* namespace delaunay_triangulation */
    } /* namespace positioning */
} /* namespace decision_making */
