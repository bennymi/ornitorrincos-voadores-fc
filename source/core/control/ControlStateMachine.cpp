//
// Created by francisco on 26/11/16.
//




#include "perception/Perception.h"
#include "control/MovementRequestType.h"
#include "control/walking/OmnidirectionalWalkZMP.h"
#include "control/kick/KickZMP.h"
#include "control/keyframe/KeyframePlayer.h"
#include "representations/RobotType.h"
#include "control/KeyframeRequest.h"
#include "control/WalkRequest.h"
#include "ControlStateMachine.h"

namespace control {

    ControlStateMachine::ControlStateMachine(RobotType::ROBOT_TYPE robotType) : previousGlobalTime(0.0), keyframePlayer(
            ::representations::ITAndroidsConstants::KEYFRAME_FOLDER), smoothing(true), perception(nullptr) {
        walkParams =
                walking::OmnidirectionalWalkZMPParams::getOmnidirectionalWalkZMPParams();
        kickParams = kick::KickZMPParams::getDefaultKickZMPParams();
        walking::RobotPhysicalParameters robotPhysicalParameters =
                walking::RobotPhysicalParameters::getNaoPhysicalParameters();
        kickZmp = new kick::KickZMP(kickParams, false, robotPhysicalParameters,
                                    new walking::InverseKinematics(robotPhysicalParameters));
        walk = new walking::OmnidirectionalWalkZMP(walkParams,
                                                   robotPhysicalParameters,
                                                   new walking::InverseKinematics(robotPhysicalParameters));

        keyframePlayer.update();
    }

    void ControlStateMachine::control(perception::Perception &perception) {

        this->perception = &perception;
        process_event(ControlMovementEvent());
        previousGlobalTime = perception.getAgentPerception().getGlobalTime();
        previousJointsPositions = perception.getAgentPerception().getNaoJoints();
    }

    void ControlStateMachine::smooth(NaoJoints &currentPositions,
                                     NaoJoints &finalPositions) {
        smoother.set(finalPositions);
        jointsPositions = smoother.smooth(currentPositions);
        if ((jointsPositions - currentPositions).lInfiniteNorm()
            < 0.1 * M_PI / 180.0)
            smoothing = false;
    }

    void ControlStateMachine::setMovementRequest(std::shared_ptr<MovementRequest> movementRequest) {
        this->movementRequest = movementRequest;
    }


    IdleState::IdleState(my_context ctx) : state(ctx) {

    }

    void IdleState::control(perception::Perception &perception) {
        //Do nothing if still in idle state
    }

    sc::result IdleState::react(const SetMovementEvent &evt) {
        auto movementRequest = evt.getMovementRequest();

        // we put the request on state machine
        outermost_context().movementRequest = movementRequest;
        // then we change to a new state
        switch (movementRequest->getMovementType()) {
            case (MovementRequestType::WALK_REQUEST):
                return transit<WalkMovementState>();
            case (MovementRequestType::KICK_REQUEST):
                return transit<KickMovementState>();
            case (MovementRequestType::KEYFRAME_REQUEST):
                return transit<KeyframeMovementState>();
        }


    }

    sc::result IdleState::react(const ControlMovementEvent &evt) {
        control(*outermost_context().perception);
        return discard_event();
    }


    KeyframeMovementState::KeyframeMovementState(my_context ctx) : state(ctx) {

    }

    void KeyframeMovementState::control(perception::Perception &perception) {
//        control the keyframe
        auto &keyframePlayer = outermost_context().keyframePlayer;

        if (isFirstTime) {
            keyframeRequest = std::dynamic_pointer_cast<KeyframeRequest>(outermost_context().movementRequest);
            outermost_context().keyframePlayer.setPage(keyframeRequest->getName());
            isFirstTime = false;
        }

        outermost_context().jointsPositions = keyframePlayer.getDesiredJoints(
                outermost_context().perception->getAgentPerception().getNaoJoints(),
                outermost_context().perception->getAgentPerception().getGlobalTime(), keyframeRequest->isInverted());
        if (hasFinished()) {
            outermost_context().keyframePlayer.getCurrentPage().reset();
            post_event(CurrentMovementEndedEvent());
        }
    }

    bool KeyframeMovementState::hasFinished() {
        return outermost_context().keyframePlayer.getCurrentPage().hasFinished();
    }

    sc::result KeyframeMovementState::react(const ControlMovementEvent &evt) {
        control(*outermost_context().perception);
        return discard_event();
    }

    WalkMovementState::WalkMovementState(my_context ctx) : state(ctx) {

    }

    void WalkMovementState::control(perception::Perception &perception) {

        if (resetMovement) {
            outermost_context().walk->requestStop();
//            resetMovement = false;
            outermost_context().smoothing = false;
        } else {
            walkRequest = std::dynamic_pointer_cast<WalkRequest>(outermost_context().movementRequest);
            outermost_context().walk->setDesiredVelocity(walkRequest->getVelocity());
        }


        if (walkRequest->getWalkMode() == WalkMode::PRECISE) {
            outermost_context().walk->setWalkParams(OmnidirectionalWalkZMPParams::getOmnidirectionalWalkZMPParams(
                    control::walking::OmnidirectionalWalkZMParamsType::PRECISE));
        } else if (walkRequest->getWalkMode() == WalkMode::NORMAL) {
            outermost_context().walk->setWalkParams(OmnidirectionalWalkZMPParams::getOmnidirectionalWalkZMPParams());
        }
        if (isFirstTime) {
//            outermost_context().walk->setWalkParams(OmnidirectionalWalkZMPParams::getOmnidirectionalWalkZMPParams());
            outermost_context().walk->restart();
            outermost_context().smoothing = true;
            isFirstTime = false;
        } else if (outermost_context().previousWalkMode != walkRequest->getWalkMode()) {
            outermost_context().walk->restart();
            outermost_context().smoothing = true;
        }
        outermost_context().previousWalkMode = walkRequest->getWalkMode();
        if (outermost_context().smoothing) {
            NaoJoints currentPositions =
                    perception.getAgentPerception().getNaoJoints()
                    * NaoJoints::getNaoDirectionsFixing();
            NaoJoints finalPositions;
            outermost_context().walk->update(0.0, finalPositions);
            outermost_context().smooth(currentPositions, finalPositions);
        } else {
            outermost_context().walk->update(
                    perception.getAgentPerception().getGlobalTime() - outermost_context().previousGlobalTime,
                    outermost_context().jointsPositions);

        }

        if (hasFinished()) {
            post_event(CurrentMovementEndedEvent());
        }
    }

    sc::result WalkMovementState::react(const SetMovementEvent &evt) {
        //set the restart flag
        outermost_context().movementRequest = evt.getMovementRequest();
        if (outermost_context().movementRequest->getMovementType() != MovementRequestType::WALK_REQUEST) {
            resetMovement = true;
        }
        return discard_event();
    }

    bool WalkMovementState::hasFinished() {

        return outermost_context().walk->hasStopped();
    }

    sc::result WalkMovementState::react(const ControlMovementEvent &evt) {
        control(*outermost_context().perception);
        return discard_event();
    }


    KickMovementState::KickMovementState(my_context ctx) : state(ctx) {

    }

    void KickMovementState::control(perception::Perception &perception) {
        //control the kick movement
        if (isFirstTime) {
            outermost_context().kickZmp->reset();
            outermost_context().smoothing = true;
        }
        if (outermost_context().smoothing) {
            std::cout << "smoothing" << std::endl;
            NaoJoints currentPositions =
                    perception.getAgentPerception().getNaoJoints()
                    * NaoJoints::getNaoDirectionsFixing();
            NaoJoints finalPositions;
            outermost_context().kickZmp->update(0.0, finalPositions);
            outermost_context().smooth(currentPositions, finalPositions);
        } else {
            outermost_context().kickZmp->update(
                    perception.getAgentPerception().getGlobalTime() - outermost_context().previousGlobalTime,
                    outermost_context().jointsPositions);
        }

        if (hasFinished()) {
            post_event(CurrentMovementEndedEvent());
        }


    }

    bool KickMovementState::hasFinished() {
        return outermost_context().kickZmp->hasKicked();
    }

    sc::result KickMovementState::react(const ControlMovementEvent &evt) {
        control(*outermost_context().perception);
        return discard_event();
    }


    NaoJoints IControlSystem::getJointsPositions() {
        return NaoJoints();
    }

    bool IControlMovementSystem::hasFinished() {
        return false;
    }

    IControlMovementSystem::IControlMovementSystem() {
        isFirstTime = true;
        resetMovement = false;
    }


}
