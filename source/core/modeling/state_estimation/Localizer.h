/*
 * Localizer.h
 *
 *  Created on: Aug 29, 2015
 *      Author: mmaximo
 */

#ifndef SOURCE_MODELING_STATE_ESTIMATION_LOCALIZER_H_
#define SOURCE_MODELING_STATE_ESTIMATION_LOCALIZER_H_

#include "Particle.h"
#include "OdometryModel.h"
#include "ObservationModel.h"
#include "Resetter.h"
#include "LocalizationRepresentations.h"
#include "geometry/Line2D.h"
#include "math/Vector3.h"
#include "math/Pose2D.h"
#include "modeling/FieldDescription.h"
#include "perception/VisibleFlag.h"
#include "perception/VisibleGoalPost.h"
#include "perception/VisibleLine.h"

#include <vector>
#include <boost/random.hpp>
#include <boost/random/normal_distribution.hpp>
#include <core/representations/NaoJoints.h>

namespace modeling {
    namespace state_estimation {

        using namespace itandroids_lib::math;
        using itandroids_lib::geometry::Line2D;
        using namespace representations;


        struct LocalizerParams {
            LocalizerParams(double distanceSigma, double bearingSigma, double odometryXSigma, double odometryYSigma,
                            double odometryPsiSigma, bool useLandmarks, bool useLines, bool useNegativeInfo);

            LocalizerParams();

            double distanceSigma;
            double bearingSigma;
            double odometryXSigma;
            double odometryYSigma;
            double odometryPsiSigma;

            bool useLandmarks;
            bool useLines;
            bool useNegativeInfo;

            static LocalizerParams getDefaultLocalizerParams();
        };

        class Localizer {
        public:
            Localizer(FieldDescription &fieldDescription, int numParticles,
                      LocalizerParams params);

            virtual ~Localizer();

            /**
             *
             * @param globalTime
             * @param visibleFlags
             * @param visibleGoalPosts
             * @param visibleLines
             * @param odometry
             */
            void update(double globalTime,
                        const std::vector<perception::VisibleFlag *> &visibleFlags,
                        const std::vector<perception::VisibleGoalPost *> &visibleGoalPosts,
                        const std::vector<perception::VisibleLine *> &visibleLines,
                        Pose2D &odometry, representations::NaoJoints &jointsPosition);

            const Pose2D &getPoseEstimate();

            Particle *getParticles();

            /**
             * Resets the particles according to a gaussian distribution centered around
             * the estimate.
             */
            void reset(Pose2D estimate, Pose2D sigmas);

        private:
            LocalizerParams params;
            OdometryModel odometryModel;
            ObservationModel observationModel;
            Resetter particleResetter;

            FieldDescription &fieldDescription;
            int numParticles;
            double slowObservationQuality;
            double fastObservationQuality;
            double alphaSlow;
            double alphaFast;
            Pose2D estimate;
            Particle *particles;
            Particle *resampledParticles;
            int *resampledIndices;
            double *landmarkObservationProbability;
            double *lineObservationProbability;

            boost::variate_generator<boost::mt19937 &, boost::uniform_real<> > uniformDistribution;

            void updateEstimate();

            /**
             * Update particles according to the movement model
             */
            void drawParticles(Particle *particles, Pose2D &odometry);

            /**
             * Update particle weights based on the observation model
             */
            void
            updateWeights(std::vector<LandmarkObservation> &landmarkObservations, std::vector<Line2D> &lineObservations,
                          representations::NaoJoints &jointsPosition);

            void resample();

            //void moveStep(std::vector<LandmarkObservation> &observations, Pose2D &odometry);

            /**
             * Reset all particle' weights equally
             */
            void resetWeights();

            /**
             * Uniformly distribute particles across the soccer field
             */
            void resetParticles();

            std::vector<LandmarkObservation> getLandmarkObservations(
                    const std::vector<perception::VisibleFlag *> &visibleFlags,
                    const std::vector<perception::VisibleGoalPost *> &visibleGoalPosts);

            LandmarkObservation getLandmarkObservation(
                    perception::VisibleFlag &flag);

            LandmarkObservation getLandmarkObservation(
                    perception::VisibleGoalPost &goalPost);

            std::vector<Line2D> getLinesObservations(const std::vector<perception::VisibleLine *> &visibleLines);
        };

    } /* namespace state_estimation */
} /* namespace modeling */

#endif /* SOURCE_MODELING_STATE_ESTIMATION_LOCALIZER_H_ */
