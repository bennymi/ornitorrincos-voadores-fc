//
// Created by jose on 11/09/16.
//

#ifndef ITANDROIDS_SOCCER3D_CPP_BASE64COMPRESSIONDATA_H
#define ITANDROIDS_SOCCER3D_CPP_BASE64COMPRESSIONDATA_H

#include <boost/algorithm/string.hpp>

namespace representations {

    class Base64CompressionData {

    public:
        std::string base64Encode(unsigned char const *bytesToEncode, unsigned int sizeString);

        std::string base64Decode(std::string const &encodedString);

    private:
        static const std::string Alphabet64;

        static inline bool isBase64(unsigned char c);
    };

}

#endif //ITANDROIDS_SOCCER3D_CPP_BASE64COMPRESSIONDATA_H
