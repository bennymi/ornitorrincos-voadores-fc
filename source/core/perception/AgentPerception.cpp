/*
 * AgentPerception.cpp
 *
 *  Created on: Aug 19, 2015
 *      Author: itandroids
 */

#include "AgentPerception.h"

namespace perception {
    /**
     * AgentPerception: This constructor initializes variables
     */
    AgentPerception::AgentPerception() {
        visionUpdated = false;
        playSide = representations::PlaySide::UNKNOWN;
        playMode = representations::RawPlayMode::BEFORE_KICK_OFF;
        temperature = 0;
        batteryPercentage = 0;
        globalTime = 0;
        uniformNumber = 0;
    }

    AgentPerception::~AgentPerception() {
    }

    /*
     * Getter Methods: Returns agent perceptor's info
     */

    int AgentPerception::getUniformNumber() {
        return uniformNumber;
    }

    double AgentPerception::getGlobalTime() {
        return globalTime;
    }


    representations::GameState AgentPerception::getGameState() {
        return gameState;
    }

    representations::PlaySide::PLAY_SIDE AgentPerception::getPlaySide() {
        return playSide;
    }

    representations::RawPlayMode::RAW_PLAY_MODE AgentPerception::getPlayMode() {
        return playMode;
    }

    representations::NaoJoints &AgentPerception::getNaoJoints() {
        return naoJoints;
    }

    double AgentPerception::getTemperature() {
        return temperature;
    }

    double AgentPerception::getBatteryPercentage() {
        return batteryPercentage;
    }


    Vector3<double> &AgentPerception::getTorsoAcceleration() {
        return torsoAcceleration;
    }

    Vector3<double> &AgentPerception::getTorsoAngularVelocities() {
        return torsoAngularVelocities;
    }

    representations::ForceResistanceData &AgentPerception::getLeftFootForceResistanceData() {
        return leftFootForceResistanceData;
    }

    representations::ForceResistanceData &AgentPerception::getRightFootForceResistanceData() {
        return rightFootForceResistanceData;
    }

    representations::HearData AgentPerception::getHearData() {
        return hearData;
    }

    std::vector<perception::VisibleObject *> &AgentPerception::getVisibleObjects() {
        return visibleObjects;
    }

    bool AgentPerception::receivedVisionUpdate() {
        return visionUpdated;
    }

    /*
     * update: This method deletes all visible objects of agent and iterates over
     * each type of perceptors, updating its information.
     */
    void AgentPerception::update(std::vector<Perceptor *> &perceptors) {
        //Delete all visible objects
        std::vector<perception::VisibleObject *>::iterator visibleObjectsIterator;
        for (visibleObjectsIterator = visibleObjects.begin();
             visibleObjectsIterator != visibleObjects.end();
             visibleObjectsIterator++) {
            delete (*visibleObjectsIterator);
        }
        visibleObjects.clear();
        visionUpdated = false;
        std::vector<Perceptor *>::iterator iter;
        std::vector<perception::HingeJointPerceptor> hingeJoints;
        //hingeJoints.reserve(representations::NaoJoints::NUM_JOINTS);

        //Iterating over all perceptors and getting information from them
        for (iter = perceptors.begin(); iter != perceptors.end(); iter++) {
            perception::PERCEPTOR_TYPE perceptorType = (**iter).getPerceptorType();
            Perceptor *basePointer = *iter;
            if (perceptorType == perception::ACCELEROMETER) {
                perception::AccelerometerPerceptor *perceptor =
                        static_cast<AccelerometerPerceptor *>(basePointer);
                torsoAcceleration = (perceptor->getAcceleration());
            } else if (perceptorType == perception::AGENT_STATE) {
                perception::AgentStatePerceptor *perceptor(
                        static_cast<AgentStatePerceptor *>(basePointer));
                batteryPercentage = perceptor->getBattery();
                temperature = perceptor->getTemperature();
            } else if (perceptorType == perception::GYRO_RATE) {
                perception::GyroRatePerceptor *perceptor =
                        static_cast<GyroRatePerceptor *>(basePointer);
                torsoAngularVelocities = perceptor->getAngularVelocity();
            } else if (perceptorType == perception::HEAR) {
                perception::HearPerceptor *perceptor =
                        static_cast<HearPerceptor *>(basePointer);
                if (perceptor->getTeam() == "ITAndroids") {
                    hearData = representations::HearData(perceptor->getTeam(), perceptor->getTime(),
                                                         perceptor->isFromSelf(), perceptor->getDirection(),
                                                         perceptor->getMessage());
                }
            } else if (perceptorType == perception::FORCE_RESISTANCE) {
                perception::ForceResistancePerceptor *perceptor =
                        (static_cast<ForceResistancePerceptor *>(basePointer));
                if (perceptor->getName().compare("lf") == 0) {
                    leftFootForceResistanceData =
                            representations::ForceResistanceData(
                                    perceptor->getOriginCoordinates(),
                                    perceptor->getForce());
                } else if (perceptor->getName().compare("rf") == 0) {
                    rightFootForceResistanceData =
                            representations::ForceResistanceData(
                                    perceptor->getOriginCoordinates(),
                                    perceptor->getForce());
                }
            } else if (perceptorType == perception::VISION) {
                visionUpdated = true;
                perception::VisionPerceptor *perceptor =
                        static_cast<perception::VisionPerceptor *>(basePointer);
                visibleObjects = perceptor->getVisibleObjects();
            } else if (perceptorType == perception::HINGE_JOINT) {
                perception::HingeJointPerceptor *perceptor =
                        static_cast<perception::HingeJointPerceptor *>(basePointer);
                hingeJoints.push_back(*perceptor);
            } else if (perceptorType == perception::UNIVERSAL_JOINT) {
                perception::UniversalJointPerceptor *perceptor =
                        static_cast<perception::UniversalJointPerceptor *>(basePointer);
            } else if (perceptorType == perception::TOUCH) {
                perception::TouchPerceptor *perceptor =
                        static_cast<perception::TouchPerceptor *>(basePointer);
            } else if (perceptorType == perception::GAME_STATE) {
                perception::GameStatePerceptor *perceptor =
                        static_cast<perception::GameStatePerceptor *>(basePointer);
                gameState = representations::GameState(perceptor->getGameTime(),
                                                       perceptor->getScoreLeft(), perceptor->getScoreRight(),
                                                       perceptor->getPlayMode());
                if (perceptor->isFirstTime()) {
                    uniformNumber = perceptor->getUniformNumber();
                    playSide = perceptor->getPlaySide();
                }
            } else if (perceptorType == perception::GLOBAL_TIME) {
                perception::GlobalTimePerceptor *perceptor =
                        static_cast<perception::GlobalTimePerceptor *>(basePointer);
                globalTime = perceptor->getGlobalTime();
            }
        }

        naoJoints.assign(hingeJoints);
        hingeJoints.clear();
    } /* namespace perception */
}
