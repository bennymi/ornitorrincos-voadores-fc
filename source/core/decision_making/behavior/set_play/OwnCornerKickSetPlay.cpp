//
// Created by dicksiano on 8/14/16.
//

#include "OwnCornerKickSetPlay.h"
#include "../BehaviorFactory.h"

namespace decision_making {
    namespace behavior {

        OwnCornerKickSetPlay::OwnCornerKickSetPlay() : SetPlay(nullptr) {
        }

        OwnCornerKickSetPlay::OwnCornerKickSetPlay(BehaviorFactory *behaviorFactory) : SetPlay(behaviorFactory) {
        }

        OwnCornerKickSetPlay::~OwnCornerKickSetPlay() {
        }

        void OwnCornerKickSetPlay::behave(modeling::Modeling &modeling) {
            /// IMPLEMENTE
        } /* function behave */

    } /* namespace behavior */
} /* namespace decision_making */