/*
 * EffectorFactory.cpp
 *
 *  Created on: Sep 30, 2015
 *      Author: mmaximo
 */

#include "action/EffectorFactory.h"

#include "action/HingeJointEffector.h"
#include "action/SayEffector.h"
#include "action/InitEffector.h"
#include "action/CreateEffector.h"
#include "action/SynchronizeEffector.h"
#include "action/BeamEffector.h"

namespace action {

    EffectorFactory::EffectorFactory() {
        jointsEffectorsNamesMap[NaoJoints::NECK_YAW] = "he1";
        jointsEffectorsNamesMap[NaoJoints::NECK_PITCH] = "he2";
        jointsEffectorsNamesMap[NaoJoints::LEFT_SHOULDER_PITCH] = "lae1";
        jointsEffectorsNamesMap[NaoJoints::LEFT_SHOULDER_YAW] = "lae2";
        jointsEffectorsNamesMap[NaoJoints::LEFT_ARM_ROLL] = "lae3";
        jointsEffectorsNamesMap[NaoJoints::LEFT_ARM_YAW] = "lae4";
        jointsEffectorsNamesMap[NaoJoints::LEFT_HIP_YAWPITCH] = "lle1";
        jointsEffectorsNamesMap[NaoJoints::LEFT_HIP_ROLL] = "lle2";
        jointsEffectorsNamesMap[NaoJoints::LEFT_HIP_PITCH] = "lle3";
        jointsEffectorsNamesMap[NaoJoints::LEFT_KNEE_PITCH] = "lle4";
        jointsEffectorsNamesMap[NaoJoints::LEFT_FOOT_PITCH] = "lle5";
        jointsEffectorsNamesMap[NaoJoints::LEFT_FOOT_ROLL] = "lle6";
        jointsEffectorsNamesMap[NaoJoints::RIGHT_SHOULDER_PITCH] = "rae1";
        jointsEffectorsNamesMap[NaoJoints::RIGHT_SHOULDER_YAW] = "rae2";
        jointsEffectorsNamesMap[NaoJoints::RIGHT_ARM_ROLL] = "rae3";
        jointsEffectorsNamesMap[NaoJoints::RIGHT_ARM_YAW] = "rae4";
        jointsEffectorsNamesMap[NaoJoints::RIGHT_HIP_YAWPITCH] = "rle1";
        jointsEffectorsNamesMap[NaoJoints::RIGHT_HIP_ROLL] = "rle2";
        jointsEffectorsNamesMap[NaoJoints::RIGHT_HIP_PITCH] = "rle3";
        jointsEffectorsNamesMap[NaoJoints::RIGHT_KNEE_PITCH] = "rle4";
        jointsEffectorsNamesMap[NaoJoints::RIGHT_FOOT_PITCH] = "rle5";
        jointsEffectorsNamesMap[NaoJoints::RIGHT_FOOT_ROLL] = "rle6";
    }

    EffectorFactory::~EffectorFactory() {
        clear();
    }

    void EffectorFactory::clear() {
        for (auto iter = effectors.begin(); iter != effectors.end(); iter++) {
            delete (*iter);
            (*iter) = nullptr;
        }
        effectors.clear(); // Clear all effectors
    }

    void EffectorFactory::addJointsEffectors(NaoJoints &naoJoints) {

        std::map<NaoJoints::HINGE_JOINTS, double> jointsMap = naoJoints.getAsMap();
        std::map<NaoJoints::HINGE_JOINTS, double>::iterator it;
        for (it = jointsMap.begin(); it != jointsMap.end(); ++it)
            effectors.push_back(
                    new HingeJointEffector(jointsEffectorsNamesMap[(*it).first],
                                           (*it).second));// Add each Joint Effector
    }

    void EffectorFactory::addSayEffector(const std::string &message) {
        effectors.push_back(new SayEffector(message)); // Add Say Effector
    }

    void EffectorFactory::addCreateEffector(const std::string &filename, int type) {
        effectors.push_back(new CreateEffector(filename, type)); // Add Create Effector
    }

    void EffectorFactory::addInitEffector(int playerNumber,
                                          const std::string &teamName) {
        effectors.push_back(new InitEffector(playerNumber, teamName)); // Add Init Effector
    }

    void EffectorFactory::addBeamEffector(const Pose2D &pose) {
        effectors.push_back(new BeamEffector(pose)); // Add Beam Effector
    }

    std::string EffectorFactory::toString() {
        std::stringstream stream;
        std::list<Effector *>::iterator it;

        // Iterate through the effectors list and add each effector as a server message
        for (it = effectors.begin(); it != effectors.end(); ++it)
            stream << (*it)->toString();
        stream << SynchronizeEffector().toString();
        return stream.str();
    }

} /* namespace action */
