/*
 * KeyframeMotionPage.h
 *
 *  Created on: Oct 6, 2015
 *      Author: itandroids
 */

#ifndef SOURCE_CONTROL_KEYFRAME_KEYFRAMEMOTIONPAGE_H_
#define SOURCE_CONTROL_KEYFRAME_KEYFRAMEMOTIONPAGE_H_

#include <vector>

#include "core/control/keyframe/KeyframeConstants.h"
#include "KeyframeStep.h"
#include "representations/NaoJoints.h"
#include "core/control/keyframe/KeyframeType.h"
#include "math/MathUtils.h"
#include "math/SplineInterpolator.h"
#include "math/Matrix.h"
#include "math/Vector2.h"
#include "control/MovementSwitchSmoother.h"

namespace control {

    namespace keyframe {

        using namespace representations;

        class KeyframePage {
        public:
            /**
             * Default constructor for a general page
             */
            KeyframePage();
            /**
             * Constructor that gets the name of the page
             * @param name name of the page
             */
            KeyframePage(std::string name);

            virtual ~KeyframePage();

            void addStep(std::shared_ptr<KeyframeStep> step);

            void setName(std::string name);

            void setInitialTime(double time);

            virtual std::string getName();

            virtual void setSpeedRate(double speedRate);

            virtual double getSpeedRate();

            virtual double getMovementTime();

            virtual bool hasFinished();

            double getCurrentTime();

            /**
             * Resets the page, in time, step and joints
             */
            virtual void reset();

            /**
             * Initialize the data, setting the interpolators and so on
             */
            virtual void init();

            virtual representations::NaoJoints &
            getDesiredJoints(representations::NaoJoints &perceivedJoints, double globalTime, bool isInverted);

            virtual std::vector<double> interpolate() = 0;

        protected:
            std::vector<std::shared_ptr<KeyframeStep>> steps;
            double currentTime;
            int currentStep;
            double speedRate;
            std::vector<double> stepTimes;
            bool hasStarted;
            double initialTime;
            representations::NaoJoints desiredJoints;
            representations::NaoJoints previousJoints;
            bool isSmoothing;
        private:
            virtual void createInterpolators() = 0;

            std::string name;
            int id;
            double movementTime;
            std::string filePath;

            control::MovementSwitchSmoother smoother;
            static const double MAXIMUM_DIFF;

            NaoJoints previousDesiredPositions;


        };

    } /* namespace keyframe */

} /* namespace control */

#endif /* SOURCE_CONTROL_KEYFRAME_KEYFRAMEMOTIONPAGE_H_ */
