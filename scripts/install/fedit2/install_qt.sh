#!/bin/bash

pushd $HOME
wget "https://pt.osdn.net/frs/redir.php?m=c3sl&f=%2Frctools%2F48791%2Ffedit2-0.0.0.tar.gz"
tar -xvf qt-all-opensource-src-4.3.5.tar.gz
cd qt-all-opensource-src-4.3.5
./configure
make
sudo make install
