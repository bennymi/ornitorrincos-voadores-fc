/*
 * CreateEffector.cpp
 *
 *  Created on: Sep 30, 2015
 *      Author: mmaximo
 */

#include "action/CreateEffector.h"

#include <sstream>

namespace action {

    CreateEffector::CreateEffector(const std::string &filename, int type) :
            filename(filename), type(type) {
    }

    CreateEffector::~CreateEffector() {
    }

    std::string CreateEffector::toString() {

        // Gets the filename (robot configuration) and type (robot type) as a string
        std::stringstream stream;
        stream << "(scene ";
        stream << filename;
        stream << " ";
        stream << type;
        stream << ")";
        return stream.str();
    }

} /* namespace action */
