/*
 * ControlStub.cpp
 *
 *  Created on: Oct 1, 2015
 *      Author: mmaximo
 */

#include "ControlStub.h"

namespace control {

    ControlStub::ControlStub() {
    }

    ControlStub::~ControlStub() {
    }

    itandroids_lib::math::Pose2D ControlStub::getWalkOdometry() {
        return walkOdometry;
    }

    representations::NaoJoints &ControlStub::getJointsCommands() {
        return naoJoints;
    }

    void ControlStub::setWalkOdometry(itandroids_lib::math::Vector3<double> odometry) {
        walkOdometry = odometry;
    }

} /* namespace control */
