/*
 * BodyModel.h
 *
 *  Created on: Oct 6, 2015
 *      Author: mmaximo
 */

#ifndef SOURCE_MODELING_BODYMODEL_H_
#define SOURCE_MODELING_BODYMODEL_H_

#include "math/Pose3D.h"
#include "math/Vector3.h"
#include "modeling/BodyPartProperties.h"

namespace modeling {

    using namespace itandroids_lib::math;
    using namespace representations;

/**
 * Class that represents the body model.
 */
    class BodyModel {
    public:
        /**
         * Default constructor of BodyModel.
         */
        BodyModel();

        /**
         * Default destructor of BodyModel.
         */

        virtual ~BodyModel();

        /**
         * Updates the body model.
         */

        void update(NaoJoints &joints);

        /**
         * Gets the transform at center of a body part type.
         *
         * @param bodyPartType the type of a the body type.
         *
         * @return the transform at center of a body part type.
         */

        Pose3D &getTransformAtCenter(BodyPartType bodyPartType);

        /**
         * Gets the bottom of the left foot.
         *
         * @return the bottom of the left foot.
         */

        Pose3D getLeftLegEndEffectorTransform();

        /**
         * Gets the bottom of the right foot.
         *
         * @return the bottom of the right foot.
         */

        Pose3D getRightLegEndEffectorTransform();

        /**
         * Gets the body part.
         *
         * @param bodyPartType the type of body part.
         *
         * @return the body part.
         */

        BodyPartProperties &getBodyPart(BodyPartType bodyPartType);

        Vector3<double> computeCenterOfMass();

    private:
        BodyPartProperties bodyParts[NUM_BODY_PARTS];
        Pose3D transformsAtJoints[NUM_BODY_PARTS];
        Pose3D transformsAtCenters[NUM_BODY_PARTS];

        /**
         * Configures the torso properties.
         */

        void configureTorso();

        /**
         * Configures head properties.
         */

        void configureHead();

        /**
         * Configures left arm properties.
         */

        void configureLeftArm();

        /**
         * Configures right arm properties.
         */

        void configureRightArm();

        /**
         * Configures left leg properties.
         */

        void configureLeftLeg();

        /**
         * Configure right leg properties.
         */

        void configureRightLeg();

        /**
         * Configures the coordinate system.
         */

        void configureCoordinateSystem();

        /**
         * Configure the children of body parts.
         */

        void configureChildren();


        void configureCaches();
        // TODO  Implement configure caches

        /**
         * Updates the body part properties.
         *
         * @param bodyPart the body part type to update.
         * @param joints the joints properties.
         */
        void updateBodyPart(BodyPartType bodyPart, NaoJoints &joints);
    };

} /* namespace modeling */

#endif /* SOURCE_MODELING_BODYMODEL_H_ */
