/*
 * UniversalJointEffector.h
 *
 *  Created on: Sep 30, 2015
 *      Author: mmaximo
 */

#ifndef SOURCE_ACTION_UNIVERSALJOINTEFFECTOR_H_
#define SOURCE_ACTION_UNIVERSALJOINTEFFECTOR_H_

#include "action/Effector.h"

namespace action {

/**
 * Class that represents an universal joint effector.
 */
    class UniversalJointEffector : public Effector {
    public:
        /**
         * Creates an UniversalJointEffector.
         *
         * @param name the joint's name.
         * @param axisSpeed1 speed commanded to the first axis.
         * @param axisSpeed2 speed commanded to the second axis.
         */
        UniversalJointEffector(const std::string &name, double axisSpeed1,
                               double axisSpeed2);

        /**
         * Default destructor of UniversalJointEffector.
         */

        virtual ~UniversalJointEffector();

        /**
         * Gets the universal joint effector as a server message.
         *
         * @return the server message.
         */

        std::string toString();

    private:
        std::string name;
        double axisSpeed1;
        double axisSpeed2;
    };

} /* namespace action */

#endif /* SOURCE_ACTION_UNIVERSALJOINTEFFECTOR_H_ */
