//
// Created by francisco on 07/05/16.
//

#include <fstream>
#include "ActiveVisionFileParser.h"
#include "utils/LogSystem.h"


using ::itandroids_lib::utils::LogSystem;

namespace decision_making {
    namespace behavior {
        namespace active_vision {

            const double ActiveVisionFileParser::INITIAL_X = -15;
            const double ActiveVisionFileParser::INITIAL_Y = -10;
            const double ActiveVisionFileParser::INITIAL_PAN = -M_PI;
            const double ActiveVisionFileParser::INITIAL_TILT = -itandroids_lib::math::MathUtils::degreesToRadians(
                    38.5);
            const double ActiveVisionFileParser::FINAL_X = 15;
            const double ActiveVisionFileParser::FINAL_Y = 10;
            const double ActiveVisionFileParser::FINAL_PAN = M_PI;
            const double ActiveVisionFileParser::FINAL_TILT = itandroids_lib::math::MathUtils::degreesToRadians(29.5);
            const double ActiveVisionFileParser::DX = 1;
            const double ActiveVisionFileParser::DY = 1;
            const double ActiveVisionFileParser::DPAN = M_PI / 32;
            const double ActiveVisionFileParser::DTILT = (FINAL_TILT - INITIAL_TILT) / 7;


            ActiveVisionFileParser::ActiveVisionFileParser() {

            }


            ActiveVisionFileParser::~ActiveVisionFileParser() {
            }


            void ActiveVisionFileParser::parse(std::string filepath) {
                std::ifstream file(filepath);
                std::string str;
                std::getline(file, str);
                //Parse the values of the dimensions
                file >> dimX >> dimY >> dimTheta >> dimPsi;
                //Allocate the matrix
                data = std::make_shared<std::vector<std::vector<std::vector<std::vector<double>>>>>(dimX,
                                                                                                    std::vector<std::vector<std::vector<double>>>(
                                                                                                            dimY,
                                                                                                            std::vector<std::vector<double>>(
                                                                                                                    dimTheta,
                                                                                                                    std::vector<double>(
                                                                                                                            dimPsi))));
                //Parse the values of the data
                double auxValue;
                for (int psiIter = 0; psiIter < dimPsi; psiIter++) {
                    for (int thetaIter = 0; thetaIter < dimTheta; thetaIter++) {
                        for (int yIter = 0; yIter < dimY; yIter++) {
                            for (int xIter = 0; xIter < dimX; xIter++) {
                                file >> auxValue >> auxValue >> auxValue >> auxValue >> auxValue;
                                (*data)[xIter][yIter][thetaIter][psiIter] = auxValue;
                            }
                        }
                    }
                }

                // The matrix is assembled.
                file.close();

            }

            const std::vector<std::vector<std::vector<std::vector<double>>>> &ActiveVisionFileParser::getData() {
                return *data;
            }

            double ActiveVisionFileParser::getCostValue(const itandroids_lib::math::Pose2D &robotPose, double pan,
                                                        double tilt) {
                itandroids_lib::math::Pose2D limitPose = robotPose;
                if (limitPose.translation.x < INITIAL_X) {
                    limitPose.translation.x = INITIAL_X;
                } else if (limitPose.translation.x > FINAL_X) {
                    limitPose.translation.x = FINAL_X;
                }
                if (limitPose.translation.y < INITIAL_Y) {
                    limitPose.translation.y = INITIAL_Y;
                } else if (limitPose.translation.y > FINAL_Y) {
                    limitPose.translation.y = FINAL_Y;
                }
                int xIndex = std::floor((limitPose.translation.x - INITIAL_X) / DX);
                int yIndex = std::floor((limitPose.translation.y - INITIAL_Y) / DY);
                auto headAngle = limitPose.rotation + pan;

                headAngle = itandroids_lib::math::MathUtils::normalizeAngle(headAngle);
                if (headAngle < INITIAL_PAN) {
                    headAngle = FINAL_PAN;
                }
                int panIndex = std::round((headAngle - INITIAL_PAN) / DPAN);
                if (panIndex >= (*data)[0][0].size()) {
                    panIndex = (*data)[0][0].size() - 1;
                } else if (panIndex < 0) {
                    panIndex = 0;
                }

                int tiltIndex = std::round((tilt - INITIAL_TILT) / DTILT);
                if (tiltIndex > std::round((FINAL_TILT - INITIAL_TILT) / DTILT)) {
                    tiltIndex = std::round((FINAL_TILT - INITIAL_TILT) / DTILT);
                } else if (tiltIndex < 0) {
                    tiltIndex = 0;
                }
                if (!(xIndex >= 0)) {
                    xIndex = 0;
                }
                if (!(yIndex >= 0)) {
                    yIndex = 0;
                }
                return (*data)[xIndex][yIndex][panIndex][tiltIndex];
            }


        }
    }
}



