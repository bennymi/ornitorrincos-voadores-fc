/*
 * Behavior.cpp
 *
 *  Created on: Oct 30, 2015
 *      Author: itandroids
 */

#include "Behavior.h"
#include "BehaviorFactory.h"

namespace decision_making {

    namespace behavior {

        Behavior::Behavior() {
            movementRequest = nullptr;
            behaviorFactory = nullptr;
        }

        Behavior::Behavior(BehaviorFactory *behaviorFactory) : behaviorFactory(behaviorFactory) {
            movementRequest = nullptr;
        }

        Behavior::~Behavior() {
        }

        void Behavior::behave(modeling::Modeling &modeling) {


        }

        std::shared_ptr<control::MovementRequest> Behavior::getMovementRequest() {
            return (movementRequest);
        }

        Vector3<double> Behavior::saturateWalkVelocity(
                Vector3<double> walkVelocity) {
            Vector3<double> saturatedWalkVelocity = walkVelocity;
            double factor = 10.0;
            if (fabs(saturatedWalkVelocity.z)
                > representations::NavigationParams::getNavigationParams().max_vpsi
                  * representations::NavigationParams::getNavigationParams().low_vpsi_factor) {
                saturatedWalkVelocity.x = MathUtils::saturate(walkVelocity.x,
                                                              -representations::NavigationParams::getNavigationParams().max_v_backward /
                                                              factor,
                                                              representations::NavigationParams::getNavigationParams().max_v_forward /
                                                              factor);
                saturatedWalkVelocity.y = MathUtils::saturate(walkVelocity.y,
                                                              -representations::NavigationParams::getNavigationParams().max_vy /
                                                              factor,
                                                              representations::NavigationParams::getNavigationParams().max_vy /
                                                              factor);
                saturatedWalkVelocity.z = MathUtils::saturate(walkVelocity.z,
                                                              -representations::NavigationParams::getNavigationParams().max_vpsi,
                                                              representations::NavigationParams::getNavigationParams().max_vpsi);
            } else if (saturatedWalkVelocity.x
                       < -representations::NavigationParams::getNavigationParams().max_v_backward
                         * representations::NavigationParams::getNavigationParams().low_v_backward_factor) {
                saturatedWalkVelocity.x = MathUtils::saturate(walkVelocity.x,
                                                              -representations::NavigationParams::getNavigationParams().max_v_backward,
                                                              representations::NavigationParams::getNavigationParams().max_v_forward);
                saturatedWalkVelocity.y = MathUtils::saturate(walkVelocity.y,
                                                              -representations::NavigationParams::getNavigationParams().max_vy /
                                                              factor,
                                                              representations::NavigationParams::getNavigationParams().max_vy /
                                                              factor);
                saturatedWalkVelocity.z = MathUtils::saturate(walkVelocity.z,
                                                              -representations::NavigationParams::getNavigationParams().max_vpsi /
                                                              factor,
                                                              representations::NavigationParams::getNavigationParams().max_vpsi /
                                                              factor);
            } else if (saturatedWalkVelocity.x
                       > representations::NavigationParams::getNavigationParams().max_v_forward
                         * representations::NavigationParams::getNavigationParams().low_v_forward_factor) {
                saturatedWalkVelocity.x = MathUtils::saturate(walkVelocity.x,
                                                              -representations::NavigationParams::getNavigationParams().max_v_backward,
                                                              representations::NavigationParams::getNavigationParams().max_v_forward);
                saturatedWalkVelocity.y = MathUtils::saturate(walkVelocity.y,
                                                              -representations::NavigationParams::getNavigationParams().max_vy /
                                                              factor,
                                                              representations::NavigationParams::getNavigationParams().max_vy /
                                                              factor);
                saturatedWalkVelocity.z = MathUtils::saturate(walkVelocity.z,
                                                              -representations::NavigationParams::getNavigationParams().max_vpsi /
                                                              (3.0f * factor),
                                                              representations::NavigationParams::getNavigationParams().max_vpsi /
                                                              (3.0f * factor));
            } else if (fabs(saturatedWalkVelocity.y)
                       > representations::NavigationParams::getNavigationParams().max_vy
                         * representations::NavigationParams::getNavigationParams().low_vy_factor) {
                saturatedWalkVelocity.x = MathUtils::saturate(walkVelocity.x,
                                                              -representations::NavigationParams::getNavigationParams().max_v_backward /
                                                              factor,
                                                              representations::NavigationParams::getNavigationParams().max_v_forward /
                                                              factor);
                saturatedWalkVelocity.y = MathUtils::saturate(walkVelocity.y,
                                                              -representations::NavigationParams::getNavigationParams().max_vy,
                                                              representations::NavigationParams::getNavigationParams().max_vy);
                saturatedWalkVelocity.z = MathUtils::saturate(walkVelocity.z,
                                                              -representations::NavigationParams::getNavigationParams().max_vpsi /
                                                              factor,
                                                              representations::NavigationParams::getNavigationParams().max_vpsi /
                                                              factor);
            }

            return saturatedWalkVelocity;
        }


    } /* namespace behavior */
} /* namespace decision_making */
