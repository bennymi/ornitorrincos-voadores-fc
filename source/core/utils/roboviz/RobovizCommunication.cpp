/*
 * RobovizConnection.cpp
 *
 *  Created on: Oct 7, 2015
 *      Author: itandroids
 */

#include "RobovizCommunication.h"

namespace utils {
    namespace roboviz {

        RobovizCommunication::RobovizCommunication(std::string host, std::string port) :
                host(host), port(port) {
            numbytes = 0;
            rv = 0;
            servinfo = NULL;
            sockfd = 0;
            p = NULL;
        }

        RobovizCommunication::~RobovizCommunication() {
        }

        void RobovizCommunication::establishConnection() {
            memset(&hints, 0, sizeof hints);
            hints.ai_family = AF_UNSPEC;
            hints.ai_socktype = SOCK_DGRAM;

            if ((rv = getaddrinfo(host.c_str(),
                                  port.c_str(), &hints, &servinfo)) != 0) {
                fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(rv));
                return;
            }

            // loop through all the results and make a socket
            for (p = servinfo; p != NULL; p =
                                                  p->ai_next) {
                if ((sockfd = socket(p->ai_family,
                                     p->ai_socktype, p->ai_protocol)) == -1) {
                    perror("socket");
                    continue;
                }

                break;
            }

            if (p == NULL) {
                fprintf(stderr, "failed to bind socket\n");
                return;
            }

        }

        void RobovizCommunication::closeConnection() {
            freeaddrinfo(servinfo);
            close(sockfd);
        }


    } /* namespace roboviz */
} /* namespace utils */
