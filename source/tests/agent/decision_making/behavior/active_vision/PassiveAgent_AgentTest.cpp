//
// Created by francisco on 5/29/16.
//

#include "PassiveAgent.h"

static const int N_LOOPS = 50000;

int main(int argc, char *argv[]) {
    std::string agentNumber(argv[1]);

    PassiveAgent agent(std::stoi(agentNumber), argv[2]);
    agent.testLoop(N_LOOPS);
}