//
// Created by francisco on 6/20/16.
//

#include "BaseFullAgentTest.h"

BaseFullAgentTest::BaseFullAgentTest(std::string host,
                                     int agentPort,
                                     int monitorPort,
                                     int agentNumber,
                                     int robotType, std::string teamName) : BaseRoleAgentTest(host, agentPort,
                                                                                              monitorPort, agentNumber,
                                                                                              robotType, teamName) {
    roleStrategy = std::make_shared<decision_making::RoleStrategyImpl>();
    decisionMaking = std::make_unique<decision_making::DecisionMakingImpl>(roleStrategy);
}

void BaseFullAgentTest::runStep() {
    decisionMaking->decide(modeling);
}

void BaseFullAgentTest::setup() {

}



