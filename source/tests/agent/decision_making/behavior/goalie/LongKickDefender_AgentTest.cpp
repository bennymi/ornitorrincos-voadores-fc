//
// Created by dicksiano on 8/15/16.
//

#include "decision_making/behavior/KickBehavior.h"
#include "base/BaseAgentTest.h"

class LongKickDefender_AgentTest : public BaseAgentTest {

public:

    LongKickDefender_AgentTest(std::string host = "127.0.0.1", int agentPort = 3100, int monitorPort = 3200,
                               int agentNumber = 5, std::string teamName = std::string("Enemy")) : BaseAgentTest(host,
                                                                                                                 agentPort,
                                                                                                                 monitorPort,
                                                                                                                 agentNumber,
                                                                                                                 0,
                                                                                                                 teamName) {

    };

    int counter;

    virtual void testLoop(int nSteps) { BaseAgentTest::testLoop(nSteps); }

    bool first;
    double t0;
    double deltaT;

    virtual void setup() {
        counter = 0;
        wiz.setBallPosition(Vector3<double>(-12.0, 0.0, 0.0));
        first = true;
        t0 = modeling.getAgentModel().getAgentPerception().getGlobalTime();
        //wiz.setAgentPositionAndDirection(1,representations::PlaySide::PLAY_SIDE::LEFT,Vector3<double>(0.0,0.0,0.3),0.0);
    }

    virtual void runStep() {
        counter++;
        deltaT = modeling.getAgentModel().getAgentPerception().getGlobalTime() - t0;


        if (counter > 400) {
            wiz.setBallPosition(Vector3<double>(-12.0, 0.0, 0.0));
            counter = 0;
        }

        utils::roboviz::Roboviz roboviz1;
        std::string name1("animated");
        std::string buff1("animated");

        Pose2D ballPosition = modeling.getWorldModel().getBall().getPosition();
        Pose2D selfPosition = modeling.getWorldModel().getSelf().getPosition();
        if (!decisionMaking.decideFall(modeling)) {

            behaviorFactory.getKickBehavior().setTarget(Vector2<double>(15.5, 0));
            behaviorFactory.getKickBehavior().behave(modeling);
            decisionMaking.movementRequest = behaviorFactory.getKickBehavior().getMovementRequest();

            behaviorFactory.getActiveVision().behave(modeling);
            decisionMaking.lookRequest = behaviorFactory.getActiveVision().getLookRequest();
        }


    }
};

int main() {
    // Start the goalie agent

    std::vector<std::thread> threads;
    for (int i = 2; i < 3; i++) {
        std::stringstream stream;
        stream << "./Goalie_AgentTest " << i << " " << "Enemy &";
        system(stream.str().c_str());
        stream.str("");
        stream << "sleep 5";
        system(stream.str().c_str());
    }

    /**
     * Start active agent
     */
    LongKickDefender_AgentTest t;
    t.testLoop(50000);
}