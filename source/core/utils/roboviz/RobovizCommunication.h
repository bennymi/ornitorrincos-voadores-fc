/*
 * RobovizConnection.h
 *
 *  Created on: Oct 7, 2015
 *      Author: itandroids
 */

#ifndef SOURCE_UTILS_ROBOVIZ_ROBOVIZCOMMUNICATION_H_
#define SOURCE_UTILS_ROBOVIZ_ROBOVIZCOMMUNICATION_H_

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <string>
#include <math.h>

using namespace std;

namespace utils {
    namespace roboviz {

        class RobovizCommunication {
        public:
            RobovizCommunication(std::string host, std::string port);

            virtual ~RobovizCommunication();

            void establishConnection();

            void closeConnection();

            struct addrinfo *p;
            struct addrinfo hints, *servinfo;
            int sockfd;
        private:
            std::string host;
            std::string port;

            int rv;
            int numbytes;

        };

    } /* namespace roboviz */
} /* namespace utils */

#endif /* SOURCE_UTILS_ROBOVIZ_ROBOVIZCOMMUNICATION_H_ */
