//
// Created by francisco on 6/22/16.
//

#ifndef ITANDROIDS_SOCCER3D_CPP_LONGDISTANCE20MKICK_H
#define ITANDROIDS_SOCCER3D_CPP_LONGDISTANCE20MKICK_H


#include "KeyframeKick.h"

namespace decision_making {
    namespace behavior {
        namespace kick_behavior {
            class LongDistance20mKick : public KeyframeKick {
            public:
                LongDistance20mKick();

            private:

            };

        }
    }
}


#endif //ITANDROIDS_SOCCER3D_CPP_LONGDISTANCE20MKICK_H
