/*
 * ControlStub.h
 *
 *  Created on: Oct 1, 2015
 *      Author: mmaximo
 */

#ifndef TESTS_SOURCE_CONTROL_CONTROLSTUB_H_
#define TESTS_SOURCE_CONTROL_CONTROLSTUB_H_

#include "control/Control.h"
#include "representations/NaoJoints.h"
#include "math/Vector3.h"

namespace control {

    class ControlStub : public Control {
    public:
        representations::NaoJoints naoJoints;

        ControlStub();

        ~ControlStub();

        itandroids_lib::math::Pose2D getWalkOdometry();

        void setWalkOdometry(itandroids_lib::math::Vector3<double> odometry);

        representations::NaoJoints &getJointsCommands();

    private:
        itandroids_lib::math::Vector3<double> walkOdometry;
    };

} /* namespace control */

#endif /* TESTS_SOURCE_CONTROL_CONTROLSTUB_H_ */
