//
// Created by dicksiano on 27/05/16.
//

#ifndef ITANDROIDS_SOCCER3D_CPP_MARKINGDEFENSE_H
#define ITANDROIDS_SOCCER3D_CPP_MARKINGDEFENSE_H

#include "Positioning.h"
#include "math/Pose2D.h"
#include "math/Vector2.h"
#include "decision_making/positioning/DelaunayPositioning.h"
#include "representations/OtherPlayer.h"

namespace modeling {
    class Modeling;
}

namespace decision_making {
    namespace positioning {
        using itandroids_lib::math::Vector2;
        using representations::OtherPlayer;

/**
 * Class that represents the behavior that controls the defensive marking.
 */
        class BasicDefensePositioning : public Positioning {
        public:
            /**
             * Constructor for marking defense
             */
            BasicDefensePositioning();

            /**
             * Destructor
             */
            virtual ~BasicDefensePositioning();

            /**
             * Calculates for which position the agent must go.
             *
             * @param agent number
             * @param modeling
             */
            virtual itandroids_lib::math::Pose2D getAgentPosition(modeling::Modeling &modeling);

        private:
            decision_making::positioning::DelaunayPositioning delaunayPositioning;

            Vector2<double> chosenOpponentMarkingPosition(modeling::Modeling &modeling,
                                                          const OtherPlayer &opponent);

            /**
             * Decides which opponent should be marked by this agent.
             *
             * @param agent number
             * @param modeling
             *
             * @return the opponent's number
             */
            const OtherPlayer &chooseOpponent(modeling::Modeling &modeling, int agentNum);

            /**
             * This method calculates an cost for each opponent.
             * This cost will be used to determine which opponent
             * is the most dangerous and should be marked.
             *
             * @param opponent's agent number
             * @param modeling
             *
             * @return the opponents cost
             */
            double opponentFunction(int agentNum, modeling::Modeling &modeling);
        };

    } /* namespace positioning */
} /* namespace decision_making */


#endif //ITANDROIDS_SOCCER3D_CPP_MARKINGDEFENSE_H
