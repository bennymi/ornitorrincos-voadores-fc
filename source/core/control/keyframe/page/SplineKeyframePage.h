//
// Created by francisco on 5/22/16.
//

#ifndef ITANDROIDS_SOCCER3D_CPP_SPLINEKEYFRAMEPAGE_H
#define ITANDROIDS_SOCCER3D_CPP_SPLINEKEYFRAMEPAGE_H

#include "control/keyframe/page/KeyframeStep.h"
#include "math/SplineInterpolator.h"
#include "math/Vector2.h"
#include "KeyframePage.h"

using ::itandroids_lib::math::Vector2;

namespace control {
    namespace keyframe {
        class SplineKeyframePage : public KeyframePage {
        public:
            std::vector<double> interpolate() override;

        private:
            std::vector<itandroids_lib::math::SplineInterpolator> interpolators;

            virtual void createInterpolators() override;
        };

    }
}


#endif //ITANDROIDS_SOCCER3D_CPP_SPLINEKEYFRAMEPAGE_H
