#ifndef SOURCE_PERCEPTION_HINGEJOINTPERCEPTOR_H_
#define SOURCE_PERCEPTION_HINGEJOINTPERCEPTOR_H_

#include "utils/parser/KeyValuePair.h"
#include "utils/parser/ParserTreeNode.h"
#include "math/MathUtils.h"
#include "perception/Perceptor.h"
#include <stdlib.h>

namespace perception {

    class HingeJointPerceptor : public Perceptor {
    public:
        /**
         * Assignment constructor
         *
         */
        HingeJointPerceptor(utils::parser::ParserTreeNode &node);

        /**
         *	Destructor
         */
        virtual ~HingeJointPerceptor();

        /**
         * Get angleHingeJoint value
         *
         * @return value
         */
        double getAngle();

    private:
        /**
         * angleHingeJoint represents the position angle of the axis in degrees
         *
         */
        double angle;
    };

} /* namespace perception */

#endif /* SOURCE_PERCEPTION_HINGEJOINTPERCEPTOR_H_ */
