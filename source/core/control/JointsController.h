/*
 * JointsController.h
 *
 *  Created on: Oct 10, 2015
 *      Author: mmaximo
 */

#ifndef SOURCE_CONTROL_JOINTSCONTROLLER_H_
#define SOURCE_CONTROL_JOINTSCONTROLLER_H_

#include <fstream>
#include "control/PController.h"
#include "representations/NaoJoints.h"

namespace control {

    class JointsController {
    public:
        /**
         * Constructor for the joints controller, that receives a type of robot
         */
        JointsController(int robotType, double kp);

        /**
         * Default destructor for the joints controller
         */
        virtual ~JointsController();

        /**
         * Generates the commands for the server, so the robot can get to the desired joints
         */
        void update(representations::NaoJoints &desired, representations::NaoJoints &actual);

        /**
         * Returns the generated commands for the server
         */
        representations::NaoJoints &getCommands();

    private:
        int robotType;
        representations::NaoJoints commands;
        itandroids_lib::control::PController controllers[representations::NaoJoints::NUM_JOINTS];
    };

} /* namespace control */

#endif /* SOURCE_CONTROL_JOINTSCONTROLLER_H_ */
