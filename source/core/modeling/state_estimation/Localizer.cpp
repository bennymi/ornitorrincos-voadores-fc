/*
 * Localizer.cpp
 *
 *  Created on: Aug 29, 2015
 *      Author: mmaximo
 */

#include "Localizer.h"
#include "geometry/GeometryUtils.h"
#include "utils/LogSystem.h"

namespace modeling {
    namespace state_estimation {

        using itandroids_lib::math::Vector2;
        using itandroids_lib::geometry::Line2D;
        using itandroids_lib::geometry::Point2D;
        using itandroids_lib::geometry::GeometryUtils;

        LocalizerParams::LocalizerParams(double distanceSigma, double bearingSigma,
                                         double odometryXSigma, double odometryYSigma, double odometryPsiSigma,
                                         bool useLandmarks, bool useLines, bool useNegativeInfo) :
                distanceSigma(distanceSigma), bearingSigma(bearingSigma), odometryXSigma(
                odometryXSigma), odometryYSigma(odometryYSigma), odometryPsiSigma(
                odometryPsiSigma), useLandmarks(useLandmarks), useLines(useLines), useNegativeInfo(useNegativeInfo) {
        }

        LocalizerParams::LocalizerParams() {
            (*this) = getDefaultLocalizerParams();
        }

        LocalizerParams LocalizerParams::getDefaultLocalizerParams() {
            return LocalizerParams(0.31, 5.0 * 0.35 * M_PI / 180.0,
                                   0.01, 0.01, 0.005, true, true, true);
        }

        Localizer::Localizer(FieldDescription &fieldDescription, int numParticles,
                             LocalizerParams params) :
                odometryModel(params.odometryXSigma, params.odometryYSigma, params.odometryPsiSigma),
                observationModel(params.distanceSigma * params.distanceSigma,
                                 params.bearingSigma * params.bearingSigma),
                particleResetter(fieldDescription, params.distanceSigma, params.bearingSigma),
                fieldDescription(fieldDescription), numParticles(numParticles), params(params),
                uniformDistribution(MathUtils::getUniformRandomGenerator(0.0, 1.0)),
                slowObservationQuality(-1.0), fastObservationQuality(-1.0) {
            alphaSlow = 0.01;
            alphaFast = 0.05;

            particles = new Particle[numParticles];
            resampledParticles = new Particle[numParticles];
            resampledIndices = new int[numParticles];

            landmarkObservationProbability = new double[numParticles];
            lineObservationProbability = new double[numParticles];

            resetParticles();
        }

        Localizer::~Localizer() {
            delete[] particles;
            delete[] resampledIndices;
            delete[] resampledParticles;
        }

        void Localizer::reset(Pose2D estimate, Pose2D sigma) {
            boost::variate_generator<boost::mt19937 &, boost::normal_distribution<> > xDistribution =
                    MathUtils::getNormalRandomGenerator(estimate.translation.x,
                                                        sigma.translation.x);
            boost::variate_generator<boost::mt19937 &, boost::normal_distribution<> > yDistribution =
                    MathUtils::getNormalRandomGenerator(estimate.translation.y,
                                                        sigma.translation.y);
            boost::variate_generator<boost::mt19937 &, boost::normal_distribution<> > psiDistribution =
                    MathUtils::getNormalRandomGenerator(estimate.rotation, sigma.rotation);

            Pose2D particlePose;
            for (int i = 0; i < numParticles; ++i) {
                particlePose.translation.x = xDistribution();
                particlePose.translation.y = yDistribution();
                particlePose.rotation = MathUtils::normalizeAngle(psiDistribution());

                particles[i].setPose(particlePose);
            }
        }

        void Localizer::update(double globalTime,
                               const std::vector<perception::VisibleFlag *> &visibleFlags,
                               const std::vector<perception::VisibleGoalPost *> &visibleGoalPosts,
                               const std::vector<perception::VisibleLine *> &visibleLines,
                               Pose2D &odometry, representations::NaoJoints &jointsPosition) {

            drawParticles(particles, odometry);
            if (!visibleFlags.empty() || !visibleGoalPosts.empty() || !visibleLines.empty()) {
                std::vector<LandmarkObservation> landmarkObservations = getLandmarkObservations(
                        visibleFlags, visibleGoalPosts);
                std::vector<Line2D> lineObservations = getLinesObservations(visibleLines);

                updateWeights(landmarkObservations, lineObservations, jointsPosition);
                resample();
                double resetProbability = std::max(0.01, 1.0 - fastObservationQuality / slowObservationQuality);
                particleResetter.sensorReset(particles, numParticles, landmarkObservations, lineObservations,
                                             resetProbability);
                //moveStep(landmarkObservations, odometry);
            }
            updateEstimate();
        }

        const Pose2D &Localizer::getPoseEstimate() {
            return estimate;
        }

        Particle *Localizer::getParticles() {
            return particles;
        }

        void Localizer::updateEstimate() {
            estimate = Pose2D();
            double sumSine = 0.0;
            double sumCosine = 0.0;

            for (int i = 0; i < numParticles; ++i) {
                const Pose2D &particlePose = particles[i].getPose();

                estimate.translation.x += particlePose.translation.x;
                estimate.translation.y += particlePose.translation.y;
                sumSine += sin(particlePose.rotation);
                sumCosine += cos(particlePose.rotation);
            }
            estimate.translation.x /= numParticles;
            estimate.translation.y /= numParticles;
            estimate.rotation = atan2(sumSine / numParticles, sumCosine / numParticles);
        }

        void Localizer::drawParticles(Particle *particles, Pose2D &odometry) {
            odometryModel.propagate(particles, numParticles, odometry);
        }

        void Localizer::updateWeights(std::vector<LandmarkObservation> &landmarkObservations,
                                      std::vector<Line2D> &lineObservations,
                                      representations::NaoJoints &jointsPosition) {
            double sumWeights = 0.0;
            double observationsQuality = 0.0;
            double landmarkObservationsQuality = 0.0;
            double lineObservationsQuality = 0.0;
            bool useLineObservations = true;
            bool useLandmarkObservations = true;

            //updates the observation Model with the current estimate
            observationModel.updateWithEstimate(lineObservations, estimate, jointsPosition);

            for (int i = 0; i < numParticles; ++i) {
                landmarkObservationProbability[i] = exp(
                        observationModel.calculateObservationLogProbabilityFromLandmarks(
                                particles[i], landmarkObservations));
                lineObservationProbability[i] = exp(observationModel.calculateObservationLogProbabilityFromLines(
                        particles[i], lineObservations));

                landmarkObservationsQuality += landmarkObservationProbability[i];
                lineObservationsQuality += lineObservationProbability[i];
            }

            //std::cout << "LineQuality: " << lineObservationsQuality << " " << "landmarkQuality" << landmarkObservationsQuality << std::endl;
            if (landmarkObservations.size() == 0) {
                useLandmarkObservations = false;
                //std::cout << "No landmark in sight" << std::endl;
            }
            if (lineObservationsQuality < 0.00000001 && landmarkObservations.size() > 0) {
                useLineObservations = false;
                //std::cout << "Will not use Line Observations!" << std::endl;
            }

            //update observation quality
            observationsQuality = 0.0;
            if (useLandmarkObservations) observationsQuality += landmarkObservationsQuality;
            if (useLineObservations) observationsQuality += lineObservationsQuality;
            for (int i = 0; i < numParticles; i++) {
                double observationsProbability = 1.0; //landmarkObservationProbability[i];
                if (useLandmarkObservations)
                    observationsProbability *= landmarkObservationProbability[i];
                if (useLineObservations)
                    observationsProbability *= lineObservationProbability[i];

                particles[i].setWeight(particles[i].getWeight() * observationsProbability);
                sumWeights += particles[i].getWeight();
            }

            for (int i = 0; i < numParticles; ++i) {
                particles[i].setWeight(particles[i].getWeight() / sumWeights);
            }
            //std::cout << "Observation Quality " << observationsQuality << std::endl;
            if (observationsQuality < 0.000000001) {
                slowObservationQuality = 1;
                fastObservationQuality = 0;
            } else if (slowObservationQuality < 0.0 && fastObservationQuality < 0.0) {
                slowObservationQuality = observationsQuality;
                fastObservationQuality = observationsQuality;
            } else {
                slowObservationQuality = (1.0 - alphaSlow) * slowObservationQuality
                                         + alphaSlow * observationsQuality;
                fastObservationQuality = (1.0 - alphaFast) * fastObservationQuality
                                         + alphaFast * observationsQuality;
            }
        }

        void Localizer::resample() {
            double r = uniformDistribution() / static_cast<double>(numParticles);
            double c = particles[0].getWeight();
            int i = 0;

            double u;
            for (int p = 0; p < numParticles; ++p) {
                u = r + static_cast<double>(p) / static_cast<double>(numParticles);
                while (u > c) {
                    ++i;
                    c += particles[i].getWeight();
                }
                resampledIndices[p] = i;
            }

            for (int i = 0; i < numParticles; ++i) {
                resampledParticles[i] = particles[resampledIndices[i]];
            }

//    for (int i = 0; i < numParticles; ++i) {
//        particles[i] = resampledParticles[i];
//    }
            std::memcpy(particles, resampledParticles, numParticles * sizeof(Particle));

            resetWeights();
        }

/*void Localizer::moveStep(std::vector<LandmarkObservation> &observations,
                         Pose2D &odometry) {
    drawParticles(previousParticles, newParticles, odometry);
    double alpha;

    for (int i = 0; i < numParticles; ++i) {
        double pStar = calculateObservationsProbability(newParticles[i],
                                                        observations);
        double pBar = calculateObservationsProbability(particles[i],
                                                       observations);
        alpha = std::min(1.0, pStar / pBar);
        double u = uniformDistribution();
        if (u <= alpha)
            particles[i] = newParticles[i];
    }
}*/

        void Localizer::resetWeights() {
            for (int i = 0; i < numParticles; ++i) {
                particles[i].setWeight(1.0 / numParticles);
            }
        }

        void Localizer::resetParticles() {
            Pose2D particlePose;
            for (int i = 0; i < numParticles; ++i) {
                particlePose.translation.x = -fieldDescription.FIELD_WIDTH / 2.0
                                             + uniformDistribution() * fieldDescription.FIELD_WIDTH;
                particlePose.translation.y = -fieldDescription.FIELD_LENGTH / 2.0
                                             + uniformDistribution() * fieldDescription.FIELD_LENGTH;
                particlePose.rotation = -M_PI + uniformDistribution() * 2.0 * M_PI;

                particles[i].setPose(particlePose);
            }

            resetWeights();
            //std::memcpy(previousParticles, particles, numParticles * sizeof(Particle));
        }

        std::vector<LandmarkObservation> Localizer::getLandmarkObservations(
                const std::vector<perception::VisibleFlag *> &visibleFlags,
                const std::vector<perception::VisibleGoalPost *> &visibleGoalPosts) {
            std::vector<LandmarkObservation> observations;
            observations.reserve(visibleFlags.size() + visibleGoalPosts.size());
            for (int i = 0; i < visibleFlags.size(); ++i) {
                observations.push_back(getLandmarkObservation(*visibleFlags[i]));
            }
            for (int i = 0; i < visibleGoalPosts.size(); ++i) {
                observations.push_back(getLandmarkObservation(*visibleGoalPosts[i]));
            }

            return observations;
        }

        LandmarkObservation Localizer::getLandmarkObservation(
                perception::VisibleFlag &flag) {
            Vector2<double> position = Vector2<double>(flag.getPosition().x,
                                                       flag.getPosition().y);
            LandmarkObservation observation;
            Vector3<double> knownPosition = fieldDescription.getFlagKnownPosition(
                    flag.getFlagType());
            observation.distance = position.abs();
            observation.bearing = atan2(position.y, position.x);
            observation.knownPosition = Vector2<double>(knownPosition.x,
                                                        knownPosition.y);
            observation.flagType = flag.getFlagType();
            observation.goalPostType = GoalPostType::INVALID;
            return observation;
        }

        LandmarkObservation Localizer::getLandmarkObservation(
                perception::VisibleGoalPost &goalPost) {
            LandmarkObservation observation;
            Vector2<double> position = Vector2<double>(goalPost.getPosition().x,
                                                       goalPost.getPosition().y);
            Vector3<double> knownPosition = fieldDescription.getGoalPostKnownPosition(
                    goalPost.getGoalPostType());
            observation.distance = position.abs();
            observation.bearing = atan2(position.y, position.x);
            observation.knownPosition = Vector2<double>(knownPosition.x,
                                                        knownPosition.y);
            observation.flagType = FlagType::INVALID;
            observation.goalPostType = goalPost.getGoalPostType();
            return observation;
        }

        std::vector<Line2D>
        Localizer::getLinesObservations(const std::vector<perception::VisibleLine *> &visibleLines) {
            std::vector<Line2D> observedLines;
            std::vector<Line2D> shortLines;

            for (int i = 0; i < visibleLines.size(); ++i) {
                Vector2<double> beginOfLine = Vector2<double>(visibleLines[i]->getPosition().x,
                                                              visibleLines[i]->getPosition().y);
                Vector2<double> endOfLine = Vector2<double>(visibleLines[i]->getPosition2().x,
                                                            visibleLines[i]->getPosition2().y);
                Line2D observedLine = Line2D(beginOfLine, endOfLine);

                if (GeometryUtils::getDistanceTo(beginOfLine, endOfLine) > 1.5) {
                    observedLines.push_back(observedLine);
                } else {
                    shortLines.push_back(observedLine);
                }
            }
            if (observedLines.size() == 0 && shortLines.size() == 2) {
                observedLines.push_back(shortLines[0]);
                observedLines.push_back(shortLines[1]);
            } else if (observedLines.size() == 0 && shortLines.size() == 1) {
                observedLines.push_back(shortLines[0]);
            } else if (observedLines.size() == 1 && (shortLines.size() == 2 || shortLines.size() == 1)) {
                observedLines.push_back(shortLines[0]);
            }
            return observedLines;
        }

    } /* namespace state_estimation */
} /* namespace modeling */