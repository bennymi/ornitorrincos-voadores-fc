//
// Created by francisco on 6/16/16.
//

#include "LongDistanceKick.h"
#include "math/RotationMatrix.h"

namespace decision_making {
    namespace behavior {
        namespace kick_behavior {
            LongDistanceKick::LongDistanceKick() : KeyframeKick(std::string("kickLeftLeg")) {

                itandroids_lib::math::RotationMatrix matrix;
//                        double rotationValue = itandroids_lib::math::MathUtils::degreesToRadians(-25.335);
                double rotationValue = itandroids_lib::math::MathUtils::degreesToRadians(-25);
                matrix.rotateZ(rotationValue);
                Vector3<double> originalvector(-0.16, -0.04, 0);
                double orientation = 0.27 + rotationValue;
                originalvector = matrix * originalvector;
                Vector2<double> finalPosition(originalvector.x, originalvector.y);
                setKickParameters(
                        KickParameters(finalPosition, orientation));
//                        KickParameters(Vector2<double>(-0.17,-0.015), 0.2));
            }
        }
    }
}



