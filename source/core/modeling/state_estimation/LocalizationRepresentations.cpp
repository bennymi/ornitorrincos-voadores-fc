//
// Created by mmaximo on 19/06/16.
//

#include "modeling/state_estimation/LocalizationRepresentations.h"

namespace modeling {
    namespace state_estimation {

        bool LandmarkObservation::isFlag() {
            return flagType != representations::FlagType::INVALID;
        }

        bool LandmarkObservation::onUpperBorder() {
            if (isFlag())
                return flagType == representations::FlagType::F1L || flagType == representations::FlagType::F1R;

            return false;
        }

        bool LandmarkObservation::onLeftBorder() {
            if (isFlag()) {
                return flagType == representations::FlagType::F1L || flagType == representations::FlagType::F2L;
            } else {
                return goalPostType == representations::GoalPostType::G1L ||
                       goalPostType == representations::GoalPostType::G2L;
            }
        }

        bool LandmarkObservation::landmarksOnSameBorder(LandmarkObservation &observation1,
                                                        LandmarkObservation &observation2) {
            bool upper1 = observation1.onUpperBorder();
            bool left1 = observation1.onLeftBorder();
            bool upper2 = observation2.onUpperBorder();
            bool left2 = observation2.onLeftBorder();

            return (upper1 == upper2) || (left1 == left2);
        }

    }
}