/*
 * ForceResistanceData.h
 *
 *  Created on: Aug 30, 2015
 *      Author: itandroids
 */

#ifndef SOURCE_REPRESENTATIONS_FORCERESISTANCEDATA_H_
#define SOURCE_REPRESENTATIONS_FORCERESISTANCEDATA_H_

#include "math/MathUtils.h"
#include "math/Vector3.h"
#include <iostream>

namespace representations {
    using namespace itandroids_lib::math;

    class ForceResistanceData {

    public:
        ForceResistanceData();

        /**
         * Constructor for the force resistance data
         * @param originCoordinates of the force
         * @param cartesian values of the force
         */
        ForceResistanceData(Vector3<double> originCoordinates, Vector3<double> force);

        virtual ~ForceResistanceData();

        Vector3<double> &getOriginCoordinates();

        Vector3<double> &getForce();

    private:
        Vector3<double> originCoordinates;
        Vector3<double> force;
    };

} /* namespace representations */

#endif /* SOURCE_REPRESENTATIONS_FORCERESISTANCEDATA_H_ */
