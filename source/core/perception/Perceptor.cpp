/*
 * Perceptor.cpp
 *
 *  Created on: Aug 8, 2015
 *      Author: mmaximo
 */

#include "perception/Perceptor.h"

namespace perception {

    /*
     * Constructor to the case of no data
     */
    Perceptor::Perceptor() {
        name = "unknown";
        perceptorType = perception::UNKNOWN;
    }

    /*
     * Constructor of Perceptor with data from parser tree node
     */
    Perceptor::Perceptor(utils::parser::ParserTreeNode &node) {
        name = node.content;
        boost::trim(name);
        std::vector<std::string> stringVector;
        boost::split(stringVector, name,
                     boost::is_any_of(" "));
        name = stringVector[0];

        if (name.compare("ACC") == 0) {
            perceptorType = perception::ACCELEROMETER;
        } else if (name.compare("GYR") == 0) {
            perceptorType = perception::GYRO_RATE;
        } else if (name.compare("See") == 0) {
            perceptorType = perception::VISION;
        } else if (name.compare("HJ") == 0) {
            perceptorType = perception::HINGE_JOINT;
        } else if (name.compare("UJ") == 0) {
            perceptorType = perception::UNIVERSAL_JOINT;
        } else if (name.compare("TCH") == 0) {
            perceptorType = perception::TOUCH;
        } else if (name.compare("FRP") == 0) {
            perceptorType = perception::FORCE_RESISTANCE;
        } else if (name.compare("AgentState") == 0) {
            perceptorType = perception::AGENT_STATE;
        } else if (name.compare("GS") == 0) {
            perceptorType = perception::GAME_STATE;
        } else if (name.compare("hear") == 0) {
            perceptorType = perception::HEAR;
        } else if (name.compare("time") == 0) {
            perceptorType = perception::GLOBAL_TIME;
        }
    }

    Perceptor::~Perceptor() {

    }

    /*
     * Getter method: returns the name of perceptor
     */
    std::string Perceptor::getName() {
        return name;
    }

    /*
     * Getter method: returns the body part of the perceptor
     */

    perception::BODY_PARTS Perceptor::getBodyPart(std::string name) {
        boost::algorithm::trim(name);
        std::vector<std::string> stringVector;
        boost::algorithm::split(stringVector, name, boost::is_any_of(" "));
        name = stringVector[0];
        if (boost::iequals(name, "torso")) {
            return perception::TORSO;
        } else if (boost::iequals(name, "neck")) {
            return perception::NECK;
        } else if (boost::iequals(name, "head")) {
            return perception::HEAD;
        } else if (boost::iequals(name, "shoulder")) {
            return perception::SHOULDER;
        } else if (boost::iequals(name, "upperarm")) {
            return perception::UPPERARM;
        } else if (boost::iequals(name, "elbow")) {
            return perception::ELBOW;
        } else if (boost::iequals(name, "lowerarm")) {
            return perception::LOWERARM;
        } else if (boost::iequals(name, "hip1")) {
            return perception::HIP1;
        } else if (boost::iequals(name, "hip2")) {
            return perception::HIP2;
        } else if (boost::iequals(name, "thigh")) {
            return perception::THIGH;
        } else if (boost::iequals(name, "shank")) {
            return perception::SHANK;
        } else if (boost::iequals(name, "ankle")) {
            return perception::ANKLE;
        } else {
            return perception::FOOT;
        }
    }

    /*
     * Getter method: returns the name of hinge joint
     */
    perception::HINGE_JOINTS Perceptor::getHingeJoint(std::string name) {
        boost::algorithm::trim(name);
        if (boost::iequals(name, "hj1")) {
            return perception::NECK_YAW;
        } else if (boost::iequals(name, "hj2")) {
            return perception::NECK_PITCH;
        } else if (boost::iequals(name, "laj1")) {
            return perception::LEFT_SHOULDER_PITCH;
        } else if (boost::iequals(name, "laj2")) {
            return perception::LEFT_SHOULDER_YAW;
        } else if (boost::iequals(name, "laj3")) {
            return perception::LEFT_ARM_ROLL;
        } else if (boost::iequals(name, "laj4")) {
            return perception::LEFT_ARM_YAW;
        } else if (boost::iequals(name, "llj1")) {
            return perception::LEFT_HIP_YAWPITCH;
        } else if (boost::iequals(name, "llj2")) {
            return perception::LEFT_HIP_ROLL;
        } else if (boost::iequals(name, "llj3")) {
            return perception::LEFT_HIP_PITCH;
        } else if (boost::iequals(name, "llj4")) {
            return perception::LEFT_KNEE_PITCH;
        } else if (boost::iequals(name, "llj5")) {
            return perception::LEFT_FOOT_PITCH;
        } else if (boost::iequals(name, "llj6")) {
            return perception::LEFT_FOOT_ROLL;
        } else if (boost::iequals(name, "rlj1")) {
            return perception::RIGHT_HIP_YAWPITCH;
        } else if (boost::iequals(name, "rlj2")) {
            return perception::RIGHT_HIP_ROLL;
        } else if (boost::iequals(name, "rlj3")) {
            return perception::RIGHT_HIP_PITCH;
        } else if (boost::iequals(name, "rlj4")) {
            return perception::RIGHT_KNEE_PITCH;
        } else if (boost::iequals(name, "rlj5")) {
            return perception::RIGHT_FOOT_PITCH;
        } else if (boost::iequals(name, "rlj6")) {
            return perception::RIGHT_FOOT_ROLL;
        } else if (boost::iequals(name, "raj1")) {
            return perception::RIGHT_SHOULDER_PITCH;
        } else if (boost::iequals(name, "raj2")) {
            return perception::RIGHT_SHOULDER_YAW;
        } else if (boost::iequals(name, "raj3")) {
            return perception::RIGHT_ARM_ROLL;
        } else if (boost::iequals(name, "raj4")) {
            return perception::RIGHT_ARM_YAW;
        } else {
            return perception::UNKNOWN_JOINT; //hack
        }
    }

    /*
     * Getter method: returns the type of perceptor
     */
    perception::PERCEPTOR_TYPE Perceptor::getPerceptorType() {
        return perceptorType;
    }

} /* namespace perception */
