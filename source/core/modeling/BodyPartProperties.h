/*
 * BodyPartProperties.h
 *
 *  Created on: Oct 4, 2015
 *      Author: mmaximo
 */

#ifndef SOURCE_MODELING_BODYPARTPROPERTIES_H_
#define SOURCE_MODELING_BODYPARTPROPERTIES_H_

#include <iostream>
#include <vector>
#include "math/Vector3.h"
#include "modeling/HingeJointProperties.h"
#include "modeling/BoundingBox.h"

namespace modeling {

    enum BodyPartType {
        TORSO = 0,
        NECK,
        HEAD,
        LEFT_SHOULDER,
        LEFT_UPPER_ARM,
        LEFT_ELBOW,
        LEFT_LOWER_ARM,
        RIGHT_SHOULDER,
        RIGHT_UPPER_ARM,
        RIGHT_ELBOW,
        RIGHT_LOWER_ARM,
        LEFT_HIP1,
        LEFT_HIP2,
        LEFT_THIGH,
        LEFT_SHANK,
        LEFT_ANKLE,
        LEFT_FOOT,
        RIGHT_HIP1,
        RIGHT_HIP2,
        RIGHT_THIGH,
        RIGHT_SHANK,
        RIGHT_ANKLE,
        RIGHT_FOOT,
        NUM_BODY_PARTS,
        INVALID_BODY_PART,
    };

    using namespace itandroids_lib::math;

/**
 * Class that represents the body part properties.
 */
    class BodyPartProperties {
    public:
        std::string name;
        BodyPartType type;
        BodyPartType parentType;
        Vector3<double> translation;
        double mass;
        BoundingBox boundingBox;
        HingeJointProperties joint;
        std::vector<BodyPartType> children;

        /**
         * Default constructor of BodyPartProperties.
         */

        BodyPartProperties();

        /** Constructor of BodyPartProperties.
         *
         * @param name the name of the body part.
         * @param type the type of the body part.
         * @param parentType the parent type of the body part.
         * @param translation the translation of the body part.
         * @param mass the mass of body part.
         * @param boundingBox the boundingBox of body part.
         * @param jointProperties the properties of hinge joint from the body part.
         */


        BodyPartProperties(std::string name, BodyPartType type,
                           BodyPartType parentType, Vector3<double> translation, double mass,
                           BoundingBox boundingBox, HingeJointProperties jointProperties);
    };

} /* namespace modeling */

#endif /* SOURCE_MODELING_BODYPARTPROPERTIES_H_ */
