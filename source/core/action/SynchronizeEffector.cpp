/*
 * SynchronizeEffector.cpp
 *
 *  Created on: Sep 30, 2015
 *      Author: mmaximo
 */

#include "action/SynchronizeEffector.h"

namespace action {

    SynchronizeEffector::SynchronizeEffector() {

    }

    SynchronizeEffector::~SynchronizeEffector() {
    }

    std::string SynchronizeEffector::toString() {
        return "(syn)";
    }

} /* namespace action */
