/*
 * Behavior.h
 *
 *  Created on: Oct 30, 2015
 *      Author: itandroids
 */

#ifndef SOURCE_DECISION_MAKING_BEHAVIOR_H_
#define SOURCE_DECISION_MAKING_BEHAVIOR_H_

#include <memory>

#include "control/MovementRequest.h"
#include "modeling/Modeling.h"
#include "math/Vector3.h"
#include "control/KeyframeRequest.h"


using namespace itandroids_lib::math;

namespace decision_making {

    namespace behavior {
        class BehaviorFactory;

        class Behavior {
        public:
            /**
             * Default constructor.
             */
            Behavior();

            /**
             * Assignment constructor.
             *
             * @param pointer to Behavior Factory.
             */
            Behavior(BehaviorFactory *behaviorFactory);

            /**
             * Destructor.
             */
            virtual ~Behavior();

            /**
             * This function controls the agent's basic actions.
             *
             * @param modeling.
             */
            virtual void behave(modeling::Modeling &modeling);

            /**
             *  Virtual method for returning a movement request
             */
            virtual std::shared_ptr<control::MovementRequest> getMovementRequest();

            /**
             *  Saturate the robot walking velocity
             *
             *  @param walkVelocity vector with the velocity to be saturated
             */
            static Vector3<double> saturateWalkVelocity(Vector3<double> walkVelocity);

        protected:
            std::shared_ptr<control::MovementRequest> movementRequest;
            BehaviorFactory *behaviorFactory;
        };

    } /* namespace behavior */
} /* namespace decision_making */

#endif /* SOURCE_DECISION_MAKING_BEHAVIOR_H_ */
