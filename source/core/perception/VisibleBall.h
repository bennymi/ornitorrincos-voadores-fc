/*
 * VisibleBall.h
 *
 *  Created on: Sep 1, 2015
 *      Author: itandroids
 */

#ifndef SOURCE_PERCEPTION_VISIBLEBALL_H_
#define SOURCE_PERCEPTION_VISIBLEBALL_H_

#include "VisibleObject.h"

namespace perception {

    class VisibleBall : public VisibleObject {
    public:
        VisibleBall();

        /**
         * Constructor for VisibleBall.
         *
         * @param node part of the parser tree that represents
         *             a visible ball.
         */
        VisibleBall(utils::parser::ParserTreeNode &);

        /**
         * Destructor
         */
        virtual ~VisibleBall();
    };

} /* namespace perception */

#endif /* SOURCE_PERCEPTION_VISIBLEBALL_H_ */
