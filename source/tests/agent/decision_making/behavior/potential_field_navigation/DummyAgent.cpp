//
// Created by mmaximo on 20/06/16.
//

#include "base/BaseAgentTest.h"

class DummyAgent : public BaseAgentTest {
private:


public:
    DummyAgent() : BaseAgentTest("127.0.0.1", 3100, 3200, 2, 0,
                                 "Dummy") {

    }

    virtual void testLoop(int nSteps) {
        setup();
        wiz.setPlayMode(representations::RawPlayMode::PLAY_ON);
        for (currentStep = 0; currentStep < nSteps; currentStep++) {
            communication.receiveMessage();
            perception.perceive(communication);
            modeling.model(perception, control);

            //step
            runStep();

            control.control(perception, modeling, decisionMaking);
            action.act(decisionMaking, control);
            communication.sendMessage(action.getServerMessage());
            elapsedTime += STEP_DT;
        }
        finish();
    }

    virtual void setup() {
        wiz.setAgentPositionAndDirection(modeling.getAgentModel().getAgentNumber(), representations::PlaySide::LEFT,
                                         Vector3<double>(-1.0, -1.0, 0.3),
                                         itandroids_lib::math::MathUtils::degreesToRadians(0));
    }

    virtual void runStep() {
        decisionMaking.movementRequest = std::make_shared<control::WalkRequest>(Vector3<double>(0.3, 0.0, 0.0));
    }
};

int main() {
    DummyAgent t;
    t.testLoop(50000);
}