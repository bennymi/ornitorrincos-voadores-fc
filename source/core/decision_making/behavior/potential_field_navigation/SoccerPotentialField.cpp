//
// Created by mmaximo on 20/06/16.
//

#include "decision_making/behavior/potential_field_navigation/SoccerPotentialField.h"

namespace decision_making {
    namespace behavior {
        namespace potential_field_navigation {

            SoccerPotentialField::SoccerPotentialField(PotentialFieldParameters parameters) : parameters(parameters) {
            }

            Vector2<double> SoccerPotentialField::calculateFieldDirection(const Vector2<double> &selfPosition) {
                return potentialField.calculateDirection(selfPosition, objects);
            }

            void SoccerPotentialField::clear() {
                objects.clear();
            }

            void SoccerPotentialField::addOpponent(Vector2<double> opponentPosition, bool counterClockwise) {
                double rotationMultiplier = counterClockwise ? 1.0 : -1.0;
                auto opponentObject = PotentialFieldObject(opponentPosition, parameters.getOpponentRepulsion(),
                                                           PotentialFieldFunctions::SQUARE_INVERSE_FUNCTION);
                auto opponentRotationObject = PotentialFieldObject(opponentPosition,
                                                                   rotationMultiplier *
                                                                   parameters.getOpponentRepulsion(),
                                                                   PotentialFieldFunctions::ROTATION_SQUARE_INVERSE_FUNCTION);
                objects.push_back(opponentObject);
                objects.push_back(opponentRotationObject);
            }

            void SoccerPotentialField::addTeammate(Vector2<double> teammatePosition, bool counterClockwise) {
                double rotationMultiplier = counterClockwise ? 1.0 : -1.0;
                auto teammateObject = PotentialFieldObject(teammatePosition, parameters.getTeammateRepulsion(),
                                                           PotentialFieldFunctions::SQUARE_INVERSE_FUNCTION);
                auto teammateRotationObject = PotentialFieldObject(teammatePosition,
                                                                   rotationMultiplier *
                                                                   parameters.getTeammateRepulsion(),
                                                                   PotentialFieldFunctions::ROTATION_SQUARE_INVERSE_FUNCTION);
                objects.push_back(teammateObject);
                objects.push_back(teammateRotationObject);
            }

            void SoccerPotentialField::addOpponent(Vector2<double> opponentPosition) {
                auto opponentObject = PotentialFieldObject(opponentPosition, parameters.getOpponentRepulsion(),
                                                           PotentialFieldFunctions::INVERSE_FUNCTION);
                objects.push_back(opponentObject);
            }

            void SoccerPotentialField::addTeammate(Vector2<double> teammatePosition) {
                auto teammateObject = PotentialFieldObject(teammatePosition, parameters.getTeammateRepulsion(),
                                                           PotentialFieldFunctions::INVERSE_FUNCTION);
                objects.push_back(teammateObject);
            }

            void SoccerPotentialField::addXFieldBorder(Vector2<double> selfPosition) {
                Vector2<double> leftBorder = selfPosition;
                Vector2<double> rightBorder = selfPosition;
                leftBorder.x = -fieldDescription.FIELD_LENGTH / 2.0;
                rightBorder.x = fieldDescription.FIELD_LENGTH / 2.0;
                auto leftBorderObject = PotentialFieldObject(leftBorder, parameters.getFieldBorderRepulsion(),
                                                             PotentialFieldFunctions::INVERSE_FUNCTION);
                auto rightBorderObject = PotentialFieldObject(rightBorder, parameters.getFieldBorderRepulsion(),
                                                              PotentialFieldFunctions::INVERSE_FUNCTION);
                objects.push_back(leftBorderObject);
                objects.push_back(rightBorderObject);
            }

            void SoccerPotentialField::addYFieldBorder(Vector2<double> selfPosition) {
                Vector2<double> upperBorder = selfPosition;
                Vector2<double> lowerBorder = selfPosition;
                upperBorder.y = fieldDescription.FIELD_WIDTH / 2.0;
                lowerBorder.y = -fieldDescription.FIELD_WIDTH / 2.0;
                auto upperBorderObject = PotentialFieldObject(upperBorder, parameters.getFieldBorderRepulsion(),
                                                              PotentialFieldFunctions::INVERSE_FUNCTION);
                auto lowerBorderObject = PotentialFieldObject(lowerBorder, parameters.getFieldBorderRepulsion(),
                                                              PotentialFieldFunctions::INVERSE_FUNCTION);
                objects.push_back(upperBorderObject);
                objects.push_back(lowerBorderObject);
            }


            void SoccerPotentialField::addTargetDipole(Vector2<double> position, double angle) {
                addTargetDipole(position, angle, parameters.getTargetAttraction(), parameters.getTargetAttraction());
            }

            void SoccerPotentialField::addTarget(Vector2<double> position) {
                addTarget(position, parameters.getTargetAttraction(), parameters.getTargetAttraction());
            }

            void SoccerPotentialField::addTargetDipole(Vector2<double> position, double angle, double unitAttraction,
                                                       double inverseAttraction) {
                auto ballUnitObject = PotentialFieldObject(position, unitAttraction,
                                                           PotentialFieldFunctions::UNIT_VECTOR_FUNCTION);
                auto ballDipoleAttractiveObject = PotentialFieldObject(position, inverseAttraction,
                                                                       PotentialFieldFunctions::INVERSE_FUNCTION);
                auto ballDipoleRepulsiveObject = PotentialFieldObject(
                        position + Vector2<double>(cos(angle), sin(angle)) * parameters.getDipoleDisplacement(),
                        -inverseAttraction, PotentialFieldFunctions::INVERSE_FUNCTION);
                objects.push_back(ballUnitObject);
                objects.push_back(ballDipoleAttractiveObject);
                objects.push_back(ballDipoleRepulsiveObject);
            }

            void
            SoccerPotentialField::addTarget(Vector2<double> position, double unitAttraction, double inverseAttraction) {
                auto ballUnitObject = PotentialFieldObject(position, unitAttraction,
                                                           PotentialFieldFunctions::UNIT_VECTOR_FUNCTION);
                auto ballAttractiveObject = PotentialFieldObject(position, inverseAttraction,
                                                                 PotentialFieldFunctions::INVERSE_FUNCTION);

                objects.push_back(ballUnitObject);
                objects.push_back(ballAttractiveObject);
            }

            void SoccerPotentialField::addRandomField() {
                auto randomObject = PotentialFieldObject(Vector2<double>(), 1.0 / parameters.getRandomRatio(),
                                                         PotentialFieldFunctions::RANDOM_FUNCTION);
                objects.push_back(randomObject);
            }

        } /* namespace potential_field_navigation */
    } /* namespace behavior */
} /* namespace decision_making */