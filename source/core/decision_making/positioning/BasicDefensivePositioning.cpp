//
// Created by dicksiano on 27/05/16.
//

#include <core/utils/roboviz/Roboviz.h>
#include "BasicDefensivePositioning.h"


namespace decision_making {
    namespace positioning {
        using itandroids_lib::math::Pose2D;
        using modeling::FieldDescription;

        BasicDefensePositioning::BasicDefensePositioning() :
                delaunayPositioning(representations::ITAndroidsConstants::FORMATION_FOLDER) {
        }

        BasicDefensePositioning::~BasicDefensePositioning() {
        }

        itandroids_lib::math::Pose2D BasicDefensePositioning::getAgentPosition(modeling::Modeling &modeling) {
            Pose2D target;

            int agentNum = modeling.getAgentModel().getAgentNumber();
            const OtherPlayer &opponentToMark = chooseOpponent(modeling, agentNum);

            Vector2<double> markingPos = chosenOpponentMarkingPosition(modeling, opponentToMark);
            Vector2<double> delaunayPos = delaunayPositioning.getAgentPosition(modeling).translation;

            target = Pose2D(0.0, delaunayPos.x, delaunayPos.y);

//    if (opponentToMark.getPosition().translation.x < FieldDescription::FIELD_LENGTH / 4) {
//        target = Pose2D(0.0, markingPos.x, markingPos.y);
//    }
//
//    else if (opponentToMark.getPosition().translation.x > 3*FieldDescription::FIELD_LENGTH / 4) {
//        target = Pose2D(0.0, delaunayPos.x, delaunayPos.y);
//    }
//
//    else {
//        //linear interpolation between delaunay and marking pos
//        double scale = (opponentToMark.getPosition().translation.x + FieldDescription::FIELD_LENGTH / 4) /
//                       FieldDescription::FIELD_LENGTH / 2;
//        Vector2<double> interpPos = delaunayPos * scale + markingPos * (1 - scale) ;
//        target = Pose2D(0.0, interpPos.x, interpPos.y);
//    }

            return target;
        }

        Vector2<double> BasicDefensePositioning::chosenOpponentMarkingPosition(modeling::Modeling &modeling,
                                                                               const OtherPlayer &opponent) {
            Vector2<double> target;
            Vector2<double> ballPos = modeling.getWorldModel().getBall().getPosition().translation;
            Vector2<double> oppPos = opponent.getPosition().translation;
            const OtherPlayer &opponentClosestFromBall = modeling.getWorldModel().getOpponentClosestFromBall();

            if (opponent.getId() == opponentClosestFromBall.getId()) {
                target = ballPos;
            } else {
                target = (ballPos + oppPos) / 2.0;
            }

            return target;
        }

        const OtherPlayer &BasicDefensePositioning::chooseOpponent(modeling::Modeling &modeling, int agentNum) {
            // @todo find which opponent should be marked according to the world model
            std::vector<std::pair<double, OtherPlayer *> > distancesFromBall = modeling.getWorldModel().getOpponentsFromBall();

            std::vector<std::pair<double, OtherPlayer *> > distancesFromGoal = modeling.getWorldModel().getOpponentsFromOurGoal();


            std::vector<std::pair<double, OtherPlayer *> > markCostFunction;
            double alpha = 1.0, beta = 1.0;
            for (auto &distFromBall: distancesFromBall) {
                double rescaleDistanceFromBall = distFromBall.first / FieldDescription::FIELD_LENGTH;
                for (auto &distFromGoal: distancesFromGoal) {
                    if (distFromGoal.second->getId() == distFromBall.second->getId()) {
                        std::pair<double, OtherPlayer *> playerCostFunction;
                        double rescaleDistanceFromGoal =
                                distFromGoal.first / (sqrt(pow(FieldDescription::FIELD_WIDTH, 2) +
                                                           pow(FieldDescription::FIELD_LENGTH, 2)));

                        playerCostFunction.first = alpha * rescaleDistanceFromBall + beta * rescaleDistanceFromGoal;
                        playerCostFunction.second = distFromGoal.second;
                        markCostFunction.push_back(playerCostFunction);
                    }
                }
            }
            qsort(&markCostFunction[0], markCostFunction.size(), sizeof(std::pair<double, OtherPlayer *>),
                  modeling::WorldModel::sortFunction);
            std::vector<OtherPlayer *> teammates = modeling.getWorldModel().getTeammates();
            for (auto &elem: markCostFunction) {
                double distanceSelfOpponent = (modeling.getWorldModel().getSelf().getPosition() -
                                               elem.second->getPosition()).abs();
                for (auto &teammate: teammates) {
                    double distanceTeamMateOpponent = (teammate->getPosition() - elem.second->getPosition()).abs();
                    if (distanceSelfOpponent < distanceTeamMateOpponent) {
                        //std::cout << elem.second->getId() << ": " << elem.first << endl;
                        return *elem.second;
                    }
                }
            }
            return *modeling.getWorldModel().getOpponentsFromSelf().front().second;
        }

    } /* namespace positioning */
} /* namespace decision_making */