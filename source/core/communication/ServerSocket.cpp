/*
 * ServerSocket.cpp
 *
 *  Created on: Aug 7, 2015
 *      Author: mmaximo
 */

#include "communication/ServerSocket.h"

#include <cstring>

namespace communication {

    const int ServerSocketImpl::BUFFER_SIZE;

    ServerSocket::ServerSocket() {
    }

    ServerSocket::~ServerSocket() {
    }

    ServerSocketImpl::ServerSocketImpl(std::string host, int port) :
            host(host), port(port), socketFileDescriptor(-1) {
        FD_ZERO(&socketSet);
        timeout.tv_sec = 0;
        timeout.tv_usec = 0;
    }

    ServerSocketImpl::~ServerSocketImpl() {
    }

    bool ServerSocketImpl::establishConnection() {
        socketFileDescriptor = socket(AF_INET, SOCK_STREAM, 0); // File descriptor
        serverAddress.sin_addr.s_addr = inet_addr(
                host.c_str()); // Convert the host address from numbers-and-dots notation in CP into binary data in network byte order
        serverAddress.sin_family = AF_INET; // Defines address family
        serverAddress.sin_port = htons(port); // Defines the door

        int c = connect(socketFileDescriptor, (struct sockaddr *) &serverAddress,
                        sizeof(serverAddress)); // Open a connection on socket FD r at server address
        FD_SET(socketFileDescriptor, &socketSet);

        return c;
    }

    void ServerSocketImpl::closeConnection() {
        close(socketFileDescriptor); // Close connection
    }

    std::string ServerSocketImpl::receiveMessage() {
        int length = 0;

        int messageRet;
        // Decode the message
        do {
            messageRet = recv(socketFileDescriptor, receiveBuffer, 4, 0);

            //connection closed
            if (messageRet == -1) {
                connectionTerminated = true;
                return std::string();
            }

            length = receiveBuffer[0];
            length = length << 8 | receiveBuffer[1];
            length = length << 8 | receiveBuffer[2];
            length = length << 8 | receiveBuffer[3];

            messageRet = recv(socketFileDescriptor, receiveBuffer, length, 0);
            //connection closed
            if (messageRet == -1) {
                connectionTerminated = true;
                return std::string();
            }
        } while (select(socketFileDescriptor + 1, &socketSet, (fd_set *) 0,
                        (fd_set *) 0, &timeout) > 0);
        return std::string(receiveBuffer, receiveBuffer + length);
    }

    void ServerSocketImpl::sendMessage(const std::string &message) {
        int length = message.length();

        // Encode the message in a sequence of bytes
        sendBuffer[0] = (length >> 24) & 0xFF;
        sendBuffer[1] = (length >> 16) & 0xFF;
        sendBuffer[2] = (length >> 8) & 0xFF;
        sendBuffer[3] = length & 0xFF;
        memcpy(&(sendBuffer[4]), message.c_str(), message.length()); // Copy the message bytes to the buffer
        int numBytes = 4 + message.length(); // The number of bytes is 4 bytes (buffer) plus the message length.
        int flag = 1;
        setsockopt(socketFileDescriptor, IPPROTO_TCP, TCP_NODELAY, (char *) &flag, sizeof(int));
//        send(socketFileDescriptor, sendBuffer, numBytes, 0); // Send the message
        write(socketFileDescriptor, sendBuffer, numBytes);
    }

} /* namespace communication */
