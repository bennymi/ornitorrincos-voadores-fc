/*
 * MovementRequest.h
 *
 *  Created on: Oct 1, 2015
 *      Author: mmaximo
 */

#ifndef SOURCE_CONTROL_MOVEMENTREQUEST_H_
#define SOURCE_CONTROL_MOVEMENTREQUEST_H_

#include "representations/NaoJoints.h"
#include "MovementRequestType.h"

namespace control {

    class MovementRequest {
    public:
        MovementRequest(MovementRequestType::MOVEMENT_REQUEST_TYPE requestType);

        virtual ~MovementRequest();

        MovementRequestType::MOVEMENT_REQUEST_TYPE getMovementType();


    protected:
        MovementRequestType::MOVEMENT_REQUEST_TYPE requestType;
    };

} /* namespace control */

#endif /* SOURCE_CONTROL_MOVEMENTREQUEST_H_ */
