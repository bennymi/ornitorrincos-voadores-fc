/*
 * VisibleFlag.h
 *
 *  Created on: Sep 1, 2015
 *      Author: itandroids
 */

#ifndef SOURCE_PERCEPTION_VISIBLEFLAG_H_
#define SOURCE_PERCEPTION_VISIBLEFLAG_H_

#include "VisibleObject.h"
#include "representations/FlagType.h"
//#include "perception/FlagType.h"

namespace perception {

    class VisibleFlag : public VisibleObject {
    public:
        VisibleFlag();

        VisibleFlag(representations::FlagType::FLAG_TYPE type, Vector3<double> position);

        VisibleFlag(utils::parser::ParserTreeNode &);

        virtual ~VisibleFlag();

        representations::FlagType::FLAG_TYPE getFlagType();

    private:
        representations::FlagType::FLAG_TYPE flagType;
    };

} /* namespace perception */

#endif /* SOURCE_PERCEPTION_VISIBLEFLAG_H_ */
