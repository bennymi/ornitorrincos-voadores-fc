//
// Created by francisco on 6/20/16.
//

#include "SoccerPlayer.h"
#include "utils/command_line_parser/AgentCommandLineParser.h"

using utils::command_line_parser::AgentCommandLineParser;

int main(int argc, char *argv[]) {
    AgentCommandLineParser parser;
    parser.parse(argc, argv);
    agent::SoccerPlayer player(parser.getServerIP(), parser.getServerPort(), parser.getTeamName(), parser.getAgentNumber());
    player.play();

}
