//
// Created by francisco on 19/12/16.
//

#include "NavigationParamsNodeCreator.h"

namespace utils {
namespace configure_tree {
namespace representations {
    template<>
    std::shared_ptr<Parameter<pt::ptree, std::string>> NavigationParamsNodeCreator::generateNode() {

        auto navigationParamsNode = std::make_shared<BranchParameter<pt::ptree, std::string>>();
        auto normalNavigationParamsNode = std::make_shared<LeafParameter<pt::ptree, std::string>>();
        boost::property_tree::read_json(
                "../configuration/core/representations/navigation_params/normal-parameters.json",
                normalNavigationParamsNode->getContent());
        navigationParamsNode->addChild("normalParams", normalNavigationParamsNode);
        return navigationParamsNode;
    };
}
}
}