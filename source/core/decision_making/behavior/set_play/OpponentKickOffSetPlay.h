//
// Created by dicksiano on 8/14/16.
//

#ifndef ITANDROIDS_SOCCER3D_CPP_OPPONENTKICKOFFSETPLAY_H
#define ITANDROIDS_SOCCER3D_CPP_OPPONENTKICKOFFSETPLAY_H

#include "../SetPlay.h"

namespace decision_making {
    namespace behavior {

        class OpponentKickOffSetPlay : public SetPlay {
        public:

            /**
            * Default constructor.
            */
            OpponentKickOffSetPlay();

            /**
            * Assignment constructor.
            *
            * @param pointer to BehaviorFactory.
            */
            OpponentKickOffSetPlay(BehaviorFactory *behaviorFactory);

            /**
            * Destructor.
            */
            virtual ~OpponentKickOffSetPlay();

            /**
            * This function controls the actions.
            *
            * @param modeling.
            */
            void behave(modeling::Modeling &modeling);

        private:

        };

    } /* namespace behavior */
} /* namespace decision_making */



#endif //ITANDROIDS_SOCCER3D_CPP_OPPONENTKICKOFFSETPLAY_H
