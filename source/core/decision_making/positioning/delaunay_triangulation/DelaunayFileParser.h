/*
 * DelaunayFileParser.h
 *
 *  Created on: Oct 28, 2015
 *      Author: itandroids
 */

#ifndef SOURCE_DECISION_MAKING_DELAUNAY_TRIANGULATION_DELAUNAYFILEPARSER_H_
#define SOURCE_DECISION_MAKING_DELAUNAY_TRIANGULATION_DELAUNAYFILEPARSER_H_

#include <iostream>
#include <fstream>
#include <boost/algorithm/string.hpp>
#include "geometry/DelaunayTriangulation.h"
#include <stdlib.h>

namespace decision_making {

    namespace positioning {

        namespace delaunay_triangulation {

            using namespace itandroids_lib::geometry;

            /**
             * Class that parses the configuration files wih predetermined positions.
             * This predetermined positions are used to interpolate new positions according to ball's position.
             */
            class DelaunayFileParser {
            public:
                /**
                 * Constructor to Delaunay File Parser
                 *
                 * @param string with the path to the configuration files.
                 */
                DelaunayFileParser(std::string filePath);

                /**
                 * Destructor to Delaunay File Parser
                 */
                virtual ~DelaunayFileParser();

                /**
                 * Getter to positions
                 *
                 * @return vector of Vector2D with the ball's positions
                 */
                std::vector<Vector2D> getPositions();

                /**
                 * Getter to predefined positions
                 *
                 * @return all predefined player's positions
                 */
                std::vector<std::vector<Vector2D> > getPredefinedPositions();

            private:
                std::vector<Vector2D> positions; // Predefined ball's positions
                std::vector<std::vector<Vector2D> > predefinedPositions; // Predefined player's positions
            };

        } /* namespace decision_making */
    } /* namespace positioning */
} /* namespace delaunay_triangulation */

#endif /* SOURCE_DECISION_MAKING_DELAUNAY_TRIANGULATION_DELAUNAYFILEPARSER_H_ */
