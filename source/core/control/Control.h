/*
 * Control.h
 *
 *  Created on: Aug 7, 2015
 *      Author: mmaximo
 */

#ifndef SOURCE_CONTROL_CONTROL_H_
#define SOURCE_CONTROL_CONTROL_H_

#include <math.h>
#include "math/Pose2D.h"
#include "representations/NaoJoints.h"
#include "perception/Perception.h"
#include "control/MovementRequestType.h"
#include "decision_making/DecisionMaking.h"
#include "control/JointsController.h"
#include "control/walking/OmnidirectionalWalkZMP.h"
#include "control/WalkRequest.h"
#include "control/keyframe/KeyframePlayer.h"
#include "control/kick/KickZMP.h"
#include "control/keyframe/KeyframeType.h"
#include "control/KeyframeRequest.h"
#include "representations/RobotType.h"
#include "control/ControlStateMachine.h"
#include "utils/patterns/Parameter.h"
#include "utils/patterns/Configurable.h"

using itandroids_lib::utils::patterns::Configurable;
using itandroids_lib::utils::patterns::Parameter;

namespace modeling {
    class Modeling;
}


namespace control {


    using namespace representations;

    class Control {
    public:
        //// todo document this code
        Control();

        virtual ~Control();

        /**
         * Get the odometry of the walk
         */
        virtual itandroids_lib::math::Pose2D getWalkOdometry() = 0;

        /**
         *
         */
        virtual NaoJoints &getJointsCommands() = 0;
    };

    class ControlImpl : public Control, public Configurable<ControlImpl> {
    public:
        // hack! Control (abstract) constructor should receive a type parameter too!
        /**
         * Depracated approach to a constructor, shouldn't be used
         * @param robotType
         * @return
         */
        ControlImpl(int robotType);

        /**
         * A constructor with a better approach, using a enum instead of an int
         * @param robotType
         * @return
         */
        ControlImpl(RobotType::ROBOT_TYPE robotType);

        /**
         * New Contructor that uses the configuration tree
         */
        template<class Content, class Key>
        ControlImpl(RobotType::ROBOT_TYPE robotType,
                    Parameter<Content, Key> &parameter): ControlImpl(robotType) {
            setParameter(parameter);
        }


        virtual ~ControlImpl();

        /**
         * Returns the Commands to be sent
         */
        virtual NaoJoints &getJointsCommands();

        void updateKeyframeMotionPlayer(std::string folderPath);

        /**
         * @return the keyframePlayer
         */
        control::keyframe::KeyframePlayer &getKeyframePlayer();

        /**
         * Function that sets the jointCommands and the odometry
         */
        void control(perception::Perception &perception,
                     modeling::Modeling &modeling,
                     decision_making::DecisionMaking &decisionMaking);

        /**
         * @return the odometry of the robot
         */
        itandroids_lib::math::Pose2D getWalkOdometry();

        /**
         * sets the walking parameters
         */

        void setWalkingParameters(const control::walking::OmnidirectionalWalkZMPParams &params);

        /**
         * Separates the parameter next nodes in its specific parts
         */
        template<class Content, class Key>
        void setParameter(Parameter<Content, Key> &parameter) {
            if (parameter.hasChild()) {
                for (auto &child: parameter.getChildren()) {
                    if (child.first == "stateMachine")
                        stateMachine.setParameter(*child.second);
                }
            }
        }

    private:

        /**
         * A state machine for control management, where the types of movements are the states
         */
        ControlStateMachine stateMachine;
        /**
         *
         */
        NaoJoints jointsPositions;

        /**
         *
         */
        control::JointsController *controller;

        /**
         * Handles a look request by setting the head joint positions
         */
        void handleLookRequest(std::shared_ptr<LookRequest> lookRequest);

        /*
         * Handles a movement request by setting all the joints of the robot
         */
        void handleMovementRequest(std::shared_ptr<MovementRequest> movementRequest,
                                   perception::Perception &perception);


    };

} /* namespace control */

#endif /* SOURCE_CONTROL_CONTROL_H_ */
