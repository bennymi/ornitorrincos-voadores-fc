//
// Created by muzio on 17/05/16.
//

#include "OdometryModel.h"
#include "math/MathUtils.h"

namespace modeling {
    namespace state_estimation {

        OdometryModel::OdometryModel(double odometryXSigma, double odometryYSigma, double odometryPsiSigma) :
                odometryXDistribution(MathUtils::getNormalRandomGenerator(0.0, odometryXSigma)),
                odometryYDistribution(MathUtils::getNormalRandomGenerator(0.0, odometryYSigma)),
                odometryPsiDistribution(MathUtils::getNormalRandomGenerator(0.0, odometryPsiSigma)) {

            alpha = 0.7;
            beta = 0.9;
            gama = 0.77;
        }

        OdometryModel::~OdometryModel() {
        }

        void OdometryModel::propagate(Particle *particles, int numParticles, const Pose2D &odometry) {
            Pose2D realOdometry = odometryModel(odometry);

            Pose2D newParticlePose;
            for (int i = 0; i < numParticles; ++i) {
                const Pose2D &particlePose = particles[i].getPose();

                newParticlePose.translation.x = particlePose.translation.x
                                                + realOdometry.translation.x * cos(particlePose.rotation)
                                                - realOdometry.translation.y * sin(particlePose.rotation)
                                                + odometryXDistribution();
                newParticlePose.translation.y = particlePose.translation.y
                                                + realOdometry.translation.x * sin(particlePose.rotation)
                                                + realOdometry.translation.y * cos(particlePose.rotation)
                                                + odometryYDistribution();
                newParticlePose.rotation = MathUtils::normalizeAngle(
                        particlePose.rotation + realOdometry.rotation
                        + odometryPsiDistribution());

                particles[i].setPose(newParticlePose);
            }
        }

        Pose2D OdometryModel::odometryModel(const Pose2D &odometry) {
            return Pose2D(gama * odometry.rotation,
                          alpha * odometry.translation.x,
                          beta * odometry.translation.y);
        }


    }
}
