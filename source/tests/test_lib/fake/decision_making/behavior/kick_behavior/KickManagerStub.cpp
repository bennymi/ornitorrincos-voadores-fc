//
// Created by francisco on 6/19/16.
//

#include "KickManagerStub.h"

namespace decision_making {
    namespace behavior {
        namespace kick_behavior {

            void KickManagerStub::setKick(std::shared_ptr<Kick> kick) {
                selectedKick = kick;
            }

            std::shared_ptr<Kick> KickManagerStub::selectKick(double distance, double direction) {
                return selectedKick;
            }


        }
    }
}


