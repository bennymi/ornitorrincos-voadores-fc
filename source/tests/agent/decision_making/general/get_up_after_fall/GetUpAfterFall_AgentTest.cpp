#include <tests/test_lib/utils/DrawVisibleObjects.h>
#include <tests/test_lib/fake/decision_making/RoleStrategyStub.h>
#include "base/BaseAgentTest.h"

using namespace itandroids_lib::math;

class GetUpAfterFall_AgentTest : public BaseAgentTest {
private:
    Pose2D pose;
    Vector3<double> groundTruth;
    double rotation;
    Vector3<double> position;
    std::shared_ptr<decision_making::RoleStrategy> roleStrategy;
    decision_making::role::RoleStub *stub;
    decision_making::DecisionMakingImpl decisionMaking;

public:
    virtual void testLoop(int nSteps) {
        setup();
        wiz.setPlayMode(representations::RawPlayMode::PLAY_ON);
        for (currentStep = 0; currentStep < nSteps; currentStep++) {
            communication.receiveMessage();
            perception.perceive(communication);
            modeling.model(perception, control);

            //step
            runStep();


            control.control(perception, modeling, decisionMaking);
            action.act(decisionMaking, control);
            communication.sendMessage(action.getServerMessage());
            elapsedTime += STEP_DT;
        }
        finish();
    }

    virtual void setup() {
        roleStrategy = std::make_shared<decision_making::RoleStrategyStub>();
        decisionMaking = decision_making::DecisionMakingImpl(roleStrategy);
        position = Vector3<double>(0.0, 0.0, 0.0);
        wiz.setAgentPositionAndDirection(1, representations::PlaySide::LEFT, Vector3<double>(-10, 0, 0.35), 0);
        stub = dynamic_cast<decision_making::role::RoleStub *>(roleStrategy->getRole(modeling));

    };

    virtual void runStep() {
        /**
         * Part of the code that decides what the robot will do
         */
        stub->movementRequest = std::make_shared<control::WalkRequest>(Pose2D(0, 1.6, 0));
        std::cout << modeling.getAgentModel().hasFallen() << std::endl;
        decisionMaking.decide(modeling);
        pose = modeling.getWorldModel().getSelf().getPosition();
        groundTruth = wiz.getAgentTranslation();
        rotation = wiz.getAgentRotation().getZAngle() - 90.0 * M_PI / 180.0;
        if (std::isnan(rotation))
            rotation = 0.0;
        std::string n1("animated.selfPosition");
        double radius = 0.5;
        roboviz.drawCircle(pose.translation.x, pose.translation.y, radius, 1.0,
                           0.0, 0.0, 1.0, &n1);
        roboviz.drawLine(pose.translation.x, pose.translation.y, 0.0,
                         pose.translation.x + radius * cos(pose.rotation),
                         pose.translation.y + radius * sin(pose.rotation), 0.0, 1.0, 0.0,
                         0.0, 1.0, &n1);
        DrawVisibleObjects drawVisibleObjects;
        drawVisibleObjects.draw(Pose2D(rotation, groundTruth.x, groundTruth.y), perception, modeling.getAgentModel(),
                                roboviz);
        std::string buffer("animated");
        if (perception.getAgentPerception().getVisibleObjects().size() != 0)
            roboviz.swapBuffers(&buffer);
    }
};

int main() {
    GetUpAfterFall_AgentTest t;
    t.testLoop(50000);
}

