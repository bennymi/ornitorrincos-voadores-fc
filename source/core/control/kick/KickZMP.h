/*
 * Kick.h
 *
 *  Created on: Oct 11, 2015
 *      Author: mmaximo
 */

#ifndef SOURCE_CONTROL_KICK_KICKZMP_H_
#define SOURCE_CONTROL_KICK_KICKZMP_H_

#include "math/Vector3.h"
#include "control/walking/InverseKinematics.h"
#include "utils/patterns/Configurable.h"


using itandroids_lib::utils::patterns::Configurable;
using itandroids_lib::utils::patterns::Parameter;

namespace control {
    namespace kick {

        using namespace control::walking;
        using namespace representations;
        using namespace itandroids_lib::math;

        enum KickZMPState {
            MOVING_ZMP_TO_SS,
            TAKING_FOOT_OFF_THE_GROUND,
            KICKING,
            PUTTING_FOOT_ON_THE_GROUND,
            MOVING_ZMP_TO_DS,
        };

        struct KickZMPParams : public Configurable<KickZMPParams> {
            double moveZmpDuration;
            double kickDuration;
            double takeFootDuration;
            double kickAmplitude;
            double zFoot;
            double accelerationMoveRatio;
            double ySeparation;
            double zCom;
            double armsCompensationFactor;

            KickZMPParams();

            template<class Content, class Key>
            void setParameter(Parameter<Content, Key> &parameter) {

                Content &content = parameter.getContent();
                for (boost::property_tree::ptree::value_type &value: content) {
                    if (value.first == "moveZmpDuration")
                        moveZmpDuration = std::stod(value.second.data());
                    else if (value.first == "zCom")
                        zCom = std::stod(value.second.data());
                    else if (value.first == "kickDuration")
                        kickDuration = std::stod(value.second.data());
                    else if (value.first == "takeFootDuration")
                        takeFootDuration = std::stod(value.second.data());
                    else if (value.first == "ySeparation")
                        ySeparation = std::stod(value.second.data());
                    else if (value.first == "armsCompensationFactor")
                        armsCompensationFactor = std::stod(value.second.data());
                    else if (value.first == "kickAmplitude")
                        kickAmplitude = std::stod(value.second.data());
                    else if (value.first == "accelerationMoveRatio")
                        accelerationMoveRatio = std::stod(value.second.data());
                    else if (value.first == "zFoot")
                        zFoot = std::stod(value.second.data());
                }
                // Set walk parameters

            }

            static KickZMPParams getDefaultKickZMPParams();
        };

        class KickZMP : public Configurable<KickZMP> {
        public:
            KickZMP(KickZMPParams params, bool left,
                    RobotPhysicalParameters physicalParams,
                    InverseKinematics *inverseKinematics);

            virtual ~KickZMP();

            void reset();

            void update(double elapsedTime, NaoJoints &jointsTargets);

            bool hasKicked();

            template<class Content, class Key>
            void setParameter(Parameter<Content, Key> &parameter) {

                // Set walk parameters
                kickParams.setParameter(parameter);

            }

        private:
            const double GRAVITY;

            KickZMPState state;

            KickZMPParams kickParams;
            bool leftLegKicks;
            RobotPhysicalParameters physicalParams;
            InverseKinematics *inverseKinematics;

            double totalDuration;
            double totalTime;
            double inStateTime;

            double tZMP;
            double aP;
            double aN;

            double a1;
            double a2;
            double b2;
            double a3;
            double b3;
            double c3;

            Vector3<double> torso;
            Vector3<double> kickFoot;
            double zKickFoot;

            Vector3<double> leftFoot;
            Vector3<double> rightFoot;
            double zLeft;
            double zRight;

            void changeState();

            void moveZmpToSS();

            void takeFootOffTheGround();

            void kick();

            void putFootOnTheGround();

            void moveZmpToDS();

            double computeTakeFrac(double t);

            double computeKickFrac(double t);

            void solveZMP(double zs, double z1, double z2, double x1, double x2,
                          double &aP, double &aN);

            double getCoM(double t, double aP, double aN, double zs, double z1,
                          double z2);

            Vector3<double> poseGlobal(Vector3<double> poseRelative,
                                       Vector3<double> pose);

            Vector3<double> poseRelative(Vector3<double> poseGlobal,
                                         Vector3<double> pose);

        };

    } /* namespace kick */
} /* namespace control */

#endif /* SOURCE_CONTROL_KICK_KICKZMP_H_ */
