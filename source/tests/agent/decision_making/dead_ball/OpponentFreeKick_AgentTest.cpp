//
// Created by dicksiano on 8/21/16.
//

#include "base/BaseAgentTest.h"

class OpponentFreeKick_AgentTest : public BaseAgentTest {

public:

    virtual void testLoop(int nSteps) { BaseAgentTest::testLoop(nSteps); }


    virtual void setup() {
        wiz.setPlayMode(representations::RawPlayMode::FREE_KICK_RIGHT);
        representations::FlagType flag;
        Vector3<double> flagPosition(modeling.getWorldModel().getFieldDescription().getFlagKnownPosition(flag.F1L));
        Vector3<double> delta(0.1, -0.1, 0);
        Vector3<double> starterBallPosition(0.0, 0.0, 0.0);
        wiz.setBallPosition(starterBallPosition);

        int agentNumber = 1;
        representations::PlaySide ourSide;
        Vector3<double> startPosition(-4.0, 0.0, 0.5);
        wiz.setAgentPositionAndDirection(agentNumber, ourSide.LEFT, startPosition, 45);

    }

    virtual void runStep() {


        wiz.setPlayMode(representations::RawPlayMode::FREE_KICK_RIGHT);
        utils::roboviz::Roboviz roboviz1;
        std::string name1("animated");
        std::string buff1("animated");
        roboviz1.swapBuffers(&buff1);

        Pose2D ballPosition = modeling.getWorldModel().getBall().getPosition();
        Pose2D ownGoalPosition = modeling.getWorldModel().getOwnGoalPosition();

        roboviz1.drawLine(ballPosition.translation.x, ballPosition.translation.y, 0, ownGoalPosition.translation.x,
                          ownGoalPosition.translation.y, 0, 1, 20, 20, 20, &name1);


        Pose2D vecPosition = (ownGoalPosition - ballPosition);
        vecPosition = vecPosition * (2.3 / vecPosition.abs());

        Pose2D strategicPosition = ballPosition + vecPosition;

        // Draw the desired position of the closest player from ball
        roboviz1.drawLine(strategicPosition.translation.x, strategicPosition.translation.y, 0,
                          strategicPosition.translation.x, strategicPosition.translation.y, 1, 1, 20, 20, 20, &name1);

        Pose2D selfPosition = modeling.getWorldModel().getSelf().getPosition();
        roboviz1.drawLine(selfPosition.translation.x, selfPosition.translation.y, 0,
                          selfPosition.translation.x, selfPosition.translation.y, 1,
                          1, 20, 20, 20, &name1);

        if (!decisionMaking.decideFall(modeling)) {
            behaviorFactory.getSetPlay().behave(modeling);
            decisionMaking.movementRequest = behaviorFactory.getSetPlay().getMovementRequest();

            behaviorFactory.getActiveVision().behave(modeling);
            decisionMaking.lookRequest = behaviorFactory.getActiveVision().getLookRequest();
        }
    }
};

int main() {

    OpponentFreeKick_AgentTest agent; // Agent test
    agent.testLoop(50000); // Run the test!
    return 0;
}
