/*
 * HingeJointProperties.h
 *
 *  Created on: Oct 4, 2015
 *      Author: mmaximo
 */

#ifndef SOURCE_MODELING_HINGEJOINTPROPERTIES_H_
#define SOURCE_MODELING_HINGEJOINTPROPERTIES_H_

#include "math/Vector3.h"
#include "representations/NaoJoints.h"

namespace modeling {

    using namespace itandroids_lib::math;
    using namespace representations;

/**
 * Class that represents the hinge joint properties: name, type, anchor, axis and the angle range.
 */
    class HingeJointProperties {
    public:
        std::string name;
        NaoJoints::HINGE_JOINTS type;
        Vector3<double> anchor;
        Vector3<double> axis;
        double minAngle;
        double maxAngle;

        /**
         * Default constructor of HingeJointProperties.
         */

        HingeJointProperties();

        /**
         * Constructor of HingeJointProperties.
         *
         * @param name the name of the hinge joint.
         * @param hingeJointType the type of the hinge joint.
         * @param anchor the anchor of the hinge joint.
         * @param axis the axis of the hinge joint.
         * @param minAngle minimum angle of the hinge joint.
         * @param maxAngle maximum angle of the hinge joint.
         */

        HingeJointProperties(std::string name,
                             NaoJoints::HINGE_JOINTS hingeJointType, Vector3<double> anchor,
                             Vector3<double> axis, double minAngle, double maxAngle);
    };

} /* namespace modeling */

#endif /* SOURCE_MODELING_HINGEJOINTPROPERTIES_H_ */
