/*
 * VisibleObject.cpp
 *
 *  Created on: Sep 1, 2015
 *      Author: itandroids
 */

#include "VisibleObject.h"

namespace perception {

    /*
     * Constructor to the case of no data.
     */
    VisibleObject::VisibleObject() {
        name = "unknown";
        position = Vector3<double>();
        objectType = perception::VisibleObjectType::UNKNOWN;

    }

    VisibleObject::VisibleObject(std::string name,
                                 perception::VisibleObjectType::VISIBLE_OBJECT_TYPE objectType,
                                 Vector3<double> position) :
            name(name), objectType(objectType), position(position) {
    }

    /**
     * This constructor takes a parser tree node and manipulates it to get information
     * about visible object (name and position).
     */
    VisibleObject::VisibleObject(utils::parser::ParserTreeNode &node) {
        // name = utils::parser::KeyValuePair(node.children[0]->content, ' ').value;
        name = node.content;
        setObjectType(name);
        std::string visibleObjectString = utils::parser::KeyValuePair(
                node.children[0]->content, ' ').value;
        boost::trim(visibleObjectString);
        std::vector<std::string> stringVector;
        boost::split(stringVector, visibleObjectString, boost::is_any_of(" "));
        Vector3<double> auxVector(
                MathUtils::degreesToRadians(atof(stringVector[1].c_str())),
                MathUtils::degreesToRadians(atof(stringVector[2].c_str())));
        position = Vector3<double>(atof(stringVector[0].c_str()), auxVector);
    }

    VisibleObject::~VisibleObject() {
    }

    std::string VisibleObject::getName() {
        return name;
    }

    Vector3<double> VisibleObject::getPosition() {
        return position;
    }

/**
 * get's the distance of the object to the camera
 * @return the distance that the object is from the camera
 */
    double VisibleObject::getDistance() {
        return position.abs();
    }

/**
 * get's the horizontal angle of the object in relation to the camera
 * @return an angle from 0 to 2pi
 */
    double VisibleObject::getHorizontalAngle() {
        return position.getAlpha();
    }

/**
 * get's the horizontal angle of the object in relation to the camera
 * @return an angle from 0 to 360
 */
    double VisibleObject::getHorizontalAngleDeg() {
        return itandroids_lib::math::MathUtils::radiansToDegrees(
                position.getAlpha());
    }

/**
 * get's the latitude angle of the object in relation to the camera
 * @return an angle from -pi to pi
 */
    double VisibleObject::getLatitudeAngle() {
        return position.getDelta();
    }

/**
 * get's the latitude angle of the object in relation to the camera
 * @return an angle from -180 to 180
 */
    double VisibleObject::getLatitudeAngleDeg() {
        return itandroids_lib::math::MathUtils::radiansToDegrees(
                position.getDelta());
    }

    void VisibleObject::setObjectType(std::string name) {
        boost::trim(name);
        if (name.compare("F1L") == 0 || name.compare("F2L") == 0
            || name.compare("F1R") == 0 || name.compare("F2R") == 0) {
            objectType = perception::VisibleObjectType::VISIBLE_FLAG;
        } else if (name.compare("B") == 0) {
            objectType = perception::VisibleObjectType::VISIBLE_BALL;
        } else if (name.compare("P") == 0) {
            objectType = perception::VisibleObjectType::VISIBLE_PLAYER;
        } else if (name.compare("L") == 0) {
            objectType = perception::VisibleObjectType::VISIBLE_LINE;
        } else if (name.compare("G1L") == 0 || name.compare("G2L") == 0
                   || name.compare("G1R") == 0 || name.compare("G2R") == 0) {
            objectType = perception::VisibleObjectType::VISIBLE_GOAL_POST;
        }
    }

    perception::VisibleObjectType::VISIBLE_OBJECT_TYPE &VisibleObject::getVisibleObjectType() {
        return objectType;
    }

    void VisibleObject::transformPosition(Pose3D &transform) {
        position = transform * position;
    }

} /* namespace perception */
