/*
 * Perception.cpp
 *
 *  Created on: Aug 7, 2015
 *      Author: mmaximo
 */

#include "perception/Perception.h"
#include "utils/Clock.h"

namespace perception {

    Perception::Perception() {

    }

    Perception::~Perception() {
    }

    /*
     * Getter method: returns the agent perception
     */
    perception::AgentPerception &Perception::getAgentPerception() {
        return agentPerception;
    }

    /*
     * perceive parses the message from communication, updates the perceptor factory and the agent perceptor
     * updates the information from the perceptors
     */
    void Perception::perceive(communication::Communication &communication) {
        using namespace std;
        utils::parser::ParserTreeNode *root = parser.parseMessage(
                communication.getCurrentMessage());
        double factoryUpdateBegin = itandroids_lib::utils::Clock::getTime();
        //updating perceptor factory
        perceptorFactory.update(*root);
        std::vector<perception::Perceptor *> &perceptors = perceptorFactory.getPerceptors();
        //updating perceptors info
        agentPerception.update(perceptors);
        delete root;
    }

} /* namespace perception */
