/*
 * RobotPhysicalParameters.cpp
 *
 *  Created on: Oct 10, 2015
 *      Author: mmaximo
 */

#include "RobotPhysicalParameters.h"

namespace control {
    namespace walking {

        RobotPhysicalParameters::RobotPhysicalParameters() :
                upperLegLength(0.0), lowerLegLength(0.0), hipPitch0(0.0), kneePitch0(
                0.0), anklePitch0(0.0), armLength(0.0) {
        }

        RobotPhysicalParameters RobotPhysicalParameters::getNaoPhysicalParameters() {
            RobotPhysicalParameters naoPhysicalParams;
            naoPhysicalParams.armLength = 0.02 + 0.07 + 0.11;
            double dzUpperLeg = 0.04 + 0.14 / 2.0 + (0.11 / 2.0 - 0.045);
            double dxUpperLeg = 0.005;
            naoPhysicalParams.upperLegLength = sqrt(
                    dxUpperLeg * dxUpperLeg + dzUpperLeg * dzUpperLeg);
            naoPhysicalParams.lowerLegLength = 0.045 + 0.055;
            naoPhysicalParams.hipPitch0 = -atan2(dxUpperLeg, dzUpperLeg);
            naoPhysicalParams.kneePitch0 = atan2(dxUpperLeg, dzUpperLeg);
            naoPhysicalParams.anklePitch0 = 0.0;
            naoPhysicalParams.torsoToLeftHip = Vector3<double>(-0.01, 0.055, -0.115);
            naoPhysicalParams.leftAnkleToLeftFoot = Vector3<double>(0.03, 0.0, -0.04);
            return naoPhysicalParams;
        }

    } /* namespace walking */
} /* namespace control */
