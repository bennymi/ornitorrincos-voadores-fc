/*
 * KeyframeRequest.h
 *
 *  Created on: Oct 24, 2015
 *      Author: itandroids
 */

#ifndef SOURCE_CONTROL_KEYFRAMEREQUEST_H_
#define SOURCE_CONTROL_KEYFRAMEREQUEST_H_

#include "control/MovementRequest.h"
#include "control/keyframe/KeyframeType.h"

namespace control {

    class KeyframeRequest : public MovementRequest {
    public:
        /**
         * Constructor for a keyframe requests, that runs the selected request and has a boolean to determine whether the movement is inverted or not
         */
        KeyframeRequest(std::string keyframeName, bool isInverted = false);

        /**
         * Default destructor
         */
        virtual ~KeyframeRequest();

        /**
         * Checks if the movement is inverted
         */
        bool isInverted();

        /**
         * Gets the movement name
         */
        std::string getName();

    private:
        std::string name;
        bool inverted;
    };

} /* namespace control */

#endif /* SOURCE_CONTROL_KEYFRAMEREQUEST_H_ */
