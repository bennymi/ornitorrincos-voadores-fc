//
// Created by jose on 27/08/16.
//

#ifndef ITANDROIDS_SOCCER3D_CPP_MESSAGEDATA_H
#define ITANDROIDS_SOCCER3D_CPP_MESSAGEDATA_H

#include <boost/algorithm/string.hpp>
#include <bitset>
#include <vector>

namespace representations {

    class MessageData {
    public:
        MessageData(double heardServerTime, std::string message);

        MessageData();

        /*
        * gets the IP Agent
        *
        * @return the IP Agent
        */
        int getHeardIPAgent();

        /*
        * gets the position X of the ball
        *
        * @return the position X of the ball
        */
        double getHeardBallPositionX();

        /*
        * gets the position Y of the ball
        *
        * @return the position y of the ball
        */
        double getHeardBallPositionY();

        /*
        * gets the heardBallTime
        *
        * @return the last time the ball has be seen
        */
        double getHeardBallTime();

        /*
        * gets the position X of the agent
        *
        * @return the position X of the agent
        */
        double getHeardPlayerPositionX();

        /*
        * gets the position Y of the agent
        *
        * @return the position Y of the agent
        */
        double getHeardPlayerPositionY();

        /*
        * gets if the agent has fallen
        *
        * @return the fFallen
        */
        bool ifHeardPlayerFallen();

        /*
         * gets the role assignment vector
         *
         * @return roleAssigment
         */
        std::vector<int> getHeardRoleAssignment();


    private:
        // roleAssigSize defines how much space we occupy for role assignment transmission
        static const int roleAssigSize;
        int uNum;
        double ballLastSeenServerTime;
        double ballX;
        double ballY;
        double agentX;
        double agentY;
        bool fFallen;
        double time;
        std::vector<int> roleAssigment;
        const std::string commAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
        std::string message;

        void stringToBits(const std::string &message, const std::vector<int> bits);

        void bitsToData(std::vector<int> bits, int uNum, std::vector<int> &roleAssigment);

        int bitsToInt(std::vector<int> &bits, const int &start, const int &end);

        std::vector<int> intToBits(std::vector<int> &bits, const int &n, unsigned long numBits);

        bool bitsToString(std::vector<int> &bits, std::string &message);

        std::vector<int> roleAssignmentVectorBits(const int &uNum, std::vector<int> roleAssignment);
        //std::bitset<114> dataCompression(std::vector<int> bits, std::bitset<114> compressedBits);

    };

} /* namespace representations */


#endif //ITANDROIDS_SOCCER3D_CPP_MESSAGEDATA_H
