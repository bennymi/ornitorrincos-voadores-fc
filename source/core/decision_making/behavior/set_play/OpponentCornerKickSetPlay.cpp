//
// Created by dicksiano on 8/14/16.
//

#include "OpponentCornerKickSetPlay.h"
#include "../BehaviorFactory.h"

namespace decision_making {
    namespace behavior {

        OpponentCornerKickSetPlay::OpponentCornerKickSetPlay() : SetPlay(nullptr) {
        }

        OpponentCornerKickSetPlay::OpponentCornerKickSetPlay(BehaviorFactory *behaviorFactory) : SetPlay(
                behaviorFactory) {
        }

        OpponentCornerKickSetPlay::~OpponentCornerKickSetPlay() {
        }

        void OpponentCornerKickSetPlay::behave(modeling::Modeling &modeling) {
            /// IMPLEMENTE
        } /* function behave */

    } /* namespace behavior */
} /* namespace decision_making */
