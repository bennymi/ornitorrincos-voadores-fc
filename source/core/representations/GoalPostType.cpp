/*
 * GoalPostType.cpp
 *
 *  Created on: Sep 6, 2015
 *      Author: mmaximo
 */

#include "GoalPostType.h"

namespace representations {

    GoalPostType::GOAL_POST_TYPE GoalPostType::identifyType(std::string goalPostName) {
        if (goalPostName.compare("G1L") == 0)
            return GoalPostType::G1L;
        else if (goalPostName.compare("G2L") == 0)
            return GoalPostType::G2L;
        else if (goalPostName.compare("G1R") == 0)
            return GoalPostType::G1R;
        else if (goalPostName.compare("G2R") == 0)
            return GoalPostType::G2R;
        else
            return GoalPostType::INVALID;
    }

} /* namespace representations */

