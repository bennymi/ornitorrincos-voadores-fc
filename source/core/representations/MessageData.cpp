//
// Created by jose on 27/08/16.
//

#include <core/modeling/WorldModel.h>
#include "MessageData.h"

namespace representations {

    const int MessageData::roleAssigSize = 128;
    std::vector<int> bits;

    MessageData::MessageData(double heardServerTime, std::string message) {
/**
 * Parses message body of hear perceptor (need to first strip off hear header, time, and self/direction).
 * message - message to process
 * heardServerTime, the server time the message was heard, NOT the game time
 * uNum - uniform of the agent who said the message
 * ballLastSeenServerTime - server time agent who said the message saw the ball, NOT the game time
 * ballX - reported global X position of the ball
 * ballY - reported global Y position of the ball
 * agentX - reported global X position of the agent who said the message
 * agentY - reported global Y position of the agent who said the message
 * fFallen - whether or not agent who said the message is currently fallen
 * time - time message was said
 * RETURN - true if valid message parsed, false otherwise
 */


        uNum = 0;
        ballLastSeenServerTime = 0;
        ballX = 0;
        ballY = 0;
        agentX = 0;
        agentY = 0;
        fFallen = false;
        time = 0;

        stringToBits(message, bits);

        bitsToData(bits, uNum, roleAssigment);

        // Update time
        time += double(int((heardServerTime - time) / 1310.72)) * 1310.72;
        ballLastSeenServerTime += double(int((heardServerTime - ballLastSeenServerTime) / 1310.72)) * 1310.72;

        /*
        if (heardServerTime-time >= .07 || heardServerTime-time < -.001) {
            return false;
        }
        */

        //Update cycle
        int cycles;
        cycles = int((time * 50) + 0.1);
        uNum = (cycles % (modeling::WorldModel::NUM_PLAYERS_PER_TEAM * 2)) / 2 + 1;
    }

    MessageData::MessageData() {}

    void MessageData::stringToBits(const std::string &message, std::vector<int> bits) {

        if (commAlphabet.size() != 64) {
            // Requires use of communication alphabet
            // Uses cerr instead of cout for error message, but no difference
            std::cerr << "string2bits: alphabet size not 64!\n";
        }

        // each char occupies 6 bits, so it resizes bits to fit it
        bits.resize(message.length() * 6);

        for (unsigned long i = 0; i < message.length(); i++) {

            // Make sure every letter in the message comes from our alphabet. Make a mapping.
            // If any violation, return false;
            const char c = message.at(i);
            unsigned long n = commAlphabet.find(c);
            // If n equals string::npos, the vector needs to be cleared for the next message
            if (n == std::string::npos) {
                bits.clear();
            }

            // Converts into bits the ASCII number for each char
            for (int j = 5; j >= 0; j--) {
                bits[i * 6 + j] = (int) (n % 2);
                n /= 2;
            }
        }

    }

    void MessageData::bitsToData(std::vector<int> bits, int uNum, std::vector<int> &roleAssigment) {

        /*
        if(bits.size() < (16 + 16 + 10 + 10 + 10 + 10 + 1)) {
            time = 0;
            ballLastSeenTime = 0;
            ballX = 0;
            ballY = 0;
            agentX = 0;
            agentY = 0;
            fFallen = false;
        }
         */

        /*
        int ctr = 0;

        // It converts the message into data for each element
        // Then it uses ctr to represent the inicial position for each data in bits
        int cycles = bitsToInt(bits, ctr, ctr + 15);
        time = cycles * 0.02;
        ctr += 16;

        int ballLastSeenCycles = bitsToInt(bits, ctr, ctr + 15);
        ballLastSeenTime = ballLastSeenCycles * 0.02;
        ctr += 16;

        int bx = bitsToInt(bits, ctr, ctr + 9);
        ballX = minBallX + ((maxBallX - minBallX) * (bx / 1023.0));
        ctr += 10;

        int by = bitsToInt(bits, ctr, ctr + 9);
        ballY = minBallY + ((maxBallY - minBallY) * (by / 1023.0));
        ctr += 10;

        int ax = bitsToInt(bits, ctr, ctr + 9);
        agentX = minAgentX + ((maxAgentX - minAgentX) * (ax / 1023.0));
        ctr += 10;

        int ay = bitsToInt(bits, ctr, ctr + 9);
        agentY = minAgentY + ((maxAgentY - minAgentY) * (ay / 1023.0));
        ctr += 10;

        fFallen = bits[ctr] != 0;
        ctr += 1;*/

        uNum = bitsToInt(bits, 0, 3);
        for (int i = 0; i < 11; i++) {
            int start = 4 + 5 * i;
            int end = 3 + 5 * (i + 1);
            roleAssigment[i] = bitsToInt(bits, start, end);
        }

    }

    int MessageData::bitsToInt(std::vector<int> &bits, const int &start, const int &end) {
        if (start < 0 || end >= (int) bits.size()) {
            return 0;//Error.
        }

        // Mathematical conversion from base 2 to base 10
        int n = 0;
        for (int i = start; i <= end; i++) {
            n *= 2;
            n += bits[i];
        }

        return n;
    }

    std::vector<int> MessageData::intToBits(std::vector<int> &bits, const int &n, unsigned long numBits) {
        // In case of invalid information
        if (n < 0) {
            bits.clear();
            return bits;
        }

        // Needs the use of the operator because n is const
        int m = n;

        // numBits is used to define how many bits the number will occupy
        bits.resize(numBits);

        for (unsigned long j = numBits - 1; j >= 0; j--) {
            bits[j] = m % 2;
            m /= 2;
        }

        return bits;
    }

    bool MessageData::bitsToString(std::vector<int> &bits, std::string &message) {

        message = "";
        if (MessageData::commAlphabet.size() != 64) {
            std::cerr << "In function bitsToString: alphabet size not 64!";
        }

        std::vector<int> index;


        index.resize((bits.size() + 5) / 6);
        // Used to go through the bits vector
        size_t ctr = 0;

        //Go through the index vector, converting the bits on integers relative to the ASCII characters.
        for (size_t i = 0; i < index.size(); i++) {
            index[i] = 0;
            for (int j = 0; j < 6; j++) {
                index[i] *= 2;

                if (ctr < bits.size()) {
                    index[i] += bits[ctr];
                    ctr++;
                }
            }
        }

        for (size_t i = 0; i < index.size(); i++)
            message += MessageData::commAlphabet.at((unsigned long) index[i]);

        return true;
    }


    std::vector<int>
    representations::MessageData::roleAssignmentVectorBits(const int &uNum, std::vector<int> roleAssignment) {

        int uNumSupport = uNum;

        // Keeps the role assignment decided by the agent in bit format
        std::vector<int> roleAssignmentBits;

        // Resize the vector for role assignment to keep 4 spaces for the uniform number (know which agent sent the message)
        // Then we keep 5 spaces for each integer in roleAssignment vector
        roleAssignmentBits.resize(4 + 5 * roleAssignment.size());

        for (int i = 3; i >= 0; i--) {
            roleAssignmentBits[i] = uNumSupport % 2;
            uNumSupport /= 2;
        }

        for (int i = 0; i < roleAssignment.size(); i++) {

            int roleOperator = roleAssignment[i];

            // starts at 4 because we already put the uniform number in the beginning
            int cts = 4;

            for (int j = 4 + cts; j >= cts; j--) {
                roleAssignmentBits[cts] = roleOperator % 2;
                roleOperator /= 2;
                cts++;
            }

        }

        return roleAssignmentBits;
    }

    int MessageData::getHeardIPAgent() {
        return uNum;
    }

    bool MessageData::ifHeardPlayerFallen() {
        return fFallen;
    }

    double MessageData::getHeardBallPositionX() {
        return ballX;
    }

    double MessageData::getHeardBallPositionY() {
        return ballY;
    }

    double MessageData::getHeardBallTime() {
        return ballLastSeenServerTime;
    }

    double MessageData::getHeardPlayerPositionX() {
        return agentX;
    }

    double MessageData::getHeardPlayerPositionY() {
        return agentY;
    }

    std::vector<int> MessageData::getHeardRoleAssignment() {
        return roleAssigment;
    }


}


