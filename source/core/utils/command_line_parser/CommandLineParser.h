//
// Created by francisco on 16/12/16.
//

#ifndef ITANDROIDS_SOCCER3D_CPP_COMMANDLINEPARSER_H
#define ITANDROIDS_SOCCER3D_CPP_COMMANDLINEPARSER_H

namespace utils{
    namespace command_line_parser{
        class CommandLineParser {
        public:
            /**
             *
             * @param argc number of arguments from command line
             * @param argv arguments from the command line
             */
            virtual void parse(int argc, char * argv[]) = 0;



        protected:

        private:
        };
    }
}



#endif //ITANDROIDS_SOCCER3D_CPP_COMMANDLINEPARSER_H
