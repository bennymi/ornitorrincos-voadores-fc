/*
 * FollowLine.h
 *
 *  Created on: Oct 31, 2015
 *      Author: itandroids
 */

#ifndef SOURCE_DECISION_MAKING_BEHAVIOR_FOLLOWLINE_H_
#define SOURCE_DECISION_MAKING_BEHAVIOR_FOLLOWLINE_H_

#include <memory>

#include "Behavior.h"
#include "math/Vector3.h"
#include "math/Vector2.h"
#include "math/Pose2D.h"
#include "modeling/Modeling.h"
#include <cmath>
#include "representations/OtherPlayer.h"
#include "control/PController.h"
#include "control/WalkRequest.h"
#include "decision_making/behavior/NavigationHelper.h"

using namespace itandroids_lib::math;

namespace decision_making {
    namespace behavior {
        class BehaviorFactory;

        class FollowLine : public Behavior {
        public:

            /**
             * Default constructor.
             */
            FollowLine();

            /**
             * Assignment constructor.
             *
             * @param behaviorFactory pointer to BehaviorFactory object.
             */
            FollowLine(BehaviorFactory *behaviorFactory);

            /**
             * Destructor.
             */
            virtual ~FollowLine();

            /**
             * Function to set a line.
             *
             * @param direction angle that indicates the direction of the line.
             * @param initialPoint initial point of the line.
             */
            virtual void setLine(double direction, itandroids_lib::math::Vector2<double> initialPoint);

            /**
             * Function to follow a line
             *
             * @param modeling.
             */
            virtual void behave(modeling::Modeling &modeling);

        private:
            itandroids_lib::control::PController yController;
            itandroids_lib::control::PController psiController;
            Vector2<double> lineVector;
            Vector2<double> initialPoint;
            // The line is aX + bY + c = 0
            double a;
            double b;
            double c;
            double getPointSide;
            NavigationHelper navigationHelper;
        };

    } /* namespace behavior */
} /* namespace decision_making */

#endif /* SOURCE_DECISION_MAKING_BEHAVIOR_FOLLOWLINE_H_ */
