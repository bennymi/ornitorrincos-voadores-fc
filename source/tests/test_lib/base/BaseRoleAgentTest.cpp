//
// Created by francisco on 6/12/16.
//

#include "BaseRoleAgentTest.h"

BaseRoleAgentTest::BaseRoleAgentTest(std::string host, int agentPort, int monitorPort, int agentNumber, int robotType,
                                     std::string teamName) :
        BaseAgentTest(host, agentPort, monitorPort, agentNumber, robotType, teamName) {

    roleStrategy = std::make_shared<decision_making::RoleStrategyStub>();
    decisionMaking = std::make_unique<decision_making::DecisionMakingImpl>(roleStrategy);
    role = dynamic_cast<decision_making::role::RoleStub *>(roleStrategy->getRole(modeling));
}

void BaseRoleAgentTest::testLoop(int numberOfSteps) {
    setup();
    wiz.setPlayMode(representations::RawPlayMode::PLAY_ON);
    for (currentStep = 0; currentStep < numberOfSteps; currentStep++) {
        communication.receiveMessage();
        perception.perceive(communication);
        modeling.model(perception, control);

        //step
        runStep();


        control.control(perception, modeling, *decisionMaking);
        action.act(*decisionMaking, control);
        communication.sendMessage(action.getServerMessage());
        elapsedTime += STEP_DT;
    }
    finish();
}





