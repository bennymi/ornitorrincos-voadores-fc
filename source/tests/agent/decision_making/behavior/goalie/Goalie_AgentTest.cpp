//
// Created by francisco on 14/02/16.
//

#include "Goalie_AgentTest.h"

Goalie_AgentTest::Goalie_AgentTest(std::string host, int agentPort, int monitorPort, int agentNumber) : BaseAgentTest(
        host, agentPort, monitorPort, agentNumber) {
}

int main() {

    Goalie_AgentTest agent;
    agent.testLoop(9000);
    return 0;
}
