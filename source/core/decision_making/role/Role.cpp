//
// Created by dicksiano on 27/05/16.
//

#include "Role.h"

namespace decision_making {
    namespace role {
        using itandroids_lib::math::Pose2D;

        Role::Role() : sayMessage(nullptr), lookRequest(nullptr), movementRequest(nullptr),
                       beamRequest(nullptr) {
        }

        Role::~Role() {
        }


        void Role::execute(modeling::Modeling &modeling) {
            reset();
            behaveMovement(modeling);
            behaveSee(modeling);
            behaveSay(modeling);
        }

        void Role::reset() {
            sayMessage = nullptr;
            lookRequest = nullptr;
            movementRequest = nullptr;
            beamRequest = nullptr;
        }

        void Role::behaveSee(modeling::Modeling &modeling) {
            //Put active vision if ball is relevant, else focus ball
            if (modeling.getWorldModel().getBall().isRelevant()) {
                behaviorFactory.getActiveVision().behave(modeling);
                lookRequest = behaviorFactory.getActiveVision().getLookRequest();
            } else {
                behaviorFactory.getFocusBall().behave(modeling);
                lookRequest = behaviorFactory.getFocusBall().getLookRequest();

                movementRequest = std::make_shared<control::WalkRequest>(
                        Pose2D(representations::NavigationParams::getNavigationParams().max_vpsi / 4, 0, 0));
            }
        }

        void Role::behaveSay(modeling::Modeling &modeling) {
            behaviorFactory.getSay().behave(modeling);
            sayMessage = behaviorFactory.getSay().getSayMessage();
        }


    } /* namespace role */
} /* namespace decision_making */