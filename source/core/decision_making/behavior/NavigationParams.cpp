//
// Created by mmaximo on 24/02/16.
//

#include "NavigationParams.h"

#include <math.h>

namespace decision_making {
    namespace behavior {

        NavigationParams NavigationParams::getDefaultNavigationParams() {
            NavigationParams navigationParams;
            navigationParams.maxVforward = 1.2;
            navigationParams.maxVbackward = 0.3;
            navigationParams.maxVy = 0.3;
            navigationParams.maxVpsi = 200.0 * M_PI / 180.0;
            navigationParams.closeDistanceThreshold = 1.0;
            navigationParams.lowVpsiFactor = 0.25;
            navigationParams.lowVforwardFactor = 0.5;
            navigationParams.lowVbackwardFactor = 0.5;
            navigationParams.lowVyFactor = 0.5;
            navigationParams.velocityGain = 1.0;
            return navigationParams;
        }

        double NavigationParams::getMaxVforward() {
            return maxVforward;
        }

        double NavigationParams::getMaxVbackward() {
            return maxVbackward;
        }

        double NavigationParams::getMaxVy() {
            return maxVy;
        }

        double NavigationParams::getMaxVpsi() {
            return maxVpsi;
        }

        double NavigationParams::getCloseDistanceThreshold() {
            return closeDistanceThreshold;
        }

        double NavigationParams::getLowVpsiFactor() {
            return lowVpsiFactor;
        }

        double NavigationParams::getLowVforwardFactor() {
            return lowVforwardFactor;
        }

        double NavigationParams::getLowVbackwardFactor() {
            return lowVbackwardFactor;
        }

        double NavigationParams::getLowVyFactor() {
            return lowVyFactor;
        }

        double NavigationParams::getVelocityGain() {
            return velocityGain;
        }

    } /* namespace behavior */
} /* namespace decision_making */