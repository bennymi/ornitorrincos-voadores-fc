//
// Created by muzio on 17/05/16.
//

#ifndef SOURCE_MODELING_STATE_ESTIMATION_OBSERVATIONMODEL_H_
#define SOURCE_MODELING_STATE_ESTIMATION_OBSERVATIONMODEL_H_

#include "Particle.h"
#include "LocalizationRepresentations.h"
#include "geometry/Line2D.h"
#include <vector>
#include <core/representations/NaoJoints.h>

namespace modeling {
    namespace state_estimation {

        using itandroids_lib::geometry::Line2D;

        class ObservationModel {
        public:
            ObservationModel(double distanceVariance, double bearingVariance);

            double calculateObservationLogProbabilityFromLandmarks(const Particle &particle,
                                                                   const std::vector<LandmarkObservation> &landmarkObservations);

            double calculateObservationLogProbabilityFromLines(const Particle &particle,
                                                               const std::vector<Line2D> &lineObservations);

            //not being used yet
            double calculateObservationLogProbabilityFromNegativeInfo(const Particle &particle,
                                                                      const std::vector<LandmarkObservation> &observations);

            void updateWithEstimate(const std::vector<Line2D> &observedLines, const Pose2D &estimate,
                                    const representations::NaoJoints &jointsPosition);

        private:
            double distanceVariance;
            double bearingVariance;

            std::vector<Line2D> actualLines;
            std::vector<Line2D> possibleActualLines;

            double distancesBetweenActualAndObservedLines[30][30];
            Vector2<LandmarkObservation> landmarksFromLinesUsingEstimation[30][30]; //change to max lines
            LandmarkObservation extractedLandmark1;
            LandmarkObservation extractedLandmark2;
            Vector2<LandmarkObservation> linesLandmarks;

            double calculateObservationLogProbabilityFromOneLandmark(const Particle &particle,
                                                                     const LandmarkObservation &observation);

            Vector2<LandmarkObservation>
            extractLandmarksFromLine(const Particle &currentParticle, const Line2D &actualLine,
                                     const Line2D &observedLine);

            void preComputeDistances(const std::vector<Line2D> &observedLines);

            void
            preComputeLandmarksUsingCurrentEstimation(const std::vector<Line2D> &observedLines, const Pose2D &estimate);

            void calculatePossibleActualLines(const Pose2D &estimate, const representations::NaoJoints &jointsPosition);
        };

    }
}


#endif //SOURCE_MODELING_STATE_ESTIMATION_OBSERVATIONMODEL_H_
