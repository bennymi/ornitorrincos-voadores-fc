/*
 * GameStatePerceptor.cpp
 *
 *  Created on: Aug 15, 2015
 *      Author: itandroids
 */

#include "GameStatePerceptor.h"

namespace perception {

    /*
     * This constructor initializes variables without data node
     */
    GameStatePerceptor::GameStatePerceptor() {
        gameTime = 0;
        uniformNumber = 0;
        scoreLeft = 0;
        scoreRight = 0;
        playSide = representations::PlaySide::UNKNOWN;
        firstTime = true;
    }

    /*
     * This constructor sets variables of game state with parser tree node information
     */
    GameStatePerceptor::GameStatePerceptor(utils::parser::ParserTreeNode &node) :
            Perceptor(node) {

        if (firstTime) {
            playSide = representations::PlaySide::UNKNOWN;
            firstTime = false;
        }
        uniformNumber = 0;
        scoreLeft = 0;
        scoreRight = 0;
        playMode = "";
        std::vector<utils::parser::ParserTreeNode *>::iterator nodeIterator;
        std::string key;
        std::string value;
        //Iterates over all children's node and sets variables accordingly to the key
        for (nodeIterator = node.children.begin(); nodeIterator != node.children.end(); nodeIterator++) {
            utils::parser::KeyValuePair pair((*nodeIterator)->content, ' ');
            key = pair.key;
            value = pair.value;
            boost::trim(key);
            boost::trim(value);

            if (key.compare("sl") == 0) {
                scoreLeft = boost::lexical_cast<int>(value);
            } else if (key.compare("sr") == 0)
                scoreRight = boost::lexical_cast<int>(value);
            else if (key.compare("t") == 0)
                gameTime = boost::lexical_cast<double>(value);
            else if (key.compare("pm") == 0) {
                playMode = pair.value;
            } else if (key.compare("unum") == 0) {
                uniformNumber = boost::lexical_cast<int>(value);
                firstTime = true;
            } else if (key.compare("team") == 0) {

                if (value.compare("left") == 0) {
                    playSide = representations::PlaySide::LEFT;
                } else if (value.compare("right") == 0) {
                    playSide = representations::PlaySide::RIGHT;
                }
            }
        }
    }

    GameStatePerceptor::~GameStatePerceptor() {
    }

    /*
     * Getter methods: returns info of game state fields (game time, play mode, score left,
     * score right, uniform number, play side)
     */
    double GameStatePerceptor::getGameTime() {
        return gameTime;
    }

    std::string GameStatePerceptor::getPlayMode() {
        return playMode;
    }

    int GameStatePerceptor::getScoreLeft() {
        return scoreLeft;
    }

    int GameStatePerceptor::getScoreRight() {
        return scoreRight;
    }

    int GameStatePerceptor::getUniformNumber() {
        return uniformNumber;
    }

    //isFirstTime: returns true if is the first time
    bool GameStatePerceptor::isFirstTime() {
        return firstTime;
    }

    representations::PlaySide::PLAY_SIDE GameStatePerceptor::getPlaySide() {
        return playSide;
    }

} /* namespace perception */
