/*
 * BodyModel.cpp
 *
 *  Created on: Oct 6, 2015
 *      Author: mmaximo
 */

#include "BodyModel.h"

namespace modeling {

    BodyModel::BodyModel() {

        // Configure each body part
        configureTorso();
        configureHead();
        configureLeftArm();
        configureRightArm();
        configureLeftLeg();
        configureRightLeg();
        configureCoordinateSystem();
        configureChildren();
    }

    BodyModel::~BodyModel() {
    }

    void BodyModel::update(NaoJoints &joints) {
        updateBodyPart(TORSO, joints);
    }

    void BodyModel::updateBodyPart(BodyPartType bodyPartType, NaoJoints &joints) {
        BodyPartProperties &part = bodyParts[bodyPartType];
        if (part.parentType != INVALID_BODY_PART) {
            BodyPartProperties &parent = bodyParts[part.parentType];
            double angle = joints.getValue(part.joint.type);
            RotationMatrix rotation = RotationMatrix(part.joint.axis, angle);
            Vector3<double> translation = -parent.joint.anchor + part.translation + part.joint.anchor;

            transformsAtJoints[bodyPartType] = transformsAtJoints[parent.type]
                                               * Pose3D(rotation, translation);

            transformsAtCenters[bodyPartType] = transformsAtJoints[bodyPartType];
            transformsAtCenters[bodyPartType].translation +=
                    transformsAtCenters[bodyPartType].rotation
                    * (-part.joint.anchor);
        }

        std::vector<BodyPartType>::iterator it;
        for (it = part.children.begin(); it != part.children.end(); ++it) {
            updateBodyPart(*it, joints);
        }
    }

    Pose3D &BodyModel::getTransformAtCenter(BodyPartType bodyPartType) {
        return transformsAtCenters[bodyPartType];
    }

    Pose3D BodyModel::getLeftLegEndEffectorTransform() {
        Pose3D leftFootBottom = transformsAtCenters[LEFT_FOOT];
        double deltaZ = -bodyParts[LEFT_FOOT].boundingBox.getHeight() / 2.0;
        leftFootBottom.translation += leftFootBottom.rotation * Vector3<double>(0.0, 0.0, deltaZ);
        return leftFootBottom;
    }

    Pose3D BodyModel::getRightLegEndEffectorTransform() {
        Pose3D rightFootBottom = transformsAtCenters[RIGHT_FOOT];
        double deltaZ = -bodyParts[RIGHT_FOOT].boundingBox.getHeight() / 2.0;
        rightFootBottom.translation += rightFootBottom.rotation * Vector3<double>(0.0, 0.0, deltaZ);
        return rightFootBottom;
    }

    BodyPartProperties &BodyModel::getBodyPart(BodyPartType bodyPartType) {
        return bodyParts[bodyPartType];
    }

    void BodyModel::configureTorso() {
        bodyParts[TORSO].name = "torso";
        bodyParts[TORSO].type = TORSO;
        bodyParts[TORSO].parentType = INVALID_BODY_PART;
        bodyParts[TORSO].translation = Vector3<double>(0.0, 0.0, 0.0);
        bodyParts[TORSO].mass = 1.2171;
        bodyParts[TORSO].boundingBox = BoundingBox(0.1, 0.1, 0.18);
        bodyParts[TORSO].joint.type = NaoJoints::INVALID;
    }

    void BodyModel::configureHead() {
        bodyParts[NECK].name = "neck";
        bodyParts[NECK].type = NECK;
        bodyParts[NECK].parentType = TORSO;
        bodyParts[NECK].translation = Vector3<double>(0.0, 0., 0.09);
        bodyParts[NECK].mass = 0.05;
        bodyParts[NECK].boundingBox = BoundingBox(0.08, 0.015);
        bodyParts[NECK].joint.name = "HJ1";
        bodyParts[NECK].joint.type = NaoJoints::NECK_YAW;
        bodyParts[NECK].joint.anchor = Vector3<double>(0.0, 0.0, 0.0);
        bodyParts[NECK].joint.axis = Vector3<double>(0.0, 0.0, 1.0);
        bodyParts[NECK].joint.minAngle = MathUtils::degreesToRadians(-120.0);
        bodyParts[NECK].joint.maxAngle = MathUtils::degreesToRadians(120.0);

        bodyParts[HEAD].name = "head";
        bodyParts[HEAD].type = HEAD;
        bodyParts[HEAD].parentType = NECK;
        bodyParts[HEAD].translation = Vector3<double>(0.0, 0.0, 0.065);
        bodyParts[HEAD].mass = 0.35;
        bodyParts[HEAD].boundingBox = BoundingBox(0.065);
        bodyParts[HEAD].joint.name = "HJ2";
        bodyParts[HEAD].joint.type = NaoJoints::NECK_PITCH;
        bodyParts[HEAD].joint.anchor = Vector3<double>(0.0, 0.0, -0.005);
        bodyParts[HEAD].joint.axis = Vector3<double>(1.0, 0.0, 0.0);
        bodyParts[HEAD].joint.minAngle = MathUtils::degreesToRadians(-45.0);
        bodyParts[HEAD].joint.maxAngle = MathUtils::degreesToRadians(45.0);
    }

    void BodyModel::configureLeftArm() {
        bodyParts[LEFT_SHOULDER].name = "leftShoulder";
        bodyParts[LEFT_SHOULDER].type = LEFT_SHOULDER;
        bodyParts[LEFT_SHOULDER].parentType = TORSO;
        bodyParts[LEFT_SHOULDER].translation = Vector3<double>(-0.098, 0.0, 0.075);
        bodyParts[LEFT_SHOULDER].mass = 0.07;
        bodyParts[LEFT_SHOULDER].boundingBox = BoundingBox(0.01);
        bodyParts[LEFT_SHOULDER].joint.name = "LAJ1";
        bodyParts[LEFT_SHOULDER].joint.type = NaoJoints::LEFT_SHOULDER_PITCH;
        bodyParts[LEFT_SHOULDER].joint.anchor = Vector3<double>(0.0, 0.0, 0.0);
        bodyParts[LEFT_SHOULDER].joint.axis = Vector3<double>(1.0, 0.0, 0.0);
        bodyParts[LEFT_SHOULDER].joint.minAngle = MathUtils::degreesToRadians(
                -120.0);
        bodyParts[LEFT_SHOULDER].joint.maxAngle = MathUtils::degreesToRadians(
                120.0);

        bodyParts[LEFT_UPPER_ARM].name = "leftUpperarm";
        bodyParts[LEFT_UPPER_ARM].type = LEFT_UPPER_ARM;
        bodyParts[LEFT_UPPER_ARM].parentType = LEFT_SHOULDER;
        bodyParts[LEFT_UPPER_ARM].translation = Vector3<double>(-0.01, 0.02, 0.0);
        bodyParts[LEFT_UPPER_ARM].mass = 0.15;
        bodyParts[LEFT_UPPER_ARM].boundingBox = BoundingBox(0.07, 0.08, 0.06);
        bodyParts[LEFT_UPPER_ARM].joint.name = "LAJ2";
        bodyParts[LEFT_UPPER_ARM].joint.type = NaoJoints::LEFT_SHOULDER_YAW;
        bodyParts[LEFT_UPPER_ARM].joint.anchor =
                -bodyParts[LEFT_UPPER_ARM].translation;
        bodyParts[LEFT_UPPER_ARM].joint.axis = Vector3<double>(0.0, 0.0, 1.0);
        bodyParts[LEFT_UPPER_ARM].joint.minAngle = MathUtils::degreesToRadians(
                -1.0);
        bodyParts[LEFT_UPPER_ARM].joint.maxAngle = MathUtils::degreesToRadians(
                95.0);

        bodyParts[LEFT_ELBOW].name = "leftElbow";
        bodyParts[LEFT_ELBOW].type = LEFT_ELBOW;
        bodyParts[LEFT_ELBOW].parentType = LEFT_UPPER_ARM;
        bodyParts[LEFT_ELBOW].translation = Vector3<double>(0.01, 0.07, 0.009);
        bodyParts[LEFT_ELBOW].mass = 0.035;
        bodyParts[LEFT_ELBOW].boundingBox = BoundingBox(0.01);
        bodyParts[LEFT_ELBOW].joint.name = "LAJ3";
        bodyParts[LEFT_ELBOW].joint.type = NaoJoints::LEFT_ARM_ROLL;
        bodyParts[LEFT_ELBOW].joint.anchor = Vector3<double>(0.0, 0.0, 0.0);
        bodyParts[LEFT_ELBOW].joint.axis = Vector3<double>(0.0, 1.0, 0.0);
        bodyParts[LEFT_ELBOW].joint.minAngle = MathUtils::degreesToRadians(-120.0);
        bodyParts[LEFT_ELBOW].joint.maxAngle = MathUtils::degreesToRadians(120.0);

        bodyParts[LEFT_LOWER_ARM].name = "leftLowerarm";
        bodyParts[LEFT_LOWER_ARM].type = LEFT_LOWER_ARM;
        bodyParts[LEFT_LOWER_ARM].parentType = LEFT_ELBOW;
        bodyParts[LEFT_LOWER_ARM].translation = Vector3<double>(0.0, 0.05, 0.0);
        bodyParts[LEFT_LOWER_ARM].mass = 0.2;
        bodyParts[LEFT_LOWER_ARM].boundingBox = BoundingBox(0.05, 0.11, 0.05);
        bodyParts[LEFT_LOWER_ARM].joint.name = "LAJ4";
        bodyParts[LEFT_LOWER_ARM].joint.type = NaoJoints::LEFT_ARM_YAW;
        bodyParts[LEFT_LOWER_ARM].joint.anchor =
                -bodyParts[LEFT_LOWER_ARM].translation;
        bodyParts[LEFT_LOWER_ARM].joint.axis = Vector3<double>(0.0, 0.0, 1.0);
        bodyParts[LEFT_LOWER_ARM].joint.minAngle = MathUtils::degreesToRadians(
                -90.0);
        bodyParts[LEFT_LOWER_ARM].joint.maxAngle = MathUtils::degreesToRadians(1.0);
    }

    void BodyModel::configureRightArm() {
        bodyParts[RIGHT_SHOULDER].name = "rightShoulder";
        bodyParts[RIGHT_SHOULDER].type = RIGHT_SHOULDER;
        bodyParts[RIGHT_SHOULDER].parentType = TORSO;
        bodyParts[RIGHT_SHOULDER].translation = Vector3<double>(0.098, 0.0, 0.075);
        bodyParts[RIGHT_SHOULDER].mass = 0.07;
        bodyParts[RIGHT_SHOULDER].boundingBox = BoundingBox(0.01);
        bodyParts[RIGHT_SHOULDER].joint.name = "RAJ1";
        bodyParts[RIGHT_SHOULDER].joint.type = NaoJoints::RIGHT_SHOULDER_PITCH;
        bodyParts[RIGHT_SHOULDER].joint.anchor = Vector3<double>(0.0, 0.0, 0.0);
        bodyParts[RIGHT_SHOULDER].joint.axis = Vector3<double>(1.0, 0.0, 0.0);
        bodyParts[RIGHT_SHOULDER].joint.minAngle = MathUtils::degreesToRadians(
                -120.0);
        bodyParts[RIGHT_SHOULDER].joint.maxAngle = MathUtils::degreesToRadians(
                120.0);

        bodyParts[RIGHT_UPPER_ARM].name = "rightUpperarm";
        bodyParts[RIGHT_UPPER_ARM].type = RIGHT_UPPER_ARM;
        bodyParts[RIGHT_UPPER_ARM].parentType = RIGHT_SHOULDER;
        bodyParts[RIGHT_UPPER_ARM].translation = Vector3<double>(0.01, 0.02, 0.0);
        bodyParts[RIGHT_UPPER_ARM].mass = 0.15;
        bodyParts[RIGHT_UPPER_ARM].boundingBox = BoundingBox(0.07, 0.08, 0.06);
        bodyParts[RIGHT_UPPER_ARM].joint.name = "RAJ2";
        bodyParts[RIGHT_UPPER_ARM].joint.type = NaoJoints::RIGHT_SHOULDER_YAW;
        bodyParts[RIGHT_UPPER_ARM].joint.anchor =
                -bodyParts[RIGHT_UPPER_ARM].translation;
        bodyParts[RIGHT_UPPER_ARM].joint.axis = Vector3<double>(0.0, 0.0, 1.0);
        bodyParts[RIGHT_UPPER_ARM].joint.minAngle = MathUtils::degreesToRadians(
                -95.0);
        bodyParts[RIGHT_UPPER_ARM].joint.maxAngle = MathUtils::degreesToRadians(
                1.0);

        bodyParts[RIGHT_ELBOW].name = "rightElbow";
        bodyParts[RIGHT_ELBOW].type = RIGHT_ELBOW;
        bodyParts[RIGHT_ELBOW].parentType = RIGHT_UPPER_ARM;
        bodyParts[RIGHT_ELBOW].translation = Vector3<double>(-0.01, 0.07, 0.009);
        bodyParts[RIGHT_ELBOW].mass = 0.035;
        bodyParts[RIGHT_ELBOW].boundingBox = BoundingBox(0.01);
        bodyParts[RIGHT_ELBOW].joint.name = "RAJ3";
        bodyParts[RIGHT_ELBOW].joint.type = NaoJoints::RIGHT_ARM_ROLL;
        bodyParts[RIGHT_ELBOW].joint.anchor = Vector3<double>(0.0, 0.0, 0.0);
        bodyParts[RIGHT_ELBOW].joint.axis = Vector3<double>(0.0, 1.0, 0.0);
        bodyParts[RIGHT_ELBOW].joint.minAngle = MathUtils::degreesToRadians(-120.0);
        bodyParts[RIGHT_ELBOW].joint.maxAngle = MathUtils::degreesToRadians(120.0);

        bodyParts[RIGHT_LOWER_ARM].name = "rightLowerarm";
        bodyParts[RIGHT_LOWER_ARM].type = RIGHT_LOWER_ARM;
        bodyParts[RIGHT_LOWER_ARM].parentType = RIGHT_ELBOW;
        bodyParts[RIGHT_LOWER_ARM].translation = Vector3<double>(0.0, 0.05, 0.0);
        bodyParts[RIGHT_LOWER_ARM].mass = 0.2;
        bodyParts[RIGHT_LOWER_ARM].boundingBox = BoundingBox(0.05, 0.11, 0.05);
        bodyParts[RIGHT_LOWER_ARM].joint.name = "RAJ4";
        bodyParts[RIGHT_LOWER_ARM].joint.type = NaoJoints::RIGHT_ARM_YAW;
        bodyParts[RIGHT_LOWER_ARM].joint.anchor =
                -bodyParts[RIGHT_LOWER_ARM].translation;
        bodyParts[RIGHT_LOWER_ARM].joint.axis = Vector3<double>(0.0, 0.0, 1.0);
        bodyParts[RIGHT_LOWER_ARM].joint.minAngle = MathUtils::degreesToRadians(
                -1.0);
        bodyParts[RIGHT_LOWER_ARM].joint.maxAngle = MathUtils::degreesToRadians(
                90.0);
    }

    void BodyModel::configureLeftLeg() {
        bodyParts[LEFT_HIP1].name = "leftHip1";
        bodyParts[LEFT_HIP1].type = LEFT_HIP1;
        bodyParts[LEFT_HIP1].parentType = TORSO;
        bodyParts[LEFT_HIP1].translation = Vector3<double>(-0.055, -0.01, -0.115);
        bodyParts[LEFT_HIP1].mass = 0.09;
        bodyParts[LEFT_HIP1].boundingBox = BoundingBox(0.01);
        bodyParts[LEFT_HIP1].joint.name = "LLJ1";
        bodyParts[LEFT_HIP1].joint.type = NaoJoints::LEFT_HIP_YAWPITCH;
        bodyParts[LEFT_HIP1].joint.anchor = Vector3<double>(0.0, 0.0, 0.0);
        bodyParts[LEFT_HIP1].joint.axis = Vector3<double>(-sqrt(2.0) / 2.0, 0.0,
                                                          -sqrt(2.0) / 2.0);
        bodyParts[LEFT_HIP1].joint.minAngle = MathUtils::degreesToRadians(-90.0);
        bodyParts[LEFT_HIP1].joint.maxAngle = MathUtils::degreesToRadians(1.0);

        bodyParts[LEFT_HIP2].name = "leftHip2";
        bodyParts[LEFT_HIP2].type = LEFT_HIP2;
        bodyParts[LEFT_HIP2].parentType = LEFT_HIP1;
        bodyParts[LEFT_HIP2].translation = Vector3<double>(0.0, 0.0, 0.0);
        bodyParts[LEFT_HIP2].mass = 0.125;
        bodyParts[LEFT_HIP2].boundingBox = BoundingBox(0.01);
        bodyParts[LEFT_HIP2].joint.name = "LLJ2";
        bodyParts[LEFT_HIP2].joint.type = NaoJoints::LEFT_HIP_ROLL;
        bodyParts[LEFT_HIP2].joint.anchor = Vector3<double>(0.0, 0.0, 0.0);
        bodyParts[LEFT_HIP2].joint.axis = Vector3<double>(0.0, 1.0, 0.0);
        bodyParts[LEFT_HIP2].joint.minAngle = MathUtils::degreesToRadians(-25.0);
        bodyParts[LEFT_HIP2].joint.maxAngle = MathUtils::degreesToRadians(45.0);

        bodyParts[LEFT_THIGH].name = "leftThigh";
        bodyParts[LEFT_THIGH].type = LEFT_THIGH;
        bodyParts[LEFT_THIGH].parentType = LEFT_HIP2;
        bodyParts[LEFT_THIGH].translation = Vector3<double>(0.0, 0.01, -0.04);
        bodyParts[LEFT_THIGH].mass = 0.275;
        bodyParts[LEFT_THIGH].boundingBox = BoundingBox(0.07, 0.07, 0.14);
        bodyParts[LEFT_THIGH].joint.name = "LLJ3";
        bodyParts[LEFT_THIGH].joint.type = NaoJoints::LEFT_HIP_PITCH;
        bodyParts[LEFT_THIGH].joint.anchor = -bodyParts[LEFT_THIGH].translation;
        bodyParts[LEFT_THIGH].joint.axis = Vector3<double>(1.0, 0.0, 0.0);
        bodyParts[LEFT_THIGH].joint.minAngle = MathUtils::degreesToRadians(-25.0);
        bodyParts[LEFT_THIGH].joint.maxAngle = MathUtils::degreesToRadians(100.0);

        bodyParts[LEFT_SHANK].name = "leftShank";
        bodyParts[LEFT_SHANK].type = LEFT_SHANK;
        bodyParts[LEFT_SHANK].parentType = LEFT_THIGH;
        bodyParts[LEFT_SHANK].translation = Vector3<double>(0.0, 0.005, -0.125);
        bodyParts[LEFT_SHANK].mass = 0.225;
        bodyParts[LEFT_SHANK].boundingBox = BoundingBox(0.08, 0.07, 0.110);
        bodyParts[LEFT_SHANK].joint.name = "LLJ4";
        bodyParts[LEFT_SHANK].joint.type = NaoJoints::LEFT_KNEE_PITCH;
        bodyParts[LEFT_SHANK].joint.anchor = Vector3<double>(0.0, -0.01, 0.045);
        bodyParts[LEFT_SHANK].joint.axis = Vector3<double>(1.0, 0.0, 0.0);
        bodyParts[LEFT_SHANK].joint.minAngle = MathUtils::degreesToRadians(-130.0);
        bodyParts[LEFT_SHANK].joint.maxAngle = MathUtils::degreesToRadians(1.0);

        bodyParts[LEFT_ANKLE].name = "leftAnkle";
        bodyParts[LEFT_ANKLE].type = LEFT_ANKLE;
        bodyParts[LEFT_ANKLE].parentType = LEFT_SHANK;
        bodyParts[LEFT_ANKLE].translation = Vector3<double>(0.0, -0.01, -0.055);
        bodyParts[LEFT_ANKLE].mass = 0.125;
        bodyParts[LEFT_ANKLE].boundingBox = BoundingBox(0.01);
        bodyParts[LEFT_ANKLE].joint.name = "LLJ5";
        bodyParts[LEFT_ANKLE].joint.type = NaoJoints::LEFT_FOOT_PITCH;
        bodyParts[LEFT_ANKLE].joint.anchor = Vector3<double>(0.0, 0.0, 0.0);
        bodyParts[LEFT_ANKLE].joint.axis = Vector3<double>(1.0, 0.0, 0.0);
        bodyParts[LEFT_ANKLE].joint.minAngle = MathUtils::degreesToRadians(-45.0);
        bodyParts[LEFT_ANKLE].joint.maxAngle = MathUtils::degreesToRadians(75.0);

        bodyParts[LEFT_FOOT].name = "leftFoot";
        bodyParts[LEFT_FOOT].type = LEFT_FOOT;
        bodyParts[LEFT_FOOT].parentType = LEFT_ANKLE;
        bodyParts[LEFT_FOOT].translation = Vector3<double>(0.0, 0.03, -0.04);
        bodyParts[LEFT_FOOT].mass = 0.2;
        bodyParts[LEFT_FOOT].boundingBox = BoundingBox(0.08, 0.16, 0.02);
        bodyParts[LEFT_FOOT].joint.name = "LLJ6";
        bodyParts[LEFT_FOOT].joint.type = NaoJoints::LEFT_FOOT_ROLL;
        bodyParts[LEFT_FOOT].joint.anchor = -bodyParts[LEFT_FOOT].translation;
        bodyParts[LEFT_FOOT].joint.axis = Vector3<double>(0.0, 1.0, 0.0);
        bodyParts[LEFT_FOOT].joint.minAngle = MathUtils::degreesToRadians(-45.0);
        bodyParts[LEFT_FOOT].joint.maxAngle = MathUtils::degreesToRadians(25.0);
    }

    void BodyModel::configureRightLeg() {
        bodyParts[RIGHT_HIP1].name = "rightHip1";
        bodyParts[RIGHT_HIP1].type = RIGHT_HIP1;
        bodyParts[RIGHT_HIP1].parentType = TORSO;
        bodyParts[RIGHT_HIP1].translation = Vector3<double>(0.055, -0.01, -0.115);
        bodyParts[RIGHT_HIP1].mass = 0.09;
        bodyParts[RIGHT_HIP1].boundingBox = BoundingBox(0.01);
        bodyParts[RIGHT_HIP1].joint.name = "RLJ1";
        bodyParts[RIGHT_HIP1].joint.type = NaoJoints::RIGHT_HIP_YAWPITCH;
        bodyParts[RIGHT_HIP1].joint.anchor = Vector3<double>(0.0, 0.0, 0.0);
        bodyParts[RIGHT_HIP1].joint.axis = Vector3<double>(-sqrt(2.0) / 2.0, 0.0,
                                                           sqrt(2.0) / 2.0);
        bodyParts[RIGHT_HIP1].joint.minAngle = MathUtils::degreesToRadians(-90.0);
        bodyParts[RIGHT_HIP1].joint.maxAngle = MathUtils::degreesToRadians(1.0);

        bodyParts[RIGHT_HIP2].name = "rightHip2";
        bodyParts[RIGHT_HIP2].type = RIGHT_HIP2;
        bodyParts[RIGHT_HIP2].parentType = RIGHT_HIP1;
        bodyParts[RIGHT_HIP2].translation = Vector3<double>(0.0, 0.0, 0.0);
        bodyParts[RIGHT_HIP2].mass = 0.125;
        bodyParts[RIGHT_HIP2].boundingBox = BoundingBox(0.01);
        bodyParts[RIGHT_HIP2].joint.name = "RLJ2";
        bodyParts[RIGHT_HIP2].joint.type = NaoJoints::RIGHT_HIP_ROLL;
        bodyParts[RIGHT_HIP2].joint.anchor = Vector3<double>(0.0, 0.0, 0.0);
        bodyParts[RIGHT_HIP2].joint.axis = Vector3<double>(0.0, 1.0, 0.0);
        bodyParts[RIGHT_HIP2].joint.minAngle = MathUtils::degreesToRadians(-45.0);
        bodyParts[RIGHT_HIP2].joint.maxAngle = MathUtils::degreesToRadians(25.0);

        bodyParts[RIGHT_THIGH].name = "rightThigh";
        bodyParts[RIGHT_THIGH].type = RIGHT_THIGH;
        bodyParts[RIGHT_THIGH].parentType = RIGHT_HIP2;
        bodyParts[RIGHT_THIGH].translation = Vector3<double>(0.0, 0.01, -0.04);
        bodyParts[RIGHT_THIGH].mass = 0.275;
        bodyParts[RIGHT_THIGH].boundingBox = BoundingBox(0.07, 0.07, 0.14);
        bodyParts[RIGHT_THIGH].joint.name = "RLJ3";
        bodyParts[RIGHT_THIGH].joint.type = NaoJoints::RIGHT_HIP_PITCH;
        bodyParts[RIGHT_THIGH].joint.anchor = -bodyParts[RIGHT_THIGH].translation;
        bodyParts[RIGHT_THIGH].joint.axis = Vector3<double>(1.0, 0.0, 0.0);
        bodyParts[RIGHT_THIGH].joint.minAngle = MathUtils::degreesToRadians(-25.0);
        bodyParts[RIGHT_THIGH].joint.maxAngle = MathUtils::degreesToRadians(100.0);

        bodyParts[RIGHT_SHANK].name = "rightShank";
        bodyParts[RIGHT_SHANK].type = RIGHT_SHANK;
        bodyParts[RIGHT_SHANK].parentType = RIGHT_THIGH;
        bodyParts[RIGHT_SHANK].translation = Vector3<double>(0.0, 0.005, -0.125);
        bodyParts[RIGHT_SHANK].mass = 0.225;
        bodyParts[RIGHT_SHANK].boundingBox = BoundingBox(0.08, 0.07, 0.110);
        bodyParts[RIGHT_SHANK].joint.name = "RLJ4";
        bodyParts[RIGHT_SHANK].joint.type = NaoJoints::RIGHT_KNEE_PITCH;
        bodyParts[RIGHT_SHANK].joint.anchor = Vector3<double>(0.0, -0.01, 0.045);
        bodyParts[RIGHT_SHANK].joint.axis = Vector3<double>(1.0, 0.0, 0.0);
        bodyParts[RIGHT_SHANK].joint.minAngle = MathUtils::degreesToRadians(-130.0);
        bodyParts[RIGHT_SHANK].joint.maxAngle = MathUtils::degreesToRadians(1.0);

        bodyParts[RIGHT_ANKLE].name = "rightAnkle";
        bodyParts[RIGHT_ANKLE].type = RIGHT_ANKLE;
        bodyParts[RIGHT_ANKLE].parentType = RIGHT_SHANK;
        bodyParts[RIGHT_ANKLE].translation = Vector3<double>(0.0, -0.01, -0.055);
        bodyParts[RIGHT_ANKLE].mass = 0.125;
        bodyParts[RIGHT_ANKLE].boundingBox = BoundingBox(0.01);
        bodyParts[RIGHT_ANKLE].joint.name = "RLJ5";
        bodyParts[RIGHT_ANKLE].joint.type = NaoJoints::RIGHT_FOOT_PITCH;
        bodyParts[RIGHT_ANKLE].joint.anchor = Vector3<double>(0.0, 0.0, 0.0);
        bodyParts[RIGHT_ANKLE].joint.axis = Vector3<double>(1.0, 0.0, 0.0);
        bodyParts[RIGHT_ANKLE].joint.minAngle = MathUtils::degreesToRadians(-45.0);
        bodyParts[RIGHT_ANKLE].joint.maxAngle = MathUtils::degreesToRadians(75.0);

        bodyParts[RIGHT_FOOT].name = "rightFoot";
        bodyParts[RIGHT_FOOT].type = RIGHT_FOOT;
        bodyParts[RIGHT_FOOT].parentType = RIGHT_ANKLE;
        bodyParts[RIGHT_FOOT].translation = Vector3<double>(0.0, 0.03, -0.04);
        bodyParts[RIGHT_FOOT].mass = 0.2;
        bodyParts[RIGHT_FOOT].boundingBox = BoundingBox(0.08, 0.16, 0.02);
        bodyParts[RIGHT_FOOT].joint.name = "RLJ6";
        bodyParts[RIGHT_FOOT].joint.type = NaoJoints::RIGHT_FOOT_ROLL;
        bodyParts[RIGHT_FOOT].joint.anchor = -bodyParts[RIGHT_FOOT].translation;
        bodyParts[RIGHT_FOOT].joint.axis = Vector3<double>(0.0, 1.0, 0.0);
        bodyParts[RIGHT_FOOT].joint.minAngle = MathUtils::degreesToRadians(-25.0);
        bodyParts[RIGHT_FOOT].joint.maxAngle = MathUtils::degreesToRadians(45.0);
    }

    Vector3<double> BodyModel::computeCenterOfMass() {
        Vector3<double> centerOfMass;

        double sumMass = 0.0;
        for (int i = 0; i < NUM_BODY_PARTS; ++i) {
            centerOfMass.x += transformsAtCenters[i].translation.x * bodyParts[i].mass;
            centerOfMass.y += transformsAtCenters[i].translation.y * bodyParts[i].mass;
            centerOfMass.z += transformsAtCenters[i].translation.z * bodyParts[i].mass;
            sumMass += bodyParts[i].mass;
        }

        centerOfMass = centerOfMass * (1.0 / sumMass);

        return centerOfMass;
    }

    void BodyModel::configureChildren() {
        for (int i = 0; i < NUM_BODY_PARTS; ++i)
            if (bodyParts[i].parentType != INVALID_BODY_PART)
                bodyParts[bodyParts[i].parentType].children.push_back(
                        bodyParts[i].type);
    }

    void BodyModel::configureCoordinateSystem() {
        double EPSILON = 0.1;
        RotationMatrix rotationMatrix = RotationMatrix::fromRotationZ(MathUtils::degreesToRadians(-90.0));
        for (int i = 0; i < NUM_BODY_PARTS; ++i) {
            bodyParts[i].translation = rotationMatrix * bodyParts[i].translation;
            bodyParts[i].boundingBox.transform(rotationMatrix);
            bodyParts[i].joint.anchor = rotationMatrix * bodyParts[i].joint.anchor;
            bodyParts[i].joint.axis = rotationMatrix * bodyParts[i].joint.axis;
//		if (bodyParts[i].joint.axis.y > EPSILON) {
//			bodyParts[i].joint.minAngle = -bodyParts[i].joint.maxAngle;
//			bodyParts[i].joint.maxAngle = -bodyParts[i].joint.minAngle;
//		}
        }
    }

} /* namespace modeling */
