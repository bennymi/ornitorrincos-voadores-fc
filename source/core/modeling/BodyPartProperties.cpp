/*
 * BodyPartProperties.cpp
 *
 *  Created on: Oct 4, 2015
 *      Author: mmaximo
 */

#include "modeling/BodyPartProperties.h"

namespace modeling {

    BodyPartProperties::BodyPartProperties() :
            name(""), type(INVALID_BODY_PART), parentType(INVALID_BODY_PART), translation(
            Vector3<double>(0.0, 0.0, 0.0)), mass(0.0), boundingBox(
            BoundingBox(0.0, 0.0, 0.0)), joint(
            HingeJointProperties()) {
    }

    BodyPartProperties::BodyPartProperties(std::string name, BodyPartType type,
                                           BodyPartType parentType, Vector3<double> translation, double mass,
                                           BoundingBox boundingBox, HingeJointProperties jointProperties) :
            name(name), type(type), parentType(parentType), translation(
            translation), mass(mass), boundingBox(boundingBox), joint(
            jointProperties) {
    }

} /* namespace modeling */
