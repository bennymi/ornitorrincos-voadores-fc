/*
 * JointsController.cpp
 *
 *  Created on: Oct 10, 2015
 *      Author: mmaximo
 */

#include "JointsController.h"

#include "representations/RobotTypeProfile.h"
#include "representations/ITAndroidsConstants.h"

namespace control {

    JointsController::JointsController(int robotType, double kp) : robotType(robotType) {
        //jointsFile.open("joints.txt");
        representations::RobotTypeProfile profile = representations::RobotTypeProfile::getRobotTypeProfile(
                static_cast<representations::RobotType::ROBOT_TYPE>(robotType));
        for (int j = 0; j < representations::NaoJoints::NUM_JOINTS; ++j) {
            representations::NaoJoints::HINGE_JOINTS joint = static_cast<representations::NaoJoints::HINGE_JOINTS>(j);
            double saturation = representations::ITAndroidsConstants::DEFAULT_JOINT_MAX_SPEED;
            if (joint == representations::NaoJoints::LEFT_FOOT_PITCH ||
                joint == representations::NaoJoints::RIGHT_FOOT_PITCH)
                saturation = profile.getFootPitchMaxSpeed();
            else if (joint == representations::NaoJoints::LEFT_FOOT_ROLL ||
                     joint == representations::NaoJoints::RIGHT_FOOT_ROLL)
                saturation = profile.getFootRollMaxSpeed();

            controllers[j].setParams(kp, saturation);
        }
    }

    JointsController::~JointsController() {
    }

    void JointsController::update(representations::NaoJoints &desired, representations::NaoJoints &actual) {
        //jointsFile << desired.leftKneePitch << " " << actual.leftKneePitch << std::endl;
        //jointsFile.flush();
        representations::NaoJoints error = desired - actual;
//	std::cout << "error.leftShoulderPitch: " << error.leftShoulderPitch << std::endl;
        for (int j = 0; j < representations::NaoJoints::NUM_JOINTS; ++j) {
            representations::NaoJoints::HINGE_JOINTS joint = static_cast<representations::NaoJoints::HINGE_JOINTS>(j);
            double command = controllers[j].update(error.getValue(joint));
            commands.setValue(joint, command);
        }
    }

    representations::NaoJoints &JointsController::getCommands() {
        return commands;
    }

} /* namespace control */
