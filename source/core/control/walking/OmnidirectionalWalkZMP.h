/*
 * OminidirecionalWalkZMP.h
 *
 *  Created on: Oct 10, 2015
 *      Author: mmaximo
 */

#ifndef SOURCE_CONTROL_WALKING_OMNIDIRECTIONALWALKZMP_H_
#define SOURCE_CONTROL_WALKING_OMNIDIRECTIONALWALKZMP_H_

#include <fstream>
#include "math/Pose2D.h"
#include "control/walking/RobotPhysicalParameters.h"
#include "control/walking/InverseKinematics.h"
#include "control/MovementSwitchSmoother.h"
#include "representations/NaoJoints.h"
#include "modeling/BodyModel.h"
#include "modeling/AgentModel.h"
#include "utils/patterns/Configurable.h"

using itandroids_lib::utils::patterns::Configurable;
using itandroids_lib::utils::patterns::Parameter;
namespace control {
    namespace walking {

        using namespace itandroids_lib::math;
        using namespace representations;
        using modeling::BodyModel;
        using modeling::AgentModel;

        enum struct OmnidirectionalWalkZMParamsType {
            NORMAL,
            PRECISE,
        };

        enum StoppingState {
            NONE,
            STOP_REQUESTED,
            ADJUSTING_1ST,
            ADJUSTING_2ND,
            STOPPED
        };

        struct OmnidirectionalWalkZMPParams : public Configurable<OmnidirectionalWalkZMPParams> {
            double period;
            double zCom;
            double zStep;
            double doubleSupportRatio;
            double ySeparation;
            double armsCompensationFactor;

            OmnidirectionalWalkZMPParams();

            OmnidirectionalWalkZMPParams(double period, double zCom, double zStep, double doubleSupportRatio,
                                         double ySeparation, double armsCompensationFactor);

            /**
             * Template function for configurable data
             * It separates the parameter nodes in walk types
             */
            template<class Content, class Key>
            void setParameter(Parameter<Content, Key> &parameter) {

                // Set walk parameters
                // @todo allow change of the default values

                for (auto &value: parameter.getChildren()) {
                    if (value.first == "current") {
                        setWalkParameters(*value.second, *this);
                    } else if (value.first == "normal") {
                        setWalkParameters(*value.second, normalParams);
                    } else if (value.first == "precise") {
                        setWalkParameters(*value.second, preciseParams);
                    }
                }
            }

            static OmnidirectionalWalkZMPParams getOmnidirectionalWalkZMPParams(
                    OmnidirectionalWalkZMParamsType type = OmnidirectionalWalkZMParamsType::NORMAL);

        private:
            static OmnidirectionalWalkZMPParams normalParams;
            static OmnidirectionalWalkZMPParams preciseParams;

            /**
             *
             * @tparam Content for the parameter tree
             * @tparam Key for the parameter tree
             * @param parameter The node of the parameter tree
             * @param omnidirectionalWalkZMPParam The walkParam to be modified
             */
            template<class Content, class Key>
            void setWalkParameters(Parameter<Content, Key> &parameter,
                                   OmnidirectionalWalkZMPParams &omnidirectionalWalkZMPParam) {

                Content &content = parameter.getContent();
                for (boost::property_tree::ptree::value_type &value: content) {
                    if (value.first == "period")
                        omnidirectionalWalkZMPParam.period = std::stod(value.second.data());
                    else if (value.first == "zCom")
                        omnidirectionalWalkZMPParam.zCom = std::stod(value.second.data());
                    else if (value.first == "zStep")
                        omnidirectionalWalkZMPParam.zStep = std::stod(value.second.data());
                    else if (value.first == "doubleSupportRatio")
                        omnidirectionalWalkZMPParam.doubleSupportRatio = std::stod(value.second.data());
                    else if (value.first == "ySeparation")
                        omnidirectionalWalkZMPParam.ySeparation = std::stod(value.second.data());
                    else if (value.first == "armsCompensationFactor")
                        omnidirectionalWalkZMPParam.armsCompensationFactor = std::stod(value.second.data());
                }
            }

        };

        class OmnidirectionalWalkZMP : public Configurable<OmnidirectionalWalkZMP> {
        public:
            static const double DEFAULT_LEFT_SHOULDER_YAW_OFFSET;
            static const double DEFAULT_RIGHT_SHOULDER_YAW_OFFSET;
            static const double DEFAULT_LEFT_ARM_ROLL_OFFSET;
            static const double DEFAULT_RIGHT_ARM_ROLL_OFFSET;
            bool leftFootIsSwing;
            Pose2D torso;

            OmnidirectionalWalkZMP(OmnidirectionalWalkZMPParams walkParams,
                                   RobotPhysicalParameters physicalParams,
                                   InverseKinematics *inverseKinematics);

            virtual ~OmnidirectionalWalkZMP();

            void restart();

            void update(double elapsedTime, NaoJoints &joints);

            void setWalkParams(const OmnidirectionalWalkZMPParams &walkParams);

            void setDesiredVelocity(Pose2D velocity);

            void requestStop();

            bool hasStopped();

            bool isStopping();

            Pose2D getOdometry();

            /**
             * Template function for configurable data, that just pass the node to the parameters
             */
            template<class Content, class Key>
            void setParameter(Parameter<Content, Key> &parameter) {

                // Set walk parameters
                walkParams->setParameter(parameter);

            }

        private:
            const double GRAVITY;

            StoppingState stoppingState;

            std::unique_ptr<OmnidirectionalWalkZMPParams> walkParams;
            InverseKinematics *inverseKinematics;
            RobotPhysicalParameters physicalParams;

            double tZMP;

            Pose2D swingFootBegin;
            Pose2D swingFootEnd;
            Pose2D torsoBegin;
            Pose2D torsoEnd;

            double walkTime;

            Pose2D commandedSpeed;
            Pose2D speed;

            Pose2D previousTorso;
            Pose2D odometry;

            BodyModel bodyModel;

            double ALPHA;

            void switchSwingFoot();

            void solveZMP(double zs, double z1, double z2, double x1, double x2, double &aP,
                          double &aN);

            double getCoM(double t, double aP, double aN, double zs, double z1, double z2);

            void computeNewStep();

            Pose2D poseGlobal(Pose2D poseRelative, Pose2D pose);

            Pose2D poseRelative(Pose2D poseGlobal, Pose2D pose);

            Pose2D getDesiredTorsoDisplacement();

            Pose2D limitTorso(Pose2D beginTorso, Pose2D torsoDisplacement);

            double computeXFrac(double t);

            double computeYFrac(double t);

            double computeZFrac(double t);

            double computeThetaFrac(double t);

            void computeTorsoToAttainCoM(double t, double xCoM, double yCoM, NaoJoints &joints);

//            void balance(AgentModel& agentModel, NaoJoints& joints);
        };

    } /* namespace walking */
} /* namespace control */

#endif /* SOURCE_CONTROL_WALKING_OMNIDIRECTIONALWALKZMP_H_ */
