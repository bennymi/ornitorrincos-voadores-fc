/*
 * WorldModel.cpp
 *
 *  Created on: Aug 8, 2015
 *      Author: mmaximo
 */

#include "modeling/WorldModel.h"
#include <core/representations/NavigationParams.h>

using itandroids_lib::math::MathUtils;
namespace modeling {

    WorldModel::WorldModel(std::string teamName) :
            selfModel(fieldDescription), ourPlaySide(
            PlaySide::UNKNOWN), theirPlaySide(PlaySide::UNKNOWN), visibleBall(
            nullptr) {
        playMode = representations::PlayMode::NONE;
        globalTime = 0;
        selfDistanceToBall = 0;
        ourTeamName = teamName;
        teammateModels.reserve(NUM_PLAYERS_PER_TEAM);
        opponentModels.reserve(NUM_PLAYERS_PER_TEAM);
        heardTeammateModels.reserve(NUM_PLAYERS_PER_TEAM);
        heardOpponentModels.reserve(NUM_PLAYERS_PER_TEAM);
        positioningModel = PositioningModel();
        opponentToTalk = 1; //The goalie starts to talk the the opponent with number 1.

        for (int i = 0; i < NUM_PLAYERS_PER_TEAM; i++) {
            auto auxModel = OtherPlayerModel(i + 1);
            auxModel.getObject().setPosition(Pose2D(0, 100, 100));
            teammateModels.push_back(auxModel);
            auxModel = OtherPlayerModel(i + 1);
            auxModel.getObject().setPosition(Pose2D(0, 100, 100));
            opponentModels.push_back(auxModel);
        }
    }

    WorldModel::~WorldModel() {
    }

    void WorldModel::update(perception::AgentPerception &agentPerception,
                            AgentModel &agentModel, Pose2D &odometry) {
        const int &uniformNumber = agentModel.getAgentNumber();
        globalTime = agentPerception.getGlobalTime();
        playMode = representations::PlayMode::determinePlayMode(gameState.getPlayMode(),
                                                                ourPlaySide); // Determine current Play Mode

        // Clear old information
        teammates.clear();
        teammatesFromBall.clear();
        teammatesFromSelf.clear();
        opponents.clear();
        opponentsFromBall.clear();
        opponentsFromSelf.clear();
        opponentsFromOurGoal.clear();

        // Update information
        gameState = agentPerception.getGameState(); // Update game state
        ourPlaySide = agentPerception.getPlaySide(); // Update play side
        theirPlaySide = representations::PlaySide::getOppositePlaySide(ourPlaySide); // Update opponent`s play side
        fieldDescription.update(ourPlaySide);
        /* Hearing Data */
        hearData = agentPerception.getHearData();
        if (hearData.getMessage().size() != 0) {
            hearData.decode(ourTeamName);
        }

        // Update visible objects
        std::vector<perception::VisibleObject *> &visibleObjects =
                agentPerception.getVisibleObjects();
        std::vector<perception::VisibleObject *>::iterator iterator;
        /*Updating visible Object Models */

        visibleObjectsCounter = 0;
        using namespace std;
        for (iterator = visibleObjects.begin(); iterator != visibleObjects.end();
             iterator++) {
            (*iterator)->transformPosition(
                    agentModel.getFromCameraToRootTransform());
            if ((*iterator)->getVisibleObjectType()
                == perception::VisibleObjectType::VISIBLE_BALL) {
                perception::VisibleBall *auxPtr =
                        dynamic_cast<perception::VisibleBall *>(*iterator);
                visibleBall = auxPtr;

                visibleObjectsCounter++; // Update number of visible objects

            } else if ((*iterator)->getVisibleObjectType()
                       == perception::VisibleObjectType::VISIBLE_FLAG) {
                perception::VisibleFlag *auxPtr =
                        dynamic_cast<perception::VisibleFlag *>(*iterator);
                visibleFlags.push_back(auxPtr);

                visibleObjectsCounter++; // Update number of visible objects

            } else if ((*iterator)->getVisibleObjectType()
                       == perception::VisibleObjectType::VISIBLE_GOAL_POST) {
                perception::VisibleGoalPost *auxPtr =
                        dynamic_cast<perception::VisibleGoalPost *>(*iterator);
                visibleGoalPosts.push_back(auxPtr);

                visibleObjectsCounter++; // Update number of visible objects
            } else if ((*iterator)->getVisibleObjectType()
                       == perception::VisibleObjectType::VISIBLE_LINE) {
                visibleLines.push_back(
                        dynamic_cast<perception::VisibleLine *>(*iterator));

                visibleObjectsCounter++; // Update number of visible objects
            } else if ((*iterator)->getVisibleObjectType()
                       == perception::VisibleObjectType::VISIBLE_PLAYER) {
                perception::VisiblePlayer *playerPtr =
                        dynamic_cast<perception::VisiblePlayer *>(*iterator);
                visiblePlayers.push_back(playerPtr);

            }
        }

        selfModel.update(globalTime, visibleFlags, visibleGoalPosts, visibleLines,
                         odometry,
                         agentPerception.getNaoJoints()); // Update self model according to the visible objects
        ballModel.update(globalTime, visibleBall, odometry,
                         selfModel.getObject().getPosition()); // Update ball model according to the visible objects

        if (visibleBall == nullptr &&
            (ballModel.getObject().getAge(globalTime) > 2.0) && hearData.getTeam() == ourTeamName
            && hearData.getUniformNumber() != 1) { //goalie doesn't send ball info

            ballModel.update(globalTime, hearData, ourTeamName, odometry,
                             selfModel.getObject().getPosition()); // Update ball model according to the hear data.
        }

        //update  heard players
        //update just if message is from another player
        if (!hearData.isFromSelf() && hearData.getTeam() == ourTeamName)
            heardTeammateModels[hearData.getUniformNumber() - 1].update(hearData);

        bool heardTeamMembers[11];
        for (int i = 0; i < 11; i++) heardTeamMembers[i] = true;

        /*for (int i = 0; i < 11; i++) {
            if (globalTime - heardTeammateModels[i].getObject().getLastHeardTime() > 5.0) //Check if heard data is old
                heardTeamMembers[i] = false;
            else heardTeamMembers[i] = true;
        }*/


        if (hearData.getUniformNumber() == 1 && !hearData.isFallen()) {
            for (auto opponent: hearData.getOpponentPositions()) {
                heardOpponentModels[opponent.getId() - 1].update(opponent);
            }
        }


        bool heardOpponentMembers[11];
        for (int i = 0; i < 11; i++) heardOpponentMembers[i] = true;

        /*for (int i = 0; i < 11; i++) {
            if (globalTime - heardOpponentModels[i].getObject().getLastHeardTime() > 5.0) //Check if heard data is old
                heardOpponentMembers[i] = false;
            else heardOpponentMembers[i] = true;
        }
        */

        bool seenTeamMembers[11];
        bool seenOpponentPlayers[11];
        for (int i = 0; i < 11; i++) seenTeamMembers[i] = false;
        seenTeamMembers[agentModel.getAgentNumber() - 1] = true;
        for (int i = 0; i < 11; i++) seenOpponentPlayers[i] = false;

        //Update seen players
        for (auto &player : visiblePlayers) {
            std::string teamName(player->getTeamName());

            if (teamName == ourTeamName) {
                seenTeamMembers[player->getId() - 1] = true;
                teammateModels[player->getId() - 1].update(globalTime, player,
                                                           odometry, selfModel.getObject().getPosition());
            } else {
                auto pos = player->getPosition();
                seenOpponentPlayers[player->getId() - 1] = true;
                opponentModels[player->getId() - 1].update(globalTime, player,
                                                           odometry, selfModel.getObject().getPosition());
            }
        }

        for (int i = 0; i < 11; i++) {
            if (seenTeamMembers[i]) { ; }
            else if (heardTeamMembers[i]) {
                teammateModels[i].update(heardTeammateModels[i].getObject());
            } else {
                //teammateModels[i].update(globalTime, nullptr,
                //                                        odometry, selfModel.getObject().getPosition());
            }
            if (seenOpponentPlayers[i]) continue;
            else if (heardOpponentMembers[i]) {
                opponentModels[i].update(heardOpponentModels[i].getObject());
            } else {
                // opponentModels[i].update(globalTime, nullptr,
                //                                          odometry, selfModel.getObject().getPosition());
            }
        }

        selfDistanceToBall = (selfModel.getObject().getPosition()
                              - ballModel.getObject().getPosition()).abs();

        // Finding the relevant teammates
        for (int i = 0; i < NUM_PLAYERS_PER_TEAM; i++) {
            OtherPlayer *otherPlayerPtr;
            double distanceToBall;
            double distanceToSelf;
            double distanceToOurGoal;

            itandroids_lib::math::Pose2D &auxSelfPose =
                    selfModel.getObject().getPosition();
            itandroids_lib::math::Pose2D &auxBallPose =
                    ballModel.getObject().getPosition();
            itandroids_lib::math::Pose2D auxOurGoalPose(getOwnGoalPosition());

            // Update teammate`s information: distance from self and distance from ball.
            if (i + 1 != uniformNumber
                && teammateModels[i].getObject().isRelevant()) {
                otherPlayerPtr = (&teammateModels[i].getObject());
                teammates.push_back(otherPlayerPtr);
                distanceToBall =
                        (otherPlayerPtr->getPosition() - auxBallPose).abs();
                distanceToSelf =
                        (otherPlayerPtr->getPosition() - auxSelfPose).abs();
                teammatesFromBall.push_back(
                        std::pair<double, OtherPlayer *>(distanceToBall,
                                                         otherPlayerPtr));
                teammatesFromSelf.push_back(
                        std::pair<double, OtherPlayer *>(distanceToSelf,
                                                         otherPlayerPtr));
            }

            // Update opponent`s information: distance from self, distance from ball and from our goal.
            if (opponentModels[i].getObject().isRelevant()) {
                otherPlayerPtr = (&opponentModels[i].getObject());
                opponents.push_back(otherPlayerPtr);
                distanceToBall =
                        (otherPlayerPtr->getPosition() - auxBallPose).abs();
                distanceToSelf =
                        (otherPlayerPtr->getPosition() - auxSelfPose).abs();
                distanceToOurGoal =
                        (otherPlayerPtr->getPosition() - auxOurGoalPose).abs();
                opponentsFromOurGoal.push_back(
                        std::pair<double, OtherPlayer *>(distanceToOurGoal,
                                                         otherPlayerPtr));
                opponentsFromBall.push_back(
                        std::pair<double, OtherPlayer *>(distanceToBall,
                                                         otherPlayerPtr));
                opponentsFromSelf.push_back(
                        std::pair<double, OtherPlayer *>(distanceToSelf,
                                                         otherPlayerPtr));
            }
        }

        //Sorting the teammates and opponents according to the distance

        sortOpponentsFromOurGoal();
        sortOpponentsFromSelf();
        sortOpponentsFromBall();
        sortTeammatesFromSelf();
        sortTeammatesFromBall();

        /* Update Positioning Model - Generating Synchronized Role Assignment Vector */
        positioningModel.update(globalTime, hearData, ourTeamName);

        /*for (auto role : getSynchronizedRoleAssignmentVector()) {
            std:: cout << role << " * ";
        }
        std::cout << std::endl;*/
/*
        utils::roboviz::Roboviz roboviz;
        std::string selfStr("animated");

        if (uniformNumber == 10 && teammates.size() == 10 && opponents.size() == 11)  {
            for (int i = 0; i < teammates.size(); i++) {
                //std::cout << teammates[i]->getPosition().translation.x << " ";
                //std::cout << heardTeammateModels[i].getObject().getPosition().translation.y << " " << std::endl;

                roboviz.drawCircle(opponents[i] ->getPosition().translation.x,
                                   opponents[i]->getPosition().translation.y, 1, 6, 12, 233, 1,
                                   &selfStr);

                roboviz.drawCircle(teammates[i]->getPosition().translation.x,
                                   teammates[i]->getPosition().translation.y, 1, 6, 12, 233, 0,
                                   &selfStr);

            }
        } */

        /**
         * Update the game situation
         */
        updateGameSituation();

        // Free each vector of visible objects. The vector visibleObjects is already cleared in Perception.
        visibleFlags.clear();
        visibleGoalPosts.clear();
        visibleLines.clear();
        visibleBall = nullptr;
        visiblePlayers.clear();

        //@todo add function to update GameSituation: attacking or defending
    }

    Ball &WorldModel::getBall() {
        return ballModel.getObject();
    }

    Self &WorldModel::getSelf() {
        return selfModel.getObject();
    }

    void WorldModel::resetLocalizer(Pose2D estimate, Pose2D sigmas) {
        selfModel.resetLocalizer(estimate, sigmas);
    }

    Vector3<double> WorldModel::getOwnGoalPosition() {
        if (ourPlaySide == representations::PlaySide::RIGHT) {
            return (fieldDescription.getGoalPostKnownPosition(
                    representations::GoalPostType::G1R)
                    + fieldDescription.getGoalPostKnownPosition(
                    representations::GoalPostType::G2R)) / 2;
        } else {
            return (fieldDescription.getGoalPostKnownPosition(
                    representations::GoalPostType::G1L)
                    + fieldDescription.getGoalPostKnownPosition(
                    representations::GoalPostType::G2L)) / 2;
        }
    }

    const Vector3<double> WorldModel::getTheirGoalPosition() {
        if (ourPlaySide == representations::PlaySide::LEFT) {
            return (fieldDescription.getGoalPostKnownPosition(
                    representations::GoalPostType::G1R)
                    + fieldDescription.getGoalPostKnownPosition(
                    representations::GoalPostType::G2R)) / 2.0;
        } else {
            return (fieldDescription.getGoalPostKnownPosition(
                    representations::GoalPostType::G1L)
                    + fieldDescription.getGoalPostKnownPosition(
                    representations::GoalPostType::G2L)) / 2.0;
        }
    }

    const OtherPlayer &WorldModel::getOurGoalie() {
        return getTeammate(GOALIE_UNIFORM_NUMBER);
    }

    const OtherPlayer &WorldModel::getTheirGoalie() {
        return getOpponent(GOALIE_UNIFORM_NUMBER);
    }

    const OtherPlayer &WorldModel::getTeammate(int uniformNumber) {
        return *teammates[uniformNumber - 1];
    }

    const OtherPlayer &WorldModel::getOpponent(int uniformNumber) {
        return *opponents[uniformNumber - 1];
    }

    const OtherPlayer &WorldModel::getOpponentClosestFromBall() {
        return *opponentsFromBall[0].second;
    }

    const OtherPlayer &WorldModel::getOpponentClosestFromSelf() {
        return *opponentsFromSelf[0].second;
    }

    const OtherPlayer &WorldModel::getTeammateClosestFromBall() {
        return *teammatesFromBall[0].second;
    }

    std::vector<OtherPlayer *> &WorldModel::getTeammates() {
        return teammates;
    }

    std::vector<OtherPlayer *> &WorldModel::getOpponents() {
        return opponents;
    }

    const std::vector<std::pair<double, OtherPlayer *> > &WorldModel::getOtherPlayersFromSelf() {
        return otherPlayersFromSelf;
    }

    const std::vector<std::pair<double, OtherPlayer *> > &WorldModel::getOtherPlayersFromBall() {
        return otherPlayersFromBall;
    }

    const std::vector<std::pair<double, OtherPlayer *> > &WorldModel::getTeammatesFromSelf() {
        return teammatesFromSelf;
    }

    const std::vector<std::pair<double, OtherPlayer *> > &WorldModel::getTeammatesFromBall() {
        return teammatesFromBall;
    }

    const std::vector<std::pair<double, OtherPlayer *> > &WorldModel::getOpponentsFromSelf() {
        return opponentsFromSelf;
    }

    const std::vector<std::pair<double, OtherPlayer *> > &WorldModel::getOpponentsFromBall() {
        return opponentsFromBall;
    }

    const std::vector<std::pair<double, OtherPlayer *> > &WorldModel::getOpponentsFromOurGoal() {
        return opponentsFromOurGoal;
    };

    const std::string &WorldModel::getOurTeamName() {
        return ourTeamName;
    }

    const std::string &WorldModel::getTheirTeamName() {
        return theirTeamName;
    }

    representations::PlaySide::PLAY_SIDE WorldModel::getOurPlaySide() {
        return ourPlaySide;
    }

    representations::PlaySide::PLAY_SIDE WorldModel::getTheirPlaySide() {
        return theirPlaySide;
    }

    representations::PlayMode::PLAY_MODE WorldModel::getPlayMode() {
        return playMode;
    }

    std::vector<int> WorldModel::getMyRoleAssignmentVector() {
        return positioningModel.getMyRoleAssignmentVector();
    }

    void WorldModel::updateMyRoleAssignmentVector(std::vector<int> newRoleVector) {
        positioningModel.setMyRoleAssignmentVector(newRoleVector);
    }

    std::vector<int> WorldModel::getSynchronizedRoleAssignmentVector() {
        return positioningModel.getSynchronizedRoleAssignmentVector();
    }

    std::vector<std::vector<int> > WorldModel::getTeammatesRoleAssignmentVector() {
        return positioningModel.getTeammatesRoleAssignmentVector();
    }

    representations::GameState &WorldModel::getGameState() {
        return gameState;
    }

    representations::GameSituation::GAME_SITUATION WorldModel::getGameSituation() {
        return gameSituation;
    }


    void WorldModel::setOurTeamName(std::string name) {
        ourTeamName = name;
    }

    bool WorldModel::selfIsClosestToBall() {
        if (teammatesFromBall.size() > 0) {
            if (selfDistanceToBall <= teammatesFromBall[0].first)
                return true;
            return false;
        }
        return true;
    }

    bool WorldModel::teammateIsClosestToBall() {
        return !selfIsClosestToBall();
    }

    bool WorldModel::selfIsSecondClosestToBall() {
        if (teammateIsClosestToBall() && selfDistanceToBall <= teammatesFromBall[1].first)
            return true;
        return false;
    }

    bool WorldModel::selfIsThirdClosestToBall() {
        if (selfDistanceToBall > teammatesFromBall[1].first && selfDistanceToBall <= teammatesFromBall[2].first)
            return true;
        return false;
    }

    void WorldModel::sortTeammatesFromBall() {
        // @todo add another informations: if the player has fallen, direction...
        Pose2D nearestPost;
        double agentDistToNearestPost;
        double ballDistToNearestPost;

        if (teammatesFromBall.size() > 1) {
            for (auto &teammates : teammatesFromBall) {

                // Goalie should not be closest if ball is in own goal box
                if (teammates.second->isGoalie() && !isBallInOurGoalBox()) {
                    teammates.first += 1000;
                }

                // Agent has fallen but not right on top of ball
                if (teammates.second->isFallen() && teammates.first < 6.50) {
                    teammates.first += 1.50;
                }

                // Ball is to the sides of the goals
                if (abs(ballModel.getObject().getPosition().translation.y) >
                    FieldDescription::GOAL_AREA_WIDTH / 2) {
                    // Ball is close to own goal
                    if (ballModel.getObject().getPosition().translation.x <
                        -1.0 * 0.5 * FieldDescription::FIELD_LENGTH -
                        (0.5 * FieldDescription::FIELD_WIDTH - 0.5 * FieldDescription::GOAL_AREA_WIDTH)) {
                        if (ballModel.getObject().getPosition().translation.y > 0)
                            nearestPost = (0, Vector2<double>(-0.5 * FieldDescription::FIELD_LENGTH,
                                                              0.5 * FieldDescription::FIELD_WIDTH));
                        else
                            nearestPost = (0, Vector2<double>(-0.5 * FieldDescription::FIELD_LENGTH,
                                                              -0.5 * FieldDescription::FIELD_WIDTH));

                        agentDistToNearestPost = (nearestPost - selfModel.getObject().getPosition().translation).abs();
                        ballDistToNearestPost = (nearestPost - ballModel.getObject().getPosition().translation).abs();

                        // Agent is in front of ball
                        if (agentDistToNearestPost > ballDistToNearestPost)
                            teammates.first += 1.0;

                    }
                    // Ball is close to opponent's goal
                    if (ballModel.getObject().getPosition().translation.x >
                        0.5 * FieldDescription::FIELD_LENGTH -
                        (0.5 * FieldDescription::FIELD_WIDTH - 0.5 * FieldDescription::GOAL_AREA_WIDTH)) {
                        if (ballModel.getObject().getPosition().translation.y > 0)
                            nearestPost = (0, Vector2<double>(0.5 * FieldDescription::FIELD_LENGTH,
                                                              0.5 * FieldDescription::FIELD_WIDTH));
                        else
                            nearestPost = (0, Vector2<double>(0.5 * FieldDescription::FIELD_LENGTH,
                                                              -0.5 * FieldDescription::FIELD_WIDTH));

                        agentDistToNearestPost = (nearestPost - selfModel.getObject().getPosition()).abs();
                        ballDistToNearestPost = (nearestPost - ballModel.getObject().getPosition()).abs();

                        // Agent is in front of ball
                        if (agentDistToNearestPost < ballDistToNearestPost)
                            teammates.first += 1.0;

                    }
                }
                    // Agent is in front of ball
                else if (teammates.second->getPosition().translation.x >
                         ballModel.getObject().getPosition().translation.x) {
                    teammates.first += 1.0; // Added distance to walk around ball'
                }

            }
        }

        qsort(&teammatesFromBall[0], teammatesFromBall.size(),
              sizeof(std::pair<double, OtherPlayer *>), sortFunction);
    }

    void WorldModel::sortTeammatesFromSelf() {
        qsort(&teammatesFromSelf[0], teammatesFromSelf.size(),
              sizeof(std::pair<double, OtherPlayer *>), sortFunction);
    }

    void WorldModel::sortOpponentsFromSelf() {
        qsort(&opponentsFromSelf[0], opponentsFromSelf.size(),
              sizeof(std::pair<double, OtherPlayer *>), sortFunction);

    }

    void WorldModel::sortOpponentsFromBall() {
        qsort(&opponentsFromBall[0], opponentsFromBall.size(),
              sizeof(std::pair<double, OtherPlayer *>), sortFunction);
    }

    void WorldModel::sortOpponentsFromOurGoal() {
        qsort(&opponentsFromOurGoal[0], opponentsFromOurGoal.size(),
              sizeof(std::pair<double, OtherPlayer *>), sortFunction);
    }

    int WorldModel::sortFunction(const void *a, const void *b) {
        if ((*(std::pair<double, OtherPlayer *> *) a).first
            < (*(std::pair<double, OtherPlayer *> *) b).first)
            return -1;
        if ((*(std::pair<double, OtherPlayer *> *) a).first
            == (*(std::pair<double, OtherPlayer *> *) b).first)
            return 0;
        else
            return 1;
    }

    int WorldModel::getVisibleObjectsCounter() {
        return visibleObjectsCounter;
    }

    FieldDescription &WorldModel::getFieldDescription() {
        return fieldDescription;
    }

    bool WorldModel::selfIsWithBall() {
        bool correctOrientation;
        auto relativeBallPosition =
                ballModel.getObject().getPosition().translation - selfModel.getObject().getPosition().translation;
        correctOrientation = MathUtils::normalizeAngle(
                std::abs(relativeBallPosition.angle() - selfModel.getObject().getPosition().rotation))
                             < itandroids_lib::math::MathUtils::degreesToRadians(10);
        return (selfDistanceToBall < representations::NavigationParams::getNavigationParams().agent_radius) &&
               correctOrientation;
    }

    void WorldModel::updateGameSituation() {
        gameSituation = modeling::GameSituation::ATTACKING;
    }

    bool WorldModel::isBallInOurGoalBox() {
        return (ballModel.getObject().getPosition().translation.x < FieldDescription::FIELD_LENGTH / 2 +
                                                                    FieldDescription::GOAL_AREA_LENGTH) &&
               (abs(ballModel.getObject().getPosition().translation.y) <
                FieldDescription::GOAL_AREA_LENGTH);
    }

    int WorldModel::getOpponentToTalk() {
        return opponentToTalk;
    }

    void WorldModel::newOpponentToTalk() {
        opponentToTalk = opponentToTalk + 4;
        if (opponentToTalk > NUM_PLAYERS_PER_TEAM)
            opponentToTalk = opponentToTalk - NUM_PLAYERS_PER_TEAM;
    }

} /* namespace modeling */
