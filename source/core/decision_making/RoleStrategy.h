//
// Created by dicksiano on 27/05/16.
//

#ifndef ITANDROIDS_SOCCER3D_CPP_ROLESTRATEGY_H
#define ITANDROIDS_SOCCER3D_CPP_ROLESTRATEGY_H

#include "role/Role.h"
#include "modeling/Modeling.h"

namespace decision_making {

    class RoleStrategy {
    public:
        virtual ~RoleStrategy() {}

        virtual role::Role *getRole(modeling::Modeling &modeling) = 0;
    };

/**
 * Class that represents which role an agent plays.
 */
    class RoleStrategyImpl : public RoleStrategy {
    public:
        /**
         * Default constructor for the Role Strategy
         */
        RoleStrategyImpl();

        /**
         * Default destructor for the Role Strategy
         */
        virtual ~RoleStrategyImpl();

        /**
         * Return the agent role
         *
         * @return role
         */
        role::Role *getRole(modeling::Modeling &modeling) override;

    private:
        /**
         * This map holds the role for each player.
         */
        role::Role *roles[11];
        //std::map<int, role::Role *> roles;
    };

} /* namespace decision_making */


#endif //ITANDROIDS_SOCCER3D_CPP_ROLESTRATEGY_H
