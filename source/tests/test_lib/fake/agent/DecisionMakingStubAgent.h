//
// Created by francisco on 18/01/16.
//

#ifndef ITANDROIDS_SOCCER3D_CPP_DECISONSTUBAGENT_H
#define ITANDROIDS_SOCCER3D_CPP_DECISONSTUBAGENT_H


#include <action/Action.h>
#include "communication/Communication.h"
#include "communication/ServerSocket.h"
#include "perception/Perception.h"
#include "control/Control.h"
#include "modeling/Modeling.h"
#include "action/Action.h"
#include "decision_making/DecisionMakingStub.h"


namespace agent {

    struct DecisionMakingStubAgent {
        DecisionMakingStubAgent(std::string host, int port, int controlId);

        void start(int agentNumber);

        communication::Communication communication;
        perception::Perception perception;
        modeling::Modeling modeling;
        decision_making::DecisionMakingStub decisionMaking;
        control::ControlImpl control;
        action::ActionImpl action;
    };

}


#endif //ITANDROIDS_SOCCER3D_CPP_DECISONSTUBAGENT_H
