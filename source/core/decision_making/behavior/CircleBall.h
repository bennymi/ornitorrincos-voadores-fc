//
// Created by mmaximo on 05/06/16.
//

#ifndef ITANDROIDS_SOCCER3D_CPP_CIRCLEBALL_H
#define ITANDROIDS_SOCCER3D_CPP_CIRCLEBALL_H

#include "decision_making/behavior/Behavior.h"
#include "decision_making/behavior/NavigationHelper.h"
#include "control/AngleController.h"

namespace decision_making {
    namespace behavior {

        class CircleBall : public Behavior {
        public:
            /**
             * Constructor that receives the behavior factory pointer
             */
            CircleBall(BehaviorFactory *factory, double radius = DEFAULT_RADIUS,
                       double facingErrorThreshold = DEFAULT_FACING_ERROR_THRESHOLD,
                       double distanceErrorThreshold = DEFAULT_DISTANCE_ERROR_THRESHOLD);

            void behave(modeling::Modeling &modeling) override;

            void setCircleRadius(double radius);

            void setDribbleDirection(double dribbleAngle);

            Pose2D getCircleBallPose(Vector2<double> ballPosition);

        private:
            static constexpr double DEFAULT_RADIUS = 0.2;
            static constexpr double DEFAULT_FACING_ERROR_THRESHOLD = 30.0;
            static constexpr double DEFAULT_DISTANCE_ERROR_THRESHOLD = 0.4;

            NavigationHelper navigationHelper;
            double dribbleAngle;
            double facingErrorThreshold;
            double distanceErrorThreshold;
            double radius;
        };

    } /* namespace behavior */
} /* namespace decision_making */

#endif //ITANDROIDS_SOCCER3D_CPP_CIRCLEBALL_H
