//
// Created by dicksiano on 21/06/16.
//

#include "base/BaseAgentTest.h"

class OwnKickOff_AgentTest : public BaseAgentTest {

public:

    virtual void testLoop(int nSteps) { BaseAgentTest::testLoop(nSteps); }


    virtual void setup() {
        wiz.setPlayMode(representations::RawPlayMode::KICK_OFF_LEFT);
    }

    virtual void runStep() {
        wiz.setPlayMode(representations::RawPlayMode::KICK_OFF_LEFT);

        utils::roboviz::Roboviz roboviz1;
        std::string name1("animated");
        std::string buff1("animated");
        roboviz1.swapBuffers(&buff1);


        roboviz1.drawLine(-3.0, 0.0, 0, -3.0, 0.0, 1, 1, 20, 20, 20, &name1);
        roboviz1.drawLine(3.0, 0.0, 0, 3.0, 0.0, 1, 1, 20, 20, 20, &name1);

        Pose2D selfPosition = modeling.getWorldModel().getSelf().getPosition();
        roboviz1.drawLine(selfPosition.translation.x, selfPosition.translation.y, 0,
                          selfPosition.translation.x, selfPosition.translation.y, 1,
                          1, 20, 20, 20, &name1);


        if (!decisionMaking.decideFall(modeling)) {
            behaviorFactory.getSetPlay().behave(modeling); // Behavior!
            decisionMaking.movementRequest = behaviorFactory.getSetPlay().getMovementRequest();
            //decisionMaking.beamRequest = behaviorFactory.getSetPlay().getBeamRequest();
            std::cout << "hello" << std::endl;
            behaviorFactory.getActiveVision().behave(modeling); // Vision!
            decisionMaking.lookRequest = behaviorFactory.getActiveVision().getLookRequest();
        }
    }
};

int main() {

    OwnKickOff_AgentTest agent; // Agent test
    agent.testLoop(500000); // Run the test!
    return 0;

}