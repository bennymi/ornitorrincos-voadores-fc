//
// Created by francisco on 18/01/16.
//

#include "DecisionMakingStubAgent.h"

namespace agent {

    DecisionMakingStubAgent::DecisionMakingStubAgent(std::string host, int port, int controlId) : communication(
            new communication::ServerSocketImpl(host, port)), control(controlId) {

    }

    void DecisionMakingStubAgent::start(int agentNumber) {
        communication.establishConnection();
        action.create(agentNumber);
        communication.sendMessage(action.getServerMessage());
        communication.receiveMessage();
        perception.perceive(communication);
        action.init(agentNumber, representations::ITAndroidsConstants::TEAM_DEFAULT_NAME);
        communication.sendMessage(action.getServerMessage());
    }
}