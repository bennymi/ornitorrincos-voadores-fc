/*
 * VisibleLine.cpp
 *
 *  Created on: Sep 1, 2015
 *      Author: itandroids
 */

#include "VisibleLine.h"

namespace perception {

    /*
     * Constructor to the case of no data.
     */
    VisibleLine::VisibleLine() {
        position2 = Vector3<double>();
    }

    /**
     * This constructor takes a parser tree node and manipulates it to get information
     * of visible line - name and position
     */
    VisibleLine::VisibleLine(utils::parser::ParserTreeNode &node) :
            VisibleObject(node) {
        name = std::string("L");
        std::string visibleLineString2 = utils::parser::KeyValuePair(
                node.children[1]->content, ' ').value;
        std::vector<std::string> stringVector2;
        boost::split(stringVector2, visibleLineString2, boost::is_any_of(" "));
        Vector3<double> auxVector(
                MathUtils::degreesToRadians(atof(stringVector2[1].c_str())),
                MathUtils::degreesToRadians(atof(stringVector2[2].c_str())));
        position2 = Vector3<double>(atof(stringVector2[0].c_str()),
                                    auxVector);
    }

    VisibleLine::~VisibleLine() {
    }

    Vector3<double> VisibleLine::getPosition2() {
        return position2;
    }

/**
 * get's the line's second distance
 * @return second line distance
 */
    double VisibleLine::getDistance2() {
        return position2.abs();
    }

/**
 * get's the line's second Horizontal Angle
 * @return horizontal angle 2
 */
    double VisibleLine::getHorizontalAngle2() {
        return position2.getAlpha();
    }

/**
 * get's the line's second Horizontal Angle
 * @return horizontal angle 2 in degrees
 */
    double VisibleLine::getHorizontalAngleDeg2() {
        return itandroids_lib::math::MathUtils::radiansToDegrees(
                position2.getAlpha());
    }

/**
 * get's the line's second latitude angle
 * @return latitude angle
 */
    double VisibleLine::getLatitudeAngle2() {
        return position2.getDelta();
    }

/**
 * get's the line's second latitude angle
 * @return latitude angle in degrees
 */
    double VisibleLine::getLatitudeAngleDeg2() {
        return itandroids_lib::math::MathUtils::radiansToDegrees(
                position2.getDelta());
    }

    void VisibleLine::transformPosition(Pose3D &transform) {
        VisibleObject::transformPosition(transform);
        position2 = transform * position2;
    }

} /* namespace perception */
