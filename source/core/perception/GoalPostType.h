/*
 * GoalPostType.h
 *
 *  Created on: Sep 11, 2015
 *      Author: itandroids
 */

#ifndef SOURCE_PERCEPTION_GOALPOSTTYPE_H_
#define SOURCE_PERCEPTION_GOALPOSTTYPE_H_

namespace perception {
    class GoalPostType {
    public:
        enum GOAL_POST_TYPE {
            LEFT_FIRST_POST,
            LEFT_SECOND_POST,
            RIGHT_FIRST_POST,
            RIGHT_SECOND_POST,
        };
    };
} /* namespace perception */



#endif /* SOURCE_PERCEPTION_GOALPOSTTYPE_H_ */
