/*
 * FocusBall.h
 *
 *  Created on: Nov 1, 2015
 *      Author: mmaximo
 */

#ifndef SOURCE_DECISION_MAKING_BEHAVIOR_FOCUSBALL_H_
#define SOURCE_DECISION_MAKING_BEHAVIOR_FOCUSBALL_H_

#include "control/LookRequest.h"
#include "modeling/Modeling.h"
#include "Behavior.h"


namespace decision_making {
    namespace behavior {

        using namespace itandroids_lib::math;

        class BehaviorFactory;

        enum FocusBallState {
            SCANNING, FOCUSING_BALL
        };

        class FocusBall : public Behavior {
        public:

            /**
             * Constructor for FocusBall.
             */
            FocusBall();

            /**
             * Assignment constructor
             *
             * @param behaviorFactory pointer to BehaviorFactory object.
             */
            FocusBall(BehaviorFactory *factory);

            /**
             * Destructor.
             */
            virtual ~FocusBall();

            /**
             * Keeps the robot looking near the ball position.
             *
             * @param modeling.
             */
            void behave(modeling::Modeling &modeling);

            /**
             * Method that returns look request.
             */
            std::shared_ptr<control::LookRequest> getLookRequest();

            /**
             * Turns the robot head.
             *
             * @param modeling.
             * @param time how much the robot will turn his head.
             */
            void turnHead(modeling::Modeling &modeling, double time);

            /**
             * Sets look request so the robot keeps in the ball direction
             *
             * @param modeling.
             */
            void focusOnTheBall(modeling::Modeling &modeling);

        private:
            static const double SCAN_TIME;
            static const double FOCUSING_TIME;
            static const double PAN_AMPLITUDE;
            static const double TILT_AMPLITUDE;

            static const double LOOK_AROUND_DELAY;
            static const double TIME_TO_LOOK_AROUND;

            bool first;

            FocusBallState state;
            std::shared_ptr<control::LookRequest> lookRequest;
            double lastStateSwitchTime;
            double timeToTurnHead;

            /**
             * Verifies if the robot should look to ohter directions
             */
            bool isBetterScan(modeling::Modeling &modeling);
        };

    } /* namespace behavior */
} /* namespace decision_making */

#endif /* SOURCE_DECISION_MAKING_BEHAVIOR_FOCUSBALL_H_ */
