//
// Created by muzio on 29/04/16.
//

#ifndef ITANDROIDS_SOCCER3D_CPP_BASEAGENTTEST_H
#define ITANDROIDS_SOCCER3D_CPP_BASEAGENTTEST_H

#include "communication/Communication.h"
#include "utils/wizard/Wizard.h"
#include "action/Action.h"
#include "decision_making/DecisionMakingStub.h"
#include "utils/roboviz/Roboviz.h"
#include "utils/configure_tree/ParameterTreeCreator.h"

class BaseAgentTest {
public:
    BaseAgentTest(std::string host = "127.0.0.1", int agentPort = 3100, int monitorPort = 3200, int agentNumber = 1,
                  int robotType = 0, std::string teamName = std::string("ITAndroids"));

    virtual ~BaseAgentTest();

    /**
     * This method is responsible for running the loop.
     */
    virtual void testLoop(int numberOfSteps);

    /**
     * This abstract method is to be implemented with one step
     * of the agent update.
     */
    virtual void runStep() = 0;

    /**
     * This abstract method is to be implemented with the initialization
     * logic for the test.
     */
    virtual void setup() = 0;

    virtual void finish();

    utils::wizard::Wizard wiz;

protected:
    const double STEP_DT = 0.020;
    int agentNumber;
    int currentStep;
    double elapsedTime = 0.0;
    communication::Communication communication;
    communication::Communication wizCommunication;
    decision_making::behavior::BehaviorFactory behaviorFactory;
    action::ActionImpl action;
    decision_making::DecisionMakingStub decisionMaking;
    control::ControlImpl control;
    perception::Perception perception;
    modeling::Modeling modeling;
    representations::HearData hearData;
    utils::roboviz::Roboviz roboviz;
    const int agentPort;
    const int monitorPort;
};

#endif //ITANDROIDS_SOCCER3D_CPP_BASEAGENTTEST_H
