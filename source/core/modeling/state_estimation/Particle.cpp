//
// Created by muzio on 16/04/16.
//

#include "Particle.h"

namespace modeling {
    namespace state_estimation {

        Particle::Particle() {

        }

        Particle::Particle(Pose2D pose)
                : pose(pose) {
        }

        Particle::~Particle() {

        }

        const Pose2D &Particle::getPose() const {
            return pose;
        }

        double Particle::getWeight() const {
            return weight;
        }

        void Particle::setPose(const Pose2D &pose) {
            this->pose.translation = pose.translation;
            this->pose.rotation = pose.rotation;
        }

        void Particle::setWeight(double weight) {
            this->weight = weight;
        }

    } /* namespace state_estimation */
} /* namespace modeling */



