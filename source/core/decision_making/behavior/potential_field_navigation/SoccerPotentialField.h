//
// Created by mmaximo on 20/06/16.
//

#ifndef ITANDROIDS_SOCCER3D_CPP_SOCCERPOTENTIALFIELD_H
#define ITANDROIDS_SOCCER3D_CPP_SOCCERPOTENTIALFIELD_H

#include "artificial_intelligence/potential_field/PotentialField.h"
#include "artificial_intelligence/potential_field/PotentialFieldObject.h"
#include "artificial_intelligence/potential_field/PotentialFieldFunctions.h"
#include "decision_making/behavior/potential_field_navigation/PotentialFieldParameters.h"
#include "modeling/FieldDescription.h"

namespace decision_making {
    namespace behavior {
        namespace potential_field_navigation {

            using itandroids_lib::artificial_intelligence::potential_field::PotentialField;
            using itandroids_lib::artificial_intelligence::potential_field::PotentialFieldObject;
            using itandroids_lib::artificial_intelligence::potential_field::PotentialFieldFunctions;
            using itandroids_lib::math::Vector2;
            using decision_making::behavior::potential_field_navigation::PotentialFieldParameters;
            using modeling::FieldDescription;

            class SoccerPotentialField {
            public:
                SoccerPotentialField(PotentialFieldParameters parameters);

                Vector2<double> calculateFieldDirection(const Vector2<double> &selfPosition);

                void clear();

                void addOpponent(Vector2<double> opponentPosition, bool counterClockwise);

                void addTeammate(Vector2<double> teammatePosition, bool counterClockwise);

                void addOpponent(Vector2<double> opponentPosition);

                void addTeammate(Vector2<double> teammatePosition);

                void addXFieldBorder(Vector2<double> selfPosition);

                void addYFieldBorder(Vector2<double> selfPosition);

                void addTargetDipole(Vector2<double> position, double angle);

                void addTarget(Vector2<double> position);

                void addTargetDipole(Vector2<double> position, double angle, double unitAttraction,
                                     double inverseAttraction);

                void addTarget(Vector2<double> position, double unitAttraction, double inverseAttraction);

                void addDipole(Vector2<double> position, double angle);

                void addRandomField();

            private:
                PotentialField potentialField;
                PotentialFieldParameters parameters;
                std::vector<PotentialFieldObject> objects;
                FieldDescription fieldDescription;
            };

        } /* namespace potential_field_navigation */
    } /* namespace behavior */
} /* namespace decision_making */

#endif //ITANDROIDS_SOCCER3D_CPP_SOCCERPOTENTIALFIELD_H
