//
// Created by dicksiano on 8/21/16.
//

#include "base/BaseAgentTest.h"

class OwnOffside_AgentTest : public BaseAgentTest {

public:

    virtual void testLoop(int nSteps) { BaseAgentTest::testLoop(nSteps); }


    virtual void setup() {
        wiz.setPlayMode(representations::RawPlayMode::OFFSIDE_LEFT);
    }

    virtual void runStep() {
        wiz.setPlayMode(representations::RawPlayMode::OFFSIDE_LEFT);

        utils::roboviz::Roboviz roboviz1;
        std::string name1("animated");
        std::string buff1("animated");
        roboviz1.swapBuffers(&buff1);


        roboviz1.drawLine(-3.0, 0.0, 0, -3.0, 0.0, 1, 1, 20, 20, 20, &name1);
        roboviz1.drawLine(3.0, 0.0, 0, 3.0, 0.0, 1, 1, 20, 20, 20, &name1);

        Pose2D selfPosition = modeling.getWorldModel().getSelf().getPosition();
        roboviz1.drawLine(selfPosition.translation.x, selfPosition.translation.y, 0,
                          selfPosition.translation.x, selfPosition.translation.y, 1,
                          1, 20, 20, 20, &name1);

        if (!decisionMaking.decideFall(modeling)) {
            behaviorFactory.getSetPlay().behave(modeling); // Behavior!
            decisionMaking.movementRequest = behaviorFactory.getSetPlay().getMovementRequest();

            behaviorFactory.getActiveVision().behave(modeling); // Vision!
            decisionMaking.lookRequest = behaviorFactory.getActiveVision().getLookRequest();
        }
    }
};

int main() {

    /**
     * Start the external agent
     */
    std::vector<std::thread> threads;
    for (int i = 2; i < 3; i++) {
        std::stringstream stream;
        stream << "./DeadBall_AgentTest 127.0.0.1 3100 3200 " << i << " &";
        system(stream.str().c_str());
        stream.str("");
        stream << "sleep 5";
        system(stream.str().c_str());
    }

    OwnOffside_AgentTest agent; // Agent test
    agent.testLoop(50000); // Run the test!
    return 0;

}