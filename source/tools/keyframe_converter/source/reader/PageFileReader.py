# -*- coding: utf-8 -*-

from reader import KeyframeFileReader
from representations import KeyframeMovement
from representations import KeyframeStep


class PageFileReader(KeyframeFileReader.KeyframeFileReader):

    def __init__(self, inputFile):
        super(PageFileReader, self).__init__(inputFile)

    def readFile(self):
        name = ""
        interpolator = ""
        speedRate = None
        steps = []
        with open(self.inputFile) as f:
            for line in f:
                splittedLine = line.rstrip().split(" ")
                if splittedLine[0] == "name":
                    name = splittedLine[2]
                if splittedLine[0] == "step":

                    tempJoints = list(map(float, splittedLine[2:-2]))
                    tempTime = float(splittedLine[-1])
                    step = KeyframeStep.KeyframeStep()
                    step.setJointPositionsAsList(tempJoints)
                    step.time = tempTime

                    steps.append(step)
                if splittedLine[0] == "interpolator":
                    interpolator = splittedLine[2]
                if splittedLine[0] == "play_param":
                    speedRate = splittedLine[5]

        self.keyframeMovement = KeyframeMovement.KeyframeMovement(interpolator,
        steps, name, speedRate)
