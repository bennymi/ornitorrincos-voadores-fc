/*
 * Roboviz.h
 *
 *  Created on: Aug 8, 2015
 *      Author: mmaximo
 */

#ifndef SOURCE_UTILS_ROBOVIZ_ROBOVIZ_H_
#define SOURCE_UTILS_ROBOVIZ_ROBOVIZ_H_

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <string>
#include <math.h>
#include "RobovizDraw.h"
#include "RobovizCommunication.h"

namespace utils {
    namespace roboviz {

        class Roboviz {
        public:

            static const int TEST_DURATION = 10000;

            Roboviz();

            virtual ~Roboviz();

            void swapBuffers(const std::string *setName);

            void drawLine(float x1, float y1, float z1, float x2, float y2, float z2,
                          float thickness, float r, float g, float b,
                          const std::string *setName);

            void drawCircle(float x, float y, float radius, float thickness, float r,
                            float g, float b, const std::string *setName);

            void drawSphere(float x, float y, float z, float radius, float r, float g,
                            float b, const std::string *setName);

            void drawPoint(float x, float y, float z, float size, float r, float g,
                           float b, const std::string *setName);

            void drawPolygon(const float *v, int numVerts, float r, float g, float b,
                             float a, const std::string *setName);

            void drawAnnotation(const std::string *text, float x, float y, float z,
                                float r, float g, float b, const std::string *setName);

            float maxf(float a, float b);

            static const std::string ROBOVIZ_HOST;
            static const std::string ROBOVIZ_PORT;
            double angle;
        private:
            utils::roboviz::RobovizCommunication comm;
            utils::roboviz::RobovizDraw draw;

        };

    } /* namespace roboviz */
} /* namespace utils */

#endif /* SOURCE_UTILS_ROBOVIZ_ROBOVIZ_H_ */
