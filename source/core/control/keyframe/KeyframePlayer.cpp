/*
 * KeyframeMotionPlayer.cpp
 *
 *  Created on: Oct 6, 2015
 *      Author: itandroids
 */

#include "KeyframePlayer.h"

namespace control {

    namespace keyframe {


        KeyframePlayer::KeyframePlayer(std::string filepath) :
                filepath(filepath) {

        }

        KeyframePlayer::~KeyframePlayer() {

        }

        void KeyframePlayer::setPage(std::string name) {
            if (currentPage != nullptr)
                currentPage->reset();
            currentPage = manager.getPage(name);

        }


        representations::NaoJoints &
        KeyframePlayer::getDesiredJoints(representations::NaoJoints &perceivedJoints, double globalTime,
                                         bool isInverted) {
            return currentPage->getDesiredJoints(perceivedJoints, globalTime, isInverted);
        }


        KeyframePage &KeyframePlayer::getCurrentPage() {
            return (*currentPage);
        }


        void KeyframePlayer::update() {
//            parser.parse(filepath);
        }

        void KeyframePlayer::update(std::string folderPath) {
            filepath = folderPath;
            update();
        }

        bool KeyframePlayer::hasPageEnded(std::string keyframeType) {
            return manager.getPage(keyframeType)->hasFinished();
        }

    


    } /* namespace keyframe */

} /* namespace control */
