//
// Created by mmaximo on 05/06/16.
//

#include "decision_making/behavior/CircleBall.h"
#include "control/WalkRequest.h"

namespace decision_making {
    namespace behavior {


        CircleBall::CircleBall(BehaviorFactory *factory, double radius, double facingErrorThreshold,
                               double distanceErrorThreshold) : Behavior(factory),
                                                                radius(radius),
                                                                facingErrorThreshold(facingErrorThreshold),
                                                                distanceErrorThreshold(distanceErrorThreshold),
                                                                dribbleAngle(0.0) {
        }

        void CircleBall::behave(modeling::Modeling &modeling) {
            using itandroids_lib::math::Pose2D;
            using itandroids_lib::math::Vector2;

            Pose2D velocity;

            modeling::WorldModel &worldModel = modeling.getWorldModel();
            Vector2<double> ballPosition = worldModel.getBall().getPosition().translation;
            Vector2<double> selfPosition = worldModel.getSelf().getPosition().translation;
            Vector2<double> selfToBall = ballPosition - selfPosition;
            double facingAngle = selfToBall.angle();
            double selfAngle = worldModel.getSelf().getPosition().rotation;
            velocity.rotation = navigationHelper.controlAngle(facingAngle, selfAngle);


            double facingError = MathUtils::normalizeAngle(facingAngle - selfAngle);
            if (facingError < MathUtils::degreesToRadians(facingErrorThreshold)) {
                Vector2<double> selfToBallDirection = selfToBall;
                selfToBallDirection.normalize();
                Vector2<double> circleBallPosition = ballPosition - selfToBallDirection * radius;
                Vector2<double> selfToCircleBall = circleBallPosition - selfPosition;
                double dx = selfToCircleBall.x * selfToBallDirection.x + selfToCircleBall.y * selfToBallDirection.y;
                velocity.translation.x = 2.0 * dx;

                if (fabs(dx) < distanceErrorThreshold) {
                    Vector2<double> dribbleDirection = Vector2<double>(cos(dribbleAngle), sin(dribbleAngle));
                    Vector2<double> desiredPosition = ballPosition - dribbleDirection * radius;
                    Vector2<double> ballToDesired = desiredPosition - ballPosition;
                    Vector2<double> ballToSelf = selfPosition - ballPosition;
                    double ballToDesiredAngle = ballToDesired.angle();
                    double ballToSelfAngle = ballToSelf.angle();
                    double deltaAngle = MathUtils::normalizeAngle(ballToDesiredAngle - ballToSelfAngle);
                    velocity.translation.y = 2.0 * -deltaAngle * radius;//1.5 * -deltaAngle * radius;
                }
            }

            //navigationHelper.saturateWalkVelocity(velocity);

            velocity = navigationHelper.saturateWalkVelocity(velocity);

            movementRequest = std::make_shared<control::WalkRequest>(velocity);
        }

        Pose2D CircleBall::getCircleBallPose(Vector2<double> ballPosition) {
            Vector2<double> dribbleDirection = Vector2<double>(cos(dribbleAngle), sin(dribbleAngle));
            Vector2<double> desiredPosition = ballPosition - dribbleDirection * radius;
            return Pose2D(dribbleAngle, desiredPosition);
        }

        void CircleBall::setCircleRadius(double radius) {
            this->radius = radius;
        }

        void CircleBall::setDribbleDirection(double dribbleAngle) {
            this->dribbleAngle = dribbleAngle;
        }

    } /* namespace behavior */
} /* namespace decision_making */