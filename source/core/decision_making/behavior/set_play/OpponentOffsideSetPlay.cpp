//
// Created by dicksiano on 8/14/16.
//

#include "OpponentOffsideSetPlay.h"
#include "../BehaviorFactory.h"

namespace decision_making {
    namespace behavior {

        OpponentOffsideSetPlay::OpponentOffsideSetPlay() : SetPlay(nullptr) {
        }

        OpponentOffsideSetPlay::OpponentOffsideSetPlay(BehaviorFactory *behaviorFactory) : SetPlay(behaviorFactory) {
        }

        OpponentOffsideSetPlay::~OpponentOffsideSetPlay() {
        }

        void OpponentOffsideSetPlay::behave(modeling::Modeling &modeling) {
            /// IMPLEMENTE
        } /* function behave */

    } /* namespace behavior */
} /* namespace decision_making */