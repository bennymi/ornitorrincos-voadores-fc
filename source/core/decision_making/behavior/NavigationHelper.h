//
// Created by mmaximo on 05/06/16.
//

#ifndef ITANDROIDS_SOCCER3D_CPP_NAVIGATIONHELPER_H
#define ITANDROIDS_SOCCER3D_CPP_NAVIGATIONHELPER_H

#include "control/AngleController.h"
#include "math/Pose2D.h"

namespace decision_making {
    namespace behavior {

        using itandroids_lib::math::Pose2D;

        struct NavigationHelperParams {
            double maxForwardSpeed;
            double maxBackwardSpeed;
            double maxSideSpeed;
            double maxAngularSpeed;
            double angleBandwith;

            static NavigationHelperParams getDefaultParams();
        };

        /**
         * Auxiliary class that supports calculating parameters that will be used in Control.
         */
        class NavigationHelper {
        public:
            /**
             * Default constructor to Navigation Helper
             */
            NavigationHelper();

            /**
             * Constructor to Navigation Helper
             *
             * @param struct NavigationHelperParams
             */
            NavigationHelper(NavigationHelperParams params);

            /**
             * Method to control angle according to the refernece angle and the current angle.
             *
             * @param reference
             * @param actual
             */
            double controlAngle(double reference, double actual);

            /**
             * Method to saturate the walk velocity
             *
             * @param velocity
             * @return Pose2D with the saturated velocity
             */
            Pose2D saturateWalkVelocity(Pose2D &velocity);

        private:
            NavigationHelperParams params;
            itandroids_lib::control::AngleController angleController;
        };

    } /* namespace behavior */
} /* namespace decision_making */

#endif //ITANDROIDS_SOCCER3D_CPP_NAVIGATIONHELPER_H
