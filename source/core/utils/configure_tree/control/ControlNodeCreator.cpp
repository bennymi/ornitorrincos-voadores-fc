//
// Created by francisco on 19/12/16.
//

#include "ControlNodeCreator.h"
#include "StateMachineNodeCreator.h"
namespace utils{
namespace configure_tree{
namespace control{
template<>
std::shared_ptr<Parameter<pt::ptree,std::string>> ControlNodeCreator::generateNode(){
    auto controlNode = std::make_shared<BranchParameter<pt::ptree, std::string>>();

    auto stateMachineNode = StateMachineNodeCreator().generateNode<pt::ptree,std::string>();
    controlNode->addChild("stateMachine", stateMachineNode);
    return controlNode;
};
}
}
}