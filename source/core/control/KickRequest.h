/*
 * KickRequest.h
 *
 *  Created on: Oct 24, 2015
 *      Author: itandroids
 */

#ifndef SOURCE_CONTROL_KICKREQUEST_H_
#define SOURCE_CONTROL_KICKREQUEST_H_

#include "MovementRequest.h"

namespace control {

    class KickRequest : public MovementRequest {
    public:
        KickRequest();

        virtual ~KickRequest();
    };

} /* namespace control */

#endif /* SOURCE_CONTROL_KICKREQUEST_H_ */
