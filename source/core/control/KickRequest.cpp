/*
 * KickRequest.cpp
 *
 *  Created on: Oct 24, 2015
 *      Author: itandroids
 */

#include "KickRequest.h"

namespace control {

    KickRequest::KickRequest() : MovementRequest(MovementRequestType::KICK_REQUEST) {
    }

    KickRequest::~KickRequest() {

    }

} /* namespace control */
