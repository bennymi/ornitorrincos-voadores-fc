//
// Created by francisco on 5/19/16.
//

#ifndef ITANDROIDS_SOCCER3D_CPP_POTENTIALFIELDNAVIGATOR_H
#define ITANDROIDS_SOCCER3D_CPP_POTENTIALFIELDNAVIGATOR_H


#include "decision_making/behavior/Behavior.h"
#include "artificial_intelligence/potential_field/PotentialField.h"
#include "artificial_intelligence/potential_field/PotentialFieldObject.h"
#include "artificial_intelligence/potential_field/PotentialFieldFunctions.h"
#include "representations/NavigationParams.h"
#include "decision_making/behavior/potential_field_navigation/PotentialFieldParameters.h"
#include "control/AngleController.h"
#include "decision_making/behavior/NavigationHelper.h"
#include "decision_making/behavior/potential_field_navigation/SoccerPotentialField.h"

using ::itandroids_lib::artificial_intelligence::potential_field::PotentialField;
using ::itandroids_lib::artificial_intelligence::potential_field::PotentialFieldObject;
using ::itandroids_lib::artificial_intelligence::potential_field::PotentialFieldFunctions;
using ::itandroids_lib::math::Vector2;
using ::decision_making::behavior::potential_field_navigation::PotentialFieldParameters;
using ::decision_making::behavior::potential_field_navigation::SoccerPotentialField;

namespace decision_making {
    namespace behavior {
        class BehaviorFactory;

        /**
         * This is a temporary method, that must remaint until we can develop a better trajectory controller and generator
         */
        class PotentialFieldNavigation : public Behavior {
        public:
            /**
             * Constructor that receives the behavior factory pointer
             */
            PotentialFieldNavigation(BehaviorFactory *factory);

            /**
             * Constructor that receives the behavior factory pointer and the parameters of the potential field
             */
            PotentialFieldNavigation(BehaviorFactory *factory, PotentialFieldParameters parameters);

            /**
             * Set the potential field target.
             *
             * @param target the potential field target.
             */
            void setTarget(Vector2<double> target);

            /**
             * Behavior that creates a movement request in the direction to which the field is pointing to
             */
            void behave(modeling::Modeling &modeling) override;

            /**
             * Getter for the position that the field is recommending to go
             */
            const Vector2<double> &getFieldDirection();

            Vector2<double> calculateFieldDirection(Vector2<double> selfPosition, modeling::Modeling &modeling);

        protected:
            Vector2<double> getFieldDirection(const Vector2<double> &selfPosition);

        private:
            NavigationHelper navigationHelper;
            Vector2<double> fieldDirection;
            Vector2<double> target;
            SoccerPotentialField soccerPotentialField;

            static const double TEAMMATE_INFLUENCE_RADIUS;
            static const double OPPONENT_INFLUENCE_RADIUS;

        };

    }
}


#endif //ITANDROIDS_SOCCER3D_CPP_POTENTIALFIELDNAVIGATOR_H
