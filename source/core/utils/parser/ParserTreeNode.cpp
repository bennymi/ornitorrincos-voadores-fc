/*
 * ParserTreeNode.cpp
 *
 *  Created on: Aug 8, 2015
 *      Author: mmaximo
 */

#include "ParserTreeNode.h"

#include <iostream>

namespace utils {
    namespace parser {

        ParserTreeNode::ParserTreeNode() {
            count = 0;
        }

        ParserTreeNode::~ParserTreeNode() {
            std::vector<ParserTreeNode *>::iterator iter;
            for (iter = children.begin(); iter != children.end(); iter++) {
                delete (*iter);
            }
            children.clear();
        }

        std::string ParserTreeNode::toString() {
            std::string string = this->content;
            std::vector<ParserTreeNode *>::iterator it;
            for (it = children.begin(); it != children.end(); ++it) {
                if (!content.empty())
                    string += " ";
                string += "(" + (*it)->toString() + ")";
            }
            return string;
        }

        std::string ParserTreeNode::toMonitorString() {
            std::string string = this->content;
            std::vector<ParserTreeNode *>::iterator it;
            for (it = children.begin(); it != children.end(); ++it) {
                string += "\n";
                for (int i = 0; i <= count; i++) {
                    string += "   ";
                }
                string += "(" + (*it)->toMonitorString() + ")";
            }
            return string;
        }

    } /* namespace parser */
} /* namespace utils */
