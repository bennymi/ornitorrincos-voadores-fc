//
// Created by luckeciano on 8/15/16.
//

#ifndef ITANDROIDS_SOCCER3D_CPP_MARKINGSYSTEM_H
#define ITANDROIDS_SOCCER3D_CPP_MARKINGSYSTEM_H


#include "core/modeling/Modeling.h"
#include <vector>
#include "decision_making/positioning/DelaunayPositioning.h"
#include "SCRAMByMMDR.h"

namespace decision_making {
    namespace positioning {
        namespace role_assignment {

            class MarkingSystem {
                /* Marking System:
                 * 1 - Decide which works to mark
                 * 2 - Select which roles to use for marking purposes
                 * 3 - Use prioritized role assignment to assign players to positions
                 */

            public:
                MarkingSystem();

                std::vector<representations::OtherPlayer *> decideWhoMarks(modeling::Modeling &modeling);

                void selectRolesAndAssign(modeling::Modeling &modeling);

                std::vector<representations::OtherPlayer *> getOpponentToMark();


            private:
                decision_making::positioning::DelaunayPositioning delaunayPositioning;
                std::vector<representations::OtherPlayer *> opponentsToMark;

            };
        }
    }
}


#endif //ITANDROIDS_SOCCER3D_CPP_MARKINGSYSTEM_H
