//
// Created by dicksiano on 8/14/16.
//

#include "OpponentKickOffSetPlay.h"
#include "../BehaviorFactory.h"

namespace decision_making {
    namespace behavior {

        OpponentKickOffSetPlay::OpponentKickOffSetPlay() : SetPlay(nullptr) {
        }

        OpponentKickOffSetPlay::OpponentKickOffSetPlay(BehaviorFactory *behaviorFactory) : SetPlay(behaviorFactory) {
        }

        OpponentKickOffSetPlay::~OpponentKickOffSetPlay() {
        }

        void OpponentKickOffSetPlay::behave(modeling::Modeling &modeling) {
            goToDelaunayPositioning(modeling);
        } /* function behave */

    } /* namespace behavior */
} /* namespace decision_making */