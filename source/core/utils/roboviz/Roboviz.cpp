/*
 * Roboviz.cpp
 *
 *  Created on: Aug 8, 2015
 *      Author: mmaximo
 */


#include <stdio.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netdb.h>
#include <string>
#include "RobovizDraw.h"

using namespace std;

#include "Roboviz.h"

namespace utils {
    namespace roboviz {


        Roboviz::Roboviz() : comm(ROBOVIZ_HOST, ROBOVIZ_PORT) {
            comm.establishConnection();
            angle = 0;
        }

        Roboviz::~Roboviz() {
            comm.closeConnection();
        }

        void Roboviz::swapBuffers(const std::string *setName) {
            int bufSize = -1;
            unsigned char *buf = draw.newBufferSwap(setName, &bufSize);
            sendto(comm.sockfd, buf, bufSize, 0, comm.p->ai_addr, comm.p->ai_addrlen);
            delete[] buf;
        }

        void Roboviz::drawLine(float x1, float y1, float z1, float x2, float y2, float z2, float thickness, float r,
                               float g, float b,
                               const std::string *setName) {
            float pa[3] = {x1, y1, z1};
            float pb[3] = {x2, y2, z2};
            float color[3] = {r, g, b};

            int bufSize = -1;
            unsigned char *buf = draw.newLine(pa, pb, thickness, color, setName, &bufSize);
            sendto(comm.sockfd, buf, bufSize, 0, comm.p->ai_addr, comm.p->ai_addrlen);
            delete[] buf;
        }

        void Roboviz::drawCircle(float x, float y, float radius, float thickness, float r, float g, float b,
                                 const std::string *setName) {
            float center[2] = {x, y};
            float color[3] = {r, g, b};

            int bufSize = -1;
            unsigned char *buf = draw.newCircle(center, radius, thickness, color, setName, &bufSize);
            sendto(comm.sockfd, buf, bufSize, 0, comm.p->ai_addr, comm.p->ai_addrlen);
            delete[] buf;
        }

        void Roboviz::drawSphere(float x, float y, float z, float radius, float r, float g, float b,
                                 const std::string *setName) {
            float center[3] = {x, y, z};
            float color[3] = {r, g, b};

            int bufSize = -1;
            unsigned char *buf = draw.newSphere(center, radius, color, setName, &bufSize);
            sendto(comm.sockfd, buf, bufSize, 0, comm.p->ai_addr, comm.p->ai_addrlen);
            delete[] buf;
        }

        void Roboviz::drawPoint(float x, float y, float z, float size, float r, float g, float b,
                                const std::string *setName) {
            float center[3] = {x, y, z};
            float color[3] = {r, g, b};

            int bufSize = -1;
            unsigned char *buf = draw.newPoint(center, size, color, setName, &bufSize);
            sendto(comm.sockfd, buf, bufSize, 0, comm.p->ai_addr, comm.p->ai_addrlen);
            delete[] buf;
        }

        void Roboviz::drawPolygon(const float *v, int numVerts, float r, float g, float b,
                                  float a, const std::string *setName) {
            float color[4] = {r, g, b, a};

            int bufSize = -1;
            unsigned char *buf = draw.newPolygon(v, numVerts, color, setName, &bufSize);
            sendto(comm.sockfd, buf, bufSize, 0, comm.p->ai_addr, comm.p->ai_addrlen);
            delete[] buf;
        }

        void Roboviz::drawAnnotation(const std::string *text, float x, float y, float z, float r,
                                     float g, float b, const std::string *setName) {
            float color[3] = {r, g, b};
            float pos[3] = {x, y, z};

            int bufSize = -1;
            unsigned char *buf = draw.newAnnotation(text, pos, color, setName, &bufSize);
            sendto(comm.sockfd, buf, bufSize, 0, comm.p->ai_addr, comm.p->ai_addrlen);
            delete[] buf;
        }

        float Roboviz::maxf(float a, float b) {
            return a > b ? a : b;
        }

// Declaring the constants
        const std::string Roboviz::ROBOVIZ_HOST = "localhost";

        const std::string Roboviz::ROBOVIZ_PORT = "32769";


    } /* namespace roboviz */
} /* namespace utils */
