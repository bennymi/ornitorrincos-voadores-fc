//
// Created by mmaximo on 21/06/16.
//

#ifndef ITANDROIDS_SOCCER3D_CPP_DRIBLEBALL_H
#define ITANDROIDS_SOCCER3D_CPP_DRIBLEBALL_H

#include "decision_making/behavior/potential_field_navigation/SoccerPotentialField.h"
#include "decision_making/behavior/NavigationHelper.h"
#include "decision_making/behavior/Behavior.h"

namespace decision_making {
    namespace behavior {

        class BehaviorFactory;

        using ::itandroids_lib::artificial_intelligence::potential_field::PotentialField;
        using ::itandroids_lib::artificial_intelligence::potential_field::PotentialFieldObject;
        using ::itandroids_lib::artificial_intelligence::potential_field::PotentialFieldFunctions;
        using ::itandroids_lib::math::Vector2;
        using ::decision_making::behavior::potential_field_navigation::PotentialFieldParameters;
        using ::decision_making::behavior::potential_field_navigation::SoccerPotentialField;

        enum DribbleState {
            CIRCLE_BALL, FOLLOW_LINE
        };

        class DribbleBall : public Behavior {
        public:
            DribbleBall(BehaviorFactory *factory);

            DribbleBall(BehaviorFactory *factory, PotentialFieldParameters parameters);

            void behave(modeling::Modeling &modeling) override;

            Vector2<double> calculateDribbleDirection(Vector2<double> ballPosition, modeling::Modeling &modeling);

        private:
            DribbleState state;
            NavigationHelper navigationHelper;
            Vector2<double> fieldDirection;
            Vector2<double> target;
            SoccerPotentialField soccerPotentialField;

            static const double TEAMMATE_INFLUENCE_RADIUS;
            static const double OPPONENT_INFLUENCE_RADIUS;
        };

    } /* namespace behavior */
} /* namespace decision_making */

#endif //ITANDROIDS_SOCCER3D_CPP_DRIBLEBALL_H
