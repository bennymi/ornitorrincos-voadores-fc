//
// Created by francisco on 6/22/16.
//

#ifndef ITANDROIDS_SOCCER3D_CPP_SHORTDISTANCE5MKICK_H
#define ITANDROIDS_SOCCER3D_CPP_SHORTDISTANCE5MKICK_H

#include "KeyframeKick.h"

namespace decision_making {
    namespace behavior {
        namespace kick_behavior {
            class ShortDistance5mKick : public KeyframeKick {
            public:
                ShortDistance5mKick();
            };
        }
    }
}


#endif //ITANDROIDS_SOCCER3D_CPP_SHORTDISTANCE5MKICK_H
