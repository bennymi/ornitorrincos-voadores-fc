//
// Created by dicksiano on 27/05/16.
//

#ifndef ITANDROIDS_SOCCER3D_CPP_DELAUNAYPOSITIONING_H
#define ITANDROIDS_SOCCER3D_CPP_DELAUNAYPOSITIONING_H


#include "decision_making/positioning/delaunay_triangulation/DelaunayInterpolator.h"
#include "decision_making/positioning/delaunay_triangulation/DelaunayFormationFactory.h"
#include "decision_making/positioning/Positioning.h"
#include "math/Pose2D.h"
#include <vector>
#include <core/modeling/Modeling.h>

namespace modeling {
    class Modeling;
}

namespace decision_making {
    namespace positioning {

/**
 * Class that represents the behavior that controls the Delaunay positioning.
 */
        class DelaunayPositioning : public Positioning {
        public:
            /**
             * Constructor for Delaunay Positioning
             *
             * @param string that holds the folder path
             */
            DelaunayPositioning(std::string folderPath);

            /**
     if (playMode == representations::PlayMode::OPPONENT_GOAL_KICK) {
                dI = formations.getTriangulations()[delaunay_triangulation::DelaunayFormationType::OPPONENT_GOAL_KICK_FORMATION];
            } else if (playMode == representations::PlayMode::OWN_CORNER_KICK) {
                dI = formations.getTriangulations()[delaunay_triangulation::DelaunayFormationType::OWN_CONER_KICK_FORMATION];
            } else if (playMode == representations::PlayMode::OWN_GOAL_KICK) {
                dI = formations.getTriangulations()[delaunay_triangulation::DelaunayFormationType::OWN_GOAL_KICK_FORMATION];
            } else {
                dI = formations.getTriangulations()[delaunay_triangulation::DelaunayFormationType::DEFAULT_FORMATION];
            }        * Destructor
             */
            virtual ~DelaunayPositioning();

            /**
             * Calculates for which position the agent must go.
             *
             * @param agent number
             * @param modeling
             */
            virtual itandroids_lib::math::Pose2D getAgentPosition(modeling::Modeling &modeling);

            /**
             * Calculates for which position the agent and teammates must go.
             *
             * @param modeling
             */

            virtual std::vector<itandroids_lib::math::Vector2<double> > getTeamPosition(modeling::Modeling &modeling);

        private:
            delaunay_triangulation::DelaunayFormationFactory formations;
        };

    } /* namespace positioning */
} /* namespace decision_making */


#endif //ITANDROIDS_SOCCER3D_CPP_DELAUNAYPOSITIONING_H
