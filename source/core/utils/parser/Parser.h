/*
 * Parser.h
 *
 *  Created on: Aug 8, 2015
 *      Author: mmaximo
 */

#ifndef SOURCE_UTILS_PARSER_PARSER_H_
#define SOURCE_UTILS_PARSER_PARSER_H_

#include <string>

#include "utils/parser/ParserTreeNode.h"

namespace utils {
    namespace parser {

        class Parser {
        public:
            Parser();

            virtual ~Parser();

            ParserTreeNode *parseMessage(const std::string &message);

        private:
            int parseMessage(ParserTreeNode *node, const std::string &message,
                             int startIndex);

            int count;
        };

    } /* namespace parser */
} /* namespace utils */

#endif /* SOURCE_UTILS_PARSER_PARSER_H_ */
