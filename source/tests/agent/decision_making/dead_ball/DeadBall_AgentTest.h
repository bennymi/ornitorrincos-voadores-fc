//
// Created by dicksiano on 21/06/16.
//

#ifndef ITANDROIDS_SOCCER3D_CPP_DEADBALL_AGENTTEST_H
#define ITANDROIDS_SOCCER3D_CPP_DEADBALL_AGENTTEST_H

#include "decision_making/behavior/SetPlay.h"
#include "base/BaseAgentTest.h"

class DeadBall_AgentTest : public BaseAgentTest {

public:
    DeadBall_AgentTest(std::string host = "127.0.0.1", int agentPort = 3100, int monitorPort = 3200,
                       int agentNumber = 7);

    virtual void testLoop(int nSteps) { BaseAgentTest::testLoop(nSteps); }


    virtual void setup() {
    }

    virtual void runStep() {
        utils::roboviz::Roboviz roboviz1;
        std::string name1("animated");
        std::string buff1("animated");
        roboviz1.swapBuffers(&buff1);


        Pose2D selfPosition = modeling.getWorldModel().getSelf().getPosition();
        roboviz1.drawLine(selfPosition.translation.x, selfPosition.translation.y, 0,
                          selfPosition.translation.x, selfPosition.translation.y, 1,
                          1, 20, 20, 20, &name1);

        if (!decisionMaking.decideFall(modeling)) {
            behaviorFactory.getSetPlay().behave(modeling); // Behavior!
            decisionMaking.movementRequest = behaviorFactory.getSetPlay().getMovementRequest();
            decisionMaking.beamRequest = behaviorFactory.getSetPlay().getBeamRequest();

            behaviorFactory.getFocusBall().behave(modeling); // Vision!
            decisionMaking.lookRequest = behaviorFactory.getFocusBall().getLookRequest();
        }
    }
};


#endif //ITANDROIDS_SOCCER3D_CPP_DEADBALL_AGENTTEST_H
