/*
 * Keep.h
 *
 *  Created on: Oct 31, 2015
 *      Author: itandroids
 */

#ifndef SOURCE_DECISION_MAKING_BEHAVIOR_KEEP_H_
#define SOURCE_DECISION_MAKING_BEHAVIOR_KEEP_H_

#include "Behavior.h"

using namespace itandroids_lib::math;

namespace decision_making {
    namespace behavior {

        class Keep : public Behavior {
        public:

            /**
             * Default constructor.
             */
            Keep();

            /**
             *  Assignment constructor.
             *
             *  @param behaviorFactory pointer to BehaviorFactory object.
             */
            Keep(BehaviorFactory *behaviorFactory);

            /**
             * Destructor.
             */
            virtual ~Keep();

            /**
             * Makes the goalie jump to keep the ball.
             *
             * @param modeling.
             */
            virtual void behave(modeling::Modeling &modeling);

        private:

        };

    } /* namespace behavior */
} /* namespace decision_making */

#endif /* SOURCE_DECISION_MAKING_BEHAVIOR_KEEP_H_ */
