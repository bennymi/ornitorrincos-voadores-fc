#!/usr/bin/env bash

SERVER_IP=$1
SERVER_PORT=3100
TEAM_NAME="ITAndroids"
AGENT_NUM=1
cd binaries
./agent --server-ip $SERVER_IP --server-port $SERVER_PORT --team-name $TEAM_NAME --agent-number $AGENT_NUM &>/dev/null &