/*
 * Ball.
 *
 *  Created on: Aug 29, 2015
 *      Author: mmaximo
 */

#ifndef SOURCE_REPRESENTATIONS_BALL_H_
#define SOURCE_REPRESENTATIONS_BALL_H_

#include "math/Vector3.h"
#include "math/Pose2D.h"

namespace representations {

    using namespace itandroids_lib::math;

    class Ball {
    public:
        Ball();

        virtual ~Ball();

        bool isRelevant();

        void setRelevant(bool relevant);

        Pose2D &getPosition();

        Pose2D &getLocalPosition();

        Pose2D &getVelocity();

        Pose2D &getLocalVelocity();

        void setPosition(Pose2D pos);

        void setLocalPosition(Pose2D pos);

        void setVelocity(Pose2D velocity);

        void setLocalVelocity(Pose2D velocity);

        void setLastSeenTime(double globalTime);

        double getLastSeenTime();

        void setLastHeardTime(double globalTime);

        double getLastHeardTime();

        double getAge(double currentTime);

    private:
        Pose2D position;
        Pose2D localPosition;
        Pose2D velocity;
        Pose2D localVelocity;
        bool hasRelevance;
        double lastSeenTime;
        double lastHeardTime;
    };

} /* namespace representations */

#endif /* SOURCE_REPRESENTATIONS_BALL_H_ */
