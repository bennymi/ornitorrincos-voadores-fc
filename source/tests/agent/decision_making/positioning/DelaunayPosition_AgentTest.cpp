//
// Created by francisco on 5/31/16.
//

#include "DelaunayPosition_AgentTest.h"

DelaunayPosition_AgentTest::DelaunayPosition_AgentTest(std::string host, int agentPort, int monitorPort,
                                                       int agentNumber) : BaseAgentTest(host, agentPort, monitorPort,
                                                                                        agentNumber),
                                                                          agentNumber(agentNumber), positioning(
                representations::ITAndroidsConstants::FORMATION_FOLDER) {

    positionName = "animated.visibleObjects";
}

void DelaunayPosition_AgentTest::setup() {
    wiz.setAgentPositionAndDirection(agentNumber, representations::PlaySide::LEFT, Vector3<double>(-10, 0, 0.3), 0);
}

void DelaunayPosition_AgentTest::runStep() {

    auto targetPose = positioning.getAgentPosition(modeling);
    std::string buffer("animated");
    roboviz.drawLine((float) targetPose.translation.x, (float) targetPose.translation.y, 0,
                     (float) targetPose.translation.x, (float) targetPose.translation.y, 1, 1, 254, 0, 0,
                     &positionName);
    roboviz.swapBuffers(&buffer);
    behaviorFactory.getNavigatePosition().setTarget(targetPose);
    behaviorFactory.getNavigatePosition().behave(modeling);
    decisionMaking.movementRequest = behaviorFactory.getNavigatePosition().getMovementRequest();
}


int main(int argc, char *argv[]) {
    DelaunayPosition_AgentTest agent("127.0.0.1", 3100, 3200, std::stoi(argv[1]));
    agent.testLoop(5000);
    return 0;
}


