/*
 * DelaunayFileParser.cpp
 *
 *  Created on: Oct 28, 2015
 *      Author: itandroids
 */

#include "DelaunayFileParser.h"
#include "modeling/FieldDescription.h"

namespace decision_making {
    namespace positioning {
        namespace delaunay_triangulation {

            DelaunayFileParser::DelaunayFileParser(std::string filePath) {
                double xScale =
                        modeling::FieldDescription::FIELD_LENGTH / modeling::FieldDescription::SOCCER_2D_FIELD_LENGTH;
                double yScale =
                        modeling::FieldDescription::FIELD_WIDTH / modeling::FieldDescription::SOCCER_2D_FIELD_WIDTH;
                std::ifstream formation(filePath);
                std::string line;
                std::vector<std::string> strVector;
                int readCount = 0;
                while (getline(formation, line)) {
                    boost::split(strVector, line, boost::is_any_of(" "));
                    if (strVector.size() > 0 && strVector[0] == "Ball") {
                        readCount++;
                        predefinedPositions.push_back(std::vector<Vector2D>());
                        double x = xScale * atof(strVector[1].c_str());
                        double y = yScale * atof(strVector[2].c_str());
                        positions.push_back(Vector2D(x, y));
                        getline(formation, line);
                        boost::split(strVector, line, boost::is_any_of(" "));
                        while (strVector.size() > 0 && strVector[0] != "-----" && strVector[0] != "End") {
                            double xx = xScale * atof(strVector[1].c_str());
                            double yy = yScale * atof(strVector[2].c_str());
                            (predefinedPositions.end() - 1)->push_back(Vector2D(xx, yy));
                            if (getline(formation, line)) {
                                boost::split(strVector, line, boost::is_any_of(" "));
                            }
                        }
                    }
                    if (strVector.size() > 0 && strVector[0] == "-----") {

                    }
                }
            }

            DelaunayFileParser::~DelaunayFileParser() {
            }

            std::vector<Vector2D> DelaunayFileParser::getPositions() {
                return positions;
            }

            std::vector<std::vector<Vector2D> > DelaunayFileParser::getPredefinedPositions() {
                return predefinedPositions;
            }

        } /* namespace delaunay_triangulation */
    }/* namespace positioning */
} /* namespace decision_making */

