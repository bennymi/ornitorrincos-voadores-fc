//
// Created by dicksiano on 27/05/16.
//

#ifndef ITANDROIDS_SOCCER3D_CPP_ATTACKINGROLE_H
#define ITANDROIDS_SOCCER3D_CPP_ATTACKINGROLE_H

#include "modeling/Modeling.h"
#include "Role.h"
#include "decision_making/positioning/DelaunayPositioning.h"
#include "math/Pose2D.h"
#include "math/Vector2.h"

namespace decision_making {
    namespace role {

/**
 * Class that represents an attack role.
 */
        class AttackingRole : public Role {
        public:

            /**
             * Default constructor for the attacking Role.
             */
            AttackingRole();

            /**
             * Default destructor for the attacking Role.
             */
            virtual ~AttackingRole();

            /**
             * Decides what actions a soccer player who plays a
             * attacking role should execute.
             */

        private:
            void behaveMovement(modeling::Modeling &modeling);
        };

    } /* namespace role */
} /* namespace decision_making */


#endif //ITANDROIDS_SOCCER3D_CPP_ATTACKINGROLE_H
