/*
 * AccelerometerPerceptor.h
 *
 *  Created on: Aug 15, 2015
 *      Author: itandroids
 */

#ifndef SOURCE_PERCEPTION_ACCELEROMETERPERCEPTOR_H_
#define SOURCE_PERCEPTION_ACCELEROMETERPERCEPTOR_H_

#include "perception/Perceptor.h"
#include <boost/algorithm/string.hpp>
#include "utils/parser/ParserTreeNode.h"
#include "utils/parser/KeyValuePair.h"
#include <boost/lexical_cast.hpp>
#include "math/Vector3.h"
#include "math/MathUtils.h"

namespace perception {

    using namespace itandroids_lib::math;

    class AccelerometerPerceptor : public Perceptor {
    public:
        /**
         * Constructor for AccelerometerPerceptor.
         *
         * @param node part of the parser tree that represents
         *             an accelerometer perceptor.
         */
        AccelerometerPerceptor(utils::parser::ParserTreeNode &node);

        /**
         * Destructor.
         */
        virtual ~AccelerometerPerceptor();

        /**
         * Gets the perceptor's name.
         *
         * @return the perceptor's name.
         */

        /**
         * Gets the acceleration measured by the accelerometer.
         *
         * @return measured acceleration.
         */
        Vector3<double> getAcceleration();

    protected:

    private:
        Vector3<double> acceleration;

    };

} /* namespace perception */

#endif /* SOURCE_PERCEPTION_ACCELEROMETERPERCEPTOR_H_ */
