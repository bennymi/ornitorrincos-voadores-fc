//
// Created by dicksiano on 8/15/16.
//
#include "decision_making/behavior/GoalieBehavior.h"
#include "base/BaseAgentTest.h"

class BallFollower_AgentTest : public BaseAgentTest {

public:
    int counter;
    int phase;
    double ballMovementRadius;
    double vY;

    virtual void testLoop(int nSteps) { BaseAgentTest::testLoop(nSteps); }

    bool first;
    double t0;
    double deltaT;

    virtual void setup() {
        counter = 0;
        ballMovementRadius = 1.0;
        phase = 1.0;
        first = true;
        t0 = modeling.getAgentModel().getAgentPerception().getGlobalTime();
        wiz.setAgentPositionAndDirection(1, representations::PlaySide::PLAY_SIDE::LEFT,
                                         Vector3<double>(-15.0, 0.0, 0.3), 0.0);
    }

    virtual void runStep() {
        counter++;
        deltaT = modeling.getAgentModel().getAgentPerception().getGlobalTime() - t0;
        //double ballX = -0.5*deltaT;
        //double ballY = ballMovementRadius * sin(phase * deltaT );
        //wiz.setBallPosition(Vector3<double>(ballX, ballY, 0));
        double ballVX = -1.0;
        double ballVY = ballMovementRadius * cos(phase * deltaT);
        wiz.setBallVelocity(Vector3<double>(ballVX, ballVY, 0));

        //std::cout << "Wiz Vx: " << ballVX << std::endl;
        //std::cout << "Wiz Vy: " << ballVY << std::endl;

        //std::cout << "Tracker Vx: " << modeling.getWorldModel().getBall().getVelocity().translation.x << std::endl;
        //std::cout << "Tracker Vy: " << modeling.getWorldModel().getBall().getVelocity().translation.y << std::endl;

        if (counter > 1200) {
            wiz.setBallPosition(Vector3<double>(0.0, 0.0, 0.0));
            counter = 0;
            phase = phase * (-1);
        }

        utils::roboviz::Roboviz roboviz1;
        std::string name1("animated");
        std::string buff1("animated");

        Pose2D ballPosition = modeling.getWorldModel().getBall().getPosition();
        Pose2D selfPosition = modeling.getWorldModel().getSelf().getPosition();
        Pose2D closestOpponentFromBall = modeling.getWorldModel().getOpponentClosestFromBall().getPosition();
        //roboviz1.drawLine(selfPosition.translation.x, selfPosition.translation.y, 0 , selfPosition.translation.x, selfPosition.translation.y , 1, 1, 20, 20, 20,  &name1);
        //roboviz1.drawLine(ballPosition.translation.x, ballPosition.translation.y, 0 , ballPosition.translation.x, ballPosition.translation.y , 1, 1, 100, 150, 150,  &name1);
        //roboviz1.drawLine(closestOpponentFromBall.translation.x, closestOpponentFromBall.translation.y, 0 , closestOpponentFromBall.translation.x, closestOpponentFromBall.translation.y , 1, 1, 255, 255, 255,  &name1);
        //roboviz1.drawLine(ballPosition.translation.x, ballPosition.translation.y, 0 , ballPosition.translation.x-10, ballPosition.translation.y , 1, 1, 50, 50, 50,  &name1);
        roboviz1.drawCircle(modeling.getWorldModel().getOwnGoalPosition().x,
                            modeling.getWorldModel().getOwnGoalPosition().y,
                            modeling::FieldDescription::PENALTY_AREA_LENGTH, 1, 100, 150, 50, &name1);
        roboviz1.drawCircle(modeling.getWorldModel().getOwnGoalPosition().x,
                            modeling.getWorldModel().getOwnGoalPosition().y, 2, 2, 50, 130, 10, &name1);
        roboviz1.swapBuffers(&buff1);

        if (!decisionMaking.decideFall(modeling)) {

            behaviorFactory.getGoalieBehavior().behave(modeling);
            decisionMaking.movementRequest = behaviorFactory.getGoalieBehavior().getMovementRequest();

            behaviorFactory.getFocusBall().behave(modeling);
            decisionMaking.lookRequest = behaviorFactory.getFocusBall().getLookRequest();
        }


    }
};

int main() {
    // Start an external agent

    /*std::vector<std::thread> threads;
    for (int i = 2; i < 3; i++){
        std::stringstream stream;
        stream << "./PassiveAgent_AgentTest " << i << " " <<  "Enemy &";
        system(stream.str().c_str());
        stream.str("");
        stream << "sleep 5";
        system(stream.str().c_str());
    }*/

    /**
     * Start active agent
     */
    BallFollower_AgentTest t;
    t.testLoop(50000);
}

