/*
 * DecisionMakingStub.cpp
 *
 *  Created on: Oct 1, 2015
 *      Author: mmaximo
 */

#include "DecisionMakingStub.h"

namespace decision_making {

    DecisionMakingStub::DecisionMakingStub() :
            sayMessage(nullptr), beamRequest(nullptr), lookRequest(nullptr), movementRequest(nullptr) {
    }

    DecisionMakingStub::~DecisionMakingStub() {
    }

    std::shared_ptr<std::string> DecisionMakingStub::getSayMessage() {
        return sayMessage;
    }

    std::shared_ptr<control::LookRequest> DecisionMakingStub::getLookRequest() {
        return std::move(lookRequest);
    }

    std::shared_ptr<control::MovementRequest> DecisionMakingStub::getMovementRequest() {
        return movementRequest;
    }

    std::shared_ptr<Pose2D> DecisionMakingStub::getBeamRequest() {
        return beamRequest;
    }

    bool DecisionMakingStub::decideFall(modeling::Modeling &modeling) {
        if (modeling.getAgentModel().hasFallenFront()) {
            movementRequest = std::make_shared<control::KeyframeRequest>(
                    control::keyframe::KeyframeType::STAND_UP_FROM_FRONT, false);
            return true;
        } else if (modeling.getAgentModel().hasFallenBack()) {
            movementRequest = std::make_shared<control::KeyframeRequest>
                    (control::keyframe::KeyframeType::STAND_UP_FROM_BACK, false);
            return true;
        } else if (modeling.getAgentModel().hasFallenSide()) {
            movementRequest = std::make_shared<control::KeyframeRequest>
                    (control::keyframe::KeyframeType::MOVE_ARM_TO_FALL_BACK);
        }
        return false;
    }

} /* namespace decision_making */
