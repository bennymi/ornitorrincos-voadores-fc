/*
 * BallModel.cpp
 *
 *  Created on: Aug 29, 2015
 *      Author: mmaximo
 */

#include "BallModel.h"

namespace modeling {

    BallModel::BallModel() {
        lastGlobalTime = 0;
        dt = 0.02;
    }

    BallModel::~BallModel() {
    }

    void BallModel::update(double globalTime,
                           perception::VisibleBall *visibleBall,
                           Pose2D &odometry, Pose2D &agentEstimate) {
        //double dt = globalTime - lastGlobalTime; //Not necessary since dt is constant
        if (visibleBall == NULL) {
            tracker.update(dt, odometry);
        } else {
            tracker.update(dt, odometry, visibleBall, agentEstimate);
            ball.setLastSeenTime(globalTime); // Update the last time that the ball was seen
        }

        ball.setLocalPosition(
                itandroids_lib::math::Pose2D(tracker.getPosition().translation.x, tracker.getPosition().translation.y));
        ball.setPosition(ball.getLocalPosition().relativeToGlobal(agentEstimate)); // Update ball's position

        ball.setLocalVelocity(
                itandroids_lib::math::Pose2D(tracker.getVelocity().translation.x, tracker.getVelocity().translation.y));
        ball.setVelocity(
                itandroids_lib::math::Pose2D(tracker.getVelocity().translation.x * cos(agentEstimate.rotation) -
                                             tracker.getVelocity().translation.y * sin(agentEstimate.rotation),
                                             tracker.getVelocity().translation.x * sin(agentEstimate.rotation) +
                                             tracker.getVelocity().translation.y * cos(agentEstimate.rotation)));
        // Update ball's velocity
        ball.setRelevant(!tracker.hasLostBall());
        lastGlobalTime = globalTime; // Update last time that the ball model was updated
    }

    void BallModel::update(double globalTime,
                           representations::HearData hearData, std::string ourTeamName,
                           Pose2D &odometry, Pose2D &agentEstimate) {
        if (hearData.isFromSelf()) {
            return; //Prevents a update from a message sent by himself
        } else if (hearData.getMessage().size() == 0) {
            return; //Prevents from a empty message.
        } else if (hearData.isFallen())
            return; //Prevents from fallen player message

        ball.setPosition(
                itandroids_lib::math::Pose2D(hearData.getHeardBallXPosition(), hearData.getHeardAgentYPosition()));
        ball.setRelevant(true);
        ball.setLastHeardTime(globalTime);

        lastGlobalTime = globalTime;
    }

    Ball &BallModel::getObject() {
        return ball;
    }

} /* namespace modeling */
