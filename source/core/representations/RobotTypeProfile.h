/*
 * RobotTypeProfile.h
 *
 *  Created on: Oct 10, 2015
 *      Author: mmaximo
 */

#ifndef SOURCE_REPRESENTATIONS_ROBOTTYPEPROFILE_H_
#define SOURCE_REPRESENTATIONS_ROBOTTYPEPROFILE_H_

#include "representations/RobotType.h"

namespace representations {


    class RobotTypeProfile {
    public:
        RobotTypeProfile();

        virtual ~RobotTypeProfile();

        double getHip1TranslationX();

        double getHip1TranslationZ();

        double getThighTranslationZ();

        double getAnkleTranslationZ();

        double getElbowTranslationY();

        double getFootPitchMaxSpeed();

        double getFootRollMaxSpeed();

        bool getUseToe();

        double getToeLength();

//        static RobotTypeProfile getRobotTypeProfile(int robotType);

        static RobotTypeProfile getRobotTypeProfile(RobotType::ROBOT_TYPE robotType);

    private:
        double hip1TranslationX;
        double hip1TranslationZ;
        double thighTranslationZ;
        double ankleTranslationZ;
        double elbowTranslationY;
        double footPitchMaxSpeed;
        double footRollMaxSpeed;
        bool useToe;
        double toeLength;
    };

} /* namespace representations */

#endif /* SOURCE_REPRESENTATIONS_ROBOTTYPEPROFILE_H_ */
