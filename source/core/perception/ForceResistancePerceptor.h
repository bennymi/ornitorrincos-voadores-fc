/*
 * ForceResistancePerceptor.h
 *
 *  Created on: Aug 15, 2015
 *      Author: itandroids
 */

#ifndef SOURCE_PERCEPTION_FORCERESISTANCEPERCEPTOR_H_
#define SOURCE_PERCEPTION_FORCERESISTANCEPERCEPTOR_H_

#include "utils/parser/KeyValuePair.h"
#include "utils/parser/ParserTreeNode.h"
#include <boost/lexical_cast.hpp>
#include <boost/algorithm/string.hpp>
#include "perception/Perceptor.h"
#include "math/Vector3.h"
#include "representations/ForceResistanceData.h"

namespace perception {

    using namespace itandroids_lib::math;

    class ForceResistancePerceptor : public Perceptor {
    public:

        /**
         * Constructor of ForceResistancePerceptor
         * @param node part of the parser tree that represents
         *             an forceResistance perceptor.
         */
        ForceResistancePerceptor(utils::parser::ParserTreeNode &node);

        /**
         * Destructor
         */
        virtual ~ForceResistancePerceptor();

        /**
         * Gets the measured origin coordinates of the force
         *
         * @return measured origin coordinates
         */
        Vector3<double> getOriginCoordinates();

        /**
         * Get the measured force
         * @return measured force in Cartesian coordinates
         */
        Vector3<double> getForce();

        /**
         * Get the force Data
         * @return force data
         */
        representations::ForceResistanceData getForceResistanceData();

    protected:

    private:
        Vector3<double> originCoordinates;
        Vector3<double> force;
        representations::ForceResistanceData forceData;
    };

} /* namespace perception */

#endif /* SOURCE_PERCEPTION_FORCERESISTANCEPERCEPTOR_H_ */
