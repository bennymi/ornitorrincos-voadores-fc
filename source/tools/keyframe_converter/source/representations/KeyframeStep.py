# -*- coding: utf-8 -*-


class KeyframeStep(object):

    def __init__(self):
        self.jointRepresentations = [
            "neckYaw",
            "neckPitch",
            "leftShoulderPitch",
            "leftShoulderYaw",
            "leftArmRoll",
            "leftArmYaw",
            "leftHipYawpitch",
            "leftHipRoll",
            "leftHipPitch",
            "leftKneePitch",
            "leftFootPitch",
            "leftFootRoll",
            "rightShoulderPitch",
            "rightShoulderYaw",
            "rightArmRoll",
            "rightArmYaw",
            "rightHipYawpitch",
            "rightHipRoll",
            "rightHipPitch",
            "rightKneePitch",
            "rightFootPitch",
            "rightFootRoll"
            ]
        self.jointsPositions = [0] * len(self.jointRepresentations)
        self.time = 0

    def getAsList(self):
        return self.jointsPositions

    def setJointPositionsAsList(self, jointsPositions):
        self.jointsPositions = jointsPositions




