//
// Created by dicksiano on 16/02/16.
//

#ifndef ITANDROIDS_SOCCER3D_CPP_NAVIGATETOSTATICPOSITION_H
#define ITANDROIDS_SOCCER3D_CPP_NAVIGATETOSTATICPOSITION_H

#include <core/decision_making/positioning/Positioning.h>
#include <core/decision_making/positioning/DelaunayPositioning.h>
#include "Behavior.h"
#include "modeling/Modeling.h"
#include "modeling/WorldModel.h"

namespace decision_making {
    namespace behavior {
        class BehaviorFactory;

        class NavigateToStaticPosition : public Behavior {
        public:
            /**
             * Default constructor.
             */
            NavigateToStaticPosition();

            /**
             * Assignment constructor.
             *
             * @param behaviorFactory pointer to BehaviorFactory object.
             */
            NavigateToStaticPosition(BehaviorFactory *behaviorFactory);

            /**
             * Destructor.
             */
            virtual ~NavigateToStaticPosition();

            /**
             * Moves to pre-defined positions.
             *
             * @param modeling.
             */
            void behave(modeling::Modeling &modeling) override;

            itandroids_lib::math::Pose2D getStrategicPosition(modeling::Modeling &modeling);

        private:
            decision_making::positioning::DelaunayPositioning delaunayPositioning;
        };

    } /* namespace behavior */
} /* namespace decision_making */


#endif //ITANDROIDS_SOCCER3D_CPP_NAVIGATETOSTATICPOSITION_H
