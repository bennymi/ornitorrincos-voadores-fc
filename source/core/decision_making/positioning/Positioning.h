/*
 * Positioning.h
 *
 *  Created on: Oct 28, 2015
 *      Author: itandroids
 */

#ifndef SOURCE_DECISION_MAKING_POSITIONING_POSITIONING_H_
#define SOURCE_DECISION_MAKING_POSITIONING_POSITIONING_H_

#include "decision_making/positioning/delaunay_triangulation/DelaunayInterpolator.h"
#include "decision_making/positioning/delaunay_triangulation/DelaunayFormationFactory.h"
#include "math/Pose2D.h"


namespace modeling {
    class Modeling;
}

namespace decision_making {
    namespace positioning {

/**
 * Abstract class that represents the behavior thats controls the agent positioning along the field.
 */
        class Positioning {
        public:
            /**
             * Default constructor for Positioning
             */
            Positioning();

            /**
             * Destructor
             */
            virtual ~Positioning();

            /**
             * Calculates for which position the agent must go.
             *
             * @param agent number
             * @param modeling
             */
            virtual itandroids_lib::math::Pose2D getAgentPosition(modeling::Modeling &modeling);

        private:
        };

    } /* namespace positioning */
} /* namespace decision_making */

#endif /* SOURCE_DECISION_MAKING_POSITIONING_POSITIONING_H_ */
