/*
 * NavigationParams.h
 *
 *  Created on: Oct 31, 2015
 *      Author: itandroids
 */

#ifndef SOURCE_REPRESENTATIONS_NAVIGATIONPARAMS_H_
#define SOURCE_REPRESENTATIONS_NAVIGATIONPARAMS_H_

#include <boost/property_tree/ptree.hpp>
#include "control/walking/OmnidirectionalWalkZMP.h"
#include "utils/patterns/Configurable.h"

using control::walking::OmnidirectionalWalkZMParamsType;
using itandroids_lib::utils::patterns::Configurable;
namespace pt = boost::property_tree;

namespace representations {

    class NavigationParams : public Configurable<NavigationParams> {
    public:
        NavigationParams();

        virtual ~NavigationParams();

        static NavigationParams &
        getNavigationParams(OmnidirectionalWalkZMParamsType type = OmnidirectionalWalkZMParamsType::NORMAL);

        template<class Content, class Key>
        void setParameter(Parameter<Content, Key> &parameter){
            for (auto &child : parameter.getChildren()) {
                if (child.first == "normalParams") {
                    setNavigationParameter(*child.second, normalParams);
                } else if (child.first == "precise") {
                    setNavigationParameter(*child.second, preciseParams);
                }
            }
        };

        double max_v_forward;
        double max_v_backward;
        double max_vy;
        double max_vpsi;
        double close_distance_threshold;
        double low_vpsi_factor;
        double low_v_forward_factor;
        double low_v_backward_factor;
        double low_vy_factor;
        double velocity_gain;
        double agent_radius;
    private:

        static std::map<OmnidirectionalWalkZMParamsType, NavigationParams> params;
        static NavigationParams normalParams;
        static NavigationParams preciseParams;

        template<class Content, class Key>
        static void setNavigationParameter(Parameter<Content, Key> &parameter, NavigationParams &navigationParameter) {

        }


    };


    template<>
    void NavigationParams::setNavigationParameter<pt::ptree, std::string>(Parameter<pt::ptree, std::string> &parameter,
                                                                          NavigationParams &navigationParameter);
} /* namespace representations */

#endif /* SOURCE_REPRESENTATIONS_NAVIGATIONPARAMS_H_ */
