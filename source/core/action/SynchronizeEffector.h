/*
 * SynchronizeEffector.h
 *
 *  Created on: Sep 30, 2015
 *      Author: mmaximo
 */

#ifndef SOURCE_ACTION_SYNCHRONIZEEFFECTOR_H_
#define SOURCE_ACTION_SYNCHRONIZEEFFECTOR_H_

#include "action/Effector.h"

namespace action {

/**
 * Class that represents a synchronize effector.
 * Allows agent's operation in synch mode.
 */
    class SynchronizeEffector : public Effector {
    public:
        /**
         * Creates a SynchronizeEffector.
         */
        SynchronizeEffector();

        /**
         * Default constructor of SynchronizeEffector.
         */

        virtual ~SynchronizeEffector();

        /**
         *
         * Gets the synchronize effector as a server message.
         *
         * @return the server message.
         *
         */

        std::string toString();
    };

} /* namespace action */

#endif /* SOURCE_ACTION_SYNCHRONIZEEFFECTOR_H_ */
