/*
 * Parser.cpp
 *
 *  Created on: Aug 8, 2015
 *      Author: mmaximo
 */

#include "Parser.h"
#include <iostream>

namespace utils {
    namespace parser {

        Parser::Parser() {
            count = 0;
        }

        Parser::~Parser() {
        }

        ParserTreeNode *Parser::parseMessage(const std::string &message) {
            int index = 0;
            count = 0;
            ParserTreeNode *root = new ParserTreeNode();

            while (index < message.length()) {
                ParserTreeNode *child = new ParserTreeNode();

                root->children.push_back(child);
                count++;
                index = parseMessage(child, message, index);
            }

            return root;
        }

        int Parser::parseMessage(ParserTreeNode *node, const std::string &message,
                                 int startIndex) {
            int index = startIndex;


            while ((message[index] != '('))
                ++index;
            // skip '('
            ++index;
            while (index < message.length() && message[index] != ')') {
                if (message[index] == '(') {
                    count++;
                    if (node->content.length() != 0) {
                        if (node->content[node->content.length() - 1] == ' ') {
                            node->content.erase(node->content.length() - 1);
                        }
                    }
                    ParserTreeNode *child = new ParserTreeNode();
                    child->count = count;
                    node->children.push_back(child);

                    index = parseMessage(child, message, index);
                } else {
                    node->content += message[index];
                    ++index;
                }
            }

            // skip ')'
            count--;
            return (index + 1);
        }

    } /* namespace parser */
} /* namespace utils */
