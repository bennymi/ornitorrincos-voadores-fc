//
// Created by mmaximo on 20/06/16.
//

#ifndef ITANDROIDS_SOCCER3D_CPP_NAVIGATETOBALL_H
#define ITANDROIDS_SOCCER3D_CPP_NAVIGATETOBALL_H

#include "decision_making/behavior/PotentialFieldNavigation.h"

namespace decision_making {
    namespace behavior {

        class NavigateToBall : public Behavior {
        public:
            NavigateToBall(BehaviorFactory *factory);

            NavigateToBall(BehaviorFactory *factory, PotentialFieldParameters parameters);

            void behave(modeling::Modeling &modeling) override;

        };

    } /* namespace behavior */
} /* namespace decision_making */

#endif //ITANDROIDS_SOCCER3D_CPP_NAVIGATETOBALL_H
