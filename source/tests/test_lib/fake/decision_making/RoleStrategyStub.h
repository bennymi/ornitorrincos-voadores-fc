//
// Created by muzio on 30/05/16.
//

#ifndef ITANDROIDS_SOCCER3D_CPP_ROLESTRATEGYSTUB_H
#define ITANDROIDS_SOCCER3D_CPP_ROLESTRATEGYSTUB_H

#include "decision_making/RoleStrategy.h"
#include "RoleStub.h"

namespace decision_making {

    class RoleStrategyStub : public RoleStrategy {
    public:
        RoleStrategyStub();

        virtual ~RoleStrategyStub();

        role::Role *getRole(modeling::Modeling &modeling);

    private:
        role::RoleStub role;
    };

} /* namespace decision_making */

#endif //ITANDROIDS_SOCCER3D_CPP_ROLESTRATEGYSTUB_H
