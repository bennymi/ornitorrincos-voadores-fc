# -*- coding: utf-8 -*-

from reader import FCPortugalFileReader
from reader import PageFileReader
from writer import PageFileWriter
from writer import JsonFileWriter
import os


class InputLoader(object):

    def __init__(self):
        self.nothing = "nothing"

    def loadInput(self, args):
        #This function load the input command line arguments
        # and selects which reader and which writer will be used
        self.inputFile = args.input
        self.fileReader = args.file_reader
        self.fileWriter = args.file_writer
        outputFileExt = ""
        #Selecting the reader
        if args.file_reader == "FCPortugal":
            self.reader = FCPortugalFileReader.FCPortugalFileReader(
            self.inputFile)
        elif args.file_reader == "page":
            self.reader = PageFileReader.PageFileReader(self.inputFile)

        #Selecting the Writer
        if args.file_writer == "page":
            outputFileExt = ".page"
            self.writer = PageFileWriter.PageFileWriter()
        elif args.file_writer == "json":
            outputFileExt = ".json"
            self.writer = JsonFileWriter.JsonFileWriter()
        if args.output is None:
            self.outputFile = os.path.splitext(self.inputFile)[0] + \
            outputFileExt
        else:
            self.outputFile = args.output

    def getReader(self):
        return self.reader

    def getWriter(self):
        return self.writer

    def getOutputFile(self):
        return self.outputFile
