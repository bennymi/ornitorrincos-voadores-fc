/*
 * PlayerModel.h
 *
 *  Created on: Aug 29, 2015
 *      Author: mmaximo
 */

#ifndef SOURCE_MODELING_OTHERPLAYERMODEL_H_
#define SOURCE_MODELING_OTHERPLAYERMODEL_H_

#include "math/Vector3.h"
#include "math/Pose2D.h"
#include "perception/VisiblePlayer.h"
#include "representations/OtherPlayer.h"
#include "modeling/state_estimation/PlayerTracker.h"

namespace modeling {

    using namespace itandroids_lib::math;
    using namespace modeling::state_estimation_player;
    using namespace representations;

/**
 * Class that represents the other players model.
 */
    class OtherPlayerModel {
    public:
        /**
         * Default constructor of OtherPlayerModel.
         */
        OtherPlayerModel();

        OtherPlayerModel(int uniformNumber);

        /**
         * Default destructor of OtherPlayerModel.
         */

        virtual ~OtherPlayerModel();

        /**
         * Updates the other player model.
         */

        void update(const double globalTime,
                    perception::VisiblePlayer *visiblePlayer,
                    Pose2D &odometry, Pose2D &agentEstimate);

        /**
         * Uodates the other player model with heard data
         */
        void update(representations::HearData hearData);

        /**
         * Updates according to a heard teammate.
         * You should'n use operator "=" (e.g, teammateModel[i] = heardTeammateModel[i]), because of the tracker
         */
        void update(OtherPlayer heardTeammate);

        /**
         * Gets the other player object.
         *
         * @return the other player object.
         */

        OtherPlayer &getObject();

        void updateFallen(perception::VisiblePlayer *visiblePlayer);

    private:
        PlayerTracker tracker;
        double lastGlobalTime;
        double dt;
        OtherPlayer otherPlayer;
    };

} /* namespace modeling */

#endif /* SOURCE_MODELING_OTHERPLAYERMODEL_H_ */
