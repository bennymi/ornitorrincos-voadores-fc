//
// Created by mmaximo on 6/29/16.
//

#ifndef ITANDROIDS_SOCCER3D_CPP_BEAMBEHAVIOR_H
#define ITANDROIDS_SOCCER3D_CPP_BEAMBEHAVIOR_H

#include "decision_making/behavior/Behavior.h"

namespace decision_making {
    namespace behavior {

        class BeamBehavior : public Behavior {
        public:
            BeamBehavior(BehaviorFactory *factory);

            void behave(modeling::Modeling &modeling) override;

            std::shared_ptr<itandroids_lib::math::Pose2D> getBeamRequest();

        private:
            std::shared_ptr<itandroids_lib::math::Pose2D> beamRequest;

        };

    } /* namespace behavior */
} /* namespace decision_making */


#endif //ITANDROIDS_SOCCER3D_CPP_BEAMBEHAVIOR_H
