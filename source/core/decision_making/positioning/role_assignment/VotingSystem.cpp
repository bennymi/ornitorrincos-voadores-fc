//
// Created by luckeciano on 9/11/16.
//

#include "VotingSystem.h"


namespace decision_making {
    namespace positioning {
        namespace role_assignment {

            void VotingSystem::receiveRoleAssignVectors(modeling::PositioningModel &positioningModel) {

                std::vector<std::vector<int> > teammatesRAVectors =
                        positioningModel.getTeammatesRoleAssignmentVector();

                for (int i = 0; i < modeling::WorldModel::NUM_PLAYERS_PER_TEAM; i++) {
                    roleAssignmentVectors[i] = teammatesRAVectors[i];
                }
            }

            /*
            std::vector<int> VotingSystem::generateSyncRoleVector(modeling::PositioningModel &positioningModel) {
                receiveRoleAssignVectors(positioningModel);
                std::vector<int> finalRoleAssingment;
                for (int player = 0; player < modeling::WorldModel::NUM_PLAYERS_PER_TEAM; player++) {
                    //vector that contains the possibilities of assignment to a specific player
                    //and the votes to each possibility
                    //example: playerAssignment = [(0,10) (3,1)] says that to this specific player (the goalie, for example)
                    //the position 0 received 10 votes and the position 3 received 1 vote.
                    std::vector<std::pair<int, int> > playerAssigns;
                    for (int roleAssignVec = 0; roleAssignVec < modeling::WorldModel::NUM_PLAYERS_PER_TEAM; roleAssignVec++) {
                        bool isNewAssignment = true;
                        for (int i = 0; i < playerAssigns.size(); i++) {
                            if (roleAssignmentVectors[roleAssignVec][player] == playerAssigns[i].first) {
                                playerAssigns[i].second++;
                                isNewAssignment = false;
                            }
                        }
                        if (isNewAssignment) {

                            int once = 1;
                            playerAssigns.push_back(std::make_pair(roleAssignmentVectors[roleAssignVec][player], once));
                        }
                    }
                    bool hasAssignment = false;
                    int maxVotes = 0, assignmentWithMaxVotes = 0;
                    for (int voteCase = 0; voteCase < playerAssigns.size(); voteCase++) {
                        if (playerAssigns[voteCase].second > maxVotes) {
                            //check if already exists this assignment in final role assignment
                            bool roleExists = false;
                            for (int role = 0; role < finalRoleAssingment.size(); role++) {
                                if (finalRoleAssingment[role] == playerAssigns[voteCase].first)
                                    roleExists = true;
                            }
                            if (!roleExists) {
                                maxVotes = playerAssigns[voteCase].second;
                                assignmentWithMaxVotes = playerAssigns[voteCase].first;
                                hasAssignment = true;
                            }
                        }
                    }

                    //player without assignment: goes to assignment with max votes (duplicated role!)
                    if (!hasAssignment) {
                        maxVotes = 0; assignmentWithMaxVotes = 0;
                        for (int voteCase = 0; voteCase < playerAssigns.size(); voteCase++) {
                            if (playerAssigns[voteCase].second > maxVotes) {
                                    maxVotes = playerAssigns[voteCase].second;
                                    assignmentWithMaxVotes = playerAssigns[voteCase].first;

                            }
                        }
                    }
                    finalRoleAssingment.push_back(assignmentWithMaxVotes);
                }

                return finalRoleAssingment;

            }*/


            std::vector<int> VotingSystem::generateSyncRoleVector(modeling::PositioningModel &positioningModel) {
                receiveRoleAssignVectors(positioningModel);
                std::vector<int> finalRoleAssingment;
                for (int player = 0; player < modeling::WorldModel::NUM_PLAYERS_PER_TEAM; player++) {
                    //vector that contains the possibilities of assignment to a specific player
                    //and the votes to each possibility
                    //example: playerAssignment = [(0,10) (3,1)] says that to this specific player (the goalie, for example)
                    //the position 0 received 10 votes and the position 3 received 1 vote.
                    std::vector<std::pair<int, int> > playerAssigns;
                    for (int roleAssignVec = 0;
                         roleAssignVec < modeling::WorldModel::NUM_PLAYERS_PER_TEAM; roleAssignVec++) {
                        bool isNewAssignment = true;
                        for (int i = 0; i < playerAssigns.size(); i++) {
                            if (roleAssignmentVectors[roleAssignVec][player] == playerAssigns[i].first) {
                                playerAssigns[i].second++;
                                isNewAssignment = false;
                            }
                        }
                        if (isNewAssignment) {

                            int once = 1;
                            playerAssigns.push_back(std::make_pair(roleAssignmentVectors[roleAssignVec][player], once));
                        }
                    }
                    int maxVotes = 0, assignmentWithMaxVotes = 0;
                    for (int voteCase = 0; voteCase < playerAssigns.size(); voteCase++) {
                        if (playerAssigns[voteCase].second > maxVotes) {
                            maxVotes = playerAssigns[voteCase].second;
                            assignmentWithMaxVotes = playerAssigns[voteCase].first;
                        }
                    }
                    finalRoleAssingment.push_back(assignmentWithMaxVotes);
                }

                return finalRoleAssingment;

            }

        }
    }
}