/*
 * InitEffector.h
 *
 *  Created on: Aug 8, 2015
 *      Author: mmaximo
 */

#ifndef SOURCE_ACTION_INITEFFECTOR_H_
#define SOURCE_ACTION_INITEFFECTOR_H_

#include "action/Effector.h"

namespace action {

/**
 * Class that represents an init effector.
 */
    class InitEffector : public Effector {
    public:
        /**
         * Creates an InitEffector.
         *
         * @param playerNumber the player's uniform number.
         * @param teamName the player's team name.
         */
        InitEffector(int playerNumber, const std::string &teamName);

        /**
         * Default destructor of InitEffector.
         */

        virtual ~InitEffector();

        /**
         * Gets the init effector as a server message.
         *
         * @return the server message.
         */

        std::string toString();

    private:
        int playerNumber;
        std::string teamName;
    };

} /* namespace action */

#endif /* SOURCE_ACTION_INITEFFECTOR_H_ */
