/*
 * EulerAngles.cpp
 *
 *  Created on: Aug 29, 2015
 *      Author: mmaximo
 */

#include "EulerAngles.h"

namespace representations {

    EulerAngles::EulerAngles() :
            roll(0.0), pitch(0.0), yaw(0.0) {
    }

} /* namespace representations */
