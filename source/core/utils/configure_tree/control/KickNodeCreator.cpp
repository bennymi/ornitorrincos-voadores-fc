//
// Created by francisco on 19/12/16.
//

#include "KickNodeCreator.h"
#include <boost/property_tree/json_parser.hpp>

namespace utils {
namespace configure_tree {
namespace control {
    template<>
    std::shared_ptr<Parameter<pt::ptree,std::string>> KickNodeCreator::generateNode(){
        auto kickNode = std::make_shared<LeafParameter<>>();
        boost::property_tree::read_json("../configuration/core/control/kick/normal-parameters.json",kickNode->getContent());
        return kickNode;
    }

}
}
}

