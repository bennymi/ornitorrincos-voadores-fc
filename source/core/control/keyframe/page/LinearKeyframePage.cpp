//
// Created by francisco on 5/22/16.
//

#include "LinearKeyframePage.h"


namespace control {
    namespace keyframe {

        std::vector<double> LinearKeyframePage::interpolate() {
            std::vector<double> v(representations::NaoJoints::NUM_JOINTS);

            auto &initialJoints = steps[currentStep]->getJoints();
            if (currentStep < steps.size()) {
                auto &afterJoints = steps[currentStep + 1]->getJoints();

                LinearInterpolator<representations::NaoJoints> interp(initialJoints, afterJoints);
                auto interpolatedJoints = interp.interpolate(
                        (currentTime * speedRate - stepTimes[currentStep]) / steps[currentStep + 1]->getDuration());

                for (int i = 0; i < representations::NaoJoints::NUM_JOINTS; i++) {
                    v[i] = interpolatedJoints.getValue(static_cast<representations::NaoJoints::HINGE_JOINTS >(i));
                }
                return v;
            } else {
                for (int i = 0; i < representations::NaoJoints::NUM_JOINTS; i++) {
                    v[i] = initialJoints.getValue(static_cast<representations::NaoJoints::HINGE_JOINTS >(i));
                }
            }

        }

        void LinearKeyframePage::createInterpolators() {

        }


    }
}




