#include <iostream>
#include <thread>
#include "communication/ServerSocket.h"
#include "SoccerPlayer.h"
#include "representations/RepresentationsLoader.h"

using namespace std;

namespace agent {
    using utils::configure_tree::ParameterTreeCreator;

    SoccerPlayer::SoccerPlayer(int argc, char *argv[]) {

        std::string serverIP = representations::ITAndroidsConstants::DEFAULT_IP; // Default server IP
        int serverPort = representations::ITAndroidsConstants::DEFAULT_PORT; // Default server Port
        //int controlID = 0; // Default control ID
        int agentNumber = 1; // Default agent number. Notice that agent number 1 means that the agent is a goalie.
        std::string teamName = representations::ITAndroidsConstants::TEAM_DEFAULT_NAME; // Default team name
        std::cout << argc << std::endl;
        if (argc > 1) {
            serverIP = argv[1];
            //hack
            if (serverIP == "localhost")
                serverIP = "127.0.0.1";
        }
        if (argc > 2) {
            serverPort = std::stoi(argv[2]);
        }
        if (argc > 3) {
            teamName = std::string(argv[3]);
        }
        if (argc > 4) {
            agentNumber = std::stoi(argv[4]);
        }
//        if (argc > 5){
//            controlID = std::stoi(argv[5]);
//        }

//        if (id >= 1 && id <= 7)
//            type = 0;
//        else if (id <= 9)
//            type = 2;
//        else
//            type = 4;
        representations::RobotType::ROBOT_TYPE robotType;
        if (agentNumber >= 1 && agentNumber <= 3)
            robotType = representations::RobotType::STANDART_NAO;
        else if (agentNumber == 4 || agentNumber == 5)
            robotType = representations::RobotType::STANDART_NAO_WITH_TOE;
        else if (agentNumber == 6)
            robotType = representations::RobotType::STANDART_NAO;
        else if (agentNumber == 7 || agentNumber == 8)
            robotType = representations::RobotType::SECONDARY_NAO;
        else
            robotType = representations::RobotType::STANDART_NAO;


        communication::ServerSocketImpl *socket = new communication::ServerSocketImpl(
                serverIP,
                serverPort);
        auto root = ParameterTreeCreator::getRoot<>();
        representations::RepresentationsLoader loader;
        loader.setParameter(root->getChild("representations"));
        communication = new communication::Communication(socket);
        control = new control::ControlImpl(robotType, root->getChild("control"));
        modeling = new modeling::Modeling(agentNumber, teamName);
        communication->establishConnection();
        action.create(robotType);
        communication->sendMessage(action.getServerMessage());
        communication->receiveMessage();
        perception.perceive(*communication);

        decisionMaking = decision_making::DecisionMakingImpl();

        action.init(agentNumber, teamName);
        communication->sendMessage(action.getServerMessage());
    }

    SoccerPlayer::SoccerPlayer(std::string serverIP ,
                               int serverPort ,
                               std::string teamName ,
                               int agentNumber
                               ) {
        representations::RobotType::ROBOT_TYPE robotType;
        if (agentNumber >= 1 && agentNumber <= 3)
            robotType = representations::RobotType::STANDART_NAO;
        else if (agentNumber == 4 || agentNumber == 5)
            robotType = representations::RobotType::STANDART_NAO_WITH_TOE;
        else if (agentNumber == 6)
            robotType = representations::RobotType::STANDART_NAO;
        else if (agentNumber == 7 || agentNumber == 8)
            robotType = representations::RobotType::SECONDARY_NAO;
        else
            robotType = representations::RobotType::STANDART_NAO;


        communication::ServerSocketImpl *socket = new communication::ServerSocketImpl(
                serverIP,
                serverPort);
        auto root = ParameterTreeCreator::getRoot<>();
        representations::RepresentationsLoader loader;
        loader.setParameter(root->getChild("representations"));
        communication = new communication::Communication(socket);
        control = new control::ControlImpl(robotType, root->getChild("control"));
        modeling = new modeling::Modeling(agentNumber, teamName);
        communication->establishConnection();
        action.create(robotType);
        communication->sendMessage(action.getServerMessage());
        communication->receiveMessage();
        perception.perceive(*communication);

        decisionMaking = decision_making::DecisionMakingImpl();

        action.init(agentNumber, teamName);
        communication->sendMessage(action.getServerMessage());
    }

    SoccerPlayer::~SoccerPlayer() {
    }

    void SoccerPlayer::play() {
        //communication loop
        while (!communication->connectionClosed()) {
            communication->receiveMessage(); // Receive a message from the server
            perception.perceive(*communication); // Transform the server message into perceptors
            modeling->model(perception, dynamic_cast<control::Control &>(*control)); // Model the world
            decisionMaking.decide(*modeling); // Decide which action should be taken
            control->control(perception, *modeling,
                             dynamic_cast<decision_making::DecisionMaking &>(decisionMaking)); // Control the agent's joints
            action.act(decisionMaking, *control); // Create a message

            communication->sendMessage(action.getServerMessage()); // Receive a message to the server
        }
    }
}

/* namespace agent */



