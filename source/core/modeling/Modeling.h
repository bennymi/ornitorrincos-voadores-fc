/*
 * Modeling.h
 *
 *  Created on: Aug 7, 2015
 *      Author: mmaximo
 */

#ifndef SOURCE_MODELING_MODELING_H_
#define SOURCE_MODELING_MODELING_H_

#include "perception/Perception.h"
#include "AgentModel.h"
#include "WorldModel.h"


namespace control {
    class Control;

    class ControlImpl;
}

namespace decision_making {
    class DecisionMaking;

    class DecisionMakingImpl;
}

namespace modeling {

/**
 * Class that updates world`s data and agent`s data.
 */
    class Modeling {
    public:


        /**
         * Modeling constructor that receives the agent number and a team name
         */

        Modeling(int agentNumber = 1, std::string teamName = representations::ITAndroidsConstants::TEAM_DEFAULT_NAME);

        /**
         * Default modeling Destructor
         */
        virtual ~Modeling();

        /**
         *
         * @return the agent model
         */
        AgentModel &getAgentModel();

        /**
         *
         * @return the world model
         */
        WorldModel &getWorldModel();

        /**
         * Updates the agentModel and the worldModel
         * @param perception
         * @param control
         */
        void model(perception::Perception &perception, control::Control &control);

    private:
        AgentModel agentModel;
        WorldModel worldModel;
        itandroids_lib::math::Pose2D odometry;
    };

} /* namespace modeling */

#endif /* SOURCE_MODELING_MODELING_H_ */
