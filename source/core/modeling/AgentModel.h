/*
 * AgentModel.h
 *
 *  Created on: Aug 8, 2015
 *      Author: mmaximo
 */

#ifndef SOURCE_MODELING_AGENTMODEL_H_
#define SOURCE_MODELING_AGENTMODEL_H_

#include "math/Vector3.h"
#include <cmath>
#include "perception/AgentPerception.h"
#include "modeling/BodyModel.h"

namespace modeling {

    using namespace itandroids_lib::math;
    using namespace representations;

/**
* Class that represents an agent model.
*/

    class AgentModel {
    public:
        /**
         * Default constructor for AgentModel.
         */
        AgentModel();

        /**
         * Constructor that receives the agent Number
         *
         * @param agent number
         */
        AgentModel(int agentNumber);

        /**
         * Default destructor for AgentModel
         */
        virtual ~AgentModel();

        /**
         * Updates the agent model.
         *
         * @param agentPerception agent perception of the current cycle.
         */
        void update(perception::AgentPerception &agentPerception);

        /**
         * Gets the torso orientation.
         *
         * @return the torso orientation.
         */
        const RotationMatrix &getTorsoRotation();

        /**
         * Gets the torso angular velocity.
         *
         * @return the torso angular velocity.
         */
        const Vector3<double> &getTorsoAngularVelocity();

        /**
         * Gets the torso acceleration.
         *
         * @return the torso acceleration.
         */
        const Vector3<double> &getTorsoAcceleration();

        const bool hasFallen();

        const bool hasFallenBack();

        const bool hasFallenFront();

        const bool hasFallenSide();

        /**
         * Gets the robot's center of mass.
         *
         * @return the center of mass.
         */
        const Vector3<double> &getCenterOfMass();

        /**
         * Gets the Zero Moment Point (ZMP).
         *
         * @return the Zero Moment Point (ZMP).
         */
        const Vector3<double> &getZeroMomentPoint();

        /**
         * Checks if the left foot is touching the ground.
         *
         * @return if the left foot is touching the ground.
         */
        bool leftFootTouchsGround();

        /**
         * Checks if the right foot is touching the ground.
         *
         * @return if the right foot is touching the ground.
         */
        bool rightFootTouchsGround();

        const int &getAgentNumber();

        Pose3D &getFromCameraToRootTransform();

        /**
         * Gets the agent perception.
         *
         * @return the agent perception.
         */
        perception::AgentPerception &getAgentPerception();

    private:
        Vector3<double> filteredAcceleration;
        double FORCE_THRESHOLD;
        double ACCELERATION_FILTER_INNOVATION;
        perception::AgentPerception *agentPerception;
        BodyModel bodyModel;
        Vector3<double> centerOfMass;
        Vector3<double> zeroMomentPoint;
        RotationMatrix torsoRotation;
        Pose3D fromCameraToRoot;
        double rollAngle;
        double pitchAngle;
        int agentNumber;
    };

} /* namespace modeling */

#endif /* SOURCE_MODELING_AGENTMODEL_H_ */
