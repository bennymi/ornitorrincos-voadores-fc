/*
 * ServerSocket_Stub.h
 *
 *  Created on: Oct 1, 2015
 *      Author: mmaximo
 */

#ifndef TESTS_SOURCE_COMMUNICATION_SERVERSOCKETSTUB_H_
#define TESTS_SOURCE_COMMUNICATION_SERVERSOCKETSTUB_H_

#include <queue>

#include "communication/ServerSocket.h"

namespace communication {

    class ServerSocketStub : public ServerSocket {
    public:
        ServerSocketStub();

        virtual ~ServerSocketStub();

        bool establishConnection();

        void closeConnection();

        std::string receiveMessage();

        void sendMessage(const std::string &message);

        void addMessageToReceive(const std::string &message);

        std::queue<std::string> getReceiveQueue();

        std::queue<std::string> getSendQueue();

        bool isConnectionEstablished();

        virtual bool connectionClosed();

    private:
        std::queue<std::string> receiveQueue;
        std::queue<std::string> sendQueue;
        bool connectionEstablished;
    };

} /* namespace communication */

#endif /* TESTS_SOURCE_COMMUNICATION_SERVERSOCKETSTUB_H_ */
