//
// Created by jose on 11/09/16.
//

#include "Base64CompressionData.h"

namespace representations {

    const std::string Alphabet64 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

    /**
     * Check if c is a base64 character
     */
    bool Base64CompressionData::isBase64(unsigned char c) {
        return (std::isalnum(c) || c == '+' || c == '/');
    }

    std::string Base64CompressionData::base64Encode(unsigned char const *bytesToEncode, unsigned int sizeString) {
        std::string message;
        int i = 0;
        int j = 0;
        unsigned char auxiliarArray3[3];
        unsigned char auxiliarArray4[4];

        while (sizeString--) {
            auxiliarArray3[i++] = *(bytesToEncode);
            if (i == 3) {
                auxiliarArray4[0] = (auxiliarArray3[0] & 0xfc) >> 2;
                auxiliarArray4[1] = ((auxiliarArray3[0] & 0x03) << 4) + ((auxiliarArray3[1] & 0x0f) >> 4);
                auxiliarArray4[2] = ((auxiliarArray3[1] & 0x0f) << 2) + ((auxiliarArray3[2] & 0xc0) >> 6);
                auxiliarArray4[3] = auxiliarArray3[2] & 0x3f;

                for (i = 0; i < 4; i++)
                    message += Alphabet64[auxiliarArray4[i]];

                i = 0;
            }
        }

        if (i) {
            for (j = i; j < 3; j++)
                auxiliarArray3[j] = '\0';

            auxiliarArray4[0] = (auxiliarArray3[0] & 0xfc) >> 2;
            auxiliarArray4[1] = ((auxiliarArray3[0] & 0x03) << 4) + ((auxiliarArray3[1] & 0x0f) >> 4);
            auxiliarArray4[2] = ((auxiliarArray3[1] & 0x0f) << 2) + ((auxiliarArray3[2] & 0xc0) >> 6);
            auxiliarArray4[3] = auxiliarArray3[2] & 0x3f;

            for (j = 0; j < i + 1; j++)
                message += Alphabet64[auxiliarArray4[j]];

            while (i++ < 3)
                message += '=';
        }

        return message;

    }

    std::string Base64CompressionData::base64Decode(std::string const &encodedString) {
        size_t stringSize = encodedString.size();
        size_t i = 0;
        size_t j = 0;
        int in = 0;
        unsigned char auxiliarArray4[4], auxiliarArray3[3];
        std::string message;

        while (stringSize-- && (encodedString[in] != '=' && isBase64(encodedString[in]))) {
            auxiliarArray4[i++] = encodedString[in];
            in++;
            if (i == 4) {
                for (i = 0; i < 4; i++) {
                    auxiliarArray4[i] = static_cast <unsigned char> (Alphabet64.find(auxiliarArray4[i]));
                }

                auxiliarArray3[0] = (auxiliarArray4[0] << 2) + ((auxiliarArray4[1] & 0x30) >> 4);
                auxiliarArray3[1] = ((auxiliarArray4[1] & 0xf) << 4) + ((auxiliarArray4[2] & 0x3c) >> 2);
                auxiliarArray3[2] = ((auxiliarArray4[2] & 0x3) << 6) + auxiliarArray4[3];

                for (i = 0; (i < 3); i++)
                    message += auxiliarArray3[i];
                i = 0;
            }
        }

        if (i) {
            for (j = i; j < 4; j++)
                auxiliarArray4[j] = 0;

            for (j = 0; j < 4; j++)
                auxiliarArray4[j] = static_cast<unsigned char> (Alphabet64.find(auxiliarArray4[j]));

            auxiliarArray3[0] = (auxiliarArray4[0] << 2) + ((auxiliarArray4[1] & 0x30) >> 4);
            auxiliarArray3[1] = ((auxiliarArray4[1] & 0xf) << 4) + ((auxiliarArray4[2] & 0x3c) >> 2);
            auxiliarArray3[2] = ((auxiliarArray4[2] & 0x3) << 6) + auxiliarArray4[3];

            for (j = 0; (j < i - 1); j++)
                message += auxiliarArray3[j];
        }

        return message;
    }

}