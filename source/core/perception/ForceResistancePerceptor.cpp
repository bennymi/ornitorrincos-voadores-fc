/*
 * ForceResistancePerceptor.cpp
 *
 *  Created on: Aug 15, 2015
 *      Author: itandroids
 */

#include "ForceResistancePerceptor.h"

namespace perception {
    using namespace itandroids_lib::math;

    /*
     * This constructor takes a parser tree node and manipulates it to get information
     * of name, origin coordinates and force in x, y and z coordinates.
     */

    ForceResistancePerceptor::ForceResistancePerceptor(
            utils::parser::ParserTreeNode &node) :
            Perceptor(node) {
        //First child of node is name
        name = utils::parser::KeyValuePair(node.children[0]->content, ' ').value;
        boost::trim(name);
        //Second child of node is values of coordinates
        std::string originCoordinatesString = utils::parser::KeyValuePair(
                node.children[1]->content, ' ').value;
        std::vector<std::string> originStringVector;
        boost::split(originStringVector, originCoordinatesString,
                     boost::is_any_of(" "));
        originCoordinates.x = boost::lexical_cast<double>(originStringVector[0]);
        originCoordinates.y = boost::lexical_cast<double>(originStringVector[1]);
        originCoordinates.z = boost::lexical_cast<double>(originStringVector[2]);
        //Third child of node is values of force in each cartesian coordinate
        std::string forcesString = utils::parser::KeyValuePair(
                node.children[2]->content, ' ').value;
        std::vector<std::string> forcesStringVector;
        boost::split(forcesStringVector, forcesString, boost::is_any_of(" "));
        force.x = boost::lexical_cast<double>(forcesStringVector[0]);
        force.y = boost::lexical_cast<double>(forcesStringVector[1]);
        force.z = boost::lexical_cast<double>(forcesStringVector[2]);
        forceData = representations::ForceResistanceData(originCoordinates, force);
    }

    ForceResistancePerceptor::~ForceResistancePerceptor() {
    }

    /*
     * Getter methods: return information of agent state perceptor (origin coordinates, force and force data)
     */
    Vector3<double> ForceResistancePerceptor::getOriginCoordinates() {
        return originCoordinates;
    }

    Vector3<double> ForceResistancePerceptor::getForce() {
        return force;
    }

    representations::ForceResistanceData ForceResistancePerceptor::getForceResistanceData() {
        return forceData;
    }

} /* namespace perception */
