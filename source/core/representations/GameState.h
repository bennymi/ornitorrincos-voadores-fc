/*
 * GameState.h
 *
 *  Created on: Aug 7, 2015
 *      Author: mmaximo
 */

#ifndef SOURCE_REPRESENTATIONS_GAMESTATE_H_
#define SOURCE_REPRESENTATIONS_GAMESTATE_H_

#include "representations/RawPlayMode.h"
#include "boost/algorithm/string.hpp"

#include <iostream>

namespace representations {

    class GameState {
    public:
        GameState();

        /**
         * Constructor GameState.
         *
         * @param gameTime represents the current play time in seconds
         *
         * @param scoreLeft represents the Left Team score
         *
         * @param scoreRight represents the Right Team score
         *
         * @param playMode represents the current getDesiredJoints mode of the soccer game
         */
        GameState(double gameTime, int scoreLeft, int scoreRight,
                  std::string playMode);

        /**
         * Destructor.
         */
        virtual ~GameState();

        /**
         * Gets the time.
         *
         * @return the time.
         */
        double getGameTime();

        /**
         * Gets the score of the Left Team.
         *
         * @return the scoreLeft.
         */
        int getScoreLeft();

        /**
         * Gets the score of the Right Team.
         *
         * @return the scoreRight.
         */
        int getScoreRight();

        /**
         * Gets the score of the current getDesiredJoints mode of the soccer game.
         *
         * @return the enum playMode.
         */
        representations::RawPlayMode::RAW_PLAY_MODE getPlayMode();

    private:
        double gameTime;
        int scoreLeft;
        int scoreRight;
        representations::RawPlayMode::RAW_PLAY_MODE playMode;
    };

} /* namespace representations */

#endif /* SOURCE_REPRESENTATIONS_GAMESTATE_H_ */
