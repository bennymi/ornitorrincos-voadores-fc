//
// Created by francisco on 6/1/16.
//

#ifndef ITANDROIDS_SOCCER3D_CPP_COMPLEXKEYFRAMEPAGE_H
#define ITANDROIDS_SOCCER3D_CPP_COMPLEXKEYFRAMEPAGE_H

#include "control/keyframe/page/KeyframePage.h"

using ::control::keyframe::KeyframePage;

namespace control {
    namespace keyframe {
        class ComplexKeyframePage : public KeyframePage {
        public:
            ComplexKeyframePage();

            void addPage(std::shared_ptr<KeyframePage> page, bool isInverted, double speedRate);

            void init() override;

            void reset() override;

            virtual bool hasFinished() override;

            virtual double getMovementTime() override;

            virtual representations::NaoJoints &
            getDesiredJoints(representations::NaoJoints &perceivedJoints, double globalTime, bool isInverted) override;

            virtual std::vector<double> interpolate() override;


        private:
            virtual void createInterpolators() override;

            std::vector<bool> invertionValues;
            std::vector<std::shared_ptr<KeyframePage>> pages;
            std::vector<double> speedRates;
            double movementTime;
            int currentPageNumber;
            std::vector<double> pageTimes;

        };
    }
}


#endif //ITANDROIDS_SOCCER3D_CPP_COMPLEXKEYFRAMEPAGE_H
