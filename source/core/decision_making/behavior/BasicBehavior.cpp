/*
 * BasicBehavior.cpp
 *
 *  Created on: Feb 2, 2016
 *      Author: dicksiano
 */

#include "BasicBehavior.h"
#include "BehaviorFactory.h"
#include "utils/roboviz/Roboviz.h"

namespace decision_making {
    namespace behavior {

        const double BasicBehavior::SAFE_ANGLE_DISPLACEMENT = M_PI / 18;
        const double BasicBehavior::AGENT_RADIUS = 0.4;

        BasicBehavior::BasicBehavior() {

        }

        BasicBehavior::BasicBehavior(BehaviorFactory *behaviorFactory) : Behavior(behaviorFactory) {
        }

        BasicBehavior::~BasicBehavior() {
        }

        void BasicBehavior::behave(modeling::Modeling &modeling) {
            movementRequest = nullptr;

            utils::roboviz::Roboviz roboviz;
            std::string name("animated");
            std::string buff("animated");
            Pose2D ballPosition = modeling.getWorldModel().getBall().getPosition();
            Pose2D selfPosition = modeling.getWorldModel().getSelf().getPosition();
            roboviz.drawLine(selfPosition.translation.x, selfPosition.translation.y, 0, selfPosition.translation.x,
                             selfPosition.translation.y, 1, 1, 255, 255, 255, &name);
            roboviz.swapBuffers(&buff);

            if (modeling.getWorldModel().selfIsClosestToBall()) {
                changeBasicBehaviorState(modeling);
                perform(modeling);

            } else {
                behaviorFactory->getNavigateToStaticPosition().behave(modeling);
                movementRequest = behaviorFactory->getNavigateToStaticPosition().getMovementRequest();
            }

        }/* function behave */

        void BasicBehavior::changeBasicBehaviorState(modeling::Modeling &modeling) {

            Pose2D selfPosition = modeling.getWorldModel().getSelf().getPosition();
            Pose2D ballPosition = modeling.getWorldModel().getBall().getPosition();
            Vector3<double> goalPosition = modeling.getWorldModel().getTheirGoalPosition();

            behindBallPosition = calculateBehindBallPosition(modeling);
            safePosition = calculateSafePosition(modeling);
            kickingPosition = calculateKickingPosition(modeling);

            distanceFromTarget = (selfPosition - behindBallPosition).abs();
            angleDisplacement = selfPosition.rotation - calculateTargetAngle(modeling);


            switch (state) {
                case BehaviorState::BEHAVIOR_STATE::MOVING_BEHIND_BALL:

                    if (ballPosition.translation.x < selfPosition.translation.x)
                        state = BehaviorState::BEHAVIOR_STATE::HAS_PASSED_BALL;

                    else if (distanceFromTarget > 2 * AGENT_RADIUS)
                        state = BehaviorState::BEHAVIOR_STATE::MOVING_BEHIND_BALL;

                    else if (angleDisplacement > SAFE_ANGLE_DISPLACEMENT ||
                             angleDisplacement < -1 * SAFE_ANGLE_DISPLACEMENT)
                        state = BehaviorState::BEHAVIOR_STATE::TURNING_TO_THE_GOAL;

                    else
                        state = BehaviorState::BEHAVIOR_STATE::CONDUCTING_BALL_FORWARD;
                    break;

                case BehaviorState::BEHAVIOR_STATE::CONDUCTING_BALL_FORWARD:

                    if (ballPosition.translation.x + 0.2 < selfPosition.translation.x)
                        state = BehaviorState::BEHAVIOR_STATE::HAS_PASSED_BALL;

                    else if (distanceFromTarget > 5.0 * AGENT_RADIUS)
                        state = BehaviorState::BEHAVIOR_STATE::MOVING_BEHIND_BALL;

                    else if (distanceFromTarget < AGENT_RADIUS && (selfPosition - goalPosition).abs() < 2.0)
                        state = BehaviorState::BEHAVIOR_STATE::PREPARING_TO_KICK;
                    else
                        state = BehaviorState::BEHAVIOR_STATE::CONDUCTING_BALL_FORWARD;
                    break;

                case BehaviorState::BEHAVIOR_STATE::TURNING_TO_THE_GOAL:

                    if (distanceFromTarget > 5.0 * AGENT_RADIUS)
                        state = BehaviorState::BEHAVIOR_STATE::MOVING_BEHIND_BALL;

                    else if (angleDisplacement > SAFE_ANGLE_DISPLACEMENT
                             || angleDisplacement < -1 * SAFE_ANGLE_DISPLACEMENT)
                        state = BehaviorState::BEHAVIOR_STATE::TURNING_TO_THE_GOAL;
                    else
                        state = BehaviorState::BEHAVIOR_STATE::MOVING_BEHIND_BALL;
                    break;

                case BehaviorState::BEHAVIOR_STATE::HAS_PASSED_BALL:

                    if ((selfPosition - safePosition).abs() < 0.5 * AGENT_RADIUS)
                        state = BehaviorState::BEHAVIOR_STATE::MOVING_BEHIND_BALL;

                    else
                        state = BehaviorState::BEHAVIOR_STATE::HAS_PASSED_BALL;
                    break;

                case BehaviorState::BEHAVIOR_STATE::PREPARING_TO_KICK:

                    if (distanceFromTarget > 3 * AGENT_RADIUS)
                        state = BehaviorState::BEHAVIOR_STATE::MOVING_BEHIND_BALL;

                    else if ((selfPosition - kickingPosition).abs() > 0.1)
                        state = BehaviorState::BEHAVIOR_STATE::PREPARING_TO_KICK;
                    else
                        state = BehaviorState::BEHAVIOR_STATE::KICKING_THE_BALL;
                    lastStateSwitchTime = modeling.getAgentModel().getAgentPerception().getGlobalTime();
                    break;

                case BehaviorState::BEHAVIOR_STATE::KICKING_THE_BALL:

                    if (distanceFromTarget > 3 * AGENT_RADIUS)
                        state = BehaviorState::BEHAVIOR_STATE::MOVING_BEHIND_BALL;

                    else if ((selfPosition - kickingPosition).abs() > 0.2)
                        state = BehaviorState::BEHAVIOR_STATE::PREPARING_TO_KICK;

                    else if (modeling.getAgentModel().getAgentPerception().getGlobalTime() - lastStateSwitchTime > 5)
                        state = BehaviorState::BEHAVIOR_STATE::HAS_PASSED_BALL;
                    else
                        state = BehaviorState::BEHAVIOR_STATE::KICKING_THE_BALL;
                    break;

                default:
                    state = BehaviorState::BEHAVIOR_STATE::MOVING_BEHIND_BALL;
            }

        }/* function changeBasicBehaviorState */

        void BasicBehavior::perform(modeling::Modeling &modeling) {

            switch (state) {
                case BehaviorState::BEHAVIOR_STATE::MOVING_BEHIND_BALL:

                    behaviorFactory->getNavigatePosition().setTarget(behindBallPosition);
                    behaviorFactory->getNavigatePosition().behave(modeling);
                    movementRequest = behaviorFactory->getNavigatePosition().getMovementRequest();

                    break;

                case BehaviorState::BEHAVIOR_STATE::CONDUCTING_BALL_FORWARD:

                    behaviorFactory->getConductBall().behave(modeling);
                    movementRequest = behaviorFactory->getConductBall().getMovementRequest();

                    break;

                case BehaviorState::BEHAVIOR_STATE::TURNING_TO_THE_GOAL:

                    behindBallPosition.rotation = calculateTargetAngle(modeling);
                    behaviorFactory->getNavigatePosition().setTarget(behindBallPosition);
                    behaviorFactory->getNavigatePosition().behave(modeling);
                    movementRequest = behaviorFactory->getNavigatePosition().getMovementRequest();

                    break;

                case BehaviorState::BEHAVIOR_STATE::HAS_PASSED_BALL:

                    behaviorFactory->getNavigatePosition().setTarget(safePosition);
                    behaviorFactory->getNavigatePosition().behave(modeling);
                    movementRequest = behaviorFactory->getNavigatePosition().getMovementRequest();

                    break;

                case BehaviorState::BEHAVIOR_STATE::PREPARING_TO_KICK:

                    behaviorFactory->getNavigatePosition().setTarget(kickingPosition);
                    behaviorFactory->getNavigatePosition().behave(modeling);
                    movementRequest = behaviorFactory->getNavigatePosition().getMovementRequest();

                    break;

                case BehaviorState::BEHAVIOR_STATE::KICKING_THE_BALL:

                    movementRequest = std::make_shared<control::KickRequest>();

                    break;

                default:
                    state = BehaviorState::BEHAVIOR_STATE::MOVING_BEHIND_BALL;
            }
        } /* perform */

        BehaviorState::BEHAVIOR_STATE BasicBehavior::getState() {
            return state;
        }

        Pose2D BasicBehavior::calculateBehindBallPosition(modeling::Modeling &modeling) {

            Pose2D ballPosition = modeling.getWorldModel().getBall().getPosition();
            Pose2D selfPosition = modeling.getWorldModel().getSelf().getPosition();
            Vector3<double> goalPosition = modeling.getWorldModel().getTheirGoalPosition();
            itandroids_lib::math::Vector2<double> simpleGoalPosition(goalPosition.x, goalPosition.y);

            Pose2D behindBall = ballPosition;

            if ((ballPosition - goalPosition).abs() > 2)
                behindBall.translation =
                        behindBall.translation - (simpleGoalPosition - ballPosition.translation).normalize() / 5.0;
            else
                behindBall.translation =
                        behindBall.translation - (simpleGoalPosition - selfPosition.translation).normalize() / 5.0;

            behindBall.rotation = calculateTargetAngle(modeling);

            return behindBall;
        }

        Pose2D BasicBehavior::calculateSafePosition(modeling::Modeling &modeling) {

            Pose2D ballPosition = modeling.getWorldModel().getBall().getPosition();
            Pose2D selfPosition = modeling.getWorldModel().getSelf().getPosition();
            Vector3<double> goalPosition = modeling.getWorldModel().getTheirGoalPosition();
            itandroids_lib::math::Vector2<double> simpleGoalPosition(goalPosition.x, goalPosition.y);

            Pose2D safePosition = ballPosition;

            if ((ballPosition - goalPosition).abs() > 2)
                safePosition.translation =
                        safePosition.translation - (simpleGoalPosition - ballPosition.translation).normalize() / 2.0;
            else
                safePosition.translation =
                        safePosition.translation - (simpleGoalPosition - selfPosition.translation).normalize() / 2.0;

            safePosition.rotation = calculateTargetAngle(modeling);

            return safePosition;
        }

        Pose2D BasicBehavior::calculateKickingPosition(modeling::Modeling &modeling) {

            Pose2D ballPosition = modeling.getWorldModel().getBall().getPosition();
            Vector3<double> goalPosition = modeling.getWorldModel().getTheirGoalPosition();
            itandroids_lib::math::Vector2<double> simpleGoalPosition(goalPosition.x, goalPosition.y);

            Pose2D kickingPosition = ballPosition;
            kickingPosition.translation =
                    kickingPosition.translation - (simpleGoalPosition - ballPosition.translation).normalize() / 15.0;
            kickingPosition.rotation = calculateTargetAngle(modeling);

            kickingPosition.translation.y += 0.1 * AGENT_RADIUS;
            return kickingPosition;
        }

        double BasicBehavior::calculateTargetAngle(modeling::Modeling &modeling) {

            Vector3<double> goalPosition = modeling.getWorldModel().getTheirGoalPosition();
            itandroids_lib::math::Vector2<double> simpleGoalPosition(goalPosition.x, goalPosition.y);
            Pose2D ballPosition = modeling.getWorldModel().getBall().getPosition();

            return (simpleGoalPosition - ballPosition.translation).angle();
        }

    } /* namespace behavior */
} /* namespace decision_making */
