//
// Created by dicksiano on 16/02/16.
//

#include "NavigateToStaticPosition.h"
#include "BehaviorFactory.h"

namespace decision_making {
    namespace behavior {

        NavigateToStaticPosition::NavigateToStaticPosition(BehaviorFactory *behaviorFactory) :
                Behavior(behaviorFactory),
                delaunayPositioning(representations::ITAndroidsConstants::FORMATION_FOLDER) {
        }

        NavigateToStaticPosition::~NavigateToStaticPosition() {
        }

        void NavigateToStaticPosition::behave(modeling::Modeling &modeling) {
            movementRequest = nullptr;
            int uniformNumber = modeling.getAgentModel().getAgentNumber();
            int role = modeling.getWorldModel().getSynchronizedRoleAssignmentVector()[uniformNumber - 1];
            itandroids_lib::math::Pose2D selfPos = modeling.getWorldModel().getSelf().getPosition();
            itandroids_lib::math::Pose2D ballPos = modeling.getWorldModel().getBall().getPosition();
            /*utils::roboviz::Roboviz roboviz;

            std::string buffer("");
            roboviz.swapBuffers(&buffer);
            std::string selfStr("pos.self");*/
            /*
             * Initializing desiredPosition in actual position. In the case of role = 25, the player stay where it is.
             */
            itandroids_lib::math::Vector2<double> desiredPosition = modeling.getWorldModel().getSelf().getPosition().translation;
            if (role <= 10) {//positioning according to delaunay

                desiredPosition =
                        delaunayPositioning.getTeamPosition(modeling)[role];

                /*roboviz.drawLine((float)selfPos.translation.x,
                                 (float) selfPos.translation.y,
                                 0,
                                 (float)(desiredPosition.x ),
                                 (float)(desiredPosition.y ),
                                 0, 6, 233, 12, 0, &selfStr);*/
            } else { //positioning to mark opponent
                int opponentNumber = role - 10;
                /*
                 * Navigate just in valid values (avoid "25" initialized in Marking System)
                 * In major cases, the player knows the position of teammates (communication).
                 * But when it doesn't know, there are not initialized positions in Role Assignment Vector.
                 * This if is to prevent from this case.
                 */
                if (opponentNumber <= modeling::WorldModel::NUM_PLAYERS_PER_TEAM) {

                    representations::OtherPlayer opponentToMark = modeling.getWorldModel().getOpponent(opponentNumber);

                    /*
                    * Prevent to mark just opponents inside the field
                    * Sometimes the player doesn't know where is the opponent to mark.
                    */

                    if (fabs(opponentToMark.getPosition().translation.x) <
                        modeling::FieldDescription::FIELD_LENGTH / 2.0 &&
                        fabs(opponentToMark.getPosition().translation.y) <
                        modeling::FieldDescription::FIELD_WIDTH / 2.0) {
                        //mark 1.5m after opponent
                        itandroids_lib::math::Vector2<double> lineFromOppToGoal = itandroids_lib::math::Pose2D
                                                                                          (modeling.getWorldModel().getOwnGoalPosition()).translation -
                                                                                  opponentToMark.getPosition().translation;
                        lineFromOppToGoal = lineFromOppToGoal.normalize();
                        desiredPosition = itandroids_lib::math::Vector2<double>(
                                opponentToMark.getPosition().translation.x + 1.5 * lineFromOppToGoal.x,
                                opponentToMark.getPosition().translation.y + 1.5 * lineFromOppToGoal.y);
                    }
                    /* roboviz.drawLine((float)selfPos.translation.x,
                                      (float) selfPos.translation.y,
                                      0,
                                      (float)(desiredPosition.x ),
                                      (float)(desiredPosition.y ),
                                      0, 6, 233, 12, 0, &selfStr);
                     roboviz.drawCircle((float)desiredPosition.x,
                                        (float) desiredPosition.y,
                                        0.2, 1, 233, 12, 0, &selfStr);*/
                }
            }
            behaviorFactory->getNavigatePosition().setTarget(desiredPosition);
            behaviorFactory->getNavigatePosition().behave(modeling);
            movementRequest = behaviorFactory->getNavigatePosition().getMovementRequest();
        }

        itandroids_lib::math::Pose2D NavigateToStaticPosition::getStrategicPosition(modeling::Modeling &modeling) {
            return delaunayPositioning.getAgentPosition(modeling);
        }

    } /* namespace behavior */
} /* namespace decision_making */