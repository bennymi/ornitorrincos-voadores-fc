/*
 * GameStatePerceptor.h
 *
 *  Created on: Aug 15, 2015
 *      Author: itandroids
 */

#ifndef SOURCE_PERCEPTION_GAMESTATEPERCEPTOR_H_
#define SOURCE_PERCEPTION_GAMESTATEPERCEPTOR_H_

#include "utils/parser/KeyValuePair.h"
#include "utils/parser/ParserTreeNode.h"
#include "perception/Perceptor.h"
#include <boost/lexical_cast.hpp>
#include "representations/PlaySide.h"
#include "representations/PlayMode.h"

namespace perception {

    class GameStatePerceptor : public Perceptor {
    public:
        /**
         * Default constructor for GameStatePerceptor.
         *
         */
        GameStatePerceptor();

        /**
         * Constructor for GameStatePerceptor.
         *
         * @param node part of the parser tree that represents
         *             an GameStatePerceptor.
         */
        GameStatePerceptor(utils::parser::ParserTreeNode &node);

        /**
         * Destructor.
         */
        virtual ~GameStatePerceptor();

        /**
         * Gets the game's time.
         *
         * @return the game's time.
         */
        double getGameTime();

        /**
         * Gets the game's play mode.
         *
         * @return a string that represents the current getDesiredJoints mode
         */
        std::string getPlayMode();

        /**
         * Gets the score of the left team.
         *
         * @return the score of the left team.
         */
        int getScoreLeft();

        /**
         * Gets the score of the right team.
         *
         * @return the score of the right team.
         */
        int getScoreRight();

        /**
         * Gets the uniform number of the robot.
         *
         * @return the uniform number of the robot.
         */
        int getUniformNumber();

        /**
         * Gets the boolean that tells if it is the first time to receive a message of game state
         * @return boolean that is true if is the first time
         */
        bool isFirstTime();

        /**
         * @return playSide
         */
        representations::PlaySide::PLAY_SIDE getPlaySide();

    protected:

    private:
        int uniformNumber;
        representations::PlaySide::PLAY_SIDE playSide;
        int scoreLeft;
        int scoreRight;
        bool firstTime;
        double gameTime;
        std::string playMode;
    };

} /* namespace perception */

#endif /* SOURCE_PERCEPTION_GAMESTATEPERCEPTOR_H_ */
