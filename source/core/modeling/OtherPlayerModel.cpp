/*
 * PlayerModel.cpp
 *
 *  Created on: Aug 29, 2015
 *      Author: mmaximo
 */

#include "OtherPlayerModel.h"

namespace modeling {

    OtherPlayerModel::OtherPlayerModel() {
        lastGlobalTime = 0;
        dt = 0.02;
    }

    OtherPlayerModel::OtherPlayerModel(int uniformNumber) {
        otherPlayer.setId(uniformNumber);
        lastGlobalTime = 0;
        dt = 0.02;
    }

    OtherPlayerModel::~OtherPlayerModel() {
    }

    void OtherPlayerModel::update(const double globalTime,
                                  perception::VisiblePlayer *visiblePlayer,
                                  Pose2D &odometry, Pose2D &agentEstimate) {
        if (visiblePlayer == nullptr) {
            tracker.update(dt, odometry);

        } else {
            tracker.update(dt, visiblePlayer, odometry);
            otherPlayer.setLastSeenTime(globalTime); // Update the last time that the ball was seen
        }
        otherPlayer.setLocalPosition(
                itandroids_lib::math::Pose2D(tracker.getPosition().translation.x, tracker.getPosition().translation.y));
        otherPlayer.setPosition(
                otherPlayer.getLocalPosition().relativeToGlobal(agentEstimate)); // Update ball's position

        otherPlayer.setLocalVelocity(
                itandroids_lib::math::Pose2D(tracker.getVelocity().translation.x, tracker.getVelocity().translation.y));
        otherPlayer.setVelocity(
                itandroids_lib::math::Pose2D(tracker.getVelocity().translation.x * cos(agentEstimate.rotation) -
                                             tracker.getVelocity().translation.y * sin(agentEstimate.rotation),
                                             tracker.getVelocity().translation.x * sin(agentEstimate.rotation) +
                                             tracker.getVelocity().translation.y * cos(agentEstimate.rotation)));
        // Update ball's velocity
        otherPlayer.setRelevant(!tracker.hasLostPlayer());
        lastGlobalTime = globalTime; // Update last time that the ball model was updated

        if (visiblePlayer != nullptr)
            updateFallen(visiblePlayer);
    }

    void OtherPlayerModel::update(representations::HearData hearData) {
        otherPlayer.setPosition(itandroids_lib::math::Pose2D(hearData.getHeardAgentXPosition(),
                                                             hearData.getHeardAgentYPosition()));
        otherPlayer.setId(hearData.getUniformNumber());
        otherPlayer.setLastHeardTime(hearData.getTime());
        otherPlayer.setIsFallen(hearData.isFallen());
        otherPlayer.setRelevant(!hearData.isFallen());


    }


    void OtherPlayerModel::update(OtherPlayer heardPlayer) {
        otherPlayer.setPosition(Pose2D(heardPlayer.getPosition()));
        otherPlayer.setId(heardPlayer.getId());
        otherPlayer.setLastHeardTime(heardPlayer.getLastHeardTime());
        otherPlayer.setIsFallen(heardPlayer.isFallen());
        otherPlayer.setRelevant(heardPlayer.isRelevant());

    }

    OtherPlayer &OtherPlayerModel::getObject() {
        return otherPlayer;
    }


    void OtherPlayerModel::updateFallen(perception::VisiblePlayer *visiblePlayer) {
        if (visiblePlayer->getAllBodyParts().size() < 3) {
            otherPlayer.setIsFallen(false);
            return;
        }

        double minZ = 1000.0;
        double maxZ = -1000.0;
        double zPart;
        for (auto &pair : visiblePlayer->getAllBodyParts()) {
            zPart = pair.second.z;
            if (zPart < minZ)
                minZ = zPart;
            if (zPart > maxZ)
                maxZ = zPart;
        }

        bool fallen;
        if (maxZ - minZ < 0.25)
            fallen = true;
        else
            fallen = false;

        otherPlayer.setIsFallen(fallen);
    }

} /* namespace modeling */
