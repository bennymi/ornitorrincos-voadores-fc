/*
 * DelaunayFormationFactory.h
 *
 *  Created on: Oct 28, 2015
 *      Author: itandroids
 */

#ifndef SOURCE_DECISION_MAKING_POSITIONING_DELAUNAY_TRIANGULATION_DELAUNAYFORMATIONFACTORY_H_
#define SOURCE_DECISION_MAKING_POSITIONING_DELAUNAY_TRIANGULATION_DELAUNAYFORMATIONFACTORY_H_

#include <map>
#include <iostream>
#include "DelaunayInterpolator.h"
#include "DelaunayFileParser.h"
#include <boost/filesystem.hpp>
#include "boost/filesystem/operations.hpp" // includes boost/filesystem/path.hpp
#include "boost/filesystem/fstream.hpp"
#include "modeling/WorldModel.h"
#include <boost/lexical_cast.hpp>
#include "DelaunayFormationType.h"
#include "modeling/Modeling.h"

namespace decision_making {
    namespace positioning {
        namespace delaunay_triangulation {

            /**
             * Class that represents Delaunay Formations Factory
             */
            class DelaunayFormationFactory {
            public:
                /**
                 * Constructor to Delaunay Formation Factory
                 *
                 * @param string with the path to the configuration files folder.
                 */
                DelaunayFormationFactory(std::string folderPath);

                /**
                 * Destructor to Delaunay Formation Factory
                 */
                virtual ~DelaunayFormationFactory();

                /**
                 * Getter for triangulations
                 *
                 * @return vector of pointers to Delaunay Interpolator
                 */
                std::vector<std::shared_ptr<DelaunayInterpolator>> getTriangulations();

            private:
                std::string path;
                std::vector<std::shared_ptr<DelaunayInterpolator>> triangulations;
                std::vector<std::vector<itandroids_lib::geometry::Vector2D> > positions;

            };

        } /* namespace delaunay_triangulation */
    } /* namespace positioning */
} /* namespace decision_making */

#endif /* SOURCE_DECISION_MAKING_POSITIONING_DELAUNAY_TRIANGULATION_DELAUNAYFORMATIONFACTORY_H_ */
