//
// Created by dicksiano on 8/14/16.
//

#include "OwnGoalSetPlay.h"
#include "../BehaviorFactory.h"

namespace decision_making {
    namespace behavior {

        OwnGoalSetPlay::OwnGoalSetPlay() : SetPlay(nullptr) {
        }

        OwnGoalSetPlay::OwnGoalSetPlay(BehaviorFactory *behaviorFactory) : SetPlay(behaviorFactory) {
        }

        OwnGoalSetPlay::~OwnGoalSetPlay() {
        }

        void OwnGoalSetPlay::behave(modeling::Modeling &modeling) {
            behaviorFactory->getBeamBehavior().behave(modeling);
            beamRequest = behaviorFactory->getBeamBehavior().getBeamRequest();
        } /* function behave */

    } /* namespace behavior */
} /* namespace decision_making */