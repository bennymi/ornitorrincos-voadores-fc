/*
 * UniversalJointPerceptor.h
 *
 *  Created on: Aug 23, 2015
 *      Author: Dicksiano
 */

#ifndef SOURCE_PERCEPTION_UNIVERSALJOINTPERCEPTOR_H_
#define SOURCE_PERCEPTION_UNIVERSALJOINTPERCEPTOR_H_

#include <string>
#include <stdlib.h>
#include "perception/Perceptor.h"
#include "utils/parser/KeyValuePair.h"
#include "utils/parser/ParserTreeNode.h"

namespace perception {
    class UniversalJointPerceptor : public Perceptor {
    public:
        /**
         * Assignment constructor
         *
         */
        UniversalJointPerceptor(utils::parser::ParserTreeNode &node);

        /**
         *	Destructor
         */
        virtual ~UniversalJointPerceptor();

        /**
         * Get name
         *
         * @return name
         */
        std::string getName();

        /**
         * Get angleAxis1 value
         *
         * @return value
         */
        double getAngleAxis1();

        /**
         * Get angleAxis2 value
         *
         * @return value
         */
        double getAngleAxis2();

    private:
        /**
         * name represents the name of the perceptor.
         *
         */
        std::string name;
        /**
         * angleAxis1 represents the position angle of the first axis in degrees
         *
         */
        double angleAxis1;
        /**
         * angleAxis2 represents the position angle of the second axis in degrees
         *
         */
        double angleAxis2;
    };

} /* namespace perception */

#endif /* SOURCE_PERCEPTION_UNIVERSALJOINTPERCEPTOR_H_ */
