/*
 * OtherPlayer.cpp
 *
 *  Created on: Sep 8, 2015
 *      Author: mmaximo
 */

#include "OtherPlayer.h"

namespace representations {

    using namespace itandroids_lib::math;

    OtherPlayer::OtherPlayer() {
        lastSeenTime = 0;
        hasRelevance = false;
    }

    OtherPlayer::OtherPlayer(Pose2D position, Pose2D velocity) {
        lastHeardTime = 0;
        lastSeenTime = 0;
        this->position = position;
        this->velocity = velocity;
        hasRelevance = false;
    }

    OtherPlayer::~OtherPlayer() {
    }

    bool OtherPlayer::isRelevant() const {
        ///@todo add relevance criteria

        return hasRelevance;
    }


    void OtherPlayer::setRelevant(bool relevant) {
        this->hasRelevance = relevant;
    }

    Pose2D OtherPlayer::getPosition() const {
        return position;
    }

    Pose2D OtherPlayer::getVelocity() const {
        return velocity;
    }

    int OtherPlayer::getId() const {
        return uniformNumber;
    }

    void OtherPlayer::setPosition(Pose2D position) {
        hasRelevance = true;
        this->position = position;
    }

    void OtherPlayer::setVelocity(Pose2D velocity) {
        this->velocity = velocity;
    }

    void OtherPlayer::setId(int uniformNumber) {
        this->uniformNumber = uniformNumber;
    }

    void OtherPlayer::setLastSeenTime(double globalTime) {
        this->lastSeenTime = globalTime;
    }

    double OtherPlayer::getLastSeenTime() {
        return lastSeenTime;
    }

    double OtherPlayer::getLastHeardTime() {
        return lastHeardTime;
    }

    double OtherPlayer::setLastHeardTime(double lastHeardTime) {
        this->lastHeardTime = lastHeardTime;
    }

    void OtherPlayer::setIsFallen(bool isFallen) {
        this->hasFallen = isFallen;
    }

    double OtherPlayer::getAge(double currentTime) {
        return currentTime - lastSeenTime;
    }

    Pose2D OtherPlayer::getLocalPosition() const {
        return localPosition;
    }

    Pose2D OtherPlayer::getLocalVelocity() const {
        return localVelocity;
    }

    void OtherPlayer::setLocalPosition(Pose2D position) {
        this->localPosition = position;
    }

    void OtherPlayer::setLocalVelocity(Pose2D velocity) {
        this->localVelocity = velocity;
    }

    bool OtherPlayer::isFallen() {
        return hasFallen;
    }

    bool OtherPlayer::isGoalie() {
        return (uniformNumber == 1);
    }
} /* namespace representations */
