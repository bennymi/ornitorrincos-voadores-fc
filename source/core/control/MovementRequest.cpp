/*
 * MovementRequest.cpp
 *
 *  Created on: Oct 1, 2015
 *      Author: mmaximo
 */

#include "control/MovementRequest.h"

namespace control {

    MovementRequest::MovementRequest(MovementRequestType::MOVEMENT_REQUEST_TYPE requestType) :
            requestType(requestType) {
    }

    MovementRequest::~MovementRequest() {
    }

    MovementRequestType::MOVEMENT_REQUEST_TYPE MovementRequest::getMovementType() {
        return requestType;
    }

} /* namespace control */
