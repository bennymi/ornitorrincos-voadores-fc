/*
 * RobotTypeProfile.cpp
 *
 *  Created on: Oct 10, 2015
 *      Author: mmaximo
 */

#include "RobotType.h"
#include "RobotTypeProfile.h"

namespace representations {

    RobotTypeProfile::RobotTypeProfile() :
            hip1TranslationX(0.0), hip1TranslationZ(0.0), thighTranslationZ(0.0), ankleTranslationZ(
            0.0), elbowTranslationY(0.0), footPitchMaxSpeed(0.0), footRollMaxSpeed(
            0.0), useToe(false), toeLength(0.0) {
    }

    RobotTypeProfile::~RobotTypeProfile() {
    }

    double RobotTypeProfile::getHip1TranslationX() {
        return hip1TranslationX;
    }

    double RobotTypeProfile::getHip1TranslationZ() {
        return hip1TranslationZ;
    }

    double RobotTypeProfile::getThighTranslationZ() {
        return thighTranslationZ;
    }

    double RobotTypeProfile::getAnkleTranslationZ() {
        return ankleTranslationZ;
    }

    double RobotTypeProfile::getElbowTranslationY() {
        return elbowTranslationY;
    }

    double RobotTypeProfile::getFootPitchMaxSpeed() {
        return footPitchMaxSpeed;
    }

    double RobotTypeProfile::getFootRollMaxSpeed() {
        return footRollMaxSpeed;
    }

    bool RobotTypeProfile::getUseToe() {
        return useToe;
    }

    double RobotTypeProfile::getToeLength() {
        return toeLength;
    }

    RobotTypeProfile RobotTypeProfile::getRobotTypeProfile(representations::RobotType::ROBOT_TYPE robotType) {
        RobotTypeProfile profile;
        switch (robotType) {
            case representations::RobotType::STANDART_NAO:
                //# Type 0 (Standard Nao)
                //{
                //    'Hip1RelTorso_X' => 0.055,
                //    'Hip1RelTorso_Z' => -0.115,
                //    'ThighRelHip2_Z' => -0.04,
                //    'AnkleRelShank_Z' => -0.055,
                //    'lj5_max_abs_speed' => 6.1395,
                //    'lj6_max_abs_speed' => 6.1395,
                //    'ElbowRelUpperArm_Y' => 0.07,
                //    'UseToe' => 'false'
                //},
                profile.hip1TranslationX = 0.055;
                profile.hip1TranslationZ = -0.115;
                profile.thighTranslationZ = -0.04;
                profile.ankleTranslationZ = -0.055;
                profile.elbowTranslationY = 0.07;
                profile.footPitchMaxSpeed = 6.1395;
                profile.footRollMaxSpeed = 6.1395;
                profile.useToe = false;
                profile.toeLength = 0.0;
                break;
            case 1:
                //# Type 1 (longer legs and arms)
                //{
                //    'Hip1RelTorso_X' => 0.055,
                //    'Hip1RelTorso_Z' => -0.115,
                //    'ThighRelHip2_Z' => -0.05832,
                //    'AnkleRelShank_Z' => -0.07332,
                //    'lj5_max_abs_speed' => 6.1395,
                //    'lj6_max_abs_speed' => 6.1395,
                //    'ElbowRelUpperArm_Y' => 0.10664,
                //    'UseToe' => 'false'
                //},
                profile.hip1TranslationX = 0.055;
                profile.hip1TranslationZ = -0.115;
                profile.thighTranslationZ = -0.05832;
                profile.ankleTranslationZ = -0.07332;
                profile.elbowTranslationY = 0.10664;
                profile.footPitchMaxSpeed = 6.1395;
                profile.footRollMaxSpeed = 6.1395;
                profile.useToe = false;
                profile.toeLength = 0.0;
                break;
            case 2:
                //# Type 2 (faster ankle-pitch and slower ankle-roll speed)
                //{
                //    'Hip1RelTorso_X' => 0.055,
                //    'Hip1RelTorso_Z' => -0.115,
                //    'ThighRelHip2_Z' => -0.04,
                //    'AnkleRelShank_Z' => -0.055,
                //    'lj5_max_abs_speed' => 8.80667,
                //    'lj6_max_abs_speed' => 3.47234,
                //    'ElbowRelUpperArm_Y' => 0.07,
                //    'UseToe' => 'false'
                //},
                profile.hip1TranslationX = 0.055;
                profile.hip1TranslationZ = -0.115;
                profile.thighTranslationZ = -0.05832;
                profile.ankleTranslationZ = -0.07332;
                profile.elbowTranslationY = 0.10664;
                profile.footPitchMaxSpeed = 8.80667;
                profile.footRollMaxSpeed = 3.47234;
                profile.useToe = false;
                profile.toeLength = 0.0;
                break;
            case 3:
                //# Type 3 (longer legs and arms + wider hip)
                //{
                //    'Hip1RelTorso_X' => 0.072954143,
                //    'Hip1RelTorso_Z' => -0.115,
                //    'ThighRelHip2_Z' => -0.067868424,
                //    'AnkleRelShank_Z' => -0.082868424,
                //    'lj5_max_abs_speed' => 6.1395,
                //    'lj6_max_abs_speed' => 6.1395,
                //    'ElbowRelUpperArm_Y' => 0.125736848,
                //    'UseToe' => 'false'
                //},
                profile.hip1TranslationX = 0.072954143;
                profile.hip1TranslationZ = -0.115;
                profile.thighTranslationZ = -0.067868424;
                profile.ankleTranslationZ = -0.082868424;
                profile.elbowTranslationY = 0.125736848;
                profile.footPitchMaxSpeed = 6.1395;
                profile.footRollMaxSpeed = 6.1395;
                profile.useToe = false;
                profile.toeLength = 0.0;
                break;
            case 4:
                //# Type 4 (Nao toes model)
                //{
                //    'Hip1RelTorso_X' => 0.055,
                //    'Hip1RelTorso_Z' => -0.115,
                //    'ThighRelHip2_Z' => -0.04,
                //    'AnkleRelShank_Z' => -0.055,
                //    'lj5_max_abs_speed' => 6.1395,
                //    'lj6_max_abs_speed' => 6.1395,
                //    'ElbowRelUpperArm_Y' => 0.07,
                //    'UseToe' => 'true',
                //    'ToeLength' => 0.035517656
                //}
                profile.hip1TranslationX = 0.055;
                profile.hip1TranslationZ = -0.115;
                profile.thighTranslationZ = -0.04;
                profile.ankleTranslationZ = -0.055;
                profile.elbowTranslationY = 0.07;
                profile.footPitchMaxSpeed = 6.1395;
                profile.footRollMaxSpeed = 6.1395;
                profile.useToe = true;
                profile.toeLength = 0.035517656;
                break;
        }
        return profile;
    }


} /* namespace representations */
