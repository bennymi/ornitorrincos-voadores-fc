/*
 * ServerSocket.h
 *
 *  Created on: Aug 7, 2015
 *      Author: mmaximo
 */

#ifndef SOURCE_COMMUNICATION_SERVERSOCKET_H_
#define SOURCE_COMMUNICATION_SERVERSOCKET_H_

#include <string>
#include <errno.h>
#include <fcntl.h>
#include <netdb.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <stdlib.h>
#include <stdio.h>
#include <strings.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

#include <errno.h>
#include <arpa/inet.h>
//#include "modeling/AgentModel.h"
//#include "modeling/WorldModel.h"

namespace communication {

/**
 * Abstract class that represents a server socket.
 */
    class ServerSocket {
    public:
        /**
         * Default constructor of ServerSocket.
         */
        ServerSocket();

        /**
         * Default destructor of ServerSocket.
         */

        virtual ~ServerSocket();

        virtual bool establishConnection() = 0;

        virtual void closeConnection() = 0;

        virtual std::string receiveMessage() = 0;

        virtual void sendMessage(const std::string &message) = 0;

        virtual bool connectionClosed() = 0;
    };

/**
 * Class that represents the implementation of the server socket.
 */
    class ServerSocketImpl : public ServerSocket {
    public:
        /**
         * Default constructor of ServerSocketImpl.
         *
         * @param host the host of the server socket.
         * @param port the port of the server socket.
         */
        ServerSocketImpl(std::string host, int port);

        /**
         * Default destructor of ServerSocketImpl.
         */

        virtual ~ServerSocketImpl();

        /**
         * Tries to establish the connection with the server.
         *
         * @return a bool that checks if the connection was established.
         */

        bool establishConnection() override;

        /**
         * Closes the connection with the server.
         */

        void closeConnection();

        /**
         * Receives the message from the server.
         *
         * @return a string that contains the message.
         */

        std::string receiveMessage();

        /**
         * Sends the message to the server.
         *
         * @param message string with the specific message.
         */

        void sendMessage(const std::string &message);

        bool connectionClosed() override { return connectionTerminated; }

    private:
        static const int BUFFER_SIZE = 16384;

        std::string host;
        int port;
        int socketFileDescriptor;
        fd_set socketSet;
        struct timeval timeout;
        struct sockaddr_in serverAddress;
        unsigned char receiveBuffer[BUFFER_SIZE];
        unsigned char sendBuffer[BUFFER_SIZE];
        bool connectionTerminated = false;
    };

} /* namespace communication */

#endif /* SOURCE_COMMUNICATION_SERVERSOCKET_H_ */
