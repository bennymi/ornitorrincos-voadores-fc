/*
 * VisiblePlayer.cpp
 *
 *  Created on: Sep 1, 2015
 *      Author: itandroids
 */

#include "VisiblePlayer.h"

namespace perception {
    /*
     * Constructor to the case of no data.
     */
    VisiblePlayer::VisiblePlayer() {
        id = 0;
        teamName = "unknown";
    }

    /**
     * This constructor takes a parser tree node and manipulates it to get information
     * of visible player - id and team name.
     */
    VisiblePlayer::VisiblePlayer(utils::parser::ParserTreeNode &node) {

        //Iterating over the children of the node
        setObjectType(node.content);
        std::vector<utils::parser::ParserTreeNode *>::iterator vectorIterator;
        for (vectorIterator = node.children.begin();
             vectorIterator != node.children.end(); vectorIterator++) {
            utils::parser::KeyValuePair pair = utils::parser::KeyValuePair(
                    (**vectorIterator).content, ' ');
            std::string keyString = pair.key;
            std::string valueString = pair.value;
            boost::trim(keyString);
            boost::trim(valueString);


            if (keyString.compare("") == 0) {
                keyString = (**vectorIterator).content;
                boost::trim(keyString);
            }


            if (keyString.compare("id") == 0) {
                id = boost::lexical_cast<int>(valueString);
            } else if (keyString.compare("team") == 0) {
                teamName = valueString;
                boost::trim(teamName);
            } else {
                utils::parser::KeyValuePair auxValue = utils::parser::KeyValuePair(
                        (**vectorIterator).children[0]->content, ' ');
                std::string polString = auxValue.value;
                std::vector<std::string> stringVector;
                boost::split(stringVector, polString, boost::is_any_of(" "));
                Vector3<double> partPosition;
                partPosition = Vector3<double>(
                        MathUtils::degreesToRadians(boost::lexical_cast<double>(stringVector[1])),
                        MathUtils::degreesToRadians(boost::lexical_cast<double>(stringVector[2])));
                partPosition = Vector3<double>(boost::lexical_cast<double>(stringVector[0]), partPosition);
                bodyPartMap.insert(
                        std::pair<std::string, Vector3<double> >(keyString,
                                                                 partPosition));
            }
        }

        // mmaximo: apparently, there was a bug here where the a player
        // position was Vector3<double>(0.0, 0.0, 0.0) when only one
        // body part was seen
        if (!bodyPartMap.empty()) {// && bodyPartMap.size() > 1) {
            // when seeing multiple body parts we average the position
            Vector3<double> result = Vector3<double>();
            for (stringVectorMap::iterator it = bodyPartMap.begin();
                 it != bodyPartMap.end(); it++) {
                result = result + (*it).second;
            }
            result = Vector3<double>(1.0 / bodyPartMap.size(), result);
            position = result;
        }
    }

    VisiblePlayer::~VisiblePlayer() {
    }

    /*
     * Getter methods: returns visible player informations (ID, team name, body parts)
     */

    int VisiblePlayer::getId() {
        return id;
    }

    std::string VisiblePlayer::getTeamName() {
        return teamName;
    }

    stringVectorMap VisiblePlayer::getAllBodyParts() {
        return bodyPartMap;
    }

    /*
     * This method sets the position of body part
     */
    void VisiblePlayer::setBodyPartPosition(std::string partName,
                                            Vector3<double> position) {
        bodyPartMap.insert(stringVectorMap::value_type(partName, position));
    }

    Vector3<double> VisiblePlayer::getBodyPartPosition(
            std::string partName) {
        return bodyPartMap[partName];
    }

    void VisiblePlayer::transformPosition(Pose3D &transform) {
        position = transform * position;
        if (!bodyPartMap.empty()) {
            auto iterator = bodyPartMap.begin();
            for (; iterator != bodyPartMap.end(); ++iterator) {
                iterator->second = transform * (*iterator).second;
            }
        }
    }
} /* namespace perception */
