/*
 * KeyframeRequest.cpp
 *
 *  Created on: Oct 24, 2015
 *      Author: itandroids
 */

#include "KeyframeRequest.h"

namespace control {

    KeyframeRequest::KeyframeRequest(std::string keyframeName, bool isInverted) : MovementRequest(
            MovementRequestType::KEYFRAME_REQUEST), inverted(isInverted), name(keyframeName) {

    }

    KeyframeRequest::~KeyframeRequest() {
    }

    bool KeyframeRequest::isInverted() {
        return inverted;
    }

    std::string KeyframeRequest::getName() {
        return name;
    }
} /* namespace control */
