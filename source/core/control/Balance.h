//
// Created by mmaximo on 22/06/16.
//

#ifndef ITANDROIDS_SOCCER3D_CPP_BALANCE_H
#define ITANDROIDS_SOCCER3D_CPP_BALANCE_H


#include "modeling/AgentModel.h"
#include "representations/NaoJoints.h"

namespace control {

    using modeling::AgentModel;
    using representations::NaoJoints;

    class Balance {
    public:
        Balance();

        void balance(AgentModel &agentModel, NaoJoints &joints);

    private:
        double hipPitchGain;
        double anklePitchGain;
        double hipRollGain;
        double ankleRollGain;
    };

} /* namespace control */


#endif //ITANDROIDS_SOCCER3D_CPP_BALANCE_H
