//
// Created by francisco on 6/20/16.
//

#include "ZMPKick.h"

namespace decision_making {
    namespace behavior {
        namespace kick_behavior {
            ZMPKick::ZMPKick() {
                setKickParameters(
                        KickParameters(Vector2<double>(-0.207577964502299, -0.015), 0.2));
            }

            void ZMPKick::behave(modeling::Modeling &modeling) {
                movementRequest = std::make_shared<control::KickRequest>();
            }
        }
    }
}
