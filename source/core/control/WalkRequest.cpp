/*
 * WalkRequest.cpp
 *
 *  Created on: Oct 24, 2015
 *      Author: itandroids
 */

#include "WalkRequest.h"

namespace control {
    WalkRequest::WalkRequest(itandroids_lib::math::Pose2D velocity) : MovementRequest(
            MovementRequestType::WALK_REQUEST), velocity(velocity) {
        requestType = MovementRequestType::WALK_REQUEST;
        walkMode = NORMAL;
    }

    WalkRequest::WalkRequest(itandroids_lib::math::Pose2D velocity, WalkMode walkMode) : WalkRequest(velocity) {
        this->walkMode = PRECISE;
    }

    WalkRequest::~WalkRequest() {

    }

    itandroids_lib::math::Pose2D &WalkRequest::getVelocity() {
        return velocity;
    }

    WalkMode WalkRequest::getWalkMode() {
        return walkMode;
    }


} /* namespace control */
