//
// Created by francisco on 6/16/16.
//

#include "LongDistance20mKick.h"

namespace decision_making {
    namespace behavior {
        namespace kick_behavior {
            LongDistance20mKick::LongDistance20mKick() : KeyframeKick(std::string("kick20mLeftLeg")) {

                itandroids_lib::math::RotationMatrix matrix;
//                        double rotationValue = itandroids_lib::math::MathUtils::degreesToRadians(-25.335);
                double rotationValue = itandroids_lib::math::MathUtils::degreesToRadians(0);
                matrix.rotateZ(rotationValue);
//                Vector3<double> originalvector(-0.16,-0.04,0);
//                double orientation = 0.2 + rotationValue;
                Vector3<double> originalvector(-0.13, -0.015, 0);
                double orientation = 0.13 + rotationValue;
                originalvector = matrix * originalvector;
                Vector2<double> finalPosition(originalvector.x, originalvector.y);
                setKickParameters(
                        KickParameters(finalPosition, orientation));
//                        KickParameters(Vector2<double>(-0.17,-0.015), 0.2));
            }
        }
    }
}



