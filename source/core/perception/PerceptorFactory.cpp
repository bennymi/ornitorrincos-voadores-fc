/*
 * PerceptorFactory.cpp
 *
 *  Created on: Aug 15, 2015
 *      Author: itandroids
 */

#include "perception/PerceptorFactory.h"
#include "utils/parser/Parser.h"

namespace perception {

    /*
     * Constructor for the case of no data: just cleares perceptor list
     */
    PerceptorFactory::PerceptorFactory() {
        perceptors.clear();
    }

    PerceptorFactory::~PerceptorFactory() {
    }

    /*
     * update cleares the list of perceptors and iterates over the parser tree to get
     * information about all perceptors, updating the data of perceptor list
     */
    void PerceptorFactory::update(utils::parser::ParserTreeNode &node) {
        //clearing list of perceptors
        for (auto iter = perceptors.begin(); iter != perceptors.end(); iter++) {
            delete (*iter);
        }
        perceptors.clear();

        //iterating over all parser tree and get information of
        //each perceptor
        std::vector<std::string> stringVector;
        for (auto perceptorVectorIterator = node.children.begin();
             perceptorVectorIterator != node.children.end();
             perceptorVectorIterator++) {

            /* Trimming the node string*/
            boost::algorithm::trim((*perceptorVectorIterator)->content);

            boost::split(stringVector, (**perceptorVectorIterator).content,
                         boost::is_any_of(" "));
            std::string compareString = stringVector[0];
            /* if else perceptors */
            if (compareString == "GYR") {
                GyroRatePerceptor *gyrometerPerceptor = new GyroRatePerceptor(
                        **(perceptorVectorIterator));
                perceptors.push_back(gyrometerPerceptor);
                //gyroRatePerceptors.insert(
                //std::pair<std::string, GyroRatePerceptor>(
                //		gyrometerPerceptor.getName(), gyrometerPerceptor));
            } else if (compareString == "HJ") {
                HingeJointPerceptor *hingeJointPerceptor = new HingeJointPerceptor(
                        **(perceptorVectorIterator));
                perceptors.push_back(hingeJointPerceptor);
                //hingeJointPerceptors.insert(
                //	std::pair<std::string, HingeJointPerceptor>(
                //		hingeJointPerceptor.getName(),
                //	hingeJointPerceptor));
            } else if (compareString == "UJ") {
                UniversalJointPerceptor *universalJointPerceptor =
                        new UniversalJointPerceptor(**(perceptorVectorIterator));
                perceptors.push_back(universalJointPerceptor);
                //universalJointPerceptors.insert(
                //	std::pair<std::string, UniversalJointPerceptor>(
                //		universalJointPerceptor.getName(),
                //		universalJointPerceptor));
            } else if (compareString == "TCH") {
                TouchPerceptor *touchPerceptor = new TouchPerceptor(
                        **(perceptorVectorIterator));
                perceptors.push_back(touchPerceptor);
                //touchPerceptors.insert(
                //	std::pair<std::string, TouchPerceptor>(
                //		touchPerceptor.getName(), touchPerceptor));
            } else if (compareString == "FRP") {
                ForceResistancePerceptor *forceResistancePerceptor =
                        new ForceResistancePerceptor(**(perceptorVectorIterator));
                perceptors.push_back(forceResistancePerceptor);
                //forceResistancePerceptors.insert(
                //	std::pair<std::string, ForceResistancePerceptor>(
                //		forceResistancePerceptor.getName(),
                //	forceResistancePerceptor));
            } else if (compareString == "ACC") {
                AccelerometerPerceptor *accelerometerPerceptor =
                        new AccelerometerPerceptor(**(perceptorVectorIterator));
                perceptors.push_back(accelerometerPerceptor);
                //			accelerometerPerceptors.insert(
                //				std::pair<std::string, AccelerometerPerceptor>(
                //					accelerometerPerceptor.getName(),
                //				accelerometerPerceptor));
            } else if (compareString == "See") {
                //	visionPerceptor.update(**(perceptorVectorIterator));
                VisionPerceptor *visionPerceptor = new VisionPerceptor(**perceptorVectorIterator);
                perceptors.push_back(visionPerceptor);
                //	perceptors.push_back(&visionPerceptor);
                //this->visionPerceptor = visionPerceptor;
            } else if (compareString == "GS") {
                GameStatePerceptor *gameStatePerceptorPtr = new GameStatePerceptor(
                        **(perceptorVectorIterator));
                perceptors.push_back(gameStatePerceptorPtr);
                //gameState = gameStatePerceptor;
            } else if (compareString == "AgentState") {
                AgentStatePerceptor *agentStatePerceptor = new AgentStatePerceptor(
                        **(perceptorVectorIterator));
                perceptors.push_back(agentStatePerceptor);
                //agentState = agentStatePerceptor;
            } else if (compareString == "hear") {
                HearPerceptor *hearPerceptor = new HearPerceptor(
                        **(perceptorVectorIterator));
                perceptors.push_back(hearPerceptor);
                //this->hearPerceptor = hearPerceptor;
            } else if (compareString == "time") {
                GlobalTimePerceptor *globalTimePerceptor = new GlobalTimePerceptor(
                        **(perceptorVectorIterator));
                perceptors.push_back(globalTimePerceptor);
                //this->globalTimePerceptor = globalTimePerceptor;
            }
        }

    }

    /*
     * Getter method: returns the vector of perceptors
     */
    std::vector<Perceptor *> &PerceptorFactory::getPerceptors() {
        return perceptors;
    }

} /* namespace perception */
