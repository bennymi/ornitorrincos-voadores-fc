/*
 * Control.cpp
 *
 *  Created on: Aug 7, 2015
 *      Author: mmaximo
 */

#include "representations/RobotType.h"
#include "control/ControlStateMachine.h"
#include "control/Control.h"
#include "control/Balance.h"

namespace control {

    Control::Control() {

    }

    Control::~Control() {
    }

//0.2,0.30,0.05,0.2,0.06,0.0
    ControlImpl::ControlImpl(int robotType) : ControlImpl(
            static_cast<representations::RobotType::ROBOT_TYPE>(robotType)) {

    }

    ControlImpl::ControlImpl(representations::RobotType::ROBOT_TYPE robotType) : stateMachine(robotType) {
        controller = new JointsController(robotType, 2.0 * 180.0 / (M_PI * 7.0));
        stateMachine.initiate();
    }

    ControlImpl::~ControlImpl() {
        delete controller;
    }

    representations::NaoJoints &ControlImpl::getJointsCommands() {
        return controller->getCommands();
    }


    void ControlImpl::handleLookRequest(std::shared_ptr<LookRequest> lookRequest) {
        if (lookRequest == nullptr)
            return;

        jointsPositions.neckYaw = lookRequest->getPan();
        jointsPositions.neckPitch = lookRequest->getTilt();

    }

    void ControlImpl::handleMovementRequest(std::shared_ptr<MovementRequest> movementRequest,
                                            perception::Perception &perception) {
        if (movementRequest == nullptr)
            return;
        double globalTime = perception.getAgentPerception().getGlobalTime();
        std::shared_ptr<WalkRequest> walkRequest;
        std::shared_ptr<KeyframeRequest> keyframeRequest;
        stateMachine.setMovementRequest(movementRequest);
        stateMachine.process_event(SetMovementEvent(movementRequest));
        stateMachine.control(perception);
        jointsPositions = stateMachine.jointsPositions;
        jointsPositions *= NaoJoints::getNaoDirectionsFixing();

    }

    void ControlImpl::control(perception::Perception &perception,
                              modeling::Modeling &modeling,
                              decision_making::DecisionMaking &decisionMaking) {
        jointsPositions.clear();
        handleMovementRequest(decisionMaking.getMovementRequest(), perception);
        Balance balance;
//        if (decisionMaking.getMovementRequest() != nullptr &&
//                decisionMaking.getMovementRequest()->getMovementType() == MovementRequestType::WALK_REQUEST)
//            balance.balance(modeling.getAgentModel(), jointsPositions);
        handleLookRequest(decisionMaking.getLookRequest());
        controller->update(jointsPositions,
                           perception.getAgentPerception().getNaoJoints());

    }

    itandroids_lib::math::Pose2D ControlImpl::getWalkOdometry() {
        //@todo fix me using real odometry (isn't this the real odometry with the fix from the state machine?)
        return stateMachine.walk->getOdometry();
    }

    void ControlImpl::updateKeyframeMotionPlayer(std::string folderPath) {
        stateMachine.keyframePlayer.update(folderPath);

    }

    keyframe::KeyframePlayer &ControlImpl::getKeyframePlayer() {
        return stateMachine.keyframePlayer;
    }

    void ControlImpl::setWalkingParameters(const control::walking::OmnidirectionalWalkZMPParams &params) {
        // set new walk parameters in state machine
        stateMachine.walk->setWalkParams(params);
    }


} /* namespace control */
