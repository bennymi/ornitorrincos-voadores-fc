/*
 * VisibleLine.h
 *
 *  Created on: Sep 1, 2015
 *      Author: itandroids
 */

#ifndef SOURCE_PERCEPTION_VISIBLELINE_H_
#define SOURCE_PERCEPTION_VISIBLELINE_H_

#include "VisibleObject.h"
#include "boost/algorithm/string.hpp"

namespace perception {

    class VisibleLine : public VisibleObject {
    public:
        VisibleLine();

        VisibleLine(utils::parser::ParserTreeNode &node);

        virtual ~VisibleLine();

        Vector3<double> getPosition2();

        /**
         * 
         */
        double getDistance2();

        double getHorizontalAngle2();

        double getHorizontalAngleDeg2();

        double getLatitudeAngle2();

        double getLatitudeAngleDeg2();

        virtual void transformPosition(Pose3D &transform) override;

    protected:

    private:
        Vector3<double> position2;
    };

} /* namespace perception */

#endif /* SOURCE_PERCEPTION_VISIBLELINE_H_ */
