//
// Created by dicksiano on 8/9/16.
//

#include "OwnKickOffSetPlay.h"
#include "../BehaviorFactory.h"

namespace decision_making {
    namespace behavior {

        OwnKickOffSetPlay::OwnKickOffSetPlay() : SetPlay(nullptr) {
        }

        OwnKickOffSetPlay::OwnKickOffSetPlay(BehaviorFactory *behaviorFactory) : SetPlay(behaviorFactory) {
        }

        OwnKickOffSetPlay::~OwnKickOffSetPlay() {
        }

        void OwnKickOffSetPlay::behave(modeling::Modeling &modeling) {
            /// IMPLEMENTE
        } /* function behave */

    } /* namespace behavior */
} /* namespace decision_making */