/*
 * InitEffector.cpp
 *
 *  Created on: Aug 8, 2015
 *      Author: mmaximo
 */

#include "action/InitEffector.h"

#include <sstream>

namespace action {

    InitEffector::InitEffector(int playerNumber, const std::string &teamName) :
            playerNumber(playerNumber), teamName(teamName) {
    }

    InitEffector::~InitEffector() {
    }

    std::string InitEffector::toString() {

        // Gets the the player's uniform number and the player's team name as a string
        std::stringstream stream;
        stream << "(init (unum ";
        stream << playerNumber;
        stream << ")(teamname ";
        stream << teamName;
        stream << "))";
        return stream.str();
    }

} /* namespace action */
