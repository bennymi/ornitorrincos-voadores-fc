/*
 * BehaviorFactory.cpp
 *
 *  Created on: Oct 31, 2015
 *      Author: itandroids
 */

#include "BehaviorFactory.h"

namespace decision_making {
    namespace behavior {

        BehaviorFactory::BehaviorFactory() : navigateToPosition(this), followLine(this), basicBehavior(this),
                                             conductBall(this), navigateToStaticPosition(this), focusBall(this),
                                             say(this), potentialFieldNavigator(this), goalieBehavior(this),
                                             activeVision(this), keep(this), circleBall(this), kickBehavior(this),
                                             navigateToBall(this), dribbleBall(this),
                                             beamBehavior(this), setPlay(this),
                                             beforeKickOffSetPlay(this), gameOverSetPlay(this),
                                             opponentCornerKickSetPlay(this), opponentFreeKickSetPlay(this),
                                             opponentGoalKickSetPlay(this), opponentGoalSetPlay(this),
                                             opponentKickInSetPlay(this), opponentKickOffSetPlay(this),
                                             opponentOffsideSetPlay(this), ownCornerKickSetPlay(this),
                                             ownFreeKickSetPlay(this), ownGoalKickSetPlay(this), ownGoalSetPlay(this),
                                             ownKickInSetPlay(this), ownKickOffSetPlay(this), ownOffsideSetPlay(this) {

        }

        BehaviorFactory::~BehaviorFactory() {
        }

        BasicBehavior &BehaviorFactory::getBasicBehavior() {
            return basicBehavior;
        }

        NavigateToPosition &BehaviorFactory::getNavigatePosition() {
            return navigateToPosition;
        }

        FollowLine &BehaviorFactory::getFollowLine() {
            return followLine;
        }

        ConductBall &BehaviorFactory::getConductBall() {
            return conductBall;
        }

        NavigateToStaticPosition &BehaviorFactory::getNavigateToStaticPosition() {
            return navigateToStaticPosition;
        }

        FocusBall &BehaviorFactory::getFocusBall() {
            return focusBall;
        }

        Say &BehaviorFactory::getSay() {
            return say;
        }

        PotentialFieldNavigation &BehaviorFactory::getPotentialFieldNavigationBehavior() {
            return potentialFieldNavigator;
        }

        GoalieBehavior &BehaviorFactory::getGoalieBehavior() {
            return goalieBehavior;
        }

        ActiveVision &BehaviorFactory::getActiveVision() {
            return activeVision;
        }

        Keep &BehaviorFactory::getKeep() {
            return keep;
        }

        CircleBall &BehaviorFactory::getCircleBall() {
            return circleBall;
        }

        KickBehavior &BehaviorFactory::getKickBehavior() {
            return kickBehavior;
        }


        NavigateToBall &BehaviorFactory::getNavigateToBall() {
            return navigateToBall;
        }

        DribbleBall &BehaviorFactory::getDribbleBall() {
            return dribbleBall;
        }

        BeamBehavior &BehaviorFactory::getBeamBehavior() {
            return beamBehavior;
        }

        SetPlay &BehaviorFactory::getSetPlay() {
            return setPlay;
        }

        BeforeKickOffSetPlay &BehaviorFactory::getBeforeKickOffSetPlay() {
            return beforeKickOffSetPlay;
        }

        GameOverSetPlay &BehaviorFactory::getGameOverSetPlay() {
            return gameOverSetPlay;
        }

        OpponentCornerKickSetPlay &BehaviorFactory::getOpponentCornerKickSetPlay() {
            return opponentCornerKickSetPlay;
        }

        OpponentFreeKickSetPlay &BehaviorFactory::getOpponentFreeKickSetPlay() {
            return opponentFreeKickSetPlay;
        }

        OpponentGoalKickSetPlay &BehaviorFactory::getOpponentGoalKickSetPlay() {
            return opponentGoalKickSetPlay;
        }

        OpponentGoalSetPlay &BehaviorFactory::getOpponentGoalSetPlay() {
            return opponentGoalSetPlay;
        }

        OpponentKickInSetPlay &BehaviorFactory::getOpponentKickInSetPlay() {
            return opponentKickInSetPlay;
        }

        OpponentKickOffSetPlay &BehaviorFactory::getOpponentKickOffSetPlay() {
            return opponentKickOffSetPlay;
        }

        OpponentOffsideSetPlay &BehaviorFactory::getOpponentOffsideSetPlay() {
            return opponentOffsideSetPlay;
        }

        OwnCornerKickSetPlay &BehaviorFactory::getOwnCornerKickSetPlay() {
            return ownCornerKickSetPlay;
        }

        OwnFreeKickSetPlay &BehaviorFactory::getOwnFreeKickSetPlay() {
            return ownFreeKickSetPlay;
        }

        OwnGoalKickSetPlay &BehaviorFactory::getOwnGoalKickSetPlay() {
            return ownGoalKickSetPlay;
        }

        OwnGoalSetPlay &BehaviorFactory::getOwnGoalSetPlay() {
            return ownGoalSetPlay;
        }

        OwnKickInSetPlay &BehaviorFactory::getOwnKickInSetPlay() {
            return ownKickInSetPlay;
        }

        OwnKickOffSetPlay &BehaviorFactory::getOwnKickOffSetPlay() {
            return ownKickOffSetPlay;
        }

        OwnOffsideSetPlay &BehaviorFactory::getOwnOffsideSetPlay() {
            return ownOffsideSetPlay;
        }

    } /* namespace behavior */
} /* namespace decision_making */
