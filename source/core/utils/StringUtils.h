/*
 * StringUtils.h
 *
 *  Created on: Aug 9, 2015
 *      Author: mmaximo
 */

#ifndef SOURCE_UTILS_STRINGUTILS_H_
#define SOURCE_UTILS_STRINGUTILS_H_

#include "math/Vector3.h"

namespace utils {

    using namespace itandroids_lib::math;

    class StringUtils {
        itandroids_lib::math::Vector3<float> f();
    };

} /* namespace utils */

#endif /* SOURCE_UTILS_STRINGUTILS_H_ */
