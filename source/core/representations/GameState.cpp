/*
 * GameState.cpp
 *
 *  Created on: Aug 7, 2015
 *      Author: mmaximo
 */

#include "representations/GameState.h"

namespace representations {

    GameState::GameState() {
        gameTime = 0;
        scoreLeft = 0;
        scoreRight = 0;
        playMode = representations::RawPlayMode::BEFORE_KICK_OFF;
    }

    GameState::GameState(double gameTime, int scoreLeft, int scoreRight,
                         std::string playMode) {

        this->gameTime = gameTime;
        this->scoreLeft = scoreLeft;
        this->scoreRight = scoreRight;

        /* Trimming the node string*/
        boost::algorithm::trim(playMode);

        if (playMode.compare("BeforeKickOff") == 0)
            this->playMode = representations::RawPlayMode::BEFORE_KICK_OFF;

        else if (playMode.compare("KickOff_Left") == 0)
            this->playMode = representations::RawPlayMode::KICK_OFF_LEFT;

        else if (playMode.compare("KickOff_Right") == 0)
            this->playMode = representations::RawPlayMode::KICK_OFF_RIGHT;

        else if (playMode.compare("PlayOn") == 0)
            this->playMode = representations::RawPlayMode::PLAY_ON;

        else if (playMode.compare("KickIn_Left") == 0)
            this->playMode = representations::RawPlayMode::KICK_IN_LEFT;

        else if (playMode.compare("KickIn_Right") == 0)
            this->playMode = representations::RawPlayMode::KICK_IN_RIGHT;

        else if (playMode.compare("corner_kick_left") == 0)
            this->playMode = representations::RawPlayMode::CORNER_KICK_LEFT;

        else if (playMode.compare("corner_kick_right") == 0)
            this->playMode = representations::RawPlayMode::CORNER_KICK_RIGHT;

        else if (playMode.compare("goal_kick_left") == 0)
            this->playMode = representations::RawPlayMode::GOAL_KICK_LEFT;

        else if (playMode.compare("goal_kick_right") == 0)
            this->playMode = representations::RawPlayMode::GOAL_KICK_RIGHT;

        else if (playMode.compare("offside_left") == 0)
            this->playMode = representations::RawPlayMode::OFFSIDE_LEFT;

        else if (playMode.compare("offside_right") == 0)
            this->playMode = representations::RawPlayMode::OFFSIDE_RIGHT;

        else if (playMode.compare("GameOver") == 0)
            this->playMode = representations::RawPlayMode::GAME_OVER;

        else if (playMode.compare("Goal_Left") == 0)
            this->playMode = representations::RawPlayMode::GOAL_LEFT;

        else if (playMode.compare("Goal_Right") == 0)
            this->playMode = representations::RawPlayMode::GOAL_RIGHT;

        else if (playMode.compare("free_kick_left") == 0)
            this->playMode = representations::RawPlayMode::FREE_KICK_LEFT;

        else if (playMode.compare("free_kick_right") == 0)
            this->playMode = representations::RawPlayMode::FREE_KICK_RIGHT;
        else
            this->playMode = representations::RawPlayMode::NONE;

    }

    GameState::~GameState() {
    }

    double GameState::getGameTime() {
        return gameTime;
    }

    int GameState::getScoreLeft() {
        return scoreLeft;
    }

    int GameState::getScoreRight() {
        return scoreRight;
    }

    representations::RawPlayMode::RAW_PLAY_MODE GameState::getPlayMode() {
        return playMode;
    }

} /* namespace representations */
