//
// Created by dicksiano on 8/14/16.
//

#include "OwnKickInSetPlay.h"
#include "../BehaviorFactory.h"

namespace decision_making {
    namespace behavior {

        OwnKickInSetPlay::OwnKickInSetPlay() : SetPlay(nullptr) {
        }

        OwnKickInSetPlay::OwnKickInSetPlay(BehaviorFactory *behaviorFactory) : SetPlay(behaviorFactory) {
        }

        OwnKickInSetPlay::~OwnKickInSetPlay() {
        }

        void OwnKickInSetPlay::behave(modeling::Modeling &modeling) {
            /// IMPLEMENTE
        } /* function behave */

    } /* namespace behavior */
} /* namespace decision_making */