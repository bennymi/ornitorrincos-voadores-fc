//
// Created by dicksiano on 08/05/16.
//

#include "Say.h"

namespace decision_making {
    namespace behavior {

        Say::Say() : Behavior(nullptr), sayMessage(nullptr), lastSayTime(0.0) {
        }

        Say::Say(BehaviorFactory *behaviorFactory) : Behavior(behaviorFactory), sayMessage(nullptr), lastSayTime(0.0) {
        }

        Say::Say(int cycles, std::vector<int> roleAssignment, double ballX, double ballY, double agentX, double agentY,
                 bool isFallen) {
            std::vector<int> bits;
            this->ballY = ballY;
            this->ballX = ballX;
            this->agentX = agentX;
            this->agentY = agentY;
            this->agentFallen = isFallen;
            this->roleAssignment = roleAssignment;
            this->cycles = cycles;
            dataToBits(bits);
            std::string message;
            bitsToString(bits, message);
            stringMessage = message;
            sayMessage = std::make_shared<std::string>(stringMessage);
        }

        Say::~Say() {
        }

        void Say::behave(modeling::Modeling &modeling) {

            sayMessage = nullptr;

            currentServerTime = modeling.getAgentModel().getAgentPerception().getGlobalTime();
            ballLastSeenServerTime = modeling.getWorldModel().getBall().getLastSeenTime();

            cycles = int((currentServerTime * 50) + 0.1);
            numberAgent = modeling.getAgentModel().getAgentNumber();

            agentX = modeling.getWorldModel().getSelf().getPosition().translation.x;
            agentY = modeling.getWorldModel().getSelf().getPosition().translation.y;

            ballX = modeling.getWorldModel().getBall().getPosition().translation.x;
            ballY = modeling.getWorldModel().getBall().getPosition().translation.y;

            agentFallen = modeling.getAgentModel().hasFallen();

            roleAssignment = modeling.getWorldModel().getMyRoleAssignmentVector();

            std::string message;

            /*
             * Check if the time of the agent say
             */

            if (numberAgent == 1 && (numberAgent - 1) * 2 == (this->cycles) %
                                                             (modeling.getWorldModel().NUM_PLAYERS_PER_TEAM *
                                                              2)) //goalie has different communication system
            {
                std::vector<int> bits;
                if (modeling.getWorldModel().getOpponents().size() == modeling::WorldModel::NUM_PLAYERS_PER_TEAM) {
                    dataToBitsGoalie(bits, modeling);
                } else dataToBits(bits);

                bitsToString(bits, message);
                modeling.getWorldModel().newOpponentToTalk();
                sayMessage = std::make_shared<std::string>(message);
            } else if ((numberAgent - 1) * 2 == (this->cycles) % (modeling.getWorldModel().NUM_PLAYERS_PER_TEAM * 2)) {
                std::vector<int> bits;
                dataToBits(bits);
                bitsToString(bits, message);
                sayMessage = std::make_shared<std::string>(message);
            }
        }

        std::shared_ptr<std::string> Say::getSayMessage() {
            return (sayMessage);
        }

        std::string Say::getStringMessage() {
            return stringMessage;
        }

        void Say::dataToBitsGoalie(std::vector<int> &bits, modeling::Modeling &modeling) {
            std::vector<int> bitsNumberAgent = intToBits(this->numberAgent, 4);
            bits.insert(bits.end(), bitsNumberAgent.begin(), bitsNumberAgent.end());
            //integer part agent x
            std::vector<int> bitsAgentX = intToSignedBits(int(agentX), 5);
            bits.insert(bits.end(), bitsAgentX.begin(), bitsAgentX.end());
            //decimal part agent x
            int decimalPart;
            decimalPart = int(10 * (fabs(agentX) - int(fabs(agentX)))); //take the first number in decimal part
            std::vector<int> bitsDecAgentX = intToBits(decimalPart, 4);
            bits.insert(bits.end(), bitsDecAgentX.begin(), bitsDecAgentX.end());

            //integer part agent y
            std::vector<int> bitsAgentY = intToSignedBits(int(agentY), 5);
            bits.insert(bits.end(), bitsAgentY.begin(), bitsAgentY.end());

            //decimal part agent y
            decimalPart = int(10 * (fabs(agentY) - int(fabs(agentY))));
            std::vector<int> bitsDecAgentY = intToBits(decimalPart, 4);
            bits.insert(bits.end(), bitsDecAgentY.begin(), bitsDecAgentY.end());

            std::vector<int> bitIsFallen;
            if (agentFallen == true)
                bitIsFallen.push_back(1);
            else bitIsFallen.push_back(0);
            bits.insert(bits.end(), bitIsFallen.begin(), bitIsFallen.end());

            //opponents
            int opponentToTalk = modeling.getWorldModel().getOpponentToTalk();
            for (int i = 0; i < 4; i++) {
                //opponent Number
                std::vector<int> bitsUniformNumberOpp = intToBits(opponentToTalk, 4);
                bits.insert(bits.end(), bitsUniformNumberOpp.begin(), bitsUniformNumberOpp.end());
                //opponent X integer
                double opponentPosX = modeling.getWorldModel().getOpponent(opponentToTalk).getPosition().translation.x;
                std::vector<int> bitsOppX = intToSignedBits(int(opponentPosX), 5);
                bits.insert(bits.end(), bitsOppX.begin(), bitsOppX.end());
                // opponent X decimal
                int decimalPart = int(10 * (fabs(opponentPosX) - int(fabs(opponentPosX))));
                std::vector<int> bitsOppXDecimal = intToBits(decimalPart, 4);
                bits.insert(bits.end(), bitsOppXDecimal.begin(), bitsOppXDecimal.end());

                //opponent Y integer
                double opponentPosY = modeling.getWorldModel().getOpponent(opponentToTalk).getPosition().translation.y;
                std::vector<int> bitsOppY = intToSignedBits(int(opponentPosY), 5);
                bits.insert(bits.end(), bitsOppY.begin(), bitsOppY.end());
                //opponent y decimal
                decimalPart = int(10 * (fabs(opponentPosY) - int(fabs(opponentPosY))));
                std::vector<int> bitsOppYDecimal = intToBits(decimalPart, 4);
                bits.insert(bits.end(), bitsOppYDecimal.begin(), bitsOppYDecimal.end());

                opponentToTalk = (opponentToTalk + 1); //next opponent
                if (opponentToTalk > modeling::WorldModel::NUM_PLAYERS_PER_TEAM)
                    opponentToTalk -= modeling::WorldModel::NUM_PLAYERS_PER_TEAM;
            }
        }

        void Say::dataToBits(std::vector<int> &bits) {
            std::vector<int> bitsNumberAgent = intToBits(this->numberAgent, 4);
            bits.insert(bits.end(), bitsNumberAgent.begin(), bitsNumberAgent.end());
            //integer part agent x
            std::vector<int> bitsAgentX = intToSignedBits(int(agentX), 5);
            bits.insert(bits.end(), bitsAgentX.begin(), bitsAgentX.end());
            //decimal part agent x
            int decimalPart;
            decimalPart = int(10 * (fabs(agentX) - int(fabs(agentX)))); //take the first number in decimal part
            std::vector<int> bitsDecAgentX = intToBits(decimalPart, 4);
            bits.insert(bits.end(), bitsDecAgentX.begin(), bitsDecAgentX.end());

            //integer part agent y
            std::vector<int> bitsAgentY = intToSignedBits(int(agentY), 5);
            bits.insert(bits.end(), bitsAgentY.begin(), bitsAgentY.end());

            //decimal part agent y
            decimalPart = int(10 * (fabs(agentY) - int(fabs(agentY))));
            std::vector<int> bitsDecAgentY = intToBits(decimalPart, 4);
            bits.insert(bits.end(), bitsDecAgentY.begin(), bitsDecAgentY.end());

            std::vector<int> bitIsFallen;
            if (agentFallen == true)
                bitIsFallen.push_back(1);
            else bitIsFallen.push_back(0);
            bits.insert(bits.end(), bitIsFallen.begin(), bitIsFallen.end());

            //integer part ball x
            std::vector<int> bitsBallX = intToSignedBits(int(ballX), 5);
            bits.insert(bits.end(), bitsBallX.begin(), bitsBallX.end());

            //decimal part ball x
            decimalPart = int(10 * (fabs(ballX) - int(fabs(ballX))));
            std::vector<int> bitsDecBallX = intToBits(decimalPart, 4);
            bits.insert(bits.end(), bitsDecBallX.begin(), bitsDecBallX.end());

            //integer part ball y
            std::vector<int> bitsBallY = intToSignedBits(int(ballY), 5);
            bits.insert(bits.end(), bitsBallY.begin(), bitsBallY.end());

            //decimal part ball y
            decimalPart = int(10 * (fabs(ballY) - int(fabs(ballY))));
            std::vector<int> bitsDecBallY = intToBits(decimalPart, 4);
            bits.insert(bits.end(), bitsDecBallY.begin(), bitsDecBallY.end());

            for (int i = 0; i < roleAssignment.size(); i++) {
                int roleAssign = roleAssignment[i];
                std::vector<int> role = intToBits(roleAssign, 5);
                bits.insert(bits.end(), role.begin(), role.end());
            }


        }

        std::vector<int> Say::intToSignedBits(const long long int &n, const int &numBits) {
            std::vector<int> bits;

            bits.resize(numBits);
            long long m;
            if (n < 0) {
                bits[0] = 1;
                m = -n;

            } else {
                bits[0] = 0;
                m = n;
            }

            for (int i = numBits - 1; i > 0; i--) {
                bits[i] = m % 2;
                m /= 2;
            }


            return bits;

        }

        /*std::vector<int> Say::intToBits(const unsigned long long int &n, const int &numBits) {
            std::vector<int> bits;

            unsigned long long m = n; //Copy.

            bits.resize(numBits);
            for(int i = numBits - 1; i >= 0; i--) {
                bits[i] = m % 2;
                m /= 2;
            }

            return bits;
        }*/

        std::vector<int> Say::intToBits(const int &n, const int &numBits) {
            std::vector<int> bits;

            if (n < 0) {
                bits.clear();
                return bits;
            }

            int m = n; //Copy.

            bits.resize(numBits);
            for (int i = numBits - 1; i >= 0; i--) {
                bits[i] = m % 2;
                m /= 2;
            }

            return bits;
        }

        void Say::bitsToString(const std::vector<int> &bits, std::string &message) {
            const std::string commAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
            message = "";

            std::vector<int> index;
            index.resize((bits.size() + 5) / 6);
            size_t ctr = 0;
            for (size_t i = 0; i < index.size(); i++) {

                index[i] = 0;
                for (int j = 0; j < 6; j++) {

                    index[i] *= 2;

                    if (ctr < bits.size()) {
                        index[i] += bits[ctr];
                        ctr++;
                    }

                }
            }

            for (size_t i = 0; i < index.size(); i++) {
                message += commAlphabet.at(index[i]);
            }
        }


    } /* namespace behavior */
} /* namespace decision_making */