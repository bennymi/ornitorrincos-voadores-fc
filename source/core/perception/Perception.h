/*
 * Perception.h
 *
 *  Created on: Aug 7, 2015
 *      Author: mmaximo
 */

#ifndef SOURCE_PERCEPTION_PERCEPTION_H_
#define SOURCE_PERCEPTION_PERCEPTION_H_

#include "communication/Communication.h"
#include "perception/AgentPerception.h"
#include "utils/parser/Parser.h"
#include "utils/parser/KeyValuePair.h"
#include <list>

namespace perception {

    class Perception {
    public:
        /**
         * Default Constructor
         */
        Perception();

        /**
         * Destructor
         */
        virtual ~Perception();

        /**
         *
         * @return a reference to agentPerception
         */
        perception::AgentPerception &getAgentPerception();

        /**
         * Function for the trainer loop, that updates the agentPerception
         * @param communication
         */
        void perceive(communication::Communication &communication);

    private:
        utils::parser::Parser parser;
        perception::AgentPerception agentPerception;
        perception::PerceptorFactory perceptorFactory;
    };

} /* namespace perception */

#endif /* SOURCE_PERCEPTION_PERCEPTION_H_ */
