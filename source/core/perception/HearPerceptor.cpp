/*
 * HearPerceptor.cpp
 *
 *  Created on: Aug 8, 2015
 *      Author: mmaximo
 */

#include "perception/HearPerceptor.h"

namespace perception {
    /*
     * This constructor initializes variables without any data
     */
    HearPerceptor::HearPerceptor() :
            Perceptor() {
        time = 0.0f;
        fromSelf = true;
        direction = 0.0f;
        message = std::string("");
    }

    /*
     * This constructor initializes variables with parser tree node data
     */

    HearPerceptor::HearPerceptor(utils::parser::ParserTreeNode &node) :
            Perceptor(node) {

        std::string value = utils::parser::KeyValuePair(node.content, ' ').value;

        std::vector<std::string> stringVector;
        boost::split(stringVector, value, boost::is_any_of(" "));

        //Team is the first information in the node
        team = stringVector[0];

        //Time is the second information in the node
        time = boost::lexical_cast<double>(stringVector[1]);

        //The third string tells if the information of hear perceptor is from self or from another
        //agent. In this second case, the perceptor information gives the direction of the other agent
        if (boost::iequals(stringVector[2], "self")) {
            fromSelf = true;
            direction = 0.0;
        } else {
            fromSelf = false;
            direction = MathUtils::degreesToRadians(
                    boost::lexical_cast<double>(stringVector[2]));
        }

        //The fourth string from the content of the node is the message itself
        message = stringVector[3];

        //The variables is organized in hear data representation
        hearData = representations::HearData(team, time, fromSelf, direction, message);

    }

    HearPerceptor::~HearPerceptor() {
    }

    /*
     * Getter methods: return data of hear perceptor (time, isFromSelf, message, direction, hearData)
     */

    std::string HearPerceptor::getTeam() {
        return team;
    }

    double HearPerceptor::getTime() {
        return time;
    }

    bool HearPerceptor::isFromSelf() {
        return fromSelf;
    }

    std::string HearPerceptor::getMessage() {
        return message;
    }

    double HearPerceptor::getDirection() {
        return direction;
    }

    representations::HearData HearPerceptor::getHearData() {
        return hearData;
    }

} /* namespace perception */
