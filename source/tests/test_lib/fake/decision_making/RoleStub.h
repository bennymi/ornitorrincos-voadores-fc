//
// Created by muzio on 30/05/16.
//

#ifndef ITANDROIDS_SOCCER3D_CPP_ROLESTUB_H
#define ITANDROIDS_SOCCER3D_CPP_ROLESTUB_H

#include "decision_making/role/Role.h"

namespace decision_making {
    namespace role {

        class RoleStub : public Role {
        public:
            RoleStub();

            ~RoleStub();

            void execute(modeling::Modeling &modeling) override;

            virtual std::shared_ptr<std::string> getSayMessage() { return sayMessage; }

            /**
             * method to get the movements for the head and neck joints
             */
            virtual std::shared_ptr<control::LookRequest> getLookRequest() { return lookRequest; }

            /**
             * method for getting the robots body joint positions
             */
            virtual std::shared_ptr<control::MovementRequest> getMovementRequest() { return movementRequest; }

            /**
             * method for returning
             */
            virtual std::shared_ptr<itandroids_lib::math::Pose2D> getBeamRequest() { return beamRequest; };


            std::shared_ptr<itandroids_lib::math::Pose2D> beamRequest;
            std::shared_ptr<control::MovementRequest> movementRequest;
            std::shared_ptr<control::LookRequest> lookRequest;
            std::shared_ptr<std::string> sayMessage;
        private:
            virtual void behaveMovement(modeling::Modeling &modeling) override {};
        };

    } /* namespace role */
} /* namespace decision_making */

#endif //ITANDROIDS_SOCCER3D_CPP_ROLESTUB_H
