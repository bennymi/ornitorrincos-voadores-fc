//
// Created by dicksiano on 8/14/16.
//

#include "OwnOffsideSetPlay.h"
#include "../BehaviorFactory.h"

namespace decision_making {
    namespace behavior {

        OwnOffsideSetPlay::OwnOffsideSetPlay() : SetPlay(nullptr) {
        }

        OwnOffsideSetPlay::OwnOffsideSetPlay(BehaviorFactory *behaviorFactory) : SetPlay(behaviorFactory) {
        }

        OwnOffsideSetPlay::~OwnOffsideSetPlay() {
        }

        void OwnOffsideSetPlay::behave(modeling::Modeling &modeling) {
            /// IMPLEMENTE
        } /* function behave */

    } /* namespace behavior */
} /* namespace decision_making */