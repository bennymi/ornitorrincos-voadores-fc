//
// Created by dicksiano on 27/05/16.
//

#include "RoleStrategy.h"
#include "role/AttackingRole.h"
#include "role/DefensiveRole.h"
#include "role/GoalieRole.h"

namespace decision_making {
    using modeling::WorldModel;

    RoleStrategyImpl::RoleStrategyImpl() {

        for (int i = 0; i < 11; ++i)
            roles[i] = nullptr;


    }

    RoleStrategyImpl::~RoleStrategyImpl() {
    }

    role::Role *RoleStrategyImpl::getRole(modeling::Modeling &modeling) {
        const int uniformNumber = modeling.getAgentModel().getAgentNumber();
        if (roles[uniformNumber - 1] == nullptr) {
            if (uniformNumber == 1)
                roles[uniformNumber - 1] = new role::GoalieRole();
            else {
                std::vector<int> syncRoleVect = modeling.getWorldModel().getSynchronizedRoleAssignmentVector();
                if (syncRoleVect[uniformNumber - 1] == 9)  //onBall Player; check marking system for further understand
                    roles[uniformNumber - 1] = new role::AttackingRole();
                else
                    roles[uniformNumber - 1] = new role::DefensiveRole();

            }
        }
        return roles[uniformNumber - 1];
    }

} /* namespace decision_making */