//
// Created by mmaximo on 05/06/16.
//

#include "base/BaseAgentTest.h"

class CircleBall_AgentTest : public BaseAgentTest {
private:


public:
    virtual void testLoop(int nSteps) { BaseAgentTest::testLoop(nSteps); }

    virtual void setup() {
        wiz.setAgentPositionAndDirection(1, representations::PlaySide::LEFT,
                                         Vector3<double>(-2, 0, 0.3), 0.0);
    }

    virtual void runStep() {
        std::string buff("animated");
        std::string name("animated");
        roboviz.drawLine(modeling.getWorldModel().getBall().getPosition().translation.x,
                         modeling.getWorldModel().getBall().getPosition().translation.y,
                         0, modeling.getWorldModel().getBall().getPosition().translation.x,
                         modeling.getWorldModel().getBall().getPosition().translation.y, 1, 1,
                         255, 255, 255, &name);
        roboviz.drawLine(modeling.getWorldModel().getSelf().getPosition().translation.x,
                         modeling.getWorldModel().getSelf().getPosition().translation.y,
                         0, modeling.getWorldModel().getSelf().getPosition().translation.x,
                         modeling.getWorldModel().getSelf().getPosition().translation.y, 1, 2,
                         255, 0, 0, &name);
        roboviz.swapBuffers(&buff);
        Vector3<double> goalPosition3D = modeling.getWorldModel().getTheirGoalPosition();
        Vector2<double> goalPosition = Vector2<double>(goalPosition3D.x, goalPosition3D.y);
        Vector2<double> ballPosition = modeling.getWorldModel().getBall().getPosition().translation;
        Vector2<double> ballToGoal = goalPosition - ballPosition;
        behaviorFactory.getCircleBall().setDribbleDirection(ballToGoal.angle());
        behaviorFactory.getCircleBall().behave(modeling);
        decisionMaking.movementRequest = behaviorFactory.getCircleBall().getMovementRequest();
    }
};

int main() {
    CircleBall_AgentTest t;
    t.testLoop(50000);
}