/*
 * InverseKinematics.h
 *
 *  Created on: Oct 10, 2015
 *      Author: mmaximo
 */

#ifndef SOURCE_CONTROL_WALKING_INVERSEKINEMATICS_H_
#define SOURCE_CONTROL_WALKING_INVERSEKINEMATICS_H_

#include "representations/NaoJoints.h"
#include "control/walking/RobotPhysicalParameters.h"

namespace control {
    namespace walking {

        using namespace representations;

        class InverseKinematics {
        private:
            double upperLegLength;
            double lowerLegLength;
            double hipPitch0;
            double kneePitch0;
            double anklePitch0;

            RobotPhysicalParameters physicalParameters;

        public:
            InverseKinematics(RobotPhysicalParameters physicalParameters);

            virtual ~InverseKinematics();

            void setLeftLeg(double leftLeg[], NaoJoints &jointsPositions);

            void setRightLeg(double rightLeg[], NaoJoints &jointsPositions);

            void getHipRotationMatrix(double z, double x, double y, double matrix[][3]);

            void computeJointsTargetsOneLeg(double x, double y, double z,
                                            double theta, double jointsTargets[]);

            void computeJointsTargets(double xLeft, double yLeft, double zLeft,
                                      double thetaLeft, double xRight, double yRight, double zRight,
                                      double thetaRight, NaoJoints &jointsTargets);
        };


    } /* namespace walking */
} /* namespace control */

#endif /* SOURCE_CONTROL_WALKING_INVERSEKINEMATICS_H_ */
