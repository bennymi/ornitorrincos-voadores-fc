/*
 * KeyValuePair.h
 *
 *  Created on: Aug 9, 2015
 *      Author: mmaximo
 */

#ifndef SOURCE_UTILS_PARSER_KEYVALUEPAIR_H_
#define SOURCE_UTILS_PARSER_KEYVALUEPAIR_H_

#include <string>

namespace utils {
    namespace parser {

        struct KeyValuePair {
            KeyValuePair(std::string string, char delimiter);

            virtual ~KeyValuePair();

            std::string key;
            std::string value;
        };

    } /* namespace parser */
} /* namespace utils */

#endif /* SOURCE_UTILS_PARSER_KEYVALUEPAIR_H_ */
