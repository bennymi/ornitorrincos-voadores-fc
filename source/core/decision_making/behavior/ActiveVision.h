//
// Created by francisco on 02/05/16.
//

#ifndef ITANDROIDS_SOCCER3D_CPP_ACTIVEVISION_H
#define ITANDROIDS_SOCCER3D_CPP_ACTIVEVISION_H

#include <core/control/LookRequest.h>
#include "decision_making/behavior/LookBehavior.h"
#include "decision_making/behavior/active_vision/ActiveVisionFileParser.h"
#include "utils/LogSystem.h"

namespace decision_making {
    namespace behavior {
        class ActiveVision : public LookBehavior {
        public:
            ActiveVision(BehaviorFactory *factory);

            virtual void behave(modeling::Modeling &modeling) override;

            double headLimitFunction(double pan, double tilt);

            double headMovementPenalization(double pan, double tilt, double currentHeadPan, double currentHeadTilt);

            double obstaclesPenalization(double pan, double tilt, modeling::Modeling &modeling,
                                         perception::VisiblePlayer &obstacle);

            double ballFunction(double pan, double tilt, modeling::Modeling &modeling);

            void setBallImportance(double alpha);

            double getCurrentHeadPan();

            double getCurrentHeadTilt();

        protected:
        private:
            static const double TILT_MAX_LIMIT;
            static const double TILT_MIN_LIMIT;
            static const std::string ACTIVE_VISION_FILE_PATH;
            static const double PAN_LIMIT;
            static const double DISPERSION;
            static const double SD;
            static const double K0;
            static const double SD0;
            double alpha;
            double currentHeadTilt;
            double currentHeadPan;
            active_vision::ActiveVisionFileParser parser;
            bool first;
        };
    }
}


#endif //ITANDROIDS_SOCCER3D_CPP_ACTIVEVISION_H
