//
// Created by muzio on 30/05/16.
//

#include "RoleStrategyStub.h"


namespace decision_making {

    RoleStrategyStub::RoleStrategyStub() {

    }

    RoleStrategyStub::~RoleStrategyStub() {

    }

    role::Role *RoleStrategyStub::getRole(modeling::Modeling &modeling) {
        return &role;
    }

} /* namespace decision_making */