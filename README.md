# ITAndroids Soccer 3D Team

### Instalação: 
* Inicialmente, é necessário instalar o git:
```sh
$ apt-get install git
```

* Faça o git clone do código. No canto superior esquerdo desta página, clique em **clone**. 

* Com o código em seu computador, acesse a pasta **itandroids-soccer3d/documentation/** e utilize o **GuiaBasico_ITAndroids3d.pdf** para realizar a instalação.

### Regras: 
* NÃO USE **git add .**, **git commit -a**. Se possível, adicione cada arquivo individualmente para evitar colocar modificações desnecessárias.
* Cuidado com as modificações nos **CmakeLists.txt**. Só faça alterações, caso você tenha certeza que não vá gerar problemas.
* Não coloque arquivos que não interessam para o time, e.g., arquivos .txt utilizados para debug. Isso evita que o repositório fique desorganizado.
* SEMPRE compile e teste o código antes de enviá-lo. SEMPRE! Lembre-se que ao colocar algo que não compila, você fará com que alguém perca um longo tempo tentando descobrir porque o código não compila.
* Novamente, SEMPRE compile o código antes de enviá-lo. SEMPRE! É aconselhável rodar todos os unit tests para verificar se as suas alterações não causaram algum problema.
* Evite enviar códigos com "std::cout" que você utilizou para debuggar algo. Isso é ruim, pois a próxima pessoa que atualizar o código terá que apagar esses couts.
* Por fim, **SEMPRE compile e teste o código antes de enviá-lo**.