//
// Created by dicksiano on 8/14/16.
//

#include "OpponentGoalKickSetPlay.h"
#include "../BehaviorFactory.h"

namespace decision_making {
    namespace behavior {

        OpponentGoalKickSetPlay::OpponentGoalKickSetPlay() : SetPlay(nullptr) {
        }

        OpponentGoalKickSetPlay::OpponentGoalKickSetPlay(BehaviorFactory *behaviorFactory) : SetPlay(behaviorFactory) {
        }

        OpponentGoalKickSetPlay::~OpponentGoalKickSetPlay() {
        }

        void OpponentGoalKickSetPlay::behave(modeling::Modeling &modeling) {
            /// IMPLEMENTE
        } /* function behave */

    } /* namespace behavior */
} /* namespace decision_making */