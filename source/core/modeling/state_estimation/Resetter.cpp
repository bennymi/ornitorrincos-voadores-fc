//
// Created by muzio on 17/05/16.
//

#include "Resetter.h"

#include "geometry/GeometryUtils.h"
#include "math/MathUtils.h"

namespace modeling {
    namespace state_estimation {

        using itandroids_lib::geometry::GeometryUtils;
        using itandroids_lib::math::MathUtils;
        using itandroids_lib::geometry::Point2D;

        Resetter::Resetter(const FieldDescription &fieldDescription, double distanceSigma, double bearingSigma) :
                fieldDescription(fieldDescription),
                uniformDistribution(MathUtils::getUniformRandomGenerator(0.0, 1.0)),
                distanceDistribution(MathUtils::getNormalRandomGenerator(0.0, distanceSigma)),
                bearingDistribution(MathUtils::getNormalRandomGenerator(0.0, bearingSigma)) {
        }

        void Resetter::sensorReset(Particle *particles, int numParticles,
                                   std::vector<LandmarkObservation> &landmarkObservations,
                                   std::vector<Line2D> &lineObservations,
                                   double resetProbability) {
            int numLandmarks = landmarkObservations.size();
            int numLines = lineObservations.size();

            //reset with 1 landmark
            if (numLandmarks == 1 && numLines == 2) {
                const LandmarkObservation &landmark = landmarkObservations[0];

                if (landmark.flagType == FlagType::INVALID)
                    return;

                //Guarantee that the lines are from correct landmarks
                if (GeometryUtils::getDistanceTo(lineObservations[0].start, lineObservations[1].start) > 3.0 &&
                    GeometryUtils::getDistanceTo(lineObservations[0].start, lineObservations[1].end) > 3.0 &&
                    GeometryUtils::getDistanceTo(lineObservations[0].end, lineObservations[1].start) > 3.0 &&
                    GeometryUtils::getDistanceTo(lineObservations[0].end, lineObservations[1].end) > 3.0)
                    return;

                for (int i = 0; i < numParticles; ++i) {
                    if (uniformDistribution() < resetProbability) {
                        computeParticleResetWithOneLandmark(particles[i], landmarkObservations[0],
                                                            lineObservations[0],
                                                            lineObservations[1]);
                    }
                }
            }

            //reset with 2 landmarks
            if (numLandmarks == 2) {
                int rndIndx;
                for (int i = 0; i < numParticles; ++i) {
                    if (uniformDistribution() < resetProbability) {
                        computeParticleResetWithTwoLandmarks(particles[i], landmarkObservations[0],
                                                             landmarkObservations[1]);
//                int s = numLandmarks;
//                for (int j = 0; j < 2; j++) {
//                    rndIndx =  rand() % s;
//                    std::swap(landmarkObservations[s - 1], landmarkObservations[rndIndx]);
//                    s--;
//                }
//                computeParticleResetWithTwoLandmarks(particles[i], landmarkObservations[numLandmarks - 1],
//                                                     landmarkObservations[numLandmarks - 2]);
                    }
                }
            }

            if (numLandmarks >= 3) {
                int rndIndx;
                for (int i = 0; i < numParticles; ++i) {
                    if (uniformDistribution() < resetProbability) {
                        int s = numLandmarks;
                        for (int j = 0; j < 3; j++) {
                            rndIndx = rand() % s;
                            std::swap(landmarkObservations[s - 1], landmarkObservations[rndIndx]);
                            s--;
                        }
                        computeParticleResetWithThreeLandmarks(particles[i], landmarkObservations[numLandmarks - 1],
                                                               landmarkObservations[numLandmarks - 2],
                                                               landmarkObservations[numLandmarks - 3]);
                    }
                }
            }
        }

        void Resetter::computeParticleResetWithOneLandmark(Particle &particle, LandmarkObservation observation,
                                                           Line2D observedLine1, Line2D observedLine2) {
            static Pose2D particlePose;

            observation.distance += distanceDistribution();
            observation.bearing += bearingDistribution();

            Line2D &lineA = observedLine1;
            Line2D &lineB = observedLine2;

            Point2D center1 = lineA.getCenter();
            Point2D center2 = lineB.getCenter();
            if (center1.y > center2.y) {
                std::swap(lineA, lineB);
            }

            switch (observation.flagType) {
                case representations::FlagType::F1L:
                    particlePose.translation.x = observation.knownPosition.x + lineB.getDistanceToPoint(Point2D(0, 0));
                    particlePose.translation.y = observation.knownPosition.y - lineA.getDistanceToPoint(Point2D(0, 0));
                    particlePose.rotation = M_PI - (atan2(lineB.getDistanceToPoint(Point2D(0, 0)),
                                                          lineA.getDistanceToPoint(Point2D(0, 0))) -
                                                    observation.bearing);
                    break;
                case representations::FlagType::F2L:
                    particlePose.translation.x = observation.knownPosition.x + lineA.getDistanceToPoint(Point2D(0, 0));
                    particlePose.translation.y = observation.knownPosition.y + lineB.getDistanceToPoint(Point2D(0, 0));
                    particlePose.rotation = M_PI + (atan2(lineB.getDistanceToPoint(Point2D(0, 0)),
                                                          lineA.getDistanceToPoint(Point2D(0, 0))) -
                                                    observation.bearing);
                    break;
                case representations::FlagType::F1R:
                    particlePose.translation.x = observation.knownPosition.x - lineA.getDistanceToPoint(Point2D(0, 0));
                    particlePose.translation.y = observation.knownPosition.y - lineB.getDistanceToPoint(Point2D(0, 0));
                    particlePose.rotation = atan2(lineB.getDistanceToPoint(Point2D(0, 0)),
                                                  lineA.getDistanceToPoint(Point2D(0, 0))) - observation.bearing;
                    break;
                case representations::FlagType::F2R:
                    particlePose.translation.x = observation.knownPosition.x - lineB.getDistanceToPoint(Point2D(0, 0));
                    particlePose.translation.y = observation.knownPosition.y + lineA.getDistanceToPoint(Point2D(0, 0));
                    particlePose.rotation = -(atan2(lineB.getDistanceToPoint(Point2D(0, 0)),
                                                    lineA.getDistanceToPoint(Point2D(0, 0))) - observation.bearing);
                    break;
            }


            particle.setPose(particlePose);
        }

        void Resetter::computeParticleResetWithTwoLandmarks(Particle &particle, LandmarkObservation observation1,
                                                            LandmarkObservation observation2) {
            static Pose2D particlePose;

            observation1.distance += distanceDistribution();
            observation1.bearing += bearingDistribution();
            observation2.distance += distanceDistribution();
            observation2.bearing += bearingDistribution();

            Vector2<double> intersection1;
            Vector2<double> intersection2;
            bool success = computeTwoCirclesIntersection(observation1.knownPosition, observation1.distance,
                                                         observation2.knownPosition, observation2.distance,
                                                         intersection1, intersection2);
            if (!success)
                return;

            if (LandmarkObservation::landmarksOnSameBorder(observation1, observation2)) {
                if (intersection1.squareAbs() < intersection2.squareAbs()) {
                    particlePose.translation = intersection1;
                } else {
                    particlePose.translation = intersection2;
                }
            } else {
                if (rand() % 2 == 0) {
                    particlePose.translation = intersection1;
                } else {
                    particlePose.translation = intersection2;
                }
            }

            double psi1 = atan2(observation1.knownPosition.y - particlePose.translation.y,
                                observation1.knownPosition.x - particlePose.translation.x) - observation1.bearing;
            double psi2 = atan2(observation2.knownPosition.y - particlePose.translation.y,
                                observation2.knownPosition.x - particlePose.translation.x) - observation2.bearing;

            particlePose.rotation = itandroids_lib::math::MathUtils::normalizeSumOfAngles(
                    std::vector<double>{psi1, psi2});

            particle.setPose(particlePose);

            /*bool firstGuessInsideField = false;
            if (p31.x > -1*FieldDescription::FIELD_LENGTH / 2.0
                && p31.x < 1*FieldDescription::FIELD_LENGTH / 2.0
                && p31.y > -1*FieldDescription::FIELD_WIDTH / 2.0
                && p31.y < 1*FieldDescription::FIELD_WIDTH / 2.0) {
                //particle.translation = p31;
                makeResettedParticle(particle, p31, observation1, observation2);
                firstGuessInsideField = true;
            }
            if (p32.x > -1*FieldDescription::FIELD_LENGTH / 2.0
                && p32.x < 1*FieldDescription::FIELD_LENGTH / 2.0
                && p32.y > -1*FieldDescription::FIELD_WIDTH / 2.0
                && p32.y < 1*FieldDescription::FIELD_WIDTH / 2.0) {
                if (firstGuessInsideField) {
                    if (rand() % 2 == 0) {
                        //particle.translation = p32;
                        makeResettedParticle(particle, p32, observation1, observation2);
                    }
                } else {
                    //particle.translation = p32;
                    makeResettedParticle(particle, p32, observation1, observation2);
                }
            }*/
        }

        void Resetter::computeParticleResetWithThreeLandmarks(Particle &particle, LandmarkObservation observation1,
                                                              LandmarkObservation observation2,
                                                              LandmarkObservation observation3) {
            static Pose2D particlePose;

            double x1l = observation1.knownPosition.x - observation2.knownPosition.x;
            double y1l = observation1.knownPosition.y - observation2.knownPosition.y;
            double x3l = observation3.knownPosition.x - observation2.knownPosition.x;
            double y3l = observation3.knownPosition.y - observation2.knownPosition.y;

            double T12 = cos(observation2.bearing - observation1.bearing)
                         / sin(observation2.bearing - observation1.bearing);
            double T23 = cos(observation3.bearing - observation2.bearing)
                         / sin(observation3.bearing - observation2.bearing);
            double T31 = (1.0 - T12 * T23) / (T12 + T23);

            double x12l = x1l + T12 * y1l;
            double y12l = y1l - T12 * x1l;
            double x23l = x3l - T23 * y3l;
            double y23l = y3l + T23 * x3l;
            double x31l = (x3l + x1l) + T31 * (y3l - y1l);
            double y31l = (y3l + y1l) - T31 * (x3l - x1l);

            double k31l = x1l * x3l + y1l * y3l + T31 * (x1l * y3l - x3l * y1l);

            double D = (x12l - x23l) * (y23l - y31l) - (y12l - y23l) * (x23l - x31l);

            double xR = observation2.knownPosition.x + (k31l * (y12l - y23l)) / D;
            double yR = observation2.knownPosition.y + (k31l * (x23l - x12l)) / D;

//    observation1.distance += distanceDistribution();
//    observation1.bearing += bearingDistribution();
//    observation2.distance += distanceDistribution();
//    observation2.bearing += bearingDistribution();
//    observation3.distance += distanceDistribution();
//    observation3.bearing += bearingDistribution();

            Vector2<double> reference(xR, yR);
            Vector2<double> closestIntersection1;
            Vector2<double> closestIntersection2;
            Vector2<double> closestIntersection3;
            bool success;
            success = computeClosestCircleIntersection(observation1.knownPosition, observation1.distance,
                                                       observation2.knownPosition, observation2.distance,
                                                       reference, closestIntersection1);
            if (!success)
                return;
            success = computeClosestCircleIntersection(observation2.knownPosition, observation2.distance,
                                                       observation3.knownPosition, observation3.distance,
                                                       reference, closestIntersection2);
            if (!success)
                return;
            success = computeClosestCircleIntersection(observation1.knownPosition, observation1.distance,
                                                       observation3.knownPosition, observation3.distance,
                                                       reference, closestIntersection3);
            if (!success)
                return;

            particlePose.translation.x =
                    (closestIntersection1.x + closestIntersection2.x + closestIntersection3.x) / 3.0;
            particlePose.translation.y =
                    (closestIntersection1.y + closestIntersection2.y + closestIntersection3.y) / 3.0;

            double psi1 = atan2(observation1.knownPosition.y - particlePose.translation.y,
                                observation1.knownPosition.x - particlePose.translation.x) - observation1.bearing;
            double psi2 = atan2(observation2.knownPosition.y - particlePose.translation.y,
                                observation2.knownPosition.x - particlePose.translation.x) - observation2.bearing;
            double psi3 = atan2(observation3.knownPosition.y - particlePose.translation.y,
                                observation3.knownPosition.x - particlePose.translation.x) - observation3.bearing;

            particlePose.rotation = itandroids_lib::math::MathUtils::normalizeSumOfAngles(
                    std::vector<double>{psi1, psi2, psi3});

            double sqrt3 = sqrt(3.0);
            particlePose.translation.x += distanceDistribution() / sqrt3;
            particlePose.translation.y += distanceDistribution() / sqrt3;
            particlePose.rotation += bearingDistribution() / sqrt3;

            particle.setPose(particlePose);
        }

        void Resetter::makeResettedParticle(Particle &particle, Vector2<double> translation,
                                            LandmarkObservation &observation1,
                                            LandmarkObservation &observation2) {
            Pose2D particlePose;
            particlePose.translation = translation;
            double psi1 = atan2(observation1.knownPosition.y - translation.y,
                                observation1.knownPosition.x - translation.x) - observation1.bearing;
            double psi2 = atan2(observation2.knownPosition.y - translation.y,
                                observation2.knownPosition.x - translation.x) - observation2.bearing;

            particlePose.rotation = itandroids_lib::math::MathUtils::normalizeSumOfAngles(
                    std::vector<double>{psi1, psi2});

            particle.setPose(particlePose);
        }


        bool Resetter::computeTwoCirclesIntersection(Vector2<double> center1, double radius1, Vector2<double> center2,
                                                     double radius2,
                                                     Vector2<double> &intersection1, Vector2<double> &intersection2) {
            double r0 = radius1;
            double r1 = radius2;
            double d = (center1 - center2).abs();

            if (d > r0 + r1)
                return false;
            if (d < fabs(r0 - r1))
                return false;

            double a = (r0 * r0 - r1 * r1 + d * d) / (2 * d);
            double h = sqrt(r0 * r0 - a * a);

            Vector2<double> p0 = center1;
            Vector2<double> p1 = center2;
            Vector2<double> p2 = p0 + (p1 - p0) * (a / d);

            intersection1.x = p2.x + (h / d) * (p1.y - p0.y);
            intersection1.y = p2.y - (h / d) * (p1.x - p0.x);

            intersection2.x = p2.x - (h / d) * (p1.y - p0.y);
            intersection2.y = p2.y + (h / d) * (p1.x - p0.x);

            return true;
        }

        bool
        Resetter::computeClosestCircleIntersection(Vector2<double> center1, double radius1, Vector2<double> center2,
                                                   double radius2, Vector2<double> reference,
                                                   Vector2<double> &closestIntersection) {
            Vector2<double> intersection1;
            Vector2<double> intersection2;
            computeTwoCirclesIntersection(center1, radius1, center2, radius2, intersection1, intersection2);

            if (intersection1.distance(reference) < intersection2.distance(reference))
                closestIntersection = intersection1;
            else
                closestIntersection = intersection2;

            return true;
        }

    } /* namespace state_estimation */
} /* namespace modeling */