/*
 * UniversalJointEffector.cpp
 *
 *  Created on: Sep 30, 2015
 *      Author: mmaximo
 */

#include "action/UniversalJointEffector.h"

#include <sstream>

namespace action {

    UniversalJointEffector::UniversalJointEffector(const std::string &name,
                                                   double axisSpeed1, double axisSpeed2) :
            name(name), axisSpeed1(axisSpeed1), axisSpeed2(axisSpeed2) {
    }

    UniversalJointEffector::~UniversalJointEffector() {
    }

    std::string UniversalJointEffector::toString() {

        // Gets the joint's name and speed as server message
        std::stringstream stream;
        stream << "(";
        stream << name;
        stream << " ";
        stream << axisSpeed1;
        stream << " ";
        stream << axisSpeed2;
        stream << ")";
        return stream.str();
    }

} /* namespace action */
