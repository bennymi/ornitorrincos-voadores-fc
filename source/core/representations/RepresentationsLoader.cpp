//
// Created by francisco on 09/12/16.
//

#include "RepresentationsLoader.h"
#include "NavigationParams.h"

namespace representations {

    template<>
    void RepresentationsLoader::setParameter(Parameter<pt::ptree, std::string> &parameter) {
        for (auto &child : parameter.getChildren()) {
            if (child.first == "navigationParams") {
                NavigationParams::getNavigationParams().setParameter(*child.second);
            }
        }
    }
}
