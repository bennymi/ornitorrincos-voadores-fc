//

// Created by luckeciano on 8/9/16.

//


#ifndef ITANDROIDS_SOCCER3D_CPP_HUNGARIANALG_H

#define ITANDROIDS_SOCCER3D_CPP_HUNGARIANALG_H


#include<iostream>

#include<fstream>

#include<cstdio>

#include<cmath>

#include<string>

#include<algorithm>

#include<vector>

#include<climits>

#include<float.h>

#include<string.h>

#include "Bit64Operator.h"




//Code for O(n^3) Hungarian algorithm from http://community.topcoder.com/tc?module=Static&d1=tutorials&d2=hungarianAlgorithm

//tutorial: https://www.topcoder.com/community/data-science/data-science-tutorials/assignment-problem-and-hungarian-algorithm/

namespace decision_making {

    namespace positioning {

        namespace role_assignment {


            class HungarianAlg {


            public:


                void initLabelsLarge();


                void updateLabelsLarge();


                void addToTreeLarge(int x, int prevX);


                bool augmentLarge();


                void hungarianLarge();


                long newDist[MAXN][MAXN];

                int xyLarge[PLAYERS]; //xyLarge[x] - vertex that is matched with x

                int yxLarge[PLAYERS]; //yxLarge[y] - vertex that is matched with y

                int nLarge;


            private:

                int maxMatchLarge; //n workers and n jobs

                uint64 lxLarge[PLAYERS][DATA_64WORDS_SIZE], lyLarge[PLAYERS][

                        DATA_64WORDS_SIZE]; //labels of X and Y matches

                bool SLarge[PLAYERS], TLarge[PLAYERS]; // sets S and T in algorithm

                uint64 slackLarge[PLAYERS][DATA_64WORDS_SIZE]; // as in the algorithm description

                int slackXLarge[PLAYERS]; //l(slackX[y]) + l(y) - w(slackX[y],y) = slack[y]

                int prevLarge[PLAYERS]; //array for memorizing alternating paths



            };

        }

    }

}


#endif //ITANDROIDS_SOCCER3D_CPP_HUNGARIANALG_H
