//
// Created by luckeciano on 16/05/16.
//

#include <iostream>
#include "utils/Logger.h"
#include "utils/LogSystem.h"
#include "decision_making/behavior/ActiveVision.h"
#include "base/BaseAgentTest.h"
#include "utils/DrawVisibleObjects.h"
#include "ActiveVisionAgent.h"
#include "PassiveAgent.h"
#include <chrono>

using ::itandroids_lib::utils::LogSystem;
using ::itandroids_lib::utils::Logger;
using ::itandroids_lib::math::Vector3;
using ::itandroids_lib::math::Pose2D;
using namespace std::chrono_literals;

using namespace decision_making::behavior::active_vision;

static const int N_LOOPS = 50000;


int main() {
    /**
     * Start the external agent
     */
    std::vector<std::thread> threads;
    for (int i = 2; i < 4; i++) {
        std::stringstream stream;
        stream << "./PassiveAgent_AgentTest " << i << " " << "Enemy &";
        system(stream.str().c_str());
        stream.str("");
        stream << "sleep 5";
        system(stream.str().c_str());
    }


    /**
     * Starts the active vision agent
     */
    ActiveVisionAgent agent;
    agent.testLoop(N_LOOPS);
    return 0;
}
