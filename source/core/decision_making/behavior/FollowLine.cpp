/*
 * FollowLine.cpp
 *
 *  Created on: Oct 31, 2015
 *      Author: itandroids
 */

#include "FollowLine.h"
#include "BehaviorFactory.h"

namespace decision_making {
    namespace behavior {
        using namespace itandroids_lib::math;
        using namespace representations;

        FollowLine::FollowLine() :
                yController(2.0 * M_PI * 1.0, 10), psiController(2, 10) {
            c = 0;
            a = 0;
            b = 0;
        }

        FollowLine::FollowLine(BehaviorFactory *behaviorFactory) :
                Behavior(behaviorFactory), yController(2.0 * M_PI * 1.0, 10), psiController(2, 10) {
            c = 0;
            a = 0;
            b = 0;
        }

        FollowLine::~FollowLine() {
        }

        void FollowLine::setLine(double direction,
                                 itandroids_lib::math::Vector2<double> initialPoint) {
            lineVector = Vector2<double>(cos(direction), sin(direction));
            this->initialPoint = initialPoint;
            a = -lineVector.y;
            b = lineVector.x;
            c = (lineVector.y * initialPoint.x) - (lineVector.x * initialPoint.y);
        }

        void FollowLine::behave(modeling::Modeling &modeling) {
            Pose2D agentPos = modeling.getWorldModel().getSelf().getPosition();
            Vector2<double> diffVector = initialPoint - agentPos.translation;
            double distanceY = 0;
            if (a * agentPos.translation.x + b * agentPos.translation.y + c > 0) {
                distanceY = -(diffVector
                              - lineVector * ((diffVector * lineVector))
                                / lineVector.sqr()).abs();
            } else {
                distanceY = (diffVector
                             - lineVector * ((diffVector * lineVector))
                               / lineVector.sqr()).abs();
            }
            //double distancePsi = -(agentPos.rotation - lineVector.angle());
            yController.update(distanceY);
//            psiController.update(distancePsi);
            Pose2D outputVelocity;
            outputVelocity.translation.x = representations::NavigationParams::getNavigationParams().max_v_forward;
            outputVelocity.translation.y = yController.getCommand();
            outputVelocity.rotation = navigationHelper.controlAngle(lineVector.angle(), agentPos.rotation);
            outputVelocity = navigationHelper.saturateWalkVelocity(outputVelocity);
            movementRequest = std::make_shared<control::WalkRequest>(outputVelocity);
        }

    } /* namespace behavior */
} /* namespace decision_making */
