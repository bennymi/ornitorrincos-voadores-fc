//
// Created by muzio on 29/04/16.
//

#include "BaseAgentTest.h"
#include "representations/RepresentationsLoader.h"

using representations::RepresentationsLoader;

BaseAgentTest::BaseAgentTest(std::string host, int agentPort, int monitorPort, int agentNumber, int robotType,
                             std::string teamName) : communication(
        new communication::ServerSocketImpl(host, agentPort)),
                                                     wizCommunication(
                                                             new communication::ServerSocketImpl(host, monitorPort)),
                                                     wiz(wizCommunication),
                                                     control(static_cast<representations::RobotType::ROBOT_TYPE>(robotType)),
                                                     modeling(agentNumber, teamName), agentNumber(agentNumber),
                                                     agentPort(agentPort), monitorPort(monitorPort) {
    auto root = utils::configure_tree::ParameterTreeCreator::getRoot<>();
    RepresentationsLoader representationsLoader;
    representationsLoader.setParameter(root->getChild("representations"));
    control.setParameter(root->getChild("control"));
    communication.establishConnection();
    wizCommunication.establishConnection();
    action.create(robotType); //number of robot type
    communication.sendMessage(action.getServerMessage());
    communication.receiveMessage();
    perception.perceive(communication);
    action.init(agentNumber, teamName);

    wiz.run();
    communication.sendMessage(action.getServerMessage());
    communication.receiveMessage();
    perception.perceive(communication);
    modeling.model(perception, control);

    decisionMaking.beamRequest = NULL;
    control.control(perception, modeling, decisionMaking);
    action.act(decisionMaking, control);
    communication.sendMessage(action.getServerMessage());


}


BaseAgentTest::~BaseAgentTest() {
    communication.closeConnection();
}

void BaseAgentTest::testLoop(int numberOfSteps) {
    setup();
    wiz.setPlayMode(representations::RawPlayMode::PLAY_ON);
    for (currentStep = 0; currentStep < numberOfSteps; currentStep++) {
        communication.receiveMessage();
        perception.perceive(communication);
        modeling.model(perception, control);

        //step
        runStep();

        control.control(perception, modeling, decisionMaking);
        action.act(decisionMaking, control);
        communication.sendMessage(action.getServerMessage());
        elapsedTime += STEP_DT;
    }
    finish();
}

void BaseAgentTest::finish() {

}







