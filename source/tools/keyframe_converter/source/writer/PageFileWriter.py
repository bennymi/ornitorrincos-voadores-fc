
from . import KeyframeFileWriter


class PageFileWriter(KeyframeFileWriter.KeyframeFileWriter):

    def __init__(self):
        super(PageFileWriter, self).__init__()

    def writeToFile(self, outputFile, keyframeMovement):
        #write beggining of the file
        if outputFile is None:
            outputFile = keyframeMovement.name + ".page"
        print(outputFile)
        f = open(outputFile, 'w')
        f.write('type = motion\n')
        f.write('version = 1.0\n')
        f.write('interpolator = ')
        f.write(keyframeMovement.interpolator)
        f.write('\n')
        f.write('page_begin\n')
        f.write('name = ')
        f.write(keyframeMovement.name)
        f.write('\n')
        f.write('play_param = 0 0 1 %s 32\n' % keyframeMovement.speedRate)
        for step in keyframeMovement.steps:
            jointsPositions = []
            jointsPositions = step.getAsList()
            jointsPositions = jointsPositions + [0, step.time]
            f.write('step = ')
            f.write(' '.join(map(str, jointsPositions)))
            f.write('\n')
            #Do something with a step to write to file
        f.write('page_end\n')
        f.write('end\n')





