//
// Created by francisco on 6/16/16.
//

#include "ShortDistanceKick.h"

namespace decision_making {
    namespace behavior {
        namespace kick_behavior {
            ShortDistanceKick::ShortDistanceKick() : KeyframeKick("fastLongKickLeftLeg") {
                setKickParameters(
                        KickParameters(Vector2<double>(-0.17, -0.04), 0));
            }
        }
    }
}