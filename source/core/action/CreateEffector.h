/*
 * CreateEffector.h
 *
 *  Created on: Sep 30, 2015
 *      Author: mmaximo
 */

#ifndef SOURCE_ACTION_CREATEEFFECTOR_H_
#define SOURCE_ACTION_CREATEEFFECTOR_H_

#include "action/Effector.h"

namespace action {

/**
 * Class that represents a create effector.
 * CreateEffector is sent once after the agent connects to create a robot within the server.
 */
    class CreateEffector : public Effector {
    public:
        /**
         * Creates a CreateEffector.
         *
         * @param filename file that specifies the
         * 				   robot configuration.
         * @param type the robot type.
         */
        CreateEffector(const std::string &filename, int type);

        /**
         * Default destructor of CreateEffector.
         */

        virtual ~CreateEffector();

        /**
         * Gets the create effector as a server message.
         *
         * @return the server message.
         */

        std::string toString();

    private:
        std::string filename;
        int type;
    };

} /* namespace action */

#endif /* SOURCE_ACTION_CREATEEFFECTOR_H_ */
