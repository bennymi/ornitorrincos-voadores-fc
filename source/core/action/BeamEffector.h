/*
 * BeamEffector.h
 *
 *  Created on: Aug 8, 2015
 *      Author: mmaximo
 */

#ifndef SOURCE_ACTION_BEAMEFFECTOR_H_
#define SOURCE_ACTION_BEAMEFFECTOR_H_

#include "action/Effector.h"
#include "math/Pose2D.h"

namespace action {

    using namespace itandroids_lib::math;

/**
 * Class that represents a beam effector.
 * BeamEffector is used to reposition the robot player at times in the game when this is allowed.
 */
    class BeamEffector : public Effector {
    public:
        /**
         * Creates a beam effector.
         *
         * @param pose pose to beam to.
         */
        BeamEffector(const Pose2D &pose);

        /**
         * Default destructor of BeamEffector.
         */

        virtual ~BeamEffector();

        /**
         * Gets the beam effector as a server message.
         *
         * @return the server message.
         */

        std::string toString();

    private:
        itandroids_lib::math::Pose2D pose;
    };

} /* namespace action */

#endif /* SOURCE_ACTION_BEAMEFFECTOR_H_ */
