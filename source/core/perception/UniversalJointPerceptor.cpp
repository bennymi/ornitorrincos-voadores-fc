/*
 * UniversalJointPerceptor.cpp
 *
 *  Created on: Aug 23, 2015
 *      Author: dicksiano
 */

#include "UniversalJointPerceptor.h"

namespace perception {

    /**
     * This constructor takes a parser tree node and manipulates it to get information
     * of name and angle axes 1 and 2.
     */
    UniversalJointPerceptor::UniversalJointPerceptor(
            utils::parser::ParserTreeNode &node) :
            Perceptor(node) {
        //First child of node is the name.
        name =
                utils::parser::KeyValuePair(node.children[0]->content, ' ').value.c_str();
        //Second child of node is the angle axis 1
        angleAxis1 =
                atof(
                        utils::parser::KeyValuePair(node.children[1]->content, ' ').value.c_str());
        //Third child of node is the angles axis 2
        angleAxis2 =
                atof(
                        utils::parser::KeyValuePair(node.children[2]->content, ' ').value.c_str());
    }

    UniversalJointPerceptor::~UniversalJointPerceptor() {

    }

    /*
     * Getter methods: returns universal joint perceptor data (name, angle axis 1, angle axis 2)
     */
    std::string UniversalJointPerceptor::getName() {
        return name;
    }

    double UniversalJointPerceptor::getAngleAxis1() {
        return angleAxis1;
    }

    double UniversalJointPerceptor::getAngleAxis2() {
        return angleAxis2;
    }

} /* namespace perception */
