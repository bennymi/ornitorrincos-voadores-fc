# -*- coding: utf-8 -*-
from writer import KeyframeFileWriter


class JsonFileWriter(KeyframeFileWriter.KeyframeFileWriter):

    def __init__(self):
        super(JsonFileWriter, self).__init__()

    def writeToFile(self, outputFile, keyframeMovement):
        f = open(outputFile, 'w')
        f.write("{\n")
        f.write("  \"name\" : \"%s\",\n" % keyframeMovement.name)
        f.write("  \"speedRate\" : %s,\n" % keyframeMovement.speedRate)
        f.write("  \"interpolator\" : \"%s\",\n" % keyframeMovement.interpolator)
        f.write("  \"steps\" : [\n")
 # Write the steps of the keyframe file
        for step in keyframeMovement.steps:
            endOfBlockChar = ""
            if(step != keyframeMovement.steps[-1]):
                endOfBlockChar = ","
            jointPositions = step.getAsList()
            f.write("    {\n")
            for index, jointPosition in enumerate(jointPositions):
                f.write("      \"%s\" : %s,\n" % (step.jointRepresentations[index],
                jointPosition))
            f.write("      \"delta\" : %s\n" % step.time)
            f.write("    }%s\n" % endOfBlockChar)
        f.write("  ]\n")
        f.write("}")