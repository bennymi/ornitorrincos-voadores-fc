/*
 * Self.h
 *
 *  Created on: Sep 8, 2015
 *      Author: mmaximo
 */

#ifndef SOURCE_REPRESENTATIONS_SELF_H_
#define SOURCE_REPRESENTATIONS_SELF_H_

#include "math/Pose2D.h"

namespace representations {

    using namespace itandroids_lib::math;

    class Self {
    public:
        Self();

        virtual ~Self();

        Pose2D &getPosition();

        void setPosition(Pose2D pose);

    private:
        Pose2D position;
        Pose2D velocity;
    };

} /* namespace representations */

#endif /* SOURCE_REPRESENTATIONS_SELF_H_ */
