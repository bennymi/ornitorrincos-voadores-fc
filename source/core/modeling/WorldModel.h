/*
 * WorldModel.h
 *
 *  Created on: Aug 8, 2015
 *      Author: mmaximo
 */

#ifndef SOURCE_MODELING_WORLDMODEL_H_
#define SOURCE_MODELING_WORLDMODEL_H_

#include <vector>
#include <stdlib.h>

#include <algorithm>
#include "math/Vector3.h"
#include "perception/AgentPerception.h"
#include "representations/Ball.h"
#include "representations/OtherPlayer.h"
#include "modeling/BallModel.h"
#include "modeling/SelfModel.h"
#include "modeling/OtherPlayerModel.h"
#include "modeling/PositioningModel.h"
#include "modeling/state_estimation/BallTracker.h"
#include "modeling/state_estimation/Localizer.h"
#include "modeling/state_estimation/PlayerTracker.h"
#include "modeling/AgentModel.h"
#include "representations/PlayMode.h"
#include "representations/ITAndroidsConstants.h"
#include <boost/algorithm/string.hpp>
#include <core/representations/GameSituation.h>
#include "math/Pose2D.h"

namespace modeling {

    using namespace itandroids_lib::math;
    using namespace modeling::state_estimation;
    using namespace representations;

/**
 * Class that represents the world model.
 */
    class WorldModel {
    public:
        /**
         * Default constructor for worldModel
         */
        WorldModel(std::string teamName = representations::ITAndroidsConstants::TEAM_DEFAULT_NAME);

        virtual ~WorldModel();

        /**
         * method to update all the data in modelling, and run the localizer
         */
        void update(perception::AgentPerception &agentPerception,
                    AgentModel &agentModel, Pose2D &odometry);

        /**
         * Returns the ball
         */
        Ball &getBall();

        /**
         * Returns self data
         */
        Self &getSelf();

        /**
         * Resets the localizer, so that the robot can find itself again
         */
        void resetLocalizer(Pose2D estimate, Pose2D sigmas);

        /**
         * Returns the position of Own goal in a 3D vector, giving the (x,y) coordinates at the middle of the goal and the z coordinates as the height
         */
        Vector3<double> getOwnGoalPosition();

        /**
         * Returns the position of the enemy team goal in a 23 vector
         */
        const Vector3<double> getTheirGoalPosition();

        /**
         * Returns the goalie, with the model for it
         */
        const OtherPlayer &getOurGoalie();

        /**
         * Returns the goalie of the enemy team, with the model for it
         */
        const OtherPlayer &getTheirGoalie();

        /**
         * Returns the player data with the uniformNumber
         */

        const OtherPlayer &getTeammate(int uniformNumber);

        /**
         * Returns the opponent data with the giver uniformNumber
         */
        const OtherPlayer &getOpponent(int uniformNumber);

        /**
         * Returns the opponent data of the opponent closest from self
         */
        const OtherPlayer &getOpponentClosestFromSelf();

        /**
         * Returns the opponent data of the oppponent closest from ball
         */
        const OtherPlayer &getOpponentClosestFromBall();

        /**
         * Returns the teammate data of the teammate closest from ball
         */
        const OtherPlayer &getTeammateClosestFromBall();

        /**
         * Returns all the teammates
         */
        std::vector<OtherPlayer *> &getTeammates();

        /**
         * Return all the Opponents
         */
        std::vector<OtherPlayer *> &getOpponents();

        /**
         * Gets the ordered players with distance from self
         */
        const std::vector<std::pair<double, OtherPlayer *> > &getOtherPlayersFromSelf();

        /**
         * Gets the ordered players with distance from ball
         */
        const std::vector<std::pair<double, OtherPlayer *> > &getOtherPlayersFromBall();

        /**
         * Gets the teammates players with distance from self
         */
        const std::vector<std::pair<double, OtherPlayer *> > &getTeammatesFromSelf();

        /**
         * Gets the teammates players with distance from ball
         */
        const std::vector<std::pair<double, OtherPlayer *> > &getTeammatesFromBall();

        /**
         * Gets the ordered opponents with distance from self
         */
        const std::vector<std::pair<double, OtherPlayer *> > &getOpponentsFromSelf();

        /**
         * Gets the ordered opponents with distance from ball
         */
        const std::vector<std::pair<double, OtherPlayer *> > &getOpponentsFromBall();

        /**
         * Gets the ordered opponents with distance from goal
         */
        const std::vector<std::pair<double, OtherPlayer *> > &getOpponentsFromOurGoal();

        /**
         * gets the name of our team
         */
        const std::string &getOurTeamName();

        /**
         * Gets the name of their team
         */
        const std::string &getTheirTeamName();

        /**
         * Gets our playside
         */
        PlaySide::PLAY_SIDE getOurPlaySide();

        /**
         * Gets their playside
         */
        PlaySide::PLAY_SIDE getTheirPlaySide();

        /**
         * Gets the concrete playMode
         */
        PlayMode::PLAY_MODE getPlayMode();

        /**
         * Gets my role assignment vector
         */
        std::vector<int> getMyRoleAssignmentVector();

        /**
         * updates role assignment vector
         */
        void updateMyRoleAssignmentVector(std::vector<int> newRoleVector);

        /**
        * Gets synchronized role assignment vector
        */
        std::vector<int> getSynchronizedRoleAssignmentVector();

        /**
          * Gets teammates role assignment vectors
        */
        std::vector<std::vector<int> > getTeammatesRoleAssignmentVector();

        /**
         * Gets the concrete game state
         */
        representations::GameState &getGameState();

        /**
         * Gets the current game situation: if the team is attacking or defending
         */
        representations::GameSituation::GAME_SITUATION getGameSituation();

        /**
         * Set our team name
         */
        void setOurTeamName(std::string name);

        /**
         * Checking if self is Closest to ball
         */
        bool selfIsClosestToBall();

        /**
         * Checking if teammate is Closest to ball
         */
        bool teammateIsClosestToBall();

        /**
         * Checking if self is the Second Closest to ball
         */
        bool selfIsSecondClosestToBall();

        /**
         * Checking if self is the Third Closest to ball
         */
        bool selfIsThirdClosestToBall();

        /**
         * A function for sorting the vectors of player models
         */
        static int sortFunction(const void *a, const void *b);

        /**
         * returns the number of visible objects
         */
        int getVisibleObjectsCounter();

        /**
         * A function that says wheter the robot is with the ball or not
         */
        bool selfIsWithBall();

        FieldDescription &getFieldDescription();

        /**
         * Gets opponent to talk.
         * Opponent to talk is the next opponent that the goalie will Say the position
         * in Say message.
         * Because of limitated bandwith (20 bytes), we have to pass just
         * 4 players per cycle to synchronize opponents in world model.
         */
        int getOpponentToTalk();

        /**
         * Sets opponent to talk.
         * Each cycle goalie talks four opponents
         * Example:
         * Cycle 1: 1, 2, 3 , 4
         * Cycle 2: 5, 6, 7, 8
         * Cycle 3: 9, 10, 11, 1
         * Cycle 4: 2, 3, 4, 5...
         */

        void newOpponentToTalk();

        static const int GOALIE_UNIFORM_NUMBER = 1;
        static const int NUM_PLAYERS_PER_TEAM = 11;

    private:
        FieldDescription fieldDescription;
        BallModel ballModel;
        SelfModel selfModel;
        PositioningModel positioningModel;
        std::vector<OtherPlayerModel> teammateModels;
        std::vector<OtherPlayerModel> opponentModels;
        std::vector<OtherPlayerModel> heardTeammateModels;
        std::vector<OtherPlayerModel> heardOpponentModels;
        std::vector<OtherPlayer *> teammates;
        std::vector<OtherPlayer *> opponents;
        std::vector<std::pair<double, OtherPlayer *> > otherPlayersFromSelf;
        std::vector<std::pair<double, OtherPlayer *> > otherPlayersFromBall;
        std::vector<std::pair<double, OtherPlayer *> > teammatesFromSelf;
        std::vector<std::pair<double, OtherPlayer *> > teammatesFromBall;
        std::vector<std::pair<double, OtherPlayer *> > opponentsFromSelf;
        std::vector<std::pair<double, OtherPlayer *> > opponentsFromBall;
        std::vector<std::pair<double, OtherPlayer *> > opponentsFromOurGoal;
        std::string ourTeamName;
        std::string theirTeamName;
        PlaySide::PLAY_SIDE ourPlaySide;
        PlaySide::PLAY_SIDE theirPlaySide;
        PlayMode::PLAY_MODE playMode;
        GameState gameState;
        double selfDistanceToBall;
        double globalTime;
        int opponentToTalk = 1;
        representations::HearData hearData;
        representations::GameSituation::GAME_SITUATION gameSituation;
        perception::VisibleBall *visibleBall;
        std::vector<perception::VisiblePlayer *> visiblePlayers;
        std::vector<perception::VisibleFlag *> visibleFlags;
        std::vector<perception::VisibleGoalPost *> visibleGoalPosts;
        std::vector<perception::VisibleLine *> visibleLines;
        int visibleObjectsCounter;

        /**
         * Sort the teammatesFromSelf vector
         */
        void sortTeammatesFromSelf();

        /**
         * Sort the teammatesFromBall vector
         */
        void sortTeammatesFromBall();

        /**
         * Sort the opponentsFromSelf vector
         */
        void sortOpponentsFromSelf();

        /**
         * Sort the opponentsFromBall vector
         */
        void sortOpponentsFromBall();

        /**
        * Sort the opponentsFromOurGoal vector
        */
        void sortOpponentsFromOurGoal();

        /**
         * Updates the game Situation
         */
        void updateGameSituation();

        /**
         * Check if the ball is in our goal box.
         */
        bool isBallInOurGoalBox();


    };

} /* namespace modeling */

#endif /* SOURCE_MODELING_WORLDMODEL_H_ */
