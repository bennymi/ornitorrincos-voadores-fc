//
// Created by luckeciano on 8/9/16.
//

#include "Bit64Operator.h"

namespace decision_making {
    namespace positioning {
        namespace role_assignment {

            bool Bit64Operator::isZero(const uint64 (&value)[DATA_64WORDS_SIZE]) {
                for (int i = 0; i < DATA_64WORDS_SIZE - 1; i++) {
                    if (value[i] != 0)
                        return false;

                }
                return true;
            }

            bool Bit64Operator::isEqual(const uint64 (&valueA)[DATA_64WORDS_SIZE],
                                        const uint64 (&valueB)[DATA_64WORDS_SIZE]) {
                if (valueA[DATA_64WORDS_SIZE - 1] != valueB[DATA_64WORDS_SIZE - 1]) {
                    if (isZero(valueA) && isZero(valueB))
                        return true;
                    return false;
                }
                for (int i = 0; i < DATA_64WORDS_SIZE - 1; i++) {
                    if (valueA[i] != valueB[i])
                        return false;
                }
                return true;
            }

            bool Bit64Operator::isLessThan(const uint64 (&valueA)[DATA_64WORDS_SIZE],
                                           const uint64 (&valueB)[DATA_64WORDS_SIZE]) {
                bool negA = valueA[DATA_64WORDS_SIZE - 1];
                bool negB = valueB[DATA_64WORDS_SIZE - 1];
                if (!negA && negB) {
                    return false;
                } else if (negA && !negB) {
                    return true;
                }

                for (int i = 0; i < DATA_64WORDS_SIZE - 1; i++) {
                    if (valueA[i] == valueB[i])
                        continue;
                    return valueA[i] < valueB[i] ^ negA;
                }
                return false;

            }

            void Bit64Operator::setMax(uint64 (&value)[DATA_64WORDS_SIZE]) {
                value[DATA_64WORDS_SIZE - 1] = 0;
                for (int i = 0; i < DATA_64WORDS_SIZE - 1; i++)
                    value[i] = (uint64) -1;
            }

            void Bit64Operator::setZero(uint64 (&value)[DATA_64WORDS_SIZE]) {
                value[DATA_64WORDS_SIZE - 1] = 0;

                for (int i = 0; i < DATA_64WORDS_SIZE - 1; i++)
                    value[i] = 0;
            }

            void Bit64Operator::setBit(uint64 (&value)[DATA_64WORDS_SIZE], long bit) {
                setZero(value);
                uint64 val = ((uint64) 1) << std::abs(bit) % 64;
                value[DATA_64WORDS_SIZE - 1 - std::abs(bit) / 64 - 1] = val;
                if (bit < 0) {
                    value[DATA_64WORDS_SIZE - 1] = 1;
                }
            }

            void Bit64Operator::assignValue(uint64 (&valueA)[DATA_64WORDS_SIZE],
                                            const uint64 (&valueB)[DATA_64WORDS_SIZE]) {
                for (int i = 0; i < DATA_64WORDS_SIZE; i++) {
                    valueA[i] = valueB[i];
                }
            }

            void Bit64Operator::subtractValue(uint64 (&valueA)[DATA_64WORDS_SIZE],
                                              const uint64 (&valueB)[DATA_64WORDS_SIZE]) {
                if (isEqual(valueA, valueB)) {
                    setZero(valueA);
                    return;
                }
                bool negA = valueA[DATA_64WORDS_SIZE - 1];
                bool negB = valueB[DATA_64WORDS_SIZE - 1];

                if (negA != negB) {
                    uint64 temp[DATA_64WORDS_SIZE];
                    assignValue(temp, valueB);
                    temp[DATA_64WORDS_SIZE - 1] = !temp[DATA_64WORDS_SIZE - 1];
                    addValue(valueA, temp);
                    return;
                }

                if ((!negA && isLessThan(valueA, valueB)) ||
                    (negA && isLessThan(valueB, valueA))) {
                    uint64 temp[DATA_64WORDS_SIZE];
                    assignValue(temp, valueB);
                    subtractValue(temp, valueA);
                    temp[DATA_64WORDS_SIZE - 1] = !temp[DATA_64WORDS_SIZE - 1];
                    assignValue(valueA, temp);
                    return;
                }

                uint64 negValueB[DATA_64WORDS_SIZE];
                for (int i = 0; i < DATA_64WORDS_SIZE - 1; i++)
                    negValueB[i] = ~valueB[i];
                negValueB[DATA_64WORDS_SIZE - 1] = 0;
                uint64 one[DATA_64WORDS_SIZE];
                setBit(one, 0);
                one[DATA_64WORDS_SIZE - 1] = 0;
                addValue(negValueB, one);
                uint signA = valueA[DATA_64WORDS_SIZE - 1];
                valueA[DATA_64WORDS_SIZE] = 0;
                addValue(valueA, valueB);
                valueA[DATA_64WORDS_SIZE - 1] = signA;

            }


            void Bit64Operator::addValue(uint64 (&valueA)[DATA_64WORDS_SIZE],
                                         const uint64 (&valueB)[DATA_64WORDS_SIZE]) {
                if (valueA[DATA_64WORDS_SIZE - 1] != valueB[DATA_64WORDS_SIZE - 1]) {
                    uint64 temp[DATA_64WORDS_SIZE];
                    assignValue(temp, valueB);
                    temp[DATA_64WORDS_SIZE] = !temp[DATA_64WORDS_SIZE - 1];
                    subtractValue(valueA, temp);
                    return;
                }
                int carry = 0;
                for (int i = DATA_64WORDS_SIZE - 2; i >= 0; i--) {
                    valueA[i] += valueB[i] + carry;
                    bool overflow = valueA[i] == valueB[i] && carry == 1;
                    if (valueA[i] < valueB[i] || overflow)
                        carry = 1;
                    else
                        carry = 0;
                }

            }
        }
    }
}

