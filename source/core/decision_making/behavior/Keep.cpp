/*
 * Keep.cpp
 *
 *  Created on: Oct 31, 2015
 *      Author: itandroids
 */

#include "Keep.h"

namespace decision_making {
    namespace behavior {

        Keep::Keep() {

        }

        Keep::Keep(BehaviorFactory *behaviorFactory) : Behavior(behaviorFactory) {

        }

        Keep::~Keep() {
        }

        void Keep::behave(modeling::Modeling &modeling) {
            Pose2D &selfPosition =
                    modeling.getWorldModel().getSelf().getPosition(); // Getting self position.
            Pose2D &ballPosition =
                    modeling.getWorldModel().getBall().getPosition(); // Getting ball position.

            double YDisplacement = ballPosition.translation.y - selfPosition.translation.y;


            if (YDisplacement >= 0) {
                movementRequest = std::make_shared<control::KeyframeRequest>(
                        control::keyframe::KeyframeType::CATCH_LEFT, false);
            } else {
                movementRequest = std::make_shared<control::KeyframeRequest>(
                        control::keyframe::KeyframeType::CATCH_RIGHT, false);
            }
        }

    } /* namespace behavior */
} /* namespace decision_making */
