//
// Created by mmaximo on 21/06/16.
//

#include "decision_making/behavior/DribbleBall.h"
#include "decision_making/behavior/BehaviorFactory.h"

namespace decision_making {
    namespace behavior {

        const double DribbleBall::TEAMMATE_INFLUENCE_RADIUS = 1.0;
        const double DribbleBall::OPPONENT_INFLUENCE_RADIUS = 2.0;

        DribbleBall::DribbleBall(BehaviorFactory *factory) : Behavior(factory),
                                                             soccerPotentialField(
                                                                     SoccerPotentialField(PotentialFieldParameters())),
                                                             state(CIRCLE_BALL) {
        }

        DribbleBall::DribbleBall(BehaviorFactory *factory, PotentialFieldParameters parameters) : Behavior(factory),
                                                                                                  soccerPotentialField(
                                                                                                          parameters),
                                                                                                  state(CIRCLE_BALL) {
        }

        void DribbleBall::behave(modeling::Modeling &modeling) {
            Vector2<double> dribbleDirection = calculateDribbleDirection(
                    modeling.getWorldModel().getBall().getPosition().translation, modeling);
            dribbleDirection.normalize();
            double dribbleAngle = dribbleDirection.angle();

            Pose2D selfPose = modeling.getWorldModel().getSelf().getPosition();
            Vector2<double> ballPosition = modeling.getWorldModel().getBall().getPosition().translation;

            behaviorFactory->getCircleBall().setDribbleDirection(dribbleAngle);
            Pose2D circleBallPose = behaviorFactory->getCircleBall().getCircleBallPose(ballPosition);

            Vector2<double> selfToBall = ballPosition - selfPose.translation;
            double selfToBallAngle = selfToBall.angle();

            double facingError = MathUtils::normalizeAngle(selfPose.rotation - dribbleAngle);
            double positionAlignedError = MathUtils::normalizeAngle(selfToBallAngle - dribbleAngle);
//    std::cout << "facingError: " << facingError * 180.0 / M_PI << "; positionAlignedError: " << positionAlignedError * 180.0 / M_PI << std::endl;

            switch (state) {
                case CIRCLE_BALL:
                    if (fabs(positionAlignedError) < 20.0 * M_PI / 180.0 &&
                        fabs(facingError) < 40.0 * M_PI / 180.0) {
                        state = FOLLOW_LINE;
                    }
                    break;
                case FOLLOW_LINE:
                    if (((fabs(positionAlignedError) > 30.0 * M_PI / 180.0 ||
                          fabs(facingError) > 60.0 * M_PI / 180.0) &&
                         ballPosition.distance(selfPose.translation) > 0.1) ||
                        ballPosition.distance(selfPose.translation) > 0.2) {
                        state = CIRCLE_BALL;
                    }
                    break;
            }

            Vector2<double> closestOpponentToBallPosition = Vector2<double>(100.0, 100.0);
            bool closestOpponentToBallIsRelevant = false;
            /*
             * This condition prevents from try to access a player when the getOpponents() is empty!
             */
            if (modeling.getWorldModel().getOpponents().size() >= 1) {
                auto closestOpponentToBall = modeling.getWorldModel().getOpponentClosestFromBall();

                closestOpponentToBallPosition = closestOpponentToBall.getPosition().translation;
                closestOpponentToBallIsRelevant = closestOpponentToBall.isRelevant();
            }
            Vector2<double> selfPosition = modeling.getWorldModel().getSelf().getPosition().translation;
            double xLimit = modeling.getWorldModel().getFieldDescription().FIELD_LENGTH / 2.0 - 2.0;
            double yLeftGoalPost = modeling.getWorldModel().getFieldDescription().GOAL_AREA_WIDTH / 2.0;
            double yRightGoalPost = -modeling.getWorldModel().getFieldDescription().GOAL_AREA_WIDTH / 2.0;

            bool ballMayBeOut;
            if ((ballPosition.y > yLeftGoalPost || ballPosition.y < yRightGoalPost) && ballPosition.x > xLimit)
                ballMayBeOut = true;
            else
                ballMayBeOut = false;
            switch (state) {
                case CIRCLE_BALL:
                    if (closestOpponentToBallPosition.distance(ballPosition) < 0.5
                        && closestOpponentToBallIsRelevant && !ballMayBeOut) {
                        behaviorFactory->getFollowLine().setLine(selfPose.rotation, ballPosition);
                        behaviorFactory->getFollowLine().behave(modeling);
                        movementRequest = behaviorFactory->getFollowLine().getMovementRequest();
                    } else {
                        behaviorFactory->getCircleBall().setDribbleDirection(dribbleAngle);
                        behaviorFactory->getCircleBall().behave(modeling);
                        movementRequest = behaviorFactory->getCircleBall().getMovementRequest();
                    }
                    break;
                case FOLLOW_LINE:
                    behaviorFactory->getFollowLine().setLine(dribbleAngle, ballPosition);
                    behaviorFactory->getFollowLine().behave(modeling);
                    movementRequest = behaviorFactory->getFollowLine().getMovementRequest();
                    break;
            }
        }

        Vector2<double>
        DribbleBall::calculateDribbleDirection(Vector2<double> ballPosition, modeling::Modeling &modeling) {
            soccerPotentialField.clear();

            Vector3<double> goalPosition3D = modeling.getWorldModel().getTheirGoalPosition();
            Vector2<double> goalPosition = Vector2<double>(goalPosition3D.x, goalPosition3D.y);
            Vector2<double> targetPosition = goalPosition;

            Vector2<double> selfToTarget = targetPosition - ballPosition;

            for (auto &opponent : modeling.getWorldModel().getOpponents()) {
                Vector2<double> selfToOpponent = opponent->getPosition().translation - ballPosition;
                double crossProduct = selfToTarget.cross(selfToOpponent);
                bool counterClockwise = (crossProduct > 0.0);

                if (selfToOpponent.abs() <= OPPONENT_INFLUENCE_RADIUS)
                    soccerPotentialField.addOpponent(opponent->getPosition().translation, counterClockwise);
            }

            for (auto &teammate : modeling.getWorldModel().getTeammates()) {
                Vector2<double> selfToTeammate = teammate->getPosition().translation - ballPosition;
                double crossProduct = selfToTarget.cross(selfToTeammate);
                bool counterClockwise = (crossProduct > 0.0);

                if (selfToTeammate.abs() <= TEAMMATE_INFLUENCE_RADIUS)
                    soccerPotentialField.addTeammate(teammate->getPosition().translation, counterClockwise);
            }

            double distanceToGoal = ballPosition.distance(goalPosition);
            double yLeftGoalPost = modeling.getWorldModel().getFieldDescription().GOAL_AREA_WIDTH / 2.0;
            double yRightGoalPost = -modeling.getWorldModel().getFieldDescription().GOAL_AREA_WIDTH / 2.0;

            if (ballPosition.y > yLeftGoalPost || ballPosition.y < yRightGoalPost || ballPosition.x < 0.0)
                soccerPotentialField.addXFieldBorder(ballPosition);
            soccerPotentialField.addYFieldBorder(ballPosition);

            Vector2<double> displacement;
            if (ballPosition.y < yLeftGoalPost && ballPosition.y > yRightGoalPost)
                displacement.x = std::max(0.0, 3.0 - distanceToGoal);
            soccerPotentialField.addTargetDipole(goalPosition + displacement, 0.0);
            if (distanceToGoal < 2.0) {
                soccerPotentialField.addTargetDipole(goalPosition + displacement, 0.0);
                soccerPotentialField.addTargetDipole(goalPosition + displacement, 0.0);
            }

            fieldDirection = soccerPotentialField.calculateFieldDirection(ballPosition);

            return fieldDirection;
        }

    } /* namespace behavior */
} /* namespace decision_making */