#include "ITAndroidsConstants.h"

namespace representations {
    //Server constants
    const std::string ITAndroidsConstants::DEFAULT_IP = "127.0.0.1";
    const int ITAndroidsConstants::DEFAULT_PORT = 3100;
    const double ITAndroidsConstants::SAMPLE_TIME = 0.02;

    //Team constants
    const std::string ITAndroidsConstants::TEAM_DEFAULT_NAME = "MEU_TIME";
    const std::string ITAndroidsConstants::KEYFRAME_FOLDER = "../configuration/core/control/keyframe";
    const std::string ITAndroidsConstants::FORMATION_FOLDER = "../configuration/core/formation/";
    //Robot constants

    const double ITAndroidsConstants::DEFAULT_JOINT_MAX_SPEED = 6.1395;
}
