//
// Created by mmaximo on 21/06/16.
//

#include "base/BaseAgentTest.h"

class DribbleBall_AgentTest : public BaseAgentTest {
private:


public:
    virtual void testLoop(int nSteps) { BaseAgentTest::testLoop(nSteps); }

    virtual void setup() {
        wiz.setAgentPositionAndDirection(1, representations::PlaySide::LEFT,
                                         Vector3<double>(-2, 0, 0.3), 0.0);
    }

    virtual void runStep() {
        std::string buff("animated");
        std::string name("animated");
        std::string navigateString = "N";
        std::string dribbleString = "D";
        roboviz.drawLine(modeling.getWorldModel().getBall().getPosition().translation.x,
                         modeling.getWorldModel().getBall().getPosition().translation.y,
                         0, modeling.getWorldModel().getBall().getPosition().translation.x,
                         modeling.getWorldModel().getBall().getPosition().translation.y, 1, 1,
                         255, 255, 255, &name);
        roboviz.drawLine(modeling.getWorldModel().getSelf().getPosition().translation.x,
                         modeling.getWorldModel().getSelf().getPosition().translation.y,
                         0, modeling.getWorldModel().getSelf().getPosition().translation.x,
                         modeling.getWorldModel().getSelf().getPosition().translation.y, 1, 2,
                         255, 0, 0, &name);
        roboviz.swapBuffers(&buff);
        behaviorFactory.getActiveVision().behave(modeling);
        decisionMaking.lookRequest = behaviorFactory.getActiveVision().getLookRequest();
        Vector2<double> ballPosition = modeling.getWorldModel().getBall().getPosition().translation;
        if (modeling.getWorldModel().getSelf().getPosition().translation.distance(ballPosition) > 1.0) {
            roboviz.drawAnnotation(&navigateString, modeling.getWorldModel().getSelf().getPosition().translation.x,
                                   modeling.getWorldModel().getSelf().getPosition().translation.y, 1.0,
                                   0.0, 0.0, 1.0, &name);
            behaviorFactory.getNavigateToBall().behave(modeling);
            decisionMaking.movementRequest = behaviorFactory.getPotentialFieldNavigationBehavior().getMovementRequest();
        } else {
            roboviz.drawAnnotation(&dribbleString, modeling.getWorldModel().getSelf().getPosition().translation.x,
                                   modeling.getWorldModel().getSelf().getPosition().translation.y, 1,
                                   0.0, 0.0, 1.0, &name);
            behaviorFactory.getDribbleBall().behave(modeling);
            decisionMaking.movementRequest = behaviorFactory.getDribbleBall().getMovementRequest();
        }
    }
};

int main() {
    DribbleBall_AgentTest t;
    t.testLoop(50000);
}