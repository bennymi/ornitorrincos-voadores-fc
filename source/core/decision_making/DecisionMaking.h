/*
 * DecisionMaking.h
 *
 *  Created on: Aug 7, 2015
 *      Author: mmaximo
 */

#ifndef SOURCE_DECISION_MAKING_DECISIONMAKING_H_
#define SOURCE_DECISION_MAKING_DECISIONMAKING_H_

#include <iostream>

#include "math/Vector3.h"
#include "math/Pose2D.h"
#include "control/LookRequest.h"
#include "control/MovementRequest.h"
#include "control/KeyframeRequest.h"
#include "RoleStrategy.h"
#include "decision_making/positioning/role_assignment/MarkingSystem.h"

namespace modeling {
    class Modeling;
}

namespace decision_making {


    class DecisionMaking {
    public:
        DecisionMaking();

        virtual ~DecisionMaking();

        virtual std::shared_ptr<std::string> getSayMessage() = 0;

        virtual std::shared_ptr<control::LookRequest> getLookRequest() = 0;

        virtual std::shared_ptr<control::MovementRequest> getMovementRequest() = 0;

        virtual std::shared_ptr<itandroids_lib::math::Pose2D> getBeamRequest() = 0;

    };

/*
 * Class that represents the implementation of agent's decision making.
 */
    class DecisionMakingImpl : public DecisionMaking {
    public:

        /**
         * Default constructor for Decision Making Impl
         */
        DecisionMakingImpl();

        /**
         * Constructor for for Decision Making Impl
         *
         * @param pointer to Role Strategy
         */
        DecisionMakingImpl(std::shared_ptr<RoleStrategy> roleStrategy);

        /**
         * Default destructor for Decision Making
         */
        virtual ~DecisionMakingImpl();

        /**
         * Method that check if the agent is fallen and then gets
         * the requests from the agent role.
         *
         * @param modeling
         */
        void decide(modeling::Modeling &modeling);

        /**
         * Getter for the Say Request
         */
        virtual std::shared_ptr<std::string> getSayMessage();

        /**
         * Getter for the Look Request
         */
        virtual std::shared_ptr<control::LookRequest> getLookRequest();

        /**
         * Getter for the Movement Request
         */
        virtual std::shared_ptr<control::MovementRequest> getMovementRequest();

        /**
         * Getter for the Beam Request
         */
        virtual std::shared_ptr<itandroids_lib::math::Pose2D> getBeamRequest();

    private:
        decision_making::positioning::role_assignment::MarkingSystem markingSystem;

        /**
         * Method to check if the agent is fallen
         *
         * @param modeling
         */
        bool decideFall(modeling::Modeling &modeling);

        bool isGettingUp;
        std::shared_ptr<RoleStrategy> roleStrategy;
        std::shared_ptr<control::MovementRequest> movementRequest;
        std::shared_ptr<itandroids_lib::math::Pose2D> beamRequest;
        std::shared_ptr<control::LookRequest> lookRequest;

        std::shared_ptr<std::string> sayRequest;
    };

} /* namespace decision_making */

#endif /* SOURCE_DECISION_MAKING_DECISIONMAKING_H_ */
