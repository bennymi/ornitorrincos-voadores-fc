#ifndef SOURCE_PERCEPTION_VISIONOBJECTTYPE_H_
#define SOURCE_PERCEPTION_VISIONOBJECTTYPE_H_

namespace perception {

    class VisibleObjectType {
    public:
        enum VISIBLE_OBJECT_TYPE {
            UNKNOWN,
            VISIBLE_LINE,
            VISIBLE_BALL,
            VISIBLE_PLAYER,
            VISIBLE_GOAL_POST,
            VISIBLE_FLAG,
        };
    };


} /* namespace perception */

#endif /* SOURCE_PERCEPTION_VISIONPERCEPTOR_H_ */
