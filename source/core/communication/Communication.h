/*
 * Communication.h
 *
 *  Created on: Aug 7, 2015
 *      Author: mmaximo
 */

#ifndef SOURCE_COMMUNICATION_COMMUNICATION_H_
#define SOURCE_COMMUNICATION_COMMUNICATION_H_

#include <iostream>
#include <memory>

#include "communication/ServerSocket.h"


class ServerSocket;

namespace communication {

/**
* Class that receive the messages from the server and send messages.
*/
    class Communication {
    public:
        /**
         * Default constructor for Communication.
         */
        Communication();

        /**
         * Constructor for Communication with a server socket.
         *
         * @param serverSocket the server socket.
         */

        Communication(ServerSocket *serverSocket);


        // TODO Add an unique ptr constructor

        //Communication(std::unique_ptr<ServerSocket> ptr);

        /**
         * Default destructor for Communication.
         */
        virtual ~Communication();

        /**
         * Tries to establish the connection with the server.
         */

        void establishConnection();

        /**
         * Closes the connection with the server.
         */

        void closeConnection();

        /**
         * Receives a message from the server.
         */

        void receiveMessage();

        /**
         *  Sends a specific message to the server.
         *
         *  @param message a string that contains the message to send.
         */

        void sendMessage(std::string message);

        /**
         * Gets the current message (i.e, the last message received from the server).
         *
         * @return the current message.
         */

        std::string &getCurrentMessage();

        bool connectionClosed() { return serverSocket->connectionClosed(); }


    private:
        ServerSocket *serverSocket;

        std::string currentMessage;
    };

} /* namespace communication */

#endif /* SOURCE_COMMUNICATION_COMMUNICATION_H_ */
