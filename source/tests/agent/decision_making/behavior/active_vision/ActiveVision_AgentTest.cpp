//
// Created by luckeciano on 16/05/16.
//

#include <iostream>
#include "utils/Logger.h"
#include "utils/LogSystem.h"
#include "decision_making/behavior/ActiveVision.h"
#include "base/BaseAgentTest.h"
#include "ActiveVisionAgent.h"

using ::itandroids_lib::utils::LogSystem;
using ::itandroids_lib::utils::Logger;
using ::itandroids_lib::math::Vector3;
using ::itandroids_lib::math::Pose2D;


using namespace decision_making::behavior::active_vision;

int main() {

    ActiveVisionAgent agent("127.0.0.1", 3100, 3200, 1,
                            4, std::string("ITAndroids"));
    agent.testLoop(9000);
    return 0;
}
