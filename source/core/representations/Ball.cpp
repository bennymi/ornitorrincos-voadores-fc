/*
 * Ball.cpp
 *
 *  Created on: Aug 29, 2015
 *      Author: mmaximo
 */

#include "Ball.h"

namespace representations {

    Ball::Ball() {
        hasRelevance = true;
        lastHeardTime = 0;
    }

    Ball::~Ball() {
    }

    itandroids_lib::math::Pose2D &Ball::getPosition() {
        return position;
    }

    itandroids_lib::math::Pose2D &Ball::getVelocity() {
        return velocity;
    }

    void Ball::setPosition(itandroids_lib::math::Pose2D pos) {
        position = pos;
    }

    void Ball::setVelocity(itandroids_lib::math::Pose2D velocity) {
        this->velocity = velocity;
    }

    void Ball::setLastSeenTime(double globalTime) {
        this->lastSeenTime = globalTime;
    }

    void Ball::setRelevant(bool relevant) {
        this->hasRelevance = relevant;
    }

    bool Ball::isRelevant() {
        return hasRelevance;
    }

    double Ball::getLastSeenTime() {
        return lastSeenTime;
    }

    void Ball::setLastHeardTime(double globalTime) {
        this->lastHeardTime = globalTime;
    }

    double Ball::getLastHeardTime() {
        return lastHeardTime;
    }

    double Ball::getAge(double currentTime) {
        return currentTime - lastSeenTime;
    }

    Pose2D &Ball::getLocalPosition() {
        return localPosition;
    }

    Pose2D &Ball::getLocalVelocity() {
        return localVelocity;
    }

    void Ball::setLocalPosition(Pose2D pos) {
        this->localPosition = pos;
    }

    void Ball::setLocalVelocity(Pose2D velocity) {
        this->localVelocity = velocity;
    }
} /* namespace representations */
