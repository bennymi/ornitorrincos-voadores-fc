//
// Created by dicksiano on 8/14/16.
//

#include "OwnFreeKickSetPlay.h"
#include "../BehaviorFactory.h"

namespace decision_making {
    namespace behavior {

        OwnFreeKickSetPlay::OwnFreeKickSetPlay() : SetPlay(nullptr) {
        }

        OwnFreeKickSetPlay::OwnFreeKickSetPlay(BehaviorFactory *behaviorFactory) : SetPlay(behaviorFactory) {
        }

        OwnFreeKickSetPlay::~OwnFreeKickSetPlay() {
        }

        void OwnFreeKickSetPlay::behave(modeling::Modeling &modeling) {
            /// IMPLEMENTE
        } /* function behave */

    } /* namespace behavior */
} /* namespace decision_making */