//
// Created by mmaximo on 20/06/16.
//

#include "decision_making/behavior/NavigateToBall.h"
#include "decision_making/behavior/BehaviorFactory.h"

namespace decision_making {
    namespace behavior {

        NavigateToBall::NavigateToBall(BehaviorFactory *factory) : Behavior(factory) {

        }

        NavigateToBall::NavigateToBall(BehaviorFactory *factory, PotentialFieldParameters parameters) {

        }

        void NavigateToBall::behave(modeling::Modeling &modeling) {
            Vector2<double> ballPosition = modeling.getWorldModel().getBall().getPosition().translation;
            behaviorFactory->getNavigatePosition().setTarget(ballPosition);
            behaviorFactory->getNavigatePosition().behave(modeling);
            movementRequest = behaviorFactory->getNavigatePosition().getMovementRequest();
        }

    }
}