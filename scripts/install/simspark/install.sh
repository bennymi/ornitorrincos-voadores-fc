#!/usr/bin/env bash
#install server

sudo apt-get install g++ subversion cmake libfreetype6-dev libode-dev libsdl-dev ruby ruby-dev libdevil-dev libboost-dev libboost-thread-dev libboost-regex-dev libboost-system-dev qt4-default subversion -y
svn co https://svn.code.sf.net/p/simspark/svn/trunk ~/simspark


pushd ~/simspark/spark
mkdir -p build
cd build
cmake ..
make -j 4
sudo make install
sudo ldconfig
popd

pushd ~/simspark/rcssserver3d
mkdir -p build
cd build
cmake ..
make -j 4
sudo make install
sudo ldconfig
sudo echo "/usr/local/lib" >> sudo /etc/ld.so.conf
sudo ldconfig
popd

