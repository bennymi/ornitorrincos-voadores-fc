//
// Created by dicksiano on 8/21/16.
//

#include "base/BaseAgentTest.h"

class BeforeKickOff_AgentTest : public BaseAgentTest {

public:

    virtual void testLoop(int nSteps) { BaseAgentTest::testLoop(nSteps); }

    virtual void setup() {
        wiz.setPlayMode(representations::RawPlayMode::BEFORE_KICK_OFF);
    }

    virtual void runStep() {
        wiz.setPlayMode(representations::RawPlayMode::BEFORE_KICK_OFF);

        utils::roboviz::Roboviz roboviz1;
        std::string name1("animated");
        std::string buff1("animated");
        roboviz1.swapBuffers(&buff1);


        roboviz1.drawLine(-2.20, 0.0, 0, -2.20, 0.0, 1, 1, 20, 20, 20, &name1);
        roboviz1.drawLine(-2.20, 0.7, 0, -2.20, 0.7, 1, 1, 20, 20, 20, &name1);
        roboviz1.drawLine(-2.20, -0.7, 0, -2.20, -0.7, 1, 1, 20, 20, 20, &name1);

        Pose2D selfPosition = modeling.getWorldModel().getSelf().getPosition();
        roboviz1.drawLine(selfPosition.translation.x, selfPosition.translation.y, 0,
                          selfPosition.translation.x, selfPosition.translation.y, 1,
                          1, 20, 20, 20, &name1);

        if (!decisionMaking.decideFall(modeling)) {
            behaviorFactory.getSetPlay().behave(modeling);
            decisionMaking.beamRequest = behaviorFactory.getSetPlay().getBeamRequest();

            behaviorFactory.getActiveVision().behave(modeling);
            decisionMaking.lookRequest = behaviorFactory.getActiveVision().getLookRequest();
        }
    }
};

int main() {

    BeforeKickOff_AgentTest agent;
    agent.testLoop(50000);
    return 0;

}