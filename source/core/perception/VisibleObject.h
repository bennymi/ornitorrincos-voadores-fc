/*
 * VisibleObject.h
 *
 *  Created on: Sep 1, 2015
 *      Author: itandroids
 */

#ifndef SOURCE_PERCEPTION_VISIBLEOBJECT_H_
#define SOURCE_PERCEPTION_VISIBLEOBJECT_H_

#include "math/Vector3.h"
#include "math/MathUtils.h"
#include <map>
#include "boost/algorithm/string.hpp"
#include "perception/Perceptor.h"
#include "utils/parser/KeyValuePair.h"
#include "perception/VisibleObjectType.h"
#include "utils/parser/ParserTreeNode.h"
#include "math/Vector3.h"
#include "math/Pose3D.h"

namespace perception {

    using namespace itandroids_lib::math;

    class VisibleObject : public Perceptor {
    public:
        /**
         * Default Constructor for visibleObject
         */
        VisibleObject();

        VisibleObject(std::string name, perception::VisibleObjectType::VISIBLE_OBJECT_TYPE objectType,
                      Vector3<double> position);

        /**
         * Constructor of the Visible Object
         * @param node
         */
        VisibleObject(utils::parser::ParserTreeNode &node);

        /**
         * Default Destructor
         */
        virtual ~VisibleObject();

        /**
         * Gets the name of the object
         * @return name of the object
         */
        std::string getName();

        /**
         * Gets the position of the object in cartesian coordinates
         * @return vector3 of the position
         */
        Vector3<double> getPosition();

        /**
         * Gets the distance from the camera to the object
         * @return a double that represents the distance
         */
        double getDistance();

        /**
         * Gets the horizontal angle of the object in relation to the camera
         * @return horizontal angle of the object
         */
        double getHorizontalAngle();

        /**
         * Gets the horizontal angle of the object in relation to the camera
         * @return horizontal angle of the object in degrees
         */
        double getHorizontalAngleDeg();

        /**
         * Gets the latitude angle of the object in relation to the camera
         * @return latitude angle of the object in radians
         */
        double getLatitudeAngle();

        /**
         * Gets the latitude angle of the object in relation to the camera
         * @return latitude angle of the object in degrees
         */
        double getLatitudeAngleDeg();

        /**
         *
         * @return visible object type
         */
        perception::VisibleObjectType::VISIBLE_OBJECT_TYPE &getVisibleObjectType();

        virtual void transformPosition(Pose3D &transform);

    protected:
        Vector3<double> position;
        perception::VisibleObjectType::VISIBLE_OBJECT_TYPE objectType;
        std::string name;

        /**
         * Sets the object's type
         * @param name
         */
        void setObjectType(std::string name);
    };

} /* namespace perception */

#endif /* SOURCE_PERCEPTION_VISIBLEOBJECT_H_ */
