//
// Created by francisco on 19/12/16.
//

#ifndef ITANDROIDS_SOCCER3D_CPP_KICKNODECREATOR_H
#define ITANDROIDS_SOCCER3D_CPP_KICKNODECREATOR_H

#include "utils/configure_tree/NodeCreator.h"

namespace utils {
namespace configure_tree {
namespace control {

    class KickNodeCreator : NodeCreator<KickNodeCreator>{
    public:
        template<class Content, class Key>
        std::shared_ptr<Parameter<Content,Key>> generateNode(){

        }

    };
    template<>
    std::shared_ptr<Parameter<pt::ptree,std::string>> KickNodeCreator::generateNode();
}
}
}




#endif //ITANDROIDS_SOCCER3D_CPP_KICKNODECREATOR_H
