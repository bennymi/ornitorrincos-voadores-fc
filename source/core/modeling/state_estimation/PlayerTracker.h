/*
 * PlayerTracker.h
 *
 *  Created on: Aug 29, 2015
 *      Author: mmaximo
 */

#ifndef SOURCE_MODELING_STATE_ESTIMATION_PLAYERTRACKER_H_
#define SOURCE_MODELING_STATE_ESTIMATION_PLAYERTRACKER_H_

#include "math/Vector3.h"
#include "math/Pose2D.h"
#include "perception/VisiblePlayer.h"
#include "signal_processing/LinearKalmanFilter.h"
#include "BallTracker.h"

namespace modeling {
    namespace state_estimation_player {

        using namespace itandroids_lib::math;
        using namespace itandroids_lib::signal_processing;
        using namespace representations;

        class PlayerTracker {
        public:
            PlayerTracker();

            virtual ~PlayerTracker();

            Pose2D getPosition();

            Pose2D getVelocity();

            void update(double globalTime, perception::VisiblePlayer *visiblePlayer,
                        Pose2D &odometry);

            void update(double globalTime, const Pose2D &odometry);

            void resetEstimate();

            bool hasLostPlayer();

        private:
            LinearDiscreteStochasticModel stochasticModel1;
            LinearKalmanFilter KF1;
            int counterNotViewPlayer;
            bool lostPlayer;
        };

    } /* namespace state_estimation */
} /* namespace modeling */

#endif /* SOURCE_MODELING_STATE_ESTIMATION_PLAYERTRACKER_H_ */
