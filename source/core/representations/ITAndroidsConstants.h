/*
 * ITAndroidsConstants.h
 *
 *  Created on: Oct 9, 2015
 *      Author: itandroids
 */

#ifndef SOURCE_REPRESENTATIONS_ITANDROIDSCONSTANTS_H_
#define SOURCE_REPRESENTATIONS_ITANDROIDSCONSTANTS_H_

#include <iostream>

namespace representations {

    class ITAndroidsConstants {
    public:
        // Server constants
        static const std::string DEFAULT_IP;
        static const int DEFAULT_PORT;
        static const double SAMPLE_TIME;

        //Team constants
        static const std::string TEAM_DEFAULT_NAME;
        static const std::string KEYFRAME_FOLDER;
        static const std::string FORMATION_FOLDER;
        //Robot constants11

        //Robot constants
        static const double DEFAULT_JOINT_MAX_SPEED;

    };

}

#endif /* SOURCE_REPRESENTATIONS_ITANDROIDSCONSTANTS_H_ */
