/*
 * ForceResistanceData.cpp
 *
 *  Created on: Aug 30, 2015
 *      Author: itandroids
 */

#include "ForceResistanceData.h"

namespace representations {

    ForceResistanceData::ForceResistanceData() {
        originCoordinates = Vector3<double>();
        force = Vector3<double>();
    }

    ForceResistanceData::ForceResistanceData(Vector3<double> originCoordinates,
                                             Vector3<double> force) {
        this->originCoordinates = originCoordinates;
        this->force = force;
    }

    ForceResistanceData::~ForceResistanceData() {
    }

    Vector3<double> &ForceResistanceData::getOriginCoordinates() {
        return originCoordinates;
    }

    Vector3<double> &ForceResistanceData::getForce() {
        return force;
    }

} /* namespace representations */
