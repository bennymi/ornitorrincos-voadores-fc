//
// Created by francisco on 6/22/16.
//

#ifndef ITANDROIDS_SOCCER3D_CPP_ATTACKERROLE_AGENTTEST_H
#define ITANDROIDS_SOCCER3D_CPP_ATTACKERROLE_AGENTTEST_H

#include "base/BaseFullAgentTest.h"

class AttackerRole_AgentTest : public BaseFullAgentTest {

public:
    using BaseFullAgentTest::BaseFullAgentTest;
private:
    void runStep() override;

    void setup() override;
};

#endif //ITANDROIDS_SOCCER3D_CPP_ATTACKERROLE_AGENTTEST_H
