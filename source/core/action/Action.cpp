/*
 * Action.cpp
 *
 *  Created on: Aug 7, 2015
 *      Author: mmaximo
 */

#include "action/Action.h"

namespace action {

    const std::string Action::NAO_SCENE_STRING = "rsg/agent/nao/nao_hetero.rsg";

    Action::Action() {
    }

    Action::~Action() {
    }

    ActionImpl::ActionImpl() {
    }

    ActionImpl::~ActionImpl() {
        effectorFactory.clear();
    }

    void ActionImpl::create(int type) {
        effectorFactory.clear(); // Clear old effectors

        // Add Create Effector to effector factory
        effectorFactory.addCreateEffector(NAO_SCENE_STRING, type);
        serverMessage = effectorFactory.toString();
    }

    void ActionImpl::init(int playerNumber, std::string teamName) {
        effectorFactory.clear(); // Clear old effectors

        // Add Init Effector to effector factory
        effectorFactory.addInitEffector(playerNumber, teamName);
        serverMessage = effectorFactory.toString();
    }

    void ActionImpl::act(DecisionMaking &decisionMaking, Control &control) {
        effectorFactory.clear(); // Clear old effectors

        // If there is a Say Message, add Say Effector to effector factory
        if (decisionMaking.getSayMessage() != nullptr) {
            effectorFactory.addSayEffector(*(decisionMaking.getSayMessage()));
        }

        // If there is a Beam Request, add Beam Request to effector factory
        if (decisionMaking.getBeamRequest() != nullptr) {
            effectorFactory.addBeamEffector(*(decisionMaking.getBeamRequest()));
        }
        // Add Joints Effectors to effector factory
        effectorFactory.addJointsEffectors(control.getJointsCommands());
        serverMessage = effectorFactory.toString();
    }

    const std::string &ActionImpl::getServerMessage() {
        return serverMessage;
    }

} /* namespace action */
