//
// Created by francisco on 6/20/16.
//

#ifndef ITANDROIDS_SOCCER3D_CPP_SIMPLEAGENTTEST_H
#define ITANDROIDS_SOCCER3D_CPP_SIMPLEAGENTTEST_H

#include "BaseRoleAgentTest.h"

class SimpleAgentTest : public BaseRoleAgentTest {
public:
    using BaseRoleAgentTest::BaseRoleAgentTest;
private:
    virtual void setup();

    virtual void runStep();
};


#endif //ITANDROIDS_SOCCER3D_CPP_SIMPLEAGENTTEST_H
