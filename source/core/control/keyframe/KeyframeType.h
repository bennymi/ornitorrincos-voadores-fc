/*
 * KeyframeMotionConstants.h
 *
 *  Created on: Sep 29, 2014
 *      Author: manga
 */

#ifndef KEYFRAMEMOTIONPAGETYPE_H_
#define KEYFRAMEMOTIONPAGETYPE_H_

#include <iostream>

namespace control {

    namespace keyframe {

        class KeyframeType {
        public:
            static const std::string TEST;
            static const std::string INIT;
            static const std::string STAND_UP_FROM_BACK;
            static const std::string STAND_UP_FROM_FRONT;
            static const std::string FALL_BACK;
            static const std::string FALL_FRONT;
            static const std::string FALL_RIGHT_SIDE;
            static const std::string MOVE_ARM_TO_FALL_BACK;
            static const std::string KICK_RIGHT_LEG;
            static const std::string KICK_LEFT_LEG;
            static const std::string CATCH_RIGHT;
            static const std::string CATCH_LEFT;
            static const std::string NONE;


        };

    } /* namespace keyframe */

} /* namespace control */

#endif /* KEYFRAMEMOTIONCONSTANTS_H_ */
