/*
 * BeamEffector.cpp
 *
 *  Created on: Aug 8, 2015
 *      Author: mmaximo
 */

#include "action/BeamEffector.h"

namespace action {

    BeamEffector::BeamEffector(const Pose2D &pose) :
            pose(pose) {
    }

    BeamEffector::~BeamEffector() {
    }

    std::string BeamEffector::toString() {

        // Add x,y and psi coordinates to an string
        std::stringstream stream;
        stream << "(beam ";
        stream << pose.translation.x;
        stream << " ";
        stream << pose.translation.y;
        stream << " ";
        stream << pose.rotation;
        stream << ")";
        return stream.str();
    }

} /* namespace action */
