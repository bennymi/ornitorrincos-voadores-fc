//
// Created by francisco on 16/12/16.
//

#include "AgentCommandLineParser.h"
#include <boost/program_options.hpp>
#include <iostream>
#include <iterator>


using representations::ITAndroidsConstants;
namespace po = boost::program_options;

namespace utils {
    namespace command_line_parser {

        AgentCommandLineParser::AgentCommandLineParser():serverIP (ITAndroidsConstants::DEFAULT_IP),
                                                         serverPort (ITAndroidsConstants::DEFAULT_PORT),
                                                         teamName (ITAndroidsConstants::TEAM_DEFAULT_NAME),
                                                         agentNumber (10){
        }

        void AgentCommandLineParser::parse(int argc, char **argv) {
            //Parsing arguments

            std::string serverIPCommand = "server-ip";
            std::string serverPortCommand = "server-port";
            std::string agentNumberCommand = "agent-number";
            std::string teamNameCommand = "team-name";


            po::options_description desc("Allowed options");
            desc.add_options().operator()("help", "produce help message").operator()
                    (serverIPCommand.c_str(), po::value<std::string>(), "set server ip").operator()
                    (serverPortCommand.c_str(), po::value<int>(), "set server port").operator()
                    (agentNumberCommand.c_str(), po::value<int>(), "set Agent Number").operator()
                    (teamNameCommand.c_str(), po::value<std::string>(), "set team name")
                    ;

            po::variables_map variablesMap;
            po::store(po::parse_command_line(argc, argv, desc), variablesMap);
            po::notify(variablesMap);
            if(variablesMap.count("help")){
                std::cout << desc << std::endl;
                return;
            }
            if(variablesMap.count(serverIPCommand)){
                serverIP = variablesMap[serverIPCommand].as<std::string>();
            }
            if(variablesMap.count(serverPortCommand)){
                serverPort = variablesMap[serverPortCommand].as<int>();
            }
            if(variablesMap.count(teamNameCommand)){
                teamName = variablesMap[teamNameCommand].as<std::string>();
            }
            if(variablesMap.count(agentNumberCommand)){
                agentNumber = variablesMap[agentNumberCommand].as<int>();
            }


        }

        std::string AgentCommandLineParser::getServerIP() {
            return serverIP;
        }

        int AgentCommandLineParser::getServerPort() {
            return serverPort;
        }

        std::string AgentCommandLineParser::getTeamName() {
            return teamName;
        }

        int AgentCommandLineParser::getAgentNumber() {
            return agentNumber;
        }
    }
}