/*
 * GyroRatePerceptor.h
 *
 *  Created on: Aug 8, 2015
 *      Author: mmaximo
 */

#ifndef SOURCE_PERCEPTION_GYRORATEPERCEPTOR_H_
#define SOURCE_PERCEPTION_GYRORATEPERCEPTOR_H_

#include "utils/parser/KeyValuePair.h"
#include "utils/parser/ParserTreeNode.h"
#include "perception/Perceptor.h"
#include "math/Vector3.h"
#include "math/MathUtils.h"
#include <boost/lexical_cast.hpp>
#include <boost/algorithm/string.hpp>

namespace perception {

    using namespace itandroids_lib::math;

    class GyroRatePerceptor : public Perceptor {
    public:
        /**
         * Constructor for GyroRatePerceptor
         *
         * @param node part of the parser tree that represents
         *             an gyrometer perceptor.
         */
        GyroRatePerceptor(utils::parser::ParserTreeNode &node);

        /**
         * Destructor
         */
        virtual ~GyroRatePerceptor();

        std::string getName();

        Vector3<double> getAngularVelocity();

    protected:

    private:
        std::string name;
        Vector3<double> angularVelocity;

    };

} /* namespace perception */

#endif /* SOURCE_PERCEPTION_GYRORATEPERCEPTOR_H_ */
