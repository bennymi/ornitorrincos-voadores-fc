//
// Created by luckeciano on 9/27/16.
//

#include "PositioningModel.h"
#include "WorldModel.h"
#include "decision_making/positioning/role_assignment/VotingSystem.h"

namespace modeling {

    PositioningModel::PositioningModel() {
        teammatesRoleAssignmentVector.reserve(modeling::WorldModel::NUM_PLAYERS_PER_TEAM);
        completePositioning = false;
        std::vector<int> initialPositioning = std::vector<int>({0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10});
        myRoleAssignmentVector = initialPositioning;
        synchronizedRoleAssignmentVector = initialPositioning;
        for (int i = 0; i < modeling::WorldModel::NUM_PLAYERS_PER_TEAM; i++) {
            teammatesRoleAssignmentVector.push_back(initialPositioning);
        }


    }

    void PositioningModel::update(double globalTime, representations::HearData hearData, std::string ourTeamName) {
        if (hearData.isFromSelf()) {
            return; //Prevents a update from a message sent by himself
        } else if (hearData.getMessage().size() == 0) {
            return; //Prevents from a empty message.
        } else if (hearData.getUniformNumber() == 1) {
            return; //prevents update from goalie message.
        }


        //update teammates role assignment vector
        if (hearData.hasStarted() && hearData.getTeam() == ourTeamName)
            teammatesRoleAssignmentVector[hearData.getUniformNumber() - 1] = hearData.getRoleAssigment();
        if (hearData.getUniformNumber() == modeling::WorldModel::NUM_PLAYERS_PER_TEAM) completePositioning = true;
        if (completePositioning) {
            //Voting System - Generate Sync Role Assignment Vector
            decision_making::positioning::role_assignment::VotingSystem votingSystem;
            synchronizedRoleAssignmentVector = votingSystem.generateSyncRoleVector(*this);
        }

    }

    std::vector<int> PositioningModel::getMyRoleAssignmentVector() {
        return myRoleAssignmentVector;
    }

    std::vector<int> PositioningModel::getSynchronizedRoleAssignmentVector() {
        return synchronizedRoleAssignmentVector;
    }

    std::vector<std::vector<int> > PositioningModel::getTeammatesRoleAssignmentVector() {
        return teammatesRoleAssignmentVector;
    }

    void PositioningModel::setMyRoleAssignmentVector(std::vector<int> newRoleVector) {
        myRoleAssignmentVector = newRoleVector;
    }

}