//
// Created by francisco on 5/22/16.
//

#ifndef ITANDROIDS_SOCCER3D_CPP_LINEARKEYFRAMEPAGE_H
#define ITANDROIDS_SOCCER3D_CPP_LINEARKEYFRAMEPAGE_H

#include "control/keyframe/page/KeyframePage.h"
#include "math/LinearInterpolator.h"

using ::itandroids_lib::math::LinearInterpolator;

namespace control {
    namespace keyframe {

        class LinearKeyframePage : public KeyframePage {
        public:
            std::vector<double> interpolate() override;

        private:
            void createInterpolators() override;
        };
    }
}


#endif //ITANDROIDS_SOCCER3D_CPP_LINEARKEYFRAMEPAGE_H
