//
// Created by dicksiano on 8/14/16.
//

#ifndef ITANDROIDS_SOCCER3D_CPP_OWNFREEKICKSETPLAY_H
#define ITANDROIDS_SOCCER3D_CPP_OWNFREEKICKSETPLAY_H


#include "../SetPlay.h"

namespace decision_making {
    namespace behavior {

        class OwnFreeKickSetPlay : public SetPlay {
        public:

            /**
            * Default constructor.
            */
            OwnFreeKickSetPlay();

            /**
            * Assignment constructor.
            *
            * @param pointer to BehaviorFactory.
            */
            OwnFreeKickSetPlay(BehaviorFactory *behaviorFactory);

            /**
            * Destructor.
            */
            virtual ~OwnFreeKickSetPlay();

            /**
            * This function controls the actions.
            *
            * @param modeling.
            */
            void behave(modeling::Modeling &modeling);

        private:

        };

    } /* namespace behavior */
} /* namespace decision_making */


#endif //ITANDROIDS_SOCCER3D_CPP_OWNFREEKICKSETPLAY_H
