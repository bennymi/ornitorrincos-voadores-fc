//
// Created by mmaximo on 22/06/16.
//

#include "Balance.h"

namespace control {

    Balance::Balance() {
        hipPitchGain = 0.03;
        anklePitchGain = 0.02;
        hipRollGain = 0.0;//0.05;
        ankleRollGain = 0.0;//0.02;
    }

    void Balance::balance(AgentModel &agentModel, NaoJoints &joints) {
        joints.leftHipPitch += hipPitchGain * agentModel.getTorsoAngularVelocity().x;
        joints.rightHipPitch += hipPitchGain * agentModel.getTorsoAngularVelocity().x;
        joints.leftFootPitch += anklePitchGain * agentModel.getTorsoAngularVelocity().x;
        joints.rightFootPitch += anklePitchGain * agentModel.getTorsoAngularVelocity().x;
        joints.leftHipRoll += hipRollGain * agentModel.getTorsoAngularVelocity().y;
        joints.rightHipRoll += hipRollGain * agentModel.getTorsoAngularVelocity().y;
        joints.leftFootRoll += ankleRollGain * agentModel.getTorsoAngularVelocity().y;
        joints.rightFootRoll += ankleRollGain * agentModel.getTorsoAngularVelocity().y;
    }

}

