/*
 * RobovizDraw.h
 *
 *  Created on: Oct 7, 2015
 *      Author: itandroids
 */

#ifndef SOURCE_UTILS_ROBOVIZ_ROBOVIZDRAW_H_
#define SOURCE_UTILS_ROBOVIZ_ROBOVIZDRAW_H_

#include <string>
#include <cstdio>
#include <cstring>

namespace utils {
    namespace roboviz {

        class RobovizDraw {
        public:
            RobovizDraw();

            virtual ~RobovizDraw();

            int writeCharToBuf(unsigned char *buf, unsigned char value);

            int writeFloatToBuf(unsigned char *buf, float value);

            int writeColorToBuf(unsigned char *buf, const float *color, int channels);

            int writeStringToBuf(unsigned char *buf, const std::string *text);

            unsigned char *newBufferSwap(const std::string *name, int *bufSize);

            unsigned char *newCircle(const float *center, float radius, float thickness,
                                     const float *color, const std::string *setName, int *bufSize);

            unsigned char *newLine(const float *a, const float *b, float thickness,
                                   const float *color, const std::string *setName, int *bufSize);

            unsigned char *newPoint(const float *p, float size, const float *color,
                                    const std::string *setName, int *bufSize);

            unsigned char *newSphere(const float *p, float radius, const float *color,
                                     const std::string *setName, int *bufSize);

            unsigned char *newPolygon(const float *v, int numVerts, const float *color,
                                      const std::string *setName, int *bufSize);

            unsigned char *newAnnotation(const std::string *text, const float *p,
                                         const float *color, const std::string *setName, int *bufSize);

            unsigned char *newAgentAnnotation(const std::string *text, bool leftTeam,
                                              int agentNum, const float *color, int *bufSize);

        };

    } /* namespace roboviz */
} /* namespace utils */

#endif /* SOURCE_UTILS_ROBOVIZ_ROBOVIZDRAW_H_ */
