/* * PerceptorFactory.h
 *
 *  Created on: Aug 15, 2015
 *      Author: itandroids
 */

#ifndef SOURCE_perception_PERCEPTORFACTORY_H_
#define SOURCE_perception_PERCEPTORFACTORY_H_

#include <list>
#include <map>

#include "utils/parser/KeyValuePair.h"
#include "utils/parser/ParserTreeNode.h"
#include "perception/AccelerometerPerceptor.h"
#include "perception/AgentStatePerceptor.h"
#include "perception/ForceResistancePerceptor.h"
#include "perception/GameStatePerceptor.h"
#include "perception/GyroRatePerceptor.h"
#include "perception/HearPerceptor.h"
#include "perception/HingeJointPerceptor.h"
#include "perception/VisionPerceptor.h"
#include "perception/TouchPerceptor.h"
#include "perception/UniversalJointPerceptor.h"
#include "perception/GlobalTimePerceptor.h"
#include "perception/Perceptor.h"

namespace perception {

    class PerceptorFactory {
    public:
        PerceptorFactory();

        virtual ~PerceptorFactory();

        /**
         * Updates the vector of perceptors
         */
        void update(utils::parser::ParserTreeNode &node);

        /**
         * Getter for the perceptors
         * @return vector of perceptor *
         */
        std::vector<Perceptor *> &getPerceptors();

    protected:

    private:
        /**
         *
         * General Perceptors
         */
        std::vector<Perceptor *> perceptors;
    };

} /* namespace perception */

#endif /* SOURCE_perception_PERCEPTORFACTORY_H_ */
