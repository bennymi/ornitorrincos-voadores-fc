/*
 * SelfModel.cpp
 *
 *  Created on: Aug 29, 2015
 *      Author: mmaximo
 */

#include <core/representations/NaoJoints.h>
#include "SelfModel.h"

namespace modeling {

    SelfModel::SelfModel(FieldDescription &fieldDescription) :
            localizer(fieldDescription, 300, LocalizerParams::getDefaultLocalizerParams()) {
    }

    SelfModel::~SelfModel() {
    }

    void SelfModel::update(double globalTime,
                           const std::vector<perception::VisibleFlag *> &visibleFlags,
                           const std::vector<perception::VisibleGoalPost *> &visibleGoalPosts,
                           const std::vector<perception::VisibleLine *> &visibleLines,
                           Pose2D &odometry, representations::NaoJoints &jointsPosition) {
        localizer.update(globalTime, visibleFlags, visibleGoalPosts, visibleLines,
                         odometry, jointsPosition);
        self.setPosition(localizer.getPoseEstimate());
    }

    Self &SelfModel::getObject() {
        return self;
    }

    void SelfModel::resetLocalizer(Pose2D estimate, Pose2D sigmas) {
        localizer.reset(estimate, sigmas);
    }

} /* namespace modeling */
