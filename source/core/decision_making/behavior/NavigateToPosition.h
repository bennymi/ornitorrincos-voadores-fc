/*
 * NavigateToPosition.h
 *
 *  Created on: Oct 31, 2015
 *      Author: itandroids
 */

#ifndef SOURCE_DECISION_MAKING_BEHAVIOR_NAVIGATETOPOSITION_H_
#define SOURCE_DECISION_MAKING_BEHAVIOR_NAVIGATETOPOSITION_H_

#include "Behavior.h"
#include <cmath>
#include "math/Vector3.h"
#include "control/PController.h"
#include "math/Pose2D.h"
#include "representations/NavigationParams.h"
#include "modeling/Modeling.h"
#include "decision_making/behavior/NavigationHelper.h"
#include "control/walking/OmnidirectionalWalkZMP.h"

using namespace itandroids_lib::math;


namespace decision_making {
    namespace behavior {
        class BehaviorFactory;

        using decision_making::behavior::NavigationHelper;

        class NavigateToPosition : public behavior::Behavior {
        public:
            /**
             * Default constructor.
             */
            NavigateToPosition();

            /**
             * Assignment constructor.
             *
             * @param behaviorFactory pointer to BehaviorFactory object.
             */
            NavigateToPosition(BehaviorFactory *behaviorFactory);

            /**
             * Destructor.
             */
            virtual ~NavigateToPosition();

            /**
             * Sets the navigation attributes.
             */
            virtual void configureUsingDefaultParams();

            virtual void configurePreciseModeParams();

            /**
             * Sets a target.
             *
             * @param target's coordinates.
             */
            virtual void setTarget(itandroids_lib::math::Pose2D target);

            /**
             * Moves to the target.
             *
             * @param modeling.
             */
            virtual void behave(modeling::Modeling &modeling);

            void setPreciseMode(bool preciseMode);

        private:


            double CLOSE_DISTANCE_THRESHOLD;
            double LOW_VFORWARD_FACTOR;
            double LOW_VPSI_FACTOR;
            double LOW_VBACKWARD_FACTOR;
            double LOW_VY_FACTOR;
            double VELOCITY_GAIN;

            bool insideCloseDistance;

            Pose2D target;

            double maxVforward;
            double maxVbackward;
            double maxVy;
            double maxVpsi;

            bool preciseMode;
            double preciseWalkSaturation;

            NavigationHelper navigationHelper;

            /**
             * Intents to define the highest velocity the robot can execute.
             *
             * @param walkVelocity actual robot velocity.
             */
            Pose2D saturateWalkVelocity(Pose2D walkVelocity);

        };


    } /* namespace behavior */
} /* namespace decision_making */

#endif /* SOURCE_DECISION_MAKING_BEHAVIOR_NAVIGATETOPOSITION_H_ */
