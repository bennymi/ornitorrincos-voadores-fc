//
// Created by francisco on 19/12/16.
//

#ifndef ITANDROIDS_SOCCER3D_CPP_REPRESENTATIONSNODECREATOR_H
#define ITANDROIDS_SOCCER3D_CPP_REPRESENTATIONSNODECREATOR_H

#include "utils/configure_tree/NodeCreator.h"



namespace utils{
namespace configure_tree{
namespace representations{

    class RepresentationsNodeCreator : public NodeCreator<RepresentationsNodeCreator>{
    public:
        template<class Content, class Key>
        std::shared_ptr<Parameter<Content,Key>> generateNode(){

        }

    };
template<>
std::shared_ptr<Parameter<pt::ptree,std::string>> RepresentationsNodeCreator::generateNode();
}
}
}



#endif //ITANDROIDS_SOCCER3D_CPP_REPRESENTATIONSNODECREATOR_H
